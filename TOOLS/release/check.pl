#!/usr/bin/perl

use File::Basename;
use Term::ANSIColor;

use Getopt::Std;
getopts("a",\%opts); # show status of RE files

@files = `find . -type f -not -name ".release_exclude"`;

@excluded = `find . -type f -name ".release_exclude" -exec dirname {} \\;`;

foreach $rep (@excluded) {
    chop($rep);
}

$dirname_prev="";

foreach $file (@files) {
    chop($file);
    
    if (!($file =~ /\.svn/)) {

	$exclu = 0;
	foreach $rep (@excluded) {
	    if ($file =~ /$rep/) { 
		$exclu = 1; 
	    }
	}
	
	if (($exclu == 0) || (defined $opts{a})) {
##
	    $dirname = dirname($file);
	    if (!($dirname =~ /^$dirname_prev$/)) {
		print color 'reset';
		print color 'bold';
		print "$dirname";

		if ($exclu == 1) {
		    print color 'bold blue';	    
		    print "  RE";
		    print color 'reset';
		}

		print "\n";

		$dirname_prev = $dirname;
	    }
	    
##

	    $ffile = basename($file);

	    $wc = `echo -n $ffile | wc -c`;
	    if ($wc < 5) { $ffile .= ": \t\t\t\t"; } 
	    elsif ($wc < 13) { $ffile .= ": \t\t\t"; } 
	    elsif ($wc < 21) { $ffile .= ": \t\t"; } 
	    elsif ($wc < 29) { $ffile .= ": \t"; } 
	    else  { $ffile .= ": "; } 

	    open(FILE, "$file") || die "Can't open '$file': $!\n";
	    @srcfile = <FILE>;
	    close(FILE);
	    
	    @authors = (); #NOT
	    @institutes = ();
	    $noheader=0;
	    $release_exclude=0;
	    $count=0;
	    foreach (@srcfile) {
		$count++;
		if($count <= 5) {

		    if (($_ =~ /\@authors/ ) && (!($_ =~ /NOT/))) { #NOT
			chop($_);
			$_ =~ s/(.*authors\s*)(.*)(\*\/)/$2/g;
			$_ =~ s/,\s*/,/g;
			
			$_ =~ s/^[\s]*//g;
			$_ =~ s/[\s]*$//g;
			
			push @authors, split(/,/); #NOT
		    } 
		    elsif (($_ =~ /\@institutes\+/ ) && (!($_ =~ /NOT/))) {
			chop($_);
			$_ =~ s/(.*institutes\+\s*)(.*)(\*\/)/$2/g;
			$_ =~ s/,\s*/,/g;
			
			$_ =~ s/^[\s]*//g;
			$_ =~ s/[\s]*$//g;
			
			push @institutes, split(/,/);
		    } 
		    elsif (($_ =~ /\@noheader/ ) && (!($_ =~ /NOT/))) {
			$noheader=1;
		    } 
		    elsif (($_ =~ /\@release_exclude/ ) && (!($_ =~ /NOT/))) {
			$release_exclude=1;
		    } 
		}
		
	    }
	    
	    if (($release_exclude == 0) || (defined $opts{a})) {

		print color 'reset';
		print color 'bold blue';	    
		if (($noheader == 1) && ($release_exclude == 1)) {
		    print "  NH RE  ";
		}
		elsif ($noheader == 1) {
		    print "  NH     ";
		}
		elsif ($release_exclude == 1) {
		    print "  RE     ";
		}
		elsif ($exclu == 1) {
		    print color 'reset';
		    print color 'blue';
		    print "   |     ";
		}
		else { 
		    print "         ";
		}
		print color 'reset';
		
		print $ffile.' ';
		
		if ($#authors < 0) { #NOT
		    $car = '??';
		    
		    if (($file =~ /\.c$/) || ($file =~ /\.h$/)) {
			print color 'bold';
		    }
		    
		    if (($noheader == 0) && ($release_exclude == 0)) {
			print color 'red';		    
		    } else {
			$car = '--';
		    }
		    
		    print $car;
		} else {
		    $txt='';
		    foreach (@authors) { #NOT
			@tmp = ();
			push @tmp, split(/ /);
			$txt .="$tmp[1], " 
			}
		    chop($txt); chop($txt);
		    print $txt.' ';
		    
		    if (($noheader == 0) && ($release_exclude == 0) && 
			(!(($file =~ /\.c$/) || ($file =~ /\.h$/)))) {
			print color 'red bold';
			print '/!\ not a source file => no header processing';
		    }
		    
		} 	 
		
		print color 'reset';
		if ($#institutes >= 0) {
#todo : institutes only if authors ...
		    $txt='';
		    foreach (@institutes) {
			$txt .="+$_, ";
		    }
		    chop($txt); chop($txt);
		    print $txt;
		} 	 
		
		print "\n";
		
	    }
	}
    }
}

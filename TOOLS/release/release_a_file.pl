#!/usr/bin/perl

use File::Basename;
use Cwd qw(realpath);
my $fullpath = dirname(realpath($0));

if ($#ARGV == -1) { exit 1; }

$srcfilename = shift;
$srcfilebasename = basename($srcfilename);

$copyright_date = '2008-2009';

$copyright_desc =
    'This file is part of HIPS : Hierarchical Iterative'.
    "\n                ".
    'Parallel Solver (http://hips.gforge.inria.fr/).';

%labos_ref = ("P. HENON" => "INRIA",
	      "J. GAIDAMOUR"=> "CNRS");

$headerfilename = "$fullpath/header.tpl";

print " - $srcfilename\n";

#system("indent -kr $srcfilename 2>&1 | cut -d':' -f3-");
#if ($? != 0) { exit 1; }
#system("rm $srcfilename".'~');

open(FILE, "$srcfilename") || die "Can't open '$srcfilename': $!\n";
@srcfile = <FILE>;
close(FILE);

@authors = (); #NOT
@institutes = ();
$noheader=0;
$count=0;
foreach (@srcfile) {
    $count++;
    if($count <= 5) {

	if (($_ =~ /\@authors/ ) && (!($_ =~ /NOT/))) { #NOT
	    chop($_);
	    $_ =~ s/(.*authors\s*)(.*)(\*\/)/$2/g; #NOT
	    $_ =~ s/,\s*/,/g;
	    
	    $_ =~ s/^[\s]*//g;
	    $_ =~ s/[\s]*$//g;
	    
	    push @authors, split(/,/); #NOT
	    
	} 
	elsif (($_ =~ /\@institutes\+/ ) && (!($_ =~ /NOT/))) { #NOT
	    chop($_);
	    $_ =~ s/(.*institutes\+\s*)(.*)(\*\/)/$2/g;
	    $_ =~ s/,\s*/,/g;
	    
	    $_ =~ s/^[\s]*//g;
	    $_ =~ s/[\s]*$//g;
	    
	    push @institutes, split(/,/); #NOT
	} 
	elsif (($_ =~ /\@noheader/ && (!($_ =~ /NOT/))) ) { #NOT
	    $noheader=1;
	} else {
	    $newsrcfile .= $_;
	}
    } else {
	$newsrcfile .= $_;
    }
       
}

if ($noheader == 0) {
    if ($#authors < 0) {
	print "  --> file $srcfilebasename : no author informations !\n";
    }
    
    my %count = (); # tmp
    my @labos = ();
    foreach $author (@authors) { #NOT
	if (exists $labos_ref{$author}) {
	    $labo = $labos_ref{$author};
	    push(@labos, $labo) unless $count{$labo}++;
	} else {
	    print "  --> file $srcfilebasename : author $author without additionnal informations !\n";
	}
    }
    foreach $labo (@institutes) {
	push(@labos, $labo) unless $count{$labo}++;
    }
    
#my %seen;
#my  @labos = grep !$seen{$_}++, @labos_tmp;
    
    $labos_l .= "$_, " for @labos;
    chop($labos_l);
    chop($labos_l);
    
###
    
    open(FILE, "cat $headerfilename | $fullpath/rectangular.sh |") || die "$!\n";
    @header = <FILE>;
    close $FILE;
    
###
    
    $noticeg .="Copyright $copyright_date $labos_l\n\n";
    $noticeg .="File          : ".$srcfilebasename."\n\n";
    $noticeg .="Description   : $copyright_desc\n\n";
    
#     if ($#authors >= 0) {
# 	$noticeg .="Authors       : $authors[0]\n";
# 	for($i=1; $i<=$#authors; $i++)
# 	{
# 	    $noticeg .="                $authors[$i]\n" ;
# 	}
# #$noticeg.= "\n";
#     }
    if ($#authors >= 0) {
 	$noticeg .="Authors       : $authors[0]";
 	for($i=1; $i<=$#authors; $i++)
 	{
 	    $noticeg .=" and $authors[$i] " ;
 	}
	$noticeg.= "\n";
    }

    
    chop($noticeg);
    
#$noticeg .="License       : CeCILL-C";
    
    open(FILE, "echo -n \"$noticeg\"  | $fullpath/rectangular.sh | ") || die "$!\n";
    @notice = <FILE>;
    close $FILE;
    
#print to the file
    open (FILE, ">$srcfilename") || die "Can't open '$srcfilename': $!\n"; #overwrite the old file in the new format
    
    print FILE @notice;
    print FILE "\n";
    print FILE @header;
    print FILE "\n";
    print FILE $newsrcfile;
    
    close(FILE);
}

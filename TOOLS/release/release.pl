#!/usr/bin/perl

use Getopt::Std;
use File::Basename;
use Cwd qw(realpath);

sub release {

    # Download HIPS (svn version)
    print "* Get lastest svn version ... \n";
    system('svn export svn+ssh://'.$LOGNAME.'@scm.gforge.inria.fr/svn/hips '. "$RELEASE_PATH >/dev/null");
    if ($? != 0) { exit 1; }

    system("rmdir $RELEASE_PATH/tags $RELEASE_PATH/branches");
    system("mv $RELEASE_PATH/trunk/* $RELEASE_PATH; rmdir $RELEASE_PATH/trunk");

    # .Svn remove si pas a partir svn

    # Move release specific files
    print "*  Move release specific files ... \n";
    system("find $RELEASE_PATH -name \"*Release\" -exec $PATH/mvrelease.sh {} ".'\\'.';');

    # Documentation
    print "*  Documentation ... \n";
    system("$PATH/docrelease.sh $RELEASE_PATH ".$opts{v});

    # Remove excluded directories
    $cmd = 'find $RELEASE_PATH -name ".release_exclude" -exec dirname {} '.'\\'.';';
    @removedir = `$cmd`;
    foreach $rep (@removedir){
	system("rm -rf ./$rep");
    }
    
    # Remove excluded files
    $cmd = 'egrep -r "@release_exclude" '.$RELEASE_PATH.'/* | cut -d ":" -f1 | sort | uniq'; #NOT
    @removefile = `$cmd`;
    foreach $file (@removefile){
	system("rm -f ./$file");
    }

    # Copyright notices (exclude links)
    print "* Copyright notices ... \n";
    system("find $RELEASE_PATH -type f -name \"*.[h,c]\" -exec $PATH/release_a_file.pl {} ".'\\'.';');

    # Make an archive
    print "* Archive $NAME".".tar.gz\n";
    system("tar -czf $NAME".".tar.gz $NAME");

    # End
    print "* END\n";

}

sub usage {
    $name = basename($0);

    print "$name [-h] [-l login_name] [-v version] [-d working_directory]\n";
    print "   -h   display this help and exit\n";
    print "   -l   Specifies the user to log in as on the remote machine\n";
    print "   -v   Choose version number for release\n";
    print "   -d   Choose a working directory (default : /tmp)\n";
}

system("which indent 2>&1 > /dev/null");
if ($? != 0) { 
    print "indent : command not found.\n";
    exit 1; 
}

getopts("hl:v:d:",\%opts);

if (defined $opts{h}) {
    usage();
    exit;
}

$LOGNAME = $ENV{LOGNAME};
if (defined $opts{l}){
        $LOGNAME = $opts{l};
}

$WORKING_DIR = '/tmp';
if (defined $opts{d}){
        $WORKING_DIR = $opts{d};
}

$NAME = 'hips';
if (defined $opts{v}){
    $NAME .= '-'.$opts{v};
}

$RELEASE_PATH = $NAME;

#$PATH=`pwd`;
#chop($PATH);
$PATH = dirname(realpath($0));

chdir $WORKING_DIR;
release();
chdir $PATH;

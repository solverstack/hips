#!/bin/bash

IFS=$'\n';

col=80

headopen='/*'
headclose='*/'
headchar='*'

open='/* '
close='*/'

###

headopen_s=$(echo -n "$headopen" | wc -c)
headclose_s=$(echo -n "$headclose" | wc -c)
headchar_s=$(echo -n "$headchar" | wc -c)

open_s=$(echo -n "$open" | wc -c)
close_s=$(echo -n "$close" | wc -c)

###

function headline {
    let nb=col-headopen_s-headclose_s
 
    echo -n "$headopen"
    for i in $(seq 1 $nb); do
	echo -n "$headchar"
    done
    echo "$headclose"
}

function line {
    l=`echo -n $* | wc -c`
    let nb=col-l-open_s-close_s

    if [ $nb -le 0 ]; then
	echo "  --> Header warning : line size : $l" >&2
#	exit 1;
    fi

    echo -n "$open"
    echo -n "$*"

    for i in $(seq 1 $nb); do
	echo -n ' '
    done

    echo "$close"
}

headline

while read -e LINE
do
  line $LINE
done;

line $LINE

headline

#!/bin/sh
#for file in `find . -name "*.c"`
#do
#echo $file
file=$1

#  sed -i 's/INT/int/g' $file
#  sed -i 's/dim_t/int/g' $file
#  sed -i 's/int_t/int/g' $file
#  sed -i 's/flag_t/int/g' $file

  sed -i 's/int\*\* /int \*\*/g' $file
  sed -i 's/int\* /int \*/g' $file
  
  sed -i 's/PRint/PRINT/g' $file
  sed -i 's/COMM_int/COMM_INT/g' $file
  sed -i 's/MPI_int/MPI_INT/g' $file


# INT
  sed -i "s/int \*cja/INT \*cja/g" $file
  sed -i "s/int \*cia/INT \*cia/g" $file
  sed -i "s/int \*rja/INT \*rja/g" $file
  sed -i "s/int \*ria/INT \*ria/g" $file
  sed -i "s/int \*ja/INT \*ja/g" $file
  sed -i "s/int \*ia/INT \*ia/g" $file

  sed -i "s/int\* ja/INT\* ja/g" $file
  sed -i "s/int\* ia/INT\* ia/g" $file

  sed -i "s/int \*Pja/INT \*Pja/g" $file
  sed -i "s/int \*Pia/INT \*Pia/g" $file

# a cause de l'interface de scotch (certains auraient pu etre en dim_t)
  sed -i "s/int \*\*rangtab/dim_t \*\*rangtab/g" $file
  sed -i "s/int \*rangtab/dim_t \*rangtab/g" $file
  sed -i "s/int \*rangetab/dim_t \*rangtab/g" $file
  sed -i "s/int \*treetab/dim_t \*treetab/g" $file
  sed -i "s/int \*nodetab/dim_t \*nodetab/g" $file
  sed -i "s/int \*perm/dim_t \*perm/g" $file
  sed -i "s/int \*iperm/dim_t \*iperm/g" $file


# dim
  sed -i 's/int \*jatab/dim_t \*jatab/g' $file
  sed -i 's/int \*nodelist/dim_t \*nodelist/g' $file
  sed -i 's/int tli/dim_t tli/g' $file
  sed -i 's/int tlj/dim_t tlj/g' $file
  sed -i 's/int bri/dim_t bri/g' $file
  sed -i 's/int brj/dim_t brj/g' $file

  sed -i 's/int cblktlj/dim_t cblktlj/g' $file
  sed -i 's/int \*cblktlj/dim_t \*cblktlj/g' $file
  sed -i 's/int cblkbrj/dim_t cblkbrj/g' $file
  sed -i 's/int \*cblkbrj/dim_t \*cblkbrj/g' $file

  sed -i 's/int cblknbr/dim_t cblknbr/g' $file

  sed -i 's/int dof/dim_t dof/g' $file


# flags
  sed -i 's/int job/flag_t job/g' $file
  sed -i 's/int mode/flag_t mode/g' $file
  sed -i 's/int locally/flag_t locally/g' $file
  sed -i 's/flag_t locally_nbr/int_t locally_nbr/g' $file
  sed -i 's/int level/flag_t level/g' $file
  sed -i 's/flag_t levelnum/int_t levelnum/g' $file
  sed -i 's/int dropE/flag_t dropE/g' $file
  sed -i 's/int fill/flag_t fill/g' $file
  sed -i 's/int filling/flag_t filling/g' $file
  sed -i 's/int diag/flag_t diag/g' $file
  sed -i 's/int facedecal/flag_t facedecal/g' $file
  sed -i 's/int unitdiag/flag_t unitdiag/g' $file
  sed -i 's/int inarow/flag_t inarow/g' $file
  sed -i 's/int schur/flag_t schur/g' $file
  sed -i 's/int schur/flag_t schur/g' $file
  sed -i 's/int symmetric/flag_t symmetric/g' $file
  sed -i 's/int verbose/flag_t verbose/g' $file

 sed -i 's/int DBreceive/flag_t DBreceive/g' $file

# iterations
  sed -i 's/int HIPS_Fgmres/int_t HIPS_Fgmres/g' $file
  sed -i 's/int HIPS_DistrFgmres/int_t HIPS_DistrFgmres/g' $file
  sed -i 's/int fgmres/int_t fgmres/g' $file
  sed -i 's/int HIPS_PCG/int_t HIPS_PCG/g' $file
  sed -i 's/int HIPS_DistrPCG/int_t HIPS_DistrPCG/g' $file
  sed -i 's/int itmax/int_t itmax/g' $file

# mpi
  sed -i 's/int proc_id/mpi_t proc_id/g' $file
  sed -i 's/int tag/mpi_t tag/g' $file

  sed -i 's/int_t \*pset/mpi_t \*pset/g' $file
  sed -i 's/mpi_t \*pset_index/INT \*pset_index/g' $file

# blas
  sed -i 's/int stride/blas_t stride/g' $file
  sed -i 's/int incx/blas_t incx/g' $file
  sed -i 's/int incy/blas_t incy/g' $file


#VARIABLE
  sed -i 's/\(int[ ]*\)\([a-z]\([ ]*,[ ]*[a-z]\)*;\)/INT \2/g' $1
#done

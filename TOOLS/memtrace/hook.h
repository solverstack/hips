/* #define LINUX */
#ifndef LINUX

int
is_set_hook(void) { }
   
static void
my_set_hook (void) { }


static void
my_init_hook (void) { }

static void
my_remove_hook (void) { }
    
static void *
my_malloc_hook (size_t size, const void *caller) { }
 
static void *
my_realloc_hook (void *memptr, size_t size, const void *caller) { }

static void
my_free_hook (void *memptr, const void *caller) { }

#else

/* TODO : */
/* en faire un .c */
/* Sauver la routine originale dans my_malloc et my_free utile ?*/
/* hook sur realloc */
/* convention de codage */

/* http://www.gnu.org/software/libc/manual/html_node/Hooks-for-Malloc.html */

/* Prototypes for __malloc_hook, __free_hook */
#include <malloc.h>

/* functions called by hooks */
void* trace_malloc(size_t size);
void* trace_realloc(void* memptr, size_t size);
void  trace_free(void* memptr);
     
/* Prototypes for our hooks.  */
/* static void my_init_hook (void); */
static void *my_malloc_hook (size_t, const void *);
static void *my_realloc_hook (void *, size_t, const void *);
static void my_free_hook (void*, const void *);
     
static void *(*old_malloc_hook) (size_t, const void *);
static void *(*old_realloc_hook) (void*, size_t, const void *);
static void (*old_free_hook) (void*, const void *);

/* /\* Override initializing hook from the C library. *\/ */
/* void (*__malloc_initialize_hook) (void) = my_init_hook; */

int
is_set_hook(void)
{
  return (__malloc_hook == my_malloc_hook);
}
     
static void
my_set_hook (void)
{
  /* set hooks */   /* Restore our own hooks */
  __malloc_hook = my_malloc_hook;
  __realloc_hook = my_realloc_hook;
  __free_hook = my_free_hook;
}

static void
my_init_hook (void)
{
  if (is_set_hook()) return; /* Don't call init 2x */

  /* bkp */
  old_malloc_hook = __malloc_hook;
  old_realloc_hook = __realloc_hook;
  old_free_hook = __free_hook;

  /* set */
  my_set_hook();
}

static void
my_remove_hook (void)
{
  /* Restore all old hooks */
  __malloc_hook = old_malloc_hook;
  __realloc_hook = old_realloc_hook;
  __free_hook = old_free_hook;
}
     
static void *
my_malloc_hook (size_t size, const void *caller)
{
  void *memptr;

  my_remove_hook();

  memptr = trace_malloc(size);

  my_set_hook();

  return memptr;
}
  
static void *
my_realloc_hook (void *memptr, size_t size, const void *caller)
{
  void * newmemptr;
  
  my_remove_hook();
  
  newmemptr = trace_realloc(memptr, size);
  
  my_set_hook();
  
  return  newmemptr;
}

static void
my_free_hook (void *memptr, const void *caller)
{
  my_remove_hook();
  
  trace_free(memptr);

  my_set_hook();
}


#endif

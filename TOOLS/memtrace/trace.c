/* TODO : option : conserver le h courant mais quitter le hook */
/* mettre des limits des tailes pour ce qui est loggé (>ko)*/
/* alléger les fichiers de traces en supprimant les points non importants */

#include <stdio.h>  /* printf */
#include<stdio.h>
#ifdef __STDC__
#include <stdlib.h>
#else
#include <malloc.h>
#endif
/*#include <stdlib.h>*/ /* exit */
/*#include <malloc.h>*/
#include <limits.h>
#include <assert.h>

#include "trace.h"
#include "hook.h"
#include "time.h"

//#define MEM_LIMIT ULONG_MAX  /* used memory < 4 Go  */
//#define MEM_LIMIT 1073741826    /* used memory 1 Go */

#define MAX(x,y) (((x)<(y))?(y):(x))

unsigned long               memalloccurrent = 0;
unsigned long               memallocmax = 0;
long                        memalloclimit = -1;

#ifdef TRACE_OLD
#define DIV 1024
#define DEFAULT_FILE "trace.data"
double tinit;
FILE* fd = NULL;
FILE* fdgp = NULL;
#endif

/*
********************************************************************************
*/
void trace_print_size(FILE* fd, unsigned long size) {

  if (size < 1024) {
    fprintf(fd,"%lu o", size);
  } else {
    if (size < 1024*1024) {
      fprintf(fd,"%0.2f Ko", (float)size/1024);
    } else {
      if (size < 1024*1024*1024) {
	fprintf(fd,"%0.2f Mo", (float)size/(1024*1024));
      } else {
	fprintf(fd,"%0.3f Go", (float)size/(1024*1024*1024));
      }
    }
  }
}

void trace_get_info(FILE * fd) {
  fprintf(fd,"MEMTRACE : current = ");
  trace_print_size(fd, trace_get_current());
  fprintf(fd,", max = ");
  trace_print_size(fd, trace_get_max());
  fprintf(fd,"\n");
}

/*
********************************************************************************
*/

unsigned long trace_get_current() {
  return memalloccurrent;
}

unsigned long trace_get_max() {
  return memallocmax;
}

void trace_set_limit(long maxMo) {
  memalloclimit = maxMo * 1024 * 1024;
}


/*
********************************************************************************
*/

/*** TODO : a définir comme des callbacks */
/* Spec : protégé des malloc / free */

void trace_check_limit(int size) {
  if (memalloclimit < 0) return;

  if (memalloclimit/2 <= memalloccurrent/2 + size/2) {
    trace_get_info(stderr);

    fprintf(stderr,"MEMTRACE : Need + ");
    trace_print_size(stderr, size);
    fprintf(stderr,"\n");

    fprintf(stderr,"MEMTRACE : Memory Limit is ");
    trace_print_size(stderr, memalloclimit);
    fprintf(stderr,"\n");

    /* TODO : fd close ... */
    exit(-1);
  }
}


/*
********************************************************************************
*/

void* trace_malloc(size_t size) {
  double* memptr;
  
  trace_check_limit(size);

  memptr = (void*)malloc (size + sizeof(double)); /* Add a double containing the size of the array */
  if (memptr != NULL) {
    memptr[0] = (double)size;
    memalloccurrent += (unsigned long) size;
    memallocmax = MAX(memallocmax, memalloccurrent);
    memptr++;
  } 
  else {
    fprintf (stderr, "memtrace : malloc\n");
    /* fprintf (stderr, "memtrace : ECHEC Occupe : %lu Demande : %lu\n", memalloccurrent, (unsigned long)size); */
  }

#ifdef TRACE_OLD
  if (fd != NULL)
    fprintf(fd, "%0.6f\t%lu\n",dwalltime2() - tinit, memalloccurrent/DIV);
#endif

  return ((void*)memptr);
}

void* trace_realloc(void* memptr, size_t size) {
  double* newmemptr = (double*) memptr;

  /* important */
  if (memptr == NULL)
    return trace_malloc(size);

  newmemptr--;
  memalloccurrent -= (unsigned long) newmemptr [0] ;

  /* here ! */
  trace_check_limit(size);

  newmemptr = realloc (newmemptr, size + sizeof (double));
  if (newmemptr != NULL) {
    newmemptr[0] = (double)size;
    memalloccurrent += (unsigned long) size;
    memallocmax = MAX(memallocmax, memalloccurrent);
    newmemptr++;
  } else {
    fprintf (stderr, "memtrace : realloc\n");
  }

  return newmemptr;
}

void trace_free(void* memptr) {
  double* newmemptr = (double*)memptr;

  newmemptr--;
  memalloccurrent -= (unsigned long)newmemptr[0];
  free (newmemptr);

#ifdef TRACE_OLD
  if (fd != NULL)
    fprintf(fd, "%0.6f\t%lu\n",dwalltime2() - tinit, memalloccurrent/DIV);
#endif
}


/*
********************************************************************************
*/


/***/
/* mode non protégé */

void trace_init() {
  if (!is_set_hook()) {
/*     char* filename; */
/*     if ((filename=getenv("MEMTRACE_FILE")) == NULL) filename = DEFAULT_FILE; */

/*     fd = fopen(filename, "w"); */
/*     fprintf(fd, "#Time  |  RAM (Mo)\n"); */

/*     /\*TODO*\/ */
/*     fdgp = fopen("gnuplot.gp", "w"); */
/*     fprintf(fdgp, "set autoscale\nset ytic auto\nset title \"Memoire\"\nset xlabel \"Temps\"\nset ylabel \"Memoire (Ko)\"\nset grid\n\nset xtics ("); */
#ifdef TRACE_OLD
    tinit = dwalltime2();
#endif
    /* à la fin */
    my_init_hook();
  }
}

#ifdef TRACE_OLD
void trace_log(char* ref, char* str) {
  int is_set;
  if ((is_set = is_set_hook())) 
    my_remove_hook();  
  
  double t = dwalltime2() - tinit;
  printf("MTRACE : log %s\t%s ... %0.4f s\n", ref, str, t);
  if (fdgp != NULL)
    fprintf(fdgp, "\"%s\" %0.6f,",ref, t);

  if (is_set)  
    my_set_hook();
}
#endif


void trace_exit() {
  if (is_set_hook()) {                           /* pas nécessaire */
    /* au début */
    my_remove_hook();
#ifdef TRACE_OLD
    if (fd != NULL) {
      fclose(fd);
      fd = NULL;
    }

    if (fdgp != NULL) {
      char* filename;
      if ((filename=getenv("MEMTRACE_FILE")) == NULL) filename = DEFAULT_FILE; /* TODO : peut changer */
      fprintf(fdgp, "%0.6f)\n\nplot '%s' using 1:2 title 'RAM' with lines\n\npause -1\n", dwalltime2()-tinit, filename);
      fclose(fdgp);
    }
#endif
  }
}

#ifdef TRACE_OLD
void trace_pause() {
    my_remove_hook();  
}

void trace_unpause() {
    my_init_hook();  
}

#endif



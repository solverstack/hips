#include <stdio.h>
#include "malloc.h" 
#include "trace.h"

int main() {
  trace_init();

  trace_set_limit(5*1024);

  while(1)
    malloc(1024);

  trace_exit();

  return 0;
}

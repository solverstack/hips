void trace_init();
void trace_exit();

void trace_set_limit(long maxMo);

void trace_get_info(FILE* fd);
unsigned long trace_get_current();
unsigned long trace_get_max();





#ifdef TRACE_OLD
void trace_log(char* ref, char* str);
void trace_pause();
void trace_unpause();
#endif

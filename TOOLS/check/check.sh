user=gaidamou

if [ $# -ne 1 ]
then
    echo "usage : $0 <revision>"
    exit 1
fi

version=$1

echo "Get revision $version ..."
if [ -e hips-r$version ]
then
    echo "... revision $version already exist locally"
else
    mkdir tmp 2>/dev/null
    cd tmp
    svn checkout -r $version svn+ssh://$user@scm.gforge.inria.fr/svn/hips >/dev/null
    mv hips/trunk ../hips-r$version
    rm -rf hips
    cd ..
    rmdir tmp
fi

# TODO : if makefile.inc == hips-r$version/makefile.inc then pas besoin compil
cp makefile.inc hips-r$version/
cp Inputs       hips-r$version/TESTS/PARALLEL

cd hips-r$version

echo "Compilation ..."
make clean >/dev/null; make -j3 >/dev/null

echo "Run ..."
cd TESTS/PARALLEL/
make -j3 >/dev/null
mpirun -n 1 ./testHIPS-RUN.ex 100
cd ../..

cd ..
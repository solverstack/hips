src=../../../hips_user
out=../../htdocs/doc

mkdir tmp

for file in install overview dir test faq
do
    cat latex-head.tex  >> tmp/$file.tex
    cat $src/$file.tex  >> tmp/$file.tex
    cat latex-foot.tex  >> tmp/$file.tex
    
    latex2html -split 0 -nonavigation -noinfo -noaddress -dir tmp/ tmp/$file.tex 
    
    sed '1,23d' tmp/$file.html | sed 'N;$!P;$!D;$d'| sed 'N;$!P;$!D;$d' > tmp/$file-trunc.html
    
# TODO : H4
    sed -i 's/<H2>/<H3>/' tmp/$file-trunc.html
    sed -i 's/<\/H2>/<\/H3>/' tmp/$file-trunc.html
    
    sed -i 's/<H1>/<H3>/' tmp/$file-trunc.html
    sed -i 's/<\/H1>/<\/H3>/' tmp/$file-trunc.html
    
    sed -n '1,/_INCLUDE_/p' $file.tmpl | grep -v "_INCLUDE_" > $out/$file.html
    cat tmp/$file-trunc.html >> $out/$file.html
    sed -n '/_INCLUDE_/,$p' $file.tmpl | grep -v "_INCLUDE_" >> $out/$file.html
done

mv $out/overview.html $out/../
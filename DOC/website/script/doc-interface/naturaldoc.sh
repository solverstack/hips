src=../../../../../SRC/INCLUDE
out=../../htdocs/doc

tmp1=in
tmp2=out

mkdir $tmp1
(cd $tmp1; ln -s $src/hips.h .)

mkdir $tmp2

~/opt/NaturalDocs-1.4/NaturalDocs -r -ro -i $tmp1 -p $tmp1 -o FramedHTML $tmp2/

sed '1,11d' $tmp2/files/hips-h.html |  sed 'N;$!P;$!D;$d' > $tmp2/hips-h-trunc.html

sed -i 's/<h1 class=CTitle><a name="HIPS"><\/a>HIPS<\/h1>//' $tmp2/hips-h-trunc.html

sed -n '1,/_INCLUDE_/p' interface.tmpl | grep -v "_INCLUDE_" >> $out/interface.html
cat $tmp2/hips-h-trunc.html >> $out/interface.html
sed -n '/_INCLUDE_/,$p' interface.tmpl | grep -v "_INCLUDE_" >> $out/interface.html

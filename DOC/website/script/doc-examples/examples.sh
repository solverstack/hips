src=../../../../TESTS/PARALLEL
out=../../htdocs/doc

src_c="testHIPS1.c testHIPS2.c testHIPS3.c"
src_f="testHIPS1-Fortran.f90 testHIPS2-Fortran.f90 testHIPS3-Fortran.f90"



mkdir tmp

## C
for file in $src_c
do
    source-highlight -s c       -i $src/$file -o tmp/$file.html -f xhtml
done

## Fortran
(cd ../../../../SRC/INCLUDE ; make hips.inc)
(cd $src ; make $src_f)

for file in $src_f
do
    # supprimer les lignes vides répétées + lignes du préprocesseur (#)
    (cd $src
	grep -v "^#" $file | cat -s > tmp.f
	mv tmp.f $file
	)
    
    source-highlight -s fortran -i $src/$file -o tmp/$file.html -f xhtml

    rm $src/$file

done

##

for file in $src_c $src_f
do

    cp $src/$file $out

    sed -n '1,/_INCLUDE_/p' examples.tmpl | grep -v "_INCLUDE_" >> $out/"$file".html
    echo "<pre><tt>" >> $out/"$file".html
    sed '1,5d' tmp/$file.html >> $out/"$file".html
    sed -n '/_INCLUDE_/,$p' examples.tmpl | grep -v "_INCLUDE_" >> $out/"$file".html

    sed -i "s/_FILE_/$file/g" $out/"$file".html
done

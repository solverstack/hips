In \texttt{TESTS/PARALLEL} you should find an executable \texttt{testHIPS.ex}.
The path of the matrix you want to try as well as some parameters must be set 
in the file \texttt{Inputs} which is in the same directory.
Once you have set the parameters you want to test in \texttt{Inputs}; you can 
run HIPS like in this example (mpich with 16 processors) :
\verb+ mpirun -np 16 ./testHIPS.ex <domsize>+
where \verb+<domsize>+ is an integer that indicates which size of interior domain you would like to use.
The \verb+<domsize>+ parameter should be small enough to create at least one domain per processor,
otherwise the program will end with an error message.
This parameter is optional in the case you choose the ``ITERATIVE'' (full iterative) strategy in the Inputs parameter file (see next subsection for details).

\subsection{Input parameters of the test program}
The test program testHIPS.ex read the input parameters in the file ``Inputs''.
Here is a description of the Inputs file : 

Example of an ``Inputs'' file for the hybrid method :\\

{\footnotesize
\begin{verbatim}
../MATRICES/bcsstk16.rsa   #0#  Matrix name and driver
2                          #1#  0=unsymmetric pattern, 1=symmetric pattern, 2=symmetric matrix in RSA
0                          #2#  RHS file name (0=make a rhs (sol = [1]))
HYBRID                     #3#  Method (HYBRID, ITERATIVE)
1e-7                       #4#  Relative residual norm
ALL                        #5#  Fill-in : Strictly=0, Locally=ALL
100                        #6#  GMRES maximum iteration
100                        #7#  GMRES restart
0.00                       #8#  Numerical threshold in ILUT for interior domain
0.001                      #9#  Numerical threshold in ILUT for the interface
0.001                      #10# Numerical threshold for coupling between the interior level and Schur
5                          #11# Verbose level [0-4] (the higher the more it prints informations)
\end{verbatim}
}

Important note for ``HYBRID'' method :
\begin{itemize}
  \item you must set the parameter \#8\# to 0 (it means that an exact factorization is used for the interior domain 
matrix);
  \item the domsize parameter of ``testHIPS.ex'' is a domain average size ; it should be sufficiently small to ensure that at least one per processor can be created (the program will stop if it is not the case).
\end{itemize}
   

Example of an ``Inputs'' file for the full ITERATIVE method :\\
{\footnotesize
\begin{verbatim}
../MATRICES/bcsstk16.rsa   #0#  Matrix name and driver
2                          #1#  0=unsymmetric pattern, 1=symmetric pattern, 2=symmetric matrix in RSA
0                          #2#  RHS file name (0=make a rhs (sol = [1]))
ITERATIVE                  #3#  Method (HYBRID, ITERATIVE)
1e-7                       #4#  Relative residual norm
ALL                        #5#  Fill-in : Strictly=0, Locally=ALL
100                        #6#  GMRES maximum iteration
100                        #7#  GMRES restart
0.001                      #8#  Numerical threshold in ILUT for interior domain
0.001                      #9#  Numerical threshold in ILUT for the interface
0.001                      #10# Numerical threshold for coupling between the interior level and Schur
5                          #11# Verbose level [0-4] (the higher the more it prints informations)
\end{verbatim}
}
Important note for ``ITERATIVE'' method :
\begin{itemize}
  \item it is better to set the parameter \#8\# a threshold value greater than 0 ;
  \item in the case of the full ``ITERATIVE'' strategy when no argument is given to testHIPS.ex,  the parallelization scheme use one domain per processor. 
\verb+ mpirun -np 16 ./testHIPS.ex +
\end{itemize}

Example to read an unsymmetric matrix in Harwell boeing format (RUA) : 
If you are sure that the matrix non zero pattern is symmetric you can set 
the parameter \#1\# to 1. If it is not the case (or you are not sure) set this parameter to 0 : 
it will symmetrize the matrix non-zero pattern.
{\footnotesize
\begin{verbatim}
../MATRICES/orsirr_1.rua   #0#  Matrix name and driver
0                          #1#  0=unsymmetric pattern, 1=symmetric pattern, 2=symmetric matrix in RSA
\end{verbatim}
} 
 
Example to read a symmetric complex matrix in matrix market format (require to compile the complex version of HIPS) :
{\footnotesize
\begin{verbatim}
3../MATRICES/young4c_.mtx   #0#  Matrix name and driver
2                          #1#  0=unsymmetric pattern, 1=symmetric pattern, 2=symmetric matrix in RSA
\end{verbatim}
}


Example to use a right hand side member :
you must write in a file the values of the right hand side in {\bf natural order} that is to say 
the original numbering of the matrix in inputs ; the $i^{th}$  line of the file contains only one entry that is the value 
of the $i^{th}$ component of the rhs. 
{\footnotesize
\begin{verbatim}
./rhs                      #2#  RHS file name (0=make a rhs (sol = [1]))
\end{verbatim}
}

%% \section{HIPS methods and parameters}
%% HIPS repose sur une approche de type complément de Schur. Commençons par expliquer le principe 
%% générale de cette approche.
%% Si l'on considère la matrice $A$ d'un système linéaire et que l'on 
%% partitionne l'ensemble des inconnues en deux groupes $B$ et $C$
%% alors la factorisation de $A$ peut s'écrire de la manière suivante :
%% \[ 
%% \label{decomp1}
%% \left(
%% \begin{array}{cc}
%% A_B & F    \\
%% E   & A_C \\
%% \end{array}
%% \right) 
%% %\approx 
%% =
%% \left(
%% \begin{array}{cc}
%% {L_B} &  \\
%% E {U_B} \inv & S \\
%% %E U \inv & {L}_S \\
%% \end{array}
%% \right) 
%% \times
%% \left(
%% \begin{array}{cc}
%% {U_B}  & {L_B} \inv F   \\
%%    & I  \\
%% %   & U_S  \\
%% \end{array}
%% \right), 
%% \]
%% o\`u $L_B.U_B$ une factorisation exacte de $A_B$ et  $S$ est le ``complément de Schur'' :
%% $S = A_C - E.U_B \inv.L_B \inv.F$.

%% Résoudre le système $A.x = r$ est équivalent \`a résoudre successivement les trois équations~:
%% \begin{eqnarray}
%% %\begin{equation}
%% %\begin{cases}           
%% % \begin{array}{cc}       
%% & & A_B.z_B = r_B  \\ 
%% & & S.x_C = r_C-E.z_B  \\
%% & & A_B.x_B = r_B - F.x_C 
%% %\end{cases}
%% % \end{array}
%% \end{eqnarray}%  
%% %\end{equation}%  
%% o\`u $x_B$, $x_C$, $r_B$ et $r_C$ sont les parties de $x$ et $r$ 
%% qui correspondent \`a $B$ et $C$. $z_B$ est un vecteur temporaire.

%% On peut donc résoudre le système $A$ en trois étapes :
%% \begin{enumerate}
%% \item ~$z_B =U_B^{-1}.(L_B^{-1}.r_B)$~;
%% \item ~obtenir $x_C$ par une méthode itérative~;
%% \item ~$x_B = U_B^{-1}.(L_B^{-1}.(r_B - F.x_C))$.
%% \end{enumerate}


%% %An important remark is that the iteration are only performed on the Schur complement system which is smaller (and better conditioned) 
%% %than the global system. 
%% Dans la méthode hybride \`a laquelle nous nous intéressons plus
%% particulièrement dans ce rapport, 
%% les équations (1) et (3) sont résolues par méthode directe
%% et l'équation (2) est résolue par une méthode itérative.
%% Pour résoudre le système (2) nous utiliserons un GMRES préconditionné par
%% une factorisation incomplète de  $S \approx \tilde{L_S}.\tilde{U_S}$. \\
%% L'originalité de HIPS repose d'une part sur un type de factorisation incomplète du complément de Schur qui utilise  des techniques de renumérotations et de partitionnement spéciale~\cite{LaBRI:sisc06} et d'autre part sur le couplage fin d'algorithmes de factorisation par blocs et de factorisation ILUT~\cite{C:LaBRI::CSE08b}.
%% Les techniques de factorisation incomplète dans HIPS repose sur une
%% décomposition en domaine du graphe d'adjacence de la matrice en
%% sous-domaines ainsi que sur la décomposition de l'interface entre ces sous-domaines en composantes 
%% appelées ``connecteurs''. Nous noterons dans la suite cette décomposition par HID ``Hierarchical Interface Decomposition''. L'HID peut \^etre vue comme une généralisation aux graphes irréguliers de la notion de {\em faces} et d'{\em ar\^etes} comme sur la figure~\ref{HID}. Sur cette figure, l'HID est très simple dans le cas d'une grille 
%% mais les algorithmes utilisés sont génériques et s'appliquent aux graphes quelconques ; 
%% le but des heuristiques étant de minimiser le nombre de niveaux de connecteurs et le nombre de sommets dans les plus hauts niveaux.

%% \begin{figure}[h!]
%%   \caption{Illustration de la numérotation obtenue par HID dans le cas simple d'une grille.}
%%   \label{HID}    
  
%%    \begin{minipage}[t]{0.5\linewidth}
%%       \centering  \includegraphics[width=0.9\linewidth]{grid5.eps}
      
%%       \centering  {\it Grid $8 \times 8$}. 
%%    \end{minipage}\hfill
%%    \begin{minipage}[t]{0.5\linewidth}   
%%       \centering \includegraphics[width=0.9\linewidth]{hid64.eps}
      
%%       \centering {\it La matrice réordonnancée}.
%%    \end{minipage}
%% \end{figure}

 
%% \subsection{Parallélisation}
%% \label{section:parallel} 

%% D'habitude dans les méthodes de décomposition de domaine, on associe un domaine par processeur.
%% Pour notre approche, le nombre de sous-domaines utilisé pour la construction du préconditionneur 
%% est généralement beaucoup plus important que le nombre de processeurs. En effet, nous utilisons 
%% une décomposition du graphe d'adjacence de la matrice du système en petits sous-domaines (la taille 
%% typique d'un sous-domaine est de l'ordre de quelques milliers d'inconnues) et nous assignons plusieurs 
%% sous-domaines sur chaque processeur. La principale raison est que la méthode de préconditionnement 
%% du complément de Schur est très robuste et se dégrade lentement avec le nombre de domaine ;
%% nous pouvons donc obtenir un meilleur compromis entre mémoire et convergence en utilisant plusieurs 
%% domaines par processeur. En effet, utiliser de grands domaines (en nombre d'inconnues) 
%% diminue le nombre d'inconnues dans l'interface (dimension du complément de Schur) et permet ainsi de résoudre 
%% plus rapidement le complément de Schur par méthode itérative mais ceci nécessite aussi beaucoup plus de mémoire
%% et de temps pour calculer la factorisation directe de $A_B$.

%% La distribution des sous-domaines est faite en utilisant un partitionneur de graphe (par exemple \scotch)
%% sur le graphe quotient $Q$ induit par la décomposition hiérarchique de l'interface.
%% $Q(V, E)$ est défini de la manière suivante~: 
%% \begin{itemize}
%% \item chaque sous-domaine est représenté par un sommet dans $V$ qui est pondéré par le nombre de non-zéros
%% que la factorisation directe produira (ceci est connu gr\^ace \`a la factorisation symbolique)~;
%% \item on défini une ar\^ete $(i,j) \in E$ si il y a un connecteur de l'interface  commun aux sous-domaines $i$ et $j$. 
%% Cette ar\^ete est pondérée par le nombre d'inconnues communes \`a $i$ et $j$.
%% \end{itemize}
%% Pour obtenir la partition du graphe pondéré $Q$ entre les $P$ processeurs, 
%% le logiciel \scotch\,est utilisé. De part la construction m\^eme du graphe $Q$, la distribution des  domaines entre les processeurs obtenue par cette méthode {équilibre naturellement la mémoire entre les processeurs.

%% \begin{figure}
%% \caption{Graphe quotient pondéré de la partition des sous-domaines}
%% \label{balancing}
%% \begin{center}
%% \includegraphics[width=0.8\linewidth]{parallelization2.eps}
%% \end{center}
%% \end{figure}


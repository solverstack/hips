mkdir tmp 2> /dev/null
in=../../../SRC/INCLUDE/hips.h
out=tmp/hips.h

egrep -v "Function:" $in > $out

# /**
sed -i 's/\/\*/\/\*\*/g' $out

# CODE
sed -i 's/^  >/   /g' $out
sed -i 's/\(Fortran interface:\)/\1\n\\code/g' $out
sed -i 's/\(.*END SUBROUTINE.*\)/\1\n\\endcode/g' $out

# \return
sed -i 's/Returns://g' $out
sed -i 's/\(^[ ]*\)\(MURGE_.*-.*\)/\1\\return \2/g' $out

# \parameters
sed -i 's/Parameters://g' $out
sed -i 's/\(^[ ]*\)\([a-zA-Z]* [ ]*-.*\)/\1\\param \2/g' $out

# \group
## répétition de \([A-Za-z]*\)\([ ]*\) avec  \1 \2
sed -i 's/Group:[ ]*\([A-Za-z]*\)\([ ]*\)\([A-Za-z]*\)\([ ]*\)\([A-Za-z]*\)\([ ]*\)\(.*\)/\n@}\n@defgroup group\1\3\5 \1\2\3\4\5\6\7\n@{/g' $out
echo "/** @} */" >> $out

# <> => #
sed -i 's/</#/g' $out
sed -i 's/>//g' $out

#sed -i 's/MURGE/HIPS/g' $out

cd tmp
doxygen ../interface.dox

# normally this files  need not be edited -- architecture-dependent
# make commands are in makefile.inc -- 
# this makefile will make the library -- for testing HIPS -- go to
# ./TESTS/

HIPS_DIR = .
include $(HIPS_DIR)/makefile.co

DIRS    =  ./TESTS ./TESTS/MATRICES ./TESTS/DBMATRIX ./TESTS/SEQUENTIAL ./TESTS/PARALLEL ./LIB ./SRC ./SRC/PHIDAL ./SRC/PHIDAL/COMMON ./SRC/PHIDAL/ORDERING ./SRC/PHIDAL/SEQUENTIAL ./SRC/PHIDAL/PARALLEL ./SRC/HIPS_WRAPPER ./SRC/INCLUDE ./SRC/IO ./SRC/BLOCK ./SRC/BLOCK/SOLVMATRIX ./SRC/BLOCK/DBMATRIX ./SRC/BLOCK/SOLVMATRIX2 ./SRC/BLOCK/PARALLEL ./TESTS/MISC_PARALLEL ./TESTS/DEBUG_PARALLEL \
  ./TESTS/SOLVERMATRIX \
  ./TESTS/GRID \
  ./TESTS/GRID/RESULTS_GRID \
  ./TESTS/ORDERING \
  ./TESTS/ORDERING/extra \
  ./TESTS/DBMATRIX/RESULTS \
  ./TESTS/DBMATRIX/TUNIT \
  ./TESTS/MISC_DEVEL \
  ./TESTS/MISC_DEVEL/extra \
  ./TESTS/MISC_DEVEL/RESULTS \
  ./TESTS/PETSc \
  ./TESTS/COMPRESS \
  ./TESTS/MISC_PARALLEL/RESULTS \
  ./SRC/PHIDAL/DEPRECATED \
  ./SRC/INCLUDE/extra \
  ./SRC/IO/vtk \
  ./TOOLS/memtrace \
  ./SRC/SPKIT \
  ./SRC/BLAS

default: lib

required: makefile.inc

all: lib tests

lib: required
	(cd ./SRC/PHIDAL/COMMON;$(MAKE))
	(cd ./SRC/PHIDAL/ORDERING;$(MAKE))
	(cd ./SRC/PHIDAL/SEQUENTIAL;$(MAKE))
	(cd ./SRC/BLOCK/DBMATRIX;$(MAKE))
	(cd ./SRC/BLOCK/SOLVMATRIX;$(MAKE))
	(cd ./SRC/BLOCK/SOLVMATRIX2;$(MAKE))
	(cd ./SRC/IO;$(MAKE))
	(cd ./SRC/PHIDAL/PARALLEL;$(MAKE))
	(cd ./SRC/BLOCK/PARALLEL;$(MAKE))
	(cd ./SRC/HIPS_WRAPPER;$(MAKE))
	(cd ./SRC/INCLUDE;$(MAKE))
	(cd ./TOOLS/memtrace;$(MAKE))
	(cd ./SRC/SPKIT;$(MAKE))
	(cd ./SRC/BLAS;$(MAKE))

tests:  lib
#	(cd TESTS/SOLVERMATRIX;$(MAKE))
#	(cd TESTS/GRID;$(MAKE))
#	(cd TESTS/ORDERING;$(MAKE))
	(cd TESTS/DBMATRIX;$(MAKE))
#	(cd TESTS/MISC_DEVEL;$(MAKE))
	(cd TESTS/SEQUENTIAL;$(MAKE))
	(cd TESTS/PARALLEL;$(MAKE))
	(cd TESTS/MISC_PARALLEL;$(MAKE))
#	(cd TESTS/DEBUG_PARALLEL;$(MAKE))

.c.o:
	$(CC) $(CFLAGS) $(ARCH) $< -c -o $@

.f.o:
	$(FC) $(FFLAGS) $< -c -o $@

clean: 
	@echo Cleaning directories ...
	@for dir in $(DIRS) ;\
          do \
          (cd $$dir; rm -f *.a *.o *.ex *core* \#* *~ ; make -si clean 2> /dev/null) ;\
          done

cleanall: clean
	(cd TESTS/NONREGRESSION; ./clean.sh)
	(cd DOC/website; $(MAKE) clean)
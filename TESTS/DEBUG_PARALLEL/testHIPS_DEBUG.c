/* @authors J. GAIDAMOUR, P. HENON */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <mpi.h>
#include "math.h"

#include "hips.h"
#include "io.h"

#define BUFLEN 200

#define MASTER_PROC 0

int main(int argc, char *argv[])
{  

  /* to read parameters */
  int  sym_pattern, sym_matrix;
  char matrixfile[BUFLEN];
  char rhsfile   [BUFLEN];
  int method;
  /*  */

  /*  */
  int id, idnbr, i;
  int numflag;
  int *nodelist;
  COEF *x, *rhs;
  int proc_id, n, ln, nnz;
  int *ia ,*ja;
  COEF *a, *rhs;
  int domsize, nproc;
  /*  */

  /** Init MPI environment **/
  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &proc_id);
  MPI_Comm_size(MPI_COMM_WORLD, &nproc);


  /************************************/
  /* Initialize HIPS for one problem  */
  /************************************/
  idnbr = 1; /* total */
  HIPS_Initialize(idnbr);

  id = 0; /** id of the linear system **/


  /******************************************/
  /* Read the parameters from "inputs" file */
  /******************************************/
  {
    int    options_int   [OPTIONS_INT_NBR];
    double options_double[OPTIONS_REAL_NBR];
    
    Read_options(NULL, matrixfile, &sym_pattern, &sym_matrix, rhsfile, &method, options_int, options_double);
    
    /*******************************/
    /* Put some specific options   */  
    /*******************************/
    
    HIPS_SetDefaultOptions(id, method);
    if(method == HIPS_HYBRID)
      {
	if(argc < 2)
	  {
	    fprintf(stderr, "You must use a domsize parameter with HYBRID strategy \n");
	    fprintf(stderr, "testHIPS.ex <domsize> \n");
	    exit(-1);
	  }
      }


    HIPS_SetOptionINT (id, HIPS_SYMMETRIC,      options_int[HIPS_SYMMETRIC]);
    HIPS_SetOptionREAL(id, HIPS_PREC,           options_double[HIPS_PREC]);

    HIPS_SetOptionINT (id, HIPS_LOCALLY,        options_int[HIPS_LOCALLY]);

    HIPS_SetOptionINT (id, HIPS_ITMAX,          options_int[HIPS_ITMAX]);
    HIPS_SetOptionINT (id, HIPS_KRYLOV_RESTART, options_int[HIPS_KRYLOV_RESTART]);

    HIPS_SetOptionREAL(id, HIPS_DROPTOL0,       options_double[HIPS_DROPTOL0]);
    HIPS_SetOptionREAL(id, HIPS_DROPTOL1,       options_double[HIPS_DROPTOL1]);
    HIPS_SetOptionREAL(id, HIPS_DROPTOLE,       options_double[HIPS_DROPTOLE]);
    HIPS_SetOptionINT (id, HIPS_VERBOSE,        options_int[HIPS_VERBOSE]);
  }

  
  /** parameter domsize is an argument of testHIPS.ex **/
  if(argc >= 2)
    {
      domsize = atoi(argv[1]);
      HIPS_SetOptionINT(id, HIPS_PARTITION_TYPE, 0);
      HIPS_SetOptionINT(id, HIPS_DOMSIZE, domsize);
    }
  else
    {
      HIPS_SetOptionINT(id, HIPS_DOMNBR, nproc);
    }

  if(proc_id == 0)
    {
      /**********************************/
      /* Read the matrix from file      */
      /**********************************/
      CSRread(matrixfile, &n, &nnz, &ia, &ja, &a, NULL/* &sym_pattern */, NULL/* &sym_matrix */);
      if (proc_id == 0)
	fprintf(stdout, "n=%d\nnnz=%d\n", n, nnz);
      
      
      /** C : numbering starts from 0 **/
      numflag = 0;
      CSR_Fnum2Cnum(ja, ia, n);
      
#ifdef DEBUG_M
      /** Check the matrix **/
      checkCSR(n, ia, ja, a, numflag);
#endif
      
    }
  /*********************************************/
  /* Compute the partitionning, load balance   */
  /* and preconditionner structure             */
  /* Then it is communicated to all processor  */
  /*  by the master processor                  */
  /*********************************************/
  HIPS_MasterSetup(id, MASTER_PROC, numflag, sym_pattern, n, ia, ja, 1);

  if(MASTER_PROC == proc_id)
    {
      free(ia);
      free(ja);
      free(a);
    }

  /*******************************/
  /* Set the matrix coefficient  */
  /*******************************/
  HIPS_MasterSetMatrixCoef(id, MASTER_PROC, numflag, 0, n, ia, ja, a, sym_matrix);

  /********************************/
  /* Compute HIPS preconditioner  */
  /********************************/
  HIPS_Precond(id);

  /****************/
  /* Set the RHS  */
  /****************/
  if(strcmp(rhsfile, "0") > 0)
    {
      rhs = (COEF *)malloc(sizeof(COEF)*n);
      VECread(rhsfile, n, rhs);
    }
  else
    rhs = NULL;

  /** If rhs = NULL; a rhs corresponding to x = 1 is computed **/
  HIPS_MasterSetRHS(id, MASTER_PROC, rhs);
  

  /******************************************/
  /* Solve the problem and get the solution */             
  /******************************************/ 
  HIPS_MasterSolve(id, MASTER_PROC, x);


  /************************************************/
  /* Free HIPS internal structure for problem id  */
  /************************************************/
  HIPS_Clean(id);


  /**********************************/
  /* Free HIPS internal structures  */
  /**********************************/
  HIPS_Finalize();

  free(x);
  free(rhs);

  /** End MPI **/
  MPI_Finalize();
  
  return 0;
}

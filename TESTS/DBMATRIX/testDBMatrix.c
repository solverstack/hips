/* @authors J. GAIDAMOUR, P. HENON */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

/* #define MEMTRACE */
#ifdef MEMTRACE
#include <trace.h>
#endif

#include "base.h"
#include "localdefs.h"
#include "block.h" /* FLOAT */
#include "db_struct.h"
#include "phidal_common.h"
#include "phidal_sequential.h"
#include "io.h"

#include "base.h"

#include "prec.h"

#define BUFLEN 200


int main(int argc, char *argv[])
{
  /* int UN = 1; */
  int i;
    
  /* declarations related to Harwell-boeing format for reading the HB
     matrix. Second part is related to I/O parameters */
  int job;
  /* working array for reading matrix */
  int *mapp,*mapptr; 
  int ndom;
  int domsize;
  PhidalMatrix *m;
  PhidalHID BL;

  /* MATRIX */
  int n; INTL nnz;
  COEF *a;
  INTL *ia; dim_t *ja;
  int numflag;  /* C or Fortran */
  
  INTL *ig; dim_t *jg;

  /* Symmetrize the matrix */
  INTL *ib; dim_t *jb;
  
  /* 
   * buf is for read from 'inputs' file, 
   * matrix represents matrix name
   */
  int unsym; /* unsym  --  symmetric pattern(0), nonsym pattern(1) */
  char matrix[BUFLEN];
  char sfile_path[BUFLEN];
  int rsa;

  int *perm, *iperm;

#ifdef OLD
  int* dom2cblktab = NULL;
  int* rangtab=NULL;   /* rangetab[i] is the beginning in the new ordering of the ith supernodes */
  int* treetab=NULL;   /* elimination tree */ 
#endif

  chrono_t t1,t2/* ,ttotal */;
  REAL nnzA/* , nnzL */;

  /* Matrices */
  SymbolMatrix* symbmtx;
  csptr mat;

  /* Vectors */
  COEF *x, *b;
  /*   REAL ro; */
  COEF* r;

  PhidalOptions phidaloptions;
  PhidalOptions* option= &phidaloptions; /* verbose printf */

  DBPrec P;

#ifdef DEBUG_M 
  printfd( "*** Warning : DEBUG_M defined\n");
#endif

#ifdef SCOTCH_PART
  printfd( "*** Info : use SCOTCH\n");
#else
  printfd( "*** Info : use METIS\n");
#endif

#ifdef DEBUG_M2 
  printfd( "*** Warning : DEBUG_M2 defined\n");
#endif

#ifdef DEBUG_NOALLOCATION 
  printfd( "*** Warning : DEBUG_NOALLOCATION defined\n");
#endif

#ifdef DEBUG_NOBLAS 
  printfd( "*** Warning : DEBUG_NOBLAS defined\n");
#endif

  if(argc < 2)
    {
      fprintferr(stderr, "Illegal argument : testPHIDAL.ex <domain size (in number of node)> \n");
      exit(-1);
    }

  domsize = atoi(argv[1]);

#ifdef MEMTRACE
  trace_init();
#endif

  PhidalOptions_Init(&phidaloptions);
  
  /** Read parameters from the input file **/
  GENERAL_setpar(NULL, matrix, sfile_path, &unsym, &rsa, &phidaloptions);

  /************************************************************************************************************/
  /****************************************** READ THE MATRIX *************************************************/
  /************************************************************************************************************/
  CSRread(matrix, &n, &nnz, &ia, &ja, &a, NULL);
  assert(phidaloptions.symmetric == rsa);

  /* Translate matrix into C numbering */
  CSR_Fnum2Cnum(ja, ia, n);
  numflag = 0;

  /********************** SUPPRESS THE ZERO IN THE CSR MATRIX *****/
#ifdef SUPPRESS_ZERO
  i = nnz;
  nnz = CSR_SuppressZeros(numflag, n, ia, ja, a);
  fprintfv(5, stdout, "Zeros removed from the matrix = %d \n", i-nnz); 
 
  if ((rsa == 0) && (unsym == 0) && (i-nnz > 0))
    unsym = 1;
#endif
  /*   fprintfv(5, stdout, "NNZ = %ld \n", (long) nnz); */
  /****************************************************************/

  nnzA = nnz; /* apres symetrize matrix = (ia[n]+n)/2.0; */
  fprintfv(5, stdout, "Number of NNZ in A (CSR) = %g \n", nnzA);

  fprintfv(5, stdout, "Needed memory for A : %d Mo (only coeftab storage)\n", (int)((nnzA*sizeof(COEF))/1048576));

  /************************ RSA matrix contains only the lower triangular part of the matrix ***************************/
  if(rsa == 1)
    {
      /** Symmetrize the matrix (in rsa we only have the lower triangular part) ****/
      ib = ia;
      jb = ja;
      b = a;
      job = 2;
      PHIDAL_SymmetrizeMatrix(job, numflag, n, ib, jb, b, &ia, &ja, &a);
      nnz = 2*nnz-n;
      unsym = 0;
      free(ib);
      free(jb);
      free(b);
    }

  /*******************************************************************************************************************/
  /*******************************************************************************************************************/
  /*******************************************************************************************************************/
  fprintfv(5, stdout,"Matrix dimension is %d, Number of nonzeros is "_int_"\n",n,nnz);

  /*****************************************************************************************************************/
  /***********************************Construct the symmetric graph G of the matrix ********************************/
  /*****************************************************************************************************************/
  ig = (INTL *)malloc(sizeof(INTL)*(n+1));
  jg = (dim_t *)malloc(sizeof(dim_t)*nnz);
  memcpy(ig ,ia, sizeof(int)*(n+1));
  memcpy(jg ,ja, sizeof(int)*nnz);
  
  if(unsym == 1) 
    {
      ib = ig;
      jb = jg;
      job = 0;
      PHIDAL_SymmetrizeMatrix(job, numflag, n, ib, jb, NULL, &ig, &jg, NULL);
      free(ib);
      free(jb);
    }

  /*****************************************************************************************************************/
  /*****************************************************************************************************************/

  /************************************************************************************************************/
  /* Compute the reordering to minimize fill-in using METIS-4.0 ***********************************************/
  /************************************************************************************************************/
 
  perm = (int *)malloc(sizeof(int)*n);
  iperm = (int *)malloc(sizeof(int)*n);

  /** Delete the self edge in the graph (METIS_NodeND need that)**/
  PHIDAL_CsrDelDiag(numflag, n, ig, jg);

  /**** Compute the overlapped partition from the matrix reordering   *****/
  t1  = dwalltime(); 
  /*PHIDAL_Perm2OverlappedDomains(ndom, n, ig, jg, &mapptr, &mapp, perm, iperm);*/
  PHIDAL_Perm2SizedDomains(domsize, n, ig, jg, &ndom, &mapptr, &mapp, perm, iperm);
  t2  = dwalltime(); 

  fprintfv(5, stdout, "Compute the grid of subdomain in %g \n", t2-t1);
  fprintfv(5, stdout, "DOMSIZE = %d \n", domsize);
  fprintfv(5, stdout, "Found %d domains \n", ndom);


  {
    /*********************************************************/
    /*    DISPLAY SOME INFO ON THE OVERLAPPED PARTITION      */
    /*********************************************************/         
    
    int maxdom, mindom;
    REAL avgdom;


    /* fprintfv(5, stdout, "PARTITION: \n");
    for(i=0;i<ndom;i++)
    fprintfv(5, stdout, "Domain %d = %d nodes \n", i, mapptr[i+1]-mapptr[i]);*/
    
    maxdom = 0;
    mindom = mapptr[1]-mapptr[0];
    avgdom = 0.0;
    for(i=0;i<ndom;i++)
      {
	avgdom += mapptr[i+1]-mapptr[i];
	if(mapptr[i+1]-mapptr[i] > maxdom)
	  maxdom = mapptr[i+1]-mapptr[i];
	if(mapptr[i+1]-mapptr[i] < mindom)
	  mindom = mapptr[i+1]-mapptr[i];
      }
    avgdom /= ndom;

    fprintfv(5, stdout, "MAX DOMAIN = %d \n", maxdom);
    fprintfv(5, stdout, "MIN DOMAIN = %d \n", mindom);
    fprintfv(5, stdout, "AVG DOMAIN = %g \n", avgdom);
    /*fprintfv(5, stdout, "IMBALANCE = %g \n", ((float)(maxdom-mindom))*100.0/mindom );*/
  }

  /*******************************************************************/
  /*******************************************************************/
  /**                                                               **/
  /** COMPUTE THE HIERARCHICAL DECOMPOSITION AND LOCAL DATA         **/
  /**                                                               **/
  /*******************************************************************/
  /*******************************************************************/
  
  /** Compute the global hierarchical interface decomposition and the associated permutation **/ 
  PhidalHID_Init(&BL);

  t1  = dwalltime(); 
  PHIDAL_HierarchDecomp(phidaloptions.verbose, numflag, n, ig, jg, mapp, mapptr, ndom, &BL, perm, iperm);
  t2  = dwalltime(); 
  fprintfv(5, stdout, " Compute Phidal Hierarchical Decomposition in %g seconds \n\n", t2-t1);


  /*#define OLD*/
#ifdef OLD
  PHIDAL_GetSupernodes(phidaloptions.verbose, numflag, n,  ig,  jg,  
		       &BL, perm, iperm, &dom2cblktab, &rangtab, &treetab);

  cblknbr_l1 = dom2cblktab[ndom];

#ifdef DEBUG_CBLK
    { 
      int i,j;
      for(i=0;i<ndom;i++)
        {
  	fprintfv(5, stderr, "Domain %d cblknbr = %d \n", i, dom2cblktab[i+1]-dom2cblktab[i]);
  	printfv(5, "[%d %d]\n", rangtab[dom2cblktab[i]], rangtab[dom2cblktab[i+1]-1+1]-1) ;
        }
    }

  {
    int icblk, iBL;
    icblk=0;
    for(iBL=0; iBL<=BL.block_levelindex[1]; iBL++) {
      while(rangtab[icblk] < BL.block_index[iBL]) { /* TODO : remplacer test avec dom2cblktab */
	/* 	printfv(5, "(%d %d)\n",rangtab[icblk],rangtab[icblk+1]-1); */
	icblk++;
	/* printfv(5, "%d %d\n",icblk, cblknbr_l1); */
	assert(icblk <= cblknbr_l1);
      }
      
      if (rangtab[icblk] > BL.block_index[iBL]) {
	printfv(5, "\n\nERREUR : COUPE : %d\n",BL.block_index[iBL]);
	assert(0);
      }
      
      if (rangtab[icblk] == BL.block_index[iBL]) {
	/*       	printfv(5, "MATCH : %d\n",BL.block_index[iBL]); */
      }
    }

  }
#endif
  free(dom2cblktab);
#else 
  fprintfv(5, stdout, "Build Symbolic Matrix \n");
  symbmtx = (SymbolMatrix*)malloc(sizeof(SymbolMatrix));
  if(phidaloptions.schur_method != 1)
    HIPS_SymbolMatrix(phidaloptions.verbose, numflag, 0.05, phidaloptions.locally_nbr, n,  ig,  jg, 
		      &BL, symbmtx, perm, iperm);
  else
    HIPS_SymbolMatrix(phidaloptions.verbose, numflag, 0.05, BL.nlevel, n,  ig,  jg, 
		      &BL, symbmtx, perm, iperm);

#endif



  free(mapp);
  free(mapptr);
  free(ig);
  free(jg);
 
  /*
  CSR -> SparRow
  */
  fprintfv(5, stdout, "Permuting the matrix \n");

  mat = (csptr)malloc(sizeof(struct SparRow));
  
  if(initCS(mat, n)) 
    {
      printfv(5, " ERROR SETTING UP bmat IN initCS \n") ;
      exit(0);
    }
  CSRcs(n, a, ja, ia, mat);
  
  /** DO NOT NEED THESE VECTORS ANYMORE **/
  free(a);
  free(ja);
  free(ia);
  
  /*** Permute the matrix according to the phidal ordering ***/
  t1  = dwalltime(); 
  CS_Perm(mat, perm);/* ! */
  t2  = dwalltime(); 
  fprintfv(5, stdout, " Permute the matrix in %g seconds \n", t2-t1);

  
  t1  = dwalltime(); 
  ascend_column_reorder(mat);/* todo : utile ? */
  t2  = dwalltime(); 
  fprintfv(5, stdout, " Ascend_column_reorder in %g seconds \n\n", t2-t1);

  /* fprintfv(5, stdout, "Number of NNZ in A (SparRow) = %ld \n", CSnnz(mat)); */

  /************************************************************************************************************/
  /************************************************************************************************************/
#ifdef OLD
  fprintfv(5, stdout, "Build DB_Prec structure\n"); 
  t1  = dwalltime(); 
  symbmtx = (SymbolMatrix*)malloc(sizeof(SymbolMatrix));

  if(phidaloptions.schur_method != 1)
    PHIDAL_SymbolMatrix(mat, symbmtx, &rangtab, treetab, cblknbr_l1, &BL, phidaloptions.locally_nbr);
  else /* S est conservÃ© et dc calculÃ© de maniÃ¨re exacte : remplissage locally consistant */
    PHIDAL_SymbolMatrix(mat, symbmtx, &rangtab, treetab, cblknbr_l1, &BL, BL.nlevel);

  t2  = dwalltime(); 
  free(treetab);  
  free(rangtab);  /* ds ce cas, pas la peine de s'embeter avec les pointeurs. idem pour cblk */
  /* Comptage du nombre de non zeros dans la symbolMatrix */
  /*nnzL = SymbolMatrix_NNZ(symbmtx);*/
  fprintfv(5, stdout, " Build DB_Prec structure in %g seconds\n", t2-t1); 
#endif
  /************************************************************************************************************/
  fprintfv(5, stdout, "Build PhidalMatrix\n"); 
  t1  = dwalltime(); 
  m  = (PhidalMatrix *)malloc(sizeof(PhidalMatrix));
  PHIDAL_MatrixBuild(mat, m, &BL, &phidaloptions); /* MPROTECT */
  t2  = dwalltime(); 
  cleanCS(mat);
  free(mat);
  assert(m->dim1 == n);

  fprintfv(5, stdout, " Build PhidalMatrix in %g seconds \n\n", t2-t1);
  /* fprintfv(5, stdout, " Number of NNZ in A (Phidal) = %ld \n", PhidalMatrix_NNZ(m)); */

  assert(phidaloptions.forwardlev == 0 || phidaloptions.forwardlev == 1);

  /* **** */
  
  DBPrec_Init(&P);  

  DBMATRIX_Precond(m, &P, symbmtx, &BL, &phidaloptions);
  /*   assert(P.E != NULL); /\* ? *\/ */

/* #define DUMP */
#ifdef DUMP
  MATLAB_dumpDBMatrix(P.nextprec->L, "matlab_ijv");
#endif

  /************************************************************************************************************/
  /* TODO */
/*   nnzL = DBPrec_NNZ(&P); */
/*   fprintfv(5, stdout, " Number of NNZ in Preconditioner = %g\n", nnzL); */
/*   fprintfv(5, stdout, " Fill Ratio of Preconditioner = %g\n\n", nnzL / nnzA); */
/*   DBPrec_Info(&P); */

/*   fprintfv(5, stdout, "Needed memory for preconditioner : %d Mo (only coeftab storage)\n",  */
/* 	  (int)((nnzL*sizeof(COEF))/1048576)); */
  /************************************************************************************************************/


  x = (COEF *)malloc(sizeof(COEF)*n);
  b = (COEF *)malloc(sizeof(COEF)*n); 
  /*#define LOAD_RHS*/
#ifdef LOAD_RHS
  read_rhs("/home/henon/5....Soft_about_work/MATRICES/IFP/sol.val", n, b);
  for(i=0;i<n;i++)
    x[i] = b[iperm[i]];
#else
  for(i=0;i<n;i++) /* todo : memset */
    x[i] = 1.0;
#endif

  PHIDAL_MatVec(m, &BL, x, b);

#ifndef DEBUG_NOALLOCATION
  bzero(x, sizeof(COEF)*n);
#endif

  t1  = dwalltime();
  DBMATRIX_Solve(m, &P, &BL, &phidaloptions, b, x, NULL, NULL);
  t2  = dwalltime(); 
  fprintfv(5, stdout, "\n Solve in %g seconds \n", t2-t1);

  /* for(i = 0; i < P.S->dim1PB_ ICI; i++) { */
  /*     x[i] = 1 - x[i]; */
  /*   } */
  
  /*   ro = DNRM2(P.S->dim1,x,UN); */
  /*   fprintfv(5, stdout, " nrm2(sol-x)) = %e\n\n", ro); */



  /*  PhidalPrec_Clean(&P); */
  
  {
    int UN=1;  
    /*     ro = BLAS_NRM2(P.SL->dim1,x,UN); */
    /*     fprintfv(5, stdout, " nrm2(sol-x)) = %e\n\n", ro); */
    
    /*  PhidalPrec_Clean(&P); */
    
    r = (COEF *)malloc(sizeof(COEF)*m->dim1);
    memcpy(r, b, sizeof(COEF)*m->dim1);
    PHIDAL_MatVecSub(m, &BL, x, r);
    /*matvecz(mat, x, r, r);*/

    fprintfv(5, stdout, "Relative residual norm = %g \n", BLAS_NRM2(m->dim1, r, UN) / BLAS_NRM2(m->dim1, b, UN));
    free(r);
  }
 
  /************************************************************************************************************/
  /* Free Memory                   ****************************************************************************/
  /************************************************************************************************************/

  /* TODO : reste des free a faire */
  free(perm);
  free(iperm); /**/
  free(x);
  free(b);

  PhidalMatrix_Clean(m);
  free(m);
  DBPrec_Clean(&P);
  PhidalHID_Clean(&BL); 
  PhidalOptions_Clean(&phidaloptions);

  fprintfv(5, stdout, "END \n");

#ifdef MEMTRACE
  trace_info();
  trace_exit();
#endif

  return 0;
}




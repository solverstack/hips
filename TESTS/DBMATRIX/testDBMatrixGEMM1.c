/* @authors J. GAIDAMOUR, P. HENON */
/* @release_exclude */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "math.h"

#include "localdefs.h"
#include "block.h" /* FLOAT */
#include "db_struct.h"
#include "phidal_sequential.h"

#include "base.h"

#define BUFLEN 200

extern void find_supernodes(int n, int *ia, int *ja, int *perm, int *iperm, int *snodenbr, int *snodetab, int *treetab);
extern void ascend_column_reorder(csptr mat);
extern void CSR_Fnum2Cnum(int *ja, int *ia, int n);

int main(int argc, char *argv[])
{
  /* int UN = 1; */
  int i;
  int ierr, len;
    
  /* declarations related to Harwell-boeing format for reading the HB
     matrix. Second part is related to I/O parameters */
  char guesol[2], title[72], key[8], type[3];
  int nrhs, nc, n, nnz, tmp0,tmp,tmp2,tmp3,job;
  /* working array for reading matrix */
  REAL *rhstmp;
  int *mapp,*mapptr; 
  int ndom;
  int domsize;
  PhidalMatrix *m;
  PhidalHID BL;

  /* MATRIX */
  REAL *a;
  int *ja, *ia;
  int numflag;  /* C or Fortran */
  
  int *jg, *ig;


  /* Symmetrize the matrix */
  int *ib, *jb;
  
  /* 
   * buf is for read from 'inputs' file, 
   * matrix represents matrix name
   */
  int unsym; /* unsym  --  symmetric pattern(0), nonsym pattern(1) */
  char matrix[BUFLEN];
  char sfile_path[BUFLEN];
  int rsa;
  int load_local_files;  /** If set to 1 then genere all the local data file **/

  char *substr;

  int *perm, *iperm;

  int cblknbr_l1;    /* number of cblk for level 1       */
  int* dom2cblktab = NULL;
  int* rangtab=NULL;   /* rangetab[i] is the beginning in the new ordering of the ith supernodes */
  int* treetab=NULL;   /* elimination tree */ 

  chrono_t t1,t2,ttotal;
  REAL nnzA, nnzL;

  /* Matrices */
  SymbolMatrix* symbmtx;
  csptr mat;

  /* Vectors */
  REAL *x, *b;
  /*   REAL ro; */
  REAL* r;

  PhidalOptions phidaloptions;
  PhidalOptions *option= &phidaloptions;

  int cut, last;
  int levelnum;
  int Mstart, Mend, Sstart, Send;

  DBPrec P;
  PhidalMatrix* PhidalTmp;
  PhidalMatrix* PhidalS; /* TODO*/

  if(argc < 2)
    {
      fprintferr(stderr, "Illegal argument : testPHIDAL.ex <domain size (in number of node)> \n");
      exit(-1);
    }

  domsize = atoi(argv[1]);


  PhidalOptions_Init(&phidaloptions);
  
  /** Read parameters from the input file **/
  GENERAL_setpar(NULL, matrix, sfile_path, &unsym, &rsa, &load_local_files, &phidaloptions);   

  /************************************************************************************************************/
  /****************************************** READ THE MATRIX *************************************************/
  /************************************************************************************************************/
  tmp0 = 0;
  nrhs = 0;
  /* Read matrix; either using user-defined function (routine user_read) or 
     |               SPARSKIT function for reading Harwell-Boeieng matrices
  */
  if (strtol(matrix, &substr,10)){
    /* If the matrix name starts with  integer in the input file, 
       | User-defined matrix input is provided. The filenames for the
       | user-defined input are stored in the file specified
       | following the integer in the variable "matrix". NOTE: there
       | should be no space between integer and the filename substr
       | contains the name of the file with filenames for the matrix
       | input 
    */
    char filename[100];
    strcpy(filename, matrix+1);
    
    len = strlen(filename);
    /*read only the sizes of matrix arrays first == call user_read
      with job=0*/ 
    tmp = 0; 
    a = NULL; ja = NULL; ia = NULL; rhstmp = NULL;
    userread(filename,&len,&tmp,&n,&nnz,a,ja,ia,&nrhs,rhstmp,&ierr);

    /* allocate proper amount of space */
    ia = (int *) malloc((n+1)*sizeof(int));
    ja = (int *) malloc(nnz*sizeof(int));
    a = (REAL *) malloc(nnz*sizeof(REAL));

    assert(ia != NULL);
    assert(ja != NULL);
    assert(a != NULL);
    /* read the (a, ja, ia) values call userread with job=1 */

    tmp = 1; 

    /************************************************/
    /* READ THE MATRIX FROM DISK (IN USER FORMAT)   */
    /************************************************/
    nrhs = 0; /* To prevent from reading rhs */
    userread(substr,&len,&tmp,&n,&nnz,a,ja,ia,&nrhs,rhstmp,&ierr);
    if(ierr != 0) {
      fprintfv(5, stderr, "cannot read matrix\n");
      exit(-1);
    }
    fprintfv(5, stdout, "Read matrix %s\n", substr);
  }
  else {
    /* variable "matrix" stores the name of the file in HB format 
       |
       |   Read a Harwell-Boeing matrix. using wreadmtc c-version of
       |      sparsit routine - call wreadmtc a first time to determine sizes
       |      of arryas. read in values on the second call. 
    */
    len = strlen(matrix);
    a = NULL; ja = NULL; ia = NULL; rhstmp = NULL;
    wreadmtc(&tmp0,&tmp0,&tmp0,matrix,&len,a,ja,ia,rhstmp,&nrhs,
	     guesol,&n,&nc,&nnz,title,key,type,&ierr); 
    
    ia = (int *) malloc((n+1)*sizeof(int));
    ja = (int *) malloc(nnz*sizeof(int));
    a = (REAL *) malloc(nnz*sizeof(REAL));
    
    tmp = 2;
    tmp2 = n;
    tmp3 = nnz;
    /*********************************/
    /* READ THE MATRIX FROM DISK     */
    /*********************************/
    /* Array sizes determined. Now call wreadmtc again for really
       reading */
    nrhs = 0; /** To prevent from reading rhs **/
    wreadmtc(&tmp2,&tmp3,&tmp,matrix,&len,a,ja,ia,rhstmp,&nrhs,
	     guesol,&n,&nc,&nnz,title,key,type,&ierr); 
    
    if(ierr != 0) 
      fprintfv(5, stderr, "cannot read matrix\n");
    
    fprintfv(5, stdout,"READ the matrix %.*s %.*s \n",8,key,3,type);
  
  }
  
  /********************** SUPPRESS THE ZERO IN THE CSR MATRIX *****/
  numflag = 1;
  i = nnz;
  nnz = CSR_SuppressZeros(numflag, n, ia, ja, a);
  fprintfv(5, stdout, "Zeros removed from the matrix = %d \n", i-nnz); 
 
  /*   fprintfv(5, stdout, "NNZ = %ld \n", (long) nnz); */
  /****************************************************************/

  nnzA = nnz; /* apres symetrize matrix = (ia[n]+n)/2.0; */
  fprintfv(5, stdout, "Number of NNZ in A (CSR) = %g \n", nnzA);

  /************************ RSA matrix contains only the lower triangular part of the matrix ***************************/
  if(rsa == 1)
    {
      /** Symmetrize the matrix (in rsa we only have the lower triangular part) ****/
      ib = ia;
      jb = ja;
      b = a;
      numflag = 1;
      job = 2;
      PHIDAL_SymmetrizeMatrix(job, numflag, n, ib, jb, b, &ia, &ja, &a);
      nnz = 2*nnz-n;
      unsym = 0;
      free(ib);
      free(jb);
      free(b);
    }

  /*******************************************************************************************************************/
  /*******************************************************************************************************************/
  /*******************************************************************************************************************/
  fprintfv(5, stdout,"Matrix dimension is %d, Number of nonzeros is %d\n",n,nnz);

  /* Translate matrix into C numbering */
  CSR_Fnum2Cnum(ja, ia, n);
  numflag = 0;

  /*****************************************************************************************************************/
  /***********************************Construct the symmetric graph G of the matrix ********************************/
  /*****************************************************************************************************************/
  ig = (int *)malloc(sizeof(int)*(n+1));
  jg = (int *)malloc(sizeof(int)*nnz);
  memcpy(ig ,ia, sizeof(int)*(n+1));
  memcpy(jg ,ja, sizeof(int)*nnz);
  
  if(unsym == 1) /*TODO utile ?*/
    {
      ib = ig;
      jb = jg;
      /* numflag = 0; */
      job = 0;
      PHIDAL_SymmetrizeMatrix(job, numflag, n, ib, jb, NULL, &ig, &jg, NULL);
      free(ib);
      free(jb);
    }
  /*****************************************************************************************************************/
  /*****************************************************************************************************************/

  /************************************************************************************************************/
  /* Compute the reordering to minimize fill-in using METIS-4.0 ***********************************************/
  /************************************************************************************************************/
 
  perm = (int *)malloc(sizeof(int)*n);
  iperm = (int *)malloc(sizeof(int)*n);

  /** Delete the self edge in the graph (METIS_NodeND need that)**/
  PHIDAL_CsrDelDiag(numflag, n, ig, jg);

  /**** Compute the overlapped partition from the matrix reordering   *****/
  t1  = dwalltime(); 
  /*PHIDAL_Perm2OverlappedDomains(ndom, n, ig, jg, &mapptr, &mapp, perm, iperm);*/
  PHIDAL_Perm2SizedDomains(domsize, n, ig, jg, &ndom, &mapptr, &mapp, perm, iperm);
  t2  = dwalltime(); 

  fprintfv(5, stdout, "Compute the grid of subdomain in %g \n", t2-t1);
  fprintfv(5, stdout, "DOMSIZE = %d \n", domsize);
  fprintfv(5, stdout, "Found %d domains \n", ndom);


  {
    /*********************************************************/
    /*    DISPLAY SOME INFO ON THE OVERLAPPED PARTITION      */
    /*********************************************************/         
    
    int maxdom, mindom;
    REAL avgdom;


    /* fprintfv(5, stdout, "PARTITION: \n");
    for(i=0;i<ndom;i++)
    fprintfv(5, stdout, "Domain %d = %d nodes \n", i, mapptr[i+1]-mapptr[i]);*/
    
    maxdom = 0;
    mindom = mapptr[1]-mapptr[0];
    avgdom = 0.0;
    for(i=0;i<ndom;i++)
      {
	avgdom += mapptr[i+1]-mapptr[i];
	if(mapptr[i+1]-mapptr[i] > maxdom)
	  maxdom = mapptr[i+1]-mapptr[i];
	if(mapptr[i+1]-mapptr[i] < mindom)
	  mindom = mapptr[i+1]-mapptr[i];
      }
    avgdom /= ndom;

    fprintfv(5, stdout, "MAX DOMAIN = %d \n", maxdom);
    fprintfv(5, stdout, "MIN DOMAIN = %d \n", mindom);
    fprintfv(5, stdout, "AVG DOMAIN = %g \n", avgdom);
    /*fprintfv(5, stdout, "IMBALANCE = %g \n", ((float)(maxdom-mindom))*100.0/mindom );*/
  }

  numflag = 0;

    
  /*******************************************************************/
  /*******************************************************************/
  /**                                                               **/
  /** COMPUTE THE HIERARCHICAL DECOMPOSITION AND LOCAL DATA         **/
  /**                                                               **/
  /*******************************************************************/
  /*******************************************************************/
  
  /** Compute the global hierarchical interface decomposition and the associated permutation **/ 
  PhidalHID_Init(&BL);

  t1  = dwalltime(); 
  PHIDAL_HierarchDecomp(phidaloptions.verbose, numflag, n, ig, jg, mapp, mapptr, ndom, &BL, perm, iperm);
  t2  = dwalltime(); 
  fprintfv(5, stdout, " Compute Phidal Hierarchical Decomposition in %g seconds \n\n", t2-t1);

  PHIDAL_GetSupernodes(phidaloptions.verbose, numflag, n,  ig,  jg,  
		       &BL, perm, iperm, &dom2cblktab, &rangtab, &treetab);

  cblknbr_l1 = dom2cblktab[ndom];

#ifdef DEBUG_CBLK
    { int i,j;
      for(i=0;i<ndom;i++)
        {
  	fprintfv(5, stderr, "Domain %d cblknbr = %d \n", i, dom2cblktab[i+1]-dom2cblktab[i]);
  	printfv(5, "[%d %d]\n", rangtab[dom2cblktab[i]], rangtab[dom2cblktab[i+1]-1+1]-1) ;
        }
    }

  {
    int icblk, iBL;
    icblk=0;
    for(iBL=0; iBL<=BL.block_levelindex[1]; iBL++) {
      while(rangtab[icblk] < BL.block_index[iBL]) { /* TODO : remplacer test avec dom2cblktab */
	/* 	printfv(5, "(%d %d)\n",rangtab[icblk],rangtab[icblk+1]-1); */
	icblk++;
	/* printfv(5, "%d %d\n",icblk, cblknbr_l1); */
	assert(icblk <= cblknbr_l1);
      }
      
      if (rangtab[icblk] > BL.block_index[iBL]) {
	printfv(5, "\n\nERREUR : COUPE : %d\n",BL.block_index[iBL]);
	assert(0);
      }
      
      if (rangtab[icblk] == BL.block_index[iBL]) {
	/*       	printfv(5, "MATCH : %d\n",BL.block_index[iBL]); */
      }
    }

  }
#endif

  free(dom2cblktab);
  
  free(mapp);
  free(mapptr);
  free(ig);
  free(jg);
 
  /*
  CSR -> SparRow
  */
  fprintfv(5, stdout, "Permuting the matrix \n");

  mat = (csptr)malloc(sizeof(struct SparRow));
  CSRcs(n, a, ja, ia, mat);
  
  /** DO NOT NEED THESE VECTORS ANYMORE **/
  free(a);
  free(ja);
  free(ia);
  
  /*** Permute the matrix according to the phidal ordering ***/
  t1  = dwalltime(); 
  CS_Perm(mat, perm);/* ! */
  t2  = dwalltime(); 
  fprintfv(5, stdout, " Permute the matrix in %g seconds \n", t2-t1);
  free(perm);
  free(iperm); /**/
  
  t1  = dwalltime(); 
  ascend_column_reorder(mat);/* todo : utile ? */
  t2  = dwalltime(); 
  fprintfv(5, stdout, " Ascend_column_reorder in %g seconds \n\n", t2-t1);

  /* fprintfv(5, stdout, "Number of NNZ in A (SparRow) = %ld \n", CSnnz(mat)); */

  /************************************************************************************************************/
  /************************************************************************************************************/
  fprintfv(5, stdout, "Build DB_Prec structure\n"); 
  t1  = dwalltime(); 
  symbmtx = (SymbolMatrix*)malloc(sizeof(SymbolMatrix));
  PHIDAL_SymbolMatrix(mat, symbmtx, &rangtab, treetab, cblknbr_l1, &BL, &phidaloptions);
  t2  = dwalltime(); 
  free(treetab);  
  free(rangtab);  /* ds ce cas, pas la peine de s'embeter avec les pointeurs. idem pour cblk */

  /* Comptage du nombre de non zeros dans la symbolMatrix */
  nnzL = SymbolMatrix_NNZ(symbmtx);
  
  fprintfv(5, stdout, " Build DB_Prec structure in %g seconds\n", t2-t1); 
  fprintfv(5, stdout, " Number of NNZ in Preconditioner = %g\n", nnzL);
  fprintfv(5, stdout, " Fill Ratio of Preconditioner = %g\n\n", nnzL / nnzA);
  /************************************************************************************************************/

  fprintfv(5, stdout, "Build PhidalMatrix\n"); 
  t1  = dwalltime(); 
  m  = (PhidalMatrix *)malloc(sizeof(PhidalMatrix));
  PHIDAL_MatrixBuild(mat, m, &BL, &phidaloptions); /* MPROTECT */
  t2  = dwalltime(); 
  cleanCS(mat);
  free(mat);

  fprintfv(5, stdout, " Build PhidalMatrix in %g seconds \n\n", t2-t1);
  /* fprintfv(5, stdout, " Number of NNZ in A (Phidal) = %ld \n", PhidalMatrix_NNZ(m)); */

  assert(phidaloptions.forwardlev == 0 || phidaloptions.forwardlev == 1);

  /* **** */
  levelnum = 0;
  
  DBPrec_Init(&P);  
  P.symmetric = 1;
  P.dim = n;
  P.forwardlev = phidaloptions.forwardlev;
  P.levelnum = levelnum; 
  P.schur_method = phidaloptions.schur_method;
  
  P.L = (DBMatrix*)malloc(sizeof(DBMatrix));
  /* **** */

  /************************************************************************************************************/
  /* Cut Test                      ****************************************************************************/
  /************************************************************************************************************/
  
  fprintfv(5, stdout, "Build DBPrec\n");
  t1  = dwalltime(); 
  if (phidaloptions.forwardlev == 1) {
    if (BL.nlevel == 1) {
      printfv(5, " ERROR : domain size to big : there is only one level\n");
      printfv(5, "         number of forward recursion must be set to 0\n");
      exit(1);
    }

    cut = BL.block_index[BL.block_levelindex[1]]-1; /* todo : faire comme Sstart ... etc pour les noms */
    last = symbmtx->nodenbr -1; 
    
    assert(last == BL.block_index[BL.nblock]-1);
    
    assert(cblknbr_l1 + (BL.nblock - BL.block_levelindex[1]) == symbmtx->cblknbr);
    assert(last == symbmtx->cblktab[symbmtx->cblknbr-1].lcolnum);
    
    /* printfv(5, "cut=%d, last=%d \n", cut, last); */
    assert(cut != last); /* sinon, pas de level2, redondant ave BL.nlevel==1 */
    
    P.E_DB = (DBMatrix*)malloc(sizeof(DBMatrix));
    P.S = (DBMatrix*)malloc(sizeof(DBMatrix));
    
    SymbolMatrix_Cut2(symbmtx, &P.L->solvmtx.symbmtx,        0,     0,  cut, cut,  0);
    SymbolMatrix_Cut2(symbmtx, &P.E_DB->solvmtx.symbmtx, cut+1,     0, last, cut,  cut+1); /* TODO : un arg en trop? */
    SymbolMatrix_Cut2(symbmtx, &P.S->solvmtx.symbmtx,    cut+1, cut+1, last, last, cut+1);
    
    /*   printfv(5, "%d = %d (%d %d %d)\n",symbmtx->bloknbr, P.L->solvmtx.symbmtx.bloknbr + P.E_DB->solvmtx.symbmtx.bloknbr + P.S->solvmtx.symbmtx.bloknbr, */
    /* 	 P.L->solvmtx.symbmtx.bloknbr, P.E_DB->solvmtx.symbmtx.bloknbr, P.S->solvmtx.symbmtx.bloknbr); */
    /*   assert(symbmtx->bloknbr == P.L->solvmtx.symbmtx.bloknbr + P.E_DB->solvmtx.symbmtx.bloknbr + P.S->solvmtx.symbmtx.bloknbr); */
    freeSymbolMatrix(symbmtx);
    
    Mstart   =  BL.block_levelindex[levelnum];
    Mend     =  BL.block_levelindex[levelnum+1]-1;
    Sstart   =  BL.block_levelindex[levelnum+1];
    Send     =  BL.nblock-1;  
    
    printfv(5, "---------------------------------- M ----------------------------------\n");
    PhidalTmp = (PhidalMatrix *)malloc(sizeof(PhidalMatrix)); /* TODO : Init */
    PhidalMatrixBuild(m, Mstart, Mstart, Mend, Mend, "L", "N", phidaloptions.locally_nbr, PhidalTmp, P.L, &BL,
		      levelnum, levelnum+1, ONE);
    PhidalMatrix_Clean(PhidalTmp);
    free(PhidalTmp);
    
    printfv(5, "---------------------------------- E ----------------------------------\n");
    P.E = (PhidalMatrix *)malloc(sizeof(PhidalMatrix));
    PhidalMatrixBuild(m, Sstart, Mstart, Send, Mend, "N", "N", phidaloptions.locally_nbr, P.E, P.E_DB, &BL,
		      levelnum, levelnum+1, ONE);
    
    P.F = (PhidalMatrix *)malloc(sizeof(PhidalMatrix));
    PhidalMatrix_Init(P.F);
    PhidalMatrix_BuildVirtualMatrix(P.E->tli, P.E->tlj, P.E->bri, P.E->brj, P.E, P.F, &BL);
    PhidalMatrix_Transpose(P.F);
    
    printfv(5, "---------------------------------- S ----------------------------------\n");
    PhidalS = (PhidalMatrix *)malloc(sizeof(PhidalMatrix));
    PhidalMatrixBuild(m, Sstart, Sstart, Send, Send, "L", "N", phidaloptions.locally_nbr, PhidalS, P.S, &BL, 
		      levelnum+1, BL.nlevel, ONE);
    
    if(phidaloptions.schur_method == 2)
      {
	P.B = (PhidalMatrix *)malloc(sizeof(PhidalMatrix));
	PhidalMatrix_Init(P.B);
	PhidalMatrix_BuildVirtualMatrix(Sstart, Sstart, Send, Send, m, P.B, &BL);
      }
    
    /* FIXME pour TRSM */
    /* printfv(5, "COEFMAX : %d\n",P.E_DB->solvmtx.coefmax); */
    P.E_DB->solvmtx.coefmax = MAX(P.E_DB->solvmtx.coefmax, calcCoefmax(&P.E_DB->solvmtx /*M*/, &P.L->solvmtx.symbmtx /*L*/));
    /* printfv(5, "COEFMAX : %d\n",P.E_DB->solvmtx.coefmax); */
    
  } else {
    cut = symbmtx->nodenbr -1; 
    Mstart   =  BL.block_levelindex[levelnum];
    Mend     =  BL.nblock-1;  

    SymbolMatrix_Cut2(symbmtx, &P.L->solvmtx.symbmtx,        0,     0,  cut+1, cut+1,  0); /* TODO ou copy, ou directement à la bonne place */
    freeSymbolMatrix(symbmtx);

    /* TODO : un build virtual est inutile ici a l'interieur de la fct */
    PhidalTmp = (PhidalMatrix *)malloc(sizeof(PhidalMatrix)); /* TODO : Init */
    PhidalMatrixBuild(m, Mstart, Mstart, Mend, Mend, "L", "N", phidaloptions.locally_nbr, PhidalTmp, P.L, &BL,
		      levelnum, BL.nlevel, ONE);
    PhidalMatrix_Clean(PhidalTmp);
    free(PhidalTmp);
  }
    
  t2  = dwalltime(); 
  fprintfv(5, stdout, " Build DBPrec in %g seconds\n\n", t2-t1);

  /************************************************************************************************************/
  /* Solve                         ****************************************************************************/
  /************************************************************************************************************/
  ttotal = 0;
  fprintfv(5, stdout, "DB_Precond\n");

  /*** ***/
  fprintfv(5, stdout, " Numeric Factorisation (M)\n"); 

  t1  = dwalltime(); 
  numericFacto2(&P.L->solvmtx);
  t2  = dwalltime(); /* ttotal += t2-t1; */
  fprintfv(5, stdout, "  Numeric Factorisation in %g seconds\n\n", t2-t1);
  /*** ***/

  if (P.forwardlev > 0) {
    /*** ***/
    fprintfv(5, stdout, " TRSM M / E\n"); 
    
    t1  = dwalltime(); 
    SolverMatrix_TRSM(1, &P.L->solvmtx, &P.E_DB->solvmtx);
    t2  = dwalltime(); /* ttotal += t2-t1; */
    fprintfv(5, stdout, "  TRSM in %g seconds\n\n", t2-t1);
    /*** ***/
    
    /*** ***/
    fprintfv(5, stdout, " GEMM E/E -> S\n"); 
    
    P.F_DB = (DBMatrix*)malloc(sizeof(DBMatrix));
    
    DBMatrix_Init(P.F_DB); /* pas ds le bench, on pourrait tuner pour pas en avoir besoin */
    DBMatrix_VirtualCpy(P.E_DB, P.F_DB);
    DBMatrix_Transpose(P.F_DB);

    t1  = dwalltime(); 
    /*     DBMatrix_GEMM(P.S, -1, P.E_DB, P.F_DB, P.L); */
    SolverMatrix_GEMM(&P.S->solvmtx, -1, &P.E_DB->solvmtx, &P.E_DB->solvmtx, &P.L->solvmtx);
    t2  = dwalltime(); ttotal += t2-t1;

    fprintfv(5, stdout, "  GEMM in %g seconds\n\n", t2-t1);
    /*** ***/
    
    exit(1);

    DBMatrix_Clean(P.E_DB);
    free(P.E_DB);
    P.E_DB=NULL;

    DBMatrix_Clean(P.F_DB);
    free(P.F_DB);
    P.F_DB=NULL;

    t1  = dwalltime(); 
    
    P.nextprec = (DBPrec *)malloc(sizeof(DBPrec));
    DBPrec_Init(P.nextprec);
    P.nextprec->symmetric = 1;
    P.nextprec->dim = P.S->dim1;
    
    P.nextprec->prevprec = &P;
    
    levelnum++;
    
    P.nextprec->forwardlev = phidaloptions.forwardlev -1;
    P.nextprec->levelnum = levelnum; /* todo : levelnum doublon de forward en terme d'info ? */
    P.nextprec->schur_method = phidaloptions.schur_method;
    
    if(phidaloptions.schur_method != 1)
      {
	/** The factorization is done in place (A is a void matrix in return **/
	P.nextprec->L = P.S;
      } else {
	P.nextprec->L = (DBMatrix*)malloc(sizeof(DBMatrix));
	DBMatrix_Init(P.nextprec->L); /*inutile*/
	DBMatrix_Cpy(P.S, P.nextprec->L); 
      }

    t2  = dwalltime(); ttotal += t2-t1;
    
    /*** ***/
    fprintfv(5, stdout, " Numeric Factorisation (S)\n"); 
    
    t1  = dwalltime(); 
    numericFacto2(&P.nextprec->L->solvmtx);
    t2  = dwalltime(); ttotal += t2-t1;
    fprintfv(5, stdout, "  Numeric Factorisation in %g seconds\n\n", t2-t1);
    /*** ***/
  }

  fprintfv(5, stdout, " DB_Precond in %g seconds \n\n", ttotal);
  /************************************************************************************************************/

  DBPrec_Info(&P);


  x = (REAL *)malloc(sizeof(REAL)*n);
  b = (REAL *)malloc(sizeof(REAL)*n); 
  for(i=0;i<n;i++)
    x[i] = 1.0;

  PHIDAL_MatVec(m, &BL, x, b);

  bzero(x, sizeof(REAL)*n);

  t1  = dwalltime();
  DBMATRIX_Solve(m, &P, &BL, &phidaloptions, b, x);
  t2  = dwalltime(); 
  fprintfv(5, stdout, "\n Solve in %g seconds \n", t2-t1);

  /* for(i = 0; i < P.S->dim1; i++) { */
  /*     x[i] = 1 - x[i]; */
  /*   } */
  
  /*   ro = DNRM2(P.S->dim1,x,UN); */
  /*   fprintfv(5, stdout, " nrm2(sol-x)) = %e\n\n", ro); */



  /*  PhidalPrec_Clean(&P); */
  
  r = (REAL *)malloc(sizeof(REAL)*m->dim1);
  memcpy(r, b, sizeof(REAL)*m->dim1);
  PHIDAL_MatVecSub(m, &BL, x, r);
  /*matvecz(mat, x, r, r);*/
  fprintfv(5, stdout, "Relative residual norm = %g \n", norm2(r, m->dim1)/norm2(b, m->dim1));
  free(r);



 
  /************************************************************************************************************/
  /* Free Memory                   ****************************************************************************/
  /************************************************************************************************************/

  /* TODO : reste des free a faire */

  free(x);
  free(b);

  PhidalMatrix_Clean(m);
  free(m);
 
  DBPrec_Clean(&P);
  PhidalHID_Clean(&BL); 
  PhidalOptions_Clean(&phidaloptions);

  fprintfv(5, stdout, "END \n");
  return 0;
}




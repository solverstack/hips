/* @authors J. GAIDAMOUR, P. HENON */
/* @release_exclude */

#include <stdio.h>
#include <stdlib.h>

#include <string.h> /*memset*/

#include <assert.h>
#include "block.h"


void PhidalMatrix_p(PhidalMatrix* a) {
  int i,i0;
  int j;

  printfv(5, "----------------------------\n");
  printfv(5, "---------- PRINT -----------\n");
  printfv(5, "----------------------------\n");

  printfv(5, "%d %d %d %d\n", a->tli, a->tlj, a->bri, a->brj);
  
  for(i0=0,i=a->tli; i<=a->bri; i0++, i++) {
    printfv(5, "i0= %d, i=%d\n",i0,i);

    printfv(5, "> %d %d\n",a->cia[i], a->cia[i+1]);
    for(j=a->cia[i];j<a->cia[i+1];j++) {
      printfv(5, "  [%d %d]\n",i, a->cja[j]);
    }
  }

  printfv(5, "----------------------------\n");
}


void PhidalMatrix_p2(DBMatrix* a) {
  int i,i0;
  int j;

  printfv(5, "----------------------------\n");
  printfv(5, "---------- PRINT -----------\n");
  printfv(5, "----------------------------\n");

  printfv(5, "%d %d %d %d\n", a->tli, a->tlj, a->bri, a->brj);
  
  for(i0=0,i=a->tli; i<=a->bri; i0++, i++) {
    printfv(5, "i0= %d, i=%d\n",i0,i);

    printfv(5, "> %d %d\n",a->cia[i], a->cia[i+1]);
    for(j=a->cia[i];j<a->cia[i+1];j++) {
      printfv(5, "  [%d %d]\n",i, a->cja[j]);
    }
  }

  printfv(5, "----------------------------\n");
}

void CS_diff(csptr a, csptr b) {
  int i,i0;
  int j;
  
/*   ascend_column_reorder(a); */

  assert(a->n == b->n);

  for(i=0; i<a->n; i++) {
    printfv(5, "> i= %d/%d\n", i, a->n);

    if (a->nnzrow[i] != b->nnzrow[i]) {
      /* ------------ */
      for(j=0; j<a->nnzrow[i];j++) {
	printfv(5, "  Origin : %d %lf\n", a->ja[i][j], a->ma[i][j]);
      }
      for(j=0; j<b->nnzrow[i];j++) {
	printfv(5, "  New    : %d %lf\n", b->ja[i][j], b->ma[i][j]);
      }
/*       continue; */
      /* ------------ */
    }

    assert(a->nnzrow[i] == b->nnzrow[i]);

    for(j=0; j<a->nnzrow[i];j++) {
      printfv(5, ">> %d %d\n", a->ja[i][j], b->ja[i][j]);
      assert(a->ja[i][j] == b->ja[i][j]);
      assert(a->ma[i][j] == b->ma[i][j]);
    }
  }
  
/*   exit(1); */

  assert(a->nnzr == b->nnzr);
/*   for(i=0;i<a->n; i++) { */
  for(i=0;i<a->nnzr; i++) {
    printfv(5, "nzrtab de %d : %d %d\n",i,a->nzrtab[i],b->nzrtab[i]);
    assert(a->nzrtab[i] == b->nzrtab[i]);
  }

}

void PhidalMatrix_diff(PhidalMatrix* a, PhidalMatrix* b) {
  int i,i0;
  int j;

  printfv(5, "----------------------------\n");
  printfv(5, "---------- DIFF ------------\n");
  printfv(5, "----------------------------\n");

  assert(a->tli == b->tli);
  assert(a->tlj == b->tlj);
  assert(a->bri == b->bri);
  assert(a->brj == b->brj);
  
  for(i0=0,i=a->tli; i<=a->bri; i0++, i++) {
    assert(a->cia[i] == b->cia[i]);
    assert(a->cia[i+1] == b->cia[i+1]);
    
    for(j=a->cia[i];j<a->cia[i+1];j++) {
      assert(a->cja[j] == b->cja[j]);
      /* if (i == a->cja[j]) { */
	printfv(5, "CS_diff on : %d %d (%d)\n", i, a->cja[j], j);
	CS_diff(a->ca[j], b->ca[j]);
	/*       } */
    }

  }

  printfv(5, "----------------------------\n");
}


/* Test si il y a bien correspondance solvtest <-> symbtest */
int check(DBMatrix* m, SolverMatrix* solvtest ,SymbolMatrix* symbtest) {
  int ok;
  int i, i0, j;
  int tl, br;
  int *ia, *ja;
  VSolverMatrix **a;

  if ((m->alloc == ONE) || (m->alloc == CBLK) || (m->alloc == BLK)) {
    tl = m->tlj;
    br = m->brj;
    ia = m->cia;
    ja = m->cja;
    a  = m->ca;     /* TODO : change variable name 'a' bcse m->a exist */
  } else {
    assert(m->alloc == RBLK);
    
    tl = m->tli;
    br = m->bri;
    ia = m->ria;
    ja = m->rja;
    a  = m->ra;
  }

  if (m->alloc == ONE) return 1;

  ok = 0;

  if (m->alloc == BLK) {
    ok = 0;
    for(i0=0,i=tl; i<=br; i0++, i++)
      for(j=ia[i];j<ia[i+1];j++)  
	if ((&m->a[j] == solvtest) 
	    && (&a[j]->symbmtx == symbtest)) ok = 1;
	  	    
    assert(ok == 1);
    return 1;
  }
        
  for(i0=0,i=tl; i<=br; i0++, i++) {
    if (&m->a[i0] == solvtest) {
      for(j=ia[i];j<ia[i+1];j++) {
	if (&a[j]->symbmtx == symbtest) {
	  ok = 1;
	  break;
	}
      }
      assert(ok == 1);
    }
  }

  return 1;
}


/* Test si il y a bien correspondance solvtest <-> symbtest */
int check_(DBMatrix* m, SolverMatrix* solvtest ,SymbolMatrix* symbtest) {
  int ok;
  int i, i0, j;
  int tl, br;
  int *ia, *ja;
  VSolverMatrix **a;

  if ((m->alloc == ONE) || (m->alloc == CBLK) || (m->alloc == BLK)) {
    printfv(5, "CBLK\n");
    tl = m->tlj;
    br = m->brj;
    ia = m->cia;
    ja = m->cja;
    a  = m->ca;     /* TODO : change variable name 'a' bcse m->a exist */
  } else {
    printfv(5, "RBLK\n");
    assert(m->alloc == RBLK);
    
    tl = m->tli;
    br = m->bri;
    ia = m->ria;
    ja = m->rja;
    a  = m->ra;
  }

  for(i0=0,i=tl; i<=br; i0++, i++) {
    for(j=ia[i];j<ia[i+1];j++) {
      if (&a[j]->symbmtx == symbtest) {
	printfv(5, "symbmtx in : %d - %d\n", i0, ja[j]);
	break;
      }
    }
  }

  return 1;
}

/**/

COEF** profilComplex(SolverMatrix* solvmtx, int* m) {
  int n;
  int k, p, i,j, i2;
  COEF** mat;
  COEF* ind;

  int stride;
  int trp;
  int maxrow;

  int decal;

  SymbolMatrix* symbmtx = &(solvmtx->symbmtx);
  n = symbmtx->nodenbr;

  mat = (COEF**)malloc(sizeof(COEF*)*n);

  /*    */
  maxrow=0;
  for(k=0;k<symbmtx->cblknbr;k++) {
    trp = symbmtx->bcblktab[k].fbloknum;
    for(p=trp;p<=symbmtx->bcblktab[k].lbloknum;p++) {

      if (symbmtx->bloktab[p].lrownum > maxrow) maxrow = symbmtx->bloktab[p].lrownum;
    }
  }
  maxrow++;
  *m=maxrow;
  /*    */

  for(i=0; i< n; i++) {
    mat[i] = (COEF*)malloc(sizeof(COEF)*maxrow);
    memset(mat[i], 0, sizeof(COEF)*maxrow);
  }

/*   printfv(5, "n=%d maxrow=%d\n", n , maxrow); */

  decal = symbmtx->ccblktab[0].fcolnum;

  for(k=0;k<symbmtx->cblknbr;k++) {
    trp = symbmtx->bcblktab[k].fbloknum;
    stride = symbmtx->stride[k];
    if (stride == 0) continue;
    /*     printfv(5, "[%d %d]\n",symbmtx->cblktab[k].fcolnum,symbmtx->cblktab[k].lcolnum); */
    /*   for(p=trp;p<=symbmtx->cblktab[k].lbloknum;p++) { */
    /*       printfv(5, "  (%d %d)\n",symbmtx->bloktab[p].frownum, symbmtx->bloktab[p].lrownum); */
    /*     } */
/*     printfv(5, "stride = %d\n", stride); */
    

    for(i = symbmtx->ccblktab[k].fcolnum, i2=0; i<=symbmtx->ccblktab[k].lcolnum; i++, i2++) {
      ind = solvmtx->coeftab + solvmtx->bloktab[trp].coefind + i2*stride;
      assert(ind != NULL);

      for(p=trp;p<=symbmtx->bcblktab[k].lbloknum;p++) {
	for(j=symbmtx->bloktab[p].frownum; j<=symbmtx->bloktab[p].lrownum; j++) {
	  mat[i-decal][j] = *ind;
	  ind++;
	}
      }
      
    }
  }

  return mat;

}



#define HTML

void printComplex(SolverMatrix* AA, char* filename) {
  int  i,j;  
  int m;

  int  n = AA->symbmtx.nodenbr; 
  if (n>100) return;

  int test;
  COEF** mat = profilComplex(AA, &m);

  FILE * stream;
  stream = fopen(filename, "w");

#ifndef HTML
  fprintfv(5, stream,"A:= matrix([\n");
#else
  /*   fprintfv(5, stream,"<table>\n"); */
#endif

  for(j=0; j<m; j++) {    

    test=0;
    for(i=0; i<n; i++) {
      if (mat[i][j] != 0) test = 1;
    }
    if (test == 0) continue;



#ifndef HTML
    fprintfv(5, stream,"[");
#else
    fprintfv(5, stream,"<tr>\n");
#endif

    for(i=0; i</* n */20; i++) {
      if (mat[i][j] != 0)
#ifndef HTML
	fprintfv(5, stream,"%f", mat[i][j]);
#else
      {
        fprintfv(5, stream,"<td>%f</td>", CREAL(mat[i][j]));
	fprintfv(5, stream,"<td>%f</td>", CIMAG(mat[i][j])); 
      }
#endif
      else
#ifndef HTML	
	fprintfv(5, stream,"0");
      if (i!=n-1) fprintfv(5, stream,",");
#else
      { fprintfv(5, stream,"<td>0</td>");  fprintf(stream,"<td>0</td>"); }
#endif
    }
#ifndef HTML
    fprintfv(5, stream,"]");
    if (j!=m-1) fprintfv(5, stream,",\n");
#else
    fprintfv(5, stream,"</tr>\n");
#endif
  }
#ifndef HTML
   fprintfv(5, stream,"]);\n");
#else
   /* fprintfv(5, stream,"</table>\n"); */
#endif
  fclose(stream);
  
}



/***/


/**/


COEF** csr_profilComplex(int n, int* ia, int* ja, COEF* a, int* m) {
  int i,j;
  COEF** mat;

  mat = (COEF**)malloc(sizeof(COEF*)*n);

  /*TODO*/
  *m=n;

  for(i=0; i<n; i++) {
    mat[i] = (COEF*)malloc(sizeof(COEF)*(*m));
    bzero(mat[i], sizeof(COEF)*(*m));
  }


  for(i=0;i<n;i++) {
    for(j=ia[i]; j<ia[i+1]; j++) {
      mat[i][ja[j]] = a[j];
    }
  }

  return mat;

}



#define HTML

void csr_printComplex(int n, int* ia, int* ja, COEF* a, char* filename) {
  int  i,j;  
  int m;

  int test;
  if (n>100) return;
  COEF** mat = csr_profilComplex(n,ia,ja,a, &m);

  FILE * stream;
  stream = fopen(filename, "w");

#ifndef HTML
  fprintfv(5, stream,"A:= matrix([\n");
#else
  /*   fprintfv(5, stream,"<table>\n"); */
#endif

  for(j=0; j<m; j++) {    

    test=0;
    for(i=0; i<n; i++) {
      if (mat[i][j] != 0) test = 1;
    }
    if (test == 0) continue;



#ifndef HTML
    fprintfv(5, stream,"[");
#else
    fprintfv(5, stream,"<tr>\n");
#endif

    for(i=0; i</* n */20; i++) {
      if (mat[i][j] != 0)
#ifndef HTML
	fprintfv(5, stream,"%f", mat[i][j]);
#else
      {
        fprintfv(5, stream,"<td>%f</td>", CREAL(mat[i][j]));
	fprintfv(5, stream,"<td>%f</td>", CIMAG(mat[i][j])); 
      }
#endif
      else
#ifndef HTML	
	fprintfv(5, stream,"0");
      if (i!=n-1) fprintfv(5, stream,",");
#else
      { fprintfv(5, stream,"<td>0</td>");  fprintf(stream,"<td>0</td>"); }
#endif
    }
#ifndef HTML
    fprintfv(5, stream,"]");
    if (j!=m-1) fprintfv(5, stream,",\n");
#else
    fprintfv(5, stream,"</tr>\n");
#endif
  }
#ifndef HTML
   fprintfv(5, stream,"]);\n");
#else
   /* fprintfv(5, stream,"</table>\n"); */
#endif
  fclose(stream);
  
}

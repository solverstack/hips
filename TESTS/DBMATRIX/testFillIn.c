/* @authors J. GAIDAMOUR, P. HENON */
/* @release_exclude */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "math.h"

#include "metis.h"
#include "localdefs.h"
#include "block.h" /* FLOAT */
#include "db_struct.h"
#include "phidal_sequential.h"

#define METIS_ORDER

#define BUFLEN 200

extern void find_supernodes(int n, int *ia, int *ja, int *perm, int *iperm, int *snodenbr, int *snodetab, int *treetab);
extern void ascend_column_reorder(csptr mat);
extern void CSR_Fnum2Cnum(int *ja, int *ia, int n);

/*DEBUG*/
extern int testSymbolicFacto(MatrixStruct* mtxstr, int cblknbr, int* treetab, int* rangtab);
extern void testDecoupe(SymbolMatrix* symbmtx, int*, int*);
extern void test2Decoupe(SymbolMatrix* symbmtx);

#define MIN(x,y) (((x)>(y))?(y):(x))

int main(int argc, char *argv[])
{
  int i;
  int ierr, len;
    
  /* declarations related to Harwell-boeing format for reading the HB
     matrix. Second part is related to I/O parameters */
  char guesol[2], title[72], key[8], type[3];
  int nrhs, nc, n, nnz, tmp0,tmp,tmp2,tmp3,job;
  /* working array for reading matrix */
  REAL *rhstmp;
  int *mapp,*mapptr; 
  int ndom;
  int domsize;
  PhidalMatrix *m;
  PhidalHID BL;

  /* MATRIX */
  REAL *a;
  int *ja, *ia;
  int numflag;  /* C or Fortran */
  
  int *jg, *ig;
  int metisoption[10]; /* Metis */

  /* Symmetrize the matrix */
  int *ib, *jb;
  
  /* 
   * buf is for read from 'inputs' file, 
   * matrix represents matrix name
   */
  int unsym; /* unsym  --  symmetric pattern(0), nonsym pattern(1) */
  char matrix[BUFLEN];
  char sfile_path[BUFLEN];
  int rsa;
  int load_local_files;  /** If set to 1 then genere all the local data file **/

  char *substr;

  int *perm, *iperm;

  int cblknbr;    /* number of supernodes */
  int* rangtab;   /* rangetab[i] is the beginning in the new ordering of the ith supernodes */
  int* treetab;   /* elimination tree */ 

  chrono_t t1,t2,ttotal;

  /* Matrices */
  SymbolMatrix* symbmtx;
  SolverMatrix* solvmtxref;
  SolverMatrix *Lref, *Eref, *Tref;
  DBMatrix *M, *E, *S;
  PhidalMatrix *PhidalM, *PhidalE, *PhidalS; 
  csptr mat;

  /* Vectors */
  REAL *b;

  PhidalOptions phidaloptions
  PhidalOptions* option= &phidaloptions;

  int levelnum; 
  int Mstart, Mend, Sstart, Send;
  int cut, last;

  if(argc < 2)
    {
      fprintferr(stderr, "Illegal argument : testPHIDAL.ex <domain size (in number of node)> \n");
      exit(-1);
    }

  domsize = atoi(argv[1]);


  PhidalOptions_Init(&phidaloptions);
  
  /** Read parameters from the input file **/
  GENERAL_setpar(NULL, matrix, sfile_path, &unsym, &rsa, &load_local_files, &phidaloptions);   

  /************************************************************************************************************/
  /****************************************** READ THE MATRIX *************************************************/
  /************************************************************************************************************/
  tmp0 = 0;
  nrhs = 0;
  /* Read matrix; either using user-defined function (routine user_read) or 
     |               SPARSKIT function for reading Harwell-Boeieng matrices
  */
  if (strtol(matrix, &substr,10)){
    /* If the matrix name starts with  integer in the input file, 
       | User-defined matrix input is provided. The filenames for the
       | user-defined input are stored in the file specified
       | following the integer in the variable "matrix". NOTE: there
       | should be no space between integer and the filename substr
       | contains the name of the file with filenames for the matrix
       | input 
    */
    char filename[100];
    strcpy(filename, matrix+1);
    
    len = strlen(filename);
    /*read only the sizes of matrix arrays first == call user_read
      with job=0*/ 
    tmp = 0; 
    a = NULL; ja = NULL; ia = NULL; rhstmp = NULL;
    userread(filename,&len,&tmp,&n,&nnz,a,ja,ia,&nrhs,rhstmp,&ierr);

    /* allocate proper amount of space */
    ia = (int *) malloc((n+1)*sizeof(int));
    ja = (int *) malloc(nnz*sizeof(int));
    a = (REAL *) malloc(nnz*sizeof(REAL));

    assert(ia != NULL);
    assert(ja != NULL);
    assert(a != NULL);
    /* read the (a, ja, ia) values call userread with job=1 */

    tmp = 1; 

    /************************************************/
    /* READ THE MATRIX FROM DISK (IN USER FORMAT)   */
    /************************************************/
    nrhs = 0; /* To prevent from reading rhs */
    userread(substr,&len,&tmp,&n,&nnz,a,ja,ia,&nrhs,rhstmp,&ierr);
    if(ierr != 0) {
      fprintfv(5, stderr, "cannot read matrix\n");
      exit(-1);
    }
    fprintfv(5, stdout, "Read matrix %s\n", substr);
  }
  else {
    /* variable "matrix" stores the name of the file in HB format 
       |
       |   Read a Harwell-Boeing matrix. using wreadmtc c-version of
       |      sparsit routine - call wreadmtc a first time to determine sizes
       |      of arryas. read in values on the second call. 
    */
    len = strlen(matrix);
    a = NULL; ja = NULL; ia = NULL; rhstmp = NULL;
    wreadmtc(&tmp0,&tmp0,&tmp0,matrix,&len,a,ja,ia,rhstmp,&nrhs,
	     guesol,&n,&nc,&nnz,title,key,type,&ierr); 
    
    ia = (int *) malloc((n+1)*sizeof(int));
    ja = (int *) malloc(nnz*sizeof(int));
    a = (REAL *) malloc(nnz*sizeof(REAL));
    
    tmp = 2;
    tmp2 = n;
    tmp3 = nnz;
    /*********************************/
    /* READ THE MATRIX FROM DISK     */
    /*********************************/
    /* Array sizes determined. Now call wreadmtc again for really
       reading */
    nrhs = 0; /** To prevent from reading rhs **/
    wreadmtc(&tmp2,&tmp3,&tmp,matrix,&len,a,ja,ia,rhstmp,&nrhs,
	     guesol,&n,&nc,&nnz,title,key,type,&ierr); 
    
    if(ierr != 0) 
      fprintfv(5, stderr, "cannot read matrix\n");
    
    fprintfv(5, stdout,"READ the matrix %.*s %.*s \n",8,key,3,type);
  
  }

  /************************ RSA matrix contains only the lower triangular part of the matrix ***************************/
  if(rsa == 1)
    {
      /** Symmetrize the matrix (in rsa we only have the lower triangular part) ****/
      ib = ia;
      jb = ja;
      b = a;
      numflag = 1;
      job = 2;
      PHIDAL_SymmetrizeMatrix(job, numflag, n, ib, jb, b, &ia, &ja, &a);
      nnz = 2*nnz-n;
      unsym = 0;
      free(ib);
      free(jb);
      free(b);
    }

  /*******************************************************************************************************************/
  /*******************************************************************************************************************/
  /*******************************************************************************************************************/
  fprintfv(5, stdout,"Matrix dimension is %d, Number of nonzeros is %d\n",n,nnz);

  /* Translate matrix into C numbering */
  CSR_Fnum2Cnum(ja, ia, n);
  numflag = 0;

  /*****************************************************************************************************************/
  /***********************************Construct the symmetric graph G of the matrix ********************************/
  /*****************************************************************************************************************/
  ig = (int *)malloc(sizeof(int)*(n+1));
  jg = (int *)malloc(sizeof(int)*nnz);
  memcpy(ig ,ia, sizeof(int)*(n+1));
  memcpy(jg ,ja, sizeof(int)*nnz);
  
  if(unsym == 1)
    {
      ib = ig;
      jb = jg;
      /* numflag = 0; */
      job = 0;
      PHIDAL_SymmetrizeMatrix(job, numflag, n, ib, jb, NULL, &ig, &jg, NULL);
      free(ib);
      free(jb);
    }
  /*****************************************************************************************************************/
  /*****************************************************************************************************************/

  /************************************************************************************************************/
  /* Compute the reordering to minimize fill-in using METIS-4.0 ***********************************************/
  /************************************************************************************************************/
  metisoption[0] = 0;
 
  perm = (int *)malloc(sizeof(int)*n);
  iperm = (int *)malloc(sizeof(int)*n);

  /** Delete the self edge in the graph (METIS_NodeND need that)**/
  PHIDAL_CsrDelDiag(numflag, n, ig, jg);

#ifdef METIS_ORDER
  /**** Compute a matrix reordering that minimizes fill-in *******/
  METIS_NodeND(&n, ig, jg, &numflag, metisoption, perm, iperm);
#else
  fprintfv(5, stderr, "METTRE SCOTCH \n");
#endif

  /************************************************************************************************************/
  /* Find SuperNodes               ****************************************************************************/
  /************************************************************************************************************/
  fprintfv(5, stderr, "Find supernodes\n");
  /*   TODO : refait en interne par phidal, ne pas le faire 2 fois */
  rangtab = (int *)malloc(sizeof(int)*(n+1));
  treetab = (int *)malloc(sizeof(int)*n);

  t1  = dwalltime(); 
exit(1);  find_supernodes(n, ia, ja, iperm, perm, &cblknbr, rangtab, treetab);
  t2  = dwalltime(); ttotal += t2-t1;
  fprintfv(5, stderr, " Find supernodes = %d, in %g seconds\n\n", cblknbr, t2-t1);

  /**** Compute the overlapped partition from the matrix reordering   *****/
  t1  = dwalltime(); 
  /*PHIDAL_Perm2OverlappedDomains(ndom, n, ig, jg, &mapptr, &mapp, perm, iperm);*/
  PHIDAL_Perm2SizedDomains(domsize, n, ig, jg, &ndom, &mapptr, &mapp, perm, iperm);
  fprintfv(5, stderr, "Found %d domains \n", ndom);
  t2  = dwalltime(); 
  fprintfv(5, stdout, "Compute the grid of subdomain in %g \n", t2-t1);


  {
    /*********************************************************/
    /*    DISPLAY SOME INFO ON THE OVERLAPPED PARTITION      */
    /*********************************************************/         
    
    int maxdom, mindom;
    REAL avgdom;


    /* fprintfv(5, stdout, "PARTITION: \n");
    for(i=0;i<ndom;i++)
    fprintfv(5, stdout, "Domain %d = %d nodes \n", i, mapptr[i+1]-mapptr[i]);*/
    
    maxdom = 0;
    mindom = mapptr[1]-mapptr[0];
    avgdom = 0.0;
    for(i=0;i<ndom;i++)
      {
	avgdom += mapptr[i+1]-mapptr[i];
	if(mapptr[i+1]-mapptr[i] > maxdom)
	  maxdom = mapptr[i+1]-mapptr[i];
	if(mapptr[i+1]-mapptr[i] < mindom)
	  mindom = mapptr[i+1]-mapptr[i];
      }
    avgdom /= ndom;

    fprintfv(5, stdout, "MAX DOMAIN = %d \n", maxdom);
    fprintfv(5, stdout, "MIN DOMAIN = %d \n", mindom);
    fprintfv(5, stdout, "AVG DOMAIN = %g \n", avgdom);
    /*fprintfv(5, stdout, "IMBALANCE = %g \n", ((float)(maxdom-mindom))*100.0/mindom );*/
  }

  numflag = 0;

    
  /*******************************************************************/
  /*******************************************************************/
  /**                                                               **/
  /** COMPUTE THE HIERARCHICAL DECOMPOSITION AND LOCAL DATA         **/
  /**                                                               **/
  /*******************************************************************/
  /*******************************************************************/
  
  /** Compute the global hierarchical interface decomposition and the associated permutation **/ 
  PhidalHID_Init(&BL);

  t1  = dwalltime(); 
  PHIDAL_HierarchDecomp(phidaloptions.verbose, numflag, n, ig, jg, mapp, mapptr, ndom, &BL, perm, iperm);
  t2  = dwalltime(); 
  fprintfv(5, stdout, " Compute Phidal Hierarchical Decomposition in %g seconds \n\n", t2-t1);

  free(ig);
  free(jg);
  free(mapp);
  free(mapptr);
  
  /************************************************************************************************************/
  /* CSR -> SparRow                ****************************************************************************/
  /************************************************************************************************************/
  fprintfv(5, stdout, "Permuting the matrix \n");

  mat = (csptr)malloc(sizeof(struct SparRow));
  CSRcs(n, a, ja, ia, mat);
  
  /** DO NOT NEED THESE VECTORS ANYMORE **/
  free(a);
  free(ja);
  free(ia);
  
  /*** Permute the matrix according to the phidal ordering ***/
  t1  = dwalltime(); 
  CS_Perm(mat, perm);
  t2  = dwalltime(); 
  fprintfv(5, stdout, " Permute the matrix in %g seconds \n", t2-t1);
  free(perm);
  free(iperm); /**/
  
  t1  = dwalltime(); 
  ascend_column_reorder(mat);/* todo : utile ? */
  t2  = dwalltime(); 
  fprintfv(5, stdout, " Ascend_column_reorder in %g seconds \n\n", t2-t1);

  /************************************************************************************************************/

  /************************************************************************************************************/
#ifdef affich
  fprintfv(5, stdout, "HID : \n");
  printfv(5, "ndom=%d n=%d nblock=%d nlevel=%d\n", BL.ndom, BL.n, BL.nblock, BL.nlevel);
  int j;
  for (i=0; i</*1*/BL.nlevel; i++) {
    printfv(5, "Level %d :\n", i);
    for (j=BL.block_levelindex[i]; j<BL.block_levelindex[i+1]; j++) {
      printfv(5, " block  %d (%d %d) %d\n", j, BL.block_index[j], BL.block_index[j+1]-1,
	      BL.block_index[j+1] - BL.block_index[j]-1+1);
    }
  }
#endif
  /************************************************************************************************************/
  /************************************************************************************************************/
  /************************************************************************************************************/
  symbmtx = (SymbolMatrix*)malloc(sizeof(SymbolMatrix));
  PHIDAL_SymbolMatrix(mat, symbmtx, &rangtab, treetab, &cblknbr, &BL, &phidaloptions);
  
  solvmtxref = (SolverMatrix*)malloc(sizeof(SolverMatrix));
  /*   PHIDAL_SymbolMatrix(mat, &solvmtxref->symbmtx, &rangtab, treetab, &cblknbr, &BL, &phidaloptions); */
  cpy_Symbol(symbmtx, &solvmtxref->symbmtx);

  testEgal_Symbol(symbmtx, &solvmtxref->symbmtx); /* TODO */

  free(treetab);  
  free(rangtab);  /* ds ce cas, pas la peine de s'embeter avec les pointeurs. idem pour cblk */

  /************************************************************************************************************/

  m  = (PhidalMatrix *)malloc(sizeof(PhidalMatrix));

  t1  = dwalltime(); 
  PHIDAL_MatrixBuild(mat, m, &BL, &phidaloptions);
  t2  = dwalltime(); 
  fprintfv(5, stdout, " Build PhidalMatrix in %g seconds \n\n", t2-t1);

  fprintfv(5, stderr, "Number of NNZ in A = %ld \n", PhidalMatrix_NNZ(m));

/*   fprintfv(5, stdout, "Number of NNZ matrix %ld \n", CSnnz(mat)); */
/*   fprintfv(5, stdout, "Number of NNZ in phidal matrix %ld \n", PhidalMatrix_NNZ(m)); */


  
  /************************************************************************************************************/
  /* Cut Test                      ****************************************************************************/
  /************************************************************************************************************/
  cut = BL.block_index[BL.block_levelindex[1]]-1; /* todo : faire comme Sstart ... etc pour les noms */
  last = symbmtx->nodenbr -1; 

  assert(last == BL.block_index[BL.nblock]-1);
  assert(last == symbmtx->cblktab[cblknbr-1].lcolnum);

  printfv(5, "cut=%d, last=%d \n", cut, last);
  assert(cut != last); /* sinon, pas de level2 */

  M = (DBMatrix*)malloc(sizeof(DBMatrix));
  E = (DBMatrix*)malloc(sizeof(DBMatrix));
  S = (DBMatrix*)malloc(sizeof(DBMatrix));

  /* int p; */
  /*   for(p=0; p<symbmtx->bloknbr; p++) { */
  /*     printfv(5, "%d [%d %d]\n", p, symbmtx->bloktab[p].frownum, symbmtx->bloktab[p].lrownum); */
  /*   } */

  SymbolMatrix_Cut2(symbmtx, &M->solvmtx.symbmtx, 0,     0,     cut,  cut,    0);
  /*     printSymbol(&M->solvmtx.symbmtx); */
  SymbolMatrix_Cut2(symbmtx, &E->solvmtx.symbmtx, cut+1, 0,     last, cut,    cut+1);/*  TODO : un arg en trop? */
  SymbolMatrix_Cut2(symbmtx, &S->solvmtx.symbmtx, cut+1, cut+1, last, last,   cut+1);
  freeSymbolMatrix(symbmtx);

  /************************************************************************************************************/
  /* Cut Test                      ****************************************************************************/
  /************************************************************************************************************/
  levelnum = 0;
  Mstart   =  BL.block_levelindex[levelnum];
  Mend     =  BL.block_levelindex[levelnum+1]-1;
  Sstart   =  BL.block_levelindex[levelnum+1];
  Send     =  BL.nblock-1;  

  printfv(5, "----------------------- M ------------------------- \n");
  PhidalM = (PhidalMatrix *)malloc(sizeof(PhidalMatrix));
  PhidalMatrix_Setup(Mstart, Mstart, Mend, Mend, "L", "N", phidaloptions.locally_nbr/*ou0*/, PhidalM, &BL);
  PhidalMatrix_Copy(m, PhidalM, &BL);

  DBMatrix_Setup(Mstart, Mstart, Mend, Mend, "L", "N", phidaloptions.locally_nbr, M, /* &M->solvmtx.symbmtx, */ &BL);
  DBMatrix_Copy(PhidalM, M, &BL, levelnum, levelnum+1, ONE, "L");

  testEgalParcours(&M->solvmtx,&M->solvmtx.symbmtx);
  /* free PhidalM */

  printfv(5, "----------------------- E ------------------------- \n");
  /*   printfv(5, "symbmtx->tli = %d\n", E->solvmtx.symbmtx.tli); */
  /*   printfv(5, "%d\n", E->solvmtx.symbmtx.bloktab[0].frownum); */

  PhidalE = (PhidalMatrix *)malloc(sizeof(PhidalMatrix));
  PhidalMatrix_Setup(Sstart, Mstart, Send, Mend, "N", "N", phidaloptions.locally_nbr/*ou0*/, PhidalE, &BL);
  PhidalMatrix_Copy(m, PhidalE, &BL);

  DBMatrix_Setup(Sstart, Mstart, Send, Mend, "N", "N", phidaloptions.locally_nbr, E, /* &M->solvmtx.symbmtx, */ &BL);
  DBMatrix_Copy(PhidalE, E, &BL, levelnum, levelnum+1, ONE, "N"); /* que des zeros! */
  /* free PhidalE */

  printfv(5, "----------------------- S ------------------------- \n");
  PhidalS = (PhidalMatrix *)malloc(sizeof(PhidalMatrix));
  PhidalMatrix_Setup(Sstart, Sstart, Send, Send, "L", "N" , phidaloptions.locally_nbr/*ou0*/, PhidalS, &BL);
  PhidalMatrix_Copy(m, PhidalS, &BL);

  DBMatrix_Setup(Sstart, Sstart, Send, Send, "L", "N", phidaloptions.locally_nbr, S, /* &M->solvmtx.symbmtx, */ &BL);
  DBMatrix_Copy(PhidalS, S, &BL, levelnum+1, BL.nlevel, ONE, "N");
  /* free PhidalS */

  /************************************************************************************************************/
  /* TEST ==                       ****************************************************************************/
  /************************************************************************************************************/
  /* */
  csr2SolverMatrix(solvmtxref, mat);
  Lref = SolverMatrix_Cut(solvmtxref, 0,     0,     cut,  cut,    0);
  Eref = SolverMatrix_Cut(solvmtxref, cut+1, 0,     last, cut,    cut+1);
  Tref = SolverMatrix_Cut(solvmtxref, cut+1, cut+1, last, last,   cut+1);


  printfv(5, "Test Egal L\n");
  /*   testEgal_Symbol(&M->solvmtx.symbmtx, &Lref->symbmtx); */
  testEgal2_2(&M->solvmtx,Lref,"L");

  printfv(5, "Test Egal E \n");
  testEgal2_2(&E->solvmtx,Eref,"N");

  printfv(5, "Test Egal S \n");
  testEgal2_2(&S->solvmtx,Tref,"L");
  /************************************************************************************************************/
 
  /************************************************************************************************************/
  /* Free Memory                   ****************************************************************************/
  /************************************************************************************************************/

  /* TODO : reste des free a faire */

  /*   todo  freeSolverMatrix(solvmtx); */
  cleanCS(mat);
  free(mat);
  
  PhidalHID_Clean(&BL); 

  PhidalOptions_Clean(&phidaloptions);

  fprintfv(5, stdout, "END \n");
  return 0;
}




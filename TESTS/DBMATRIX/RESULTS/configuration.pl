#!/usr/bin/perl -w

$matrixlist = 'MATRIX';
$inputsT = 'inputs.template';

#---
#$machine = 'CCRT';
#$machine = 'M3PEC';
$machine = 'LOCAL';

$prog = '../testDBMatrix.ex';

#---

$droptol0 = 0.0;
@droptol1list = (0.001);

#---

@levellist = (0, "ALL");
@ndomlist  = (500, 1000, 2000);
@stratlist = (2);

#---

$mem = 27000;
$temps = '04:00:00';
if($machine =~ /CCRT/)
{
    $temps = '29'; #minutes
    $temps = '240';
}
#---

$sym = 1;
$driver = "";

#---

$matrixlist = 'MATRIX';
$inputsT = 'inputs.template';

#if($machine =~ /M3PEC/)
#{
    $submitT = 'submit'.$machine.'.template';
#}

#!/usr/bin/perl -w

#$machine = 'M3PEC';
$machine = 'LOCAL';

$inputsT = 'inputs.template';

$prog = '../testDBMatrix.ex';
$matrixlist = 'MATRIX';

$droptol0 = 0.0;
$droptol1 = 0.0;



#@levellist = ("0", "1", "ALL");
@levellist = ("ALL");


#@ndomlist = (1000, 2000);
@ndomlist = (1000);


#@stratlist = ("NOREC", "0", "1", "2");
@stratlist = ("2");


$inputsT = 'inputs.template';

if($machine =~ /M3PEC/)
{
    $submitT = 'submitM3PEC.template';
}

$sym = 2;
$driver = "";

if($machine !~ /LOCAL/)
{
    if( ! -f $submitT)
    {
	print "le fichier $submitT n\'existe pas \n";
	exit;
    }
}

if( ! -f $inputsT)
{
    print "le fichier $inputsT n\'existe pas \n";
    exit;
}


if( ! -e $prog)
{
    $flag=0;
    until($flag eq 1)
    { 
	print 'Exec à utiliser ? ';
	$prog = <STDIN>;
	chop $prog;
	
	if( -e $prog)
	{
	    $flag = 1;
	}
	else
	{
	    print "Erreur: $prog n\' existe pas \n";
	}
    }
}


if( ! -f $matrixlist)
    {
	print "le fichier $matrixlist n\'existe pas \n";
	exit;
    }


## Ouverture fichier contenant liste de matrice
open(CMD, "<$matrixlist");
@matfilelist = <CMD>;
close CMD;
my @matlist;
for($i=0;$i <= $#matfilelist;$i++)
{
    chop $matfilelist[$i];
    if( $matfilelist[$i] =~ /..*/)
    {
	$matlist[$i] = $matfilelist[$i];
    }
}


for($i=0;$i <= $#matlist;$i++)
{
    if( ! -f $matlist[$i])
    {
	print "le fichier matrice : $matlist[$i]  n\'existe pas \n";
	exit;
    }
    print $matlist[$i]."\n";
}

	    
#print 'Nom des matrices :';
#$a = <STDIN>;
#chop $a;
#$ma = "$a";
#@matlist = split(/ /, $ma);


#print 'Precision de la solution :';
#$prec = <STDIN>;
#chop $prec;    
$prec = "1e-7";

if($droptol0 < 0)
{
    print 'Dropping threshold interior domain :';
    $droptol0 = <STDIN>;
    chop $droptol0;
}

if($droptol1 < 0)
{
    print 'Dropping threshold on interface :';
    $droptol1 = <STDIN>;
    chop $droptol1;
}



print "############################################## \n";
print "#  ON EST SUR $machine                         \n";
if($sym == 0)
{
    print "# MATRICES RUA PATTERN NON SYMMETRIQUES \n";
}
if($sym == 1)
{
    print "# MATRICES RUA PATTERN SYMMETRIQUES  \n";
}
if($sym == 2)
{
    print "# MATRICES RSA (SYMMETRIQUES)  \n";
}

print "# Liste matrices: @matlist \n";
print "# Executable    : $prog \n";
print "# Domnbr        : @ndomlist \n";
print "# Level         : @levellist \n";
print "# Strat         : @stratlist \n";
print "# Precision     : $prec\n";
print "# Droptol0      : $droptol0 \n";
print "# Droptol1      : $droptol1 \n";
print "# Fichier inputs:  $inputsT \n";
if($machine !~ /LOCAL/)
{
    print "# Fichier lance : $submitT \n";
}
print "############################################## \n \n";
print "==> Est-ce correcte ? o/n \n";

$ans = &ouinon;
if( $ans =~ /n/ )
{
    exit;
}
	  


foreach $mat (@matlist)
{
    foreach $locally (@levellist)
    {
	$matinputs = $mat;
	$matinputs =~ s/.*\///g;
	
	$matpath = $mat;
	$matpath =~  s/^1//;
	$matpath =~  s/\/1/\//;
	$matname = $matpath;
	$matname =~ s/.*\///g;
	
	
	if( ! -e $matpath)
	{
	    print "$matpath n'existe pas : SKIP IT \n";
	    next;
	}
	
	$maindir = `pwd`;
	chop $maindir;
	
	$testdir = $maindir.'/RESULT_'.$matname."_LOC_".$locally."_".$prec;
	
	if(! -d $testdir)
	{
	    print "Creation de $testdir \n";
	    system "mkdir $testdir";
	}
	else
	{
	    print "$testdir Existe deja !\n";
	    print "Continuer ? (o/n) \n";
	    $ans = &ouinon;
	    if( $ans =~ /n/ )
	    {
		exit;
	    }
	    
	}
	
	
	$progname = $prog;
	$progname =~ s/.*\///g;
	
	print "cp -f $prog ${testdir}/$progname \n";
	system "cp -f $prog ${testdir}/$progname";
	
	
	#### CREATION DES FICHIERS INPUTS ET SUBMIT
	foreach $strat (@stratlist)
	{
	    
	    $i = $strat;
	    if( $strat =~ /NOREC/ )
	    {
		$rec = 0;
		$i = 0;
	    }
	    else
	    {
		$rec = 1;
	    }
	    
	    print "Creating run for strat ${strat} \n";
	    $stratdir = $testdir . "/STRAT_${strat}";
	    if( ! -d $stratdir)
	    {
		print "Creation de $stratdir \n";
		system "mkdir $stratdir";
	    }
	    
	    
	    print "ln -sf ${testdir}/$progname ${stratdir}/ \n";
	    system "ln -sf ${testdir}/$progname ${stratdir}/";
	    
	    if($matpath =~ /^\//)
	    {
		$command = 'ln -sf '.$matpath." ${stratdir} \n";
	    }
	    else
	    {
		$command = 'ln -sf ../../'.$matpath." ${stratdir} \n";
	    }
	    system $command;
	    
	    ## Creation fichier inputs 
	    open(CMD, "<$inputsT");
	    @inp = <CMD>;
	    close CMD;
	    
	    if($locally =~ /ALL/)
	    {
		$locally = 200;
	    }
	    
	    $krylov = 300;
	    if($strat =~ /NOREC/ || $strat =~/0/)
	    {
		$maxit = 250;
		$inner = 0;
	    }
	    else
	    {
		$maxit = 1;
		$inner = 250;
	    }


	    foreach (@inp)
	    {
		s/_MATFILE_/$driver$matinputs/g;
		s/_DROPTOL1_/$droptol1/g;
		s/_PREC_/$prec/g;
		s/_DROPTOL0_/$droptol0/g;
		s/_DROPTOL1_/$droptol1/g;
		s/_KRYLOV_/$krylov/g;
		s/_MAXIT_/$maxit/g;
		s/_INNER_/$inner/g;
		s/_STRAT_/$i/g;
		s/_SYM_/$sym/g;
		s/_REC_/$rec/g;
		s/_LOCALLY_/$locally/g;
	    }
	    open(FOUT, ">$stratdir/inputs");
	    print FOUT @inp;
	    close FOUT;
	    
	    ## Creation fichiers submits
	    if($machine =~ /M3PEC/)
	    {
		&submit_M3PEC;
	    }
	    else
	    {
		&submit_LOCAL;
	    }
	    
	}
    }
}


sub submit_M3PEC
{
    #### CREATION DES SCRIPTS DE SOUSMISSION
    open(CMD, "<$submitT");
    @script = <CMD>;
    close CMD;
    
    $mem = 8000;
    $temps = '04:00:00';

   #$fout = $matname.'_'.$p;
   # $ferror = 'Err_'.$matname.'_'.$p;
    $fout = $matname;
    $ferror = 'Err_'.$matname;
    @subf = @script;
    foreach (@subf)
    {
	s/_OUTPUT_/$fout/g;
	s/_ERROR_/$ferror/g;
	s/_NPROC_/1/g;
	s/_MEMORY_/$mem/;
	s/_TEMPS_/$temps/;
	s/_INITDIR_/$stratdir/;
    }

    open(FOUT, ">$stratdir/lance.$strat");
    print FOUT @subf;
    foreach $p (@ndomlist)
    {
	$resout = $matname.'_'.$p;
	print FOUT "./$progname $p > $resout ;\n";
    }

    system "llsubmit $stratdir/lance.$strat";
    

}

sub submit_LOCAL
{
    #### CREATION D'UN SCRIPT DE LANCEMENT #####
    

    open(FOUT, ">$stratdir/lance.$strat");
    print FOUT '#!/bin/sh';
    print FOUT "\n";

    foreach $p (@ndomlist)
    {
	$resout = $matname.'_'.$p;
#	$ferror = 'Err_'.$matname.'_'.$p;
	print FOUT "echo ./$progname $p > $resout ;\n";
	print FOUT "./$progname $p > $resout ;\n";
    }

   
 
    close FOUT;
    chdir $stratdir;
    system "chmod u+x ./lance.$strat";
    system "./lance.$strat";
    chdir $maindir;

}



sub ouinon
{
    local($r);
    $r = <STDIN>;
    chop $r;
    
    until($r =~ /(^o|^n)/)
    {
	print 'o/n ? ';
	$r = <STDIN>;  
	chop $r;
    }
    
    $_[0] = $r;
}

./scriptSEQ_unsym.pl 2> /dev/null

./clean.sh

var=`egrep -r END * | grep test.rua | grep -v test3.sh | wc -l`
var2=`egrep -r "iter" * | grep "39" | grep -v "Maximum" | wc -l`
var3=`egrep -r "iter" * | grep " 10" | grep -v "Maximum" | grep -v "No iteration" | wc -l`
var4=`egrep -r "iter" * | grep " 10" | grep -v "Maximum" | wc -l`

if [ $var -eq 12 ] 
then
    echo "OK ($1)"
else
    echo "PAS OK 1 ($1)"
    exit 1
fi

if [ $var2 -eq 4 ] 
then
    echo "OK ($1)"
else
    echo "PAS OK 2 ($1)"
    exit 1
fi


if [ $var3 -eq 8 ] 
then
    echo "OK ($1)"
else
    echo "PAS OK 3 ($1)"
    exit 1
fi


if [ $var4 -eq 8 ] 
then
    echo "OK ($1)"
else
    echo "PAS OK 4 ($1)"
    exit 1
fi

rm -rf RESULT_test.rua_*

./scriptSEQ_symnorec.pl 2> /dev/null

./clean.sh

var=`egrep -r END * | grep test.rsa | grep -v test.sh | wc -l`
var2=`egrep -r "iter" * | grep "25" | grep -v "Maximum" | wc -l`
var3=`egrep -r "iter" * | grep "8" | grep -v "Maximum" | wc -l`
var4=`egrep -r "iter" * | grep "15" | grep -v "Maximum" | wc -l`

if [ $var -eq 3 ] 
then
    echo "OK ($1)"
else
    echo "PAS OK 1 ($1)"
    exit 1
fi

if [ $var2 -eq 1 ] 
then
    echo "OK ($1)"
else
    echo "PAS OK 2 ($1)"
    exit 1
fi

if [ $var3 -eq 1 ] 
then
    echo "OK ($1)"
else
    echo "PAS OK 3 ($1)"
    exit 1
fi


if [ $var4 -eq 1 ] 
then
    echo "OK ($1)"
else
    echo "PAS OK 4 ($1)"
    exit 1
fi

rm -rf RESULT_test.rsa_*

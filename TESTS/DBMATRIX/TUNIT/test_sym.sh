rm -rf RESULT_test.rsa_*

########################################################
cd ../../../
make clean > /dev/null
make -j3 > /dev/null
########################################################


########################################################
cd SRC/BLOCK/DBMATRIX/

gcc -g -DDEBUG_M -Wall   -Di686 -DLINUX -I/home/jeremie/travail/dev/metis-4.0/Lib -I../../../INCLUDE -I /home/jeremie/travail/dev/Linux_PIIISSE1/include/   DBMATRIX_MLICCPrec.c -c -o DBMATRIX_MLICCPrec.o -DNO_PIC_DECREASE

make > /dev/null
########################################################


########################################################
cd ../../../TESTS/DBMATRIX
make clean > /dev/null
make > /dev/null
########################################################

cd TUNIT
./subtest_sym.sh NO_PIC_DECREASE
if [ $? -eq 1 ] 
then
  exit 1
fi



cd ../../../

########################################################
cd SRC/BLOCK/DBMATRIX/

gcc -g -DDEBUG_M -Wall   -Di686 -DLINUX -I/home/jeremie/travail/dev/metis-4.0/Lib -I../../../INCLUDE -I /home/jeremie/travail/dev/Linux_PIIISSE1/include/   DBMATRIX_MLICCPrec.c -c -o DBMATRIX_MLICCPrec.o

make
########################################################


########################################################
cd ../../../TESTS/DBMATRIX
make clean
make
########################################################

cd TUNIT
./subtest_sym.sh PIC_DECREASE
if [ $? -eq 1 ] 
then
  exit 1
fi



echo FIN

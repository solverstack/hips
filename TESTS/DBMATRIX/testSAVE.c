/* @authors J. GAIDAMOUR, P. HENON */
/* @release_exclude */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "math.h"

#include "localdefs.h"
#include "block.h" /* FLOAT */
#include "db_struct.h"
#include "phidal_sequential.h"

#include "base.h"

#define BUFLEN 200

extern void find_supernodes(int n, int *ia, int *ja, int *perm, int *iperm, int *snodenbr, int *snodetab, int *treetab);
extern void ascend_column_reorder(csptr mat);
extern void CSR_Fnum2Cnum(int *ja, int *ia, int n);

int main(int argc, char *argv[])
{
  /* int UN = 1; */
  int i;
  int ierr, len;
    
  /* declarations related to Harwell-boeing format for reading the HB
     matrix. Second part is related to I/O parameters */
  char guesol[2], title[72], key[8], type[3];
  int nrhs, nc, n, nnz, tmp0,tmp,tmp2,tmp3,job;
  /* working array for reading matrix */
  REAL *rhstmp;
  int *mapp,*mapptr; 
  int ndom;
  int domsize;
  PhidalMatrix *m;
  PhidalHID BL;

  /* MATRIX */
  REAL *a;
  int *ja, *ia;
  int numflag;  /* C or Fortran */
  
  int *jg, *ig;

  /* Symmetrize the matrix */
  int *ib, *jb;
  
  /* 
   * buf is for read from 'inputs' file, 
   * matrix represents matrix name
   */
  int unsym; /* unsym  --  symmetric pattern(0), nonsym pattern(1) */
  char matrix[BUFLEN];
  char sfile_path[BUFLEN];
  int rsa;

  char *substr;

  int *perm, *iperm;

  int cblknbr_l1;    /* number of cblk for level 1       */
  int* dom2cblktab = NULL;
  int* rangtab=NULL;   /* rangetab[i] is the beginning in the new ordering of the ith supernodes */
  int* treetab=NULL;   /* elimination tree */ 

  chrono_t t1,t2/* ,ttotal */;
  REAL nnzA, nnzL;

  /* Matrices */
  SymbolMatrix* symbmtx;
  csptr mat;

  /* Vectors */
  REAL *x, *b;
  /*   REAL ro; */
  REAL* r;

  PhidalOptions phidaloptions;
  PhidalOptions*option= &phidaloptions;

  DBPrec P;

/*   if(argc < 2) */
/*     { */
/*       fprintferr(stderr, "Illegal argument : testPHIDAL.ex <domain size (in number of node)> \n"); */
/*       exit(-1); */
/*     } */

/*   domsize = atoi(argv[1]); */


  PhidalOptions_Init(&phidaloptions);
  
  /** Read parameters from the input file **/
  GENERAL_setpar(NULL, matrix, sfile_path, &unsym, &rsa, &phidaloptions);   

  /************************************************************************************************************/
  /****************************************** READ THE MATRIX *************************************************/
  /************************************************************************************************************/
  tmp0 = 0;
  nrhs = 0;
  /* Read matrix; either using user-defined function (routine user_read) or 
     |               SPARSKIT function for reading Harwell-Boeieng matrices
  */
  if (strtol(matrix, &substr,10)){
    /* If the matrix name starts with  integer in the input file, 
       | User-defined matrix input is provided. The filenames for the
       | user-defined input are stored in the file specified
       | following the integer in the variable "matrix". NOTE: there
       | should be no space between integer and the filename substr
       | contains the name of the file with filenames for the matrix
       | input 
    */
    char filename[100];
    strcpy(filename, matrix+1);
    
    len = strlen(filename);
    /*read only the sizes of matrix arrays first == call user_read
      with job=0*/ 
    tmp = 0; 
    a = NULL; ja = NULL; ia = NULL; rhstmp = NULL;
    userread(filename,&len,&tmp,&n,&nnz,a,ja,ia,&nrhs,rhstmp,&ierr);

    /* allocate proper amount of space */
    ia = (int *) malloc((n+1)*sizeof(int));
    ja = (int *) malloc(nnz*sizeof(int));
    a = (REAL *) malloc(nnz*sizeof(REAL));

    assert(ia != NULL);
    assert(ja != NULL);
    assert(a != NULL);
    /* read the (a, ja, ia) values call userread with job=1 */

    tmp = 1; 

    /************************************************/
    /* READ THE MATRIX FROM DISK (IN USER FORMAT)   */
    /************************************************/
    nrhs = 0; /* To prevent from reading rhs */
    userread(substr,&len,&tmp,&n,&nnz,a,ja,ia,&nrhs,rhstmp,&ierr);
    if(ierr != 0) {
      fprintfv(5, stderr, "cannot read matrix\n");
      exit(-1);
    }
    fprintfv(5, stdout, "Read matrix %s\n", substr);
  }
  else {
    /* variable "matrix" stores the name of the file in HB format 
       |
       |   Read a Harwell-Boeing matrix. using wreadmtc c-version of
       |      sparsit routine - call wreadmtc a first time to determine sizes
       |      of arryas. read in values on the second call. 
    */
    len = strlen(matrix);
    a = NULL; ja = NULL; ia = NULL; rhstmp = NULL;
    wreadmtc(&tmp0,&tmp0,&tmp0,matrix,&len,a,ja,ia,rhstmp,&nrhs,
	     guesol,&n,&nc,&nnz,title,key,type,&ierr); 
    
    ia = (int *) malloc((n+1)*sizeof(int));
    ja = (int *) malloc(nnz*sizeof(int));
    a = (REAL *) malloc(nnz*sizeof(REAL));
    
    tmp = 2;
    tmp2 = n;
    tmp3 = nnz;
    /*********************************/
    /* READ THE MATRIX FROM DISK     */
    /*********************************/
    /* Array sizes determined. Now call wreadmtc again for really
       reading */
    nrhs = 0; /** To prevent from reading rhs **/
    wreadmtc(&tmp2,&tmp3,&tmp,matrix,&len,a,ja,ia,rhstmp,&nrhs,
	     guesol,&n,&nc,&nnz,title,key,type,&ierr); 
    
    if(ierr != 0) 
      fprintfv(5, stderr, "cannot read matrix\n");
    
    fprintfv(5, stdout,"READ the matrix %.*s %.*s \n",8,key,3,type);
  
  }
  
  /********************** SUPPRESS THE ZERO IN THE CSR MATRIX *****/
  numflag = 1;
  i = nnz;
  nnz = CSR_SuppressZeros(numflag, n, ia, ja, a);
  fprintfv(5, stdout, "Zeros removed from the matrix = %d \n", i-nnz); 
 
  /*   fprintfv(5, stdout, "NNZ = %ld \n", (long) nnz); */
  /****************************************************************/

  nnzA = nnz; /* apres symetrize matrix = (ia[n]+n)/2.0; */
  fprintfv(5, stdout, "Number of NNZ in A (CSR) = %g \n", nnzA);

  /************************ RSA matrix contains only the lower triangular part of the matrix ***************************/
  if(rsa == 1)
    {
      /** Symmetrize the matrix (in rsa we only have the lower triangular part) ****/
      ib = ia;
      jb = ja;
      b = a;
      numflag = 1;
      job = 2;
      PHIDAL_SymmetrizeMatrix(job, numflag, n, ib, jb, b, &ia, &ja, &a);
      nnz = 2*nnz-n;
      unsym = 0;
      free(ib);
      free(jb);
      free(b);
    }

  /*******************************************************************************************************************/
  /*******************************************************************************************************************/
  /*******************************************************************************************************************/
  fprintfv(5, stdout,"Matrix dimension is %d, Number of nonzeros is %d\n",n,nnz);

  /* Translate matrix into C numbering */
  CSR_Fnum2Cnum(ja, ia, n);
  numflag = 0;

  /*****************************************************************************************************************/
  /***********************************Construct the symmetric graph G of the matrix ********************************/
  /*****************************************************************************************************************/
  ig = (int *)malloc(sizeof(int)*(n+1));
  jg = (int *)malloc(sizeof(int)*nnz);
  memcpy(ig ,ia, sizeof(int)*(n+1));
  memcpy(jg ,ja, sizeof(int)*nnz);
  
  if(unsym == 1) 
    {
      ib = ig;
      jb = jg;
      /* numflag = 0; */
      job = 0;
      PHIDAL_SymmetrizeMatrix(job, numflag, n, ib, jb, NULL, &ig, &jg, NULL);
      free(ib);
      free(jb);
    }
  /*****************************************************************************************************************/
  /*****************************************************************************************************************/

  /************************************************************************************************************/
  /* Compute the reordering to minimize fill-in using METIS-4.0 ***********************************************/
  /************************************************************************************************************/
 
  perm = (int *)malloc(sizeof(int)*n);
  iperm = (int *)malloc(sizeof(int)*n);

  /** Delete the self edge in the graph (METIS_NodeND need that)**/
  PHIDAL_CsrDelDiag(numflag, n, ig, jg);

  /**** Compute the overlapped partition from the matrix reordering   *****/
  t1  = dwalltime(); 
  /*PHIDAL_Perm2OverlappedDomains(ndom, n, ig, jg, &mapptr, &mapp, perm, iperm);*/
  PHIDAL_Perm2SizedDomains_SAVE(domsize, n, ig, jg, &ndom, &mapptr, &mapp, perm, iperm);

  fprintfv(5, stdout, "END \n");
  return 0;
}




/* @authors J. GAIDAMOUR, P. HENON */
/* @release_exclude */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "io.h"
#include "localdefs.h"
#include "block.h" /* FLOAT */
#include "db_struct.h"
#include "phidal_sequential.h"

#include "base.h"

#define BUFLEN 200

#define TR_INF

COEF** convertCSR2Dense(int N, int nnz, int* ia, int* ja, COEF* a) {
  int i,j;
  COEF** A = (COEF**)malloc(sizeof(COEF*)*N);
  for (i=0; i<N; i++)
#ifdef TR_INF
    A[i] = (COEF*)calloc(i+1, sizeof(COEF));
#else
    A[i] = (COEF*)calloc(N, sizeof(COEF));
#endif

  for(i=0;i<N;i++)
    {
      for(j=ia[i];j<ia[i+1];j++) {
	/* La matrice young4c.mtx est symmétrique mais a tous les termes */
	/* cat ~/travail/matrices/young4c.mtx | grep "^1 " */

	/* A[i] : 1er ligne donc lecture d'une CSR (row) */
#ifdef TR_INF
	if (i >= ja[j])  /* que le tr. inf. */
#endif
	  A[i][ja[j]] = a[j];
	
      }
    }

  printfv(5, "Dense : %d Mo\n",N*N*sizeof(COEF)/(1024*1024));

  return A;

}


COEF** convertCS2Dense(csptr mat) {
  int i,j;
  int N = mat->n;

  COEF** A = (COEF**)malloc(sizeof(COEF*)*N);
  for (i=0; i<N; i++)
#ifdef TR_INF
    A[i] = (COEF*)calloc(i+1, sizeof(COEF));
#else
    A[i] = (COEF*)calloc(N, sizeof(COEF));
#endif

  for(i=0;i<N;i++) {
    for(j=0;j<mat->nnzrow[i];j++) {
#ifdef TR_INF
      if (i >= mat->ja[i][j])  /* que le tr. inf. */
#endif
	A[i][mat->ja[i][j]] = mat->ma[i][j];
      
    }
  }

  printfv(5, "Dense : %d Mo\n",N*N*sizeof(COEF)/(1024*1024));

  return A;

}

void factoLDLtDense(int N, COEF** A) {
  int k,p,q;
  COEF f;

  for(k=0; k<N; k++) {
    for(p=k+1; p<N; p++) {
      
      f = A[p][k] / A[k][k];
      
      for(q=p; q<N; q++) {
	A[q][p] = A[q][p] - A[q][k] * f;
      }
      
      A[p][k] = f;
      
    }
  }
}

void printDense(int N, COEF** A) {
  int i,j;
  
  FILE* fd = fopen("matrix.mm_","w");

  fprintfv(5, fd, "%%%%MatrixMarket matrix coordinate complex symmetric\n");
  fprintfv(5, fd, "841 841 4089\n"); /* TODO */

  for (j=0; j<N; j++) {
    for (i=0; i<N; i++) {
#ifdef TR_INF
      if (i >= j) {
#endif
	if (A[i][j] != 0) {
#ifdef TYPE_COMPLEX
	  fprintfv(5, fd, "%d %d %.13e  %.13e\n", i+1, j+1, pcoef(A[i][j])); 
#else
	  fprintfv(5, fd, "%d %d %.13e\n", i+1, j+1, pcoef(A[i][j])); 
#endif
	}
#ifdef TR_INF
      }
#endif
    }
  }

  fclose(fd);
}

void freeDense(int N, COEF** A) {
  int i;
  for (i=0; i<N; i++)
    free(A[i]);
  free(A);
  
}


int main(int argc, char *argv[])
{
  /* int UN = 1; */
  int i;
    
  int n, nnz, job;
  /* working array for reading matrix */
  /*   COEF *rhstmp; */
  int *mapp,*mapptr; 
  int ndom;
  int domsize;
  PhidalMatrix *m;
  PhidalHID BL;

  /* MATRIX */
  COEF *a;
  int *ja, *ia;
  int numflag;  /* C or Fortran */
  
  int *jg, *ig;



  /* Symmetrize the matrix */
  int *ib, *jb;
  
  /* 
   * buf is for read from 'inputs' file, 
   * matrix represents matrix name
   */
  int unsym; /* unsym  --  symmetric pattern(0), nonsym pattern(1) */
  char matrix[BUFLEN];
  char sfile_path[BUFLEN];
  int rsa;

  int *perm, *iperm;

  chrono_t t1,t2/* ,ttotal */;
  REAL nnzA, nnzL;

  /* Matrices */
  SymbolMatrix* symbmtx;
  csptr mat;

  /* Vectors */
  COEF *x, *b;
  /*   REAL ro; */
  COEF* r;

  PhidalOptions phidaloptions;
  PhidalOptions* option= &phidaloptions;

  DBPrec P;

  domsize = 100000000;

  PhidalOptions_Init(&phidaloptions);
  
  /** Read parameters from the input file **/
  GENERAL_setpar(NULL, matrix, sfile_path, &unsym, &rsa, &phidaloptions);   

  /************************************************************************************************************/
  /****************************************** READ THE MATRIX *************************************************/
  /************************************************************************************************************/
  CSRread(matrix, &n, &nnz, &ia, &ja, &a, NULL, NULL);

  assert(phidaloptions.symmetric == rsa);

  /* Translate matrix into C numbering */
  CSR_Fnum2Cnum(ja, ia, n);
  numflag = 0;

  /* { */
  /*   COEF** A = convertCSR2Dense(n, nnz, ia, ja, a); */
  /*   printDense(n,A); */
  /*   freeDense(n, A); */
  /* } */

  /********************** SUPPRESS THE ZERO IN THE CSR MATRIX *****/
#ifdef SUPPRESS_ZERO
  i = nnz;
  nnz = CSR_SuppressZeros(numflag, n, ia, ja, a);
  fprintfv(5, stdout, "Zeros removed from the matrix = %d \n", i-nnz); 
 
  if ((rsa == 0) && (unsym == 0) && (i-nnz > 0))
    unsym = 1;
#endif
  /*   fprintfv(5, stdout, "NNZ = %ld \n", (long) nnz); */
  /****************************************************************/

#ifdef DEBUG__
  for(i=0;i<n;i++)
    {
      ia[i] = i;
      ja[i] = i;
#ifdef TYPE_COMPLEX
      a[i] = 1.0;
#else
      a[i] = 1.0;
#endif
    }
  ia[n] = n;
  nnz = n;
#endif



  nnzA = nnz; /* apres symetrize matrix = (ia[n]+n)/2.0; */
  fprintfv(5, stdout, "Number of NNZ in A (CSR) = %g \n", nnzA);
  
  fprintfv(5, stdout, "Needed memory for A : %d Mo (only coeftab storage)\n", (int)((nnzA*sizeof(REAL))/1048576));

  /*******************************************************************************************************************/
  /*******************************************************************************************************************/

  /************************ RSA matrix contains only the lower triangular part of the matrix ***************************/
  /* TODO : notre code REAL les coef si la matrice est symmetrique mais avec tous les termes (young4c.mtx) */

  if(rsa == 1)
    {
      /** Symmetrize the matrix (in rsa we only have the lower triangular part) ****/
      ib = ia;
      jb = ja;
      b = a;
      job = 2 /* 2 */; /* TODO : pk pas 0 */
      PHIDAL_SymmetrizeMatrix(job, numflag, n, ib, jb, b, &ia, &ja, &a); /* RETOURNE A ! */
      nnz = 2*nnz-n;
      unsym = 0;
      free(ib);
      free(jb);
      free(b); /* si 2, a commenter */
    }

/*   { */
/*     COEF** A = convertCSR2Dense(n, nnz, ia, ja, a); */
/*     printDense(n,A); */
/*     freeDense(n, A); */
/*   } */

  fprintfv(5, stdout,"RSA = %d UNSYM = %d\n",rsa,unsym);

  /*******************************************************************************************************************/
  fprintfv(5, stdout,"Matrix dimension is %d, Number of nonzeros is %d\n",n,nnz);

  /*****************************************************************************************************************/
  /***********************************Construct the symmetric graph G of the matrix ********************************/
  /*****************************************************************************************************************/
  ig = (int *)malloc(sizeof(int)*(n+1));
  jg = (int *)malloc(sizeof(int)*nnz);
  memcpy(ig ,ia, sizeof(int)*(n+1));
  memcpy(jg ,ja, sizeof(int)*nnz);
  
  if(unsym == 1) 
    {
      ib = ig;
      jb = jg;
      job = 0;
      PHIDAL_SymmetrizeMatrix(job, numflag, n, ib, jb, NULL, &ig, &jg, NULL);
      free(ib);
      free(jb);
    }

  /*****************************************************************************************************************/
  /*****************************************************************************************************************/

  /************************************************************************************************************/
  /* Compute the reordering to minimize fill-in using METIS-4.0 ***********************************************/
  /************************************************************************************************************/
 
  perm = (int *)malloc(sizeof(int)*n);
  iperm = (int *)malloc(sizeof(int)*n);

  /** Delete the self edge in the graph (METIS_NodeND need that)**/
  PHIDAL_CsrDelDiag(numflag, n, ig, jg);

  /**** Compute the overlapped partition from the matrix reordering   *****/
  t1  = dwalltime(); 
  /*PHIDAL_Perm2OverlappedDomains(ndom, n, ig, jg, &mapptr, &mapp, perm, iperm);*/
  PHIDAL_Perm2SizedDomains(domsize, n, ig, jg, &ndom, &mapptr, &mapp, perm, iperm);
  t2  = dwalltime(); 

  fprintfv(5, stdout, "Compute the grid of subdomain in %g \n", t2-t1);
  fprintfv(5, stdout, "DOMSIZE = %d \n", domsize);
  fprintfv(5, stdout, "Found %d domains \n", ndom);


  {
    /*********************************************************/
    /*    DISPLAY SOME INFO ON THE OVERLAPPED PARTITION      */
    /*********************************************************/         
    
    int maxdom, mindom;
    REAL avgdom;


    /* fprintfv(5, stdout, "PARTITION: \n");
    for(i=0;i<ndom;i++)
    fprintfv(5, stdout, "Domain %d = %d nodes \n", i, mapptr[i+1]-mapptr[i]);*/
    
    maxdom = 0;
    mindom = mapptr[1]-mapptr[0];
    avgdom = 0.0;
    for(i=0;i<ndom;i++)
      {
	avgdom += mapptr[i+1]-mapptr[i];
	if(mapptr[i+1]-mapptr[i] > maxdom)
	  maxdom = mapptr[i+1]-mapptr[i];
	if(mapptr[i+1]-mapptr[i] < mindom)
	  mindom = mapptr[i+1]-mapptr[i];
      }
    avgdom /= ndom;

    fprintfv(5, stdout, "MAX DOMAIN = %d \n", maxdom);
    fprintfv(5, stdout, "MIN DOMAIN = %d \n", mindom);
    fprintfv(5, stdout, "AVG DOMAIN = %g \n", avgdom);
    /*fprintfv(5, stdout, "IMBALANCE = %g \n", ((float)(maxdom-mindom))*100.0/mindom );*/
  }

  /*******************************************************************/
  /*******************************************************************/
  /**                                                               **/
  /** COMPUTE THE HIERARCHICAL DECOMPOSITION AND LOCAL DATA         **/
  /**                                                               **/
  /*******************************************************************/
  /*******************************************************************/
  
  /** Compute the global hierarchical interface decomposition and the associated permutation **/ 
  PhidalHID_Init(&BL);

  t1  = dwalltime(); 
  PHIDAL_HierarchDecomp(phidaloptions.verbose, numflag, n, ig, jg, mapp, mapptr, ndom, &BL, perm, iperm);
  t2  = dwalltime(); 
  fprintfv(5, stdout, " Compute Phidal Hierarchical Decomposition in %g seconds \n\n", t2-t1);


  /*#define OLD*/
#ifdef OLD
  PHIDAL_GetSupernodes(phidaloptions.verbose, numflag, n,  ig,  jg,  
		       &BL, perm, iperm, &dom2cblktab, &rangtab, &treetab);

  cblknbr_l1 = dom2cblktab[ndom];

#ifdef DEBUG_CBLK
    { 
      int i,j;
      for(i=0;i<ndom;i++)
        {
  	fprintfv(5, stderr, "Domain %d cblknbr = %d \n", i, dom2cblktab[i+1]-dom2cblktab[i]);
  	printfv(5, "[%d %d]\n", rangtab[dom2cblktab[i]], rangtab[dom2cblktab[i+1]-1+1]-1) ;
        }
    }

  {
    int icblk, iBL;
    icblk=0;
    for(iBL=0; iBL<=BL.block_levelindex[1]; iBL++) {
      while(rangtab[icblk] < BL.block_index[iBL]) { /* TODO : remplacer test avec dom2cblktab */
	/* 	printfv(5, "(%d %d)\n",rangtab[icblk],rangtab[icblk+1]-1); */
	icblk++;
	/* printfv(5, "%d %d\n",icblk, cblknbr_l1); */
	assert(icblk <= cblknbr_l1);
      }
      
      if (rangtab[icblk] > BL.block_index[iBL]) {
	printfv(5, "\n\nERREUR : COUPE : %d\n",BL.block_index[iBL]);
	assert(0);
      }
      
      if (rangtab[icblk] == BL.block_index[iBL]) {
	/*       	printfv(5, "MATCH : %d\n",BL.block_index[iBL]); */
      }
    }

  }
#endif
  free(dom2cblktab);
#else 
  fprintfv(5, stdout, "Build Symbolic Matrix \n");
  symbmtx = (SymbolMatrix*)malloc(sizeof(SymbolMatrix));
  if(phidaloptions.schur_method != 1)
    HIPS_SymbolMatrix(phidaloptions.verbose, numflag, 0.05, phidaloptions.locally_nbr, n,  ig,  jg, 
		      &BL, symbmtx, perm, iperm);
  else
    HIPS_SymbolMatrix(phidaloptions.verbose, numflag, 0.05, BL.nlevel, n,  ig,  jg, 
		      &BL, symbmtx, perm, iperm);

#endif



  free(mapp);
  free(mapptr);
  free(ig);
  free(jg);
 
  /*
  CSR -> SparRow
  */
  fprintfv(5, stdout, "Permuting the matrix \n");

  mat = (csptr)malloc(sizeof(struct SparRow));
  
  if(initCS(mat, n)) 
    {
      printfv(5, " ERROR SETTING UP bmat IN initCS \n") ;
      exit(0);
    }
  CSRcs(n, a, ja, ia, mat);   /* TODO : normaliser nom : cs2csr(P, m->ria, m->rja, NULL); */
  
  /***************µµµ*****/
  /*   COEF** A = convertCS2Dense(mat); */
  /*   /\*   factoLDLtDense(n, A); *\/ */
  /*   printDense(n, A); */
  /*   freeDense(n, A); */
  /* OK : correspond au fichier de départ de la 3young4c.mtx*/
  /***************µµµ*****/

  /** DO NOT NEED THESE VECTORS ANYMORE **/
  free(a);
  free(ja);
  free(ia);
  
  /*** Permute the matrix according to the phidal ordering ***/
  t1  = dwalltime(); 
  CS_Perm(mat, perm);/* ! */
  t2  = dwalltime(); 
  fprintfv(5, stdout, " Permute the matrix in %g seconds \n", t2-t1);
  free(perm);
  free(iperm); /**/
  
  t1  = dwalltime(); 
  ascend_column_reorder(mat);/* todo : utile ? */
  t2  = dwalltime(); 
  fprintfv(5, stdout, " Ascend_column_reorder in %g seconds \n\n", t2-t1);

  /* fprintfv(5, stdout, "Number of NNZ in A (SparRow) = %ld \n", CSnnz(mat)); */
  
/* { */
/*   COEF** A = convertCS2Dense(mat); */
/*   /\*   factoLDLtDense(n, A); *\/ */
/*   printDense(n, A); */
/*   freeDense(n, A); */
/* } */

  /************************************************************************************************************/
  /************************************************************************************************************/
#ifdef OLD
  fprintfv(5, stdout, "Build DB_Prec structure\n"); 
  t1  = dwalltime(); 
  symbmtx = (SymbolMatrix*)malloc(sizeof(SymbolMatrix));

  if(phidaloptions.schur_method != 1)
    PHIDAL_SymbolMatrix(mat, symbmtx, &rangtab, treetab, cblknbr_l1, &BL, phidaloptions.locally_nbr);
  else /* S est conservé et dc calculé de manière exacte : remplissage locally consistant */
    PHIDAL_SymbolMatrix(mat, symbmtx, &rangtab, treetab, cblknbr_l1, &BL, BL.nlevel);

  t2  = dwalltime(); 
  free(treetab);  
  free(rangtab);  /* ds ce cas, pas la peine de s'embeter avec les pointeurs. idem pour cblk */
  /* Comptage du nombre de non zeros dans la symbolMatrix */
  /*nnzL = SymbolMatrix_NNZ(symbmtx);*/
  fprintfv(5, stdout, " Build DB_Prec structure in %g seconds\n", t2-t1); 
#endif
  /************************************************************************************************************/
  fprintfv(5, stdout, "Build PhidalMatrix %d\n", phidaloptions.symmetric); 
  fprintfv(5, stdout, "Build PhidalMatrix\n"); 
  t1  = dwalltime(); 
  m  = (PhidalMatrix *)malloc(sizeof(PhidalMatrix));
  PHIDAL_MatrixBuild(mat, m, &BL, &phidaloptions); /* MPROTECT */
  t2  = dwalltime(); 
  cleanCS(mat);
  free(mat);
  assert(m->dim1 == n);

  fprintfv(5, stdout, " Build PhidalMatrix in %g seconds \n\n", t2-t1);
  /* fprintfv(5, stdout, " Number of NNZ in A (Phidal) = %ld \n", PhidalMatrix_NNZ(m)); */

  /*   fdumpPhidalMatrix("phidalbuild1.txt", m); */

  assert(m->bloknbr == 1);

  assert(phidaloptions.forwardlev == 0 || phidaloptions.forwardlev == 1);

/*   assert(m->symmetric == 0); */

  /* **** */
  
  DBPrec_Init(&P);  

  DBMATRIX_Precond(m, &P, symbmtx, &BL, &phidaloptions);
 
  nnzL = DBPrec_NNZ(&P);
  fprintfv(5, stdout, " Number of NNZ in Preconditioner = %g\n", nnzL);
  fprintfv(5, stdout, " Fill Ratio of Preconditioner = %g\n\n", nnzL / nnzA);

/*   DBPrec_Info(&P); */

  fprintfv(5, stdout, "Needed memory for preconditioner : %d Mo (only coeftab storage)\n", 
	  (int)((nnzL*sizeof(REAL))/1048576));

  x = (COEF *)malloc(sizeof(COEF)*n);
  b = (COEF *)malloc(sizeof(COEF)*n); 
  for(i=0;i<n;i++)
    /*x[i] = I/sqrt(n);*/
    x[i] = 1;

  PHIDAL_MatVec(m, &BL, x, b);

#ifndef DEBUG_NOALLOCATION
  bzero(x, sizeof(COEF)*n);
#endif
/*   for(i=0;i<n;i++) */
/*     b[i] = I/sqrt(n); */
  
#define WITH_GMRES
#ifdef WITH_GMRES
  t1  = dwalltime();
  DBMATRIX_Solve(m, &P, &BL, &phidaloptions, b, x);
  t2  = dwalltime(); 
  fprintfv(5, stdout, "\n Solve in %g seconds \n", t2-t1);
#endif


  {
    int UN=1;  
    /*     ro = BLAS_NRM2(P.SL->dim1,x,UN); */
    /*     fprintfv(5, stdout, " nrm2(sol-x)) = %e\n\n", ro); */
    
    /*  PhidalPrec_Clean(&P); */
    
    r = (COEF *)malloc(sizeof(COEF)*m->dim1);
    memcpy(r, b, sizeof(COEF)*m->dim1);
    PHIDAL_MatVecSub(m, &BL, x, r);
    /*matvecz(mat, x, r, r);*/

    fprintfv(5, stdout, "GMRES : Relative residual norm = %g \n", BLAS_NRM2(m->dim1, r, UN) / BLAS_NRM2(m->dim1, b, UN));
    free(r);
  }

  
#ifndef DEBUG_NOALLOCATION
  bzero(x, sizeof(COEF)*n);
#endif

  DBMatrix_Lsolv(1, P.L, x, b, &BL);
  
  if(P.symmetric == 1) 
    DBMatrix_Dsolv(P.L, x);

  DBMatrix_Usolv(P.symmetric, P.U, x, x, &BL);

  
  /*   for(i = 0; i < n; i++) { */
  /*     x[i] = 1 - x[i]; */
  /*   } */

  {
    int UN=1;  
    /*     ro = BLAS_NRM2(P.SL->dim1,x,UN); */
    /*     fprintfv(5, stdout, " nrm2(sol-x)) = %e\n\n", ro); */
    
    /*  PhidalPrec_Clean(&P); */
    
    r = (COEF *)malloc(sizeof(COEF)*m->dim1);
    memcpy(r, b, sizeof(COEF)*m->dim1);
    PHIDAL_MatVecSub(m, &BL, x, r);
    /*matvecz(mat, x, r, r);*/

    fprintfv(5, stdout, "Direct : Relative residual norm = %g \n", BLAS_NRM2(m->dim1, r, UN) / BLAS_NRM2(m->dim1, b, UN));
    free(r);
  }
 
  /************************************************************************************************************/
  /* Free Memory                   ****************************************************************************/
  /************************************************************************************************************/

  /* TODO : reste des free a faire */

  free(x);
  free(b);

  PhidalMatrix_Clean(m);
  free(m);
 
  DBPrec_Clean(&P);
  PhidalHID_Clean(&BL); 
  PhidalOptions_Clean(&phidaloptions);

  fprintfv(5, stdout, "END \n");
  return 0;
}




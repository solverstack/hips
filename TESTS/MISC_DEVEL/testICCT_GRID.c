/* @authors J. GAIDAMOUR, P. HENON */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "math.h"

#include "metis.h"
#include "localdefs.h"
#include "phidal_sequential.h"



#define METIS_ORDER


#define BUFLEN 200


#ifndef TYPE_REAL
#error Ne marche qu en DOUBLE
#endif

extern int CSrealloc(csptr mat);
extern void reorder_interiors(csptr mat, PhidalHID *BL);
extern void GENERAL_setpar(char *filename, char *matrix, char *sfile_path,  int
	    *unsym, int *rsa, int *load_local_files, PhidalOptions *option) ;
extern void writeVecInt(FILE *fp, int *vec, int n);
extern void readVecInt(FILE *fp, int *vec, int n);
extern void checkCSR(int n, int *ia, int *ja, REAL *a, int num);

extern void compute_row_perm(csptr M, int *perm, REAL *rscale);

extern void patch_for_AQUILON(int n, int *ia, int *ja, REAL *a);
extern void diag_dominance(int baseval, int n, int *ia, int *ja, REAL *a); /** Use to test the convergence **/
extern void dumpLLt(FILE *fp, csptr L, csptr U);
extern void symmetric_scaling(int n, int *ia, int *ja, REAL *a, int numflag, REAL *Rowscal, REAL *Colscal);
extern void ascend_column_reorder(csptr mat);
extern void lower_triangular(csptr M, csptr L, int inarow);


int main(int argc, char *argv[])
{
  /*
   * num    --  working scalar
   * iov    --  overlap
   * unsym  --  symmetric pattern(0), nonsym pattern(1)
   */
  int iov, unsym, ierr,len;
  int load_local_files;  /** If set to 1 then genere all the local data file **/

  /* declarations related to Harwell-boeing format for reading the HB
     matri. Second part is related to I/O parameters */
  char guesol[2], title[72], key[8], type[3];
  int nrhs, nc, n, nnz, tmp0,tmp,tmp2,tmp3,job;
  long nnzL;

  /* working array for reading matrix */
  REAL *a,*rhstmp,  res, dnnz_pre, dgprec_nnz;
  chrono_t t1,t2,t3,t4;
  int *ja, *ia,*jb,*ib, *jg, *ig, volume,option[10],wgtflag,numflag;
  int nloc,*mapp,*mapptr, maxmp,
    *iwk,node,i1,i2,i,j,k; 
  csptr mat, L;
  int ndom;
  int domsize;
  REAL *x;
  REAL *b;
  PhidalMatrix *m;
  PhidalHID BL;

  /* working array for symmetrizing matrix */
  REAL *mc;
  int *jc, *ic;

  FILE *fp;

  


  /* 
   * buf is for read from 'inputs' file, 
   * matrix represents matrix name
   */
  char matrix[BUFLEN];
  char sfile_path[BUFLEN];
  char *substr;

  int *riord;
  int *perm, *iperm;
  int rsa;

  PhidalOptions phidaloptions;
  PhidalOptions* option= &phidaloptions;

  int maxdomsize;

  if(argc < 2)
    {
      fprintferr(stderr, "Illegal arguement : testORDERING <number of domains> \n");
      exit(-1);
    }

  domsize = atoi(argv[1]);


  PhidalOptions_Init(&phidaloptions);

  /** Read parameters from the input file **/
	       /*if(argc == 0)*/
    /** Default: read from file "input" **/
  GENERAL_setpar(NULL, matrix, sfile_path, &unsym, &rsa, &load_local_files, &phidaloptions);   
  /*else
    GENERAL_setpar(argv[1], matrix, sfile_path, &unsym, &rsa,  &load_local_files, &phidaloptions);   */
  

  
  /*{ 
    int *ja;
    REAL *ma;
    int *ja2;
    REAL *ma2;
    int g, h;
    int n1, n2;
    Queue heap;
    g = atoi(argv[1]);
    h = atoi(argv[2]);
    ja = (int *)malloc(sizeof(int)*g);
    ma = (REAL *)malloc(sizeof(REAL)*g);
    ja2 = (int *)malloc(sizeof(int)*g);
    ma2 = (REAL *)malloc(sizeof(REAL)*g);

    for(i=0;i<g;i++)
      {
	ja[i] = 2*i;
	if(i%2 == 0)
	  ma[i] = -(int) ((float)rand()/RAND_MAX * g);
	else
	  ma[i] =  (int) ((float)rand()/RAND_MAX * g);
      }
    memcpy(ja2, ja, sizeof(int)*g);
    memcpy(ma2, ma, sizeof(REAL)*g);

    for(i=0;i<g;i++)
      fprintfv(5, stdout, "%d:%g ", ja[i], ma[i]);
    fprintfv(5, stdout, "\n\n");
    queueInit(&heap, g);
    n1 = g;
    vec_filldrop(&n1, ja, ma, h, &heap);
    for(i=0;i<n1;i++)
      fprintfv(5, stdout, "%d:%g ", ja[i], ma[i]);
    fprintfv(5, stdout, "\n\n");

    n1 = g/2;
    n2 = g-n1;
    LU_filldrop(&n1, ja2, ma2, &n2, ja2+n1, ma2+n2, h, &heap);
    
    for(i=0;i<n1;i++)
      fprintfv(5, stdout, "%d:%g ", ja2[i], ma2[i]);
    fprintfv(5, stdout, " || " );
    for(i=0;i<n2;i++)
      fprintfv(5, stdout, "%d:%g ", ja2[i+g/2], ma2[i+g/2]);
    fprintfv(5, stdout, "\n\n");

    free(ja);
    free(ma);
    exit(0);
    }*/



  /************************************************************************************************************/
  /****************************************** READ THE MATRIX *************************************************/
  /************************************************************************************************************/

  CSRread(matrix, &n, &nnz, &ia, &ja, &a, NULL, NULL);
  assert(phidaloptions.symmetric == rsa);

  /************************ RSA matrix contains only the lower triangular part of the matrix ***************************/
  if(rsa == 1)
    {
      /** Symmetrize the matrix (in rsa we only have the lower triangular part) ****/
      ib = ia;
      jb = ja;
      b = a;
      numflag = 1;
      job = 2;
      PHIDAL_SymmetrizeMatrix(job, numflag, n, ib, jb, b, &ia, &ja, &a);
      nnz = 2*nnz-n;
      unsym = 0;
      free(ib);
      free(jb);
      free(b);
    }
  else
    {
      fprintfv(5, stderr, "ERROR: matrix must be symmetric (inputs file : symmetric should be equal to 2) \n");
      exit(-1);
    }

  /*******************************************************************************************************************/
  /*******************************************************************************************************************/
  /*******************************************************************************************************************/
  fprintfv(5, stdout,"Matrix dimension is %d, Number of nonzeros is %d\n",n,nnz);



  /*****************************************************************************************************************/
  /***********************************Construct the symmetric graph G of the matrix ********************************/
  /*****************************************************************************************************************/
  ig = (int *)malloc(sizeof(int)*(n+1));
  jg = (int *)malloc(sizeof(int)*nnz);
  memcpy(ig ,ia, sizeof(int)*(n+1));
  memcpy(jg ,ja, sizeof(int)*nnz);

  
  if(unsym == 1)
    {
      
      ib = ig;
      jb = jg;
      numflag = 1;
      job = 0;
      PHIDAL_SymmetrizeMatrix(job, numflag, n, ib, jb, NULL, &ig, &jg, NULL);
      free(ib);
      free(jb);
    }
  /*****************************************************************************************************************/
  /*****************************************************************************************************************/


  /* Translate matrix into C numbering */
  fprintfv(5, stderr, "Convert FORTRAN to C indexing \n");
  CSR_Fnum2Cnum(ja, ia, n);
  CSR_Fnum2Cnum(jg, ig, n);
  numflag = 0;
  

  perm = (int *)malloc(sizeof(int)*n);
  iperm = (int *)malloc(sizeof(int)*n);

  /*** Compute the reordering to minimize fill-in using METIS-4.0 ***/
  option[0] = 0;

  /** Delete the self edge in the graph (METIS_NodeND need that)**/
  PHIDAL_CsrDelDiag(numflag, n, ig, jg);

#ifdef METIS_ORDER
  /**** Compute a matrix reordering that minimizes fill-in *******/
  METIS_NodeND(&n, ig, jg, &numflag, option, perm, iperm);
#else
  fprintfv(5, stderr, "METTRE SCOTCH \n");
#endif
  /**** Compute the overlapped partition from the matrix reordering   *****/
  /*PHIDAL_Perm2OverlappedDomains(ndom, n, ig, jg, &mapptr, &mapp, perm, iperm);*/
  PHIDAL_Perm2SizedDomains(domsize, n, ig, jg, &ndom, &mapptr, &mapp, perm, iperm);
  fprintfv(5, stderr, "Found %d domains \n", ndom);




  {
    /*********************************************************/
    /*    DISPLAY SOME INFO ON THE OVERLAPPED PARTITION      */
    /*********************************************************/         
    
    int maxdom, mindom;
    REAL avgdom;


    /* fprintfv(5, stdout, "PARTITION: \n");
    for(i=0;i<ndom;i++)
    fprintfv(5, stdout, "Domain %d = %d nodes \n", i, mapptr[i+1]-mapptr[i]);*/
    
    maxdom = 0;
    mindom = mapptr[1]-mapptr[0];
    avgdom = 0.0;
    for(i=0;i<ndom;i++)
      {
	avgdom += mapptr[i+1]-mapptr[i];
	if(mapptr[i+1]-mapptr[i] > maxdom)
	  maxdom = mapptr[i+1]-mapptr[i];
	if(mapptr[i+1]-mapptr[i] < mindom)
	  mindom = mapptr[i+1]-mapptr[i];
      }
    avgdom /= ndom;

    fprintfv(5, stdout, "MAX DOMAIN = %d \n", maxdom);
    fprintfv(5, stdout, "MIN DOMAIN = %d \n", mindom);
    fprintfv(5, stdout, "AVG DOMAIN = %g \n", avgdom);
    fprintfv(5, stdout, "IMBALANCE = %g \n", ((float)(maxdom-mindom))*100.0/mindom );
  }

  /***********************************************************/
  /*           REORDERING IN MULTI BLOCK DIAGONAL LEVEL      */
  /***********************************************************/ 

  numflag = 0;

    
  /*******************************************************************/
  /*******************************************************************/
  /**                                                               **/
  /** COMPUTE THE HIERARCHICAL DECOMPOSITION AND LOCAL DATA         **/
  /**                                                               **/
  /*******************************************************************/
  /*******************************************************************/
  
  /** Compute the global hierarchical interface decomposition and the associated permutation **/ 
  PhidalHID_Init(&BL);
  PHIDAL_HierarchDecomp(1, numflag, n, ig, jg, mapp, mapptr, ndom, &BL, perm, iperm);

  free(ig);
  free(jg);
  free(mapp);
  free(mapptr);


  /****************************************************************/
  /****************************************************************/
  /**                                                            **/
  /**   REODER THE MATRIX ACCORDING TO THE PHIDAL PERMUTATION    **/
  /**                                                            **/
  /****************************************************************/
  /****************************************************************/
  /*---------------------------------------------------------------------*
    |  Convert the matrix in SparRow and permute it                       |
    *---------------------------------------------------------------------*/
  fprintfv(5, stdout, "Permuting the matrix \n");

  mat = (csptr)malloc(sizeof(struct SparRow));
  initCS(mat, n);
  CSRcs(n, a, ja, ia, mat);
  
  /** DO NOT NEED THESE VECTORS ANYMORE **/
  free(a);
  free(ja);
  free(ia);
  
  /*** Permute the matrix according to the phidal ordering ***/
  /*dpermC(mat, perm);*/
  CS_Perm(mat, perm);

  m = (PhidalMatrix *)malloc(sizeof(PhidalMatrix));
  PhidalMatrix_Build(1, "L", mat, m, &BL);

  fprintfv(5, stderr, "Number of NNZ in A = %ld \n", PhidalMatrix_NNZ(m));


  /*fprintfv(5, stdout, "Number of NNZ matrix %ld \n", CSnnz(mat));*/
  /*fprintfv(5, stdout, "Number of NNZ in phidal matrix %ld \n", PhidalMatrix_NNZ(m));*/

  /********* Test multiply **********/
  x = (REAL *)malloc(sizeof(REAL)*mat->n);
  b = (REAL *)malloc(sizeof(REAL)*mat->n);

  for(i=0;i<mat->n;i++)
    x[i] = 1.0;


  {
    PhidalPrec P;
    int levelfor;
    REAL *r;

    bzero(b, sizeof(REAL)*mat->n);
    PHIDAL_MatVec(m, &BL, x, b);


    if(argc < 3)
      {
	fprintfv(5, stderr, "testPHIDAL_ICCT.ex <ndom> <nlevelforward> \n");
	exit(-1);
      }
    levelfor = atoi(argv[2]);
    
    phidaloptions.forwardlev = levelfor;
    /** Correct the options **/
    PhidalOptions_Fix(&phidaloptions, &BL);
    PhidalOptions_Print(stdout, &phidaloptions);

    t1  = dwalltime();    
    PHIDAL_MLICCTPrec(m, &P, &BL, &phidaloptions);
    t2  = dwalltime(); 
    fprintfv(5, stdout, " PHIDAL_MLICCTPrec in %g seconds \n\n", t2-t1);
    
    fprintfv(5, stdout, "PhidalPrec NNZ = %ld \n ", PhidalPrec_NNZ(&P));
    fprintfv(5, stdout, "Fill Ratio = %g \n\n", ((REAL)PhidalPrec_NNZ(&P))/((REAL)PhidalMatrix_NNZ(m)));
    fprintfv(5, stdout, "Fill Ratio for symmetrized matrix = %g \n\n", ((REAL)2*PhidalPrec_NNZ(&P)-mat->n)/((REAL)2*PhidalMatrix_NNZ(m)-mat->n));

    /*PhidalMatrix_PrintBLockNNZ(stdout, P.L);*/
    
    bzero(b, sizeof(REAL)*mat->n);
      PHIDAL_MatVec(m, &BL , x, b);

    bzero(x, sizeof(REAL)*mat->n);
    t1  = dwalltime();
    PHIDAL_Fgmresd(phidaloptions.verbose, phidaloptions.tol, phidaloptions.itmax, m, &P, &BL, &phidaloptions, b, x, stdout);  
    t2  = dwalltime(); 
    fprintfv(5, stdout, "\n Multi-level Solve in %g seconds \n", t2-t1);
    PhidalPrec_Clean(&P);
    
    r = (REAL *)malloc(sizeof(REAL)*m->dim1);
    memcpy(r, b, sizeof(REAL)*m->dim1);
    /*PHIDAL_MatVecSub(m, &BL, x, r);*/
    matvecz(mat, x, r, r);
    fprintfv(5, stdout, "Relative residual norm = %g \n", norm2(r, m->dim1)/norm2(b, m->dim1));
    free(r);

  }

  
  free(x);
  free(b);
  

  PhidalMatrix_Clean(m);
  free(m);
 
  free(perm);
  free(iperm);

  PhidalOptions_Clean(&phidaloptions);
  cleanCS(mat);
  free(mat);
  
  PhidalHID_Clean(&BL); 
  fprintfv(5, stdout, "END \n");

}




/* @authors J. GAIDAMOUR, P. HENON */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "base.h"
#include "io.h"
#include "localdefs.h"
#include "phidal_sequential.h"

#define BUFLEN 200


#ifndef TYPE_REAL
#error Ne marche qu en DOUBLE
#endif

int main(int argc, char *argv[])
{
  /*
   * num    --  working scalar
   * unsym  --  symmetric pattern(0), nonsym pattern(1)
   */
  int unsym;

  /* declarations related to Harwell-boeing format for reading the HB
     matrix. Second part is related to I/O parameters */
  int n, nnz, job;

  /* working array for reading matrix */
  REAL *a;
  chrono_t t1,t2;
  int *ja, *ia,*jb,*ib, *jg, *ig, numflag;
  int *mapp,*mapptr,i; 
  csptr mat;
  int ndom;
  int domsize;
  REAL *x;
  REAL *b;
  PhidalMatrix *m;
  PhidalHID BL;


  /* 
   * buf is for read from 'inputs' file, 
   * matrix represents matrix name
   */
  char matrix[BUFLEN];
  char sfile_path[BUFLEN];

  int *perm, *iperm;
  int rsa;

  PhidalOptions phidaloptions;
  PhidalOptions* option= &phidaloptions;

  if(argc < 2)
    {
      fprintferr(stderr, "Illegal argument : testPHIDAL.ex <domain size (in number of node)> \n");
      exit(-1);
    }

  domsize = atoi(argv[1]);


  PhidalOptions_Init(&phidaloptions);

  /** Read parameters from the input file **/
	       /*if(argc == 0)*/
    /** Default: read from file "input" **/
  GENERAL_setpar(NULL, matrix, sfile_path, &unsym, &rsa, &phidaloptions);   

  /************************************************************************************************************/
  /****************************************** READ THE MATRIX *************************************************/
  /************************************************************************************************************/

  CSRread(matrix, &n, &nnz, &ia, &ja, &a, NULL, NULL);
  assert(phidaloptions.symmetric == rsa);

  /********************** SUPPRESS THE ZERO IN THE CSR MATRIX *****/
#ifdef SUPPRESS_ZERO  
  numflag = 1;
  i = nnz;
  nnz = CSR_SuppressZeros(numflag, n, ia, ja, a);
  fprintfv(5, stdout, "Zeros removed from the matrix = %d \n", i-nnz); 
 
  if ((rsa == 0) && (unsym == 0) && (i-nnz > 0))
    unsym = 1;
#endif
  fprintfv(5, stdout, "NNZ = %ld \n", (long) nnz);

  /************************ RSA matrix contains only the lower triangular part of the matrix ***************************/
  if(rsa == 1)
    {
      /** Symmetrize the matrix (in rsa we only have the lower triangular part) ****/
      ib = ia;
      jb = ja;
      b = a;
      numflag = 1;
      job = 2;
      PHIDAL_SymmetrizeMatrix(job, numflag, n, ib, jb, b, &ia, &ja, &a);
      nnz = 2*nnz-n;
   
      unsym = 0;
      free(ib);
      free(jb);
      free(b);
      fprintfv(5, stdout, "This matrix is in RSA format \n");
    }



  /*******************************************************************************************************************/
  /*******************************************************************************************************************/
  /*******************************************************************************************************************/
  fprintfv(5, stdout,"Matrix dimension is %d, Number of nonzeros is %d\n",n,nnz);



  /*****************************************************************************************************************/
  /***********************************Construct the symmetric graph G of the matrix ********************************/
  /*****************************************************************************************************************/
  ig = (int *)malloc(sizeof(int)*(n+1));
  jg = (int *)malloc(sizeof(int)*nnz);
  memcpy(ig ,ia, sizeof(int)*(n+1));
  memcpy(jg ,ja, sizeof(int)*nnz);

  
  if(unsym == 1)
    {
      ib = ig;
      jb = jg;
      numflag = 1;
      job = 0;
      PHIDAL_SymmetrizeMatrix(job, numflag, n, ib, jb, NULL, &ig, &jg, NULL);
      free(ib);
      free(jb);
    }
  /*****************************************************************************************************************/
  /*****************************************************************************************************************/


  /* Translate matrix into C numbering */
  /*fprintfv(5, stderr, "Convert FORTRAN to C indexing \n");*/
  CSR_Fnum2Cnum(ja, ia, n);
  CSR_Fnum2Cnum(jg, ig, n);
  numflag = 0;
  

  perm = (int *)malloc(sizeof(int)*n);
  iperm = (int *)malloc(sizeof(int)*n);

  /** Delete the self edge in the graph (METIS_NodeND need that)**/
  /** OIMBE Put THAT IN Perm2SizedDomain ?? **/
  PHIDAL_CsrDelDiag(numflag, n, ig, jg);

  /**** Compute the overlapped partition from the matrix reordering   *****/
  t1  = dwalltime(); 
  PHIDAL_Perm2SizedDomains(domsize, n, ig, jg, &ndom, &mapptr, &mapp, perm, iperm);
  t2  = dwalltime(); 
  fprintfv(5, stdout, "Compute the grid of subdomain in %g \n", t2-t1);
  fprintfv(5, stdout, "DOMSIZE = %d \n", domsize);
  fprintfv(5, stdout, "Found %d domains \n", ndom);


  {
    /*********************************************************/
    /*    DISPLAY SOME INFO ON THE OVERLAPPED PARTITION      */
    /*********************************************************/         
    
    int maxdom, mindom;
    REAL avgdom;


    fprintfv(5, stdout, "OVELAPED PARTITION INFO: \n");
    for(i=0;i<ndom;i++)
      fprintfv(5, stdout, "Domain %d = %d nodes \n", i, mapptr[i+1]-mapptr[i]);
    
    maxdom = 0;
    mindom = mapptr[1]-mapptr[0];
    avgdom = 0.0;
    for(i=0;i<ndom;i++)
      {
	avgdom += mapptr[i+1]-mapptr[i];
	if(mapptr[i+1]-mapptr[i] > maxdom)
	  maxdom = mapptr[i+1]-mapptr[i];
	if(mapptr[i+1]-mapptr[i] < mindom)
	  mindom = mapptr[i+1]-mapptr[i];
      }
    avgdom /= ndom;

    fprintfv(5, stdout, "MAX DOMAIN = %d \n", maxdom);
    fprintfv(5, stdout, "MIN DOMAIN = %d \n", mindom);
    fprintfv(5, stdout, "AVG DOMAIN = %g \n", avgdom);
    /*fprintfv(5, stdout, "IMBALANCE = %g \n", ((float)(maxdom-mindom))*100.0/mindom );*/
  }


  numflag = 0;

    
  /*******************************************************************/
  /*******************************************************************/
  /**                                                               **/
  /** COMPUTE THE HIERARCHICAL DECOMPOSITION AND LOCAL DATA         **/
  /**                                                               **/
  /*******************************************************************/
  /*******************************************************************/
  
  /** Compute the global hierarchical interface decomposition and the associated permutation **/ 
  PhidalHID_Init(&BL);

  t1  = dwalltime(); 
  PHIDAL_HierarchDecomp(phidaloptions.verbose, numflag, n, ig, jg, mapp, mapptr, ndom, &BL, perm, iperm);
  t2  = dwalltime(); 
  fprintfv(5, stdout, " Compute Phidal Hierarchical Decomposition in %g seconds \n\n", t2-t1);

  free(ig);
  free(jg);
  free(mapp);
  free(mapptr);


  


  /****************************************************************/
  /****************************************************************/
  /**                                                            **/
  /**   REODER THE MATRIX ACCORDING TO THE PHIDAL PERMUTATION    **/
  /**                                                            **/
  /****************************************************************/
  /****************************************************************/
  /*---------------------------------------------------------------------*
    |  Convert the matrix in SparRow and permute it                       |
    *---------------------------------------------------------------------*/
  fprintfv(5, stdout, "Permuting the matrix \n");

  mat = (csptr)malloc(sizeof(struct SparRow));
  initCS(mat, n);
  CSRcs(n, a, ja, ia, mat);
  



  /** DO NOT NEED THESE VECTORS ANYMORE **/
  free(a);
  free(ja);
  free(ia);
  
  /*** Permute the matrix according to the phidal ordering ***/
  /*dpermC(mat, perm);*/
  t1  = dwalltime(); 
  CS_Perm(mat, perm);
  t2  = dwalltime(); 
  fprintfv(5, stdout, " Permute the matrix in %g seconds \n\n", t2-t1);

  
  m = (PhidalMatrix *)malloc(sizeof(PhidalMatrix));
  
  t1  = dwalltime(); 
  PHIDAL_MatrixBuild(mat, m, &BL, &phidaloptions);
  t2  = dwalltime(); 
  fprintfv(5, stdout, " Build PhidalMatrix in %g seconds \n\n", t2-t1);

  fprintfv(5, stdout, "Number of NNZ in A = %ld \n", PhidalMatrix_NNZ(m));



  /********* Test multiply **********/
  x = (REAL *)malloc(sizeof(REAL)*mat->n);
  b = (REAL *)malloc(sizeof(REAL)*mat->n);

  for(i=0;i<mat->n;i++)
    x[i] = 1.0;


  {
    PhidalPrec P;
    REAL *r;

    bzero(b, sizeof(REAL)*mat->n);
    PHIDAL_MatVec(m, &BL, x, b);

    /*{
      struct SparRow csp;
      fprintfv(5, stderr, "1: Norm b = %g \n", norm2(b, m->dim1));
      fprintfv(5, stderr, "NNZ in m = %ld Norm = %g \n", (long)PhidalMatrix_NNZ(m), PhidalMatrix_NormFrob(m));
      PhidalMatrix2SparRow(1, m, &csp, &BL);
      fprintfv(5, stderr, "NNZ in csp = %ld Norm = %g \n", (long)CSnnz(&csp), CSnormFrob(&csp));
      bzero(b, sizeof(COEF)*m->dim1);
      if(m->symmetric == 1)
      CSC_matvec_add(&csp, x, b);
      else
      matvec(&csp, x, b);
      fprintfv(5, stderr, "2: Norm b = %g \n", norm2(b, csp.n));
      exit(0);
      }*/
    t1  = dwalltime();    
    PHIDAL_Precond(m, &P, &BL,  &phidaloptions);

    /*#if defined(ILUT_WHOLE_SCHUR) || defined(ILUT_SOLVE)
      dumpCS(0, stdout, P.csL);
      #else
      dumpCS(0, stdout, P.L->ra[0]);
      #endif*/

    t2  = dwalltime(); 
    fprintfv(5, stdout, " PHIDAL_Precond in %g seconds \n\n", t2-t1);
    PhidalPrec_Info(&P);

    /*fprintfv(5, stdout, "Factor L row density = %g \n", PhidalMatrix_RowDensity(P.L));
      fprintfv(5, stdout, "Factor U row density = %g \n", PhidalMatrix_RowDensity(P.U));*/
    fprintfv(5, stdout, " NNZ in A = %ld \n", PhidalMatrix_NNZ(m)); /*TODO : variable !*/
    fprintfv(5, stdout, "PhidalPrec NNZ = %ld \n", PhidalPrec_NNZ(&P)); /*TODO : variable !*/
    fprintfv(5, stdout, "Fill Ratio of Preconditioner = %g \n\n", ((REAL)PhidalPrec_NNZ(&P))/((REAL)PhidalMatrix_NNZ(m)));
    /*fprintfv(5, stdout, "Fill Ratio for symmetrized matrix = %g \n\n", ((REAL)2*PhidalPrec_NNZ(&P)-mat->n)/((REAL)2*PhidalMatrix_NNZ(m)-mat->n));*/
   
/* #define DEBUG_SCAL */
#ifdef DEBUG_SCAL
  /* fdumpPhidalMatrix("A.txt", m); */
  /*   PhidalMatrix L; */
    /* DBMatrix2PhidalMatrix(P->L, &L, option->locally_nbr, BL, 0/\*alloc : todo 1*\/, 0/\*diagonal non sÃ©parÃ©e*\/); */
    fdumpPhidalMatrix("Ascal.txt", m);
    fdumpPhidalMatrix("Lscal.txt", P.L);
  /*   exit(1); */
#endif



 
    bzero(b, sizeof(REAL)*mat->n);
    PHIDAL_MatVec(m, &BL , x, b);
    
    bzero(x, sizeof(REAL)*mat->n);
    t1  = dwalltime();
    PHIDAL_Solve(m, &P, &BL, &phidaloptions, b, x);
    t2  = dwalltime(); 
    fprintfv(5, stdout, "\n Solve in %g seconds \n", t2-t1);
    PhidalPrec_Clean(&P);
    
    r = (REAL *)malloc(sizeof(REAL)*m->dim1);
    memcpy(r, b, sizeof(REAL)*m->dim1);
    PHIDAL_MatVecSub(m, &BL, x, r);
    /*matvecz(mat, x, r, r);*/
    fprintfv(5, stdout, "Relative residual norm = %g \n", norm2(r, m->dim1)/norm2(b, m->dim1));
    free(r);
  }

  
  free(x);
  free(b);
  

  PhidalMatrix_Clean(m);
  free(m);
 
  free(perm);
  free(iperm);

  PhidalOptions_Clean(&phidaloptions);
  cleanCS(mat);
  free(mat);
  
  PhidalHID_Clean(&BL); 
  fprintfv(5, stdout, "END \n");

  return 0;
}

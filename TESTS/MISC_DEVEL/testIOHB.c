/* @authors J. GAIDAMOUR, P. HENON */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "math.h"

#include "localdefs.h"
#include "block.h" /* FLOAT */
#include "db_struct.h"
#include "phidal_sequential.h"

#include "base.h"

#include "iohb.h"

#define BUFLEN 200

#ifndef TYPE_REAL
#error Ne marche qu en DOUBLE
#endif

int main(int argc, char *argv[])
{
  int ierr, len;
    
  /* declarations related to Harwell-boeing format for reading the HB
     matrix. Second part is related to I/O parameters */
  char guesol[2], title[72], key[8], type[3];
  int nrhs, nc, n, nnz, tmp0,tmp,tmp2,tmp3;
  /* working array for reading matrix */
  REAL *rhstmp;

  /* MATRIX */
  REAL *a;
  int *ja, *ia;

  char *substr;

  char* matrix = argv[1];

  if(argc < 4)
    {
      fprintferr(stderr, "Illegal argument : testIOHB.ex <matrix_in> <matrix_out> <RSA/RUA>\n");
      exit(-1);
    }

  /************************************************************************************************************/
  /****************************************** READ THE MATRIX *************************************************/
  /************************************************************************************************************/

  CSRread(matrix, &n, &nnz, &ia, &ja, &a, NULL, NULL);

  /************************************************************************************************************/
  /****************************************** WRITE THE MATRIX ************************************************/
  /************************************************************************************************************/

  {
    /* not enough for MHD       */
    /*  char Ptrfmt[]="(13I6)"; */
    /*  char Indfmt[]="(16I5)"; */
    char Ptrfmt[]="(11I10)";
    char Indfmt[]="(11I10)";
    char Valfmt[]="(3E26.18)";
    char Rhsfmt[]="(3E26.18)";
    /****************************/
    writeHB_mat_REAL(argv[2], n, n, 
		       nnz, ia, ja, 
		       a, 0, NULL, 
		       NULL, NULL,
		       "TEST", "Test", argv[3], 
		       Ptrfmt,Indfmt,Valfmt,Rhsfmt, /* NULL, NULL, NULL, NULL,		      */
		       "FGN");
    /****************************/
  }

  fprintfv(5, stdout, "END \n");
  return 0;
}

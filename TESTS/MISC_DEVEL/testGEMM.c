/* @authors J. GAIDAMOUR, P. HENON */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "math.h"

#include "metis.h"
#include "localdefs.h"
#include "phidal_sequential.h"



#define ORDER_TO_DOMAINS
#define METIS_ORDER

#ifndef ORDER_TO_DOMAINS
#define DOMAIN_PART
#endif

#define BUFLEN 200


#ifndef TYPE_REAL
#error Ne marche qu en DOUBLE
#endif

extern int CSrealloc(csptr mat);
extern void reorder_interiors(csptr mat, BL_str *BL);
extern void GENERAL_setpar(char *filename, char *matrix, char *sfile_path,  int
	    *unsym, int *rsa, int *load_local_files, PhidalOptions *option) ;
extern void writeVecInt(FILE *fp, int *vec, int n);
extern void readVecInt(FILE *fp, int *vec, int n);
extern void checkCSR(int n, int *ia, int *ja, REAL *a, int num);

extern void compute_row_perm(csptr M, int *perm, REAL *rscale);

extern void patch_for_AQUILON(int n, int *ia, int *ja, REAL *a);
extern void diag_dominance(int baseval, int n, int *ia, int *ja, REAL *a); /** Use to test the convergence **/
extern void dumpLLt(FILE *fp, csptr L, csptr U);
extern void symmetric_scaling(int n, int *ia, int *ja, REAL *a, int numflag, REAL *Rowscal, REAL *Colscal);
extern void ascend_column_reorder(csptr mat);
extern void lower_triangular(csptr M, csptr L, int inarow);


int main(int argc, char *argv[])
{
  /*
   * num    --  working scalar
   * iov    --  overlap
   * unsym  --  symmetric pattern(0), nonsym pattern(1)
   */
  int iov, unsym, ierr,len;
  int load_local_files;  /** If set to 1 then genere all the local data file **/

  /* declarations related to Harwell-boeing format for reading the HB
     matri. Second part is related to I/O parameters */
  char guesol[2], title[72], key[8], type[3];
  int nrhs, nc, n, nnz, tmp0,tmp,tmp2,tmp3,job;
  long nnzL;

  /* working array for reading matrix */
  REAL *a,*rhstmp,  res, dnnz_pre, dgprec_nnz;
  chrono_t t1,t2,t3,t4;
  int *ja, *ia,*jb,*ib, *jg, *ig, volume,option[10],wgtflag,numflag;
  int nloc,*mapp,*mapptr, maxmp,
    *iwk,node,i1,i2,i,j,k; 
  csptr mat, L;
  int ndom;
  REAL *x;
  REAL *b;
  PhidalMatrix *m;


  /* working array for symmetrizing matrix */
  REAL *mc;
  int *jc, *ic;

  FILE *fp;






  /* 
   * buf is for read from 'inputs' file, 
   * matrix represents matrix name
   */
  char matrix[BUFLEN];
  char sfile_path[BUFLEN];
  char *substr;

  int *riord;
  int *perm, *iperm;
  int rsa;

  PHIDAL_PREC *PREC;
  PhidalOptions phidaloptions;
  int maxdomsize;

  if(argc < 2)
    {
      fprintf(stderr, "Illegal arguement : testORDERING <number of domains> \n");
      exit(-1);
    }

  ndom = atoi(argv[1]);


  PhidalOptions_Init(&phidaloptions);

  /** Read parameters from the input file **/
	       /*if(argc == 0)*/
    /** Default: read from file "input" **/
  GENERAL_setpar(NULL, matrix, sfile_path, &unsym, &rsa, &load_local_files, &phidaloptions);   
  /*else
    GENERAL_setpar(argv[1], matrix, sfile_path, &unsym, &rsa,  &load_local_files, &phidaloptions);   */
  

  
  /*{ 
    int *ja;
    REAL *ma;
    int *ja2;
    REAL *ma2;
    int g, h;
    int n1, n2;
    Queue heap;
    g = atoi(argv[1]);
    h = atoi(argv[2]);
    ja = (int *)malloc(sizeof(int)*g);
    ma = (REAL *)malloc(sizeof(REAL)*g);
    ja2 = (int *)malloc(sizeof(int)*g);
    ma2 = (REAL *)malloc(sizeof(REAL)*g);

    for(i=0;i<g;i++)
      {
	ja[i] = 2*i;
	if(i%2 == 0)
	  ma[i] = -(int) ((float)rand()/RAND_MAX * g);
	else
	  ma[i] =  (int) ((float)rand()/RAND_MAX * g);
      }
    memcpy(ja2, ja, sizeof(int)*g);
    memcpy(ma2, ma, sizeof(REAL)*g);

    for(i=0;i<g;i++)
      fprintfv(5, stdout, "%d:%g ", ja[i], ma[i]);
    fprintfv(5, stdout, "\n\n");
    queueInit(&heap, g);
    n1 = g;
    vec_filldrop(&n1, ja, ma, h, &heap);
    for(i=0;i<n1;i++)
      fprintfv(5, stdout, "%d:%g ", ja[i], ma[i]);
    fprintfv(5, stdout, "\n\n");

    n1 = g/2;
    n2 = g-n1;
    LU_filldrop(&n1, ja2, ma2, &n2, ja2+n1, ma2+n2, h, &heap);
    
    for(i=0;i<n1;i++)
      fprintfv(5, stdout, "%d:%g ", ja2[i], ma2[i]);
    fprintfv(5, stdout, " || " );
    for(i=0;i<n2;i++)
      fprintfv(5, stdout, "%d:%g ", ja2[i+g/2], ma2[i+g/2]);
    fprintfv(5, stdout, "\n\n");

    free(ja);
    free(ma);
    exit(0);
    }*/



  /************************************************************************************************************/
  /****************************************** READ THE MATRIX *************************************************/
  /************************************************************************************************************/

  CSRread(matrix, &n, &nnz, &ia, &ja, &a, NULL, NULL);
  assert(phidaloptions.symmetric == rsa);

  /************************ RSA matrix contains only the lower triangular part of the matrix ***************************/
  if(rsa == 1)
    {
      /** Symmetrize the matrix (in rsa we only have the lower triangular part) ****/
      ib = ia;
      jb = ja;
      b = a;
      numflag = 1;
      job = 2;
      PHIDAL_SymmetrizeMatrix(job, numflag, n, ib, jb, b, &ia, &ja, &a);
      nnz = 2*nnz-n;
      unsym = 0;
    }

  /*******************************************************************************************************************/
  /*******************************************************************************************************************/
  /*******************************************************************************************************************/
  fprintfv(5, stdout,"Matrix dimension is %d, Number of nonzeros is %d\n",n,nnz);



  /*****************************************************************************************************************/
  /***********************************Construct the symmetric graph G of the matrix ********************************/
  /*****************************************************************************************************************/
  ig = (int *)malloc(sizeof(int)*(n+1));
  jg = (int *)malloc(sizeof(int)*nnz);
  memcpy(ig ,ia, sizeof(int)*(n+1));
  memcpy(jg ,ja, sizeof(int)*nnz);

  
  if(unsym == 1)
    {
      
      ib = ig;
      jb = jg;
      numflag = 1;
      job = 0;
      PHIDAL_SymmetrizeMatrix(job, numflag, n, ib, jb, NULL, &ig, &jg, NULL);
    
    }
  /*****************************************************************************************************************/
  /*****************************************************************************************************************/


  /* Translate matrix into C numbering */
  fprintfv(5, stderr, "Convert FORTRAN to C indexing \n");
  CSR_Fnum2Cnum(ja, ia, n);
  CSR_Fnum2Cnum(jg, ig, n);
  numflag = 0;
  

#ifdef DOMAIN_PART /**** HOW TO CONSTRUCT AN OVERLAPPED PARTITION FROM A VERTEX PARTITION (e.g. that produces by METIS) *****/
  riord = (int *)malloc(n*sizeof(int));
  /********************************************************/
  /* Compute a  vertex-based partition  using METIS       */
  /* then compute an edge-based partition wich ovelapps   */
  /* on the vertex separator                              */
  /********************************************************/
  /* Compute an edge-partition using METIS */
  wgtflag = 0;
  option[0] = 0;
  if(ndom > 1)  /** METIS crash for one domain **/
    METIS_PartGraphVKway(&n, ig, jg, NULL, NULL, &wgtflag, &numflag, &ndom, option, &volume, riord);
  else 
    for(i = 0; i < n; i++) 
      riord[i] = 0;


  /*********************************************************************/
  /*********************************************************************/
  /**                                                                 **/
  /** Convert the vertex-based partition into an edge-based partition **/
  /** i.e. vertex separator is overlaped in the subdomain             **/ 
  /** (DO NOT USE THIS FUNCTION IF YOUR DOMAIN ARE ALREADY OVERLAPPED)**/
  /*********************************************************************/
  /*********************************************************************/
  PHIDAL_Partition2OverlappedPartition(numflag, ndom, n, ig, jg, riord, &mapp, &mapptr);


  /** Do not need riord anymore **/
  free(riord);
#endif /** DOMAIN_PART **/


  perm = (int *)malloc(sizeof(int)*n);
  iperm = (int *)malloc(sizeof(int)*n);


#ifdef ORDER_TO_DOMAINS /**** HOW TO CONSTRUCT AN OVERLAPPED PARTITION FROM A MATRIX REORDERING *****/
  /*** Compute the reordering to minimize fill-in using METIS-4.0 ***/
  option[0] = 0;

  /** Delete the self edge in the graph (METIS_NodeND need that)**/
  PHIDAL_CsrDelDiag(numflag, n, ig, jg);

#ifdef METIS_ORDER
  /**** Compute a matrix reordering that minimizes fill-in *******/
  METIS_NodeND(&n, ig, jg, &numflag, option, perm, iperm);

  /**** Compute the overlapped partition from the matrix reordering   *****/
  PHIDAL_Perm2OverlappedDomains(ndom, n, ig, jg, &mapptr, &mapp, perm, iperm);
  /*maxdomsize = atoi(argv[1]);
  PHIDAL_Perm2SizedDomains(maxdomsize, n, ig, jg, &ndom, &mapptr, &mapp, perm, iperm);
    fprintfv(5, stderr, "Found %d domains \n", ndom);*/
#else
  PHIDAL_ScotchOrder2OverlappedDomains(ndom, n, ig, jg, &mapptr,  &mapp, perm, iperm);
#endif 

#endif /*ORDER_TO_DOMAINS*/


  {
    /*********************************************************/
    /*    DISPLAY SOME INFO ON THE OVERLAPPED PARTITION      */
    /*********************************************************/         
    
    int maxdom, mindom;
    REAL avgdom;


    /* fprintfv(5, stdout, "PARTITION: \n");
    for(i=0;i<ndom;i++)
    fprintfv(5, stdout, "Domain %d = %d nodes \n", i, mapptr[i+1]-mapptr[i]);*/
    
    maxdom = 0;
    mindom = mapptr[1]-mapptr[0];
    avgdom = 0.0;
    for(i=0;i<ndom;i++)
      {
	avgdom += mapptr[i+1]-mapptr[i];
	if(mapptr[i+1]-mapptr[i] > maxdom)
	  maxdom = mapptr[i+1]-mapptr[i];
	if(mapptr[i+1]-mapptr[i] < mindom)
	  mindom = mapptr[i+1]-mapptr[i];
      }
    avgdom /= ndom;

    fprintfv(5, stdout, "MAX DOMAIN = %d \n", maxdom);
    fprintfv(5, stdout, "MIN DOMAIN = %d \n", mindom);
    fprintfv(5, stdout, "AVG DOMAIN = %g \n", avgdom);
    fprintfv(5, stdout, "IMBALANCE = %g \n", ((float)(maxdom-mindom))*100.0/mindom );
  }

  /***********************************************************/
  /*           REORDERING IN MULTI BLOCK DIAGONAL LEVEL      */
  /***********************************************************/ 

  PREC = (PHIDAL_PREC *)malloc(sizeof(PHIDAL_PREC));

  numflag = 0;

    
  /*******************************************************************/
  /*******************************************************************/
  /**                                                               **/
  /** COMPUTE THE HIERARCHICAL DECOMPOSITION AND LOCAL DATA         **/
  /**                                                               **/
  /*******************************************************************/
  /*******************************************************************/
  
  /** Compute the global hierarchical interface decomposition and the associated permutation **/ 

  PHIDAL_HierarchDecomp(1, numflag, n, ig, jg, mapp, mapptr, ndom, &(PREC->BL), perm, iperm);

  free(ig);
  free(jg);
  free(mapp);
  free(mapptr);


  /****************************************************************/
  /****************************************************************/
  /**                                                            **/
  /**   REODER THE MATRIX ACCORDING TO THE PHIDAL PERMUTATION    **/
  /**                                                            **/
  /****************************************************************/
  /****************************************************************/
  /*---------------------------------------------------------------------*
    |  Convert the matrix in SparRow and permute it                       |
    *---------------------------------------------------------------------*/
  fprintfv(5, stdout, "Permuting the matrix \n");

  mat = (csptr)malloc(sizeof(struct SparRow));
  CSRcs(n, a, ja, ia, mat);
  
  /** DO NOT NEED THESE VECTORS ANYMORE **/
  free(a);
  free(ja);
  free(ia);
  
  /*** Permute the matrix according to the phidal ordering ***/
  /*dpermC(mat, perm);*/
  CS_Perm(mat, perm);

  m = (PhidalMatrix *)malloc(sizeof(PhidalMatrix));
  PhidalMatrix_Build(0, "N", mat, m, &(PREC->BL));

#ifdef DOMAIN_PART
  /*** Reorder the interior domain to reduce the fill-in ***/
  fprintfv(5, stdout, "Reordering interior domains \n");
  PhidalMatrix_ReorderInterior(m, &(PREC->BL), perm, iperm);
  fprintfv(5, stdout, "... done\n");
#endif

  /*fprintfv(5, stdout, "Number of NNZ matrix %ld \n", CSnnz(mat));*/
  /*fprintfv(5, stdout, "Number of NNZ in phidal matrix %ld \n", PhidalMatrix_NNZ(m));*/

  /********* Test multiply **********/
  x = (REAL *)malloc(sizeof(REAL)*mat->n);
  b = (REAL *)malloc(sizeof(REAL)*mat->n);

  for(i=0;i<mat->n;i++)
    x[i] = 1.0;


  {
    csptr mat2;
    csptr matres;
    csptr *X;
    csptr *Y;
    

    ndom = atoi(argv[2]);

    mat2 = (csptr)malloc(sizeof(struct SparRow));
    matres = (csptr)malloc(sizeof(struct SparRow));
    initCS(mat2, mat->n);
    initCS(matres, mat->n);
    cscpy(mat, mat2);

    
    t1  = dwalltime();    
    ascend_column_reorder(mat2);
    t2  = dwalltime(); 
    fprintfv(5, stdout, "Ascend column in %g \n", t2-t1);

    cscpy(mat, matres);


    X = (csptr *)malloc(sizeof(csptr)*ndom);
    Y = (csptr *)malloc(sizeof(csptr)*ndom);
    
    for(i=0;i<ndom;i++)
      {
	X[i] = mat;
	Y[i] = mat2;
      }

    t1  = dwalltime();    
    CSRrowMultCSRcol(0.0, NULL, -1.0, ndom, -1.0, X, Y, matres, perm, iperm, b);
    t2  = dwalltime(); 
    fprintfv(5, stdout, "CSR  gemm in %g seconds \n", t2-t1);
    fprintfv(5, stderr, "NNZ %ld ratio %g \n", CSnnz(matres), ((float)CSnnz(matres))/CSnnz(mat));


    cleanCS(matres);
    initCS(matres, mat->n);
    cscpy(mat, matres);

    t1  = dwalltime(); 
    CSCrowMultCSRcol(0.0, NULL, -1.0, ndom, -1.0, X, Y, matres, mat->n, perm, iperm, b);   
    /*CSCxCSR_CSC_GEMMT(-1.0, mat, mat2, matres, mat->n, 0.0, NULL, -1.0, perm, iperm, b);*/
    t2  = dwalltime(); 
    fprintfv(5, stdout, "CSC  gemm in %g seconds \n", t2-t1);
    fprintfv(5, stderr, "NNZ %ld ratio %g \n", CSnnz(matres), ((float)CSnnz(matres))/CSnnz(mat));



    cleanCS(mat2);
    cleanCS(matres);
    free(mat2);
    free(matres);
    free(X);
    free(Y);
  }

  
  free(x);
  free(b);
  

  PhidalMatrix_Clean(m);
  free(m);
 
  free(perm);
  free(iperm);

  PhidalOptions_Clean(&phidaloptions);
  cleanCS(mat);
  free(mat);
  
  cleanBL(&(PREC->BL)); 
  free(PREC);
  fprintfv(5, stdout, "END testORDERING \n");

}




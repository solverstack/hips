/* @authors J. GAIDAMOUR, P. HENON */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#if defined(TYPE_REAL)
#define COEF double
#elif defined(TYPE_COMPLEX)
#include <complex.h>
#define COEF double complex
#else
#define COEF double
#error TYPE_REAL (or TYPE_COMPLEX) macro undefined
#endif

#include "io.h"

#include "phidal_common.h" /* tmp */

#define SYM 1
#define UNSYM 0

int compare(char* matfile1, char* matfile2, int sym) {
  int i,j;

  int n1, nnz1;
  int *ia1, *ja1;
  COEF *a1;

  int n2, nnz2;
  int *ia2, *ja2;
  COEF *a2;

  fprintf(stdout, "********************************************************************************\n");
  fprintf(stdout, "%s <-> %s\n", matfile1, matfile2);

  fprintf(stdout, "--\n");
  CSRread(matfile1, &n1, &nnz1, &ia1, &ja1, &a1, NULL/* &sym_pattern */, NULL/* &sym_matrix */);
  fprintf(stdout, "--\n");
  CSRread(matfile2, &n2, &nnz2, &ia2, &ja2, &a2, NULL/* &sym_pattern */, NULL/* &sym_matrix */);
  fprintf(stdout, "--\n");

  CSR_Fnum2Cnum(ja1, ia1, n1);
  CSR_Fnum2Cnum(ja2, ia2, n2);
  
  if (sym == SYM) {
    printf("SYM test ja<=i\n");
    /* sym in CSR (i = column indices) */
    for(i=0; i<n1; i++) {
      for(j=ia1[i]; j<ia1[i+1]; j++) {
	assert(ja1[j]>=i);
      }
    }
    
  }

  /* test : == */

  assert(n1 == n2);
  assert(nnz1 == nnz2);

  for(i=0; i<n1; i++) {
    assert(ia1[i] == ia2[i]);
  }

  for(i=0; i<nnz1; i++) {
    assert(ja1[i] == ja2[i]);
    assert(a1[i] == a2[i]);
  }

  free(ia1);
  free(ja1);
  free(a1);

  free(ia2);
  free(ja2);
  free(a2);

  fprintf(stdout, "********************************************************************************\n");
}

#define PATH "/home/henon/5....Soft_about_work/HIPS/TESTS/MATRICES/"

int main(int argc, char *argv[])
{  

  char matfile1[300];
  char matfile2[300];

#ifdef TYPE_REAL


#ifndef JG_MATRICE
  /* SPKIT >-<* IOHB */ 
  /* sym */
  sprintf(matfile1, "%d%s%s", DRIVER_SPKIT, PATH, "bcsstk16.rsa");
  sprintf(matfile2, "%d%s%s", DRIVER_IOHB,  PATH, "bcsstk16.rsa");
  compare(matfile1, matfile2, SYM);

  /* unsym */
  sprintf(matfile1, "%d%s%s", DRIVER_SPKIT, PATH, "orsirr_1.rua");
  sprintf(matfile2, "%d%s%s", DRIVER_IOHB,  PATH, "orsirr_1.rua");
  compare(matfile1, matfile2, UNSYM);


#else
  /* SPKIT >-<* IOHB */ 
  /* sym */
  sprintf(matfile1, "%d%s%s", DRIVER_SPKIT, PATH, "bcsstk18.rsa");
  sprintf(matfile2, "%d%s%s", DRIVER_IOHB,  PATH, "bcsstk18.rsa");
  compare(matfile1, matfile2, SYM);

  /* unsym */
  sprintf(matfile1, "%d%s%s", DRIVER_SPKIT, PATH, "sherman3.rua");
  sprintf(matfile2, "%d%s%s", DRIVER_IOHB,  PATH, "sherman3.rua");
  compare(matfile1, matfile2, UNSYM);

  /* IOHB >-<* IOMM */
  /* sym */
  sprintf(matfile1, "%d%s%s", DRIVER_IOHB,  PATH, "bcsstk18.rsa");
  sprintf(matfile2, "%d%s%s", DRIVER_IOMM,  PATH, "bcsstk18.mtx");
  compare(matfile1, matfile2, SYM);

  /* unsym */
  sprintf(matfile1, "%d%s%s", DRIVER_IOHB,  PATH, "sherman3.rua");
  sprintf(matfile2, "%d%s%s", DRIVER_IOMM,  PATH, "sherman3.mtx");
  compare(matfile1, matfile2, UNSYM);
#endif
#elif TYPE_COMPLEX

  /* test if Haltere is SYM CSR after loading */
  sprintf(matfile1, "%d%s%s", DRIVER_IOMM,  PATH, "Haltere.mm");
  compare(matfile1, matfile1, SYM);

#endif

  return 0;
}

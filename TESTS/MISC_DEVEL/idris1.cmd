# Nom arbitraire du travail LoadLeveler
# @ job_name = testPHIDAL.ex
# Type de travail
# @ job_type = serial
# Fichier de sortie standard du travail
# @ output = extra/$(job_name).$(jobid).out
# Fichier de sortie d'erreur du travail
# @ error = extra/$(job_name).$(jobid).err
# Temps CPU max. en seconde par processus
# @ cpu_limit = 18000
# Memoire max. utilisee par processus
# @ data_limit = 16gb
# @ resources = ConsumableCpus(1)
# @ queue
# Pour avoir l'echo des commandes
set -x
# Repertoire temporaire de travail
cd $TMPDIR
# La variable LOADL_STEP_INITDIR est automatiquement positionnee par
# LoadLeveler au repertoire dans lequel on tape la commande llsubmit
ln -s $LOADL_STEP_INITDIR/testPHIDAL.ex
ln -s $LOADL_STEP_INITDIR/inputs
ln -s $LOADL_STEP_INITDIR/thematrix
ln -s $LOADL_STEP_INITDIR/matr5
# La memoire STACK max. (defaut 4Mo) utilisee par
# les variables privees de chaque thread.
#export XLSMPOPTS=stack=65536000
# Variables d'environnement indiquant le nombre de threads OpenMP
# (indiquer une valeur identique a celle positionnee plus haut
# dans la directive threads_per_task).
#export OMP_NUM_THREADS=4
#export XLSMPOPTS=parthds=4
# Execution du programme parallele mixte
./testPHIDAL.ex 300


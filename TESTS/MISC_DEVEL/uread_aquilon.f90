        subroutine userread(matrix,len,job,n,nnz,a,ja,ia,nrhs,rhs,ierr)
        implicit none
        character (len=100):: matrix
        integer:: n, nnz, ierr, nrhs, ja(*), ia(*)
        real*8:: a(*), rhs(*)
        integer:: job, len
! =============================================
! User-defined read function:
! ---- reads the files provided by A. Soulaimani.
! Input:
!   matrix      (character) file containing names of the files with matrix input
!   len            (integer)   size of variable matrix
!   job            (integer)   flag: if 0 return the sizes only; if 1 read the
!                           matrix information.
!   nrhs        (integer) if nrhs is zero, then no RHS is read from file
! Output:
!   n            (integr) number of unknowns
!   nnz            (integr) number of nonzeros
!   (a,ja,ia)      matrix in CSR format, returned only if job=1
!   nrhs        (integer) number of rhs
!   rhs            (double) right-hand side
!   ierr      (integer) not equal zero if read error occurs
!
!===============================================
! local variables
!
      integer:: i, iounit=12
        character (len=200):: matrix1,reada,readja, readrhs
        character(LEN=100) :: FMTA,FMTJ ! répertoire de lecture des fic
! 3D----
        FMTA='(15E24.16)'
        FMTJ='(15I12)'
! 2D----
!        FMTA='(9E24.16)'
!        FMTJ='(9I12)'

        ierr=0
        matrix1(1:len)=matrix(1:len)
! open file stating matrix locations
        open(unit=iounit,form="FORMATTED",file=matrix1,status="unknown")
        read(iounit, *,err= 10) reada   !filename for real data of the m
        read(iounit, *,err= 10) readja  !filename for integer data
        if (nrhs .ne. 0 ) then
          read(iounit, *,err=101) readrhs !filename for rhs data of the
!-------- assume that only one rhs is provided, otherwise the rhs file
!-------- should be modified to have its first enry the number of RHSs
          nrhs =1
        end if
        goto 9
101     print *, "Error in getting RHS file -- no RHS available"
        nrhs = 0
!
! read integer matrix data
9       open(unit=iounit,form="FORMATTED",file=readja,status="unknown")
        read(iounit,*, err=11)n
        read(iounit,*, err=11)nnz
        if (job == 0 ) return ;
!-----------------------------------------------------------------------
        read(iounit, FMTJ(1:len_trim(FMTJ)), err=11)(ja(i),i=1,nnz)
        read(iounit,*,err= 11)n
        read(iounit,'(I12)',err= 11)(ia(i),i=1,n+1)
!
! read real values stored in unformated form
        open(unit=iounit,form="FORMATTED",file=reada,status="unknown")
        read(iounit, FMTA(1:len_trim(FMTA)), err=12) (a(i),i=1,nnz)
! read right-hand side stored in unformated form
        if ( nrhs .ne. 0 ) then
          open(unit=iounit,form="FORMATTED",file=readrhs, &
              status="unknown")
          read(iounit, '(E24.16)', err=13) (rhs(i),i=1,n)
          close(iounit)
        end if





        return
!
 10     print *, "Error in reading inputs file", matrix1
 11     print *, "Error in reading file with integers", readja
 12     print *, "Error in reading file with real values", reada
 13     print *, "Error in reading file with rhs values", readrhs
        ierr = 1
        return
        end subroutine userread

/* @authors J. GAIDAMOUR, P. HENON */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "base.h"
#include "io.h"
#include "localdefs.h"
#include "phidal_sequential.h"


#ifndef SCOTCH_PART

#define SUPPRESS_ZERO 
#include "metis.h"

#else /** Scotch **/

#define restrict
#include "scotch.h"



#endif




#define BUFLEN 200
#define MATLAB_OUTPUT  /* Save partition info that can be used for matlab experiments */
/*#define DUMP_MTX  */ /** To dump the matrix in i,j value format **/
/*#define ALL_READ*/ /* If set all the processors read the matrix on disk */

/*#define WRITE_PERM */ /** To save the permutation vector **/
/*#define READ_PERM*/ /** To load the permutation vector **/


#ifndef TYPE_REAL
#error Ne marche qu en DOUBLE
#endif

extern REAL norm2(REAL *x, int n);
extern long SF_GSurow(csptr A, int level, csptr P);
extern int CSRcs_InaRow(int n, REAL *a, int *ja, int *ia, csptr bmat);
extern long SF_level(int job, csptr A, int level, csptr P);
extern int LDLk(csptr L, REAL normA, REAL epsilon);
extern void sort_row(csptr P);
extern void set_coeff(csptr A, csptr B);
extern void CSC_Lsol(int diag, csptr mata, REAL *b, REAL *x);
extern void CSC_Ltsol(int diag, csptr mata, REAL *b, REAL *x);
extern void matvec(csptr mata, REAL *x, REAL *y);
extern void print_mat(csptr A);
#ifndef SCOTCH_PART
extern void METIS_NodeND(int *, idxtype *, idxtype *, int *, int *, idxtype *, idxtype *); 
#endif
extern void ascend_column_reorder(csptr mat);
extern void lower_triangular(csptr M, csptr L, int inarow);
extern int HIPS_Fgmresd_CS_D1(int verbose, REAL tol, int itmax, 
			      csptr A, csptr L,
			      PhidalOptions *option, 
			      COEF *rhs, COEF *x, FILE *fp);


extern void writeVec(FILE *fp, int *vec, int n);
extern void readVec(FILE *fp, int *vec, int n);
extern void checkCSR(int n, int *ia, int *ja, REAL *a, int num);


extern void patch_for_AQUILON(int n, int *ia, int *ja, REAL *a);
extern void diag_dominance(int baseval, int n, int *ia, int *ja, REAL *a); /** Use to test the convergence **/
extern void dumpLLt(FILE *fp, csptr L, csptr U);
extern void symmetric_scaling(int n, int *ia, int *ja, REAL *a, int numflag, REAL *Rowscal, REAL *Colscal);
extern REAL CSC_LDLt_OPC(csptr mat);




int main(int argc, char *argv[])
{
  /*
   * num    --  working scalar
   * unsym  --  symmetric pattern(0), nonsym pattern(1)
   */
  int proc_id, unsym, ierr;

  /* declarations related to Harwell-boeing format for reading the HB
     matri. Second part is related to I/O parameters */
  int n, nnz, job;


  /* working array for reading matrix */
  REAL *a,*b, dgnnz;
  chrono_t t1,t2;
  int *ja, *ia,*jb,*ib,wgtflag,numflag;
  REAL *x, *r;
  int /* nproc, */ *iwk,/* i1, */i,j; 

  /* working array for symmetryzing matrix */
  REAL *mc;
  int *jc, *ic;

  /*
   * dm      --  distributed matrix object
   * prepar  --  structure for preconditioner
   * ipar    --  structure for iteration
   */

  /* 
   * buf is for read from 'inputs' file, 
   * matrix represents matrix name
   */
  char matrix[BUFLEN];
  char sfile_path[BUFLEN];

  int *perm, *iperm;
  int ind;
  int rsa;
  csptr mat, P;
  long nnzL;
  int levelk;
  int metisoptions[10];


  PhidalOptions phidaloptions;
  PhidalOptions* option= &phidaloptions;
  REAL opc;

  /* extract the number of processors and processor ID */
  proc_id = 0;


  /** Init MPI environment **/
  /*MPI_Init(&argc, &argv);
  MPI_Comm_size(MPI_COMM_WORLD, &nproc);
  MPI_Comm_rank(MPI_COMM_WORLD, &proc_id);*/


  PhidalOptions_Init(&phidaloptions);

  /** Read parameters from the input file **/
  if(argc == 0)
    /** Default: read from file "input" **/
    GENERAL_setpar(NULL, matrix, sfile_path, &unsym, &rsa, &phidaloptions);   
  else
    GENERAL_setpar(argv[1], matrix, sfile_path, &unsym, &rsa,  &phidaloptions);   

  
  /*** For rsa to 0 ****/
  /*rsa = 0;
    unsym = 0;*/



  /*-----------------------------------------------------------------/
  /   Processor 0 read the matrix, compute a partition etc..         /
  /-----------------------------------------------------------------*/
  if(proc_id == 0)
    {

      CSRread(matrix, &n, &nnz, &ia, &ja, &a, NULL, NULL);
      assert(phidaloptions.symmetric == rsa);

#ifdef SUPPRESS_ZERO
      /********************** SUPPRESS THE ZERO IN THE CSR MATRIX *****/
      numflag = 1;
      i = nnz;
      nnz = CSR_SuppressZeros(numflag, n, ia, ja, a);
      fprintfv(5, stdout, "Zeros removed from the matrix = %d \n", i-nnz); 
#endif


      if (proc_id == 0){
	if(rsa == 0)
	  fprintfv(5, stdout,"Matrix dimension is %d, Number of nonzeros is %d\n",n,nnz)
	else
	  fprintfv(5, stdout,"Matrix dimension is %d, Number of nonzeros is %d\n",n,2*nnz-n);
      }
      dgnnz = (REAL)nnz;

      /*#define TOTO*/
#ifdef TOTO
      /***************** FOR TEST WITH A VERY SMALL MATRIX ****************************/
      free(ia);
      free(ja);
      free(a);
      n = 3;
      nnz = 5;
      ia = (int *) malloc((n+1)*sizeof(int));
      ja = (int *) malloc(nnz*sizeof(int));
      a = (REAL *) malloc(nnz*sizeof(REAL));
      ia[0] = 1;
      ja[0] = 1;
      a[0]  = 2.0;
      ja[1] = 2;
      a[1] = 1.0;
      ja[2] = 3;
      a[2] = 1.0;
      
      ia[1] = 4;
      ja[3] = 2;
      a[3] = 2.0;
      
      ia[2] = 5;
      ja[4] = 3;
      a[4] = 2.0;
      
      ia[3] = 6;
     
     
      /*********************************************************************************/
#endif




      /*-------------------- non symmetric pattern case */
      if(unsym == 1) {
	/*------------------------------------------------/
	  / The symmetric pattern of A will be stored in    /
	  /  ic, jc                                         /
	  /------------------------------------------------*/
    
	if(proc_id == 0)
	  fprintfv(5, stdout, "Symmetrize the matrix patern \n");
	iwk = (int *)malloc(n*sizeof(int));
	b = (REAL *)malloc(nnz*sizeof(REAL));
	jb = (int *)malloc(nnz*sizeof(int));
	ib = (int *)malloc((n+1)*sizeof(int));
    
	mc = (REAL *)malloc(2*nnz*sizeof(REAL));
	jc = (int *)malloc(2*nnz*sizeof(int));
	ic = (int *)malloc((n+1)*sizeof(int));

	if(rsa == 1)
	  job = 1;
	else
	  job = 0;

	/* i = 1; SPKIT (Fortran) */
	/* csrcsc(&n, &job, &i, a, ja, ia, b, jb, ib); SPKIT (Fortran) */
	/*Fcsrcsc(n, job, 1, a, ja, ia, b, jb, ib);*/
	csr2csc(n, job, a, ja, ia, b, jb, ib);

	if(rsa == 0)
	  for(i = 0; i < nnz; i++) {
	    b[i] = 0.0;
	  }
	else
	  {
	    if(proc_id==0)
	      fprintfv(5, stdout, "Matrix in RSA FORMAT \n");

	    /** Annul the diagonal in B **/
	    for(i=0;i<n;i++)
	      for(j=ib[i]-1; j<ib[i+1]-1;j++)
		if(jb[j] == i+1)
		  b[j] = 0.0;
	  }

	/* job = 1; */
	/* i1 = 2*nnz; */
	/* 	aplb(&n, &n, &job, a, ja, ia, b, jb, ib, mc, jc, ic, &i1, iwk, */
	/* 	     &ierr); SPKIT (Fortran) */
	ierr = aplb(n, n, 1, a, ja, ia, b, jb, ib, mc, jc, ic, 2*nnz, iwk);

	nnz = ic[n]-1;
	ja = (int *)realloc(ja, nnz*sizeof(int));
	a = (REAL *)realloc(a, nnz*sizeof(REAL));
	memcpy(ia, ic, (n+1)*sizeof(int));
	memcpy(ja, jc, nnz*sizeof(int));
	memcpy(a, mc, nnz*sizeof(REAL));
	free(b); free(jb); free(ib);
	free(mc); free(jc); free(ic);
	free(iwk);
      }
      
      /* non-overlapping */
      wgtflag = 0;
      numflag = 1;



      /********************************************************/
      /* Compute a  vertex-based partition                    */
      /* then compute an edge-based partition wich ovelapps   */
      /* on the vertex separator                              */
      /********************************************************/

      /* Translate matrix into C numbering */
      if(proc_id == 0)
	fprintfv(5, stderr, "Convert FORTRAN to C indexing \n");
      CSR_Fnum2Cnum(ja, ia, n);
      numflag = 0;

      

     
      /*---------------------------------------------------------------------*
      |  Convert the matrix in SparRow and permute it                       |
      *---------------------------------------------------------------------*/
      mat = (csptr)malloc(sizeof(SparMat));
      initCS(mat, n);
      CSRcs_InaRow(n, a, ja, ia, mat);

      /************************************************/
      /** Compute the permutation to reduce fill-in ***/
      /************************************************/
      /** Retrieve the diagonal element for the partitionner **/
      ind = 0;
      for(i=0;i<mat->n;i++)
	{
	  ia[i] = ind;
	  for(j=0;j< mat->nnzrow[i];j++)
	    if(mat->ja[i][j] != i) /* We do not store the diagonale (if stored partitioner crash !) */
	      ja[ind++] = mat->ja[i][j];
	}
      ia[ mat->n ] = ind;
      
      
      perm = (int *)malloc(sizeof(int)*mat->n);
      iperm = (int *)malloc(sizeof(int)*mat->n);
#ifndef SCOTCH_PART
      metisoptions[0] = 0;
     
      METIS_NodeND(&n, ia, ja, &numflag, metisoptions, perm, iperm);
#else /** SCOTCH **/

#ifdef ORDER_SCOTCH
      {
	Graph               grafmesh;                   /* Mesh graph               */
	
	Order               ordemesh;                   /* Mesh graph ordering      */ 
	graphInit (&grafmesh);  
	graphBuildGraph(&grafmesh, 0, n, 2*nnz-2*n, ia, NULL, ja);
	graphBase(&grafmesh,0);
	orderInit(&ordemesh);
	printfv(5, "Nouvelle strategie generalisee\n");  
	/*orderGraphStrat(&ordemesh, &grafmesh, "c{rat=0.7,cpr=n{sep=/(vert>120)?(m{vert=100,low=h{pass=10},asc=f{bal=0.2}}|m{vert=100,low=h{pass=10},asc=f{bal=0.2}}|g{pass=6});,ole=h{cmin=15,cmax=100000,frat=0.08},ose=g},unc=n{sep=/(vert>120)?((m{vert=100,low=h{pass=10},asc=f{bal=0.2}})|m{vert=100,low=h{pass=10},asc=f{bal=0.2}}|g{pass=6});,ole=h{cmin=15,cmax=100000,frat=0.08},ose=g}}");*/
	orderGraphStrat(&ordemesh, &grafmesh, "c{rat=0.7,cpr=n{sep=/(vert>120)?m{vert=100,low=h{pass=10},asc=f{bal=0.2}}|m{vert=100,low=h{pass=10},asc=f{bal=0.2}};,ole=f{cmin=1,cmax=100000,frat=0.10},ose=g},unc=n{sep=/(vert>120)?(m{vert=100,low=h{pass=10},asc=f{bal=0.2}})|m{vert=100,low=h{pass=10},asc=f{bal=0.2}};,ole=f{cmin=1,cmax=100000,frat=0.10},ose=g}}");
	orderBase(&ordemesh,0);
	graphExit(&grafmesh);
	
	CS_Perm(mat,ordemesh.permtab);

	orderExit(&ordemesh);
      }
#else
       {
	 int cblknbr;
	 int *rangtab;
	 int *treetab;
	 char STRAT[400];

	 SCOTCH_Graph        grafdat;
	 SCOTCH_Strat        stratdat;
	 SCOTCH_stratInit (&stratdat);

	 rangtab = (int *)malloc(sizeof(int)*mat->n);
	 treetab = (int *)malloc(sizeof(int)*mat->n);
	 
	 
	 
	 /** Chaine de PAstix **/
	 /*sprintfv(5, STRAT, "c{rat=0.7,cpr=n{sep=/(vert>120)?m{vert=100,low=h{pass=10},asc=f{bal=0.2}}|m{vert=100,low=h{pass=10},asc=f{bal=0.2}};,ole=f{cmin=1,cmax=100000,frat=0.10},ose=g},unc=n{sep=/(vert>120)?(m{vert=100,low=h{pass=10},asc=f{bal=0.2}})|m{vert=100,low=h{pass=10},asc=f{bal=0.2}};,ole=f{cmin=1,cmax=100000,frat=0.10},ose=g}}");*/
	 sprintfv(5, STRAT, "c{rat=0.7,cpr=n{sep=/(vert>120)?m{vert=100,low=h{pass=10},asc=f{bal=0.2}}|m{vert=100,low=h{pass=10},asc=f{bal=0.2}};,ole=f{cmin=1,cmax=100000,frat=0.100000},ose=g},unc=n{sep=/(vert>120)?(m{vert=100,low=h{pass=10},asc=f{bal=0.2}})|m{vert=100,low=h{pass=10},asc=f{bal=0.2}};,ole=f{cmin=1,cmax=100000,frat=0.100000},ose=g}}");

	 
	 SCOTCH_stratGraphOrder (&stratdat, STRAT);
	 
	 SCOTCH_graphInit  (&grafdat);
	 SCOTCH_graphBuild (&grafdat, 0, n, ia, NULL, NULL, NULL, ia[n], ja, NULL);
	 
	 /*  fprintfv(5, stderr, "NDOM %ld \n", (long)ndom);*/
	 /** Do not free verttab and edgetab before SCOTCH_graphOrder **/
	 SCOTCH_graphOrder (&grafdat, &stratdat, iperm, perm, &cblknbr, rangtab, treetab);
	 free(rangtab);
	 free(treetab);

	 SCOTCH_graphExit (&grafdat);
	 SCOTCH_stratExit (&stratdat);
       }
#endif
#endif
      /*sort_row(mat);*/
      /** DO NOT NEED THESE VECTORS ANYMORE **/
      free(ja);
      free(ia);
      free(a);
      
      /*{
	x = (REAL *)malloc(sizeof(REAL)*n);
	b = (REAL *)malloc(sizeof(REAL)*n); 
	for(i=0;i<n;i++)
	  x[i] = 1.0;
	matvec(mat, x, b);      
	fprintfv(5, stdout, "Norm b avant reordering %g \n", norm2(b, n));
	free(x);
	free(b);
	}*/


      /************************/
      /** Permute the matrix **/
      /************************/

      /*dpermC(mat, iperm);*/
      CS_Perm(mat, iperm);

      free(perm);
      free(iperm);

      ascend_column_reorder(mat);

      /******************************************************/
      /**** Compute the symbolic factorization of level k ***/
      /******************************************************/
      P = (csptr)malloc(sizeof(SparMat));
      initCS(P, n);
      
      levelk = atoi(argv[1]);

      /*fprintfv(5, stdout, "Process the Symbolic Factorization for a fill level of %d ...\n", levelk);
      t1 = dwalltime();
      nnzL = SF_GSurow_nnz(mat, levelk, P);
      t2 = dwalltime();
      fprintfv(5, stdout, " Done in %g seconds \n", t2-t1);*/


      fprintfv(5, stdout, "Process the Symbolic Factorization for a fill level of %d ...\n", levelk);
      t1 = dwalltime();
      nnzL = SF_level(0, mat, levelk, P);
      t2 = dwalltime();
      fprintfv(5, stdout, " Done in %g seconds \n", t2-t1);

      fprintfv(5, stdout, "Set the coeffecients...\n");
      nnzL = SF_level(1, mat, levelk, P);

      /*nnzL = SF_GSurow(mat, levelk, P);*/
      
      fprintfv(5, stdout, "nnzA %ld nnzP %ld ; RATIO = %g \n", (long)( CSnnz(mat) + n)/2, (long)CSnnz(P), (REAL)CSnnz(P)/(REAL)( (CSnnz(mat)+n)/2.0 ));
      /*fprintfv(5, stdout, "Number of NNZ in L will be %ld \n", (long)nnzL);*/
      
      opc =  CSC_LDLt_OPC(P);
      fprintfv(5, stdout, " \n Number of OPC %g \n \n", opc);

      
    
      /** Sort the rows of the symbolic matrix */
      sort_row(P);
      /** Copy the coefficients of A into the allocated matrix **/
      set_coeff(mat, P);
      fprintfv(5, stdout, "done \n");

      
      /*** Get the lower triangular part of the matrix ****/
      /*A = (csptr)malloc(sizeof(SparMat));
      initCS(A, n);
      lower_triangular(mat, A, 1);*/

     

      

      
      /*cleanCS(A);*/

      /********************************************/
      /** Compute the incomplete factorization   **/
      /********************************************/
      /*fprintfv(5, stdout, "MATRIX P \n");
	print_mat(P);*/

      fprintfv(5, stdout, "Compute the numerical factorization... \n"); 
      t1 = dwalltime();
      LDLk(P, 0, 0.0001);
      t2 = dwalltime();
      fprintfv(5, stdout, " Done in %g seconds \n", t2-t1);


      /*fprintfv(5, stdout, "MATRIX L \n");
	print_mat(P);*/
      /********************************************/
      /* Compute a R.H.S                          */
      /********************************************/
      x = (REAL *)malloc(sizeof(REAL)*n);
      b = (REAL *)malloc(sizeof(REAL)*n); 
      for(i=0;i<n;i++)
	/*b[i] = 1.0;*/
	x[i] = 1.0;
      matvec(mat, x, b);



      /**********************/
      /* Solve with gmres   */
      /**********************/
      bzero(x, sizeof(REAL)*n);
      t1 = dwalltime();
      HIPS_Fgmresd_CS_D1(phidaloptions.verbose, phidaloptions.tol, phidaloptions.itmax, 
			 mat, P,  &phidaloptions, b, x, stdout);
      t2 = dwalltime();
      
      fprintfv(5, stdout, " Total Solve in %g seconds \n", t2-t1);
      
      r = (REAL *)malloc(sizeof(REAL)*mat->n);
      memcpy(r, b, sizeof(REAL)*mat->n);
      matvecz(mat, x, r, r);
      fprintfv(5, stdout, "Relative residual norm = %g \n", norm2(r, mat->n)/norm2(b, mat->n));
      free(r);


      free(x);
      free(b);
      cleanCS(P);
      free(P);
      cleanCS(mat);
      free(mat);
      PhidalOptions_Clean(&phidaloptions);
    }
  
  return 0;
}

#!/bin/bash

# Lancement automatique de jobs dans la file TEST

# job-todo contient les prochains jobs � lancer
# job-done contient les jobs d�j� execut�s ou en cours

while [ `cat job-todo | wc -l` -ne "0" ]
  do
  
  if [ `bjobs 2>&1 | wc -l` -ne 3 ]
      then
      
      # Remove core dump
      find . -name "core.*" -exec rm {} \;
      
      echo -n "Submit : ";
      
      cmd=`head -n1 job-todo`
      dn=`dirname $cmd`
      bn=`basename $cmd`
      
      echo $cmd
      
      cd $dn
      bsub < $bn
      cd -
      
      tail -n +2 job-todo > job-todo2
      mv job-todo2 job-todo
      
      echo $cmd >> job-done
      
  else
      
      echo "Wait ...";
      sleep 180
      
  fi
  
done;

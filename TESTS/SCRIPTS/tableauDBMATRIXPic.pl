#!/usr/bin/perl -w


     sub getnum {
        use POSIX qw(strtod);
        my $str = shift;
        $str =~ s/^\s+//;
        $str =~ s/\s+$//;
        $! = 0;
        my($num, $unparsed) = strtod($str);
        if (($str eq '') || ($unparsed != 0) || $!) {
            return undef;
        } else {
            return $num;
        } 
    }

sub round {
    my($number) = shift;
    $number = $number * 100;
    return (int($number + .5 * ($number <=> 0))/100);
}

#$clef1 = 'LEVEL';
$clef1 = 'STRAT';
$endrun = "END";
$fillkey = "Fill Ratio";
$solvekey = "Solve in";
$factokey = "DB_Precond in";
$iterkey = "outer iterations";
#$innerkey = "smoothing iter";
$domsizekey = "DOMSIZE";
$domnbrkey = "Found";
#$levelnbrkey = "number of levels";
$levelnbrkey = "Level ";
$connectorkey = "number of connectors";
$indimkey = "Level 0 =";
$dimkey = "Matrix dimension";
$nnzAkey = "Number of nonzeros";

$pickey = "Pic : NNZ Pic Minimized : ";
$picbeforekey = "Before GEMM : ";
$picgemmkey = "Pic : NNZ Pic :";
$picafterkey = ", After :";

$fichhead = '\documentclass[10pt,a4wide]{report}'."\n".
    '\usepackage[latin1]{inputenc}'."\n".
    '\usepackage{amsmath}'."\n".
    '\usepackage{amsfonts}'."\n".
    '\usepackage{amssymb}'."\n".
    '\usepackage{epsf}'."\n".
    '\usepackage{here}'."\n".
    '\title{Experiments}'."\n".
    '\begin{document}'."\n".
    '\maketitle'."\n";

$fichfoot = '\end{document}'."\n";

#$tabhead ='\begin{tabular}{|c||c|c|c|c|c||c|c|c|c|} \hline'."\n".
#           "domsize   &  Facto  & Solve   & Total  & Iter  & Fill   & Pic   & before GEMM & in GEMM & after GEMM\\\\ \n" .
#           "          &  (sec.) &  (sec.) & (sec)  &       &  ratio &       & \$L+EU^{-1}\$ & \$L+EU^{-1}+\\tilde{S}\$ & \$L+L_S+S\$ \\\\ \n \\hline \n \\hline \n";

$tabhead ='\begin{tabular}{|c||c|c|c|c|} \hline'."\n".
           "domsize   &  Pic   & before GEMM & in GEMM & after GEMM\\\\ \n" .
           "          &        & \$L+EU^{-1}\$ & \$L+EU^{-1}+\\tilde{S}\$ & \$L+L_S+S\$ \\\\ \n \\hline \n \\hline \n";


#$tabfoot = "\n".'\end{tabular}'."\n".'\end{minipage}'."\n".'\hspace{0.1cm}~'."\n\n";
$tabfoot = "\n".'\end{tabular}'."\n"."\n\n";

open(F, ">Table.tex");
print F $fichhead;

@listerep = `ls -d RESULT*`;



#boucle sur les RESULT_ 
foreach $rep (@listerep)
{
    $Gtabhead1 = '\begin{tabular}{|c||c|c|c|c|';
    $Gtabhead2 = "Domsize    &  number of & number of & number of & dim(S)/n  ";
    $Gtabhead3 = "parameter  &  domains   &  levels   & connectors  &  (percent)   ";
    
    $Ftabhead1 = '\begin{tabular}{|c||';
    $Ftabhead2 = "Domsize    ";
    $Ftabhead3 = "parameter  ";


    $Wtabhead1 = '\begin{tabular}{|c||c|';
    $Wtabhead2 = "Domsize    & Method NOREC  ";
    $Wtabhead3 = "parameter  & opc         ";
    
    


    chop $rep;

    $matname = $rep;
    $matname =~ s/RESULT_//;
    $matname =~ s/_.*//;
    
    $rsa = 0;


    $rep =~ /LOC_(.*)_/;
    $loclv = $1;
    print F '\section{'."Cas test $matname locally = $loclv"."\}\n\n\n";

    $dir = `pwd`;
    chop $dir;

    chdir $rep;
    print "Traite repertoire $rep \n";

#    @listesubrep = `ls -d ${clef1}_NOREC`;
#    push @listesubrep ,`ls -d ${clef1}_[0-9]`;
    
    @listesubrep = `ls -d ${clef1}_[0-9]`;

    if(@listesubrep == 0)
    {
	print "$rep est vide : ON l'OUBLIE ! \n";
        system "ls -d ${clef1}_[0-9]";
	next;
    }

    
    my %tt;
    my %vv;
    my %uu;
    my %itnorec;

    $flag = 0;

    #### Boucle sur les sous-repertoires STRAT  #####
    foreach $subrep (@listesubrep)
    {

	chop $subrep;
	$strat = $subrep;
	$strat =~ s/${clef1}_//;

	$subdir =  `pwd`;
	chop $subdir;
	chdir $subrep;
	print "Traite $subrep \n";
	
	@files = `ls ${matname}_*`;
	
	if(@files == 0)
	{
	    print "$subrep est vide : ON l'OUBLIE ! \n";
	    next;
	}
	
	$Ftabhead1 = $Ftabhead1.'c|';
	$Ftabhead2 = $Ftabhead2."& Method $strat ";
	$Ftabhead3 = $Ftabhead3."& Fill ";

	
	if( $strat !~ /NOREC/)
	{
	    $Wtabhead1 = $Wtabhead1.'c|';
	    $Wtabhead2 = $Wtabhead2."& Method $strat ";
	    $Wtabhead3 = $Wtabhead3."&  \\\#iter eq.   ";
	}

	my %m;

	foreach $f (@files)
	{
	    chop $f;

	    open (M, "$f");
	    @st = <M>;
	    close M;
	    $fact = "0.0";

	    
	    $L0 = 0;
	    $U0 = 0;
	    $D0 = 0;
	   
	    $E0 = 0;
	    $F0 = 0;
	    $S0 = 0;
	    $C0 = 0;

	    $L1 = 0;
	    $U1 = 0;
	    $D1 = 0;


	    $pic=-1;
	    $picbefore=-1;
	    $picgemm=-1;
	    $picafter=-1;

	    foreach (@st)
	    {
		
		$fact = $3 if /($factokey)([^\d]*)(\d*\.\d*)/;
		$fill = $3 if /($fillkey)([^\d]*)(\d*\.\d*)/;
		$solve = $3 if /($solvekey)([^\d]*)(\d*\.\d*)/;
		$iter = $3 if /($iterkey)([^\d]*)(\d*)/;
#		$inner = $3 if /($innerkey)([^\d]*)(\d*)/;
		$np = $3 if /($domsizekey)([^\d]*)(\d*)/;

		$levelnbr  = $3 if /($levelnbrkey)([^\d]*)(\d*)/;
		$connectnbr = $3 if /($connectorkey)([^\d]*)(\d*)/;
		$nnzA = $3 if /($nnzAkey)([^\d]*)(\d*)/;
		$domnbr = $3 if /($domnbrkey)([^\d]*)(\d*)/;
		$indim = $3 if /($indimkey)([^\d]*)(\d*)/;
		$dim = $3 if /($dimkey)([^\d]*)(\d*)/;

		$pic = $3 if /($pickey)([^\d]*)(\d*)/;
		$picbefore = $3 if /($picbeforekey)([^\d]*)(\d*)/;
		$picgemm = $3 if /($picgemmkey)([^\d]*)(\d*)/;
		$picafter = $3 if /($picafterkey)([^\d]*)(\d*)/;

		if( /RSA\sformat/ )
		{
		    $rsa = 1;
		}

		$L0 = $3 if /(L\(0\))([^\d]*)(\d*)/;
		$U0 = $3 if /(U\(0\))([^\d]*)(\d*)/;
		$D0 = $3 if /(D\(0\))([^\d]*)(\d*)/;
		$E0 = $3 if /(E\(0\))([^\d]*)(\d*)/;
		$F0 = $3 if /(F\(0\))([^\d]*)(\d*)/;
		$S0 = $3 if /(S\(0\))([^\d]*)(\d*)/;
		$C0 = $3 if /(C\(0\))([^\d]*)(\d*)/;

		$L1 = $3 if /(L\(1\))([^\d]*)(\d*)/;
		$U1 = $3 if /(U\(1\))([^\d]*)(\d*)/;
		$D1 = $3 if /(D\(1\))([^\d]*)(\d*)/;


		if(/$endrun/)
		{
#		    if($rsa == 1) {
			$nnzA = getnum($nnzA);
#		    }
		    $fill = round($fill);
		    $pic = round(getnum($pic)/$nnzA);
		    $picbefore = round(getnum($picbefore)/$nnzA);
		    $picgemm = round(getnum($picgemm)/$nnzA);
		    $picafter = round(getnum($picafter)/$nnzA);


		    if($fact == 0)
		    {
			print "$f : FACT NULL \n";
		    }
		    $interf = (($dim-$indim) / $dim)* 100.0;
#		    $interf =~ /(\d*\.\d*)/;
#		    $interf = $1;

		    $total = $fact + $solve;
		    $interf = sprintf("%.2f", $interf);
		    
		    $fact = sprintf("%.2f", $fact);
		    $solve = sprintf("%.2f", $solve);
		    $total = sprintf("%.2f", $total);

		    if($strat =~ /NOREC/ || $strat =~ /0/)
		    {
#			$m{$np} = "$fact & $solve & $total & $iter & $fill & $pic & $picbefore & $picgemm & $picafter\\\\";
			$m{$np} = "$pic & $picbefore & $picgemm & $picafter\\\\";
		    }
		    else
		    {
#			$m{$np} = "$fact & $solve & $total & $inner & $fill & $pic & $picbefore & $picgemm & $picafter\\\\";
			$m{$np} = "$pic & $picbefore & $picgemm & $picafter\\\\";
		    }

		    
		   
		    $itcost = &itercost;
		    

		    if($flag == 0)
		    {
#               	if($strat !~ /NOREC/)
# 			{
# 			    print "ERROR directory NOREC should be treated first !!! \n";
# 			    exit;
# 			}

			$tt{$np} = "$domnbr & $levelnbr & $connectnbr & $interf ";
			$uu{$np} = " $fill ";
		
			$w = sprintf("%g", $itcost);
			$vv{$np} = " $w ";
			$itnorec{$np} = $itcost;
			#$itcostnorec = $itcost;
#			print "Strat $strat: update itcostnorec\[$np\] $itnorec{$np} \n";
		    }
		    else
		    {
			$uu{$np} = $uu{$np}." & $fill ";
			
			if($itcost == 0)
			{
			    $g = 0.0;
			}
			else
			{
			    $g = $itnorec{$np} / $itcost;
			}

			$w = sprintf("%.2f", $g);
			$vv{$np} = $vv{$np}." & $w ";
		    }
		    
		    $fact = 0.0;
		    $fill = 0.0;
		    $solve = 0.0;
		    $iter = 0.0;
		    $total = 0.0;

		}
	    }
 	}


	print F '\begin{table}[H]'."\n";

	@p = keys %m;
	@p = sort { $a <=> $b} @p;
	print F '\caption{'."$matname: method = $strat"."\}\n";
	print F '\begin{center}'."\n";
	print F $tabhead;
	for($i=0;$i<@p-1;$i++)
	{
	    $np = $p[$i];
	    print F "$np & $m{$np} \n";
	}
	$np = $p[@p-1];
	print F "$np & $m{$np} \\hline \n";
	print F $tabfoot;
	###
	print F '\end{center}'."\n";
	print F '\end{table}'."\n";

	chdir $subdir;

	$flag++;
    }
#    print F '\newpage'. "\n";
    ########################################
#    print F '\begin{table}[H]'."\n";
#    @p = keys %tt;
#    @p = sort { $a <=> $b} @p;
#    print F '\caption{'."$matname: n = $dim nnzA = $nnzA, Number of locally levels = $loclv"."\}\n";
#    print F '\begin{center}'."\n";
#    print F $Gtabhead1.'} \hline'."\n";
#    print F $Gtabhead2." \\\\ \n";
#    print F $Gtabhead3."    \\\\ \n \\hline \n \\hline \n";
#    for($i=0;$i<@p-1;$i++)
#    {
#	$np = $p[$i];
#	print F "$np & $tt{$np} \\\\ \n";
#    }
#    $np = $p[@p-1];
#    print F "$np & $tt{$np} \\\\ \\hline \n";
#    print F $tabfoot;
#    print F '\end{center}'."\n";
#    print F '\end{table}'."\n";
#    ########################################
#    print F '\begin{table}[H]'."\n";
#    @p = keys %uu;
#    @p = sort { $a <=> $b} @p;
#    print F '\caption{'."$matname: n = $dim nnzA = $nnzA, Number of locally levels = $loclv"."\}\n";
#    print F '\begin{center}'."\n";
#    print F $Ftabhead1.'} \hline'."\n";
#    print F $Ftabhead2." \\\\ \n";
#    print F $Ftabhead3."    \\\\ \n \\hline \n \\hline \n";
#    for($i=0;$i<@p-1;$i++)
#    {
#	$np = $p[$i];
#	print F "$np & $uu{$np} \\\\ \n";
#    }
#    $np = $p[@p-1];
#    print F "$np & $uu{$np} \\\\ \\hline \n";
#    print F $tabfoot;
#    print F '\end{center}'."\n";
#    print F '\end{table}'."\n";
    ###################################
   #  print F '\begin{table}[H]'."\n";
#     @p = keys %vv;
#     @p = sort { $a <=> $b} @p;
#     print F '\caption{'."$matname: n = $dim nnzA = $nnzA, Number of locally levels = $loclv"."\}\n";
#     print F '\begin{center}'."\n";
#     print F $Wtabhead1.'} \hline'."\n";
#     print F $Wtabhead2." \\\\ \n";
#     print F $Wtabhead3."    \\\\ \n \\hline \n \\hline \n";
#     for($i=0;$i<@p-1;$i++)
#     {
# 	$np = $p[$i];
# 	print F "$np & $vv{$np} \\\\ \n";
#     }
#     $np = $p[@p-1];
#     print F "$np & $vv{$np} \\\\ \\hline \n";
#     print F $tabfoot;
#    print F '\end{center}'."\n";
#    print F '\end{table}'."\n";
    print F '\newpage'. "\n";
    
    chdir $dir;
}

print F $fichfoot;    
close F;

sub itercost
{
    if($rsa == 1)
    {
	if($strat =~ /0/)
	{
	    $c = $nnzA + 2*(2 * $L0 + $D0) + 2 * $E0 + 2 * $L1 + $D1;
	}
	elsif($strat =~ /1/)
	{
	    $c = $S0 + 2 * $L1 + $D1;
	}
	elsif($strat =~ /2/)
	{
	    $c = ((2*$L0+$D0) + 2*$E0+$C0) +  2*$L1+$D1;
	}
	elsif($strat =~ /NOREC/)
	{
	    $c = $nnzA + 2*$L0+$D0;
	}
	else
	{
	    print "Erreur dans itercost : $strat n\'est pas une strategie \n";
	    exit;
	}
    }
    else
    {
	if($strat =~ /0/)
	{
	    $c = $nnzA + 2*($L0 + $U0) + $E0 + $F0 + $L1 + $U1;
	}
	elsif($strat =~ /1/)
	{
	    $c = $S0 + $L1 + $U1;
	}
	elsif($strat =~ /2/)
	{
	    $c = ($L0+$U0) + $E0+$F0 + $C0 + $L1 + $U1;
	}
	elsif($strat =~ /NOREC/)
	{
	    $c = $nnzA + $L0+$U0;
	}
	else
	{
	    print "Erreur dans itercost : $strat n\'est pas une strategie \n";
	    exit;
	}
    }

    $c;
}

echo "Checkout"
mkdir tmp
cd tmp

for rev in `seq 125 30 259`
  do
  rev=`svn export -r $rev svn+ssh://gaidamou@scm.gforge.inria.fr/svn/hips | tail -n 1 | grep "Exported revision" | cut -d" " -f3 | tr -d "."`
  
  mv hips ../hips-rev$rev
:
done;

cd ..
rmdir tmp

echo "Test"
for dir in `ls -d hips*`
do
cp inputs $dir/trunk/TESTS/DBMATRIX/
cp lance.test $dir/trunk/TESTS/DBMATRIX/
done


echo "Compil"
for dir in `ls -d hips*`
do
cp makefile.in $dir/trunk/
cd $dir/trunk/
#make clean 2>&1 > /dev/null
#make tests -j2 2>&1 > /dev/null
cd -
done

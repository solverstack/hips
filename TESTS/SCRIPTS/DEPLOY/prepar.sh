mkdir INSTALL
cd INSTALL
echo "Suppr hips-rev*"
rm -rf hips-rev*

echo "Checkout"
mkdir tmp
cd tmp

rev=`svn export svn+ssh://henon@scm.gforge.inria.fr/svn/hips | tail -n 1 | grep "Exported revision" | cut -d" " -f3 | tr -d "."`

mv hips ../hips-rev$rev
cd ..
rmdir tmp

VERSION=`cd INSTALL; ls -d hips-rev* | head -n 1`
echo "Version : $VERSION"
CONFIGDIRLIST=`find CONFIG -type d -maxdepth 1 -mindepth 1 -printf "%f\n"`

SPEED=10

for i in $CONFIGDIRLIST
do 

  DIR=INSTALL/$VERSION-$i/trunk/TESTS/DBMATRIX/
    
  #echo "Copie ..."
  cp -r INSTALL/$VERSION INSTALL/$VERSION-$i
  
  echo "* Config $i ..."
  
# COMPIL
  cp    CONFIG/$i/makefile.in   INSTALL/$VERSION-$i/trunk/
  cp -f CONFIG/$i/block.h       INSTALL/$VERSION-$i/trunk/INCLUDE 2> /dev/null
  cp -f CONFIG/$i/hips_define.h INSTALL/$VERSION-$i/trunk/INCLUDE 2> /dev/null
  
  rm -f $DIR/RESULTS/inputs.template
  rm -f $DIR/RESULTS/configuration.pl
  rm -f $DIR/RESULTS/MATRIX
  
  INSTALLLOG="$VERSION-$i/install_log"

  mkdir INSTALL/$INSTALLLOG
  
  ln INSTALL/$VERSION-$i/trunk/makefile.in   INSTALL/$INSTALLLOG
  ln INSTALL/$VERSION-$i/trunk/INCLUDE/block.h               INSTALL/$INSTALLLOG
  ln INSTALL/$VERSION-$i/trunk/INCLUDE/hips_define.h INSTALL/$INSTALLLOG
  
  cd INSTALL/$VERSION-$i/trunk/
  make clean > /dev/null
  make -j $SPEED all > ../../$INSTALLLOG/make.stdout 2> ../../$INSTALLLOG/make.stderr
  cd -
  
  for j in `find CONFIG/$i -type d -maxdepth 1 -mindepth 1 -printf "%f\n"`
    do
    echo " - Tests $i $j"

    DIRRESULTS=$DIR/RESULTS_$i_$j
    
    cp -r $DIR/RESULTS $DIRRESULTS
    
    cp CONFIG/$i/$j/inputs.template $DIRRESULTS/
    cp CONFIG/$i/$j/configuration.pl $DIRRESULTS/
    cp CONFIG/$i/$j/MATRIX $DIRRESULTS/
  done
  
    echo
done;

echo 
echo
echo "Lancement"

for i in $CONFIGDIRLIST
do 

  DIR=INSTALL/$VERSION-$i/trunk/TESTS/DBMATRIX/

  for j in `find CONFIG/$i -type d -maxdepth 1 -mindepth 1 -printf "%f\n"`
    do
    echo " - Tests $i $j"
    DIRRESULTS=$DIR/RESULTS_$i_$j

    cd $DIRRESULTS/
    ./scriptSEQm.pl
    cd -
  done;
done;

cd ..

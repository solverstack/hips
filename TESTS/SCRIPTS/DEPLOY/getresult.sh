VERSION=`cd INSTALL; ls -d hips-rev* | head -n 1`
echo "Version : $VERSION"
CONFIGDIRLIST=`find CONFIG -type d -maxdepth 1 -mindepth 1 -printf "%f\n"`

mkdir RESULTS

# BKP version
cd INSTALL
tar -cjf $VERSION.tbz2 $VERSION
mv $VERSION.tbz2 ../RESULTS/
rm -rf $VERSION
cd -

for i in $CONFIGDIRLIST
do 

  mkdir RESULTS/$i
  mv INSTALL/$VERSION-$i/install_log/ RESULTS/$i

  DIR=INSTALL/$VERSION-$i/trunk/TESTS/DBMATRIX/

  for j in `find CONFIG/$i -type d -maxdepth 1 -mindepth 1 -printf "%f\n"`
    do
    echo " - Tests $i $j"
    DIRRESULTS=$DIR/RESULTS_$i_$j
    mv $DIRRESULTS/ RESULTS/$i/

  done;

  rm -rf INSTALL/$VERSION-$i
done;

rmdir INSTALL

cd RESULTS
find . -type l -exec rm {} \;
find . -name "core.*" -exec rm -f {} \;
find . -name "*~" -exec rm -f {} \;

CLEANLIST="testDBMatrix.ex configuration.pl inputs.template MATRIX scriptSEQm.pl scriptSEQ.pl submitCCRT.template submitM3PEC.template"
for i in $CLEANLIST
do 
find . -name "$i" -exec rm -f {} \;
done

cd -


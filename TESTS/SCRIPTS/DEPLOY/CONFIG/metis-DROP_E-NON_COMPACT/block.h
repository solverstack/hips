#include "type.h"

/* double */
#ifndef TEST
#include "phidal_sequential.h"
#endif

#include "symbol.h"
#include "solver.h"

#include "db_struct.h"

#ifdef PARALLEL

#define ALLOC_NOREC BLK
#define ALLOC_L     ONE
#define ALLOC_E     CBLK
#define ALLOC_S     BLK
#define ALLOC_S2    BLK
#define ALLOC_FILL  INGEMM

/* #define AMALGAMATE */

#else
/***********************************************************/
/***********************************************************/

#define AMALGAMATE  /* undef for test between scalar version & block */

/* #define GEMM_LEFT_LOOKING */

#ifdef GEMM_LEFT_LOOKING
/* a commenter pour ./test.sh */
/* #define NO_PIC_DECREASE */

#define ALLOC_NOREC ONE
#define ALLOC_L     ONE
#define ALLOC_S2    ONE

/* r�duction du pic m�moire */
#ifndef NO_PIC_DECREASE
#define ALLOC_E     RBLK
#define ALLOC_S     CBLK
#define ALLOC_FILL  INGEMM
#else
/* sans r�duction du pic memoire */
#define ALLOC_E     ONE
#define ALLOC_S     ONE
#define ALLOC_FILL  BEFORE
#endif

#else 

#define GEMM_RIGHT_LOOKING

#define ALLOC_NOREC ONE
#define ALLOC_L     ONE

#define ALLOC_E     CBLK
#define ALLOC_S     ONE
#define ALLOC_S2    ONE
#define ALLOC_FILL  INGEMM

#endif

#endif /* PARALLEL */

/* TODO : if avec ALLOC_ => defined() */


/*TODO*/
#undef ALLOC_S2
#define ALLOC_S2 ALLOC_S

/***********************************************************/
/***********************************************************/


/*#ifdef stride
  #warning STRIDE MACRO DEFINED !
  #undef stride
  #endif*/

/* #define Dprintfv(5, ...) printf(__VA_ARGS__) */
/* void Dprintfv(5, const char *format, ...) { static int i=0; if (i==0) printf("Warning\n : DEBUG COMMENTS\n"); i=1; } */
/* #define Dprintfv(5, args...) */
/* #define printLD2(arg1,arg2) */
/* void Dprintfv(5, const char *format, ...); */

enum walloc   {BEFORE, INGEMM};

void pvec(char* bla, REAL* vec, int n, int ex);
int calcCoefmax(SymbolMatrix* symbmtx1 /*M*/, SymbolMatrix* symbmtx /*L*/) ;
int DBMatrix_calcCoefmax(DBMatrix* M /*M*/, DBMatrix* L /*L*/);

void DBMatrix_HID2SymbolMatrix(DBMatrix* m, SymbolMatrix* symbmtx, PhidalHID* BL, int facedecal, int tli);


void DBMATRIX_PrecSolve(REAL tol, DBPrec *P, COEF *x, COEF *b, PhidalHID *BL, PhidalOptions *option);
void DBMATRIX_Precond(PhidalMatrix *A, DBPrec *P, SymbolMatrix* symbmtx, PhidalHID *BL, PhidalOptions *option);
void DBMATRIX_MLICCPrec(PhidalMatrix *A, DBPrec *P, SymbolMatrix* symbmtx, PhidalHID *BL, PhidalOptions *option);
void DBMATRIX_MLILUPrec(PhidalMatrix *A, DBPrec *P, SymbolMatrix* symbmtx, PhidalHID *BL, PhidalOptions *option);

REAL SymbolMatrix_NNZ(SymbolMatrix *symbmtx);
void DBPrec_Info(DBPrec *p);
long DBPrec_NNZ(DBPrec *p);
long DBPrec_NNZBefore(DBPrec *p);
long DBPrec_NNZPic(DBPrec *p);
long DBPrec_NNZMinPic(DBPrec *p);
long DBPrecPart_NNZ(char* TYPE, int symmetric, DBMatrix* m);

void DBPrec_SchurProd(DBPrec *P, PhidalHID *BL, COEF *x, COEF *y);

void DBMatrix_Lsolv(int unitdiag, DBMatrix *L, COEF *x, COEF *b, PhidalHID *BL);
void DBMatrix_Usolv(int unitdiag, DBMatrix *U, COEF *x, COEF *b, PhidalHID *BL);
void DBMatrix_Dsolv(DBMatrix *D, COEF* b);

/* void DBMatrix_Lsolv2(int unitdiag, DBMatrix *L, COEF *x, COEF *b, PhidalHID *BL); */
/* void DBMatrix_Usolv2(int unitdiag, DBMatrix *U, COEF *x, COEF *b, PhidalHID *BL); */
void DBMatrix_Dsolv2(DBMatrix *D, COEF* b);

void DBMATRIX_MatVec(DBMatrix *L, DBMatrix *U, PhidalHID *BL, COEF *x, COEF *y);
void DBMATRIX_MatVec2(DBMatrix *L, DBMatrix *U, PhidalHID *BL, COEF *x, COEF *y);

/**********/

void DBPrec_Init(DBPrec *p);
void DBPrec_Print(DBPrec *p);
void DBPrec_Clean(DBPrec *p);

void print_size(unsigned long size);
unsigned long DBMatrix_size(DBMatrix* m, PhidalHID* BL, int verbose);
unsigned long SolverMatrix_size(SolverMatrix* m, int verbose);
unsigned long SymbolMatrix_size(SymbolMatrix* m, int verbose);

void freeMatrixStruct(MatrixStruct* mtxstr, int cblknbr);
void freeSolverMatrix(SolverMatrix* solvmtx);
void freeSolverMatrix(SolverMatrix* solvmtx);
void freeSymbolMatrix(SymbolMatrix* symbmtx);
void DBMatrix_Clean(DBMatrix* m);
void SymbolMatrix_Init(SymbolMatrix* symbmtx);
void SymbolMatrix_Clean(SymbolMatrix* symbmtx);
void SolverMatrix_Clean(SolverMatrix* solvmtx);

void SymbolMatrix_Clean(SymbolMatrix* symbmtx);
void SolverMatrix_Clean(SolverMatrix* solvmtx);


void DBMatrix_Build(PhidalMatrix* mat, 
		    int tli, int tlj, int bri, int brj, char *UPLO, char *DIAG, int locally_nbr, 
		    PhidalMatrix* Phidal, 
		    DBMatrix* DBM, SymbolMatrix* symbmtx, PhidalHID *BL,
		    int levelnum, int levelnum_l,
		    alloc_t alloc, int filling);
void DBMatrix_BuildV(PhidalMatrix* mat, 
		     char *UPLO, char *DIAG, int locally_nbr, 
		     PhidalMatrix* Phidal, 
		     DBMatrix* DBM, DBMatrix* DBM1, PhidalHID *BL,
		     int levelnum, int levelnum_l, int filling);

/**/
void DBMatrix_Setup(int tli, int tlj, int bri, int brj, char *UPLO, char *DIAG, 
		    int locally_nbr, DBMatrix* m, 
		    SymbolMatrix* symbmtx, PhidalHID *BL, 
		    int levelnum, int levelnum_l, alloc_t alloc);
void DBMatrix_Setup_HID(int tli, int tlj, int bri, int brj, char *UPLO, char *DIAG, 
			int locally_nbr, DBMatrix* m, PhidalHID *BL);
void DBMatrix_Setup_SYMBOL(DBMatrix* m, SymbolMatrix* symbmtx, 
			   PhidalHID *BL, int levelnum, int levelnum_l);
/* void DBMatrix_Setup_cblktosolvmtx(DBMatrix* m); */

void DBMatrix_SetupV(DBMatrix *M, DBMatrix *Cpy);
/**/

void DBMatrix_Copy(PhidalMatrix* mat, PhidalHID* BL, 
		   PhidalMatrix* Phidal, DBMatrix* DBM,
		   char* UPLO, int levelnum, int levelnum_l, int filling);


/**/



void DBMATRIX_Solve(PhidalMatrix *A, DBPrec *P, PhidalHID *BL, PhidalOptions *option, COEF *rhs, COEF * x);



void DBMatrix_VirtualCpy(DBMatrix *M, DBMatrix *Cpy);
void DBMatrix_Transpose(DBMatrix *M);

/* void printSymbol(SymbolMatrix* symbmtx); */
/* void testEgal(SolverMatrix* solvmtx, SolverMatrix* solvmtx2, char* UPLO); */

/* void testEgalParcours(SolverMatrix* solvmtx, SymbolMatrix* symbmtx); */
/* void testEgal_Symbol(SymbolMatrix* symbmtx, SymbolMatrix* symbmtx2); */
/* void printCSR(csptr cs, char* filename) ; */

void cpy_Symbol(SymbolMatrix* symbmtx, SymbolMatrix* symbmtx2);

void DBMatrix_Cpy(DBMatrix *M, DBMatrix *Cpy); /*  TODO : nom trop ressemblant */
void DBMatrix_Copy2(DBMatrix* M, DBMatrix* Copy);

void DBMatrix_Symb2SolvMtx(SolverMatrix* solvmtx);
void DBMatrix_Symb2SolvMtx_(SolverMatrix* solvmtx, COEF* coeftab);

void DBMatrix_AllocSymbmtx(int i0, int i, DBMatrix* m, alloc_t alloc,
			   int* ia, int* ja, VSolverMatrix **a,
			   int* Pia, int* Pja, struct SparRow **Pa, 
			   SolverMatrix* solver, char* TRANS, PhidalHID* BL);

void DBMatrix_Init(DBMatrix *a);
void SymbolMatrix_VirtualCopy(SymbolMatrix *M, SymbolMatrix *Cpy);

void SparRow2SolverMatrix(int tli, int tlj, csptr mat, SymbolMatrix* symbmtx, SolverMatrix* solvmtx);

void print(PhidalMatrix* m, PhidalHID* BL);

void PHIDAL_SymbolMatrix(csptr mat, SymbolMatrix* symbmtx, int** rangtab_pt, int* treetab, int cblknbr_l1, PhidalHID* BL, int locally_nbr);

void symbolicBlok(MatrixStruct* mtxstr, csptr mat, int cblknbr, int* rangtab);
void symbolicFacto(MatrixStruct* mtxstr, int cblknbr, int* treetab, int* rangetab);

void MatrixStruct2SymbolMatrix(SymbolMatrix* symbmtx, MatrixStruct* mtxstr, int cblknbr, int* rangtab);
void csrSolverMatrix(SolverMatrix* solvmtx, csptr mat);

void numericFacto(SolverMatrix* solvmtx);
void DBMatrix_FACTO(DBMatrix* m);
void DBMatrix_FACTO2(DBMatrix* m);
void DBMatrix_FACTOu(DBMatrix* L, DBMatrix* U);

void DBMatrix_TRSM(int unitdiag, DBMatrix* L, DBMatrix* M);

void Lsolv(int unitdiag, SolverMatrix* solvmtx, COEF *b, COEF *x);
void Usolv(int unitdiag, SolverMatrix* solvmtx, COEF *b, COEF *x);
void Ltsolv(int unitdiag, SolverMatrix* solvmtx, COEF *b, COEF *x);
void Dsolv(SolverMatrix* solvmtx, COEF* b);
void bmatvec(SolverMatrix* solvmtx, COEF* x, COEF* y, char* UPLO);

#ifndef TEST
int fgmresd_blok(int proc_id, csptr A, SolverMatrix* L,  PhidalOptions *option, 
		 COEF *rhs, COEF * x, FILE *fp);

void SolverMatrix_TRSM(int unitdiag, SolverMatrix* L, SolverMatrix* M);
void DBMatrix_GEMM(DBMatrix* C, COEF alpha, DBMatrix* A, DBMatrix* B, DBMatrix* D, 
		   int filling, PhidalMatrix* phidal, PhidalHID *BL);
void DBMatrix_GEMMu(DBMatrix* CL, DBMatrix* CU, COEF alpha, DBMatrix* A, DBMatrix* B, DBMatrix* D, 
		    int filling, PhidalMatrix* PhidalL, PhidalMatrix* PhidalU, PhidalHID *BL);
void SolverMatrix_Div(SolverMatrix* A, SolverMatrix* D);

/* void SymbolMatrix_Cut(SymbolMatrix* symbA, SymbolMatrix* symbB, */
/* 		      int tli, int tlj, int bri, int brj, /\* alloc_t alloc, *\/ SymbolBCblk* pAtab); */

/* void SymbolMatrix_Cut2(SymbolMatrix* symbA, SymbolMatrix* symbB,/\* todo : verifier que c les meme conventions sur phidalseq *\/ */
/* 		       int tli, int tlj, int bri, int brj, alloc_t alloc,  SymbolBCblk* pAtab); */


void SymbolMatrix_Cut(SymbolMatrix* symbA, SymbolMatrix* symbB,
		      int tli, int tlj, int bri, int brj);

void SymbolMatrix_Cut2(SymbolMatrix* symbA, SymbolMatrix* symbB,/* todo : verifier que c les meme conventions sur phidalseq */
		       int tli, int tlj, int bri, int brj, alloc_t alloc);

void tlj2cblktlj(SymbolMatrix* symbA, int tlj, int brj, int* cblktlj, int* cblkbrj);
void initpAtab(SymbolMatrix* symbA, SymbolBCblk* pAtab);
void setpAtab(SymbolMatrix* symbA, int cblktlj, int cblkbrj, int tli, int bri, SymbolBCblk * pAtab);
int bloknbr(SymbolMatrix* symbA, int cblktlj, int cblkbrj, int tli, int bri, int use_pAtab, SymbolBCblk * pAtab);


void DcpyMatrixToDiag(SolverMatrix* solvmtx, COEF* Diag);
void DcpyDiagToMatrix(SolverMatrix* solvmtx, COEF* Diag);

void SolverMatrix_MatrixBuild(SolverMatrix* M, PhidalMatrix* PhidalM, PhidalHID* BL, int levelnum, alloc_t alloc);



void DBMatrix_Setup1(int tli, int tlj, int bri, int brj, char *UPLO, char *DIAG, int locally_nbr, DBMatrix* m, PhidalHID *BL);
void DBMatrix_Setup2(DBMatrix* m, SymbolMatrix* symbmtx, PhidalHID *BL, int levelnum, int levelnum_l);

void SF_Direct(csptr A, int cblknbr, int *rangtab, int *treetab, csptr P);
void amalgamate(REAL rat, int nrow, csptr P, int snodenbr, int *snodetab, int *treetab, 
		int *cblknbr, int **rangtab, int *nodetab);
void HIPS_SymbolMatrix(int verbose, int numflag, REAL rat, int locally_nbr, int n,  int *ia,  int *ja, 
		       PhidalHID *BL, SymbolMatrix *symbmtx, int *perm, int *iperm);

void symbolicBlok2(MatrixStruct* mtxstr, csptr P, int cblknbr, int* rangtab);

void HID2MatrixStruct(int* rangtab, MatrixStruct* mtxstruct, PhidalHID* BL, int startlevel, int locally_nbr);

/* void DBMatrix_Setup_solvermatrix(SolverMatrix* solvmtx); */
void DBMatrix_Setup_solvermatrix(SolverMatrix* solvmtx, SymbolMatrix* symbmtx);
void DBMatrix_Setup_cblktosolvmtx(DBMatrix* m);

void DBMatrix2PhidalMatrix(DBMatrix* L, PhidalMatrix* Copy, int locally_nbr,  PhidalHID *BL, int inarow, int unitdiag);
void DBMatrix_GetDiag(DBMatrix* M, COEF* D);

void DBMatrix2PhidalMatrix_Unsym(DBMatrix* L, DBMatrix* U, PhidalMatrix* Copy, int locally_nbr,  PhidalHID *BL, int inarow);

void DBPrec_SchurProd(DBPrec *P, PhidalHID *BL, COEF *x, COEF *y);
#endif

int cblksize(DBMatrix* db);
void cblkbuffer_size(DBMatrix* A, int* W, int* F);

void VS_ICCT(VSolverMatrix* vsolvmtx, COEF* F, COEF* W);
void VS_InvLT(int unitdiag, VSolverMatrix* L, VSolverMatrix* M, COEF* W);
void VS2_InvLT(int unitdiag, SolverMatrix* L, SymbolMatrix* symbL, SolverMatrix* M, SymbolMatrix* symbM, COEF* W);
void VS_RowDiv(VSolverMatrix* L, VSolverMatrix* M);

void DB_ICCprod(int iB, COEF alpha, 
		DBMatrix* A,
		int nk, int* jak, VSolverMatrix** rak,
		int* tabA, int* tabC, 
		COEF* E, COEF* F, COEF* W);

void DBMatrix_FACTO_TRSM_GEMM(DBMatrix* L, DBMatrix* E, DBMatrix* S, 
			      int fill, PhidalMatrix* phidalE, PhidalHID* BL);
void DBMatrix_FACTO_TRSM_DROP(DBMatrix* L, DBMatrix* E, PhidalMatrix* PhidalEU, 
			      REAL droptol, REAL *droptab, 
			      int fill, PhidalMatrix* PhidalE, int locally_nbr, PhidalHID* BL);
void  DBMatrix_FACTO_TRSM_DROPu(DBMatrix* L, DBMatrix* U, DBMatrix* E, DBMatrix* F, 
				PhidalMatrix* PhidalEU, PhidalMatrix* PhidalLF, 
				REAL  droptol, REAL * droptab, int fill, 
				PhidalMatrix* PhidalE, PhidalMatrix* PhidalF, int locally_nbr, PhidalHID* BL);

void DBMatrix_GEMMpart(int jj, DBMatrix* C, COEF alpha, DBMatrix* A, /* DBMatrix* B_OLD, */ DBMatrix* D, 
		       /* int fill, */ /* PhidalMatrix* Phidal, PhidalHID *BL,  */
		       COEF* E, COEF* F, COEF* W, int* tabA, int* tabC);

void DBMatrix_GEMMpartu(int jj, DBMatrix* CL, DBMatrix* CU, COEF alpha, DBMatrix* A, DBMatrix* B,
			COEF* E, COEF* F, COEF* W, int* tabA, int* tabC);


int LDLt_piv(int n, int stride, COEF *coefftab, REAL normA, REAL epsilon);
void LU(int n, COEF *coeftabL, int strideL, COEF *coeftabU, int strideU);

int calc_Emax(DBMatrix* E);
long DBPrec_NNZ_LEV1_RL(DBPrec *p);

void DBMatrix_FACTO_TRSM_GEMMu(DBMatrix* L, DBMatrix* U, DBMatrix* E, DBMatrix* F, 
			       DBMatrix* SL, DBMatrix* SU, int fill, PhidalMatrix* PhidalE, PhidalMatrix* PhidalF, PhidalHID* BL);

void DBMatrix_FACTO_Lu(DBMatrix* L, DBMatrix* U);
void VS_ICCTu(VSolverMatrix* vsolvmtxL, VSolverMatrix* vsolvmtxU, COEF* W);

void PhidalMatrix_Init_fromDB(DBMatrix* M, PhidalMatrix* Copy, char* UPLO, int locally_nbr,  PhidalHID *BL);


int HIPS_Fgmresd_DB_PH(int verbose, REAL tol, int itmax, 
		       DBPrec *Q, PhidalPrec *P, 
		       PhidalHID *BL, PhidalOptions *option, 
		       COEF *rhs, COEF * x, FILE *fp);

int HIPS_Fgmresd_PH_DB(int verbose, REAL tol, int itmax, 
		       PhidalMatrix *A, DBPrec *P, 
		       PhidalHID *BL, PhidalOptions *option, 
		       COEF *rhs, COEF *x, FILE *fp);

int HIPS_Fgmresd_DB   (int verbose, REAL tol, int itmax, 
		       DBPrec *P, 
		       PhidalHID *BL, PhidalOptions *option, 
		       COEF *rhs, COEF *x, FILE *fp);

int HIPS_Fgmresd_DB_DB(int verbose, REAL tol, int itmax, 
		       DBMatrix *L, DBMatrix *U, DBPrec *P, 
		       PhidalHID *BL, PhidalOptions *option, 
		       COEF *rhs, COEF *x, FILE *fp);


int HIPS_PCG_DB_PH(int verbose, REAL tol, int itmax, 
		       DBPrec *Q, PhidalPrec *P, 
		       PhidalHID *BL, PhidalOptions *option, 
		       COEF *rhs, COEF * x, FILE *fp);

int HIPS_PCG_PH_DB(int verbose, REAL tol, int itmax, 
		       PhidalMatrix *A, DBPrec *P, 
		       PhidalHID *BL, PhidalOptions *option, 
		       COEF *rhs, COEF *x, FILE *fp);

int HIPS_PCG_DB   (int verbose, REAL tol, int itmax, 
		       DBPrec *P, 
		       PhidalHID *BL, PhidalOptions *option, 
		       COEF *rhs, COEF *x, FILE *fp);

int HIPS_PCG_DB_DB(int verbose, REAL tol, int itmax, 
		       DBMatrix *L, DBMatrix *U, DBPrec *P, 
		       PhidalHID *BL, PhidalOptions *option, 
		       COEF *rhs, COEF *x, FILE *fp);


void DBMatrix_Copy3(DBMatrix* L, DBMatrix* Lcopy, int levelnum, int locally_nbr, PhidalHID* BL);
void DBMatrix_Copy3_unsym(DBMatrix* L, DBMatrix* U, DBMatrix* Lcopy, DBMatrix* Ucopy, int levelnum, int locally_nbr, PhidalHID* BL);


void DBMatrix_Setup_HID(int tli, int tlj, int bri, int brj, char *UPLO, char *DIAG, 
			int locally_nbr, DBMatrix* m, PhidalHID *BL);


void DBPrec_SymmetricUnscale(REAL *scaletab, REAL *iscaletab, DBPrec *P, PhidalHID *BL);
void DBMatrix_ColMult(COEF *b, DBMatrix *D, PhidalHID *BL);
void DBMatrix_RowMult(COEF *b, DBMatrix *D, PhidalHID *BL);
void DBMatrix_DiagMult(COEF *b, DBMatrix *D, PhidalHID *BL);

#ifndef HIPS_DEFINE_H
#define HIPS_DEFINE_H
#include "base.h"
#include "localdefs.h"

/*#define ILUT_WHOLE_SCHUR */ /** To factorize the Schur in 
			      a single CS matrices instead 
			      of using a PhidalMatrix block pattern **/
/*#define ILUT_SOLVE*/  /** The schur preconditioner is transformed into a
			single SparRow matrix for the solve    **/


/*#define OPTIM_FEW_NZR  NOT GOOD ON TESTS **/

/*#define REALLOC_BLOCK*/

/** This corresponds to a page of 2Mo BETTER ON AIX 64b **/
#define SMALLBLOCK 232144  


/**********************************/
/* Alloc in block ILUT subroutine */
/**********************************/
/* #define ALLOC_COMPACT_ILUT     */
/* #define ALLOC_COMPACT_RATIO 2  */
/* #define ALLOC_COMPACT_INIT 4 */



/** This corresponds to a page of 1Mo **/
/*#define SMALLBLOCK 131072  */

/** To activate ready send for vectors communication **/
/*#define VEC_READY_SEND */

/** Minimum buffer size (in nnz) under which the comm mem. ratio is not applied
    **/
/*#define MIN_BUFF_SIZE  88036 */
#define MIN_BUFF_SIZE 2500
#define SYMMETRIC_DROP
/*#define SCALE_ALL*/    /** PUT THIS OPTION TO BE SURE IN DBMATRIX + ILUT
			 IN SCHUR TO RESCALE THE SCHUR **/
#define EPSILON 10e-12
#define ZERO    10e-30
#define EPSMAC   1.0e-16

#ifndef TRUE
#define TRUE 1
#endif

#ifndef FALSE
#define FALSE 0
#endif

/** Macros used in the communication layer **/
#define LEAD_TAG 1
#define CTRB_TAG 2
#define CTRB_TAG_L 3
#define CTRB_TAG_U 4
#define LEAD_TAG_L 5
#define LEAD_TAG_U 6
#define DIAG_TAG 7
#define SYNCHRONOUS 0
#define ASYNCHRONOUS 1
#define NOT_READY 0
#define READY 1

#ifdef DEBUG_M
#define free(a)  {assert(a); free(a); a=NULL;}
#define malloc(b) ((b)>0?(malloc((b))):(NULL))
#endif

/*#define TRACE_COM *//** print communication for debugging purpose **/

/*#ifdef DEBUG_M
#define free(a) MonFree(a)
#define malloc(a) MonMalloc(a)
#define realloc(p, a) MonRealloc(p, a)
#endif*/


#define MAX(x,y) (((x)<(y))?(y):(x))
#define MIN(x,y) (((x)>(y))?(y):(x))

#endif

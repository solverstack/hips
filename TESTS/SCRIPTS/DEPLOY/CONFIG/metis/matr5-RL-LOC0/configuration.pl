#!/usr/bin/perl -w

#---
#$machine = 'LOCAL';
#$machine = 'M3PEC';
$machine = 'CCRT';

$prog = '../testDBMatrix.ex';

#---

$droptol0 = 0.0;
@droptol1list = (0);

#---

@levellist = (0);
@ndomlist  = (500, 1000, 2000, 4000, 8000);
@stratlist = (2);

#---

$mem = 0000;
$temps = '29';

#---

$sym = 0;
$driver = "";

#---

$matrixlist = 'MATRIX';
$inputsT = 'inputs.template';

$submitT = 'submit'.$machine.'.template';


CONFIGDIRLIST=`find CONFIG -type d -maxdepth 1 -mindepth 1 -printf "%f\n"`

mkdir TABLE

for i in $CONFIGDIRLIST
do 

  for j in `find CONFIG/$i -type d -maxdepth 1 -mindepth 1 -printf "%f\n"`
    do
    echo " - Tests $i $j"
    DIRRESULTS=RESULTS/$i/RESULTS_$j
    
    cd $DIRRESULTS/
     cp ../../../tableauDBMATRIX.pl .
    ./tableauDBMATRIX.pl
    rm ./tableauDBMATRIX.pl
    mv Table.tex ../../../TABLE/Table-$i-$j.tex
    cd -
  done;

done;


#!/usr/bin/perl -w

use List::Util qw(first max maxstr min minstr reduce shuffle sum);

$compare_norec = 1;
$gfw_tab = 1;
$gfw_tab2 = 1;
$latex_tab = 0;

$stratcmp = "1";

sub getnum {
    use POSIX qw(strtod);
    my $str = shift;
    $str =~ s/^\s+//;
    $str =~ s/\s+$//;
    $! = 0;
    my($num, $unparsed) = strtod($str);
    if (($str eq '') || ($unparsed != 0) || $!) {
	return undef;
    } else {
	return $num;
    } 
}

sub round {
    my($number) = shift;
    $number = $number * 100;
    return (int($number + .5 * ($number <=> 0))/100);
}

#$clef1 = 'LEVEL';
$clef1 = 'STRAT';
$endrun = "END";
#$fillkey = "Fill Ratio";
$solvekey = "Solve in";
#$factokey = "PHIDAL_Precond";
$iterkey = "outer iterations";
$innerkey = "smoothing iter";
$domsizekey = "DOMSIZE";
$domnbrkey = "Found";
#$levelnbrkey = "number of levels";
$levelnbrkey = "Total number of levels ";
$connectorkey = "number of connectors";
$indimkey = "Level 0 =";
$dimkey = "Matrix dimension";
$nnzAkey = "Number of nonzeros";

$picBeforeGemmkey = "Pic : Before GEMM :";
$picInGemmWithoutkey = "Pic : In GEMM without desalloc in GEMM";
$picInGemmWithkey = "Pic : In GEMM with    desalloc in GEMM :";
$picAfterGemmkey = "Pic : After GEMM :";

$itOneIterkey = "One iter in ";
$itPrecSolvekey = "PrecSolve in ";
$itShurProdkey = "ShurProd in";

$precondLkey  = "M : Numeric Factorisation in";
$precondEkey  = "TRSM in";
$precondSkey  = "GEMM in";
$precondSLkey = "S : Numeric Factorisation in";

$factokey = "DB_Precond in";

$fichhead = '\documentclass[10pt,a4wide]{article}'."\n".
    '\usepackage[latin1]{inputenc}'."\n".
    '\usepackage{amsmath}'."\n".
    '\usepackage{amsfonts}'."\n".
    '\usepackage{amssymb}'."\n".
    '\usepackage{epsf}'."\n".
    '\usepackage{here}'."\n \n\n";

# $fichhead .= '\newcommand{\langtitle}{Experiments}'."\n". #TODO : faire diff pour v�rifier identique vers d'avant
#     '\newcommand{\langdomsize}{Domsize}'."\n".    
#     '\newcommand{\langsecond}{sec.}'."\n".
#     '\newcommand{\langfacto}{Facto}'."\n".
#     '\newcommand{\langsolve}{Solve}'."\n".
#     '\newcommand{\langtotal}{Total}'."\n".
#     '\newcommand{\langiter}{Iter}'."\n".
#     '\newcommand{\langfill}{Fill}'."\n". 
#     '\newcommand{\langratio}{ratio}'."\n".
#     '\newcommand{\langnumberof}{number of}'."\n".
#     '\newcommand{\langdomains}{domains}'."\n".
#     '\newcommand{\langlevels}{levels}'."\n".
#     '\newcommand{\langconnectors}{connectors}'."\n".
#     '\newcommand{\langpercent}{percent}'."\n".
#     '\newcommand{\langmethod}{Method}'."\n".
#     '\newcommand{\langparameter}{parameter}'."\n".
#     '\newcommand{\langitereq}{\\# iter eq.}'."\n".
#     '\newcommand{\langitereqabrev}{\\# iter eq.}'."\n".
#     '\newcommand{\langopc}{opc}'."\n".
#     '\newcommand{\langnblocallylevels}{Number of locally levels}'."\n \n\n";



$fichhead .= '\usepackage[latin1]{inputenc}'."\n".
    '\usepackage[francais]{babel}'."\n \n".

    '\newcommand{\langtitle}{Exp�rimentations}'."\n".
    '\newcommand{\langdomsize}{Taille}'."\n".    
    '\newcommand{\langsecond}{s}'."\n".
    '\newcommand{\langfacto}{Factorisation}'."\n".
    '\newcommand{\langsolve}{R�solution}'."\n".
    '\newcommand{\langtotal}{Total}'."\n".
    '\newcommand{\langiter}{It�rations}'."\n".
    '\newcommand{\langfill}{Remplissage}'."\n". 
    '\newcommand{\langratio}{(rapport)}'."\n". #TODO : parenthese en eng
    '\newcommand{\langnumberof}{Nombre de}'."\n".
    '\newcommand{\langdomains}{domaines}'."\n".
    '\newcommand{\langlevels}{niveaux}'."\n".
    '\newcommand{\langconnectors}{connecteurs}'."\n".
    '\newcommand{\langpercent}{\%}'."\n".
    '\newcommand{\langmethod}{M�thode}'."\n".
    '\newcommand{\langparameter}{domaines}'."\n".
    '\newcommand{\langitereq}{\\# d\'it�ration �quivalente}'."\n".
    '\newcommand{\langitereqabrev}{\\# d\'it. �q.}'."\n".
    '\newcommand{\langopc}{\\# op�rations}'."\n".
    '\newcommand{\langnblocallylevels}{Nombre de niveaux localement consistant}'."\n \n\n";

$fichhead .= '\title{\langtitle}'."\n".
    '\begin{document}'."\n".
    '\maketitle '."\n";

$fichfoot = '\end{document}'."\n";

$tabhead ='\begin{tabular}{|c||c|c|c|c|c|} \hline'."\n".
    '\langdomsize   &  \langfacto  & \langsolve   & \langtotal  & \langiter  & \langfill \\\\'."\n" .
    '\langparameter &  (\langsecond) &  (\langsecond) & (\langsecond)  &       &  \langratio \\\\'."\n \\hline \n \\hline \n";

#$tabfoot = "\n".'\end{tabular}'."\n".'\end{minipage}'."\n".'\hspace{0.1cm}~'."\n\n";
$tabfoot = "\n".'\end{tabular}'."\n"."\n\n";


$tabpichead ='\begin{tabular}{|c||c|c|c||c|} \hline'."\n".
           "\\langdomsize   & before GEMM   & in GEMM & after GEMM & \\\\ \n" .
           "\\langparameter & \$L+EU^{-1}\$ & \$L+EU^{-1}+\\tilde{S}\$ & \$L+L_S+S\$ & \\\\ \n \\hline \n \\hline \n";

$tabpicfoot = "\n".'\end{tabular}'."\n"."\n\n";

$tabithead ='\begin{tabular}{|c||c|c|c|} \hline'."\n".
    "\\langdomsize   & One iter  & PrecSolve & ShurProd \\\\ \n" .
    "\\langparameter & (\\langsecond) & (\\langsecond) & (\\langsecond) \\\\ \n \\hline \n \\hline \n";

$tabitfoot = "\n".'\end{tabular}'."\n"."\n\n";

$tabdetailhead ='\begin{tabular}{|c||c|c|c|c||c|} \hline'."\n".
    "\\langdomsize   & Facto L  & TRSM E & GEMM S & Facto S & \\langfacto \\\\ \n" .
    "\\langparameter & (\\langsecond) & (\\langsecond) & (\\langsecond)  & (\\langsecond) & (\\langsecond) \\\\ \n \\hline \n \\hline \n";

$tabdetailfoot = "\n".'\end{tabular}'."\n"."\n\n";

open(F, ">Table.tex");
print F $fichhead;

@listerep1 = `ls -d RESULT-*`;

$Droptoltabhead1 = '\begin{tabular}{|c||';
$Droptoltabhead2 = '\langdomsize    ';
$Droptoltabhead3 = '\langparameter  ';

foreach $rep1 (@listerep1)
{
    chop $rep1;
    chdir $rep1;

    $droptol1 = $rep1;
    $droptol1 =~ s/RESULT-//;

    print "Traite repertoire $rep1 ($droptol1)\n";

    @listerep = `ls -d RESULT_*`;

    $prevmatname="";
    
    $Droptoltabhead1 = $Droptoltabhead1.'c|';
    $Droptoltabhead2 = $Droptoltabhead2."& droptol1 = $droptol1";
    $Droptoltabhead3 = $Droptoltabhead3.'& \langfill ';
    
#boucle sur les RESULT_ 
foreach $rep (@listerep)
{

    if($gfw_tab == 1) {
	$Gtabhead1 = '\begin{tabular}{|c||c|c|c|c|';
	$Gtabhead2 = '\langdomsize    &  \langnumberof & \langnumberof & \langnumberof & dim(S)/n';
	$Gtabhead3 = '\langparameter  &  \langdomains   &  \langlevels   & \langconnectors  &  (\langpercent)   ';
	
	$Ftabhead1 = '\begin{tabular}{|c||';
	$Ftabhead2 = '\langdomsize    ';
	$Ftabhead3 = '\langparameter  ';
	
	$Wtabhead1 = '\begin{tabular}{|c||c|';
	$Wtabhead2 = '\langdomsize    & \langmethod \, '."$stratcmp";
	$Wtabhead3 = '\langparameter  & \langopc         ';
    }

    chop $rep;

    $matname = $rep;
    $matname =~ s/RESULT_//;
    $matname =~ s/_.*//;

    if($matname !~ /$prevmatname/) {
	$prevmatname = $matname;
	$toprint .= '\section{'."Cas test $matname : droptol1 = $droptol1"."\}\n\n\n";
    }
    
    $rsa = 0;


    $rep =~ /LOC_(.*)_/;
    $loclv = $1;
    $toprint .= '\subsection{'."Cas test $matname : droptol1 = $droptol1, locally = $loclv"."\}\n\n\n";

    $dir = `pwd`;
    chop $dir;

    chdir $rep;
    print "Traite repertoire $rep \n";

    @listesubrep = `ls -d ${clef1}_NOREC`;
    push @listesubrep ,`ls -d ${clef1}_[0-9]`;
    
#    @listesubrep = `ls -d ${clef1}_[0-9]`;

    if(@listesubrep == 0)
    {
	print "$rep est vide : ON l'OUBLIE ! \n";
#       system "ls -d ${clef1}_[0-9]";
	next;
    }

    
    my %tt;
    my %vv;
    my %uu;
    my %itnorec;

    $flag = 0;

    #### Boucle sur les sous-repertoires STRAT  #####
    foreach $subrep (@listesubrep)
    {

	chop $subrep;
	$strat = $subrep;
	$strat =~ s/${clef1}_//;

	$subdir =  `pwd`;
	chop $subdir;
	chdir $subrep;
	print "Traite $subrep \n";
	
	@files = `ls ${matname}_*`;
	
	if(@files == 0)
	{
	    print "$subrep est vide : ON l'OUBLIE ! \n";
	    next;
	}
	
	if($gfw_tab == 1) {
	    $Ftabhead1 = $Ftabhead1.'c|';
	    $Ftabhead2 = $Ftabhead2."& \\langmethod \\, $strat ";
	    $Ftabhead3 = $Ftabhead3.'& \langfill ';
		
	    if( $strat !~ /$stratcmp/)
	    {
		$Wtabhead1 = $Wtabhead1.'c|';
		$Wtabhead2 = $Wtabhead2."& \\langmethod \\, $strat ";
		$Wtabhead3 = $Wtabhead3.'&  \langitereqabrev   ';
	    }
	}

	my %m;

	foreach $f (@files)
	{
	    chop $f;

	    open (M, "$f");
	    @st = <M>;
	    close M;
	    $fact = "0.0";

	    
	    $L0 = 0;
	    $U0 = 0;
	    $D0 = 0;
	   
	    $E0 = 0;
	    $F0 = 0;
	    $S0 = 0;
	    $C0 = 0;

	    $L1 = 0;
	    $U1 = 0;
	    $D1 = 0;

       	    $picMax=-1;
	    $picBeforeGemm=-1;
	    $picInGemmWithout=-1;
	    $picInGemmWith=-1;
	    $picAfterGemm=-1;

	    $itOneIter=-1;
	    $itPrecSolve=-1;
	    $itShurProd=-1;

	    $precondL=-1;
	    $precondE=-1;
	    $precondS=-1;
	    $precondSL=-1;

	    foreach (@st)
	    {
		
		#if( /($factokey)([^\d]*)(\d*\.\d{2})/)
		#{
		#    $fact = $3 if($3 gt $fact);
		#}

		$fact = $3 if /($factokey)([^\d]*)(\d*\.\d*)/;
#		$fill = $3 if /($fillkey)([^\d]*)(\d*\.\d*)/;
		$solve = $3 if /($solvekey)([^\d]*)(\d*\.\d*)/;
		$iter = $3 if /($iterkey)([^\d]*)(\d*)/;
		$inner = $3 if /($innerkey)([^\d]*)(\d*)/;
		$np = $3 if /($domsizekey)([^\d]*)(\d*)/;

		$levelnbr  = $3 if /($levelnbrkey)([^\d]*)(\d*)/;
		$connectnbr = $3 if /($connectorkey)([^\d]*)(\d*)/;
		$nnzA = $3 if /($nnzAkey)([^\d]*)(\d*)/;
		$domnbr = $3 if /($domnbrkey)([^\d]*)(\d*)/;
		$indim = $3 if /($indimkey)([^\d]*)(\d*)/;
		$dim = $3 if /($dimkey)([^\d]*)(\d*)/;

#		$picMax = $3 if /($picMaxkey)([^\d]*)(\d*)/;
		$picBeforeGemm = $3 if /($picBeforeGemmkey)([^\d]*)(\d*)/;
		$picInGemmWithout = $3 if /($picInGemmWithoutkey)([^\d]*)(\d*)/;
		$picInGemmWith = $3 if /($picInGemmWithkey)([^\d]*)(\d*)/;
		$picAfterGemm = $3 if /($picAfterGemmkey)([^\d]*)(\d*)/;

		$itOneIter = $3 if /($itOneIterkey)([^\d]*)(\d*\.\d*)/;
		$itPrecSolve = $3 if /($itPrecSolvekey)([^\d]*)(\d*\.\d*)/;
		$itShurProd = $3 if /($itShurProdkey)([^\d]*)(\d*\.\d*)/;

		$precondL  = $3 if /($precondLkey)([^\d]*)(\d*\.\d*)/;
		$precondE  = $3 if /($precondEkey)([^\d]*)(\d*\.\d*)/;
		$precondS  = $3 if /($precondSkey)([^\d]*)(\d*\.\d*)/;
		$precondSL = $3 if /($precondSLkey)([^\d]*)(\d*\.\d*)/;

		if( /RSA\sformat/ )
		{
		    $rsa = 1;
		}

		$L0 = $3 if /(L\(0\))([^\d]*)(\d*)/;
		$U0 = $3 if /(U\(0\))([^\d]*)(\d*)/;
		$D0 = $3 if /(D\(0\))([^\d]*)(\d*)/;
		$E0 = $3 if /(E\(0\))([^\d]*)(\d*)/;
		$F0 = $3 if /(F\(0\))([^\d]*)(\d*)/;
		$S0 = $3 if /(S\(0\))([^\d]*)(\d*)/;
		$C0 = $3 if /(C\(0\))([^\d]*)(\d*)/;

		$L1 = $3 if /(L\(1\))([^\d]*)(\d*)/;
		$U1 = $3 if /(U\(1\))([^\d]*)(\d*)/;
		$D1 = $3 if /(D\(1\))([^\d]*)(\d*)/;


		if(/$endrun/)
		{
#		    if($rsa == 1) {
			$nnzA = getnum($nnzA);
#		    }
#		    $fill = round($fill);
			
#		    $picMax = round(getnum($picMax)/$nnzA); 
		    $picBeforeGemm =  round(getnum($picBeforeGemm)/$nnzA);
		    $picInGemmWithout = round(getnum($picInGemmWithout)/$nnzA);
		    $picInGemmWith = round(getnum($picInGemmWith)/$nnzA);
		    $picAfterGemm = round(getnum($picAfterGemm)/$nnzA);

		    $picMax = max $picInGemmWith, $picAfterGemm;

                    $bftest =($picInGemmWith < $picAfterGemm);

		    $picMax =  sprintf("%.2f", $picMax);
		    $picBeforeGemm =  sprintf("%.2f", $picBeforeGemm);
		    $picInGemmWithout = sprintf("%.2f", $picInGemmWithout);
		    $picInGemmWith = sprintf("%.2f", $picInGemmWith);
		    $picAfterGemm = sprintf("%.2f", $picAfterGemm); 

#        	    $itOneIter = round(getnum($itOneIter));
#		    $ititOneiter = round(getnum($itPrecSolve));
#		    $itShurProd = round(getnum($itShurProd));

        	    $itOneIter = sprintf("%.2f", $itOneIter);
        	    $itPrecSolve = sprintf("%.2f", $itPrecSolve);
        	    $itShurProd = sprintf("%.2f", $itShurProd);

			$precondL = sprintf("%.2f", $precondL);
			$precondE = sprintf("%.2f", $precondE);
			$precondS = sprintf("%.2f", $precondS);
			$precondSL = sprintf("%.2f", $precondSL);

		    if($fact == 0)
		    {
			print "$f : FACT NULL \n";
		    }
		    $interf = (($dim-$indim) / $dim)* 100.0;
#		    $interf =~ /(\d*\.\d*)/;
#		    $interf = $1;

		    $total = $fact + $solve;
		    $interf = sprintf("%.2f", $interf);
		    
		    $fact = sprintf("%.2f", $fact);
		    $solve = sprintf("%.2f", $solve);
		    $total = sprintf("%.2f", $total);

		    if($strat =~ /NOREC/ || $strat =~ /0/)
		    {
			$m{$np} = "$fact & $solve & $total & $iter & $picMax \\\\";
		    }
		    else
		    {
			$m{$np} = "$fact & $solve & $total & $inner & $picMax \\\\";
		    }

        	    if($bftest)
		    {
			$mpic{$np} = "$picBeforeGemm & $picInGemmWith & \\textbf{$picAfterGemm} & $picInGemmWithout\\\\";
		    }
		    else
		    {
			$mpic{$np} = "$picBeforeGemm & \\textbf{$picInGemmWith} & $picAfterGemm & $picInGemmWithout\\\\";
		    }

			$mit{$np} = "$itOneIter & $itPrecSolve & $itShurProd\\\\";

			$mdetail{$np} = "$precondL & $precondE & $precondS & $precondSL & $fact \\\\";

		    $itcost = &itercost;


############
			print "Fill : {$loclv}{$strat}{$np}\n";
			$Fill1uu{$loclv}{$strat}{$np} = $Fill1uu{$loclv}{$strat}{$np}." & $picMax ";
		
###########

		    if($flag == 0)
		    {
			if($compare_norec == 1) {
			    if($strat !~ /$stratcmp/)
			    {
				print "ERROR directory $stratcmp should be treated first !!! \n";
				exit;
			    }
			}
			
			$tt{$np} = "$domnbr & $levelnbr & $connectnbr & $interf ";
#			$uu{$np} = " $fill ";			
			$uu{$np} = " $picMax ";
			
			$w = sprintf("%g", $itcost);
			$vv{$np} = " $w ";
			$itnorec{$np} = $itcost;
			#$itcostnorec = $itcost;
#			print "Strat $strat: update itcostnorec\[$np\] $itnorec{$np} \n";
		    }
			else
			{
#			$uu{$np} = $uu{$np}." & $fill ";
			$uu{$np} = $uu{$np}." & $picMax ";
			
			if($itcost == 0)
			{
			    $g = 0.0;
			}
			else
			{
			    $g = $itnorec{$np} / $itcost;
			}

			$w = sprintf("%.2f", $g);
			$vv{$np} = $vv{$np}." & $w ";
		    }
		    
		    $fact = 0.0;
#		    $fill = 0.0;
		    $solve = 0.0;
		    $iter = 0.0;
		    $total = 0.0;

		}
	    }
 	}

############
    if($gfw_tab2 == 1) {
	if($latex_tab == 1) {
	    print F '\begin{table}[H]'."\n";
	}
	@p = keys %tt;
	@p = sort { $a <=> $b} @p;
	if($latex_tab == 1) {
	    print F '\caption{'."$matname: n = $dim nnzA = $nnzA, \\langnblocallylevels \\, = $loclv"."\}\n";
	}
	print F '\begin{center}'."\n";
	print F $Gtabhead1.'} \hline'."\n";
	print F $Gtabhead2." \\\\ \n";
	print F $Gtabhead3."    \\\\ \n \\hline \n \\hline \n";
	for($i=0;$i<@p-1;$i++)
	{
	    $np = $p[$i];
	    print F "$np & $tt{$np} \\\\ \n";
	}
	$np = $p[@p-1];
	print F "$np & $tt{$np} \\\\ \\hline \n";
	print F $tabfoot;
	print F '\end{center}'."\n";
	if($latex_tab == 1) {
	    print F '\end{table}'."\n";
	}
	
	$gfw_tab2 = 0;
    }
	print F '\newpage';
	print F $toprint;
	$toprint = '';
############

	print F '\subsubsection{'."Cas test $matname : droptol1 = $droptol1, locally = $loclv".", method = $strat"."\}\n\n\n";

 	if($latex_tab == 1) {
	    print F '\begin{table}[H]'."\n";
	}
	@p = keys %m;
	@p = sort { $a <=> $b} @p;
 	if($latex_tab == 1) {
	    print F '\caption{'."$matname: method = $strat"."\}\n";
	}
	print F '\begin{center}'."\n";
	print F $tabhead;
	for($i=0;$i<@p-1;$i++)
	{
	    $np = $p[$i];
	    print F "$np & $m{$np} \n";
	}
	$np = $p[@p-1];
	print F "$np & $m{$np} \\hline \n";
	print F $tabfoot;
	###
	print F '\end{center}'."\n";
 	if($latex_tab == 1) {
	    print F '\end{table}'."\n";
	}


	# Detail
 	if($latex_tab == 1) {
	    print F '\begin{table}[H]'."\n";
	}
	@p = keys %mit;
	@p = sort { $a <=> $b} @p;
 	if($latex_tab == 1) {
	    print F '\caption{'."$matname: method = $strat"."\}\n";
	}
	print F '\begin{center}'."\n";
	print F $tabdetailhead;
	for($i=0;$i<@p-1;$i++)
	{
	    $np = $p[$i];
	    print F "$np & $mdetail{$np} \n";
	}
	$np = $p[@p-1];
	print F "$np & $mdetail{$np} \\hline \n";
	print F $tabdetailfoot;
	###
	print F '\end{center}'."\n";
 	if($latex_tab == 1) {
	    print F '\end{table}'."\n";
	}


	# Pic Tab.
 	if($latex_tab == 1) {
	    print F '\begin{table}[H]'."\n";
	}
	@p = keys %mpic;
	@p = sort { $a <=> $b} @p;
 	if($latex_tab == 1) {
	    print F '\caption{'."$matname: method = $strat"."\}\n";
 	}
	print F '\begin{center}'."\n";
	print F $tabpichead;
	for($i=0;$i<@p-1;$i++)
	{
	    $np = $p[$i];
	    print F "$np & $mpic{$np} \n";
	}
	$np = $p[@p-1];
	print F "$np & $mpic{$np} \\hline \n";
	print F $tabpicfoot;
	###
	print F '\end{center}'."\n";
 	if($latex_tab == 1) {
	    print F '\end{table}'."\n";
	}


	# Iter.
 	if($latex_tab == 1) {
	    print F '\begin{table}[H]'."\n";
	}
	@p = keys %mit;
	@p = sort { $a <=> $b} @p;
 	if($latex_tab == 1) {
	    print F '\caption{'."$matname: method = $strat"."\}\n";
	}
	print F '\begin{center}'."\n";
	print F $tabithead;
	for($i=0;$i<@p-1;$i++)
	{
	    $np = $p[$i];
	    print F "$np & $mit{$np} \n";
	}
	$np = $p[@p-1];
	print F "$np & $mit{$np} \\hline \n";
	print F $tabitfoot;
	###
	print F '\end{center}'."\n";
 	if($latex_tab == 1) {
	    print F '\end{table}'."\n";
 	}


	chdir $subdir;
	$flag++;
    }

    if($gfw_tab == 1) {
	print F '\newpage'. "\n";
    }
    ########################################
    if($gfw_tab == 1) {
	########################################
	if($latex_tab == 1) {
	    print F '\begin{table}[H]'."\n";
	}
	@p = keys %uu;
	@p = sort { $a <=> $b} @p;
	if($latex_tab == 1) {
	    print F '\caption{'."$matname: n = $dim nnzA = $nnzA, \\langnblocallylevels \\, = $loclv"."\}\n";
	}
	print F '\begin{center}'."\n";
	print F $Ftabhead1.'} \hline'."\n";
	print F $Ftabhead2." \\\\ \n";
	print F $Ftabhead3."    \\\\ \n \\hline \n \\hline \n";
	for($i=0;$i<@p-1;$i++)
	{
	    $np = $p[$i];
	    print F "$np & $uu{$np} \\\\ \n";
	}
	$np = $p[@p-1];
	print F "$np & $uu{$np} \\\\ \\hline \n";
	print F $tabfoot;
	print F '\end{center}'."\n";
	if($latex_tab == 1) {
	    print F '\end{table}'."\n";
	}
	###################################
	if($compare_norec == 1) 
	{
	    if($latex_tab == 1) {
		print F '\begin{table}[H]'."\n";
	    }
	    @p = keys %vv;
	    @p = sort { $a <=> $b} @p;
	    if($latex_tab == 1) {
		print F '\caption{'."$matname: n = $dim nnzA = $nnzA, \\langnblocallylevels \\, = $loclv"."\}\n";
	    }
	    print F '\begin{center}'."\n";
	    print F $Wtabhead1.'} \hline'."\n";
	    print F $Wtabhead2." \\\\ \n";
	    print F $Wtabhead3."    \\\\ \n \\hline \n \\hline \n";
	    for($i=0;$i<@p-1;$i++)
	    {
		$np = $p[$i];
		print F "$np & $vv{$np} \\\\ \n";
	    }
	    $np = $p[@p-1];
	    print F "$np & $vv{$np} \\\\ \\hline \n";
	    print F $tabfoot;
	    print F '\end{center}'."\n";
	    if($latex_tab == 1) {
		print F '\end{table}'."\n";
	    }
	}
	print F '\newpage'. "\n";
    }
    chdir $dir;
}
#    print F '\newpage'. "\n";
    chdir '..';
}


@$tab = ('0', '1', "ALL"); # bug fix

#foreach $l (@$tab) {
#    print "locally = $l\n";
# 
#foreach $s (sort keys %{$Fill1uu{$l}}) {
#    print " method = $s\n";
#    
#    foreach $np (sort keys %{$Fill1uu{$l}{$s}}) {
#	print "  np = $np\n";
#	
#    }
#
#}
#}

print F '\section{'."Cas test $matname : comparaisons"."\}\n\n\n";

# # # # #
foreach $l (@$tab) {

print F '\subsection{'."Cas test $matname : locally = $l"."\}\n\n\n";
  
foreach $s (sort keys %{$Fill1uu{$l}}) {
     
print F '\subsubsection{'."Cas test $matname : locally = $l".", method = $s"."\}\n\n\n";

#########
if($latex_tab == 1) {
    print F '\begin{table}[H]'."\n";
}

@p = keys %{$Fill1uu{$l}{$s}};
@p = sort { $a <=> $b} @p;
if($latex_tab == 1) {
    print F '\caption{'."$matname: n = $dim nnzA = $nnzA, \\langnblocallylevels \\, = $loclv"."\}\n";
}
print F '\begin{center}'."\n";
print F $Droptoltabhead1.'} \hline'."\n";
print F $Droptoltabhead2." \\\\ \n";
print F $Droptoltabhead3."    \\\\ \n \\hline \n \\hline \n";
for($i=0;$i<@p-1;$i++)
{
    $np = $p[$i];
    print F "$np $Fill1uu{$l}{$s}{$np} \\\\ \n";
}
$np = $p[@p-1];
print F "$np $Fill1uu{$l}{$s}{$np} \\\\ \\hline \n";
print F $tabfoot;
print F '\end{center}'."\n";
if($latex_tab == 1) {
    print F '\end{table}'."\n";
}
#########

}
}
# # # # #




print F $fichfoot;    
close F;

sub itercost
{
    if($rsa == 1)
    {
	if($strat =~ /0/)
	{
	    $c = $nnzA + 2*(2 * $L0 + $D0) + 2 * $E0 + 2 * $L1 + $D1;
	}
	elsif($strat =~ /1/)
	{
	    $c = $S0 + 2 * $L1 + $D1;
	}
	elsif($strat =~ /2/)
	{
	    $c = ((2*$L0+$D0) + 2*$E0+$C0) +  2*$L1+$D1;
	}
	elsif($strat =~ /NOREC/)
	{
	    $c = $nnzA + 2*$L0+$D0;
	}
	else
	{
	    print "Erreur dans itercost : $strat n\'est pas une strategie \n";
	    exit;
	}
    }
    else
    {
	if($strat =~ /0/)
	{
	    $c = $nnzA + 2*($L0 + $U0) + $E0 + $F0 + $L1 + $U1;
	}
	elsif($strat =~ /1/)
	{
	    $c = $S0 + $L1 + $U1;
	}
	elsif($strat =~ /2/)
	{
	    $c = ($L0+$U0) + $E0+$F0 + $C0 + $L1 + $U1;
	}
	elsif($strat =~ /NOREC/)
	{
	    $c = $nnzA + $L0+$U0;
	}
	else
	{
	    print "Erreur dans itercost : $strat n\'est pas une strategie \n";
	    exit;
	}
    }

    $c;
}

/* @authors J. GAIDAMOUR, P. HENON */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "io.h"
#include "base.h"
#include "block.h"

#define METIS_ORDER

#ifdef METIS_ORDER
#include "metis.h"
#endif

#define BUFLEN 200

/*TODO : unsym version */

extern void find_supernodes(int n, int *ia, int *ja, int *perm, int *iperm, int *snodenbr, int *snodetab, int *treetab);
extern void ascend_column_reorder(csptr mat);
extern void CSR_Fnum2Cnum(int *ja, int *ia, int n);

void csr2SolverMatrix(SolverMatrix* solvmtx, csptr mat);

/*DEBUG*/
extern int testSymbolicFacto(MatrixStruct* mtxstr, int cblknbr, int* treetab, int* rangtab);
extern void testDecoupe(SymbolMatrix* symbmtx, int*, int*);
extern void test2Decoupe(SymbolMatrix* symbmtx);

int main(int argc, char *argv[])
{
  int UN = 1;
  int i;
    
  /* declarations related to Harwell-boeing format for reading the HB
     matrix. Second part is related to I/O parameters */
  int n, nnz, job;

  REAL nnzA, nnzL;

  /* MATRIX */
  COEF *a;
  int *ja, *ia;
  int numflag;  /* C or Fortran */
  
  int *jg, *ig;
  int metisoption[10]; /* Metis */

  /* Symmetrize the matrix */
  int *ib, *jb;
  
  /* 
   * buf is for read from 'inputs' file, 
   * matrix represents matrix name
   */
  int unsym; /* unsym  --  symmetric pattern(0), nonsym pattern(1) */
  char matrix[BUFLEN];
  char sfile_path[BUFLEN];
  int rsa;

  int *perm, *iperm;

  int cblknbr;  /* number of supernodes */
  int* rangtab; /* rangetab[i] is the beginning in the new ordering of the ith supernodes */
  int* treetab; /* elimination tree */ 

  chrono_t t1,t2,ttotal;

  /* Matrices */
  MatrixStruct* mtxstr;
  SolverMatrix* solvmtx;
  csptr mat;

  /* Vectors */
  COEF *x;
  COEF *b;
  REAL ro; /* norm */

  PhidalOptions phidaloptions;
  PhidalOptions* option= &phidaloptions;
  PhidalOptions_Init(&phidaloptions);
  
  /** Read parameters from the input file **/
  GENERAL_setpar(NULL, matrix, sfile_path, &unsym, &rsa, &phidaloptions);   

  /************************************************************************************************************/
  /****************************************** READ THE MATRIX *************************************************/
  /************************************************************************************************************/

  CSRread(matrix, &n, &nnz, &ia, &ja, &a, NULL, NULL);
  assert(phidaloptions.symmetric == rsa);


  nnzA = nnz; /* apres symetrize matrix = (ia[n]+n)/2.0; */
  fprintfv(5, stdout, "Number of NNZ in A (CSR) = %g \n", nnzA);

  /************************ RSA matrix contains only the lower triangular part of the matrix ***************************/
  if(rsa == 1)
    {
      /** Symmetrize the matrix (in rsa we only have the lower triangular part) ****/
      ib = ia;
      jb = ja;
      b = a;
      numflag = 1;
      job = 2;
      PHIDAL_SymmetrizeMatrix(job, numflag, n, ib, jb, b, &ia, &ja, &a);
      nnz = 2*nnz-n;
      unsym = 0;
      free(ib);
      free(jb);
      free(b);
    }

  /*******************************************************************************************************************/
  /*******************************************************************************************************************/
  /*******************************************************************************************************************/
  fprintfv(5, stdout,"Matrix dimension is %d, Number of nonzeros is %d\n",n,nnz);

  /* Translate matrix into C numbering */
  CSR_Fnum2Cnum(ja, ia, n);
  numflag = 0;

  /*****************************************************************************************************************/
  /***********************************Construct the symmetric graph G of the matrix ********************************/
  /*****************************************************************************************************************/
  ig = (int *)malloc(sizeof(int)*(n+1));
  jg = (int *)malloc(sizeof(int)*nnz);
  memcpy(ig ,ia, sizeof(int)*(n+1));
  memcpy(jg ,ja, sizeof(int)*nnz);
  
  if(unsym == 1)
    {
      ib = ig;
      jb = jg;
      /* numflag = 0; */
      job = 0;
      PHIDAL_SymmetrizeMatrix(job, numflag, n, ib, jb, NULL, &ig, &jg, NULL);
      free(ib);
      free(jb);
    }
  /*****************************************************************************************************************/
  /*****************************************************************************************************************/

  /************************************************************************************************************/
  /* Compute the reordering to minimize fill-in using METIS-4.0 ***********************************************/
  /************************************************************************************************************/
  metisoption[0] = 0;
 
  perm = (int *)malloc(sizeof(int)*n);
  iperm = (int *)malloc(sizeof(int)*n);

  /** Delete the self edge in the graph (METIS_NodeND need that)**/
  PHIDAL_CsrDelDiag(numflag, n, ig, jg);

#ifdef METIS_ORDER
  /**** Compute a matrix reordering that minimizes fill-in *******/
  METIS_NodeND(&n, ig, jg, &numflag, metisoption, perm, iperm);
#else
  fprintfv(5, stderr, "METTRE SCOTCH \n");
#endif
  free(ig);
  free(jg);

  /************************************************************************************************************/
  /* Find SuperNodes               ****************************************************************************/
  /************************************************************************************************************/
  fprintfv(5, stderr, "Find supernodes\n");

  rangtab = (int *)malloc(sizeof(int)*(n+1));
  treetab = (int *)malloc(sizeof(int)*n);

  t1  = dwalltime(); 
  find_supernodes(n, ia, ja, iperm, perm, &cblknbr, rangtab, treetab);
  t2  = dwalltime(); ttotal += t2-t1;
  fprintfv(5, stderr, " Find supernodes = %d, in %g seconds\n\n", cblknbr, t2-t1);

  /************************************************************************************************************/
  /* CSR -> SparRow                ****************************************************************************/
  /************************************************************************************************************/
  fprintfv(5, stdout, "Permuting the matrix \n");

  mat = (csptr)malloc(sizeof(struct SparRow));
  CSRcs(n, a, ja, ia, mat);
  
  /** DO NOT NEED THESE VECTORS ANYMORE **/
  free(a);
  free(ja);
  free(ia);
  
  /*** Permute the matrix according to the phidal ordering ***/
  t1  = dwalltime(); 
  CS_Perm(mat, iperm);
  t2  = dwalltime(); 
  fprintfv(5, stdout, " Permute the matrix in %g seconds \n", t2-t1);
  free(perm);
  free(iperm); /**/
  
  t1  = dwalltime(); 
  ascend_column_reorder(mat);
  t2  = dwalltime(); 
  fprintfv(5, stdout, " Ascend_column_reorder in %g seconds \n\n", t2-t1);

  /************************************************************************************************************/
  /* Symbolic Block Factorisation  ****************************************************************************/
  /************************************************************************************************************/
  fprintfv(5, stdout, "Symbolic Factorisation\n");
  ttotal = 0;

  /* Build a sorted list of intervalls for each columns */
  t1  = dwalltime(); 
  mtxstr = (MatrixStruct*)malloc(sizeof(MatrixStruct)*cblknbr);
  symbolicBlok(mtxstr, mat, cblknbr, rangtab);
  t2  = dwalltime(); ttotal += t2-t1;
  fprintfv(5, stderr, " Compute block structure in %g seconds\n", t2-t1);

  /* Symbolic factorisation : compute block fill-in */
  t1  = dwalltime(); 
  symbolicFacto(mtxstr, cblknbr,  treetab,  rangtab); 
  t2  = dwalltime(); ttotal += t2-t1;
  fprintfv(5, stderr, " Symbolic Factorisation  in %g seconds\n", t2-t1);
  free(treetab); 

  /* Build SymbolMatrix structure from mtxstr */ 
  t1  = dwalltime(); 

  solvmtx = (SolverMatrix*)malloc(sizeof(SolverMatrix));
  MatrixStruct2SymbolMatrix(&solvmtx->symbmtx, mtxstr, cblknbr, rangtab);
  free(rangtab);  
  freeMatrixStruct(mtxstr, cblknbr);

  /* Store in solvmtx corresponding to symbmtx */
  csr2SolverMatrix(solvmtx, mat);

  t2  = dwalltime(); ttotal += t2-t1;
  fprintfv(5, stderr, " Build Symb+SolverMatrix in %g seconds\n", t2-t1);
    
  fprintfv(5, stdout, "= %g seconds \n\n", ttotal);

  nnzL = SymbolMatrix_NNZ(&solvmtx->symbmtx) + n;
  fprintfv(5, stdout, " Fill Ratio = %g\n\n",  nnzL / nnzA);

  /************************************************************************************************************/
  /* Numeric Factorisation         ****************************************************************************/
  /************************************************************************************************************/
  fprintfv(5, stdout, "Numeric Factorisation\n"); 

  t1  = dwalltime(); 
  assert(rsa == 1);
  numericFacto(solvmtx);

  t2  = dwalltime(); 
  fprintfv(5, stderr, " Numeric Factorisation in %g seconds\n\n", t2-t1);

  /************************************************************************************************************/
  /* Compute a R.H.S               ****************************************************************************/
  /************************************************************************************************************/
  x = (COEF *)malloc(sizeof(COEF)*n);
  b = (COEF *)malloc(sizeof(COEF)*n); 
    
  for(i=0;i<n;i++)
    x[i] = 1.0;
  matvec(mat, x, b);

  /************************************************************************************************************/
  /* Solve (direct)                ****************************************************************************/
  /************************************************************************************************************/
  fprintfv(5, stdout,"DIRECT LDLtx=b\n");

  /* bzero(x, sizeof(COEF)*n); */

  t1 = dwalltime();  

  Lsolv(1, solvmtx, b, x);   /* solve Ly=b  */
  Dsolv(solvmtx,x);
  Ltsolv(1, solvmtx, x, x);  /* solve Ltx=b */
  
  t2 = dwalltime();
  fprintfv(5, stdout, " solve in %g seconds\n", t2-t1);

  {
    int i;
    for(i=0;i<n;i++)
      printfv(5, _coef_" ", pcoef(x[i]));
    printfv(5, "\n");
  }

  {
    int i;
    for(i=0;i<nnz;i++) {
      printfv(5, "coef : "_coef_"\n", pcoef(solvmtx->coeftab[i]));
    }
  }

  for(i = 0; i < n; i++) {
    x[i] = 1 - x[i];
  }
  ro = BLAS_NRM2(n,x,UN);

  fprintfv(5, stdout, " nrm2(sol-x)) = %e\n\n", ro);


#ifdef WITH_GMRES
  /************************************************************************************************************/
  /* Solve with gmres              ****************************************************************************/
  /************************************************************************************************************/
  fprintfv(5, stdout, "GMRES\n");

  bzero(x, sizeof(COEF)*n);
  t1 = dwalltime();
  fgmresd_blok(0, mat, solvmtx,  &phidaloptions, b, x, stdout);
  t2 = dwalltime();
  fprintfv(5, stdout, " GMRES in %g seconds \n\n", t2-t1);
  /* fprintfv(5, stdout, " Total Solve in %g seconds \n", t2-t1); */
 
#endif

  /************************************************************************************************************/
  /* Free Memory                   ****************************************************************************/
  /************************************************************************************************************/

  free(x);
  free(b);
  
  freeSolverMatrix(solvmtx);
  cleanCS(mat);
  free(mat);

  PhidalOptions_Clean(&phidaloptions);

  fprintfv(5, stdout, "END \n");
  return 0;
  }




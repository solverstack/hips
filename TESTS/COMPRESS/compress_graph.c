/* @authors J. GAIDAMOUR, P. HENON */

/************************************************************/
/**                                                        **/
/**   NAME       : compact_graph.c                         **/
/**                                                        **/
/**   AUTHOR     : Pascal HENON                            **/
/**                                                        **/
/**                                                        **/
/**   DATES      : # Version 0.0  : from : 10/08/2006      **/
/**                                                        **/
/**                                                        **/
/************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include "phidal_sequential.h"

int col_compare(int n1, int *ja1, int n2, int *ja2);

void compress_graph(csptr *P, int *snodetab, int *nodelist)
{
  /************************************************/
  /* This function compress a graph: all column   */
  /* of the graph that have the same pattern are  */
  /* merged                                       */
  /* The compressed graph is compute in place (P) */
  /* the set of unknowns that are in vertex i are */
  /* nodelist[snodetab[i]:snodetab[i+1][          */
  /* On entry/return:                             */
  /* P: a symmetric graph (with the diagonal !)   */
  /* iperm : the permutation that was 
  /* On return only                               */
  /* snodetab, iperm
  /* vwgt: the vertex weight (number of unknowns) */
  /************************************************/
  int i, j, k;
  int *colflag;
  int *ja;
  
  
#ifdef DEBUG_M
  for(i=0;i<P->n;i++)
    {
      k = 0;
      /** Check the row are sorted **/
      for(j=1;j<P->nnzrow[i];j++)
	assert(P->ja[j]>P->ja[j-1]);

      /** Check the diagonal is here **/
      for(j=0;j<P->nnzrow[i];j++)
	if(P->ja[j] == i)
	  k = 1;
      assert(k>0);
    }
#endif

  colflag = (int *)malloc(sizeof(int)*P->n);
  bzero(colflag, sizeof(int)*P->n);
  for(k=0;k<P->n;k++)
    {
      /** Find the set of column that can be merged 
	  in the set of columns that have a non-zero 
	  in the row k (NB the graph is symmetric !) **/
      ja = P->ja[k];
      for(i=0;i<P->nnzrow[k];k++)
	{
	  if(colflag[ja[i]] != 0)
	    continue;
	  
	}
    }
  

}




int col_compare(int n1, int *ja1, int n2, int *ja2)
{
  int i1, i2;
  i1 = 0;
  i2 = 0;
  while(i1 < n1 && i2 < n2)
    {
      if(ja1[i1] == ja2[i2])
	{
	  i1++;
	  i2++;
	  continue;
	}
      return 0;
    }

  return 1;
}


###
###  Compiler
###

ARCH       = -DSP2

CC	   = xlc       # C compiler 
MPICC      = mpcc_r
FC         = xlf       # Fortran compiler 
MPIFC      = mpif90
LD	   = $(FC)     # Linker
MPILD      = $(MPIFC)

CFLAGS	   = -q64      # Additional C compiler flags
FFLAGS	   = -q64      # Additional Fortran compiler flags
LFLAGS     =           # Additional linker flags

COPTFLAGS2 = -qarch=pwr4 -O3 -qtune=pwr4 -qstrict -qlanglvl=extended       # Optimization flags
FOPTFLAGS2 = -O3       # 

###
###  Library
###

IBLAS      =           # BLAS include path
LBLAS      = -lessl    # BLAS linker flags

IMPI       =           # Additional MPI include path
LMPI       =           # Additional MPI linker flags

##   METIS_DIR : path to METIS
METIS_DIR  = $(HOME)/lib/metis-4.0
IMETIS     = -I$(METIS_DIR)/Lib
LMETIS2    = -L$(METIS_DIR) -lmetis

##   SCOTCH_DIR : path to SCOTCH
SCOTCH_DIR = $(HOME)/lib/scotch_5.1
ISCOTCH    = -I$(SCOTCH_DIR)/include
LSCOTCH2   = -L$(SCOTCH_DIR)/lib -lscotch -lscotcherr

###
###  Misc
###

MAKE	   = make
AR	   = /usr/bin/ar
ARFLAGS	   = -ruvs -X64
LN	   = ln
CP	   = cp


###
###  Compiler
###

ARCH       = -DLINUX

CC         = icc       # C compiler
MPICC      = icc
FC         = ifort     # Fortran compiler
MPIFC      = ifort
LD         = $(CC)     # Linker
MPILD      = $(MPICC)

CFLAGS     =           # Additional C compiler flags
FFLAGS     =           # Additional Fortran compiler flags
LFLAGS     = -L/work/SGI/intel/fce/10.1.017/lib/ -lifcore

COPTFLAGS2 = -O3       # Optimization flags
FOPTFLAGS2 = -O3       #

###
###  Library
###

IBLAS      =           # BLAS include path
LBLAS      = -L/work/SGI/intel/mkl/10.0.3.020/lib/em64t -lmkl_intel_lp64 -lmkl_sequential -lmkl_core -liomp5 -lpt\hread

IMPI       =           # Additional MPI include path
LMPI       = -lmpi     # Additional MPI linker flags

##   METIS_DIR : path to METIS
METIS_DIR  = /scratch/gaidamou/lib/metis-4.0
IMETIS     = -I$(METIS_DIR)/Lib
LMETIS2    = -L$(METIS_DIR) -lmetis

##   SCOTCH_DIR : path to SCOTCH
SCOTCH_DIR = /scratch/gaidamou/lib/scotch_5.1
ISCOTCH    = -I$(SCOTCH_DIR)/include
LSCOTCH2   = -L$(SCOTCH_DIR)/lib -lscotch -lscotcherr

###
###  Misc
###

MAKE       = make
AR         = ar
ARFLAGS    = -crs
LN         = ln
CP         = cp



###
###  Compiler
###

ARCH       = -DLINUX

CC	   = icc       # C compiler 
MPICC      = mpicc -cc=icc
FC         = ifort  # Fortran compiler 
MPIFC      = mpif90 -f90=ifort
LD	   = $(FC) -nofor_main    # Linker
MPILD      = $(MPIFC) -nofor_main


CFLAGS	   =           # Additional C compiler flags
FFLAGS	   =           # Additional Fortran compiler flags
LFLAGS     =           # Additional linker flags

COPTFLAGS2 = -O3       # Optimization flags
FOPTFLAGS2 = -O3       # 

###
###  Library
###

IBLAS      =           # BLAS include path
#LBLAS      = -lblas    # BLAS linker flags
LBLAS      =  -L/opt/intel/mkl/10.1.2.024/lib/32/   -lmkl_intel -lmkl_sequential -lmkl_core

IMPI       =           # Additional MPI include path
LMPI       =           # Additional MPI linker flags

##   METIS_DIR : path to METIS
#METIS_DIR  = /opt/metis/int32/
METIS_DIR  = $(HOME)/travail/lib/metis-4.0-intel
IMETIS     = -I$(METIS_DIR)/Lib
LMETIS2    = -L$(METIS_DIR) -lmetis

##   SCOTCH_DIR : path to SCOTCH
#SCOTCH_DIR = /opt/scotch/int32
SCOTCH_DIR = $(SCOTCH_HOME)
ISCOTCH    = -I$(SCOTCH_DIR)/include
LSCOTCH2   = -L$(SCOTCH_DIR)/lib -lscotch -lscotcherr

###
###  Misc
###

MAKE	   = make
AR	   = ar
ARFLAGS	   = -crs
LN	   = ln
CP	   = cp

###
###  Compiler
###

ARCH       = -DLINUX

CC	   = icc       # C compiler 
MPICC      = mpicc
FC         = ifort     # Fortran compiler 
MPIFC      = mpif90
LD	   = $(FC) -nofor_main    # Linker
MPILD      = $(MPIFC) -nofor_main

CFLAGS	   =           # Additional C compiler flags
FFLAGS	   =           # Additional Fortran compiler flags
LFLAGS     =           # Additional linker flags

COPTFLAGS2 = -O3       # Optimization flags
FOPTFLAGS2 = -O3       # 

###
###  Library
###

IBLAS      =           # BLAS include path
#LBLAS     = -L/applications/intel/mkl/10.0.5.025/lib/64/ -lmkl -lguide -lpthread export /!\ OMP_NUM_THREADS=1 
LBLAS      = -L/applications/intel/mkl/10.0.5.025/lib/64/ -lmkl_intel_lp64 -lmkl_sequential -lmkl_core -lguide -lpthread
IMPI       =           # Additional MPI include path
LMPI       =           # Additional MPI linker flags

##   METIS_DIR : path to METIS
METIS_DIR  = $(HOME)/lib/metis-4.0
#METIS_DIR  = $(METIS_HOME)
IMETIS     = -I$(METIS_DIR)/Lib
LMETIS2    = -L$(METIS_DIR) -lmetis

##   SCOTCH_DIR : path to SCOTCH
SCOTCH_DIR = $(HOME)/lib/scotch_5.1
#SCOTCH_DIR = $(SCOTCH_HOME)
ISCOTCH    = -I$(SCOTCH_DIR)/include
LSCOTCH2   = -L$(SCOTCH_DIR)/lib -lscotch -lscotcherr

###
###  Misc
###

MAKE	   = make
AR	   = ar
ARFLAGS	   = -crs
LN	   = ln
CP	   = cp

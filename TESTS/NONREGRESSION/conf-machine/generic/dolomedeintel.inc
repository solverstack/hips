###
###  Compiler
###

ARCH       = -DLINUX

CC	   = icc       # C compiler 
MPICC      = mpicc -cc=$(CC)
#FC         = gfortran  # Fortran compiler 
FC         = ifort      # Fortran compiler 
MPIFC      = mpif90
LD	   = $(FC)                  # Linker
MPILD      = $(MPIFC) -f90=$(FC)

CFLAGS	   =           # Additional C compiler flags
FFLAGS	   =           # Additional Fortran compiler flags
LFLAGS     = -ldl -nofor-main          # Additional linker flags

COPTFLAGS2  = -O3       # Optimization flags
FOPTFLAGS2  = -O3       # 

###
###  Library
###
IBLAS      = -I/cvos/shared/apps/intel/mkl/10.1.0.015/include/
LBLAS      = -liomp5 -L/cvos/shared/apps/intel/mkl/10.1.0.015/lib/em64t -lmkl    # BLAS linker flags

IMPI       =           # Additional MPI include path
LMPI       =           # Additional MPI linker flags

##   METIS_DIR : path to METIS
METIS_DIR  = $(HOME)/lib/metis-4.0
IMETIS     = -I$(METIS_DIR)/Lib
LMETIS2    = -L$(METIS_DIR) -lmetis

##   SCOTCH_DIR : path to SCOTCH
SCOTCH_DIR = $(HOME)/lib/scotch_5.1
ISCOTCH    = -I$(SCOTCH_DIR)/include
LSCOTCH2   = -L$(SCOTCH_DIR)/lib -lscotch -lscotcherr

###
###  Misc
###

MAKE	   = make
AR	   = ar
ARFLAGS	   = -crs
LN	   = ln
CP	   = cp

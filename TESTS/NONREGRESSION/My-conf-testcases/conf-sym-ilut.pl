#!/usr/bin/perl -w

use strict;
use Submit::Submit;

require 'global.pl';

our %global;

my %configuration = (
#todo : faire une table de table de hash + quelque variables pour name et desc
		     'name'          =>     'sym-ilut',
		     'descr'         =>     'Test non regression en double ILUT',
		     
		     'prog'          =>     'testHIPS1.ex testHIPS1-Fortran.ex testHIPS2.ex testHIPS2-Fortran.ex testHIPS3.ex testHIPS3-Fortran.ex',
		     'compilversion' =>     'metis-optim-real-double-int64',
		     'param'         =>     '', # domsize
		     'nbproc'        =>     '1 2 4 8 16',
		     'driver'        =>     '',
		     
# inputs
		     'matfile'  =>     'bcsstk16.rsa',
		     'sym'      =>     '2',
		     'rhs'      =>     '0',
		     'method'   =>     'ILUT',
		     'prec'     =>     '1e-6',
		     'locally'  =>     '0 ALL',
		     'maxit'    =>     '100',
		     'krylov'   =>     '30',
		     'droptol0' =>     '0.001',
		     'droptol1' =>     '0.001',
		     'droptol2' =>     '0.001',
		     'verbose'  =>     '5',
		     
		     'memory'   =>     '0000',
		     'timelimit'=>     '1',
		     );


my @sort = ('matfile', 'prec', 'method', 'locally', 'droptol0', 'droptol1', 'droptol2', 'param', 'nbproc');
my $sort_option = 2;

Submit::generateTestCases(\%global, \%configuration, \@sort, $sort_option);

##
# $configuration{'name'}  = $configuration{'name'}.'-ilut';
# $configuration{'method'} = 'ILUT';
# $configuration{'param'} = '';

# Submit::generateTestCases(\%global, \%configuration, \@sort, $sort_option);

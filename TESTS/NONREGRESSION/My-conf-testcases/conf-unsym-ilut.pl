#!/usr/bin/perl -w

use strict;
use Submit::Submit;

require 'global.pl';

our %global;

my %configuration = (
		     'name'          =>     'unsym-ilut',
		     'descr'         =>     'Test non regression en double nonsym',
		     
		     'prog'          =>     'testHIPS1.ex testHIPS1-Fortran.ex testHIPS2.ex testHIPS2-Fortran.ex testHIPS3.ex testHIPS3-Fortran.ex',
		     'compilversion' =>     'metis-optim-real-simple-int64',
		     'param'         =>     '60', # domsize
		     'nbproc'        =>     '1 2 4',
		     'driver'        =>     '',
		     
# inputs
		     'matfile'  =>     'orsirr_1.rua sherman3.rua',
		     'sym'      =>     '0',
		     'rhs'      =>     '0',
		     'method'   =>     'ILUT',
		     'prec'     =>     '1e-6',
		     'locally'  =>     '0 ALL',
		     'maxit'    =>     '100',
		     'krylov'   =>     '30',
		     'droptol0' =>     '0.001',
		     'droptol1' =>     '0.001',
		     'droptol2' =>     '0.001',
		     'verbose'  =>     '5',
		     
		     'memory'   =>     '0000',
		     'timelimit'=>     '1',
		     );


my @sort = ('matfile', 'prec', 'method', 'locally', 'droptol0', 'droptol1', 'droptol2', 'param', 'nbproc');
my $sort_option = 2;

Submit::generateTestCases(\%global, \%configuration, \@sort, $sort_option);

##
$configuration{'name'}  = $configuration{'name'}.'-ilut';
$configuration{'method'} = 'ILUT';
$configuration{'param'} = '';

Submit::generateTestCases(\%global, \%configuration, \@sort, $sort_option);

#!/usr/bin/perl -w

use strict;
use Submit::Submit;

require 'global.pl';

our %global;

my %configuration = (
#todo : faire une table de table de hash + quelque variables pour name et desc
		     'name'          =>     'sym-hybrid',
		     'descr'         =>     'Test non regression en double HYBRID',
		     'prog'          =>     'testHIPS1.ex testHIPS1-Fortran.ex testHIPS2.ex testHIPS2-Fortran.ex testHIPS-RUN.ex',
		     'compilversion' =>     'scotch-optim-real-simple-int64 scotch-optim-real-double-int64',
		     'param'         =>     '100', # domsize
		     'nbproc'        =>     '1 2 4 8',
		     'driver'        =>     '',
		     
# inputs
		     'matfile'  =>     'bcsstk16.rsa',
		     'sym'      =>     '2',
		     'rhs'      =>     '0',
		     'method'   =>     'HYBRID',
		     'prec'     =>     '1e-6',
		     'locally'  =>     '0 ALL',
		     'maxit'    =>     '100',
		     'krylov'   =>     '100',
		     'droptol0' =>     '0.00',
		     'droptol1' =>     '0 0.001',
		     'droptol2' =>     '0 0.001',
		     'verbose'  =>     '5',
		     
		     'memory'   =>     '0000',
		     'timelimit'=>     '1',
		     );


my @sort = ('matfile', 'prec', 'method', 'locally', 'droptol0', 'droptol1', 'droptol2', 'param', 'nbproc');
my $sort_option = 2;

Submit::generateTestCases(\%global, \%configuration, \@sort, $sort_option);

##
# $configuration{'name'}  = $configuration{'name'}.'-ilut';
# $configuration{'method'} = 'HYBRID';
# $configuration{'param'} = '';

# Submit::generateTestCases(\%global, \%configuration, \@sort, $sort_option);

#!/usr/bin/perl -w

use strict;
use Submit::Submit;

require 'global.pl';

our %global;

my %configuration = (
		     'name'          =>     'complex-ilut',
		     'descr'         =>     'Test non regression en complex ILUT',
		     
		     'prog'          =>     'testHIPS1.ex testHIPS1-Fortran.ex testHIPS2.ex testHIPS2-Fortran.ex',
		     'compilversion' =>     'scotch-debug-complex-double-int32',
		     'param'         =>     '', # domsize
		     'nbproc'        =>     '1 2 4 8',
		     'driver'        =>     '',
		     
# inputs
		     'matfile'  =>     'young4c_.mtx',
		     'sym'      =>     '2',
		     'rhs'      =>     '0',
		     'method'   =>     'ILUT',
		     'prec'     =>     '1e-6',
		     'locally'  =>     '0 ALL',
		     'maxit'    =>     '100',
		     'krylov'   =>     '30',
		     'droptol0' =>     '0.001',
		     'droptol1' =>     '0.001',
		     'droptol2' =>     '0.001',
		     'verbose'  =>     '5',
		     
		     'memory'   =>     '0000',
		     'timelimit'=>     '1',
		     );


my @sort = ('matfile', 'prec', 'method', 'locally', 'droptol0', 'droptol1', 'droptol2', 'param', 'nbproc');
my $sort_option = 2;

Submit::generateTestCases(\%global, \%configuration, \@sort, $sort_option);

##
# $configuration{'name'}  = $configuration{'name'}.'-ilut';
# $configuration{'method'} = 'ILUT';
# $configuration{'param'} = '';

# Submit::generateTestCases(\%global, \%configuration, \@sort, $sort_option);

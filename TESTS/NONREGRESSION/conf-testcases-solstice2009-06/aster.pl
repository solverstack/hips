#!/usr/bin/perl -w

use strict;
use Submit::Submit;

require 'global.pl';

our %global;

my %configuration = (
         'name'          =>     'aster-seq-',
         'descr'         =>     'Tests SOLSTICE 06/2009',
         
         'prog'          =>     'testHIPS-RUN.ex',
         'compilversion' =>     'scotch-dbseq-real-double-int',
         'param'         =>     '1000 2000 3000 4000 5000 6000 7000 8000 9000 10000 11000 12000 13000 14000 15000', # domsize
         'nbproc'        =>     '1',
        
# inputs
         'matfile'  =>     'Aster.mm',
         'rhs'      =>     '0',
         'method'   =>     'HYBRID',
         'prec'     =>     '1e-4 1e-5 1e-6 1e-7 1e-8 1e-9 1e-10',
         'locally'  =>     'ALL',
         'maxit'    =>     '500',
         'krylov'   =>     '500',
         'droptol0' =>     '0',
         'droptol1' =>     '0',
         'droptol2' =>     '0',
         'verbose'  =>     '5',
         
         'memory'   =>     '0000',
         'timelimit'=>     '60',

                     'sym'      =>     '-1',
         'driver'   =>     '3',
    );

my @sort = ('matfile', 'prec', 'method', 'locally', 'droptol0', 'droptol1', 'droptol2', 'param', 'nbproc');
my $sort_option = 2;

Submit::generateTestCases(\%global, \%configuration, \@sort, $sort_option);

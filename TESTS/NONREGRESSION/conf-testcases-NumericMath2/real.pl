#!/usr/bin/perl -w

use strict;
use Submit::Submit;

require 'global.pl';

our %global;

my %configuration = (
		     'name'          =>     'real',
		     'descr'         =>     '',
		     'prog'          =>     'testHIPS-RUN.ex',
		     'compilversion' =>     'scotch-optim-real-double-int',
		     'param'         =>     '1000 2000 4000 8000', # domsize
		     'nbproc'        =>     '8',
		     'driver'        =>     '',
		     
# inputs
		     'matfile'  =>     'NICE20.mm NICE25.mm inline.rsa audi.rsa mchlnf.rua matr5.rua matr6.rua',
		     'sym'      =>     '-1',
		     'rhs'      =>     '0',
		     'method'   =>     'HYBRID',
		     'prec'     =>     '1e-7 1e-10',
		     'locally'  =>     'ALL',
		     'maxit'    =>     '600',
		     'krylov'   =>     '600',
		     'droptol0' =>     '0.000',
		     'droptol1' =>     '0.001',
		     'droptol2' =>     '0.001',
		     'verbose'  =>     '5',
		     
		     'memory'   =>     '0000',
		     'timelimit'=>     '60',
		     );


my @sort = ('matfile', 'prec', 'method', 'locally', 'droptol0', 'droptol1', 'droptol2', 'param', 'nbproc');
my $sort_option = 2;

Submit::generateTestCases(\%global, \%configuration, \@sort, $sort_option);

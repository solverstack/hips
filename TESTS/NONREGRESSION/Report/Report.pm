#!/usr/bin/perl

package Report;

use strict;
use DBI;
use HTML::Template;
use Report::GnuplotHTML;

BEGIN
{
#   print "Chargement du module\n";
}
END
{
#   print "Fin d'usage du module\n";
}


sub trunc {
    return "CAST(CAST(100*r.$_[0] AS INT) AS FLOAT)/100 as $_[0]";
}

sub connect {
    my $db = shift(@_);

    if (!(-e "$db")) { print "error : file $db required\n"; exit(1); }
    
    my $dbargs = {AutoCommit => 0,
		  PrintError => 1};
    
    my $dbh = DBI->connect("dbi:SQLite:dbname=$db","","",$dbargs);
    if ($dbh->err()) { die "DB Error : $DBI::errstr\n"; }

    return $dbh;
}

sub disconnect{
    my $dbh = shift(@_);
    $dbh->disconnect();
}

sub tab2str {
    my @arg = @_;
    my $string = "";
    foreach my $a (@arg) { $string .= $a."-"; }
    chop($string);
    return $string;
}

# Open a file, return his content as a string
# Usage : $string = f2str("file.txt")
sub f2str {
    my $filename = shift(@_);
    
    my $string ='';
    open(IN, "<$filename");
    while (<IN>) { $string .= $_; }
    close IN;

    return $string;

}

# Replace "_ARGi_" by arg[i] on string $string 
# Usage : str_replace("string _ARG0_ _ARG1_", ("toto", "tata"));
sub str_replace {
    my @arg     = @_;
    my $string = shift(@arg);

    my $new_string = $string;

    foreach my $argnum (1 .. $#arg+1) {
	my $s = '_ARG'.($argnum)."_";
	$new_string =~ s/$s/$arg[$argnum-1]/g;
    }

    return $new_string;
}



sub sql_where {
    my @row      = @_;
    my $l_string = shift(@row);

    my $i = 0;

    my @l_list = split(',', $l_string);

    my $r='';
    foreach my $l (@l_list) {
	$r .= "$l = '@row[$i++]' AND ";
    }

    chop($r);
    chop($r);
    chop($r);
    chop($r);

#    print $r."\n";

    return $r;
}






# Usage : one_report_fsql("data.db", "req.sql", "tab.tmpl", "out.tex")
sub one_report_fsql
{
    my @arg = @_;
    my $fdb   = shift(@arg);
    my $fsql  = shift(@arg);
    my $ftmpl = shift(@arg);
    my $ftex  = shift(@arg);

    my $req = Report::f2str($fsql);
    $req    = Report::str_replace($req, @arg);
    
    Report::one_report_sql($fdb, $req, $ftmpl, $ftex);
}

# See one_report_sql(). SQL request is now a string
# Usage : one_report_sql("data.db", "SELECT * from...", "tab.tmpl", "out.tex")
sub one_report_sql
{
    my @arg = @_;
    my $fdb   = shift(@arg);
    my $req   = shift(@arg);
    my $ftmpl = shift(@arg);
    my $ftex  = shift(@arg);

    my $dbh = Report::connect($fdb);
    open(FOUT, ">$ftex");	    
    print FOUT Report::report_sql($dbh, $req, $ftmpl);
    close FOUT;
    
    Report::disconnect($dbh);
}

sub report_fsql {
    my @arg = @_;
    my $dbh = shift(@arg);
    my $fsql = shift(@arg);
    my $ftmpl = shift(@arg);

    my $req = Report::f2str($fsql);
    $req    = Report::str_replace($req, @arg);

    return Report::report_sql($dbh, $req, $ftmpl);
}

# Send the SQL request and apply the HTML template
sub report_sql {
    my @arg = @_;
    my $dbh   = shift(@arg);
    my $req   = shift(@arg);
    my $ftmpl = shift(@arg);

    my $sth = $dbh->prepare($req); 
    
    if ($dbh->err()) { die "$req\n"; }
    $sth->execute();
    my $res = $sth->fetchall_arrayref({});
    
    $sth->finish();
    
    if ( @$res <= 1 ) { return ''; } # TODO : à remonter

#     my $tmpl = HTML::Template->new(filename => $ftmpl, die_on_bad_params => 0); 
#     $tmpl->param(TAB1 => $res);

#     return $tmpl->output();
    
    return Report::template_apply($ftmpl, $res);
}


sub template_apply {
    my @res = @_;
    my $ftmpl = shift(@res);
    
    my $tmpl = HTML::Template->new(filename => $ftmpl, die_on_bad_params => 0); 
    $tmpl->param(TAB1 => @res);

    return $tmpl->output();
}

sub report_all {
    my @arg = @_;
    my $dbh   = shift(@arg);
    my $reqa  = shift(@arg);
    my $fsql  = shift(@arg);
    my $ftmpl = shift(@arg);
    my $out   = shift(@arg);

    my $res = $dbh->selectall_arrayref($reqa);
    open(FOUT, ">$out");	    
    for my $row (@$res) {

	my $tmp = Report::report_fsql($dbh, $fsql, $ftmpl, @$row);
	if (!($tmp eq '')) {
	    my $gpname = "parall-".Report::tab2str(@$row);
	    print FOUT "@$row\n";
	    print FOUT GnuplotHTML::img_parall_link($gpname);
	    print FOUT $tmp."\n";
	}

    }
    close FOUT;
}

sub report_all_sep {
    my @arg = @_;
    my $dbh   = shift(@arg);
    my $reqa  = shift(@arg);
    my $fsql  = shift(@arg);
    my $ftmpl = shift(@arg);
    my $outdir = shift(@arg);

    my $res = $dbh->selectall_arrayref($reqa);
    for my $row (@$res) {
	my $out = Report::tab2str(@$row);
	open(FOUT, ">$outdir"."$out".".data");	
	print FOUT Report::report_fsql($dbh, $fsql, $ftmpl, @$row);
	close FOUT;
    }
}


sub sql_selectall_arrayref {
    my @arg = @_;
    my $dbh   = shift(@arg);
    my $req   = shift(@arg);

    return $dbh->selectall_arrayref($req);
}

sub sql_fetchall_arrayref { # todo : a utiliser partout, donne + d'info
#simplifier http://www.perlmonks.org/?node_id=284436#lol
#    my $employees_loh = $dbh->selectall_arrayref($query2, {Slice => {}

    my @arg = @_;
    my $dbh   = shift(@arg);
    my $req   = shift(@arg);

    my $sth = $dbh->prepare($req); 
    if ($dbh->err()) { die "$req\n"; }
    $sth->execute();
    my $res = $sth->fetchall_arrayref({});
    $sth->finish();
    
    return $res;
}










1;

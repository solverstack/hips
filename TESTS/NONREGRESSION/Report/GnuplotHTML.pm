#!/usr/bin/perl

package GnuplotHTML;

#todo : utiliser qq^partout

sub head {

    return qq^
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
  <meta content="text/html; charset=ISO-8859-1" http-equiv="content-type">
  <title></title>
</head>
<body>
^;

}

sub foot {

    return qq^
</body>
</html>
^;

}

sub img_seq {
    my $gpout = shift(@_);

    return qq^

# of Iteration / # of domains and Convergence History:<br>
<img style="width: 45%; height: 45%;" alt=""
 src="gp/$gpout-it.png">
<img style="width: 45%; height: 45%;" alt=""
 src="gp/$gpout-histo.png"><br>
<br>

Memory / # of domains:<br>
<img style="width: 45%; height: 45%;" alt=""
 src="gp/$gpout-mem.png">
<img style="width: 45%; height: 45%;" alt=""
 src="gp/$gpout-peak.png"><br>

 ^;

}

sub img_parall {
    my $gpout = shift(@_);

    return qq^

Time scalability:<br>
<img style="width: 45%; height: 45%;" alt=""
 src="gp/$gpout-time.png"><br>

Memory scalability:<br>
<img style="width: 45%; height: 45%;" alt=""
 src="gp/$gpout-mem.png">
<img style="width: 45%; height: 45%;" alt=""
 src="gp/$gpout-peak.png"><br>

 ^;

}

sub img_parall_link {
    my $gpout = shift(@_);

    return qq^
<a href="gp/$gpout-time.png">[Time]</a>
<a href="gp/$gpout-mem.png">[Mem]</a>
<a href="gp/$gpout-peak.png">[Peak]</a>
<a href="gp/$gpout.gp">[.gp]</a>
<a href="gp/data/$gpout.data">[.data]</a>
 ^;

}

sub img_seq_link {
    my $gpout = shift(@_);

    return qq^
<a href="gp/$gpout-it.png">[It]</a>
<a href="gp/$gpout-mem.png">[Mem]</a>
<a href="gp/$gpout-peak.png">[Peak]</a>
<a href="gp/$gpout-histo.png">[Histo]</a>
<a href="gp/data/$gpout.data">[.data]</a>
<a href="gp/$gpout.gp">[.gp]</a>
<a href="gp/$gpout-histo.gp">[.gp]</a>
 ^;

}

sub img_convhisto {
    my $gpout = shift(@_);

    return qq^

Convergence History :<br>
<img style="width: 45%; height: 45%;" alt=""
 src="gp/$gpout.png"><br>

 ^;

}

sub latex_link {
    my $gpout = shift(@_);

    return qq^
<a href="latex/data/$gpout.txt">[.tex]</a>
 ^;

}

1;

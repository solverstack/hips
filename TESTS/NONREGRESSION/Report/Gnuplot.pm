#!/usr/bin/perl

package Gnuplot;

# TODO : utiliser HTMLtemplate sur les .gp ?

# TODO use strict;
use DBI;
use HTML::Template;

BEGIN
{
#   print "Chargement du module\n";
}
END
{
#   print "Fin d'usage du module\n";
}

#$set_terminal = 'set terminal postscript eps color';
#$ext = '.eps';
#$set_pointsize = 'set pointsize 1.5';
#$lw='lw 6';

$set_terminal = 'set terminal png';
$ext = '.png';

sub GNUplot_seq {
    my $res = shift(@_);
    my $out = shift(@_);

    my $r = '';

    $r .= '#set bmargin 0'."\n";
    $r .= '#set lmargin 0'."\n";
    $r .= '#set rmargin 0'."\n";
    $r .= '#set tmargin 0'."\n";
    $r .= ''."\n";
    $r .= '#set logscale x'."\n";
    $r .= ''."\n";
    $r .= 'set autoscale'."\n";
    $r .= 'set ytic auto'."\n";
    $r .= 'set xtic auto'."\n";
    $r .= ''."\n";
#    $r .= '#set xrange [80:2000]'."\n";
#    $r .= ''."\n";
#    $r .= '#set title "M1"'."\n";
    $r .= '#set grid'."\n";
    $r .= ''."\n";
    $r .= '#set key left Left box 3'."\n";
    $r .= ''."\n";
    $r .= $set_pointsize."\n";
    $r .= '#set size 0.6,0.6'."\n";
    $r .= ''."\n";
    $r .= '### export EPS'."\n";
    $r .= $set_terminal."\n";
    $r .= ''."\n";

    $r .= 'set xlabel "# of domains"'."\n";
    $r .= 'set ylabel "# of Iteration"'."\n";
    $r .= "set output '$out-it$ext' "."\n";
    $r .= 'plot \\'."\n";

    my $tmp='';
    for my $row (@$res) {
	my $gptab = Report::tab2str(@$row);
	#print " - $gptab\n";

	my $ref_line=`cat _results/gp/data/seq-$gptab.data | head -n 1 | tr -d '\n'`;
	@ref = split(/ /, $ref_line);
        #print $_."\n" foreach (@ref);
	
	$tmp .= "'data/seq-$gptab.data'    using 2:6 title '$gptab' with lines $lw, \\"."\n";
    }
    chop($tmp); chop($tmp); chop($tmp); chop($tmp); $r .= $tmp."\n";
    $r .= ''."\n";

#   $r .= 'set yrange [1:40]'."\n";
#   $r .= ''."\n";
    $r .= 'set xlabel "# of domains"'."\n";
    $r .= 'set ylabel "Memory (ratio) "'."\n";
    $r .= "set output '$out-mem$ext' "."\n";
    $r .= 'plot \\'."\n";

    my $tmp='';
    for my $row (@$res) {
	my $gptab = Report::tab2str(@$row);
	#print " - $gptab\n";

	my $ref_line=`cat _results/gp/data/seq-$gptab.data | head -n 1 | tr -d '\n'`;
	@ref = split(/ /, $ref_line);
        #print $_."\n" foreach (@ref);
	
	$tmp .= "'data/seq-$gptab.data'    using 2:8 title '$gptab' with lines $lw, \\"."\n";
    }
    chop($tmp); chop($tmp); chop($tmp); chop($tmp); $r .= $tmp."\n";
    $r .= ''."\n";

    $r .= 'set xlabel "# of domains"'."\n";
    $r .= 'set ylabel "Memory peak (ratio) "'."\n";
    $r .= "set output '$out-peak$ext' "."\n";
    $r .= 'plot \\'."\n";
    my $tmp='';
    for my $row (@$res) {
	my $gptab = Report::tab2str(@$row);
	#print " - $gptab\n";

	my $ref_line=`cat _results/gp/data/seq-$gptab.data | head -n 1 | tr -d '\n'`;
	@ref = split(/ /, $ref_line);
        #print $_."\n" foreach (@ref);
	
	$tmp .= "'data/seq-$gptab.data'    using 2:7 title '$gptab' with lines $lw, \\"."\n";
    }
    chop($tmp); chop($tmp); chop($tmp); chop($tmp); $r .= $tmp."\n";
    $r .= ''."\n";

#   $r .= 'unset yrange'."\n";
    $r .= ''."\n";

    return $r;
}

sub GNUplot_parall {
    my $res = shift(@_);
    my $out = shift(@_);

#     for my $row (@$res) {
# 	my $gptab = Report::tab2str(@$row);
# 	print " - $gptab\n";
#     }

    my $r = '';
    $r .= '#set bmargin 0'."\n";
    $r .= '#set lmargin 0'."\n";
    $r .= '#set rmargin 0'."\n";
    $r .= '#set tmargin 0'."\n";

    $r .= ''."\n";
    $r .= '#set logscale x'."\n";
    $r .= ''."\n";
    $r .= 'set autoscale'."\n";
    $r .= 'set ytic auto'."\n";
    $r .= 'set xtic auto'."\n";
    $r .= ''."\n";
    $r .= '#set xrange [80:2000]'."\n";
    $r .= ''."\n";
    $r .= '#set title "M1"'."\n";
    $r .= '#set grid'."\n";
    $r .= ''."\n";
    $r .= '#set key left Left box 3'."\n";
    $r .= ''."\n";
    $r .= $set_pointsize."\n";
    $r .= '#set size 0.6,0.6'."\n";
    $r .= ''."\n";
    $r .= '### export EPS'."\n";
    $r .= $set_terminal."\n";
    $r .= ''."\n";

    $r .= 'set autoscale'."\n";
    $r .= 'set ytic auto'."\n";
    $r .= 'set grid'."\n";
    $r .= 'set logscale x 2'."\n";
    $r .= 'set logscale y 2'."\n";
    $r .= '#set xrange [1:128]'."\n";
    $r .= 'set xlabel "# of processors"'."\n";
    $r .= 'set ylabel "Total time"'."\n";
    $r .= "set output '$out-time$ext'"."\n";
    $r .= 'plot \\'."\n";

    my $tmp='';
    for my $row (@$res) {
	my $gptab = Report::tab2str(@$row);
	#print " - $gptab\n";

	my $ref_line=`cat _results/gp/data/parall-$gptab.data | head -n 1 | tr -d '\n'`;
	@ref = split(/ /, $ref_line);
        #print $_."\n" foreach (@ref);
	
	$tmp .= "'data/parall-$gptab.data'    using 1:4 title '$gptab' with lines $lw, \\"."\n";
	$tmp .= "'data/parall-$gptab.data'    using 1:($ref[4-1]/\$1) title 'optimal' with lines lt 3 $lw, \\"."\n";
    }
    chop($tmp); chop($tmp); chop($tmp); chop($tmp); $r .= $tmp."\n";
    
    $r .= ''."\n";

    $r .= 'set autoscale'."\n";
    $r .= 'set ytic auto'."\n";
    $r .= 'set grid'."\n";
    $r .= 'set logscale x 2'."\n";
    $r .= 'set logscale y 2'."\n";
    $r .= '#set xrange [1:128]'."\n";
    $r .= 'set xlabel "# of processors"'."\n";
    $r .= 'set ylabel "Local memory (ratio)"'."\n";
    $r .= "set output '$out-mem$ext'"."\n";
    $r .= 'plot \\'."\n";

    $tmp='';
    for my $row (@$res) {
	my $gptab = Report::tab2str(@$row);
	#print " - $gptab\n";

	my $ref_line=`cat _results/gp/data/parall-$gptab.data | head -n 1 | tr -d '\n'`;
	@ref = split(/ /, $ref_line);
        #print $_."\n" foreach (@ref);
	
	$tmp .= "'data/parall-$gptab.data'    using 1:(\$6/\$1) title '$gptab' with lines $lw, \\"."\n";
	$tmp .= "'data/parall-$gptab.data'    using 1:($ref[6-1+1]/\$1) title 'optimal' with lines lt 3 $lw, \\"."\n";
    }
    chop($tmp); chop($tmp); chop($tmp); chop($tmp); $r .= $tmp."\n";
    
    $r .= ''."\n";

    $r .= 'set autoscale'."\n";
    $r .= 'set ytic auto'."\n";
    $r .= 'set grid'."\n";
    $r .= 'set logscale x 2'."\n";
    $r .= 'set logscale y 2'."\n";
    $r .= '#set xrange [1:128]'."\n";
    $r .= 'set xlabel "# of processors"'."\n";
    $r .= 'set ylabel "Local memory peak (ratio)"'."\n";
    $r .= "set output '$out-peak$ext'"."\n";
    $r .= 'plot \\'."\n";

    $tmp='';
    for my $row (@$res) {
	my $gptab = Report::tab2str(@$row);
	#print " - $gptab\n";

	my $ref_line=`cat _results/gp/data/parall-$gptab.data | head -n 1 | tr -d '\n'`;
	@ref = split(/ /, $ref_line);
        #print $_."\n" foreach (@ref);
	
	$tmp .= "'data/parall-$gptab.data'    using 1:(\$7/\$1) title '$gptab' with lines $lw, \\"."\n";
	$tmp .= "'data/parall-$gptab.data'    using 1:($ref[7-1+1]/\$1) title 'optimal' with lines lt 3 $lw, \\"."\n";
    }
    chop($tmp); chop($tmp); chop($tmp); chop($tmp); $r .= $tmp."\n";

    return $r;
}

sub GNUplot_convhisto {
    my $type = shift(@_);
    my $res = shift(@_);
    my $out = shift(@_);

    if ($type eq 'HIPS') {
	return Gnuplot::GNUplot_convhisto_hips($res, $out);
    } elsif ($type eq 'PETSC') {
	return Gnuplot::GNUplot_convhisto_petsc($res, $out);
    } else { exit(1); }
}

sub GNUplot_convhisto_hips {
    my $res = shift(@_);
    my $out = shift(@_);

    $r = '';
    $r .= 'set autoscale'."\n";
    $r .= 'set ytic auto'."\n";
    $r .= 'set xtic auto'."\n";
    $r .= ''."\n";
    $r .= 'set yrange [0.0000000001:1]'."\n";
    $r .= ''."\n";
    $r .= $set_pointsize."\n";
    $r .= ''."\n";
    $r .= 'set logscale y'."\n";
    $r .= ''."\n";
    $r .= '### export EPS'."\n";
    $r .= $set_terminal."\n";
    $r .= ''."\n";
    $r .= 'set xlabel "Sequential time (s)"'."\n";
    $r .= 'set ylabel "Relative residual norm"'."\n";
    $r .= "set output '$out$ext'\n";
    $r .= ''."\n";
    $r .= 'plot \\'."\n";

    my $tmp=''; my $c=0;

    for my $row (@$res) {

	$stdout = $row->{'stdout'};
	my $out = "_results/gp/data/".$stdout."_it";

	if (! (-e $neededfile)) {

	    system("mkdir -p _results/gp/data/`dirname $stdout`; cat $stdout | grep '     GMRES' | sed -e 's/    GMRES Iter //' | sed -e 's/   |r|\\/|b| = / /' > $out");
	    
	}

#TODO : such informations are on the db
	$precond_time=`cat $stdout | grep "HIPS preconditioner computed in " | cut -d " " -f 5`;
	$one_iter=`cat $stdout | grep " One iter in " | cut -d " " -f 5`;
	$last_prec=`cat $stdout | grep "^GMRES Iter 1   |r|/|b| = " | sed -e 's/GMRES Iter 1   |r|\\/|b| = //'`;
	$last_prec_s=`cat $out | tail -n 1 | sed -e 's/^ [0-9]*//'`;
	$ndom=`cat $stdout | grep "Level 0 = " | tail -n 1 | cut -d " " -f 9`;
	$fill=`cat $stdout | grep Fill | cut -d " " -f 9`;
	$param=`cat $stdout | grep "_PARAM_ = " | tail -n 1 | cut -d " " -f 3`;
	$matfile=`cat $stdout | grep "_MATFILE_ = " | tail -n 1 | cut -d " " -f 3`;
	
	chop($precond_time);
	chop($one_iter);
	chop($last_prec);
	chop($last_prec_s);
	chop($ndom);
	chop($fill);
	chop($param);
	chop($matfile);
	
	if (( $precond_time != "" ) &&
	    ( $one_iter != "" ) &&
	    ( $last_prec != "" ) &&
	    ( $last_prec_s != "" )
	    ) {
	    
	    $tmp .= "'../../$out' using ($precond_time+\$1*$one_iter):(\$2*$last_prec/$last_prec_s) title '$matfile $ndom domains ($param, $fill)' with lines lt $c $lw, \\\ "; chop($tmp); $tmp .="\n";
	    $c++;
	}
    }
    
    chop($tmp);chop($tmp);chop($tmp);chop($tmp);
    $r .= $tmp."\n";

#    print $r;
    return $r;
}


sub GNUplot_convhisto_petsc {
    my $res = shift(@_);
    my $out = shift(@_);

    $r = '';
    $r .= 'set autoscale'."\n";
    $r .= 'set ytic auto'."\n";
    $r .= 'set xtic auto'."\n";
    $r .= ''."\n";
    $r .= 'set yrange [0.0000000001:1]'."\n";
    $r .= ''."\n";
    $r .= $set_pointsize."\n";
    $r .= ''."\n";
    $r .= 'set logscale y'."\n";
    $r .= ''."\n";
    $r .= '### export EPS'."\n";
    $r .= $set_terminal."\n";
    $r .= ''."\n";
    $r .= 'set xlabel "Sequential time (s)"'."\n";
    $r .= 'set ylabel "Relative residual norm"'."\n";
    $r .= "set output '$out$ext'\n";
    $r .= ''."\n";
    $r .= 'plot \\'."\n";

    my $tmp=''; my $c=0;

    for my $row (@$res) {

	$stdout = $row->{'stdout'};
	my $out = "_results/gp/data/".$stdout."_it";

 	system("mkdir -p _results/gp/data/`dirname $stdout`; cat $stdout | grep 'KSP preconditioned resid norm' | sed -e 's/KSP preconditioned resid norm.*Ax||//' > $out");
	
	my $matfile=`cat $stdout | grep "_MATFILE_" | cut -d " " -f3`;
	chop($matfile);
	
	$stdout = $row->{'stdout'};

	$facto   = $row->{'facto'};
	$solve   = $row->{'solve'};
	$itouter = $row->{'itinner'};
	$ndom    = $row->{'ndom'};
	$param   = $row->{'param'};
	$fill    = $row->{'ratio'};

	if ($itouter != '') {
	    my $one_iter = $solve / $itouter;
	}
#	print "$stdout\n $facto $solve $itouter $one_iter\n";

 	$tmp .= "'../$out' using ($facto+\$1*$one_iter):(\$2) title '$matfile $ndom domains ($param, $fill)' with lines lt $c lw 6, \\\ "; chop($tmp); $tmp .="\n";

 	$c++;
	
    }
    
    chop($tmp);chop($tmp);chop($tmp);chop($tmp);
    $r .= $tmp."\n";

#    print $r;
    return $r;
}



sub filter {
    my @list = @_;

    $r1 = '(';
    foreach $l (@list) {
	$r1.= "matfile = '$l' OR ";
    }
    chop($r1);
    chop($r1);
    chop($r1);
    chop($r1);
    $r1 .= ') ';

    return $r1;
}

1;

#./clean.sh
#./compil.sh

rm -rf _run/* _results/*

if [ $# -ne 0 -a -d "$1" ]
then
    cd $1
else 
    cd conf-testcases
fi

for i in `ls *.pl | grep -v global.pl`
  do
  echo $i
  perl $i
  echo; echo
done

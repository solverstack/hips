#!/usr/bin/perl -w



%parser_conf = (
		'name'            => ["_NAME_ =", 'TXT'], 
		'descr'           => ["_DESCR_ =", 'TXT'], 
		
		'prog'            => ["_PROG_ =", 'TXT'],  
		'compilversion'   => ["_COMPILVERSION_ =", 'TXT'], 
		'param'           => ["_PARAM_ =", 'NUM'],         
		'nbproc'          => ["_NBPROC_ =", 'NUM'],        
		'driver'          => ["_DRIVER_ =", 'TXT'],        
		
# inputs
		'matfile'  => ["_MATFILE_ =", 'TXT'],  
		'sym'      => ["_SYM_ =", 'TXT'],  
		'rhs'      => ["_RHS_ =", 'TXT'],    
		'method'   => ["_METHOD_ =", 'TXT'],   
		'prec'     => ["_PREC_ =", 'TXT'],   
		'locally'  => ["_LOCALLY_ =", 'TXT'],   
		'maxit'    => ["_MAXIT_ ="],   
		'krylov'   => ["_KRYLOV_ ="],   
		'droptol0' => ["_DROPTOL0_ ="],   
		'droptol1' => ["_DROPTOL1_ ="],  
		'droptol2' => ["_DROPTOL2_ ="],   
		'verbose'  => ["_VERBOSE_ ="],   
		
		'memory'   => ["_MEMORY_ =", 'TXT'],   
		'timelimit'=> ["_TIMELIMIT_ =", 'TXT'],   

		'launched' => ["_LAUNCHED_ =", 'NUM'],   

                'forward'  => ["_D6FORWARD_ =", 'NUM'],
                'schur_method' => ["_D7SCHURMETHOD_ =", 'NUM'],

##
                'ndom'     => ["Level 0 = .* Nodes", 'NUM'],

		"facto"            => ["HIPS preconditioner computed in"], 
		"solve"            => ["HIPS : solved in"], 
		"itinner"          => ["inner iterations ="],
		"itouter"          => ["Number of outer iterations "],

		"nnzA"             => ["Info..1.* nnzA = ", 'NUM'], # 1er proc
		"nnzP"             => ["Info..1.* nnzP = ", 'NUM'],
		"peak"             => ["Info..1.* peak = ", 'NUM'],

		"ratio"            => ["Info..total. ratio = ", 'NUM'],
		"peakratio"        => ["Info..total.* peak ratio = ", 'NUM'],

		"oldfill"          => ["HIPS Fill Ratio of Preconditioner ="],

                "connectors"  => ["Total number of connectors =", 'NUM'],
                "levels"      => ["Total number of levels =", 'NUM'],
                "nodeslevel0" => ["Level 0 =", 'NUM'],

                "dim" => ["Matrix : dim=", 'NUM'],
                "nnz" => ["Matrix : dim=.*nnz=", 'NUM'],

    "MemA" => ["Mem : A", 'NUM'],
    "MemL0" => ["Mem : L", 'NUM'],
    "MemC" => ["Matrix : C", 'NUM'],
    "MemE" => ["Matrix : E       : ", 'NUM'], # attention, : utile !
    "MemW" => ["Matrix : E_DB", 'NUM'],
    "MemEmax " => ["Matrix : E_max", 'NUM'],
    "MemS" => ["Matrix : S", 'NUM'],
    "MemL1" => ["Matrix : LS", 'NUM'],

    
	      );

%parser_conf_regexp = ('NUM'     => ['(_TXT_)([^\d]*)([\d\.]*)', 3],
		       'TXT'     => ['(_TXT_)([ ]*)([:a-zA-Z0-9-_.]*)', 3]);

$parser_conf_regexp_default = 'NUM';

1;

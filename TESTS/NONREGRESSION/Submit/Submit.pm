#!/usr/bin/perl

package Submit;

#use strict;
use List::Util qw[min max];
use POSIX qw(ceil floor);
use File::Basename;

BEGIN
{
#   print "Chargement du module\n";
}
END
{
#   print "Fin d'usage du module\n";
}

# -- MISC --
#http://www.motreja.com/ankur/examplesinperl/parsing_config_files.htm
#http://perl.enstimac.fr/DocFr/perlfaq5.html#comment%20traduire%20les%20tildes%20(~)%20dans%20un%20nom%20de%20fichier
sub parse_config_file {

    local ($config_line, $Name, $Value, $Config);

    ($File, $Config) = @_;

    $File =~ s{
	^ ~             # cherche le tilde en tête
	    (               # sauvegarde dans $1 :
			    [^/]        # tout caractère sauf un slash
			    *     # et ce 0 ou plusieurs fois (0 pour mon propre login)
			    )
        }{
	    $1
		? (getpwnam($1))[7]
		: ( $ENV{HOME} || $ENV{LOGDIR} )
	    }ex;

    if (!open (CONFIG, "<$File")) {
        print "ERROR: Config file not found : $File";
        exit(0);
    }

    while (<CONFIG>) {
        my $config_line=$_;
        chop ($config_line);          # Get rid of the trailling \n
        $config_line =~ s/^\s*//;     # Remove spaces at the start of the line
        $config_line =~ s/\s*$//;     # Remove spaces at the end of the line
        if ( ($config_line !~ /^#/) && ($config_line ne "") ){    # Ignore lines starting with # and blank lines
            ($Name, $Value) = split (/=/, $config_line);          # Split each line into name value pairs
            $$Config{$Name} = $Value;                             # Create a hash of the name value pairs
        }
    }

    close(CONFIG);

}
# -- MISC -- /

sub generateTestCases {
    my($gref,  $cref, $sref, $sort_option) = @_;
    my %global = %$gref;
    my %configuration = %$cref;
    my @sort = @$sref;

    my $descr = $configuration{'descr'};
    delete ($configuration{'descr'});
    
    my %ConfigM;
    &parse_config_file ('~/machine.sh', \%ConfigM);
    $global{'machine'} = $ConfigM{'machine'};

#    print 'CONFIG : '."\n";
#    while ((my $confkey, my $confvalue) = each(%global)) {
#	print "$confkey : $confvalue\n";
#    }
#    while ((my $confkey, my $confvalue) = each(%configuration)) {
#	print "$confkey : $confvalue\n";
#    }
    
#my $count=0;
    my $myrunpath;
    my $myresultspath;
    
    {
	my @sortrep;
	foreach my $s (@sort)
	{
	    if (exists($configuration{$s})) {
		my @split = split(/ /, $configuration{$s});
		if ($#split > 0) {
		    push(@sortrep, $s);	
		}
	    }
	}
	
	foreach my $s (keys(%configuration)) {
	    my @split = split(/ /, $configuration{$s});
	    if ($#split > 0) {
		push(@sortrep, $s);	    
	    }
	}
	
	my %dejavu = ();
	my @unique = grep { ! $dejavu{ $_ }++ } @sortrep;
	
	@sort= ();
	foreach my $s (@unique)
	{
	    if ($s ne 'descr') {
		push(@sort, $s);
	    }
	}
    }
    
    if ((keys(%global)) && (keys(%configuration))) { 
	testConfig(\%global, \%configuration);
	my %null = (); generateConfig(\%global, \%configuration, \%null, \@sort);
    } else {
	print "no config !\n";
    }
}
########################################################

sub testConfig {
    my($gref,  $cref) = @_;
    my %global = %$gref;
    my %configuration = %$cref;
    
    if (exists($configuration{'matrix'})) {
	my @matlist = split(/ /, $configuration{'matrix'});
	
	foreach my $mat (@matlist)
	{
	    my $matpath = $global{'matrixpath'} . '/' . $mat;
	    
	    if( ! -e $matpath)
	    {
		print "$matpath n'existe pas\n";
		exit;
	    }
	}
    }
}

sub generateConfig {
    my($gref,  $cref, $href, $sref) = @_;
    my %global = %$gref;
    my %configuration = %$cref;
    my %hash = %$href;
    my @sort = @$sref;

    my @tab = keys(%configuration);
    if ($#tab != -1) {
	my $confkey = $tab[0];
	my $confvalue = $configuration{$confkey};
	
	my @tabconfvalue = split(/ /, $confvalue);
	
	delete ($configuration{$confkey});
	
	if ($#tabconfvalue != -1) {
	    foreach my $paramvalue (@tabconfvalue) {
		$hash{$confkey} = $paramvalue;
		generateConfig(\%global, \%configuration, \%hash, \@sort);
	    }
	} else { # fix bug driver=''
	    $hash{$confkey} = '';
	    generateConfig(\%global, \%configuration, \%hash, \@sort);
	}

    } else {

#	while ((my $confkey, my $confvalue) = each(%hash)) {
#	    print "$confkey $confvalue\n";
#	}

	oneConfig(\%global, \%hash, \@sort);

    }
}

sub oneConfig {
    my($gref,  $href, $sref) = @_;
    my %global = %$gref;
#    my %configuration = %$cref;
    my %hash = %$href;
    my @sort = @$sref;
    
#	print 'CONFIG : '."\n";
	while ((my $confkey, my $confvalue) = each(%hash)) {
	#    print "$confkey $confvalue\n";
	}

#if option0
#    my $mypath = $runpath.$count;
#option 1 ou 2
    
    my $path=''; 
    foreach my $s (@sort) {
	$path .= '/'.$s.'_'.$hash{$s};
    }

#todo : harmoniser les deux lignes :
    my $myrunpath = '../_run/'.$hash{'name'}.$path;
    my $myresultspath = $global{'resultspath'}.'/'.$hash{'name'}.$path;

    system("mkdir -p $myrunpath");
    system("mkdir -p $myresultspath");
    oneConfigCreateInputs(\%global, \%hash, $myrunpath);
    
    system('ln -s '.$global{'progpath'}.'/'.$hash{'compilversion'}.'/'.$hash{'prog'}.' '.$myrunpath.'/');

#    if ($global{'machine'} eq "local") {
    my $stdout = "$myresultspath/stdout"; #$count
    my $stderr = "$myresultspath/stderr";

    system("touch $stdout"); # important to do that

    open(FOUT, ">$myrunpath/job.sh");
    print FOUT '#!/bin/sh'."\n";
    
    print FOUT 'stdout="'."$stdout\"\n";
    print FOUT 'stderr="'."$stderr\"\n";

    print FOUT '> $stdout'."\n";
    print FOUT 'echo -n "_DATE_ = " >> $stdout'."\n";
    print FOUT 'date +"%D %H:%M" >> $stdout'."\n";
#	print FOUT 'echo _STDOUT_ = '.$myresultspath.'/stdout >> $stdout'."\n";
#	print FOUT 'echo _STDERR_ = '.$myresultspath.'/stderr >> $stdout'."\n";

    while ((my $confkey, my $confvalue) = each(%hash)) {
	print FOUT 'echo _'.uc($confkey).'_ = '.$confvalue.' >> $stdout'."\n";
    }
    print FOUT 'echo >> $stdout'."\n\n";

    print FOUT 't1=`date +%s`'."\n";

    close FOUT;
    system("chmod +x $myrunpath/job.sh; $myrunpath/job.sh; echo \"_LAUNCHED_ = 0\" >> $stdout"); #todo : l'ecrire directement
    
#####
    open(FOUT, ">>$myrunpath/job.sh");

    print FOUT 'echo "_LAUNCHED_ = 1" >> $stdout'."\n";

    $machine = $global{'machine'};

    if ($global{'machine'} eq "local") {
	print FOUT "mpirun -n ".$hash{'nbproc'}." ";
    } elsif ($global{'machine'} eq "localintel") {
	print FOUT "mpirun -n ".$hash{'nbproc'}." ";
    } elsif ($machine eq "hephaistos") {
	print FOUT "mpirun -n ".$hash{'nbproc'}." ";
    } elsif ($machine eq "hephaistosintel") {
        print FOUT "mpirun -n ".$hash{'nbproc'}." ";
    } elsif ($machine eq "borderline" or $machine eq "borderlinelocal") {
	print FOUT "mpirun -machinefile ".'$OAR_NODE_FILE'." -np ".$hash{'nbproc'}." ";
    } elsif ($machine eq "ccrt") {
	print FOUT "srun -J HIPS -n ".$hash{'nbproc'}." -t ".$hash{'timelimit'}." -K ";
    } elsif ($machine eq "jade") {
	print FOUT 'export MPI_COMM_MAX=8192'."\n";
	print FOUT 'cd $PBS_O_WORKDIR'."\n";
	print FOUT "/usr/pbs/bin/mpiexec ";
    } elsif ($machine eq "dolomede" or $machine eq "dolomedeintel") {
	print FOUT 'module purge;'."\n";
	print FOUT 'source /etc/bash.bashrc.local;'."\n";
	print FOUT 'module add shared;'."\n";
	print FOUT 'module add cluster-tools;'."\n";
	print FOUT 'module add intel/compiler/64;'."\n";
	print FOUT 'module add torque;'."\n";
	print FOUT 'module add ofed/1.4/intel/mvapich2;'."\n";
	print FOUT 'module add mpiexec;'."\n";
	print FOUT 'module add intel/mkl/10.1.0.015;'."\n";

	my $myrunpath2 = $global{'runpath'}.'/'.$hash{'name'}.$path;
	print FOUT 'cd '."$myrunpath2"."\n";

	print FOUT "mpiexec ";
    }
    print FOUT "./".$hash{'prog'}.' '.$hash{'param'}.' >> $stdout 2> $stderr'."\n";

    print FOUT 't2=`date +%s`'."\n";
    print FOUT 'let "ttotal=t2-t1"'."\n";
    print FOUT 'echo "_TTOTAL_ = $ttotal" >> $stdout'."\n";

    close FOUT;
#####


    system("chmod +x $myrunpath/job.sh"); #borderline
    #   }
    
#    $count++;

## CONF
    open(FOUT, ">$myrunpath/conf.sh");
    print FOUT '#!/bin/sh'."\n";

    print FOUT "nbproc=".$hash{'nbproc'}."\n";
    print FOUT "timelimit=".$hash{'timelimit'}."\n";
    print FOUT "memory=".$hash{'memory'}."\n";

    close FOUT;
## -- CONF
}

sub oneConfigCreateInputs {
    my($gref,  $href, $myrunpath) = @_;
    my %global = %$gref;
    my %hash = %$href;
    
    my $inputsT = '../conf-exec/'.$global{'Inputs.template'};

    open(CMD, "<$inputsT");
    my @inp = <CMD>;
    close CMD;
    
    foreach (@inp)
    {
	# TODO
	my $var = $hash{'driver'}.$global{'matrixpath'} . '/' . $hash{matfile};
#	print $hash{'driver'}," - ", $global{'matrixpath'}, " - ", $hash{matfile}, "\n";
	s/_MATFILE_/$var/g;
	
	while ((my $confkey, my $confvalue) = each(%hash)) {
	    my $var = "_".uc($confkey)."_";
	    s/$var/$confvalue/g;
	}
    }

    open(FOUT, ">$myrunpath/Inputs");
    print FOUT @inp;
    close FOUT;
    
}


####
#TODO : variable de chemin

sub getconfTestCases {
    my $data = shift;
    my @joblist = @_;

    my $cmd;

    foreach my $jobfile (@joblist) {
	my $path = dirname($jobfile);
	
	my %Config;
	&parse_config_file ($path.'/conf.sh', \%Config);
	
	$data->{$jobfile}{'nbproc'} = $Config{'nbproc'};
	$data->{$jobfile}{'timelimit'} = $Config{'timelimit'};
    }
}

sub groupTestCases {
    my %data = ();
    my $jobpath = shift;
    my @joblist = @_;

    getconfTestCases(\%data, @joblist);
    groupTestCases2(\%data, $jobpath, @joblist);
}

sub groupTestCases2 {
    my $data = shift;
    my $jobpath = shift;
    my @joblist = @_;

    my %data = ();
    
    my $nbproc_max=0;
    my $timelimit_tot=0;

    mkdir $jobpath;

    open(FOUT, ">$jobpath/job.sh");
    print FOUT '#!/bin/sh'."\n";
    print FOUT 'cd ../..'."\n"; #todo

    my $filedata='';
    foreach my $jobfile (@joblist) {
	my $base = basename($jobfile);
	my $path = dirname($jobfile);
	
	my $nbproc=$data->{$jobfile}{'nbproc'};
	my $timelimit=$data->{$jobfile}{'timelimit'};

	$nbproc_max = max($nbproc_max, $nbproc);
	$timelimit_tot += $timelimit;

	#print FOUT "(cd $path; ./$base)\n"; # trop trop long !
	$filedata .= "(cd $path; ./$base)\n";
    }

    print FOUT $filedata;

    close(FOUT);
    system("chmod +x $jobpath/job.sh");

## CONF code dupliquÃ©
    open(FOUT, ">$jobpath/conf.sh");
    print FOUT '#!/bin/sh'."\n";

    print FOUT "nbproc=".$nbproc_max."\n";
    print FOUT "timelimit=".$timelimit_tot."\n";
#    print FOUT "memory=".$hash{'memory'}."\n";

    close FOUT;
## -- CONF

}

sub runTestCases {
    my @joblist = @_;

    my %ConfigM;
    &parse_config_file ('~/machine.sh', \%ConfigM);
    my $machine=$ConfigM{'machine'};
   
    foreach my $jobfile (@joblist) {
	my $path = dirname($jobfile);
	
	# todo : ne pas refaire
	my %Config;
	&parse_config_file ($path.'/conf.sh', \%Config);
	
	my $nbproc    = $Config{'nbproc'};
	my $timelimit = $Config{'timelimit'};

	if ($machine eq "local") {
	    runTestCaseInteractive($path, $jobfile, $nbproc, $timelimit);
	} 
	elsif ($machine eq "borderlinelocal") {
	    runTestCaseInteractive($path, $jobfile, $nbproc, $timelimit);
	} 
	elsif ($machine eq "localintel") {
	    runTestCaseInteractive($path, $jobfile, $nbproc, $timelimit);
	} elsif ($machine eq "hephaistos" or $machine eq "hephaistosintel" ) {
	    runTestCaseInteractive($path, $jobfile, $nbproc, $timelimit);
	} elsif ($machine eq "borderline") {
	    runTestCaseOAR($path, $jobfile, $nbproc, $timelimit);
	} elsif ($machine eq "m3pec") {
	    runTestCaseIBM($path, $jobfile, $nbproc, $timelimit);
	} elsif ($machine eq "ccrt") {
	    runTestCaseBSUB($path, $jobfile, $nbproc, $timelimit);
	} elsif ($machine eq "jade") {
	    runTestCasePBS($path, $jobfile, $nbproc, $timelimit);
	} elsif ($machine eq "dolomede" or $machine eq "dolomedeintel") {
	    runTestCasePBSmath($path, $jobfile, $nbproc, $timelimit);
	}
    }
    
}

sub runTestCaseInteractive {
    my ($path, $jobfile, $nbproc, $timelimit) = @_;

    my $pwd=`pwd`; chop $pwd;

    print "$jobfile\n";
    system("(cd $pwd/$path/; $pwd/$jobfile)");
}

# TODO : fichier + template
sub runTestCaseOAR {
    my ($path, $jobfile, $nbproc, $timelimit) = @_;

    my $pwd=`pwd`; chop $pwd;

    my $node=ceil($nbproc/8);

    my $temps;
    my $heure = floor($timelimit/60);
    my $min = $timelimit - ($heure*60);
    $temps="$heure:$min:00";
	
    my $resout="$pwd/$jobfile.log";
	
    my $lauch = "oarsub ".
#	'-t allow_classic_ssh '.
#	'--type=besteffort '.
	
#	"-t 'timesharing=jgaidamo,* ".
	
	'-p "cluster=\'borderline\'" '.
	
	"-S $pwd/$jobfile ".
	"-d $pwd/$path ".
	
#	"-l /cpu=$cpu". ==> todo en faire une option
#	"-l /core=$cpu".
	"-l /nodes=$node".
	",walltime=$temps ".
	"-n HIPS ".
	"-E $resout".'.err '.
	"-O $resout ";

#    print $lauch."\n";    
    print "HIPS : node=$node (proc=$nbproc), walltime=$temps\n";
    system($lauch);
#    return ("HIPS", $node, $temps);
}

sub runTestCaseBSUB {
    my ($path, $jobfile, $nbproc, $timelimit) = @_;

    my $pwd=`pwd`; chop $pwd;

    my $resout="$pwd/$jobfile.log";
	

    my $lauch = "cd $pwd/$path; bsub ".
#	"-q bigmem ". 
	"-n $nbproc ". 
	"-W $timelimit ".
	"-J HIPS ".
	"-e $resout".'.err '.
	"-o $resout ".
	"$pwd/$jobfile";
        
#mem limit : "-M"  -q "queue_name ..."-u mail_user
    print $lauch."\n";    

    print "HIPS : node=$node (proc=$nbproc), timelimit=$timelimit\n";
    system($lauch);
#    return ("HIPS", $node, $temps);
}

sub runTestCaseIBM {
    my ($path, $jobfile, $nbproc, $timelimit) = @_;

    my $pwd=`pwd`; chop $pwd;

    my $node=ceil($nbproc/8);

    my $temps;
    my $heure = floor($timelimit/60);
    my $min = $timelimit - ($heure*60);
    $temps="$heure:$min:00";
	
    my $resout="$pwd/$jobfile.log";
	
    my $lauch = "oarsub ".
#	'-t allow_classic_ssh '.
#	'--type=besteffort '.
	
	"-t 'timesharing=jgaidamo,* ".
	
	'-p "cluster=\'borderline\'" '.
	
	"-S $pwd/$jobfile ".
	"-d $pwd/$path/.. ".
	
#	"-l /cpu=$cpu". ==> todo en faire une option
#	"-l /core=$cpu".
	"-l /nodes=$node".
	"-l /nodes=2".
	",walltime=$temps".
	"-n HIPS ".
	"-E $resout".'.err '.
	"-O $resout ";
    
    system($lauch);
}

sub runTestCasePBS {
    my ($path, $jobfile, $nbproc, $timelimit) = @_;

    my $pwd=`pwd`; chop $pwd;

    my $node=ceil($nbproc/8);

    if ($nbproc > 8) {
	$nbproc = 8;
    }

    my $temps;
    my $heure = floor($timelimit/60);
    my $min = $timelimit - ($heure*60);
    $temps="$heure:$min:00";
	
    my $resout="$pwd/$jobfile.log";
	
    my $lauch = "(cd $path; qsub ".
	"-N HIPS ".
	"-e $resout".'.err '.
	"-o $resout ".
	"-l select=$node:ncpus=$nbproc:mpiprocs=$nbproc ". #==> todo le 8
	"-l walltime=$temps job.sh".
	")";

    print "HIPS : node=$node (proc=$nbproc), walltime=$temps\n";
#    print $lauch."\n";    
    system($lauch);
#    return ("HIPS", $node, $temps);
}

sub runTestCasePBSmath {
    my ($path, $jobfile, $nbproc, $timelimit) = @_;

    my $pwd=`pwd`; chop $pwd;

    my $node=ceil($nbproc/8);

    if ($nbproc > 8) {
	$nbproc = 8;
    }

    my $temps;
    my $heure = floor($timelimit/60);
    my $min = $timelimit - ($heure*60);
    $temps="$heure:$min:00";
	
    my $resout="$pwd/$jobfile.log";
	
    my $lauch = "(cd $path; qsub ".
	"-N HIPS ".
	"-e $resout".'.err '.
	"-o $resout ".
	"-q bonobo ".
        "-l mem=30gb ".
        "-l nodes=$node:ppn=$nbproc ".
#	"-l select=$node:ncpus=$nbproc:mpiprocs=$nbproc ". #==> todo le 8
	"-l walltime=$temps job.sh".
	")";

#PBS -q bonobo3j
#PBS -l mem=32000mb
#PBS -l nodes=4:ppn=8 

    print "HIPS : node=$node (proc=$nbproc), walltime=$temps\n";
#    print $lauch."\n";    
   system($lauch);
#   return ("HIPS", $node, $temps);
}

1;

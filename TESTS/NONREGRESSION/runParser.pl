#!/usr/bin/perl -w

#DB
use DBI;

require 'conf-parser/parser_config.pl';
our %parser_conf;
our %parser_conf_regexp;
our $parser_conf_regexp_default;

require 'Parser/parser.pl';

my %convert = (
    'TXT' => 'CHAR',
    'NUM' => 'FLOAT'
    );


main:
{

    my $argc = $#ARGV + 1;
    my $results;
    if ($argc == 1) { $results=$ARGV[0]; } else { $results="_results"; }
    my $ref = "ref-results";

# DB
    system("rm $results/data.db 2>/dev/null");
    
    if (-e "$ref/data.db") { 
	system("cp $ref/data.db $results/data.db");
    }
    
    my $dbargs = {AutoCommit => 0,
		  PrintError => 1};
    
    my $dbh = DBI->connect("dbi:SQLite:dbname=$results/data.db","","",$dbargs);
    #if ($dbh->err()) { die "DB Error : $DBI::errstr\n"; }
    
    if (-e "$ref/data.db") { 
	my $req = 'alter table results rename to ref';
	$dbh->do($req);
    }
    
# create DB
    my $req = 'CREATE TABLE results ( input CHAR, stdout CHAR, stderr CHAR, ';
    foreach my $k (keys(%parser_conf)) {
	my $param;
	my $size = $#{$parser_conf->{$k}} + 1;
	
	$param = $parser_conf_regexp_default;
	$param = $parser_conf->{$k}[1] if ($size == 2);
	my $type = $convert{$param};
	
	$req .= "$k $type, ";
    }
    chop($req); chop($req);
    $req .= ")";
    #print $req ."\n";
    $dbh->do($req);
#     
    
    
# PARSER
    my %parser;
    
    parser_init(\%parser_conf, \%parser_conf_regexp, $parser_conf_regexp_default, \%parser);
    
#    my @files = ("stdout");
    my @files = `find $results/ -name "stdout"`;    # "/" important if ln -s
    my %data;
    
    foreach my $file (@files)
    {
	chop($file);
	parser_parse($file, \%parser, \%{$data{$file}});
	
#	foreach my $k (keys(%{$data{$file}}))
#	{
#	    print "Clef=$k Valeur=$data{$file}{$k}\n";
#	}
	#todo
	my $input = $file; $input =~ s/$results/_run/g; $input =~ s/stdout/Inputs/g; # todo : fix si deplacement
	my $stderr = $file; $stderr =~ s/stdout/stderr/g;
	my $stdout = $file;

	my $req1='input, stdout, stderr, '; my $req2="'$input', '$stdout', '$stderr', ";
	while ((my $key, my $value) = each(%{$data{$file}}))
	{
	    $req1 .= "$key, ";
	    $req2 .= "'$value', ";
	}
	chop($req1); chop($req1);
	chop($req2); chop($req2);
	$req = "INSERT INTO results ($req1) VALUES ($req2)";
	#print $req ."\n";
	$dbh->do( $req );
	if ($dbh->err()) { print $req ."\n"; }
    }

    $dbh->commit();
    $dbh->disconnect();
}

#!/usr/bin/perl -w

use strict;
use Submit::Submit;

require 'global.pl';

our %global;

my %configuration = (
		     'name'          =>     'audi-parall',
		     'descr'         =>     'Tests de PMAA',
		     
		     'prog'          =>     'testHIPS.ex',
		     'compilversion' =>     'scotch-double',
		     'param'         =>     '3000', # domsize
		     'nbproc'        =>     '1 2 4 8 16 32 64',
		     
# inputs
		     'matfile'  =>     'audi.rsa',
		     'rhs'      =>     '0',
		     'method'   =>     'HYBRID',
		     'prec'     =>     '1e-7',
		     'locally'  =>     '0 ALL',
		     'maxit'    =>     '200',
		     'krylov'   =>     '200',
		     'droptol0' =>     '0',
		     'droptol1' =>     '0.001',
		     'droptol2' =>     '0.001',
		     'verbose'  =>     '5',
		     
		     'memory'   =>     '0000',
		     'timelimit'=>     '60',

                     'sym'      =>     '-1',
		     'driver'   =>     '',
		     );


my @sort = ('matfile', 'prec', 'method', 'locally', 'droptol0', 'droptol1', 'droptol2', 'param', 'nbproc');
my $sort_option = 2;

Submit::generateTestCases(\%global, \%configuration, \@sort, $sort_option);

#!/usr/bin/perl -w

use strict;
use Submit::Submit;

require 'global.pl';

#exit;

our %global;

my %configuration = (
		     'name'          =>     'GRID',
		     'descr'         =>     'GRID',
		     
		     'prog'          =>     'testHIPS-Grid.ex',
		     'compilversion' =>     'scotch-dbseq-real-double-int',

		     'param'         =>     '100', # domsize
		     'nc'            =>     '10 21 43 87',
		     '3d'            =>     '0',

		     'nbproc'        =>     '1',
		     
# inputs
		     'matfile'  =>     'grid',
		     'rhs'      =>     '0',
		     'method'   =>     'HYBRID',
		     'prec'     =>     '1e-7',
		     'locally'  =>     '0 ALL',
		     'maxit'    =>     '100',
		     'krylov'   =>     '100',
		     'droptol0' =>     '0',
		     'droptol1' =>     '0',
		     'droptol2' =>     '0',
		     'verbose'  =>     '5',
		     
		     'memory'   =>     '0000',
		     'timelimit'=>     '30',

                     'sym'      =>     '-1',
		     'driver'   =>     '',

		     );


my @sort = ('matfile', 'prec', 'method', 'locally', 'droptol0', 'droptol1', 'droptol2', 'param', 'nbproc');
my $sort_option = 2;

Submit::generateTestCases(\%global, \%configuration, \@sort, $sort_option);

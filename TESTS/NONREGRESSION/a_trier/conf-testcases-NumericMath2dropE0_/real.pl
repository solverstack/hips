#!/usr/bin/perl -w

use strict;
use Submit::Submit;

require 'global.pl';

our %global;

my $glob='real';
my $name;

my %configuration = (
		     'name'          =>     '----',
		     'descr'         =>     '',
		     'prog'          =>     'testHIPS-RUN.ex',
		     'compilversion' =>     'scotch-optim-real-double-int',
		     'param'         =>     '----', # domsize
		     'nbproc'        =>     '----',
		     'driver'        =>     '',
		     
# inputs
#		     'matfile'  =>     'NICE20.mm NICE25.mm inline.rsa audi.rsa mchlnf.rua matr5.rua matr6.rua',
		     'matfile'  =>     'mchlnf.rua matr5.rua matr6.rua',
		     'sym'      =>     '-1',
		     'rhs'      =>     '0',
		     'method'   =>     'HYBRID',
		     'prec'     =>     '----',
		     'locally'  =>     '0 ALL',
		     'maxit'    =>     '1000',
		     'krylov'   =>     '1000',
		     'droptol0' =>     '0.000',
		     'droptol1' =>     '----',
		     'droptol2' =>     '0.000',
		     'verbose'  =>     '5',
		     
		     'memory'   =>     '0000',
		     'timelimit'=>     '60',
		     );


my @sort = ('matfile', 'prec', 'method', 'locally', 'droptol0', 'droptol1', 'droptol2', 'param', 'nbproc');
my $sort_option = 2;

###
$name='seq8';
$configuration{'nbproc'}  = '8';
$configuration{'param'}   = '1000 2000 4000 8000';
$configuration{'prec'}    = '1e-7 1e-10';
###

##                                                                                                                              
$configuration{'droptol1'} = '0.01';
$configuration{'name'}  = $glob.'-'.$name.'-'.$configuration{'droptol1'}.'-'.$configuration{'droptol2'};
Submit::generateTestCases(\%global, \%configuration, \@sort, $sort_option);
##                                                                                                                              
$configuration{'droptol1'} = '0.001';
$configuration{'name'}  = $glob.'-'.$name.'-'.$configuration{'droptol1'}.'-'.$configuration{'droptol2'};
Submit::generateTestCases(\%global, \%configuration, \@sort, $sort_option);
##                                                                                                                              
$configuration{'droptol1'} = '0.0001';
$configuration{'name'}  = $glob.'-'.$name.'-'.$configuration{'droptol1'}.'-'.$configuration{'droptol2'};
Submit::generateTestCases(\%global, \%configuration, \@sort, $sort_option);

###
$name='parall';
$configuration{'nbproc'}  = '1 2 4 8 16 32 64';
$configuration{'param'}   = '2000';
$configuration{'prec'}    = '1e-7';
###

##                                                                                                                              
$configuration{'droptol1'} = '0.01';
$configuration{'name'}  = $glob.'-'.$name.'-'.$configuration{'droptol1'}.'-'.$configuration{'droptol2'};
Submit::generateTestCases(\%global, \%configuration, \@sort, $sort_option);
##                                                                                                                              
$configuration{'droptol1'} = '0.001';
$configuration{'name'}  = $glob.'-'.$name.'-'.$configuration{'droptol1'}.'-'.$configuration{'droptol2'};
Submit::generateTestCases(\%global, \%configuration, \@sort, $sort_option);
##                                                                                                                              
$configuration{'droptol1'} = '0.0001';
$configuration{'name'}  = $glob.'-'.$name.'-'.$configuration{'droptol1'}.'-'.$configuration{'droptol2'};
Submit::generateTestCases(\%global, \%configuration, \@sort, $sort_option);



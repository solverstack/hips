#!/usr/bin/perl -w

use strict;
use Submit::Submit;

require 'global.pl';

our %global;

my $glob='complex';
my $name;

my %configuration = (
		     'name'          =>     '10m',
		     'descr'         =>     '',
		     'prog'          =>     'testHIPS-RUN.ex',
		     'compilversion' =>     'scotch-optim-complex-double-int',
		     'param'         =>     '10000', # domsize
		     'nbproc'        =>     '64',
		     'driver'        =>     '',
		     
# inputs
#		     'matfile'  =>     'Haltere.mm Amande.mm',
		     'matfile'  =>     '10M_Matrice.mm',
#		     'matfile'  =>     'Haltere.mm',
		     'sym'      =>     '-1',
		     'rhs'      =>     '0',
		     'method'   =>     'HYBRID',
		     'prec'     =>     '1e-7',
		     'locally'  =>     'ALL',
		     'maxit'    =>     '1000',
		     'krylov'   =>     '1000',
		     'droptol0' =>     '0.000',
		     'droptol1' =>     '0.000',
		     'droptol2' =>     '0.001',
		     'verbose'  =>     '5',
		     
		     'memory'   =>     '0000',
#		     'timelimit'=>     '60',
		     'timelimit'=>     '180',
		     );


my @sort = ('matfile', 'prec', 'method', 'locally', 'droptol0', 'droptol1', 'droptol2', 'param', 'nbproc');
my $sort_option = 2;

Submit::generateTestCases(\%global, \%configuration, \@sort, $sort_option);

#!/usr/bin/perl -w

# todo : factoriser la config

use Submit::Submit; # parse_config_file
my %ConfigM;
Submit::parse_config_file ('~/machine.sh', \%ConfigM);
my $machine=$ConfigM{'machine'};

my $pwd=`cd ..; pwd`;
chop $pwd;


if ($machine eq "local" or $machine eq "localintel" ) {

    if ($pwd =~ /^\/home\/jeremie\//) {
        %global = ( 'matrixpath' => '/home/jeremie/travail/matrices');
    } else {
        %global = ( 'matrixpath' => '/home/henon/5....Soft_about_work/MATRICES');
    }

} elsif ($machine eq "borderline") {
    %global = ( 'matrixpath' => '/home/bordeaux/jgaidamo/matrices');

} elsif ($machine eq "m3pec") {

    %global = ( 'matrixpath' => '/home/bordeaux/jgaidamo/matrices');

} elsif ($machine eq "ccrt") {
    %global = ( 'matrixpath' => '/home/cont002/henon/scratch/matrices');

} elsif ($machine eq "hephaistos" or $machine eq "hephaistosintel" ) {
    %global = ( 'matrixpath' => '/matrices');

} elsif ($machine eq "jade") {
    %global = ( 'matrixpath' => '/scratch/gaidamou/matrices');

}

$global{'progpath'} = $pwd.'/_exec';
$global{'resultspath'} = $pwd.'/_results';

#!/usr/bin/perl -w

use strict;
use Submit::Submit;

require 'global.pl';

our %global;

my %configuration = (
		     'name'          =>     'complex',
		     'descr'         =>     '',
		     'prog'          =>     'testHIPS-RUN.ex',
		     'compilversion' =>     'scotch-optim-complex-double-int',
		     'param'         =>     '1000 2000 4000 8000', # domsize
		     'nbproc'        =>     '8',
		     'driver'        =>     '',
		     
# inputs
		     'matfile'  =>     'Amande.mm',
		     'sym'      =>     '-1',
		     'rhs'      =>     '0',
		     'method'   =>     'HYBRID',
		     'prec'     =>     '1e-4 1e-5',
		     'locally'  =>     'ALL',
		     'maxit'    =>     '1000',
		     'krylov'   =>     '1000',
		     'droptol0' =>     '0.00',
		     'droptol1' =>     '0.01',
		     'droptol2' =>     '0.01',
		     'verbose'  =>     '5',
		     
		     'memory'   =>     '0000',
		     'timelimit'=>     '60',
		     );


my @sort = ('matfile', 'prec', 'method', 'locally', 'droptol0', 'droptol1', 'droptol2', 'param', 'nbproc');
my $sort_option = 2;

Submit::generateTestCases(\%global, \%configuration, \@sort, $sort_option);

$configuration{'name'}     = $configuration{'name'}.'2';
$configuration{'droptol1'} = '0.001';
$configuration{'droptol2'} = '0.001';

Submit::generateTestCases(\%global, \%configuration, \@sort, $sort_option);

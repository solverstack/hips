#!/usr/bin/perl -w

use strict;
use Submit::Submit;

require 'global.pl';

our %global;

my %configuration = (
		     'name'          =>     'complex',
		     'descr'         =>     '',
		     'prog'          =>     'testHIPS-RUN.ex',
		     'compilversion' =>     'scotch-optim-complex-double-int scotch-db-complex-double-int',
		     'param'         =>     '1000 2000 4000 8000', # domsize
		     'nbproc'        =>     '8',
		     'driver'        =>     '',
		     
# inputs
		     'matfile'  =>     'Amande.mm',
		     'sym'      =>     '-1',
		     'rhs'      =>     '0',
		     'method'   =>     'HYBRID',
		     'prec'     =>     '1e-7',
		     'locally'  =>     '0 ALL',
		     'maxit'    =>     '500',
		     'krylov'   =>     '500',
		     'droptol0' =>     '0.000',
		     'droptol1' =>     '0.000',
		     'droptol2' =>     '0.000',
		     'verbose'  =>     '5',
		     
		     'memory'   =>     '0000',
		     'timelimit'=>     '120',
		     );


my @sort = ('matfile', 'prec', 'method', 'locally', 'droptol0', 'droptol1', 'droptol2', 'param', 'nbproc');
my $sort_option = 2;

Submit::generateTestCases(\%global, \%configuration, \@sort, $sort_option);

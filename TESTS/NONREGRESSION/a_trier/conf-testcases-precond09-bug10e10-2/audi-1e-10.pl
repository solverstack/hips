#!/usr/bin/perl -w

use strict;
use Submit::Submit;

require 'global.pl';

our %global;

my %configuration = (
		     'name'          =>     'audi-1e-10',
		     'descr'         =>     'Tests de SparseDays pour PRECOND09',
		     
		     'prog'          =>     'testHIPS-RUN.ex',
		     'compilversion' =>     'scotch-double',
		     'param'         =>     '4000', # domsize
		     'nbproc'        =>     '1',
		     
# inputs
		     'matfile'  =>     'audi.rsa',
		     'rhs'      =>     '0',
		     'method'   =>     'HYBRID',
		     'prec'     =>     '1e-10',
		     'locally'  =>     'ALL',
		     'maxit'    =>     '1000',
		     'krylov'   =>     '1000',
		     'droptol0' =>     '0',
		     'droptol1' =>     '0.001',
		     'droptol2' =>     '0 0.001',
		     'verbose'  =>     '5',
		     
		     'memory'   =>     '0000',
		     'timelimit'=>     '60',

                     'sym'      =>     '-1',
		     'driver'   =>     '',
		     );


my @sort = ('matfile', 'prec', 'method', 'locally', 'droptol0', 'droptol1', 'droptol2', 'param', 'nbproc');
my $sort_option = 2;

Submit::generateTestCases(\%global, \%configuration, \@sort, $sort_option);

#!/usr/bin/perl -w

my $debug = 0;

sub parser_init {
    my ($parser_conf, $parser_conf_regexp, $parser_conf_regexp_default, $parser) = @_;

    foreach my $k (keys(%$parser_conf)) {
	my $param;
	my $size = $#{$parser_conf->{$k}} + 1;
	
	$param = $parser_conf_regexp_default;
	$param = $parser_conf->{$k}[1] if ($size == 2);
    
	$parser->{$k}[0] = $parser_conf_regexp->{$param}[0];
	$parser->{$k}[0] =~ s/_TXT_/$parser_conf->{$k}[0]/g; 
	
	$parser->{$k}[1] = $parser_conf_regexp->{$param}[1];
    }
}

sub parser_parse {
    my $k;
    my ($file, $parser, $data) = @_;

    open(M, "$file") || die("Erreur � l'ouverture du fichier $file");
    
    while (<M>)
    {
	foreach $k (keys(%$parser))
	{
	    $data->{$k} = ${$parser->{$k}[1]} if /$parser->{$k}[0]/;
	}
    }
    close M;
 
    if ($debug == 1) {
	foreach $k (keys(%$parser))
	{
	    if (!exists($data->{$k})) {
		print "Warning : $k ($parser->{$k}) not found in $file\n";
	    }
	}
    }
    
}

1;

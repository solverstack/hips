#!/usr/bin/perl

use Report::Report;
use Report::Gnuplot;
use Report::GnuplotHTML;

my $html    = 1;
my $gnuplot = 1;

my $tmpldir = "conf-report/tmpl";
my $results;

require 'conf-report/global.pl'; our $distinct;
   
main:
{
    my $argc = $#ARGV + 1;
    $results; if ($argc == 1) { $results=$ARGV[0]; } else { $results="_results"; }
    my $fdb    = "$results/data.db";

    my $dbh = Report::connect($fdb);

    system("mkdir -p $results/gp/");

#     # 
#     {
# 	my $name = 'parall';
# 	my $la       = "matfile, $distinct, prec, param";
# 	my $where    = "nbproc <> 8";
# 	my $reqa     = "SELECT DISTINCT $la FROM results WHERE $where ORDER BY $la";

# 	report_parall($dbh, $la, $where, $reqa, $name);
#     }
    
    #  par matrices
    {
	my $name = 'parall-grp-matfile';
	my $la       = "$distinct, prec, param";
	my $where    = "nbproc <> 8";
	my $reqa     = "SELECT DISTINCT $la FROM results WHERE $where ORDER BY $la";

	report_parall($dbh, $la, $where, $reqa, $name);
    }

# par matrices selectionnées
    {

	%list = (
	    'l1' => ['Haltere.mm', 'NICE20.mm', 'NICE25.mm', 'matr5.rua', 'matr6.rua', 'ultrasound80.rua'],
	    'l2' => ['mchlnf.rua', 'audi.rsa', 'inline.rsa']
	    );

	foreach my $k (keys(%list)) {
#	    foreach my $k2 (@{$list{$k}}) {
#		print $k2."\n";
#	    }

	    my $name     = "parall-grp-matfile-$k";
	    my $la       = "$distinct, prec, param";
	    my $where    = "nbproc <> 8 AND ".Gnuplot::filter(@{$list{$k}});
	    my $reqa     = "SELECT DISTINCT $la FROM results WHERE $where ORDER BY $la";
	    
	    report_parall($dbh, $la, $where, $reqa, $name);
	}
    }

    Report::disconnect($dbh);
}

sub report_parall {    
#PARALL
    {
	my ($dbh, $la, $w, $reqa, $name) =  @_;

	open  HTMLGP, ">_results/gp-$name.html";
	print HTMLGP GnuplotHTML::head();

	my $resa = Report::sql_selectall_arrayref($dbh, $reqa);
	for my $row (@$resa) {
	    
	    my $where = $w." AND ".Report::sql_where($la,@$row); 

	    my $lref  = "matfile, $distinct, prec, param";
	    my $reqc  = q(SELECT DISTINCT ).$lref.q( FROM results WHERE ).$where
		.q( ORDER BY ).$lref;
	    my $resc  = Report::sql_selectall_arrayref($dbh, $reqc);

	    {
# GNUPLOT (GP)
		my $out   = $name."-".Report::tab2str(@$row);
		my $outgp = "$results/gp/$out.gp";
		open(FGP, ">$outgp");
		print FGP Gnuplot::GNUplot_parall($resc, $out);
		close FGP;

# GNUPLOT (HTML)
		my $gpname = "$name-".Report::tab2str(@$row);
		print HTMLGP '<b>'."@$row".'</b><br>'
		    .GnuplotHTML::img_parall_link($gpname)
		    .GnuplotHTML::img_parall($gpname);
	    }
	}

	print HTMLGP GnuplotHTML::foot();
	close HTMLGP;

    }

}

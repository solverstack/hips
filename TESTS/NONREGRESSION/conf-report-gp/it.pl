#!/usr/bin/perl

use Report::Report;
use Report::Gnuplot;
use Report::GnuplotHTML;

my $html    = 1;
my $gnuplot = 1;

my $tmpldir = "conf-report/tmpl";
my $results;

require 'conf-report/global.pl'; our $distinct;
   
main:
{
    my $argc = $#ARGV + 1;
    $results; if ($argc == 1) { $results=$ARGV[0]; } else { $results="_results"; }
    my $fdb    = "$results/data.db";

    my $dbh = Report::connect($fdb);

    system("mkdir -p $results/gp/");

    #  par matrices
    {
	my $name = 'it-grp-matfile';
	my $la       = "nbproc, prec, $distinct";
	my $where    = "nbproc = 8 AND prec = '1e-07'";
	my $reqa     = "SELECT DISTINCT $la FROM results WHERE $where ORDER BY $la";

	report_it($dbh, $la, $where, $reqa, $name);
    }

    Report::disconnect($dbh);

}

sub report_it {        
#SEQ
    {
	my ($dbh, $la, $w, $reqa, $name) =  @_;

	open(FHTML, ">$results/$name.html");
	
	my $resa = Report::sql_selectall_arrayref($dbh, $reqa);
	for my $row (@$resa) {
	    
	    my $where = $w." AND ".Report::sql_where($la,@$row); 
	    
	    my $lref  = "matfile, param, itinner, maxit, round(ratio,2) as ratio";
	    my $lref2  = "matfile, param, itinner, maxit, ratio";
	    my $reqc  = q(SELECT DISTINCT ).$lref.q( FROM results WHERE ).$where
		.q( ORDER BY ).$lref2;
	    my $resc  = Report::sql_selectall_arrayref($dbh, $reqc);

	    my $out   = $name."-".Report::tab2str(@$row);

	    print FHTML "@$row\n";
	    print FHTML GnuplotHTML::latex_link($out); # if (($latex);
	    print FHTML report_it_sql($resc, $out, 'conf-report-gp/it.thtml');
	    
#TODO : prendre en compte parametre if ($latex) 
	    open(FTEX, ">$results/latex/data/$out.txt");
	    print FTEX report_it_sql($resc, $out, 'conf-report-gp/it.ttex');
	    close FTEX;
	}

	close FHTML;

    }

}


sub report_it_sql {
    my $res = shift(@_);
    my $out = shift(@_);
    my $tmpl = shift(@_);

    # open the html template
    my $template = HTML::Template->new(filename => $tmpl);
    
    # fill in some parameters
    
    my %row_data; # get a fresh hash for the row data
    
    for my $row (@$res) {
	$mat=$row->[0];
	$param=$row->[1];
	$it=$row->[2];
	$itmax=$row->[3];
	$ratio=$row->[4];
	
	$matlist{$mat} = 1;
	$paramlist{$param} = 1;
	
	$it{$param}{$mat} = $it;
	$itmax{$param}{$mat} = $itmax;
	$ratio{$param}{$mat} = $ratio;
    }
    
#SCHEMA
#     $template->param(LOOP => [
# 			 { matfile => 'Matrice',
# 			   param => [
# 			       { param => '1000' }, 
# 			       { param => '2000' },
# 			       ],
# 			 },
			 
# 			 { matfile => 'audi.rsa',
# 			   param => [
# 			       { param => '1' }, 
# 			       { param => '5' },
# 			       ],
# 			 }
# 		     ],
# 	);
    

    my @loop_data = ();

    # ENTETE
    {
	my %row_data;
	$row_data{matfile} = 'Matrice';
	
	$i=0;
	for my $param (sort keys %paramlist) {
	    $row_data{param}[$i]{param} = $param;
	    $i++;
#x2
	    $row_data{param}[$i]{param} = $param;
	    $i++;
	}
	
	push(@loop_data, \%row_data);
    }
    #

    for my $mat (sort keys %matlist) {
	my %row_data;
	$row_data{matfile} = $mat;

	$i=0;
	for my $param (sort keys %paramlist) {
#	    print $param ." ". $mat ." ". $it{$param}{$mat}. "\n";
	    $it = $it{$param}{$mat};
	    $itma = $itmax{$param}{$mat};

	    if ($it != $itmax) {
		$row_data{param}[$i]{param} = $it{$param}{$mat};
	    } else {
		$row_data{param}[$i]{param} = 'X';
	    }
	    $i++;

	    $row_data{param}[$i]{param} = $ratio{$param}{$mat};

	    $i++;
	}
	
	push(@loop_data, \%row_data);
    }


    $template->param(LOOP => \@loop_data);
    
    # send the obligatory Content-Type and print the template output
    return $template->output;
  
}

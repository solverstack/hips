#!/usr/bin/perl

use Report::Report;
use Report::Gnuplot;
use Report::GnuplotHTML;

my $html    = 1;
my $gnuplot = 1;

my $tmpldir = "conf-report/tmpl";
my $results;

require 'conf-report/global.pl'; our $distinct; our $type;
   
main:
{
    my $argc = $#ARGV + 1;
    $results; if ($argc == 1) { $results=$ARGV[0]; } else { $results="_results"; }
    my $fdb    = "$results/data.db";

    my $dbh = Report::connect($fdb);

    system("mkdir -p $results/gp/data");

# # 
#     {
# 	my $name = 'TODO';
# 	my $la      = "nbproc, prec, $distinct, matfile";
# 	my $where    = "nbproc = 8";
# 	my $reqa     = "SELECT DISTINCT $la FROM results WHERE $where ORDER BY $la";

# 	report_convhisto($dbh, $la, $where, $reqa, $name);

#     }

# par param
    {
	my $name = 'histo-grp-param';
	my $la       = "nbproc, prec, $distinct, matfile";
	my $where    = "nbproc = 8";
	my $reqa     = "SELECT DISTINCT $la FROM results WHERE $where ORDER BY $la";

	report_convhisto($dbh, $la, $where, $reqa, $name);

    }
    
# par matrices
    {
	my $name = 'histo-grp-matfile';
	my $la       = "nbproc, prec, $distinct, param";
	my $where    = "nbproc = 8";
	my $reqa     = "SELECT DISTINCT $la FROM results WHERE $where ORDER BY $la";

	report_convhisto($dbh, $la, $where, $reqa, $name);

    }

# par matrices selectionnées
    {

	%list = (
	    'l1' => ['Haltere.mm', 'NICE20.mm', 'NICE25.mm', 'matr5.rua', 'matr6.rua', 'ultrasound80.rua'],
	    'l2' => ['mchlnf.rua', 'audi.rsa', 'inline.rsa']
	    );

	foreach my $k (keys(%list)) {
#	    foreach my $k2 (@{$list{$k}}) {
#		print $k2."\n";
#	    }

	    my $name     = "histo-grp-matfile-$k";
	    my $la       = "nbproc, prec, $distinct, param";
	    my $where    = "nbproc = 8 AND ".Gnuplot::filter(@{$list{$k}});
	    my $reqa     = "SELECT DISTINCT $la FROM results WHERE $where ORDER BY $la";

	    report_convhisto($dbh, $la, $where, $reqa, $name);
	}
    }

    Report::disconnect($dbh);
}

sub report_convhisto {
    {    
	my ($dbh, $la, $w, $reqa, $name) =  @_;

	open  HTMLGP, ">_results/gp-$name.html";
	print HTMLGP GnuplotHTML::head();

	my $resa = Report::sql_selectall_arrayref($dbh, $reqa);
	for my $row (@$resa) {
	    
	    my $where = $w." AND ".Report::sql_where($la,@$row); 
	    
	    my $lref=$la;
	    my $reqb  = q(SELECT DISTINCT stdout )
		.",facto, solve, itinner, ndom, param, ratio"
		.q( FROM results WHERE ).$where
		.q( ORDER BY ).$lref;
	    my $resb  = Report::sql_fetchall_arrayref($dbh, $reqb);

	    {

# GNUPLOT (CONVHISTO)
		my $out   = $name  ."-".Report::tab2str(@$row)."-histo";
		my $outgp = "$results/gp/$out.gp";
		open(FGP, ">$outgp");
		print FGP Gnuplot::GNUplot_convhisto($type, $resb, $out);
		close FGP;

# GNUPLOT (HTML)
		print HTMLGP '<b>'."@$row".'</b>' 
#		    .GnuplotHTML::img_convhisto($gpname) #todo
		    .GnuplotHTML::img_convhisto($out);
		
	    }
	}

	print HTMLGP GnuplotHTML::foot();
	close HTMLGP;

    }

}

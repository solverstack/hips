#!/usr/bin/perl -w

use strict;
use Submit::Submit;

require 'global.pl';

our %global;

my %configuration = (
		     'name'          =>     'haltere-para1',
		     'descr'         =>     'Tests de SparseDay2009',
		     
		     'prog'          =>     'testHIPS-RUN.ex',
		     'compilversion' =>     'scotch-complex',
		     'param'         =>     '2000', # domsize
		     'nbproc'        =>     '1 2 4 8 16 32 64 128',
		     
# inputs
		     'matfile'  =>     'Haltere.mm',
		     'rhs'      =>     '0',
		     'method'   =>     'HYBRID',
		     'prec'     =>     '1e-7',
		     'locally'  =>     'ALL',
		     'maxit'    =>     '200',
		     'krylov'   =>     '200',
		     'droptol0' =>     '0',
		     'droptol1' =>     '0.01',
		     'droptol2' =>     '0 0.01',
		     'verbose'  =>     '5',
		     
		     'memory'   =>     '0000',
		     'timelimit'=>     '30',

                     'sym'      =>     '-1',
		     'driver'   =>     '',
		     );


my @sort = ('matfile', 'prec', 'method', 'locally', 'droptol0', 'droptol1', 'droptol2', 'param', 'nbproc');
my $sort_option = 2;

Submit::generateTestCases(\%global, \%configuration, \@sort, $sort_option);

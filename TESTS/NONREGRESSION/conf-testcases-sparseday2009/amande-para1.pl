#!/usr/bin/perl -w

use strict;
use Submit::Submit;

require 'global.pl';

our %global;

my %configuration = (
		     'name'          =>     'amande-para1',
		     'descr'         =>     'Tests de SparseDays',
		     
		     'prog'          =>     'testHIPS-RUN.ex',
		     'compilversion' =>     'scotch-complex',
		     'param'         =>     '2000', # domsize
		     'nbproc'        =>     '1 2 4 8 16 32 64 128',
		     
# inputs
		     'matfile'  =>     'Amande.mm',
		     'rhs'      =>     '0',
		     'method'   =>     'HYBRID',
		     'prec'     =>     '1e-7',
		     'locally'  =>     'ALL',
		     'maxit'    =>     '500',
		     'krylov'   =>     '500',
		     'droptol0' =>     '0',
		     'droptol1' =>     '0.001',
		     'droptol2' =>     '0 0.001',
		     'verbose'  =>     '5',
		     
		     'memory'   =>     '0000',
		     'timelimit'=>     '180',

                     'sym'      =>     '-1',
		     'driver'   =>     '',
		     );


my @sort = ('matfile', 'prec', 'method', 'locally', 'droptol0', 'droptol1', 'droptol2', 'param', 'nbproc');
my $sort_option = 2;

Submit::generateTestCases(\%global, \%configuration, \@sort, $sort_option);

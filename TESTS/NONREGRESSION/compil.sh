source $HOME/machine.sh

jvar=`cat /proc/cpuinfo | grep processor | wc -l`

CONFMACHINE=conf-machine/$machine

if [ ! -e conf-machine/$machine ]
then 
    (cd conf-machine; ./gen-makefile.sh $machine)
fi

TMPEXEC=_exec

(cd ../..; mv makefile.inc makefile.inc_bkp 2>/dev/null)

if [ ! -e "_run" ]
then
    echo "Compil (all)"
    CONFIGDIRLIST=`find $CONFMACHINE -maxdepth 1 -mindepth 1 -type d -printf "%f\n" | grep -v .svn`
else
    CONFIGDIRLIST=`find _run -name "*.ex" -printf "%l\n" | sed 's/.*_exec\/\(.*\)\/.*/\1/' | sort | uniq`
    echo "Compil (_run) : "`echo $CONFIGDIRLIST | tr "\n" " "`
fi

for i in $CONFIGDIRLIST
do
  
  cp $CONFMACHINE/$i/makefile.inc ../..

  (cd ../../; make clean >/dev/null; make -j$jvar all >/dev/null); 
  (cd conf-exec; make -j$jvar >/dev/null)
  
  mkdir -p $TMPEXEC/$i
  mv conf-exec/*.ex $TMPEXEC/$i/
  
done;

(cd ../../; mv makefile.inc_bkp makefile.inc)
(cd ../../; make clean >/dev/null)

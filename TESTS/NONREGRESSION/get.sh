results=results-$1

mkdir $results
mv _results _run $results
tar -cjf $results.tbz2 $results
mv $results/* .
rmdir $results
#!/usr/bin/perl

use Submit::Submit;

##
my @joblist = `find _run -name "job.sh"`;
foreach my $jobfile (@joblist) {
    chop $jobfile;
}

Submit::runTestCases(@joblist);
exit(1);
##





my $c=0;
my @matlist = `find _run -name "matfile_*"`;
#my @matlist = `find _run -name "real-*"`;
foreach my $mat (@matlist) {
    chop $mat;
    my @joblist = `find $mat -name "job.sh"`;
    foreach my $jobfile (@joblist) {
	chop $jobfile;
    }

# get infos
my %data = ();
Submit::getconfTestCases(\%data, @joblist);

# group
my %array = ();

# group by proc
foreach $k (keys %data)
{
    push(@{$array{$data{$k}{'nbproc'}}}, $k);
}

# group timelimit
#my $index=0;
#my $timelimit=0;
#foreach $k (keys %data)
#{
#    $timelimit += $data{$k}{'timelimit'};
#    push(@{$array{$index}}, $k);

#    if ($timelimit >= 180) {
#	$timelimit=0;
#	$index++;
#    }
#}

my @finaljoblist;
foreach $k (sort keys %array)
{
    #    Submit::groupTestCases("_run/group$k", @{$array{$k}});
    Submit::groupTestCases2(\%data, "_run/group$k-$c", @{$array{$k}});
    push(@finaljoblist, "_run/group$k-$c/job.sh");
}

Submit::runTestCases(@finaljoblist);

#todo : tester le nb de job dans la file apres submission + retour d'erreur

}

#!/usr/bin/perl -w

use strict;
use Submit::Submit;

require 'global.pl';

our %global;

my %configuration = (
		     'name'          =>     'complex-parall',
		     'descr'         =>     'Test non regression complex/parallel',
		     
		     'prog'          =>     'testHIPS.ex',
		     'compilversion' =>     'scotch-complex',
		     'param'         =>     '50', # domsize
		     'nbproc'        =>     '2 4 8',
		     
# inputs
		     'matfile'  =>     'young4c_.mtx',
		     'rhs'      =>     '0',
		     'method'   =>     'HYBRID ILUT',
		     'prec'     =>     '1e-7',
		     'locally'  =>     '0 ALL',
		     'maxit'    =>     '50',
		     'krylov'   =>     '50',
		     'droptol0' =>     '0 0.001',
		     'droptol1' =>     '0 0.001',
		     'droptol2' =>     '0 0.001',
		     'verbose'  =>     '5',
		     
		     'memory'   =>     '0000',
		     'timelimit'=>     '1',

                     'sym'      =>     '-1',
		     'driver'   =>     '',
		     );


my @sort = ('matfile', 'prec', 'method', 'locally', 'droptol0', 'droptol1', 'droptol2', 'param', 'nbproc');
my $sort_option = 2;

Submit::generateTestCases(\%global, \%configuration, \@sort, $sort_option);

##
$configuration{'name'}  = $configuration{'name'}.'-ilut';
$configuration{'method'} = 'ILUT';
$configuration{'param'} = '';

Submit::generateTestCases(\%global, \%configuration, \@sort, $sort_option);

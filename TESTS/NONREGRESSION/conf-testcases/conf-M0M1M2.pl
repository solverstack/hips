#!/usr/bin/perl -w

use strict;
use Submit::Submit;

require 'global.pl';

#exit;

our %global;

$global{'Inputs.template'} = 'Inputs.M0M1M2.template';

my %configuration = (
		     'name'          =>     'one-test',
		     'descr'         =>     'Test non regression',
		     
		     'prog'          =>     'testHIPS.ex',
		     'compilversion' =>     'scotch-dbseqm0-real-double-int',
		     'param'         =>     '5000', # domsize
		     'nbproc'        =>     '1',
		     
# inputs
		     'matfile'  =>     'inline.rsa',
		     'rhs'      =>     '0',
		     'method'   =>     'HYBRID',
		     'prec'     =>     '1e-7',
		     'locally'  =>     '0 ALL',
		     'maxit'    =>     '1',
		     'krylov'   =>     '1',
		     'droptol0' =>     '0',
		     'droptol1' =>     '0',
		     'droptol2' =>     '0',
		     'verbose'  =>     '5',
		     
		     'memory'   =>     '0000',
		     'timelimit'=>     '30',

                     'sym'      =>     '-1',
		     'driver'   =>     '',

#                    'd6forward'       =>     '1',
#		     'd7schurmethod'   =>     '0 1 2',
                     'd6forward'       =>     '1',
		     'd7schurmethod'   =>     '1',
		     );


my @sort = ('matfile', 'prec', 'method', 'locally', 'droptol0', 'droptol1', 'droptol2', 'param', 'nbproc');
my $sort_option = 2;

Submit::generateTestCases(\%global, \%configuration, \@sort, $sort_option);

# $configuration{'name'}  = $configuration{'name'}.'-norec';
# $configuration{'d6forward'} = '0';
# $configuration{'d7schurmethod_method'} = '';

# Submit::generateTestCases(\%global, \%configuration, \@sort, $sort_option);

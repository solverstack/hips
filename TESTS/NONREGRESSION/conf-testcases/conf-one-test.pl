#!/usr/bin/perl -w

use strict;
use Submit::Submit;

require 'global.pl';

#exit;

our %global;

my %configuration = (
		     'name'          =>     'one-test',
		     'descr'         =>     'Test non regression',
		     
		     'prog'          =>     'testHIPS.ex',
		     'compilversion' =>     'scotch-complex',
		     'param'         =>     '50 100', # domsize
		     'nbproc'        =>     '1 2 4',
		     
# inputs
		     'matfile'  =>     'young4c_.mtx',
		     'rhs'      =>     '0',
		     'method'   =>     'HYBRID',
		     'prec'     =>     '1e-7',
		     'locally'  =>     'ALL',
		     'maxit'    =>     '50',
		     'krylov'   =>     '50',
		     'droptol0' =>     '0',
		     'droptol1' =>     '0',
		     'droptol2' =>     '0',
		     'verbose'  =>     '5',
		     
		     'memory'   =>     '0000',
		     'timelimit'=>     '1',

                     'sym'      =>     '-1',
		     'driver'   =>     '',
		     );


my @sort = ('matfile', 'prec', 'method', 'locally', 'droptol0', 'droptol1', 'droptol2', 'param', 'nbproc');
my $sort_option = 2;

Submit::generateTestCases(\%global, \%configuration, \@sort, $sort_option);

#!/usr/bin/perl -w

use strict;
use Submit::Submit;

require 'global.pl';



our %global;

my %configuration = (
		     'name'          =>     'complex-hybrid',
		     'descr'         =>     'Test non regression en complex hybrid',
		     
		     'prog'          =>     'testHIPS.ex testHIPS2.ex',
                     'compilversion' =>     'metis-debug-real-double-int32 scotch-optim-real-double-int64',
		     'param'         =>     '1000',# domsize
		     'nbproc'        =>     '1 2 4 8',
		     'driver'        =>     '-1',
		     
# inputs
		     'matfile'  =>     'Haltere.mm',
		     'sym'      =>     '2',
		     'rhs'      =>     '0',
		     'method'   =>     'HYBRID',
		     'prec'     =>     '1e-7',
		     'locally'  =>     'ALL',
		     'maxit'    =>     '150',
		     'krylov'   =>     '60',
		     'droptol0' =>     '0',
		     'droptol1' =>     '0.01',
		     'droptol2' =>     '0 0.01',
		     'verbose'  =>     '5',
		     
		     'memory'   =>     '0000',
		     'timelimit'=>     '1',
		     );


my @sort = ('matfile', 'prec', 'method', 'locally', 'droptol0', 'droptol1', 'droptol2', 'param', 'nbproc');
my $sort_option = 2;

Submit::generateTestCases(\%global, \%configuration, \@sort, $sort_option);

##
# $configuration{'name'}  = $configuration{'name'}.'-ilut';
# $configuration{'method'} = 'ILUT';
# $configuration{'param'} = '';

# Submit::generateTestCases(\%global, \%configuration, \@sort, $sort_option);

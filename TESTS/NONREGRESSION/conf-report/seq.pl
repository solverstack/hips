#!/usr/bin/perl

use Report::Report;
use Report::Gnuplot;
use Report::GnuplotHTML;

my $html    = 1;
my $latex   = 1;
my $gnuplot = 1;
my $gnuplot_data = 1;

my $tmpldir = "conf-report/tmpl";

require 'conf-report/global.pl'; our $distinct; our $type;

main:
{
    my $argc = $#ARGV + 1;
    my $results; if ($argc == 1) { $results=$ARGV[0]; } else { $results="_results"; }
    my $fdb    = "$results/data.db";

    my $dbh = Report::connect($fdb);

    system("mkdir -p $results/gp/data/"); #if ($gnuplot ...)
    system("mkdir -p $results/latex/data/") if ($latex);
    
#SEQ
    {
	my $name = 'seq';

	my $la      = "nbproc, param, prec, matfile, forward, schur_method, $distinct";
#	my $la      = "nbproc, prec, $distinct, matfile";
#	my $reqa    = q( SELECT DISTINCT ).$la.q( FROM results WHERE nbproc = 1 ORDER BY ).$la;
#	my $reqa    = q( SELECT DISTINCT ).$la.q( FROM results WHERE nbproc = 8 ORDER BY ).$la;


	my $reqa    = q( SELECT DISTINCT ).$la.q( FROM results ORDER BY ).$la;

	my $reqb_tmp = Report::f2str("$tmpldir/$name.tsql");

	open(FHTML, ">$results/$name.html") if ($html);

	if ($gnuplot) {
	    open  HTMLGP, ">_results/gp-$name.html";
	    print HTMLGP GnuplotHTML::head();
	}

	my $resa = Report::sql_selectall_arrayref($dbh, $reqa);
	for my $row (@$resa) {
	    
	    my $where = Report::sql_where($la,@$row); 
	    my $reqb = $reqb_tmp; 
	    $reqb =~ s/_WHERE_/$where/;

	    my $resb = Report::sql_fetchall_arrayref($dbh, $reqb);

# PAGE HTML	    
	    if ($html) {
		my $tmp = Report::template_apply("$tmpldir/$name.thtml", $resb);
		if (!($tmp eq '')) {
#1
		    my $gpname = "$name-".Report::tab2str(@$row);
		    print FHTML "@$row\n";
		    print FHTML GnuplotHTML::img_seq_link($gpname) if (($gnuplot) && (scalar @$resb != 1));
		    print FHTML GnuplotHTML::latex_link($gpname) if (($latex) && (scalar @$resb != 1));
		    print FHTML $tmp."\n";
		    
#2
		    if ($type eq 'HIPS') {  #HIPS specific
			$t=''; for ($i = 2; $i < 8; $i++) { $t.= '-'.$row->[$i]; }
			open(FHTML2, ">>$results/$name".$t.".html"); 
			print FHTML2 "@$row\n";
			print FHTML2 GnuplotHTML::img_seq_link($gpname) if (($gnuplot) && (scalar @$resb != 1));
			print FHTML2 GnuplotHTML::latex_link($gpname) if (($latex) && (scalar @$resb != 1));
			print FHTML2 $tmp."\n";
			close(FHTML2);		
		    }
		}
	    }

# LATEX (DATA)
	    if ($latex)
	    #if (($latex) && (scalar @$resb != 1)) 
	    {
		my $out = "$results/latex/data/$name-".Report::tab2str(@$row).".txt";#.".tex";
		open(FTEX, ">$out");
		print FTEX "% $out\n";
		print FTEX Report::template_apply("$tmpldir/$name.ttex", $resb);
		close FTEX;
	    }

# GNUPLOT (DATA)
	    #if (($gnuplot_data) && (scalar @$resb != 1)) 
	    {
		my $out = "$results/gp/data/$name-".Report::tab2str(@$row).".data";
		open(FGP, ">$out");
		print FGP Report::template_apply("$tmpldir/$name.tgp", $resb);
		close FGP;
	    }

	    if (($gnuplot) && (scalar @$resb != 1)) {
# GNUPLOT (GP)
		my @array = ($row);
		my $out   = $name."-".Report::tab2str(@$row);
		my $outgp = "$results/gp/$out.gp";
		open(FGP, ">$outgp");
		print FGP Gnuplot::GNUplot_seq(\@array, $out);
		close FGP;
		
# GNUPLOT (HTML)
		my $gpname = "$name-".Report::tab2str(@$row);
		print HTMLGP '<b>'."@$row".'</b>' 
		    .GnuplotHTML::img_seq_link($gpname)
		    .GnuplotHTML::img_seq($gpname);
		
# GNUPLOT (CONVHISTO)
		my $out   = $name  ."-".Report::tab2str(@$row)."-histo";
		my $outgp = "$results/gp/$out.gp";
		open(FGP, ">$outgp");
		print FGP Gnuplot::GNUplot_convhisto($type, $resb, $out);
		close FGP;
	    }
	    
	}

	if ($gnuplot) {
	    print HTMLGP GnuplotHTML::foot();
	    close HTMLGP;
	}

	close FHTML if ($html);

    }

    Report::disconnect($dbh);
}

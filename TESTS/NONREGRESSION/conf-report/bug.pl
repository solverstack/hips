#!/usr/bin/perl

use Report::Report;

my $tmpldir = "conf-report/tmpl";

main:
{
    my $argc = $#ARGV + 1;
    my $results; if ($argc == 1) { $results=$ARGV[0]; } else { $results="_results"; }
    my $fdb    = "$results/data.db";

    my $dbh = Report::connect($fdb);

#BUG
    {    
	my $ftmpl  = "$tmpldir/bug.thtml";
	my $fsqlS  = "$tmpldir/bugseq.tsql";
	my $fsqlP  = "$tmpldir/bugparal.tsql";
	my $out    = "$results/bug.html";

	{	
	    my @tab=();
	    
	    open(FOUT, ">$out");	    
	    
	    print FOUT "<h1>SEQUENTIAL :</h1>\n\n";
	    print FOUT Report::report_fsql($dbh, $fsqlS, $ftmpl, @tab);
	    
	    print FOUT "<h1>PARALLEL :</h1>\n\n";
	    print FOUT Report::report_fsql($dbh, $fsqlP, $ftmpl, @tab);
	    
	    close FOUT;
	}
    }

    Report::disconnect($dbh);
}








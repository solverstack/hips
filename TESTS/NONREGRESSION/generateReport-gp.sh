if [ $# -eq 1 ]
then
    results=$1
else
    results="_results"
fi

#rm $results/*.html
#rm -rf $results/gp
#rm $results/*~

for i in `find -L conf-report-gp -name "*.pl" -not -name "gnuplot-*pl"`
  do
  echo $i
  perl $i $results
  echo; echo
done

for i in `find -L conf-report-gp -name "*.pl" -name "gnuplot-*pl"`
  do
  echo $i
  perl $i $results
  echo; echo
done

find $results -size 0 -name "*.html" -exec rm {} \;

echo Gnuplot ...
#(cd $results/gp; gnuplot *.gp) : skip error :
(cd $results/gp; find . -name "*.gp" -exec gnuplot {} \;) 2>/dev/null
echo ... done
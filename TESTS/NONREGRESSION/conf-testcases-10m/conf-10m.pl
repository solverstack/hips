#!/usr/bin/perl -w

use strict;
use Submit::Submit;

require 'global.pl';

#exit;

our %global;

my %configuration = (
		     'name'          =>     '10m',
		     'descr'         =>     'recherche parametre',
		     
		     'prog'          =>     'testHIPS-RUN.ex',
		     'compilversion' =>     'scotch-complex',
		     'param'         =>     '7000', # domsize
		     'nbproc'        =>     '16',
		     
# inputs
		     'matfile'  =>     '10M_Matrice.mm',
		     'rhs'      =>     '0',
		     'method'   =>     'HYBRID',
		     'prec'     =>     '1e-7',
		     'locally'  =>     'ALL',
		     'maxit'    =>     '600',
		     'krylov'   =>     '600',
		     'droptol0' =>     '0',
		     'droptol1' =>     '0.001',
		     'droptol2' =>     '0',
		     'verbose'  =>     '5',
		     
		     'memory'   =>     '0000',
		     'timelimit'=>     '60',

                     'sym'      =>     '-1',
		     'driver'   =>     '',
		     );


my @sort = ('matfile', 'prec', 'method', 'locally', 'droptol0', 'droptol1', 'droptol2', 'param', 'nbproc');
my $sort_option = 2;

Submit::generateTestCases(\%global, \%configuration, \@sort, $sort_option);

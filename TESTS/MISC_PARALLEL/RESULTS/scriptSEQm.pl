#!/usr/bin/perl -w

#use strict;
#use warnings;
#todo

use POSIX qw(ceil floor);

require 'configuration.pl';

our $matrixlist;
our $inputsT;
our $submitT;
our $machine;
our @nbproc;
our $prog;
our $droptol0;
our @droptol1list;
our @levellist;
our @ndomlist;
our @stratlist;
our $mem;
our $temps;
our $sym;
our $driver;

#--------------------------------------------------------------

if($machine !~ /LOCAL/)
{
    if( ! -f $submitT)
    {
	print "le fichier $submitT n\'existe pas \n";
	exit;
    }
}

if( ! -f $inputsT)
{
    print "le fichier $inputsT n\'existe pas \n";
    exit;
}


if( ! -e $prog)
{
    $flag=0;
    until($flag eq 1)
    { 
	print 'Exec à utiliser ? ';
	$prog = <STDIN>;
	chop $prog;
	
	if( -e $prog)
	{
	    $flag = 1;
	}
	else
	{
	    print "Erreur: $prog n\' existe pas \n";
	}
    }
}


if( ! -f $matrixlist)
    {
	print "le fichier $matrixlist n\'existe pas \n";
	exit;
    }


## Ouverture fichier contenant liste de matrice
open(CMD, "<$matrixlist");
@matfilelist = <CMD>;
close CMD;
my @matlist;
for($i=0;$i <= $#matfilelist;$i++)
{
    chop $matfilelist[$i];
    if( $matfilelist[$i] =~ /..*/)
    {
	$matlist[$i] = $matfilelist[$i];
    }
}


for($i=0;$i <= $#matlist;$i++)
{
    if( ! -f $matlist[$i])
    {
	print "le fichier matrice : $matlist[$i]  n\'existe pas \n";
	exit;
    }
    print $matlist[$i]."\n";
}

	    
#print 'Nom des matrices :';
#$a = <STDIN>;
#chop $a;
#$ma = "$a";
#@matlist = split(/ /, $ma);


#print 'Precision de la solution :';
#$prec = <STDIN>;
#chop $prec;    
$prec = "1e-7";

#if($droptol0 < 0)
#{
#    print 'Dropping threshold interior domain :';
#    $droptol0 = <STDIN>;
#    chop $droptol0;
#}

#if($droptol1 < 0)
#{
#    print 'Dropping threshold on interface :';
#    $droptol1 = <STDIN>;
#    chop $droptol1;
#}

print "############################################## \n";
print "#  ON EST SUR $machine                         \n";
if($sym == 0)
{
    print "# MATRICES RUA PATTERN NON SYMMETRIQUES \n";
}
if($sym == 1)
{
    print "# MATRICES RUA PATTERN SYMMETRIQUES  \n";
}
if($sym == 2)
{
    print "# MATRICES RSA (SYMMETRIQUES)  \n";
}

print "# Liste matrices: @matlist \n";
print "# Liste nbprocs : @nbproc \n";
print "# Executable    : $prog \n";
print "# Domnbr        : @ndomlist \n";
print "# Level         : @levellist \n";
print "# Strat         : @stratlist \n";
print "# Precision     : $prec\n";
print "# Droptol0      : $droptol0 \n";
print "# Droptol1      : @droptol1list \n";
print "# Fichier inputs:  $inputsT \n";
if($machine !~ /LOCAL/)
{
    print "# Fichier lance : $submitT \n";
}
print "############################################## \n \n";
print "==> Est-ce correcte ? o/n \n";

if($machine !~ /LOCAL/)
{

    $ans = &ouinon;
    if( $ans =~ /n/ )
    {
	exit;
    }
    
}

foreach $droptol1 (@droptol1list) {
    
    $maindir = `pwd`;
    chop $maindir;
    $test1dir = $maindir.'/RESULT-'.$droptol1;
    
    if(! -d $test1dir)
    {
	print "Creation de $test1dir \n";
	system "mkdir $test1dir";
    }
    else
    {
	print "$test1dir Existe deja !\n";
	print "Continuer ? (o/n) \n";
	$ans = &ouinon;
	if( $ans =~ /n/ )
	{
	    exit;
	}
	
    }
    foreach $mat (@matlist)
    {
	
	foreach $locally (@levellist)
	{
	    $matinputs = $mat;
	    $matinputs =~ s/.*\///g;
	    
	    $matpath = $mat;
	    $matpath =~  s/^1//;
	    $matpath =~  s/\/1/\//;
	    $matname = $matpath;
	    $matname =~ s/.*\///g;
	    
	    if( ! -e $matpath)
	    {
		print "$matpath n'existe pas : SKIP IT \n";
		next;
	    }
	    
	    $testdir = $test1dir.'/RESULT_'.$matname."_LOC_".$locally."_".$prec;

    	    if(! -d $testdir)
	    {
		print "Creation de $testdir \n";
		system "mkdir $testdir";
	    }
	    else
	    {
		print "$testdir Existe deja !\n";
		print "Continuer ? (o/n) \n";
		$ans = &ouinon;
		if( $ans =~ /n/ )
		{
		    exit;
		}
		
	    }
	    
	    $progname = $prog;
	    $progname =~ s/.*\///g;
	    
	    print "cp -f $prog ${testdir}/$progname \n";
	    system "cp -f $prog ${testdir}/$progname";
	    
	    #### CREATION DES FICHIERS INPUTS ET SUBMIT
	    foreach $strat (@stratlist)
	    {
		
		$i = $strat;
		if( $strat =~ /NOREC/ )
		{
		    $rec = 0;
		    $i = 0;
		}
		else
		{
		    $rec = 1;
		}
		
		print "Creating run for strat ${strat} \n";
		$stratdir = $testdir . "/STRAT_${strat}";
		if( ! -d $stratdir)
		{
		    print "Creation de $stratdir \n";
		    system "mkdir $stratdir";
		}
		
		
		print "ln -sf ${testdir}/$progname ${stratdir}/ \n";
		system "ln -sf ${testdir}/$progname ${stratdir}/";
		
		if($matpath =~ /^\//)
		{
		    $command = 'ln -sf '.$matpath." ${stratdir} \n";
		}
		else
		{
		    $command = 'ln -sf ../../'.$matpath." ${stratdir} \n";
		}
		system $command;
		
		## Creation fichier inputs 
		open(CMD, "<$inputsT");
		@inp = <CMD>;
		close CMD;
		
		if($locally =~ /ALL/)
		{
		    $locally = 200;
		}
		
		$krylov = 300;
		if($strat =~ /NOREC/ || $strat =~/0/)
		{
		    $maxit = 250;
		    $inner = 0;
		}
		else
		{
		    $maxit = 1;
		    $inner = 250;
		}
		
		
		foreach (@inp)
		{
		    s/_MATFILE_/$driver$matinputs/g;
		    s/_PREC_/$prec/g;
		    s/_DROPTOL0_/$droptol0/g;
		    s/_DROPTOL1_/$droptol1/g;
		    s/_KRYLOV_/$krylov/g;
		    s/_MAXIT_/$maxit/g;
		    s/_INNER_/$inner/g;
		    s/_STRAT_/$i/g;
		    s/_SYM_/$sym/g;
		    s/_REC_/$rec/g;
		    s/_LOCALLY_/$locally/g;
		}
		open(FOUT, ">$stratdir/inputs");
		print FOUT @inp;
		close FOUT;
		
		if($locally =~ /200/) # FIX ME 
		{
		    $locally = "ALL";
		}

		## Creation fichiers submits
		foreach $np (@nbproc) {
		    
		    $lancefile = "lance.$strat.$np";
		    print $lancefile;

		    if($machine =~ /M3PEC/)
		    {
			&submit_M3PEC;
		    }
		    else
		    {
			if($machine =~ /CCRT/)
			{
			    &submit_CCRT;
			} else {
			    if($machine =~ /G5K/)
			    {
				$node = ceil($np/8);
				&submit_G5K;
			    } else {
				
				&submit_LOCAL;
			    }
			}
		    }
		}
	
	    }	
	}
    }

}

sub submit_M3PEC
{
    #### CREATION DES SCRIPTS DE SOUSMISSION
    open(CMD, "<$submitT");
    @script = <CMD>;
    close CMD;

    $fout = $matname;
    $ferror = 'Err_'.$matname;
    
    @subf = @script;
    foreach (@subf)
    {
	s/_OUTPUT_/$fout/g;
	s/_ERROR_/$ferror/g;
	s/_NPROC_/1/g;
	s/_MEMORY_/$mem/;
	s/_TEMPS_/$temps/;
	s/_INITDIR_/$stratdir/;
    }

    open(FOUT, ">$stratdir/$lancefile");
    print FOUT @subf;
    foreach $p (@ndomlist)
    {
	$resout = $matname.'_'.$p;
	print FOUT "./$progname $p > $resout ;\n";
    }

    system "llsubmit $stratdir/$lancefile";
    

}

sub submit_CCRT_1
{
    #### CREATION DES SCRIPTS DE SOUSMISSION
    open(CMD, "<$submitT");
    @script = <CMD>;
    close CMD;

    $fout = $matname;
    $ferror = 'Err_'.$matname;
    
    @subf = @script;
    foreach (@subf)
    {
	s/_OUTPUT_/$fout/g;
	s/_ERROR_/$ferror/g;
	s/_NPROC_/1/g;
	s/_MEMORY_/$mem/;
	s/_TEMPS_/$temps/;
	s/_INITDIR_/$stratdir/;
    }

    open(FOUT, ">$stratdir/$lancefile");
    print FOUT @subf;
    foreach $p (@ndomlist)
    {
	$resout = $matname.'_'.$p;
#	print FOUT "srun -n 1 -t $temps -K ./$progname $p  > $resout;\n";
	print FOUT "./$progname $p > $resout;\n";
    }
    close FOUT;

    $home=$ENV{'HOME'};
    open(TODO, ">>$home/J/job-todo");
    print TODO "$stratdir/$lancefile\n";
    close TODO;


#    system "llsubmit $stratdir/$lancefile";   

}

sub submit_CCRT
{

    foreach $p (@ndomlist)
    {

	#### CREATION DES SCRIPTS DE SOUSMISSION
	open(CMD, "<$submitT");
	@script = <CMD>;
	close CMD;
	
	$fout = $matname;
	$ferror = 'Err_'.$matname;
	
	@subf = @script;
	foreach (@subf)
	{
	    s/_OUTPUT_/$fout/g;
	    s/_ERROR_/$ferror/g;
	    s/_NPROC_/$np/g;
	    s/_MEMORY_/$mem/;
	    s/_TEMPS_/$temps/;
	    s/_INITDIR_/$stratdir/;
	}
	
	open(FOUT, ">$stratdir/$lancefile".'_'.$p);
	print FOUT @subf;
	
#	$resout = $matname.'_'.$p;
	$resout = $matname.'_'.$p.'.'.$np;
	
#	if ($np == 1) {
#	    print FOUT "./$progname $p > $resout;\n";
#	} else {
	    print FOUT "srun -n $np -t $temps -K ./$progname $p  > $resout;\n";
#	}

	close FOUT;

	$home=$ENV{'HOME'};
	open(TODO, ">>$home/J/job-todo");
	print TODO "$stratdir/$lancefile".'_'.$p."\n";
	close TODO;
    }


#    system "llsubmit $stratdir/$lancefile";   

}



sub submit_G5K
{

    foreach $p (@ndomlist)
    {

	#### CREATION DES SCRIPTS DE SOUSMISSION
	open(CMD, "<$submitT");
	@script = <CMD>;
	close CMD;
	
	$fout = $matname;
	$ferror = 'Err_'.$matname;
	
	@subf = @script;
#	foreach (@subf)
#	{
#	    s/_OUTPUT_/$fout/g;
#	    s/_ERROR_/$ferror/g;
#	    s/_NPROC_/$np/g;
#	    s/_MEMORY_/$mem/;
#	    s/_TEMPS_/$temps/;
#	    s/_INITDIR_/$stratdir/;
#	}
	
	open(FOUT, ">$stratdir/$lancefile".'_'.$p);
	print FOUT @subf;
	
	$resout = $matname.'_'.$p.'.'.$np;
	
#	print FOUT "mpirun -machinefile ".'$OAR_NODE_FILE'." -n $np ./$progname $p > $resout;\n";
	print FOUT "mpirun -machinefile ".'$OAR_NODE_FILE'." -n $np ./$progname $p\n";

	close FOUT;
	$ffff = "$stratdir/$lancefile".'_'.$p;
	system "chmod +x $ffff";


	$home=$ENV{'HOME'};
	open(TODO, ">>$home/job-todo");
		
	$lauch = "oarsub ".
	    '-t allow_classic_ssh '.
	    '-p "cluster=\'borderline\'" '.
	    "--type=besteffort ".
	    "-S $stratdir/$lancefile".'_'.$p." ".
	    "-d $stratdir ".
	    "-l /nodes=$node,walltime=$temps ".
	    "-n HIPS ".
	    "-E $resout".'.err '.
	    "-O $resout ";

#	    "-E $lancefile".'_'.$p.'.err '.
#	    "-O $lancefile".'_'.$p.'.out ';

	print TODO "$lauch\n";
	
	close TODO;
    }


#    system "llsubmit $stratdir/$lancefile";   

}


sub submit_LOCAL
{
    #### CREATION D'UN SCRIPT DE LANCEMENT #####
    

    open(FOUT, ">$stratdir/$lancefile");
    print FOUT '#!/bin/sh';
    print FOUT "\n";

    foreach $p (@ndomlist)
    {
	$resout = $matname.'_'.$p.'.'.$np;
#	$ferror = 'Err_'.$matname.'_'.$p;
	print FOUT "echo mpirun -n $np ./$progname $p > $resout ;\n";
#	if ($np == 1) {
# 	   print FOUT "./$progname $p > $resout ;\n";
#	} else {
	    print FOUT "mpirun -n $np ./$progname $p > $resout ;\n";
#	}
    }

   
 
    close FOUT;
    chdir $stratdir;
    system "chmod u+x ./$lancefile";
    system "./$lancefile";
    chdir $maindir;

}

sub ouinon
{
    local($r);
    $r = <STDIN>;
    chop $r;
    
    until($r =~ /(^o|^n)/)
    {
	print 'o/n ? ';
	$r = <STDIN>;  
	chop $r;
    }
    
    $_[0] = $r;
}

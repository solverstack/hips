/* @authors J. GAIDAMOUR, P. HENON */

#ifndef READ_PARALLEL

/* main */ {

  INTL nnz;
  int job;
  
  INTL *ib; dim_t *jb;
  int *node2dom;

  /*-----------------------------------------------------------------/
    /   Processor 0 read the matrix, compute a partition etc..         /
    /-----------------------------------------------------------------*/
  /*if(proc_id == 0)*/
  /*   { */
  
  CSRread(matrix, &n, &nnz, &ia, &ja, &a, NULL);
  assert(phidaloptions.symmetric == rsa);
  
  /************************ RSA matrix contains only the lower triangular part of the matrix ***************************/
  if(rsa == 1)
    {
      /** Symmetrize the matrix (in rsa we only have the lower triangular part) ****/
      ib = ia;
      jb = ja;
      b = a;
      numflag = 1;
      job = 2;
      PHIDAL_SymmetrizeMatrix(job, numflag, n, ib, jb, b, &ia, &ja, &a);
      nnz = 2*nnz-n;
      unsym = 0;
      free(ib);
      free(jb);
      free(b);
      fprintfv(5, stdout, "This matrix is in RSA format \n");
    }

  /*******************************************************************************************************************/
  /*******************************************************************************************************************/
  /*******************************************************************************************************************/
  if(proc_id == 0)
    fprintfv(5, stdout,"Matrix dimension is %d, Number of nonzeros is %ld\n",n,(long)nnz);



  /*****************************************************************************************************************/
  /***********************************Construct the symmetric graph G of the matrix ********************************/
  /*****************************************************************************************************************/
  ig = (INTL *)malloc(sizeof(INTL)*(n+1));
  jg = (int *)malloc(sizeof(int)*nnz);
  memcpy(ig ,ia, sizeof(INTL)*(n+1));
  memcpy(jg ,ja, sizeof(int)*nnz);

  
  if(unsym == 1)
    {
      ib = ig;
      jb = jg;
      numflag = 1;
      job = 0;
      PHIDAL_SymmetrizeMatrix(job, numflag, n, ib, jb, NULL, &ig, &jg, NULL);
      free(ib);
      free(jb);
    }
  /*****************************************************************************************************************/
  /*****************************************************************************************************************/


  /* Translate matrix into C numbering */
  /*fprintfv(5, stderr, "Convert FORTRAN to C indexing \n");*/
  CSR_Fnum2Cnum(ja, ia, n);
  CSR_Fnum2Cnum(jg, ig, n);
  numflag = 0;
  

  perm = (int *)malloc(sizeof(int)*n);
  iperm = (int *)malloc(sizeof(int)*n);

  /********************************************************/
  /* Compute a  vertex-based partition  using METIS       */
  /* then compute an edge-based partition wich ovelapps   */
  /* on the vertex separator                              */
  /********************************************************/
  node2dom = perm; /** use perm as a temporary working area **/

  /** Delete the self edge in the graph (METIS_NodeND need that)**/
  /** OIMBE Put THAT IN Perm2SizedDomain ?? **/
  PHIDAL_CsrDelDiag(numflag, n, ig, jg);

  /*#ifdef ONE_DOMAIN
  domsize = (int)(n*0.9/nproc);
  #endif*/

  /**** Compute the overlapped partition from the matrix reordering   *****/
  t1  = dwalltime(); 
  PHIDAL_Perm2SizedDomains(domsize, n, ig, jg, &ndom, &mapptr, &mapp, perm, iperm);
  t2  = dwalltime(); 

  if(proc_id == 0)
    {
      fprintfv(5, stdout, "Compute the grid of subdomain in %g \n", t2-t1);
      fprintfv(5, stdout, "DOMSIZE = %d \n", domsize);
      fprintfv(5, stdout, "Found %d domains \n", ndom);
    }
  {
    /*********************************************************/
    /*    DISPLAY SOME INFO ON THE OVERLAPPED PARTITION      */
    /*********************************************************/         
    
    int maxdom, mindom;
    REAL avgdom;


    /* fprintfv(5, stdout, "PARTITION: \n");
       for(i=0;i<ndom;i++)
       fprintfv(5, stdout, "Domain %d = %d nodes \n", i, mapptr[i+1]-mapptr[i]);*/
    
    maxdom = 0;
    mindom = mapptr[1]-mapptr[0];
    avgdom = 0.0;
    for(i=0;i<ndom;i++)
      {
	avgdom += mapptr[i+1]-mapptr[i];
	if(mapptr[i+1]-mapptr[i] > maxdom)
	  maxdom = mapptr[i+1]-mapptr[i];
	if(mapptr[i+1]-mapptr[i] < mindom)
	  mindom = mapptr[i+1]-mapptr[i];
      }
    avgdom /= ndom;

    if(proc_id == 0)
      {
	fprintfv(5, stdout, "MAX DOMAIN = %d \n", maxdom);
	fprintfv(5, stdout, "MIN DOMAIN = %d \n", mindom);
	fprintfv(5, stdout, "AVG DOMAIN = %g \n", avgdom);
	/*fprintfv(5, stdout, "IMBALANCE = %g \n",
	  ((float)(maxdom-mindom))*100.0/mindom );*/
      }
  }
    

}

#else

/* main */ {

  /*******************************************************************/
  /*******************************************************************/
  /**                                                               **/
  /** LOAD THE LOCAL CSR MATRIX AND THE GLOBAL HID FROM DISK        **/
  /**                                                               **/
  /*******************************************************************/
  /*******************************************************************/
  
  /** Build the file name associated to the local processor **/
  sprintf(filename, "%s_%d.%d.%d", sfile_path, domsize, nproc, proc_id);

  fp = fopen(filename, "r");
  if(fp == NULL)
    { 
      fprintfv(5, stderr, "Proc %d ERROR: Unable to open file %s \n", proc_id, filename);
      MPI_Abort(MPI_COMM_WORLD, -1);
    }

  /** Load the local csr matrix **/
  fprintfv(5, stderr, "Proc %d: Load local matrix from file %s ...\n", proc_id, filename);
  CSR_Load(fp, &phidaloptions.symmetric, &ln, &lia, &lja, &la);
  fprintfv(5, stderr, "done \n");

  fclose(fp);


  /*************************/
  /** Load the global HID **/
  /*************************/

  sprintf(filename, "%s_%d.%d.HID", sfile_path, domsize, nproc);
  fprintfv(5, stderr, "Proc %d: Load HID and domain decomposition from file %s ...\n", proc_id, filename);
  fp = fopen(filename, "r");
  if(fp == NULL)
    { 
      fprintfv(5, stderr, "Proc %d ERROR: Unable to open file %s \n", proc_id, filename);
      MPI_Abort(MPI_COMM_WORLD, -1);
    }

  PHIDAL_LoadHID(fp, &BL);
  
  ndom = BL.ndom;
  
  /** load the inverse permutation done by the HID **/
  iperm = (int *)malloc(sizeof(int)*(BL.n));
  for(i=0;i<=BL.n;i++)
    fscanf(fp, "%d ", iperm+i);
  
  /** Load the user domain decomposition **/
  mapptr = (int *)malloc(sizeof(int)*(ndom+1));
  for(i=0;i<=ndom;i++)
    fscanf(fp, "%d ", mapptr+i);

  mapp = (int *)malloc(sizeof(int)* mapptr[ndom]);
  for(i=0;i<mapptr[ndom];i++)
    fscanf(fp, "%d ", mapp+i);



  fprintfv(5, stderr, "done \n");
  fclose(fp);

#ifdef BLOCK
  /**********************************/
  /** Load the global SymbolMatrix **/
  /**********************************/
  sprintf(filename, "%s_%d.%d.Symbol", sfile_path, domsize, nproc);
  fprintfv(5, stderr, "Proc %d: Load SymbolMatrix from file %s ...\n", proc_id, filename);
  fp = fopen(filename, "r");
  if(fp == NULL)
    { 
      fprintfv(5, stderr, "Proc %d ERROR: Unable to open file %s \n", proc_id, filename);
      MPI_Abort(MPI_COMM_WORLD, -1);
    }

  symbmtx = (SymbolMatrix*)malloc(sizeof(SymbolMatrix));
  PHIDAL_LoadSymbolMatrix(fp, symbmtx);
  
  fprintfv(5, stderr, "done \n");
  
  fclose(fp);
#endif
}

#endif

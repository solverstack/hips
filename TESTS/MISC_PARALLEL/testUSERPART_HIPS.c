/* @authors J. GAIDAMOUR, P. HENON */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <mpi.h>
#include "math.h"

#include "base.h"
#include "io.h"
#include "block.h" /* FLOAT */
#include "db_struct.h"
#include "phidal_parallel.h"
#include "scotch_metis_wrapper.h"

#define BUFLEN 200

/* #define ONE_DOMAIN */

int main(int argc, char *argv[])
{
  /*unsym  --  symmetric pattern(0), nonsym pattern(1) */
  int unsym;

  /* working array for reading matrix */
  REAL res;
  chrono_t t1,t2;
  flag_t numflag;
  dim_t *mapp,*mapptr,i; 
  int nproc, ndom;
  int domsize=-1;
  int proc_id;
  COEF *x;
  COEF *b, *r;
  int ln; INTL *lia; dim_t *lja;
  COEF *la;

  PhidalDistrMatrix A;
  PhidalHID BL;
  PhidalDistrHID DBL;
  PhidalDistrPrec P;


  
  /* 
   * buf is for read from 'inputs' file, 
   * matrix represents matrix name
   */
  char matrix[BUFLEN];
  char sfile_path[BUFLEN];

  int *iperm;
  int rsa;

  PhidalOptions phidaloptions;
  PhidalOptions* option= &phidaloptions;

#ifndef READ_PARALLEL
  dim_t n;
  COEF *a;
  INTL *ia, *ig;
  dim_t *ja, *jg;
  dim_t *perm;
#else
  FILE *fp;
  char filename[BUFLEN];
#endif

  /** Init MPI environment **/
  MPI_Init(&argc, &argv);
  MPI_Comm_size(MPI_COMM_WORLD, &nproc);
  MPI_Comm_rank(MPI_COMM_WORLD, &proc_id);

  if(argc < 2)
    {
      ndom = nproc;
    }
 


  PhidalOptions_Init(&phidaloptions);

  /** Read parameters from the input file **/
  GENERAL_setpar(NULL, matrix, sfile_path, &unsym, &rsa, &phidaloptions);   

  /************************************************************************************************************/
  /****************************************** READ THE MATRIX *************************************************/
  /************************************************************************************************************/
  {
    INTL nnz;
    int job;
   
    INTL *ib; dim_t *jb; 
    int *node2dom;
   
    /*-----------------------------------------------------------------/
      /   Processor 0 read the matrix, compute a partition etc..         /
      /-----------------------------------------------------------------*/
    /*if(proc_id == 0)*/
    /*   { */
   
    CSRread(matrix, &n, &nnz, &ia, &ja, &a, NULL);
    assert(phidaloptions.symmetric == rsa);

    if(argc >= 2)
      {
	int domsize;
	domsize = atoi(argv[1]);
	ndom = n/(domsize+1);
	if(ndom == 0)
	  ndom = 1;
      }
    /************************ RSA matrix contains only the lower triangular part of the matrix ***************************/
    if(rsa == 1)
      {
	/** Symmetrize the matrix (in rsa we only have the lower triangular part) ****/
	ib = ia;
	jb = ja;
	b = a;
	numflag = 1;
	job = 2;
	PHIDAL_SymmetrizeMatrix(job, numflag, n, ib, jb, b, &ia, &ja, &a);
	nnz = 2*nnz-n;
	unsym = 0;
	free(ib);
	free(jb);
	free(b);
	fprintfv(5, stdout, "This matrix is in RSA format \n");
      }

    /*******************************************************************************************************************/
    /*******************************************************************************************************************/
    /*******************************************************************************************************************/
    if(proc_id == 0)
      fprintfv(5, stdout,"Matrix dimension is %d, Number of nonzeros is %ld\n",n,(long)nnz);



    /*****************************************************************************************************************/
    /***********************************Construct the symmetric graph G of the matrix ********************************/
    /*****************************************************************************************************************/
    ig = (INTL *)malloc(sizeof(INTL)*(n+1));
    jg = (int *)malloc(sizeof(int)*nnz);
    memcpy(ig ,ia, sizeof(INTL)*(n+1));
    memcpy(jg ,ja, sizeof(int)*nnz);

  
    if(unsym == 1)
      {
	ib = ig;
	jb = jg;
	numflag = 1;
	job = 0;
	PHIDAL_SymmetrizeMatrix(job, numflag, n, ib, jb, NULL, &ig, &jg, NULL);
	free(ib);
	free(jb);
      }
    /*****************************************************************************************************************/
    /*****************************************************************************************************************/


    /* Translate matrix into C numbering */
    /*fprintfv(5, stderr, "Convert FORTRAN to C indexing \n");*/
    CSR_Fnum2Cnum(ja, ia, n);
    CSR_Fnum2Cnum(jg, ig, n);
    numflag = 0;
  

    perm = (int *)malloc(sizeof(int)*n);
    iperm = (int *)malloc(sizeof(int)*n);

    /********************************************************/
    /* Compute a  vertex-based partition  using METIS       */
    /* then compute an edge-based partition wich ovelapps   */
    /* on the vertex separator                              */
    /********************************************************/
    node2dom = perm; /** use perm as a temporary working area **/

    /** Delete the self edge in the graph (METIS_NodeND need that)**/
    /** OIMBE Put THAT IN Perm2SizedDomain ?? **/
    PHIDAL_CsrDelDiag(numflag, n, ig, jg);

    /*#ifdef ONE_DOMAIN
      domsize = (int)(n*0.9/nproc);
      #endif*/

    /**** Compute the overlapped partition from the matrix reordering   *****/
    t1  = dwalltime(); 
    /*PHIDAL_Perm2SizedDomains(domsize, n, ig, jg, &ndom, &mapptr, &mapp, perm, iperm);*/
    {
      int *node2dom = perm; /** use perm as a temporary working area **/


      if(ndom > 1) 
	{
	  numflag = 0;
#ifdef SCOTCH_PART	  
	  {
	    SCOTCH_Graph        grafdat;
	    SCOTCH_Strat        grafstrat;
	    SCOTCH_graphInit  (&grafdat);
	    /*SCOTCH_graphBuild (&grafdat, 0, n, ia, NULL, vwgt, NULL, ia[n], ja, ewgt);*/
	    SCOTCH_graphBuild_WRAPPER(&grafdat, 0, n, ig, NULL, jg, NULL);
	    
	    if(SCOTCH_graphCheck(&grafdat) != 0)
	      {
		fprintfv(5, stderr," Error in scotch graph check \n");
		exit(-1);
	      }
	    /*SCOTCH_graphSave (&grafdat, stdout);*/
	    
	    SCOTCH_stratInit (&grafstrat);
	    SCOTCH_graphPart_WRAPPER (n, &grafdat, ndom, &grafstrat, node2dom);
	    SCOTCH_graphExit (&grafdat);
	    
	  } 
#else
	  {
	    int metisoption[10];
	    flag_t wgtflag = 0;
	    int volume;
	    wgtflag = 0;
	    metisoption[0] = 0;
	    fprintfv(5, stdout, "Partition the graph using METIS \n");
	    /*METIS_PartGraphVKway(&n, ig, jg, NULL, NULL, &wgtflag, &numflag, &ndom, metisoption, &volume, node2dom);*/
	    METIS_PartGraphVKway_WRAPPER(n, ig, jg, NULL, NULL, wgtflag, numflag, ndom, metisoption, &volume, node2dom);
	    fprintfv(5, stdout, "partition done \n");
	  }
#endif
	}
      else 
	if(ndom == 1) 
	  for(i = 0; i < n; i++) 
	    node2dom[i] = 0;
      
      for(i=0;i<n;i++)
	assert(node2dom[i] >= 0 && node2dom[i]<ndom);
      
      /** Transform the vertex-based partition into an edge-based partition **/
      PHIDAL_Partition2OverlappedPartition(0, ndom, n, ig, jg, node2dom, &mapp, &mapptr);  
      for(i=0;i<n;i++)
	{
	  perm[i] = i;
	  iperm[i] = i;
	}
    }

    t2  = dwalltime(); 
    if(proc_id == 0)
      {
	fprintfv(5, stdout, "Compute the grid of subdomain in %g \n", t2-t1);
	fprintfv(5, stdout, "DOMSIZE = %d \n", domsize);
	fprintfv(5, stdout, "Found %d domains \n", ndom);
      }
    {
      /*********************************************************/
      /*    DISPLAY SOME INFO ON THE OVERLAPPED PARTITION      */
      /*********************************************************/         
    
      int maxdom, mindom;
      REAL avgdom;


      /* fprintfv(5, stdout, "PARTITION: \n");
	 for(i=0;i<ndom;i++)
	 fprintfv(5, stdout, "Domain %d = %d nodes \n", i, mapptr[i+1]-mapptr[i]);*/
    
      maxdom = 0;
      mindom = mapptr[1]-mapptr[0];
      avgdom = 0.0;
      for(i=0;i<ndom;i++)
	{
	  avgdom += mapptr[i+1]-mapptr[i];
	  if(mapptr[i+1]-mapptr[i] > maxdom)
	    maxdom = mapptr[i+1]-mapptr[i];
	  if(mapptr[i+1]-mapptr[i] < mindom)
	    mindom = mapptr[i+1]-mapptr[i];
	}
      avgdom /= ndom;

      if(proc_id == 0)
	{
	  fprintfv(5, stdout, "NDOM = %d \n" ,ndom);
	  fprintfv(5, stdout, "MAX DOMAIN = %d \n", maxdom);
	  fprintfv(5, stdout, "MIN DOMAIN = %d \n", mindom);
	  fprintfv(5, stdout, "AVG DOMAIN = %g \n", avgdom);
	  /*fprintfv(5, stdout, "IMBALANCE = %g \n",
	    ((float)(maxdom-mindom))*100.0/mindom );*/
	}
    }
    
  }

#ifndef READ_PARALLEL
  /*******************************************************************/
  /*******************************************************************/
  /**                                                               **/
  /** COMPUTE THE HIERARCHICAL DECOMPOSITION AND LOCAL DATA         **/
  /**                                                               **/
  /*******************************************************************/
  /*******************************************************************/
  
  /** Compute the global hierarchical interface decomposition and the associated permutation **/ 
  numflag = 0;
  PhidalHID_Init(&BL);
  
  t1  = dwalltime(); 
  PHIDAL_HierarchDecomp(phidaloptions.verbose, numflag, n, ig, jg, mapp, mapptr, ndom, &BL, perm, iperm);
  t2  = dwalltime(); 
  
  if(proc_id == 0)
    fprintfv(5, stdout, " Compute Phidal Hierarchical Decomposition in %g seconds \n\n", t2-t1);


  
 
  free(perm);
#endif

  /*fprintfv(5, stderr, "Proc %d Compute the local HID \n", proc_id);*/
  PhidalDistrHID_Setup(proc_id, nproc,0,  &BL, &DBL, iperm, MPI_COMM_WORLD);
  free(iperm);
  fprintfv(5, stderr, "Proc %d: Domain size = %d interior = %d \n", proc_id, DBL.LHID.n, DBL.LHID.block_index[DBL.LHID.block_levelindex[1]]);


  if(DBL.LHID.n == 0)
    {
      fprintfv(5, stderr, "ERROR too much processors \n");
      exit(-1);
    }

  ln = DBL.LHID.n;
#ifndef READ_PARALLEL
  /*** Build the local part of the csr matrix ****/
  /*fprintfv(5, stderr, "Compute the local submatrix ... ");*/
  /*ln = mapptr[proc_id+1]-mapptr[proc_id];
    CSR_GetSquareSubmatrix(ln, mapp + mapptr[proc_id], 
    n, ia, ja, a,
    &lia, &lja, &la);*/

#define REORDER
#ifdef REORDER
  {
    INTL *lig;
    dim_t *ljg;
    int fillopt[10];
    CSR_GetSquareSubmatrix(ln, DBL.loc2orig, 
			   n, ig, jg, NULL,
			   &lig, &ljg, NULL);
    fillopt[0] = 2; /** 0 ND, 1 MD, 2 MF **/
    PHIDAL_MinimizeFill(fillopt, ln, lig, ljg, &DBL.LHID, DBL.loc2orig);

    for(i=0;i<ln;i++)
      DBL.orig2loc[DBL.loc2orig[i]] = i;

    free(lig);
    free(ljg);
  }
#endif
  CSR_GetSquareSubmatrix(ln, DBL.loc2orig, 
			 n, ia, ja, a,
			 &lia, &lja, &la);
    
  

  /** DO NOT NEED THESE VECTORS ANYMORE **/
  free(a);
  free(ja);
  free(ia);
  free(ig);
  free(jg);
#endif

  



  /*** Build the PHIDAL matrix ******/
  
  fprintfv(5, stderr, "Proc %d Build the PHIDAL Distributed matrix  ... ", proc_id);
  /*PHIDAL_SetMatrixCoef(numflag, ln, lia, lja, la, mapp + mapptr[proc_id], rsa,  &A, &DBL);*/

  /*fprintfv(5, stderr, "\n \n RSA FORCE TO NULL \n \n");
    rsa = 0;*/

  PHIDAL_SetMatrixCoef(0, 0, numflag, ln, lia, lja, la, DBL.loc2orig, rsa, rsa,  &A, &DBL);


  fprintfv(5, stderr, "Proc %d done MATRIX BUILDING\n", proc_id);

  /** DO NOT NEED THESE VECTORS ANYMORE **/
  free(mapp);
  free(mapptr);
  free(la);
  free(lja);
  free(lia);
  /**/

  /*fprintfv(5, stderr, "Proc %d A norms = \n", proc_id);
    for(i=A.M.tli;i<=A.M.bri;i++)
    for(j=A.M.ria[i]; j<A.M.ria[i+1];j++)
    if(A.rlead[j] == proc_id)
    fprintfv(5, stderr, "block (%d %d) = %g nnz = %ld \n", A.M.rja[j], i, CSnormFrob(A.M.ra[j]), (long)CSnnz(A.M.ra[j]));*/


  
  /********* Construct b = A.x x = 1 ****/
  x = (COEF *)malloc(sizeof(COEF)*ln);
  for(i=0;i<ln;i++)
    x[i] = 1.0;
  
  b = (COEF *)malloc(sizeof(COEF)*ln);
  
  
  /*fprintfv(5, stderr, "Proc %d Setup the communicator for vectors \n", proc_id);*/
  PhidalCommVec_Setup(0, &A, &A.commvec, &DBL);


  /*** Compute the norms ****/
  /*PhidalDistrMatrix_RowNorm2(&A, &DBL, b);
    fprintfv(5, stderr, "Norm Frob of A = %g \n", dist_ddot(proc_id, b, b, 0, DBL.LHID.nblock-1, &DBL));*/

  /*fprintfv(5, stderr, "Proc %d Matvec \n", proc_id);*/
  PhidalDistrMatrix_MatVec(0, &A, &DBL, x, b);
  /*fprintfv(5, stderr, "Proc %d done \n", proc_id);*/

  /*memcpy(x, b, sizeof(COEF)*ln);
    fprintfv(5, stderr, "Proc %d Matvec \n", proc_id);
    PhidalDistrMatrix_MatVec(&A, &DBL, x, b);
    fprintfv(5, stderr, "Proc %d done\n", proc_id);*/
  res = sqrt(dist_ddot(proc_id, b, b, 0, DBL.LHID.nblock-1, &DBL));
  if(proc_id == 0)
    fprintfv(5, stderr, "Norm of b = %g \n", res);

  /*phidaloptions.krylov_method = 0;*/
  t1  = dwalltime(); 

  P.info = NULL;

  PHIDAL_DistrPrecond(&A, &P, &DBL, &phidaloptions);
  t2  = dwalltime(); 
  if(proc_id == 0)
    fprintfv(5, stdout, "\n PRECOND IN %g \n", t2-t1);
  
  fprintfv(5, stdout, "NNZ in Prec = %g \n", (REAL)PhidalDistrPrec_NNZ_All(&P, DBL.mpicom));
  fprintfv(5, stdout, "NNZ in A = %g \n", (REAL)PhidalDistrMatrix_NNZ_All(&A, DBL.mpicom));
  fprintfv(5, stdout, "Proc %d Fill Ratio of Preconditioner = %g \n\n", proc_id,
	   ((REAL)PhidalDistrPrec_NNZ_All(&P, DBL.mpicom))/((REAL)PhidalDistrMatrix_NNZ_All(&A, DBL.mpicom)));

  bzero(x, sizeof(COEF)*ln);
  t1  = dwalltime(); 
  PHIDAL_DistrSolve(&A, &P, &DBL, &phidaloptions, b, x, NULL, NULL);
  t2  = dwalltime(); 
  if(proc_id == 0)
    fprintfv(5, stdout, "\n PROC %d SOLVED IN %g \n",proc_id,  t2-t1);

  r = (COEF *)malloc(sizeof(COEF)*ln);
  memcpy(r, b, sizeof(COEF)*ln);
  PhidalDistrMatrix_MatVecSub(0, &A, &DBL, x, r);
  res = sqrt(dist_ddot(proc_id, r, r, A.M.tli, A.M.bri,&DBL))/sqrt(dist_ddot(proc_id, b, b, A.M.tli, A.M.bri, &DBL));
  if(proc_id == 0)
    fprintfv(5, stdout, "Relative residual norm = %g \n", res);

  free(r);
  PhidalCommVec_Clean(&A.commvec);
  PhidalDistrPrec_Clean(&P);
  PhidalDistrMatrix_Clean(&A);
  PhidalDistrHID_Clean(&DBL);
  PhidalHID_Clean(&BL); 

  free(b);
  free(x);

  PhidalOptions_Clean(&phidaloptions);
  /*MPI_Barrier(MPI_COMM_WORLD);*/
  if(proc_id == 0)
    fprintfv(5, stdout, "PROC %d END \n", proc_id);

  /** End MPI **/
  MPI_Finalize();

  return 0;
}

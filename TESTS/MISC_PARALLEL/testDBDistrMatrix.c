 /* @authors J. GAIDAMOUR, P. HENON */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <mpi.h>
#include "math.h"

#include "base.h"
#include "io.h"
#include "block.h" /* FLOAT */
#include "db_struct.h"
#include "phidal_parallel.h"
#include "db_parallel.h"

#define BUFLEN 200



void ascend_column_reorder2(int n, int* ia, int* ja, COEF* a);

int main(int argc, char *argv[])
{
  /*unsym  --  symmetric pattern(0), nonsym pattern(1) */
  int unsym;

  /* declarations related to Harwell-boeing format for reading the HB
     matri. Second part is related to I/O parameters */
#ifndef READ_PARALLEL
  int n;
#endif

  /* working array for reading matrix */
  chrono_t t1,t2;
  REAL res;
  int numflag=-1;
  int *mapp,*mapptr,i;
  int nproc, ndom;
  int domsize;
  int proc_id;
  COEF *x;
  COEF *b;
  int ln; INTL *lia; dim_t *lja;
  COEF *la;
  PhidalDistrMatrix A;
  PhidalHID BL;
  PhidalDistrHID DBL;
  SymbolMatrix* symbmtx, *symbloc;

  /* 
   * buf is for read from 'inputs' file, 
   * matrix represents matrix name
   */
  char matrix[BUFLEN];
  char sfile_path[BUFLEN];

  int *iperm;
  int rsa;

  PhidalOptions  phidaloptions;
  PhidalOptions* option= &phidaloptions;

  DBDistrPrec P;

#ifndef READ_PARALLEL
  /* int n; */
  COEF *a;
  INTL *ia, *ig;
  dim_t *ja, *jg;
  int *perm;
#else
  FILE *fp;
  char filename[BUFLEN];
#endif

  /** Init MPI environment **/
  MPI_Init(&argc, &argv);
  MPI_Comm_size(MPI_COMM_WORLD, &nproc);
  MPI_Comm_rank(MPI_COMM_WORLD, &proc_id);

  /*   if(proc_id !=0) fclose(stdout); */
  /*   if(proc_id !=0) fclose(stderr); */

  if(argc < 2)
    {
      fprintferr(stderr, "Illegal argument : testALLREAD_HIPS.ex <domain size (in number of node)> \n");
      exit(-1);
    }
  
  domsize = atoi(argv[1]);

  PhidalOptions_Init(&phidaloptions);

  /** Read parameters from the input file **/
  GENERAL_setpar(NULL, matrix, sfile_path, &unsym, &rsa, &phidaloptions);   

  /************************************************************************************************************/
  /****************************************** READ THE MATRIX *************************************************/
  /************************************************************************************************************/
#define BLOCK
#include "matrix_read.c"

#ifndef READ_PARALLEL
  /*******************************************************************/
  /*******************************************************************/
  /**                                                               **/
  /** COMPUTE THE HIERARCHICAL DECOMPOSITION AND LOCAL DATA         **/
  /**                                                               **/
  /*******************************************************************/
  /*******************************************************************/
  
  /** Compute the global hierarchical interface decomposition and the associated permutation **/ 
  numflag = 0;
  PhidalHID_Init(&BL);
  
  t1  = dwalltime(); 
  PHIDAL_HierarchDecomp(phidaloptions.verbose, numflag, n, ig, jg, mapp, mapptr, ndom, &BL, perm, iperm);
  t2  = dwalltime(); 
  
  if(proc_id == 0)
    fprintfv(5, stdout, " Compute Phidal Hierarchical Decomposition in %g seconds \n\n", t2-t1);

  /****************************************/
  /*** Compute the global symbol matrix ***/
  /****************************************/
  fprintfv(5, stdout, "Build Symbolic Matrix \n");
  symbmtx = (SymbolMatrix*)malloc(sizeof(SymbolMatrix));
  if(phidaloptions.schur_method != 1)
    HIPS_SymbolMatrix(phidaloptions.verbose, numflag, 0.05, phidaloptions.locally_nbr, n,  ig,  jg, 
		      &BL, symbmtx, perm, iperm);
  else
    HIPS_SymbolMatrix(phidaloptions.verbose, numflag, 0.05, BL.nlevel, n,  ig,  jg, 
		      &BL, symbmtx, perm, iperm);

  /* **** */
  HIPS_CountNNZ_L(0, symbmtx, &BL);
  HIPS_CountNNZ_E(n,  ig,  jg, perm, &BL);
  /* **** */  

  free(ig);
  free(jg);

  /* free(perm); not here */
#endif  
  
  /****************************************/
  /*** Compute the local HID            ***/
  /****************************************/
  fprintfv(5, stderr, "Proc %d Compute the local HID ... \n", proc_id);
  PhidalDistrHID_Setup(proc_id, nproc, 0, &BL, &DBL, iperm, MPI_COMM_WORLD);
  /* free(iperm); not here */
  fprintfv(5, stderr, "Proc %d: Domain size = %d interior = %d \n", proc_id, DBL.LHID.n, DBL.LHID.block_index[DBL.LHID.block_levelindex[1]]);

/* #ifdef DEBUG_M undeclared n */
/*   { */
/*     if(nproc == 1) { */
/*       for(i=0;i<n;i++) */
/* 	if(iperm[i] != DBL.loc2orig[i]) */
/* 	  fprintfv(5, stderr, "i %d erreur \n", i); */
/*     } */
/*   } */
/* #endif */

  ln = DBL.LHID.n;
#ifndef READ_PARALLEL
  /*** Build the local part of the csr matrix ****/
  /*fprintfv(5, stderr, "Compute the local submatrix ... ");*/
  /*ln = mapptr[proc_id+1]-mapptr[proc_id];
    CSR_GetSquareSubmatrix(ln, mapp + mapptr[proc_id], 
    n, ia, ja, a,
    &lia, &lja, &la);*/
  CSR_GetSquareSubmatrix(ln, DBL.loc2orig, 
			 n, ia, ja, a,
			 &lia, &lja, &la);
    
  fprintfv(5, stderr, "done \n");

#ifdef TOTO
  {
    csptr mat;
    PhidalMatrix *m;
    
    fprintfv(5, stdout, "Permuting the matrix \n");
    mat = (csptr)malloc(sizeof(struct SparRow));
    
    if(initCS(mat, n)) 
      {
	printfv(5, " ERROR SETTING UP bmat IN initCS \n") ;
	exit(0);
      }
    CSRcs(n, a, ja, ia, mat);
    
    /*** Permute the matrix according to the phidal ordering ***/
    CS_Perm(mat, perm);/* ! */
    ascend_column_reorder(mat);/* todo : utile ? */

    for(i=0;i<mat->nnzrow[0];i++) 
      fprintfv(5, stderr, "%d (%g)- ", mat->ja[0][i], mat->ma[0][i]);
    fprintfv(5, stderr, "\n");

    /*m  = (PhidalMatrix *)malloc(sizeof(PhidalMatrix));
      PHIDAL_MatrixBuild(mat, m, &BL, &phidaloptions);
      fdumpPhidalMatrix("seq.txt", m);

      cleanCS(mat);
      initCS(mat, ln);
      CSRcs(ln, la, lja, lia, mat);
      ascend_column_reorder(mat);
      PhidalMatrix_Clean(m);
      PHIDAL_MatrixBuild(mat, m, &BL, &phidaloptions);

      fdumpPhidalMatrix("para.txt", m);*/
  }  

#endif




  /** DO NOT NEED THESE VECTORS ANYMORE **/
  free(perm);
  free(iperm);
  free(a);
  free(ja);
  free(ia);

#endif /* READ_PARALLEL */

  /****************************************/
  /*** Compute the local symbolmatrix   ***/
  /****************************************/
  symbloc = (SymbolMatrix *)malloc(sizeof(SymbolMatrix));
  SymbolMatrix_GetLocal(symbmtx, symbloc, &BL, &DBL);

#ifdef DEBUG_M
  SymbolMatrix_FillCheck(ln, lia, lja, symbloc);
#endif

  /*** Build the PHIDAL matrix ******/
  fprintfv(5, stderr, "Build the PHIDAL Distributed matrix  ... ");
  assert(numflag != -1); /* debug ‘numflag’ may be used uninitialized in this function */
  PHIDAL_SetMatrixCoef(0, 0, numflag, ln, lia, lja, la, DBL.loc2orig, rsa,  rsa, &A, &DBL);
  fprintfv(5, stderr, "done \n");
  
#ifdef TITI
  {
    csptr mat = (csptr)malloc(sizeof(struct SparRow));
    initCS(mat, ln) ;
    CSRcs(ln, la, lja, lia, mat);
    ascend_column_reorder(mat);
    PhidalMatrix* m  = (PhidalMatrix *)malloc(sizeof(PhidalMatrix));
    PHIDAL_MatrixBuild(mat, m, &DBL.LHID, &phidaloptions);
    if(proc_id == 0)
      {
	fdumpPhidalMatrix("seq.txt", m);
	fdumpPhidalMatrix("para.txt", &A.M);
      }
    else
      {
	fdumpPhidalMatrix("seq1.txt", m);
	fdumpPhidalMatrix("para1.txt", &A.M);
      }
  }
#endif

  /** DO NOT NEED THESE VECTORS ANYMORE **/
  free(mapp);
  free(mapptr);
  free(la);
  free(lja);
  free(lia);
  /**/

  DBDistrPrec_Init(&P);  

  fflush(stdout);
  MPI_Barrier(MPI_COMM_WORLD); /*tmp */
  
  DBDISTRMATRIX_Precond(&A, &P, symbloc, &DBL, &phidaloptions);

  /**/ /**/ /**/ /**/ /**/
  /********* Construct b = A.x x = 1 ****/
  x = (COEF *)malloc(sizeof(COEF)*ln);
  for(i=0;i<ln;i++)
    x[i] = 1.0;
  
  b = (COEF *)malloc(sizeof(COEF)*ln);
  
  
  /*fprintfv(5, stderr, "Proc %d Setup the communicator for vectors \n", proc_id);*/
  /* PhidalCommVec_Setup(0, &A, &DBL); /\* fait dans DBMATRIX_MLICC *\/ */
  
  /*** Compute the norms ****/
  PhidalDistrMatrix_MatVec(0, &A, &DBL, x, b);

  res = sqrt(dist_ddot(proc_id, b, b, 0, DBL.LHID.nblock-1, &DBL));
  if(proc_id == 0)
    fprintfv(5, stderr, "Norm of b = %g \n", res);

  /**/ /**/ /**/ /**/ /**/
#ifdef TEST_ME
  assert(P.forwardlev == 0);

  PhidalDistrMatrix L, U;
  COEF * D;

  D = (COEF *)malloc(sizeof(COEF)*ln);
  DBMatrix_GetDiag(&P.L->M, D);  /* TODO : a inclure dans DBDistrMatrix2PhidalDistrMatrix ? */

  DBDistrMatrix2PhidalDistrMatrix(P.L, &L, phidaloptions.locally_nbr, &BL, 0/*alloc : todo 1*/, 1/*diagonal sÃ©parÃ©e*/);
  free(P.L);
  P.L = NULL; /* DBMatrix_Clean in DBMatrix2PhidalMatrix() */

  /**/ /**/ /**/ /**/ /**/

  /*   {  */
  /*     FILE* stream = fopen("db.txt", "w"); */
  /*     dumpPhidalMatrix(stream, &L.M); */
  /*     fclose(stream); */
  /*   } */
  
  /**/ /**/ /**/ /**/ /**/
  PhidalCommVec_Setup(1, &L, &DBL);
  
  /*** Compute the norms ****/
  PhidalDistrMatrix_RowNorm2(&L, &DBL, x);
  res =  dist_ddot(proc_id, x, x, 0, DBL.LHID.nblock-1, &DBL);
  if(proc_id == 0)
    fprintfv(5, stderr, "\n Norm Frob of L = %g \n",res);

  /**/ /**/ /**/ /**/ /**/
  PhidalDistrMatrix_BuildVirtualMatrix(0, 0, DBL.LHID.nblock-1, DBL.LHID.nblock-1, &L, &U, &DBL);
  PhidalDistrMatrix_Transpose(&U);
  PhidalCommVec_Setup(2, &U, &DBL);

  bzero(x, sizeof(COEF)*ln);
  t1  = dwalltime();

  {
    PhidalDistrPrec P;
    PhidalDistrPrec_Fake(&P, &L, &U, D, rsa, &phidaloptions);
    
/*     HIPS_DistrFgmresd_PH_PH(phidaloptions.verbose, phidaloptions.tol, phidaloptions.itmax, */
/* 			    &A, &P, */
/* 			    &DBL, &phidaloptions, */
/* 			    b, x, stdout); */

  /*   { */
/*       if (DBL.proc_id == 0) { */
/* 	FILE* stream = fopen("x.txt", "w"); */
/* 	for(i=0; i<ln; i++) { */
/* 	  fprintfv(5, stream, "%lf\n", x[i]); */
/* 	} */
/* 	fclose(stream); */
/*       } */
/*     } */
      
    DBMATRIX_DistrSolve(&A, &P, &DBL, &phidaloptions, b, x, NULL, NULL);

  }

  t2  = dwalltime();
  if(proc_id == 0)
    fprintfv(5, stdout, "\n SOLVED IN %g \n", t2-t1);



  /** End MPI **/
  fflush(stdout);
  MPI_Barrier(MPI_COMM_WORLD);
  fprintfv(5, stdout, "END %d \n", proc_id);
  MPI_Finalize();
  exit(1);
#endif

  /**/ /**/
  bzero(x, sizeof(int)*ln);
  t1  = dwalltime(); 

  {

    DBMATRIX_DistrSolve(&A, &P, &DBL, &phidaloptions, b, x, NULL, NULL);


    /* HIPS_DistrFgmresd_PH_DB(phidaloptions.verbose, phidaloptions.tol, phidaloptions.itmax, */
    /* 			    &A, &P, */
    /* 			    &DBL, &phidaloptions, */
    /* 			    b, x, stdout); */
  }

  t2  = dwalltime(); 
  if(proc_id == 0)
    fprintfv(5, stdout, "\n SOLVED IN %g \n", t2-t1);

  {
    COEF* r;

    r = (COEF *)malloc(sizeof(COEF)*ln);
    memcpy(r, b, sizeof(COEF)*ln);
    PhidalDistrMatrix_MatVecSub(0, &A, &DBL, x, r);
    res = sqrt(dist_ddot(proc_id, r, r, A.M.tli, A.M.bri,&DBL))/sqrt(dist_ddot(proc_id, b, b, A.M.tli, A.M.bri, &DBL));
    if(proc_id == 0)
      fprintfv(5, stdout, "Relative residual norm = %g \n", res);
    free(r);
  }

  /**/ /**/

  /*   for(i=0;i<ln;i++) */
  /*     x[i] -= 1; */
  
  /*   /\*if(proc_id == 0)*\/ */
  /*   res =  sqrt(dist_ddot(proc_id, x, x, 0, DBL.LHID.nblock-1, &DBL)); */
  /*   if(proc_id == 0) */
  /*     fprintfv(5, stderr, "Norm of x-sol = %g \n",res); */

  /**/ /**/ /**/ /**/ /**/

  /** DO NOT NEED THESE VECTORS ANYMORE **/
  PhidalDistrHID_Clean(&DBL);
  PhidalHID_Clean(&BL); 
  PhidalOptions_Clean(&phidaloptions);
  
  fflush(stdout);
  
  MPI_Barrier(MPI_COMM_WORLD);
  fprintfv(5, stdout, "END %d \n", proc_id);

  /** End MPI **/
  MPI_Finalize();

  return 0;
}

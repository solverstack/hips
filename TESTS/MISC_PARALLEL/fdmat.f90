! @release_exclude
      subroutine gen5pt (nx,ny,nz,a,ja,ia,iau)
      integer ja(*),ia(*),iau(*)
      real*8 a(*), h
!--------------------------------------------------------------------
! This subroutine computes the sparse matrix in compressed
! format for the elliptic operator
!
! L u = delx( a delx u ) + dely ( b dely u) + delz ( c delz u ) +
!       d delx ( u ) + e dely (u) + f delz( u ) + g u
!
! with Dirichlet Boundary conditions, on a rectangular 1-D,
! 2-D or 3-D grid using centered difference schemes.
!
! The functions a, b, ..., g are known through the
! subroutines  afun, bfun, ..., gfun.
! note that to obtain the correct matrix, any function that is not
! needed should be set to zero. For example for two-dimensional
! problems, nz should be set to 1 and the functions cfun and ffun
! should be zero functions.
!
! uses natural ordering, first x direction, then y, then z
! mesh size h is uniform and determined by grid points
! in the x-direction.
!--------------------------------------------------------------------
! parameters:
!
! nx      = number of points in x direction
! ny        = number of points in y direction
! nz        = number of points in z direction
!
! a, ja, ia =  resulting matrix in row-sparse format
!
! iau     = integer*n containing the poisition of the diagonal element
!           in the a, ja, ia structure
!
! stencil =  work array of size 7, used to store local stencils.
!
!--------------------------------------------------------------------
!
!     stencil [1:7] has the following meaning:
!
!     center point = stencil(1)
!     west point = stencil(2)
!     east point = stencil(3)
!     south point = stencil(4)
!     north point = stencil(5)
!     front point = stencil(6)
!     back point = stencil(7)
!
!
!                           st(5)
!                            |
!                            |
!                            |
!                            |          .st(7)
!                            |     .
!                            | .
!         st(2) ----------- st(1) ---------- st(3)
!                       .    |
!                   .        |
!               .            |
!            st(6)           |
!                            |
!                            |
!                           st(4)
!
!-------------------------------------------------------------------
      real*8 stencil(7)
!
      h = 1.0/real(nx+1)
      kx = 1
      ky = nx
      kz = nx*ny
      iedge = 1
      node = 1
      do 100 iz = 1,nz
         do 90 iy = 1,ny
            do 80 ix = 1,nx
               ia(node) = iedge
               call getsten(nx,ny,nz,ix,iy,iz,stencil,h)
!     west
               if (ix.gt.1) then
                  ja(iedge)=node-kx
              a(iedge) = stencil(2)
                  iedge=iedge + 1
               end if
!     south
               if (iy.gt.1) then
                  ja(iedge)=node-ky
              a(iedge) = stencil(4)
                  iedge=iedge + 1
               end if
!     front plane
               if (iz.gt.1) then
                  ja(iedge)=node-kz
              a(iedge) = stencil(6)
                  iedge=iedge + 1
               endif
!     center node
               ja(iedge) = node
               iau(node) = iedge
               a(iedge) = stencil(1)
               iedge = iedge + 1
!     -- upper part
!     east
               if (ix.lt.nx) then
                  ja(iedge)=node+kx
              a(iedge) = stencil(3)
                  iedge=iedge + 1
               end if
!     north
               if (iy.lt.ny) then
                  ja(iedge)=node+ky
              a(iedge) = stencil(5)
                  iedge=iedge + 1
               end if
!     back plane
               if (iz.lt.nz) then
                  ja(iedge)=node+kz
                  a(iedge) = stencil(7)
                  iedge=iedge + 1
               end if
!------next node -------------------------
               node=node+1
 80         continue
 90      continue
 100  continue
      ia(node)=iedge
      return
!----------end of gen5pt-----------------------------------------------
!-----------------------------------------------------------------------
      end
!-----------------------------------------------------------------------
      subroutine gen5loc (nx,ny,nz,nloc,riord,a,ja,ia,stencil)
      integer nx, ny, nz, nloc, ja(*),ia(*),riord(*)
      real*8 a(*), stencil(*), h
!--------------------------------------------------------------------
! Local version of gen5pt. This subroutine generates only the nloc
! rows of the matrix that are specified by the integer array riord
! rows  riord(1), ..., riord(nloc) of the matrix will be generated
! and put in the a, ja, ia  data structure. The column indices in
! ja will still be given in the original labeling.
!-----------------------------------------------------------------------
! see documentation for gen5pt for mode details.
! on input
! ------
! same arguments as for gen5pt. Plus:
!
! nloc   = number of rows to be generated
! riord  = integer array of length nloc. the code will generate rows
!          riord(1), ..., riord(nloc) of the matrix.
!
! on return
!----------
! a, ja, ia =  resulting matrix in CSR format.
!              can be  viewed as a rectangular matrix of size
!              nloc x n, containing the rows riord(1),riord(2), ...
!              riord(nloc) of A (in this order)  in CSR format.
!-------------------------------------------------------------------
      h = 1.0/real(nx+1)
      kx = 1
      ky = nx
      kz = nx*ny
      iedge = 1
      node = 1
      next = 1
      ia(next) = iedge
      do 100 iz = 1,nz
         do 90 iy = 1,ny
            do 80 ix = 1,nx
               if (node .eq. riord(next)) then
                  call getsten(nx,ny,nz,ix,iy,iz,stencil,h)
!     west
                  if (ix.gt.1) then
                     ja(iedge)=node-kx
                     a(iedge) = stencil(2)
                     iedge=iedge + 1
                  end if
!     south
                  if (iy.gt.1) then
                     ja(iedge)=node-ky
                     a(iedge) = stencil(4)
                     iedge=iedge + 1
                  end if
!     front plane
                  if (iz.gt.1) then
                     ja(iedge)=node-kz
                     a(iedge) = stencil(6)
                     iedge=iedge + 1
                  endif
!     center node
                  ja(iedge) = node
                  a(iedge) = stencil(1)
                  iedge = iedge + 1
!     -- upper part
!     east
                  if (ix.lt.nx) then
                     ja(iedge)=node+kx
                     a(iedge) = stencil(3)
                     iedge=iedge + 1
                  end if
!     north
                  if (iy.lt.ny) then
                     ja(iedge)=node+ky
                     a(iedge) = stencil(5)
                     iedge=iedge + 1
                  end if
!     back plane
                  if (iz.lt.nz) then
                     ja(iedge)=node+kz
                     a(iedge) = stencil(7)
                     iedge=iedge + 1
                  end if
!------next node -------------------------
                  next = next+1
                  ia(next) = iedge
               endif
               node = node + 1
               if (next .gt. nloc) return
 80         continue
 90      continue
 100  continue
      return
!----------end of gen5loc-----------------------------------------------
!-----------------------------------------------------------------------
      end
!-----------------------------------------------------------------------
       subroutine getsten (nx,ny,nz,kx,ky,kz,stencil,h)
!-----------------------------------------------------------------------
!     This subroutine calcultes the correct stencil values for
!     centered difference discretization of the elliptic operator
!
! L u = delx( a delx u ) + dely ( b dely u) + delz ( c delz u ) +
!      delx ( d u ) + dely (e u) + delz( f u ) + g u
!
!   For 2-D problems the discretization formula that is used is:
!
! h**2 * Lu == a(i+1/2,j)*{u(i+1,j) - u(i,j)} +
!             a(i-1/2,j)*{u(i-1,j) - u(i,j)} +
!              b(i,j+1/2)*{u(i,j+1) - u(i,j)} +
!              b(i,j-1/2)*{u(i,j-1) - u(i,j)} +
!              (h/2)*d(i,j)*{u(i+1,j) - u(i-1,j)} +
!              (h/2)*e(i,j)*{u(i,j+1) - u(i,j-1)} +
!              (h/2)*e(i,j)*{u(i,j+1) - u(i,j-1)} +
!               (h**2)*g(i,j)*u(i,j)
!-----------------------------------------------------------------------
      real*8 stencil(*), h, hhalf,cntr, afun, bfun, cfun, dfun, &
            efun, ffun, gfun, x, y, z, coeff
!------------
      do 200 k=1,7
         stencil(k) = 0.0
 200  continue
!------------
      hhalf = h*0.5
      x = h*real(kx)
      y = h*real(ky)
      z = h*real(kz)
      cntr = 0.0
!     differentiation wrt x:
      coeff = afun(x+hhalf,y,z)
      stencil(3) = stencil(3) + coeff
      cntr = cntr + coeff
!
      coeff = afun(x-hhalf,y,z)
      stencil(2) = stencil(2) + coeff
      cntr = cntr + coeff
!
      coeff = dfun(x,y,z)*hhalf
      stencil(3) = stencil(3) + coeff
      stencil(2) = stencil(2) - coeff
      if (ny .le. 1) goto 99
!
!     differentiation wrt y:
!
      coeff = bfun(x,y+hhalf,z)
      stencil(5) = stencil(5) + coeff
      cntr = cntr + coeff
!
      coeff = bfun(x,y-hhalf,z)
      stencil(4) = stencil(4) + coeff
      cntr = cntr + coeff
!
      coeff = efun(x,y,z)*hhalf
      stencil(5) = stencil(5) + coeff
      stencil(4) = stencil(4) - coeff
      if (nz .le. 1) goto 99
!
! differentiation wrt z:
!
      coeff = cfun(x,y,z+hhalf)
      stencil(7) = stencil(7) + coeff
      cntr = cntr + coeff
!
      coeff = cfun(x,y,z-hhalf)
      stencil(6) = stencil(6) + coeff
      cntr = cntr + coeff
!
      coeff = ffun(x,y,z)*hhalf
      stencil(7) = stencil(7) + coeff
      stencil(6) = stencil(6) - coeff
!
! discretization of  product by g:
!
 99   coeff = gfun(x,y,z)
      stencil(1) = h*h*coeff - cntr
!
      return
!------end-of-getsten---------------------------------------------------
!-----------------------------------------------------------------------
      end
!-----------------------------------------------------------------------
      subroutine part0 (nx,ny,mpx,mpy,ovlp,lst,lstptr,iout)
      integer  nx,ny,mpx,mpy,ovlp,iout,lst(*),lstptr(*)
!-----------------------------------------------------------------------
!     does a trivial mapping of a square grid into a virtual mpx x mpy
!     processor grid. One way overlapping allowed:
!     when assigning a block as a subdomain then `ovlp'
!     extra lines to the right and `ovlp' extra lines to the
!     top of the subrectangle being consired are added.
!-----------------------------------------------------------------------
! on entry:
!---------
! nx, ny   = number of mesh points in x and y directions respectively
! mpx, mpy = number of processors in x and y directions respectively
!            with (mpx .le. nx)  and (mpy .le. ny)
! ovlp     = a nonnegative integer determing the amount of overlap
!            (number of lines) in each direction. One=way overlap
!            see above explanation.
! on return:
! ---------
! lst      = node per processor list. The nodes are listed contiguously
!            from proc 1 to nproc = mpx*mpy.
! lstptr   = pointer array for array lst. list for proc. i starts at
!            lstptr(i) and ends at lstptr(i+1)-1 in array lst.
!-----------------------------------------------------------------------
! iout     = not used.
!-----------------------------------------------------------------------
      nproc = mpx*mpy
      mx = nx/mpx
      my = ny/mpy
      nod = 0
      ipr = 1
      ko = 1
      lstptr(ipr) = 1
!-----------------------------------------------------------------------
      do 1 jj = 1, mpy
         j = (jj-1)*my
         do 2 ii = 1, mpx
            i = (ii-1)*mx
!           write (iout,*) ' *** * proc = ',ipr, ' ****** '
            mymax = min(my+ovlp,ny-j)
            mxmax = min(mx+ovlp,nx-i)
            do ky = 0, mymax-1
               do kx = 0, mxmax-1
                  nod = (j+ky)*nx + i + kx + 1
                  lst(ko) = nod
!                  write (iout,*) ' ko ', ko, ' nod ', nod
                  ko = ko+1
               enddo
            enddo
            ipr = ipr+1
            lstptr(ipr) = ko
 2       continue
 1    continue
!-----------------------------------------------------------------------
      return
      end
!-----------------------------------------------------------------------
      subroutine part1 (nx,ny,mpx,mpy,ovlp,lst,lstptr,iout)
      integer  nx,ny,mpx,mpy,ovlp,iout,lst(*),lstptr(*)
!-----------------------------------------------------------------------
!
!     does a trivial map of a square grid into a virtual mpx x mpy
!     processor grid. Overlapping allowed: when assigning a block to a
!     subdomain then `ovlp' extra lines  are added to each of the four
!     sides of the subrectangle being considered, outward.
!
!-----------------------------------------------------------------------
! on entry:
!---------
! nx, ny   = number of mesh points in x and y directions respectively
! mpx, mpy = number of processors in x and y directions respectively
!            with (mpx .le. nx)  and (mpy .le. ny)
! ovlp     = a nonnegative integer determing the amount of overlap
!            (number of lines) in each direction.
!
! on return:
! ---------
! lst      = node per processor list. The nodes are listed contiguously
!            from proc 1 to nproc = mpx*mpy.
! lstptr   = pointer array for array lst. list for proc. i starts at
!            lstptr(i) and ends at lstptr(i+1)-1 in array lst.
!-----------------------------------------------------------------------
! iout     = not used.
!-----------------------------------------------------------------------
      nproc = mpx*mpy
      mx = nx/mpx
      my = ny/mpy
      nod = 0
      ipr = 1
      ko = 1
      lstptr(ipr) = 1
!-----------------------------------------------------------------------
      do 1 jj = 1, mpy
         j = (jj-1)*my
         do 2 ii = 1, mpx
            i = (ii-1)*mx
!           write (iout,*) ' *** * proc = ',ipr, ' ****** '
            mymax = min(my+ovlp,ny-j)
            mxmax = min(mx+ovlp,nx-i)
            mymin = - ovlp
            if (mymin + j .lt. 0) mymin = 0
            mxmin = -ovlp
            if (mxmin + i .lt. 0) mxmin = 0
!-----------------------------------------------------------------------
            do ky = mymin, mymax-1
               do kx = mxmin, mxmax-1
                  nod = (j+ky)*nx + i + kx + 1
                  lst(ko) = nod
!                  write (iout,*) ' ko ', ko, ' nod ', nod
                  ko = ko+1
               enddo
            enddo
            ipr = ipr+1
            lstptr(ipr) = ko
 2       continue
 1    continue
!-----------------------------------------------------------------------
      return
!-----------------------------------------------------------------------
      end

      subroutine part2 (nx,ny,nz,mpx,mpy,mpz,ovlp,lst,lstptr,iout)
      integer  nx,ny,nz,mpx,mpy,mpz,ovlp,iout,lst(*),lstptr(*)
!-----------------------------------------------------------------------
!
!     does a trivial map of a square grid into a virtual mpx x mpy
!     processor grid. Overlapping allowed: when assigning a block to a
!     subdomain then `ovlp' extra lines  are added to each of the four
!     sides of the subrectangle being considered, outward.
!
!-----------------------------------------------------------------------
! on entry:
!---------
! nx, ny,nz   = number of mesh points in x, y and z directions respectively
! mpx,mpy,mpz = number of processors in x, y and z directions respectively
!            with (mpx .le. nx), (mpy .le. ny) and (mpz .le. nz)
! ovlp     = a nonnegative integer determing the amount of overlap
!            (number of lines) in each direction.
!
! on return:
! ---------
! lst      = node per processor list. The nodes are listed contiguously
!            from proc 1 to nproc = mpx*mpy.
! lstptr   = pointer array for array lst. list for proc. i starts at
!            lstptr(i) and ends at lstptr(i+1)-1 in array lst.
!-----------------------------------------------------------------------
! iout     = not used.
!-----------------------------------------------------------------------
      nproc = mpx*mpy*mpz
      mx = nx/mpx
      my = ny/mpy
      mz = nz/mpz
      nod = 0
      ipr = 1
      ko = 1
      lstptr(ipr) = 1
!-----------------------------------------------------------------------
      do kk = 1, mpz
         k = (kk-1)*mz
         do 1 jj = 1, mpy
            j = (jj-1)*my
            do 2 ii = 1, mpx
               i = (ii-1)*mx
!           write (iout,*) ' *** * proc = ',ipr, ' ****** '
               mzmax = min(mz+ovlp,nz-k)
               mymax = min(my+ovlp,ny-j)
               mxmax = min(mx+ovlp,nx-i)
               mzmin = - ovlp
               if (mzmin + k .lt. 0) mzmin = 0
               mymin = - ovlp
               if (mymin + j .lt. 0) mymin = 0
               mxmin = -ovlp
               if (mxmin + i .lt. 0) mxmin = 0
!-----------------------------------------------------------------------
               do kz = mzmin, mzmax-1
                  do ky = mymin, mymax-1
                     do kx = mxmin, mxmax-1
                        nod = (k+kz)*nx*ny+ (j+ky)*nx + i + kx + 1
                        lst(ko) = nod
!     write (iout,*) ' ko ', ko, ' nod ', nod
                        ko = ko+1
                     enddo
                  enddo
               end do
               ipr = ipr+1
               lstptr(ipr) = ko
 2          continue
 1       continue
      end do
!-----------------------------------------------------------------------
      return
!-----------------------------------------------------------------------
      end

      subroutine partedge (nx,ny,nz,mpx,mpy,mpz,ovlp,lst,lstptr,iout)
      integer  nx,ny,nz,mpx,mpy,mpz,ovlp,iout,lst(*),lstptr(*)
!-----------------------------------------------------------------------
!
!     does a trivial edge based map of a square grid into a virtual mpx x mpy
!     processor grid. Overlapping allowed: when assigning a block to a
!     subdomain then `ovlp' extra lines  are added to each of the four
!     sides of the subrectangle being considered, outward.
!
!-----------------------------------------------------------------------
! on entry:
!---------
! nx, ny,nz   = number of mesh points in x, y and z directions respectively
! mpx,mpy,mpz = number of processors in x, y and z directions respectively
!            with (mpx .le. nx), (mpy .le. ny) and (mpz .le. nz)
! ovlp     = a nonnegative integer determing the amount of overlap
!            (number of lines) in each direction.
!
! on return:
! ---------
! lst      = node per processor list. The nodes are listed contiguously
!            from proc 1 to nproc = mpx*mpy.
! lstptr   = pointer array for array lst. list for proc. i starts at
!            lstptr(i) and ends at lstptr(i+1)-1 in array lst.
!-----------------------------------------------------------------------
! iout     = not used.
!-----------------------------------------------------------------------
! Coded by: Dr. Yousef Saad.
! Modifications: Dev Kulkarni
! Last Date of Modification: July 31, 2002
! Modification: Modified the part2 function into partedge function
! which will give the edge based partitioning
!-----------------------------------------------------------------------


      nproc = mpx*mpy*mpz
      mx = nx/mpx
      my = ny/mpy
      mz = nz/mpz
      nod = 0
      ipr = 1
      ko = 1
      lstptr(ipr) = 1
!-----------------------------------------------------------------------
      do kk = 1, mpz
         k = (kk-1)*mz
         do 1 jj = 1, mpy
            j = (jj-1)*my
            do 2 ii = 1, mpx
               i = (ii-1)*mx
!           write (iout,*) ' *** * proc = ',ipr, ' ****** '
! To get the edge based partition it is necessary to start
! the min of mzmax, mymax and mxmax at a location
! min(mz+ovlp+1,nz-k)
! Done by Dev -- July 31, 2002
               mzmax = min(mz+ovlp+1,nz-k)
               mymax = min(my+ovlp+1,ny-j)
               mxmax = min(mx+ovlp+1,nx-i)
               mzmin = - ovlp
               if (mzmin + k .lt. 0) mzmin = 0
               mymin = - ovlp
               if (mymin + j .lt. 0) mymin = 0
               mxmin = -ovlp
               if (mxmin + i .lt. 0) mxmin = 0
!-----------------------------------------------------------------------
               do kz = mzmin, mzmax-1
                  do ky = mymin, mymax-1
                     do kx = mxmin, mxmax-1
                        nod = (k+kz)*nx*ny+ (j+ky)*nx + i + kx + 1
                        lst(ko) = nod
!     write (iout,*) ' ko ', ko, ' nod ', nod
                        ko = ko+1
                     enddo
                  enddo
               end do
               ipr = ipr+1
               lstptr(ipr) = ko
 2          continue
 1       continue
      end do
!-----------------------------------------------------------------------
      return
!-----------------------------------------------------------------------
      end
!-----------------------------------------------------------------------

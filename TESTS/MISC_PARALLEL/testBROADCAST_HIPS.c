/* @authors J. GAIDAMOUR, P. HENON */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <mpi.h>
#include "math.h"

#include "base.h"
#include "io.h"
#include "block.h" /* FLOAT */
#include "db_struct.h"
#include "phidal_parallel.h"

#define BUFLEN 200

/* #define ONE_DOMAIN */

int main(int argc, char *argv[])
{
  /*unsym  --  symmetric pattern(0), nonsym pattern(1) */
  int unsym;

  /* working array for reading matrix */
  REAL res;
  chrono_t t1,t2;
  int numflag=-1;
  int *mapp,*mapptr,i; 
  int nproc, ndom;
  int domsize;
  int proc_id;
  COEF *x;
  COEF *b, *r;
  int ln; INTL *lia; dim_t *lja;
  COEF *la;

  PhidalDistrMatrix A;
  PhidalHID BL;
  PhidalDistrHID DBL;
  PhidalDistrPrec P;

  /* 
   * buf is for read from 'inputs' file, 
   * matrix represents matrix name
   */
  char matrix[BUFLEN];
  char sfile_path[BUFLEN];

  int *iperm;
  int rsa;

  PhidalOptions phidaloptions;
  PhidalOptions* option= &phidaloptions;

#ifndef READ_PARALLEL
  int n;
  COEF *a;
  INTL *ia, *ig;
  dim_t *ja, *jg;
  int *perm;
#else
  FILE *fp;
  char filename[BUFLEN];
#endif

  /** Init MPI environment **/
  MPI_Init(&argc, &argv);
  MPI_Comm_size(MPI_COMM_WORLD, &nproc);
  MPI_Comm_rank(MPI_COMM_WORLD, &proc_id);

#ifndef ONE_DOMAIN
  if(argc < 2)
    {
      fprintferr(stderr, "Illegal argument : testBROADCAST_HIPS.ex <domain size (in number of node)> \n");
      exit(-1);
    }
  domsize = atoi(argv[1]);
#endif

  PhidalOptions_Init(&phidaloptions);

  /** Read parameters from the input file **/
  GENERAL_setpar(NULL, matrix, sfile_path, &unsym, &rsa, &phidaloptions);   

  /************************************************************************************************************/
  /****************************************** READ THE MATRIX *************************************************/
  /************************************************************************************************************/
#include "matrix_read.c"

#ifndef READ_PARALLEL
  /*******************************************************************/
  /*******************************************************************/
  /**                                                               **/
  /** COMPUTE THE HIERARCHICAL DECOMPOSITION AND LOCAL DATA         **/
  /**                                                               **/
  /*******************************************************************/
  /*******************************************************************/
  
  /** Compute the global hierarchical interface decomposition and the associated permutation **/ 
  numflag = 0;
  PhidalHID_Init(&BL);
  
  t1  = dwalltime(); 
  PHIDAL_HierarchDecomp(phidaloptions.verbose, numflag, n, ig, jg, mapp, mapptr, ndom, &BL, perm, iperm);
  t2  = dwalltime(); 
  
  if(proc_id == 0)
    fprintfv(5, stdout, " Compute Phidal Hierarchical Decomposition in %g seconds \n\n", t2-t1);
  
  free(ig);
  free(jg);
  free(perm);
#endif

  /*fprintfv(5, stderr, "Proc %d Compute the local HID \n", proc_id);*/
  PhidalDistrHID_Setup(proc_id, nproc, 0, &BL, &DBL, iperm, MPI_COMM_WORLD);
  free(iperm);
  fprintfv(5, stderr, "Proc %d: Domain size = %d interior = %d \n", proc_id, DBL.LHID.n, DBL.LHID.block_index[DBL.LHID.block_levelindex[1]]);

  if(DBL.LHID.n == 0)
    {
      fprintfv(5, stderr, "ERROR too much processors \n");
      exit(-1);
    }

  ln = DBL.LHID.n;
#ifndef READ_PARALLEL
  /*** Build the local part of the csr matrix ****/
  /*fprintfv(5, stderr, "Compute the local submatrix ... ");*/
  /*ln = mapptr[proc_id+1]-mapptr[proc_id];
    CSR_GetSquareSubmatrix(ln, mapp + mapptr[proc_id], 
    n, ia, ja, a,
    &lia, &lja, &la);*/
  CSR_GetSquareSubmatrix(ln, DBL.loc2orig, 
			 n, ia, ja, a,
			 &lia, &lja, &la);
    
  /** DO NOT NEED THESE VECTORS ANYMORE **/
  free(a);
  free(ja);
  free(ia);
#endif

  /*** Build the PHIDAL matrix ******/
  
  fprintfv(5, stderr, "Proc %d Build the PHIDAL Distributed matrix  ... ", proc_id);
  /*PHIDAL_SetMatrixCoef(numflag, ln, lia, lja, la, mapp + mapptr[proc_id], rsa,  &A, &DBL);*/

  /*fprintfv(5, stderr, "\n \n RSA FORCE TO NULL \n \n");
    rsa = 0;*/

  assert(numflag != -1); /* debug ‘numflag’ may be used uninitialized in this function */
  PHIDAL_SetMatrixCoef(0, 0, numflag, ln, lia, lja, la, DBL.loc2orig, rsa, rsa,  &A, &DBL);
  fprintfv(5, stderr, "Proc %d done MATRIX BUILDING\n", proc_id);


  /** DO NOT NEED THESE VECTORS ANYMORE **/
  free(mapp);
  free(mapptr);
  free(la);
  free(lja);
  free(lia);
  /**/

  /*fprintfv(5, stderr, "Proc %d A norms = \n", proc_id);
    for(i=A.M.tli;i<=A.M.bri;i++)
    for(j=A.M.ria[i]; j<A.M.ria[i+1];j++)
    if(A.rlead[j] == proc_id)
    fprintfv(5, stderr, "block (%d %d) = %g nnz = %ld \n", A.M.rja[j], i, CSnormFrob(A.M.ra[j]), (long)CSnnz(A.M.ra[j]));*/


  
  /********* Construct b = A.x x = 1 ****/
  x = (COEF *)malloc(sizeof(COEF)*ln);
  for(i=0;i<ln;i++)
    x[i] = 1.0;
  
  b = (COEF *)malloc(sizeof(COEF)*ln);
  
  
  /*fprintfv(5, stderr, "Proc %d Setup the communicator for vectors \n", proc_id);*/
  PhidalCommVec_Setup(0, &A, &A.commvec, &DBL);


  /*** Compute the norms ****/
  /*PhidalDistrMatrix_RowNorm2(&A, &DBL, b);
    fprintfv(5, stderr, "Norm Frob of A = %g \n", dist_ddot(proc_id, b, b, 0, DBL.LHID.nblock-1, &DBL));*/

  /*fprintfv(5, stderr, "Proc %d Matvec \n", proc_id);*/
  PhidalDistrMatrix_MatVec(0, &A, &DBL, x, b);
  /*fprintfv(5, stderr, "Proc %d done \n", proc_id);*/

  res = sqrt(dist_ddot(proc_id, b, b, 0, DBL.LHID.nblock-1, &DBL));
  if(proc_id == 0)
    fprintfv(5, stderr, "Norm of b = %g \n", res);

  /*phidaloptions.krylov_method = 0;*/
  t1  = dwalltime(); 
  /*PHIDAL_DistrMLICCT(&A, &P, &DBL, &phidaloptions);*/
  PHIDAL_DistrPrecond(&A, &P, &DBL, &phidaloptions);
  t2  = dwalltime(); 
  if(proc_id == 0)
    fprintfv(5, stdout, "\n PRECOND IN %g \n", t2-t1);
  
  fprintfv(5, stdout, "Proc %d Fill Ratio of Preconditioner = %g \n\n", proc_id,
	   ((REAL)PhidalDistrPrec_NNZ_All(&P, DBL.mpicom))/((REAL)PhidalDistrMatrix_NNZ_All(&A,DBL.mpicom)));

  bzero(x, sizeof(COEF)*ln);
  t1  = dwalltime(); 
  PHIDAL_DistrSolve(&A, &P, &DBL, &phidaloptions, b, x, NULL, NULL);
  t2  = dwalltime(); 
  if(proc_id == 0)
    fprintfv(5, stdout, "\n PROC %d SOLVED IN %g \n",proc_id,  t2-t1);

  r = (COEF *)malloc(sizeof(COEF)*ln);
  memcpy(r, b, sizeof(COEF)*ln);
  PhidalDistrMatrix_MatVecSub(0, &A, &DBL, x, r);
  res = sqrt(dist_ddot(proc_id, r, r, A.M.tli, A.M.bri,&DBL))/sqrt(dist_ddot(proc_id, b, b, A.M.tli, A.M.bri, &DBL));
  if(proc_id == 0)
    fprintfv(5, stdout, "Relative residual norm = %g \n", res);

  free(r);
  PhidalCommVec_Clean(&A.commvec);
  PhidalDistrPrec_Clean(&P);
  PhidalDistrMatrix_Clean(&A);
  PhidalDistrHID_Clean(&DBL);
  PhidalHID_Clean(&BL); 

  free(b);
  free(x);

  PhidalOptions_Clean(&phidaloptions);
  /*MPI_Barrier(MPI_COMM_WORLD);*/
  if(proc_id == 0)
    fprintfv(5, stdout, "PROC %d END \n", proc_id);

  /** End MPI **/
  MPI_Finalize();

  return 0;
}

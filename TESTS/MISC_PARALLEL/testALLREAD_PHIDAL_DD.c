/* @authors J. GAIDAMOUR, P. HENON */
/* @release_exclude */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <mpi.h>
#include "math.h"

#include "phidal_parallel.h"

#define BUFLEN 200


#ifndef TYPE_REAL
#error Ne marche qu en DOUBLE
#endif

int main(int argc, char *argv[])
{
  /*
   * num    --  working scalar
   * iov    --  overlap
   * unsym  --  symmetric pattern(0), nonsym pattern(1)
   */
  int iov, unsym, ierr,len;

  /* declarations related to Harwell-boeing format for reading the HB
     matri. Second part is related to I/O parameters */
  char guesol[2], title[72], key[8], type[3];
  int nrhs, nc, n, nnz, tmp0,tmp,tmp2,tmp3,job;
  long nnzL;

  /* working array for reading matrix */
  REAL *a,*rhstmp,  res, dnnz_pre, dgprec_nnz;
  chrono_t t1,t2,t3,t4;
  int *ja, *ia,*jb,*ib, *jg, *ig, volume,metisoption[10],wgtflag,numflag;
  int nloc,*mapp,*mapptr, maxmp,
    *iwk,node,i1,i2,i,j,k; 
  csptr mat, L;
  int *node2dom;
  int nproc;
  int proc_id;
  REAL *x;
  REAL *b;
  int ln, *lia, *lja;
  REAL *la;
  PhidalDistrMatrix A;
  PhidalHID BL;
  PhidalDistrHID DBL;

  /* working array for symmetrizing matrix */
  REAL *mc;
  int *jc, *ic;

  FILE *fp;


  /* 
   * buf is for read from 'inputs' file, 
   * matrix represents matrix name
   */
  char matrix[BUFLEN];
  char sfile_path[BUFLEN];
  char *substr;

  int *riord;
  int *perm, *iperm;
  int rsa;

  PhidalOptions phidaloptions;
  PhidalOptions* option= &phidaloptions;
  int maxdomsize;


  /** Init MPI environment **/
  MPI_Init(&argc, &argv);
  MPI_Comm_size(MPI_COMM_WORLD, &nproc);
  MPI_Comm_rank(MPI_COMM_WORLD, &proc_id);



  PhidalOptions_Init(&phidaloptions);

  /** Read parameters from the input file **/
  GENERAL_setpar(NULL, matrix, sfile_path, &unsym, &rsa, &phidaloptions);   

  

  /************************************************************************************************************/
  /****************************************** READ THE MATRIX *************************************************/
  /************************************************************************************************************/
  /*-----------------------------------------------------------------/
    /   Processor 0 read the matrix, compute a partition etc..         /
    /-----------------------------------------------------------------*/
  /*if(proc_id == 0)*/
    {

      CSRread(matrix, &n, &nnz, &ia, &ja, &a, NULL, NULL);
      assert(phidaloptions.symmetric == rsa);

      /************************ RSA matrix contains only the lower triangular part of the matrix ***************************/
      if(rsa == 1)
	{
	  /** Symmetrize the matrix (in rsa we only have the lower triangular part) ****/
	  ib = ia;
	  jb = ja;
	  b = a;
	  numflag = 1;
	  job = 2;
	  PHIDAL_SymmetrizeMatrix(job, numflag, n, ib, jb, b, &ia, &ja, &a);
	  nnz = 2*nnz-n;
	  unsym = 0;
	  free(ib);
	  free(jb);
	  free(b);
	}

      /*******************************************************************************************************************/
      /*******************************************************************************************************************/
      /*******************************************************************************************************************/
      fprintfv(5, stdout,"Matrix dimension is %d, Number of nonzeros is %d\n",n,nnz);



      /*****************************************************************************************************************/
      /***********************************Construct the symmetric graph G of the matrix ********************************/
      /*****************************************************************************************************************/
      ig = (int *)malloc(sizeof(int)*(n+1));
      jg = (int *)malloc(sizeof(int)*nnz);
      memcpy(ig ,ia, sizeof(int)*(n+1));
      memcpy(jg ,ja, sizeof(int)*nnz);

  
      if(unsym == 1)
	{
      
	  ib = ig;
	  jb = jg;
	  numflag = 1;
	  job = 0;
	  PHIDAL_SymmetrizeMatrix(job, numflag, n, ib, jb, NULL, &ig, &jg, NULL);
	  free(ib);
	  free(jb);
	}
      /*****************************************************************************************************************/
      /*****************************************************************************************************************/


      /* Translate matrix into C numbering */
      /*fprintfv(5, stderr, "Convert FORTRAN to C indexing \n");*/
      CSR_Fnum2Cnum(ja, ia, n);
      CSR_Fnum2Cnum(jg, ig, n);
      numflag = 0;
  

      perm = (int *)malloc(sizeof(int)*n);
      iperm = (int *)malloc(sizeof(int)*n);

      /********************************************************/
      /* Compute a  vertex-based partition  using METIS       */
      /* then compute an edge-based partition wich ovelapps   */
      /* on the vertex separator                              */
      /********************************************************/
      node2dom = perm; /** use perm as a temporary working area **/


      if(nproc > 1) 
	{
	  wgtflag = 0;
	  metisoption[0] = 0;
	  METIS_PartGraphVKway(&n, ig, jg, NULL, NULL, &wgtflag, &numflag, &nproc, metisoption, &volume, node2dom);
	}
      else 
	if(nproc == 1) 
	  for(i = 0; i < n; i++) 
	    node2dom[i] = 0;
  
      /** Transform the vertex-based partition into an edge-based partition **/
      PHIDAL_Partition2OverlappedPartition(0, nproc, n, ig, jg, node2dom, &mapp, &mapptr);
  

      {
	/*********************************************************/
	/*    DISPLAY SOME INFO ON THE OVERLAPPED PARTITION      */
	/*********************************************************/         
    
	int maxdom, mindom;
	REAL avgdom;


	/* fprintfv(5, stdout, "PARTITION: \n");
	   for(i=0;i<ndom;i++)
	   fprintfv(5, stdout, "Domain %d = %d nodes \n", i, mapptr[i+1]-mapptr[i]);*/
    
	maxdom = 0;
	mindom = mapptr[1]-mapptr[0];
	avgdom = 0.0;
	for(i=0;i<nproc;i++)
	  {
	    avgdom += mapptr[i+1]-mapptr[i];
	    if(mapptr[i+1]-mapptr[i] > maxdom)
	      maxdom = mapptr[i+1]-mapptr[i];
	    if(mapptr[i+1]-mapptr[i] < mindom)
	      mindom = mapptr[i+1]-mapptr[i];
	  }
	avgdom /= nproc;

	fprintfv(5, stdout, "MAX DOMAIN = %d \n", maxdom);
	fprintfv(5, stdout, "MIN DOMAIN = %d \n", mindom);
	fprintfv(5, stdout, "AVG DOMAIN = %g \n", avgdom);
	/*fprintfv(5, stdout, "IMBALANCE = %g \n", ((float)(maxdom-mindom))*100.0/mindom );*/
      }
    

    }

  



 
  /*******************************************************************/
  /*******************************************************************/
  /**                                                               **/
  /** COMPUTE THE HIERARCHICAL DECOMPOSITION AND LOCAL DATA         **/
  /**                                                               **/
  /*******************************************************************/
  /*******************************************************************/
  
  /** Compute the global hierarchical interface decomposition and the associated permutation **/ 
  numflag = 0;
  PhidalHID_Init(&BL);
  
  t1  = dwalltime(); 
  PHIDAL_HierarchDecomp(phidaloptions.verbose, numflag, n, ig, jg, mapp, mapptr, nproc, &BL, perm, iperm);
  t2  = dwalltime(); 
  
  fprintfv(5, stdout, " Compute Phidal Hierarchical Decomposition in %g seconds \n\n", t2-t1);
  
  free(ig);
  free(jg);

  fprintfv(5, stderr, "Compute the local HID ... ");
  PhidalDistrHID_Setup(proc_id, nproc, 0, &BL, &DBL);
  fprintfv(5, stderr, "done \n");
  fprintfv(5, stderr, "Proc %d: Domain size = %d interior = %d \n", proc_id, DBL.LHID.n, DBL.LHID.block_index[DBL.LHID.block_levelindex[1]]);

#define TOTO
#ifdef TOTO
  {
    int *tmp;
    int count;
    tmp = (int *)malloc(sizeof(int)*n);
    bzero(tmp, sizeof(int)*n);

    fprintfv(5, stdout, "PROC %d NODE DOMAIN IN HID \n", proc_id );
    for(i=0;i<DBL.LHID.n;i++)
      tmp[DBL.loc2orig[i]] = 1;

    /*for(i=0;i<DBL.LHID.n;i++)
      fprintfv(5, stdout ,"%d ", DBL.loc2orig[i]);
      fprintfv(5, stdout, "\n");*/
   
    count = 0;
    for(i=mapptr[proc_id];i<mapptr[proc_id+1];i++)
      if(tmp[mapp[i]] != 1)
	{
	  count++;
	  /*fprintfv(5, stderr, "Proc %d error node %d is not in mapp \n",
	    proc_id, mapp[i]);*/
	}

    fprintfv(5, stderr, "Proc %d LHID.n %d mapptr = %d || NOT IN MAPP = %d \n", proc_id, DBL.LHID.n, mapptr[proc_id+1]-mapptr[proc_id], count); 

    /*for(i=0;i<nproc;i++)
      {
      fprintfv(5, stdout, "Node In dom %d = %d nodes \n", i, mapptr[i+1]-mapptr[i]);
      for(j=mapptr[i];j<mapptr[i+1];j++)
      fprintfv(5, stdout,"%d ", mapp[j]);
      fprintfv(5, stdout, "\n");
      }*/
  
    free(tmp);
    fprintfv(5, stderr, "Proc %d BARRIER \n" ,proc_id);
    MPI_Barrier(MPI_COMM_WORLD);
    MPI_Finalize();
    exit(0);
  }
#endif
  


  /*** Build the local part of the csr matrix ****/
  fprintfv(5, stderr, "Compute the local submatrix ... ");
  /*ln = mapptr[proc_id+1]-mapptr[proc_id];
  CSR_GetSquareSubmatrix(ln, mapp + mapptr[proc_id], 
			 n, ia, ja, a,
			 &lia, &lja, &la);*/
  ln = DBL.LHID.n;
  CSR_GetSquareSubmatrix(ln, DBL.loc2orig, 
			 n, ia, ja, a,
			 &lia, &lja, &la);
    
  fprintfv(5, stderr, "done \n");

  /** DO NOT NEED THESE VECTORS ANYMORE **/
  free(a);
  free(ja);
  free(ia);

  /*** Build the PHIDAL matrix ******/
  fprintfv(5, stderr, "Build the PHIDAL Distributed matrix  ... ");
 
  /*PHIDAL_SetMatrixCoef(numflag, ln, lia, lja, la, mapp + mapptr[proc_id], rsa,  &A, &DBL);*/
  /*PHIDAL_SetMatrixCoef(numflag, ln, lia, lja, la, DBL.loc2orig, rsa,  &A, &DBL);*/
  fprintfv(5, stderr, "done \n");

  /**/

  




  /**/


  /** DO NOT NEED THESE VECTORS ANYMORE **/
  free(mapp);
  free(mapptr);
  free(la);
  free(lja);
  free(lia);
  



  PhidalDistrHID_Clean(&DBL);
  PhidalHID_Clean(&BL); 
  free(perm);
  free(iperm);
  PhidalOptions_Clean(&phidaloptions);
  
  
  fprintfv(5, stdout, "END \n");

  /** End MPI **/
  MPI_Finalize();

}




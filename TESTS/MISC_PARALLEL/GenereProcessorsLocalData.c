/* @authors J. GAIDAMOUR, P. HENON */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "math.h"

#include "base.h"
#include "io.h"
#include "localdefs.h"
#include "phidal_sequential.h"
#include "phidal_parallel.h"

/*#ifdef SCOTCH_PART
  #define restrict
  #include "scotch.h"
  #else
  #include "metis.h"
  #endif*/

#define BUFLEN 300

int main(int argc, char *argv[])
{
  /*
   * num    --  working scalar
   * unsym  --  symmetric pattern(0), nonsym pattern(1)
   */
  int unsym;
  /* int load_local_files; */  /** If set to 1 then genere all the local data file **/

  /* declarations related to Harwell-boeing format for reading the HB
     matri. Second part is related to I/O parameters */
  int n, job;
  INTL nnz;

  /* working array for reading matrix */
  COEF *a, *b;
  chrono_t t1,t2;
  INTL *ia, *ib, *ig;
  dim_t *ja, *jb, *jg;
  int numflag;
  int *mapp,*mapptr, i; 

  /** Used to call METIS **/
  /*   int volume; */

  int nproc, ndom;
  int domsize;
  int *node2dom;
  /*   REAL *x; */
  /*   PhidalMatrix *m; */
  PhidalHID BL;
  PhidalDistrHID DBL;
  FILE *fp;

  int ln;
  INTL *lia; dim_t *lja;
  COEF *la;

  /* 
   * buf is for read from 'inputs' file, 
   * matrix represents matrix name
   */
  char matrix[BUFLEN];
  char sfile_path[BUFLEN];
  char filename[BUFLEN];

  int *perm, *iperm;
  int rsa;

  PhidalOptions phidaloptions;
  /* PhidalOptions *option= &phidaloptions; */

  if(argc < 3)
    {
      fprintferr(stderr, "ERROR: GenereProcessorsLocalData.ex <domsize> <number of processors> \n");
      exit(-1);
    }
  
  domsize = atoi(argv[1]);
  nproc = atoi(argv[2]);

  PhidalOptions_Init(&phidaloptions);

  /** Read parameters from the input file **/
  /*if(argc == 0)*/
  /** Default: read from file "input" **/

  GENERAL_setpar(NULL, matrix, sfile_path, &unsym, &rsa, &phidaloptions);   
  /*else
    GENERAL_setpar(argv[1], matrix, sfile_path, &unsym, &rsa,  &load_local_files, &phidaloptions);   */
  

  /************************************************************************************************************/
  /****************************************** READ THE MATRIX *************************************************/
  /************************************************************************************************************/

  CSRread(matrix, &n, &nnz, &ia, &ja, &a, NULL);
  assert(phidaloptions.symmetric == rsa);

  /********************** SUPPRESS THE ZERO IN THE CSR MATRIX *****/
  /*numflag = 1;
    i = nnz;
    nnz = CSR_SuppressZeros(numflag, n, ia, ja, a);
    fprintfd(stdout, "Zeros removed from the matrix = %d \n", i-nnz); 
 
    fprintfd(stdout, "NNZ = %ld \n", (long) nnz);*/

  /************************ RSA matrix contains only the lower triangular part of the matrix ***************************/
  if(rsa == 1)
    {
      /** Symmetrize the matrix (in rsa we only have the lower triangular part) ****/
      ib = ia;
      jb = ja;
      b = a;
      numflag = 1;
      job = 2;
      PHIDAL_SymmetrizeMatrix(job, numflag, n, ib, jb, b, &ia, &ja, &a);
      nnz = 2*nnz-n;
      unsym = 0;
      free(ib);
      free(jb);
      free(b);
      fprintfd(stdout, "This matrix is in RSA format \n");
    }

  /*******************************************************************************************************************/
  /*******************************************************************************************************************/
  /*******************************************************************************************************************/
  fprintfd(stdout,"Matrix dimension is %d, Number of nonzeros is %ld\n",n,(long)nnz);



  /*****************************************************************************************************************/
  /***********************************Construct the symmetric graph G of the matrix ********************************/
  /*****************************************************************************************************************/
  ig = (INTL *)malloc(sizeof(INTL)*(n+1));
  jg = (int *)malloc(sizeof(int)*nnz);
  memcpy(ig ,ia, sizeof(INTL)*(n+1));
  memcpy(jg ,ja, sizeof(int)*nnz);

  
  if(unsym == 1)
    {
      ib = ig;
      jb = jg;
      numflag = 1;
      job = 0;
      PHIDAL_SymmetrizeMatrix(job, numflag, n, ib, jb, NULL, &ig, &jg, NULL);
      free(ib);
      free(jb);
    }
  /*****************************************************************************************************************/
  /*****************************************************************************************************************/


  /* Translate matrix into C numbering */
  /*fprintfd(stderr, "Convert FORTRAN to C indexing \n");*/
  CSR_Fnum2Cnum(ja, ia, n);
  CSR_Fnum2Cnum(jg, ig, n);
  numflag = 0;
  

  perm = (int *)malloc(sizeof(int)*n);
  iperm = (int *)malloc(sizeof(int)*n);

  /********************************************************/
  /* Compute a  vertex-based partition  using METIS       */
  /* then compute an edge-based partition wich ovelapps   */
  /* on the vertex separator                              */
  /********************************************************/
  node2dom = perm; /** use perm as a temporary working area **/

  /** Delete the self edge in the graph (METIS_NodeND need that)**/
  /** OIMBE Put THAT IN Perm2SizedDomain ?? **/
  PHIDAL_CsrDelDiag(numflag, n, ig, jg);
  
  /**** Compute the overlapped partition from the matrix reordering   *****/
  t1  = dwalltime(); 
  PHIDAL_Perm2SizedDomains(domsize, n, ig, jg, &ndom, &mapptr, &mapp, perm, iperm);
  t2  = dwalltime(); 
  fprintfd(stdout, "Compute the grid of subdomain in %g \n", t2-t1);
  fprintfd(stdout, "DOMSIZE = %d \n", domsize);
  fprintfd(stdout, "Found %d domains \n", ndom);

  /* 1 DOMAIN PER PROCESSOR
     if(nproc > 1) 
     {
     int wgtflag;
     int option[8];
  
     wgtflag = 0;
     option[0] = 0;
     METIS_PartGraphVKway(&n, ig, jg, NULL, NULL, &wgtflag, &numflag, &nproc, option, &volume, node2dom);
     }
     else {
     assert(nproc == 1);
     memset(node2dom, 0, sizeof(int)*n);
     }
      
     PHIDAL_Partition2OverlappedPartition(0, nproc, n, ig, jg, node2dom, &mapp, &mapptr);
  */  
  {
    /*********************************************************/
    /*    DISPLAY SOME INFO ON THE OVERLAPPED PARTITION      */
    /*********************************************************/         
    
    int maxdom, mindom;
    REAL avgdom;


    /* fprintfd(stdout, "PARTITION: \n");
       for(i=0;i<ndom;i++)
       fprintfd(stdout, "Domain %d = %d nodes \n", i, mapptr[i+1]-mapptr[i]);*/
    
    maxdom = 0;
    mindom = mapptr[1]-mapptr[0];
    avgdom = 0.0;
    for(i=0;i<ndom;i++)
      {
	avgdom += mapptr[i+1]-mapptr[i];
	if(mapptr[i+1]-mapptr[i] > maxdom)
	  maxdom = mapptr[i+1]-mapptr[i];
	if(mapptr[i+1]-mapptr[i] < mindom)
	  mindom = mapptr[i+1]-mapptr[i];
      }
    avgdom /= ndom;
    
    fprintfd(stdout, "MAX DOMAIN = %d \n", maxdom);
    fprintfd(stdout, "MIN DOMAIN = %d \n", mindom);
    fprintfd(stdout, "AVG DOMAIN = %g \n", avgdom);
  }
    
  /*******************************************************************/
  /*******************************************************************/
  /**                                                               **/
  /** COMPUTE THE HIERARCHICAL DECOMPOSITION                        **/
  /**                                                               **/
  /*******************************************************************/
  /*******************************************************************/
  
  /** Compute the global hierarchical interface decomposition and the associated permutation **/ 
  numflag = 0;
  PhidalHID_Init(&BL);
  
  t1  = dwalltime(); 
  PHIDAL_HierarchDecomp(phidaloptions.verbose, numflag, n, ig, jg, mapp, mapptr, ndom, &BL, perm, iperm);
  t2  = dwalltime(); 
  fprintfd(stdout, " Compute Phidal Hierarchical Decomposition in %g seconds \n\n", t2-t1);

#define BLOCK
#ifdef BLOCK
#include "block.h"
 {
   SymbolMatrix* symbmtx;

   fprintfd(stdout, " Compute Phidal Hierarchical Decomposition in %g seconds \n\n", t2-t1);
   
   /****************************************/
   /*** Compute the global symbol matrix ***/
   /****************************************/
   fprintfd(stdout, "Build Symbolic Matrix \n");
   symbmtx = (SymbolMatrix*)malloc(sizeof(SymbolMatrix));
   
   if(phidaloptions.schur_method != 1)
     HIPS_SymbolMatrix(phidaloptions.verbose, numflag, 0.05, phidaloptions.locally_nbr, n,  ig,  jg, 
		       &BL, symbmtx, perm, iperm);
   else
     HIPS_SymbolMatrix(phidaloptions.verbose, numflag, 0.05, BL.nlevel, n,  ig,  jg, 
		       &BL, symbmtx, perm, iperm);
   free(ig);
   free(jg);
   
   free(perm);
   
   /*** Write the Global Phidal decomposition and ordering structure ***/
   sprintf(filename, "%s_%d.%d.Symbol", sfile_path, domsize, nproc);
   fp = fopen(filename, "w");
   if(fp == NULL)
     {
       fprintfd(stderr, "ERROR: Unable to create file %s \n", filename);
       exit(-1);
     }
   
   fprintfd(stderr, "Save Symbol structure in file %s ..." , filename);
   PHIDAL_WriteSymbolMatrix(fp, symbmtx);
   fprintfd(stderr, "done \n");
   fclose(fp);
   /***/

   SymbolMatrix_Clean(symbmtx);
   free(symbmtx);
 }

#else
  free(ig);
  free(jg);
  free(perm);
#endif /* BLOCK */
 
  /****************************************************************/
  /****************************************************************/
  /**                                                            **/
  /**   GENERATE THE LOCAL DATA FOR EACH PROCESSOR AND SAVE THE  **/
  /**   PHIDAL DECOMPOSITION                                     **/
  /**                                                            **/
  /****************************************************************/
  /****************************************************************/
  /*** Build the local part of the csr matrix for each processor and
       save it in a file <filename>.<procid>  ****/
  for(i=0;i<nproc;i++)
    {
      PhidalDistrHID_Setup(i, nproc, -1,  &BL, &DBL, iperm, MPI_COMM_WORLD);
      fprintfd(stderr, "Proc %d: Domain size = %d interior = %d \n", i, DBL.LHID.n, DBL.LHID.block_index[DBL.LHID.block_levelindex[1]]);
      
      if(DBL.LHID.n == 0)
	{
	  fprintfd(stderr, "ERROR too much processor \n");
	  exit(-1);
	}
      
      /*** Build the local part of the csr matrix ****/

      fprintfd(stderr, "Construct matrix of domain %d ..." ,i);
      ln = DBL.LHID.n;
      CSR_GetSquareSubmatrix(ln, DBL.loc2orig, 
			     n, ia, ja, a,
			     &lia, &lja, &la);
      fprintfd(stderr, "done \n");
      
      sprintf(filename, "%s_%d.%d.%d", sfile_path, domsize, nproc, i);
      fp = fopen(filename, "w");
      if(fp == NULL)
	{
	  fprintfd(stderr, "ERROR: Unable to create file %s \n", filename);
	  exit(-1);
	}
      
      fprintfd(stderr, "Write the local matrix in file  %s ..." ,filename);
      CSR_Write(fp, phidaloptions.symmetric, ln, lia, lja, la);
      fclose(fp);
      fprintfd(stderr, "done \n");
      
      PhidalDistrHID_Clean(&DBL);
      free(lia);
      free(lja);
      free(la);
    }

  
  /*** Write the Global Phidal decomposition and ordering structure ***/
  sprintf(filename, "%s_%d.%d.HID", sfile_path, domsize, nproc);
  fp = fopen(filename, "w");
  if(fp == NULL)
    {
      fprintfd(stderr, "ERROR: Unable to create file %s \n", filename);
      exit(-1);
    }

  fprintfd(stderr, "Save PHIDAL structure in file %s ..." , filename);
  PHIDAL_WriteHID(fp, &BL);
  fprintfd(stderr, "done \n");

  /** Write the inverse permutation done by the HID **/
  for(i=0;i<BL.n;i++)
    fprintfd(fp, "%d ", iperm[i]);
  fprintfd(fp, "\n");

  /*** Write the user domain decomposition ***/
  for(i=0;i<=ndom;i++)
    fprintfd(fp, "%d ", mapptr[i]);
  fprintfd(fp, "\n");
  for(i=0;i<mapptr[ndom];i++)
    fprintfd(fp, "%d ", mapp[i]);
  fprintfd(fp, "\n");
  
  fclose(fp);

  free(mapp);
  free(mapptr);
  free(a);
  free(ja);
  free(ia);
  
  free(iperm);

  PhidalOptions_Clean(&phidaloptions);
  PhidalHID_Clean(&BL); 
  fprintfd(stdout, "END OF DATA GENERATION \n");


  return 0;
}

# !/bin/bash

OPTMACHINE="-machinefile $OAR_NODE_FILE"
OPTGENERAL="-mumps -preload off -ksp_gmres_restart 200 -ksp_max_it 200 -ksp_monitor -ksp_converged_reason"

OPTORDERING="-hips_grid_ordering"

OPTPARALL="-pc_asm_type basic"

EXEC="./testPETSc-GRID.ex"

#ulimit -s "30000000k"

PROC="1"

cube=0
sqrtdomsize=100 # taille de base d'un domaine

for matrix in GRID
do
    for proc in $PROC
    do
	for sqrtndom in 1 2 4 8 16
	do
	    ndom=`echo "$sqrtndom*$sqrtndom" | bc`
	    domsize=`echo "$sqrtdomsize*$sqrtdomsize" | bc`

#3D	    ndom=`echo "$sqrtndom*$sqrtndom*$sqrtndom" | bc`
#3D         domsize=`echo "$sqrtdomsize*$sqrtdomsize*$sqrtdomsize" | bc`

            nc=`echo "$sqrtdomsize*$sqrtndom+($sqrtndom-1)" | bc`

	    if [ $ndom -ge $proc ]
	    then
		for overlap in 1 #2 5
		do
		    rep="_results-petsc-grid-2d-hipsordering-overlap-$overlap-a_domsize_cst/$domsize/$sqrtndom"
		    mkdir -p  $rep
		    echo $rep

                    echo "_MATFILE_ = $matrix" > $rep/stdout
                    echo "_NBPROC_ = $proc" >> $rep/stdout
		    echo "_DOMSIZE_ = $domsize" >> $rep/stdout		   
		    echo "_SQRTNDOM_ = $sqrtndom" >> $rep/stdout ; echo "_NDOM_ = $ndom" >> $rep/stdout
                    echo "_NC_ = $nc" >> $rep/stdout
                    echo "_CUBE_ = $cube" >> $rep/stdout
		    echo "_OVERLAP_ = $overlap" >> $rep/stdout

		    mpirun $OPTMACHINE -n $proc $EXEC $OPTGENERAL \
			$OPTORDERING -domsize $domsize \
			-nc $nc -cube $cube \\
			>> $rep/stdout 2> $rep/stderr

		done
	    fi
	done
    done
done


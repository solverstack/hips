#!/usr/bin/perl

use strict;
use DBI;
use HTML::Template;

use Report::Report;

my $conf = "matfile, nproc, overlap";

##################################################################
main:
{

    my $argc = $#ARGV + 1;
    my $results;
    if ($argc == 1) { $results=$ARGV[0]; } else { $results="_results"; }

    if (!(-e "$results/data.db")) { print "run parser before ! (file $results/data.db required)\n"; exit(1); }
    
    my $dbargs = {AutoCommit => 0,
		  PrintError => 1};
    
    my $dbh = DBI->connect("dbi:SQLite:dbname=$results/data.db","","",$dbargs);
    if ($dbh->err()) { die "DB Error : $DBI::errstr\n"; }
    
    open(FOUT, ">$results/seq.html");	    
    
    my $req = 
	q( SELECT DISTINCT )
	.$conf
	.q( FROM results ORDER BY )
	.$conf;
    
    my $res = $dbh->selectall_arrayref($req);
    
    for my $row (@$res) {
	my ($matfile, $nproc, $overlap) = @$row;
	my @tab = @$row;
	
	print FOUT "matrix=$matfile, nproc = $nproc, overlap=$overlap\n";
	
	print FOUT Report::lire($dbh, 'conf-report/seq/seq.sql', 'conf-report/seq/seq.tmpl', '$results/seq.html', @tab);
	
    }
    
    close FOUT;
    
    $dbh->disconnect();

}




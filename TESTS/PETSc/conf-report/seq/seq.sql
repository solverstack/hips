SELECT 

r.ndom, r.facto, r.solve, r.facto + r.solve as total, r.it, r.fill

FROM results r

WHERE 
r.matfile  = '_ARG1_'
AND
r.nproc   = '_ARG2_'
AND
r.overlap = '_ARG3_'

ORDER BY 
r.ndom, r.facto, r.solve
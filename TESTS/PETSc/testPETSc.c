/* todo : une fct convert mapptr en IS */
/* todo :ndom=Nsub, virer une des deux variables */
/* TODO : revoir peutêtre l'ordre (hips) / ordering */
/* TODO : LU et gis a voir */
/* TODO : dans le cas LOAD + ordering hips, on charge duex fois la matrice 
   -> si pas ordering petsc, ne pas la charger deux fois
   -> ne plus charger 2 fois du tout
*/

/* TODO LIST */
/* - preload on marche pas tj et dans tous les cas */
/* - update last rev PETSc + repondre 2 derniers emails */

/* compil : */
/* l'un des trois : -DLOAD_MATRIX ou -DLOAD_GRID ou -DLOAD_PETSC */
/* facultatif : -DSAVE_PETSC */

#if !defined(LOAD_MATRIX) && !defined(LOAD_GRID) && !defined(LOAD_PETSC)
#error need -DLOAD_MATRIX or -DLOAD_GRID or -DLOAD_PETSC
#endif

/* DEFAULT */
#define LOAD_MATRIX

/* STAGES : */
#ifndef LOAD_PETSC
#define STAGE_ASSEMBLY
#endif

#ifndef SAVE_PETSC
#define RUN
#define STAGE_PREC
#define STAGE_SOLVE
#endif
/**/

#include <stdlib.h>
#include <assert.h>
#include <string.h> /* strdup */

/* #include <petsc.h> */
#include <petscksp.h>
/* #include <private/pcimpl.h> */

/* #define TYPE_REAL */
/* #define TYPE_COMPLEX */
#include "../../LIB/hips.h" 
#include "../../LIB/io.h"

#include "petsc-mumps.h"

#ifdef LOAD_GRID
#define gen5pt gen5pt_ /* TODO */
#define HIPS_DEFINE_H
#include "../../SRC/INCLUDE/phidal_struct.h"
void GRID_setpar(char *filename,  int *nc, int *cube, char *sfile_path, int *unsym, int *rsa,  PhidalOptions *option);
extern void gen5pt(int *nx,int *ny,int *nz, double *a,int *ja,int *ia, int *iau);
#endif /* LOAD_GRID */

#define HIPS_ORDERING
#ifdef HIPS_ORDERING
#include "../../SRC/INCLUDE/phidal_common.h"
#include "../../SRC/INCLUDE/phidal_ordering.h"
#endif

#define LOCAL_IS

double dwalltime();
#define myPreLoadStage(arg)
#define FPRINTF(arg...) { if (!PetscPreLoadingOn) { ierr = PetscFPrintf(PETSC_COMM_WORLD, arg); CHKERRQ(ierr);} }

void PHIDAL_SymmetrizeMatrix(flag_t job, flag_t numflag, dim_t n, INTL *ia, dim_t *ja, COEF *a, INTL **gia, dim_t **gja, COEF **ga);
INTS Read_options(char *filename,  char *matrix, INTS *sym_pattern, INTS *sym_matrix, char* rhs, INTS* method, INTS *options_int, REAL *options_double);

static char help[] = "PETSC vs HIPS.\n\n";

enum ordering { PETSC_BASIC, PETSC_2DGRID, HIPS_GRID, HIPS_MATRIX};

int main(int argc, char *argv[]) { 
  int i;
  chrono_t t1, t2, ttotal=0;

  int sym_matrix_as_unsym = 0;
/* #warning sym_matrix;   */
  PetscTruth      PreLoad      = PETSC_FALSE; /* TODO PETSC_TRUE; */ /* may be overridden with command line option 
									-preload true or -preload false */
  PetscTruth     ordering;
  PetscTruth     ismumps;
  PetscInt N=-1, overlap=-1;

  PetscInt domsize=-1;

  int numflag;
  int sym_matrix;

  int n, nnz;
  int nproc;
  int proc_id;

  PetscInt       rstart,rend,nlocal;

  PetscErrorCode ierr;
  Vec            x, b, u;      /* approx solution, RHS, exact solution */
  Mat            A;            /* linear system matrix */
  KSP            ksp;         /* linear solver context */
  PC             pc;           /* preconditioner context */
  PetscScalar    one = 1.0;

  char matrixfile[PETSC_MAX_PATH_LEN];

  PetscInt      Nsub=-1; /* ordering = hips_* */
  IS             *is;                     /* array of index sets that define the subdomains */

#ifdef LOCAL_IS
  PetscInt Nlocalsub;
  IS      *localis;
#endif

  assert(sizeof(PetscScalar) == sizeof(COEF)); /* DOUBLE / COMPLEX */

/* #ifdef LOAD_GRID */
  int cube, nc;
/* #endif */

/* #if defined(LOAD_MATRIX) || defined(LOAD_GRID) */
  int *ia, *ja;
  COEF* a;
/* #else */
/* #if defined(HIPS_ORDERING) */
/* #undef HIPS_ORDERING */
/* #warning HIPS_ORDERING disabled */
/* #endif */
/* #endif */

  PetscInitialize(&argc, &argv,(char *)0,help); 
  
  PetscOptionsTruth("-mumps", "Use MUMPS (if ASM method is selected)", argv[0], PETSC_FALSE, &ismumps, PETSC_NULL);

  /* option ordering */
  {
    PetscTruth petsc_basic;
    PetscTruth petsc_2dgrid;
    PetscTruth hips_matrix;
    PetscTruth hips_grid;
    
    PetscOptionsHasName(PETSC_NULL,"-petsc_basic_ordering",&petsc_basic);
    PetscOptionsHasName(PETSC_NULL,"-petsc_2dgrid_ordering",&petsc_2dgrid);
    PetscOptionsHasName(PETSC_NULL,"-hips_matrix_ordering",&hips_matrix);
    PetscOptionsHasName(PETSC_NULL,"-hips_grid_ordering",&hips_grid);
#ifndef SAVE_PETSC
    assert(petsc_basic + petsc_2dgrid + hips_matrix + hips_grid == 1);
#endif
    if (petsc_basic)  ordering = PETSC_BASIC;
    if (petsc_2dgrid) ordering = PETSC_2DGRID;
    if (hips_matrix)  ordering = HIPS_MATRIX;
    if (hips_grid)    ordering = HIPS_GRID;
    
    /* HIPS ordering advanced options */
    if (hips_matrix || hips_grid) {
      domsize = 100000;
      PetscOptionsGetInt(PETSC_NULL,"-domsize",&domsize,PETSC_NULL);
    }

    /* PETSC ordering advanced options */
    if (petsc_basic || petsc_2dgrid) {
      overlap = 1;
      PetscOptionsGetInt(PETSC_NULL,"-overlap",&overlap,PETSC_NULL); /* todo : a recup de _asm_overlap */
    }

    if (petsc_2dgrid) {
      N=1;
      PetscOptionsGetInt(PETSC_NULL,"-N",&N,PETSC_NULL); /* todo : a recup de -asm_block */
    }
  }
  
    

  /** Init MPI environment **/
  /* MPI_Init(&argc, &argv); */
  MPI_Comm_rank(MPI_COMM_WORLD, &proc_id);
  MPI_Comm_size(MPI_COMM_WORLD, &nproc);

  /* PetscPrintf(PETSC_COMM_WORLD, "Hello World :-) \n"); */

  PreLoadBegin(PreLoad,"Main Stage");
  {
    int stage;
    ierr = PetscLogStageGetId("Main Stage",  &stage);CHKERRQ(ierr);
    ierr = PetscLogStageSetVisible(stage, PETSC_FALSE);CHKERRQ(ierr);
  }

#ifdef LOAD_MATRIX
  {
    /******************************************/
    /* Read the parameters from "inputs" file */
    /******************************************/
    myPreLoadStage("Load Config");
    {
#ifdef USE_INPUT
      /* useless parameters of Read_options */
      INTS   options_int   [OPTIONS_INT_NBR];
      double options_double[OPTIONS_REAL_NBR];
      char   rhsfile   [PETSC_MAX_PATH_LEN];
      int    method;
      int    sym_pattern;

      Read_options(NULL, matrixfile, &sym_pattern, &sym_matrix, rhsfile,
      		   &method, options_int, options_double);
#else /* USE_INPUT */
      PetscTruth flg;
      PetscOptionsGetString(PETSC_NULL,"-fhips",matrixfile,PETSC_MAX_PATH_LEN-1,&flg);
      if (!flg) SETERRQ(1,"Must indicate matrix file with the -fhips option");
      
      PetscTruth is_sym_matrix;
      PetscOptionsTruth("-sym", "Load : Symmetric matrix", 
			argv[0], PETSC_FALSE, &is_sym_matrix, PETSC_NULL);
      sym_matrix = is_sym_matrix;
#endif /* USE_INPUT */
    }

    /**********************************/
    /* Read the matrix from file      */
    /**********************************/
    myPreLoadStage("Read Matrix");
    FPRINTF(stdout, "* Read the matrix %s\n", matrixfile);
    
    {
      if (PetscPreLoadingOn) {
	char            mtpreload[PETSC_MAX_PATH_LEN];
	if (sym_matrix) sprintf(mtpreload, "../MATRICES/bcsstk01.rsa");
	else            sprintf(mtpreload, "../MATRICES/orsirr_1.rua");
	
	PetscPrintf(PETSC_COMM_WORLD, "* Preloading\n");

	CSRread(mtpreload, &n, &nnz, &ia, &ja, &a, NULL/* &sym_matrix */);
      } else
	CSRread(matrixfile, &n, &nnz, &ia, &ja, &a, NULL/* &sym_matrix */); /* todo : pour simplifier, utiliser sym_matrix au lieu de -sym*/
      
      /** C : numbering starts from 0 (HIPS and PETSC lib use C numbering) **/
      CSR_Fnum2Cnum(ja, ia, n);
      numflag = 0;
    }
    
  }
#endif /* LOAD_MATRIX */
#ifdef LOAD_GRID
  {
    /* int  nc, cube; */ /* grid type and size */

    /******************************************/
    /* Read the parameters from "inputs" file */
    /******************************************/
    myPreLoadStage("Load Config");
    {
      if (PetscPreLoadingOn) {  
	int nblock; PetscTruth flg;
	PetscOptionsGetInt(PETSC_NULL, "-pc_asm_blocks", &nblock, &flg);
	/* printf("nblock=%d\n", nblock); */
	cube=0; nc=MAX(nproc, nblock);
	sym_matrix=1;
      } else {
	/* get cube and nc from inputs file */
	char sfile_path[PETSC_MAX_PATH_LEN];
	int unsym, rsa;
	PhidalOptions options;
      
	GRID_setpar(NULL, &nc, &cube, sfile_path, &unsym, &rsa, &options);
	PetscOptionsGetInt(PETSC_NULL,"-nc",&nc,PETSC_NULL);     /* remplace inputs dans les scripts */
	PetscOptionsGetInt(PETSC_NULL,"-cube",&cube,PETSC_NULL); /* remplace inputs dans les scripts */

	assert(unsym==1); assert(rsa==1); sym_matrix=1;
	assert(options.symmetric == rsa);
      }
    
      if(cube == 0) n = nc*nc; else n = nc*nc*nc;

/* #ifdef SAVE_PETSC */ /* TODO  : uncomment (warning) */
      sprintf(matrixfile, "grid-%s-%d", (cube==0) ? "2D" : "3D", nc);
/* #endif */
    }

    /**********************************/
    /* Generate the matrix            */
    /**********************************/
    myPreLoadStage("Generate the matrix");
    FPRINTF(stdout, "* Generate the matrix\n");
    {
      int cparam;
      int* iau = (int *)malloc((n+1)*sizeof(int));

      nnz = 10*(n+1);

      ia = (int *)malloc((n+1)*sizeof(int));      
      ja = (int *)malloc(nnz*sizeof(int));
      a  = (COEF *)malloc(nnz*sizeof(COEF));
   
      if(cube == 0) cparam=1; else cparam=nc;
      gen5pt(&nc, &nc, &cparam, a, ja, ia, iau);
      free(iau);
   
      nnz = ia[n]-ia[0];

      /** C : numbering starts from 0 (HIPS and PETSC lib use C numbering) **/
      CSR_Fnum2Cnum(ja, ia, n);
      numflag = 0;
    }

  }
#endif /* LOAD_GRID */

#ifdef LOAD_PETSC
  {
    /*TODO : PreLoadingOn*/
    PetscTruth flg;
    PetscViewer fd;
    /* char filename[PETSC_MAX_PATH_LEN]; */
    /* #ifdef LOAD_MATRIX       */
    /*     sprintf(filename, "/tmp/%s", matrixfile); */
    /* #ifdef LOAD_GRID */
    /*     sprintf(filename, "/tmp/grid-%s-%d", (cube==0) ? "2D" : "3D", nc); */
    /* #endif */

    PetscOptionsGetString(PETSC_NULL,"-f",matrixfile,PETSC_MAX_PATH_LEN-1,&flg);
    if (!flg) SETERRQ(1,"Must indicate binary file with the -f option");

    PetscTruth is_sym_matrix;
    PetscOptionsTruth("-sym", "Load : Symmetric matrix (must be used with -mat_ignore_lower_triangular)", 
		      argv[0], PETSC_FALSE, &is_sym_matrix, PETSC_NULL);
    sym_matrix = is_sym_matrix;

    ierr = PetscViewerBinaryOpen(PETSC_COMM_WORLD,matrixfile,FILE_MODE_READ,&fd);CHKERRQ(ierr);
    if (sym_matrix) {
      ierr = MatLoad(fd,MATMPISBAIJ,&A);CHKERRQ(ierr);
      /* MatSetOption(A,MAT_IGNORE_LOWER_TRIANGULAR, PETSC_TRUE); */ /* needed (save unsym) */
    } else {
      ierr = MatLoad(fd,MATMPIAIJ,&A);CHKERRQ(ierr);
    }
    PetscViewerDestroy(fd);
    
    MatGetSize(A, PETSC_NULL, &n);
 
    { /* set nnz */
      MatInfo info;
      MatGetInfo(A,MAT_GLOBAL_SUM,&info);
#ifndef LOAD_MATRIX
      nnz = info.nz_allocated;
      
      if (sym_matrix) nnz = nnz/2;
#else /* LOAD_MATRIX */
      if (sym_matrix) assert(info.nz_allocated == 2*nnz-n); else  assert(info.nz_allocated == nnz);
#endif /* LOAD_MATRIX */
    }

    numflag = 0;
  }
#endif

  FPRINTF(stdout, "  Matrix : dim=%ld nnz=%ld\n", (long)n, (long)nnz);

  if (sym_matrix && sym_matrix_as_unsym) {
    FPRINTF(stdout, "! Symmetric Matrix solved as an unsymmetric matrix\n");
#ifdef LOAD_MATRIX
    /** Symmetrize the matrix (in rsa we only have the lower triangular part) ****/
    int job;
    INTL *ib; dim_t *jb; COEF* b;
    ib = ia; jb = ja; b = a;
    job = 2;
    PHIDAL_SymmetrizeMatrix(job, numflag, n, ib, jb, b, &ia, &ja, &a);
    free(ib); free(jb); free(b);
#endif /* LOAD_MATRIX */

#ifdef LOAD_GRID
    /* gen5pt => lower + upper part of the sym matrix ... */
#endif /* LOAD_GRID */
   
    sym_matrix = 0;
  }
  
  FPRINTF(stdout, "  done\n");








#ifdef HIPS_ORDERING
/* #ifdef ISgis   */
/*   IS gis;  */
/* #endif */
  if (ordering == HIPS_MATRIX || ordering == HIPS_GRID) {
      FPRINTF(stdout, "* Partitionning (Scotch + Overlap)\n");

      int ndom;
      dim_t* mapptr;
      dim_t* mapp;

      dim_t *perm, *iperm;

      INTL* ig = (INTL *) malloc(sizeof(INTL) * (n + 1));
      dim_t* jg = (dim_t *) malloc(sizeof(dim_t) * nnz);
      memcpy(ig, ia, sizeof(INTL) * (n + 1));
      memcpy(jg, ja, sizeof(dim_t) * nnz);

      CSR_SuppressDouble(0/*numflag*/, n, ig, jg, NULL);
      
      if (/* sym_pattern == TODO */ 1) {
	INTL* ib = ig;
	dim_t* jb = jg;
	int job = 0;
	PHIDAL_SymmetrizeMatrix(job, numflag, n, ib, jb, NULL, &ig, &jg,
				NULL);
	free(ib);
	free(jb);
      }

      perm = (dim_t *) malloc(sizeof(dim_t) * n);
      iperm = (dim_t *) malloc(sizeof(dim_t) * n);
    
      /** Delete the self edge in the graph (METIS_NodeND need that)**/
      /** OIMBE Put THAT IN Perm2SizedDomain ?? **/
      PHIDAL_CsrDelDiag(0, n, ig, jg);
      
 
  if (ordering == HIPS_MATRIX) {
    printf("MATRIX ORDERING\n");
#ifndef LOCAL_IS
    /* OLD : sans utiliser l'interface de HIPS : */
    /* Qu'en un dom / proc (pas de recup info balance)  */
    PHIDAL_Perm2SizedDomains(domsize, n, ig, jg, &ndom, &mapptr, &mapp, perm, iperm);
    /* TODO : remodif dans HIPS pour respecter regles dans intefaces entre domaines. => impact ? */

    /* /!\ IS is a local index set with matrix_ordering and LOCAL_IS macro - else : global */
    Nsub = ndom;
    is = (IS*)malloc(sizeof(IS)*Nsub);
    for(i=0; i<ndom; i++) {
      ISCreateGeneral(PETSC_COMM_SELF,mapptr[i+1]-mapptr[i],&mapp[mapptr[i]],&is[i]);
    }
    /* todo free mapptr */

#else /* LOCAL_IS */
    /*NEW*/ 
    int idnbr=1, id=1;
    /* int ln; */
    HIPS_Initialize(idnbr);
    HIPS_SetDefaultOptions(id, HIPS_HYBRID);
    HIPS_SetOptionINT(id, HIPS_FORTRAN_NUMBERING, 0);
    HIPS_SetOptionINT(id, HIPS_PARTITION_TYPE, 0);
    HIPS_SetOptionINT(id, HIPS_DOMSIZE, domsize);
    
    HIPS_GraphGlobalCSR(id, n, ia, ja, 0);
    
    {
      INTS locdomnbr;
      INTS size;
      INTS *mapptr, *mapp;
      HIPS_GetLocalDomainNbr(id, &locdomnbr, &size);
      fprintf(stderr, "Proc %ld has %ld domain \n", (long)proc_id, (long)locdomnbr);
      
      mapp = (INTS *)malloc(sizeof(INTS)*size);
      mapptr = (INTS *)malloc(sizeof(INTS)*(locdomnbr+1));
      HIPS_GetLocalDomainList(id, mapptr, mapp);
      
      /* for(i=0;i<locdomnbr;i++) */
      /*   fprintf(stderr, "Proc %ld Domain %ld size = %ld \n", (long)proc_id, (long)i, (long)(mapptr[i+1]-mapptr[i])); */
      
      /* /!\ IS is a local index set with matrix_ordering and LOCAL_IS macro - else : global */
      Nlocalsub = locdomnbr; /* virer une des deux variables */
      localis = (IS*)malloc(sizeof(IS)* Nlocalsub);
      for(i=0; i<Nlocalsub; i++) {
	ISCreateGeneral(PETSC_COMM_SELF,mapptr[i+1]-mapptr[i],&mapp[mapptr[i]],&localis[i]);
      }

      free(mapp);
      free(mapptr);
    }
    
    /* very OLD, debut de version // :  */
    /*       HIPS_GetLocalUnknownNbr(id, &ln); */
    /*       int* unknownlist = (INTS *)malloc(sizeof(INTS)*ln); */
    /*       HIPS_GetLocalUnknownList(id, numflag, unknownlist); */
    /*       int domnbr; */
    /*       int unknownnbr; */
    /*       int partnbr; */
    /*       HIPS_GetLocalPartitionNbr(id, &domnbr, &unknownnbr, INTS *partnbr, INTS *partkeynbr); */
    /*       HIPS_GetLocalPartition(id, INTS numflag, INTS *unknownlist,  INTS *parttab, INTS *prockeyindex, INTS *prockey); */
    /* ICI TODO */
    /*       INTS *mapptr, *mapp; */
    /*       INTS domnbr, listsize; */
    /*       HIPS_GetLocalDomainNbr(id, &domnbr, &listsize); */
    
    /*       mapptr=(INTS)malloc(sizeof(INTS)*domnbr); */
    /*       mapp=(INTS)malloc(sizeof(INTS)*listsize); */
    
    /*       HIPS_GetLocalDomainList(id, mapptr, mapp); */

#endif /* LOCAL_IS */
    
    
#ifdef DEBUG_M3
    int i,j;
    printf("ndom=%d\n", ndom);
    for(i=0; i<ndom; i++) {
      printf("dom n %d\n", i);
      for(j=mapptr[i]; j<mapptr[i+1]; j++) {
	printf("%d ", mapp[j]);
      }
      printf("\n");
    }
#endif /* DEBUG_M3 */

    /* HIPS_Finalize(); bug */

  } /* else */ if (ordering == HIPS_GRID) {
    printf("GRID ORDERING\n");
    if(cube == 1)
      PHIDAL_GridRegularSizeDomains(domsize, nc, nc, nc, n, ig, jg, &ndom, &mapptr, &mapp, perm, iperm);
    else
      PHIDAL_GridRegularSizeDomains(domsize, nc, nc, 1, n, ig, jg, &ndom, &mapptr, &mapp, perm, iperm);

    /* /!\ IS is a local index set with matrix_ordering and LOCAL_IS macro - else : global */
    Nsub = ndom;
    is = (IS*)malloc(sizeof(IS)*Nsub);
    for(i=0; i<ndom; i++) {
      ISCreateGeneral(PETSC_COMM_SELF,mapptr[i+1]-mapptr[i],&mapp[mapptr[i]],&is[i]);

#ifdef DEBUG_PRINT
      /* print mapp / mapptr */
      
      /* debug only  : local sort of node id inside a domain */
      /* for(i=0; i<ndom; i++) { */
      /*   quicksort(mapp, mapptr[i], mapptr[i+1]-1); */
      /* } */

      for(i=0; i<ndom; i++) {
	int j;
	printf("dom %d :", i);
	for(j=mapptr[i]; j<mapptr[i+1]; j++) {
	  printf("%d ", mapp[j]);
	}
	printf("\n");
      }

      /* print doms */
      for(i=0; i<Nsub; i++)
	ISView(is[i],PETSC_VIEWER_STDOUT_SELF);
      /* -- print */
#endif /* DEBUG_PRINT */
    }
  }
  
  free(ig); free(jg);

/* #ifdef ISgis */
/*       /\* IS gis; *\/ */
/*       ISCreateGeneral(PETSC_COMM_SELF,n,perm,&gis); */
/*       ISSetPermutation(gis); /\* pas chiant ... *\/ */
/*       /\* ISView(gis,PETSC_VIEWER_STDOUT_SELF); *\/ */
/* #endif /\* ISgis *\/ */

/* #ifdef LU */
/*       CSR_Perm(n, ia, ja, a, perm); */
/* #endif /\* LU *\/ */
      free(perm); free(iperm);


     
      FPRINTF(stdout, "  done\n"); /* todo prise de temps */ 

    } /* (ordering == hips_*) */
#else
/*   printf("ICI\n"); */
/*     exit(1); */
#endif /* HIPS_ORDERING */












  /******************************************/
  /* Create Vector */
  /******************************************/
  myPreLoadStage("Create Vector");
  ierr = VecCreate(PETSC_COMM_WORLD,&x);CHKERRQ(ierr);
  ierr = VecSetSizes(x,PETSC_DECIDE,n);CHKERRQ(ierr);
  ierr = VecSetFromOptions(x);CHKERRQ(ierr);

  ierr = VecDuplicate(x,&u);CHKERRQ(ierr);
  ierr = VecDuplicate(x,&b);CHKERRQ(ierr);
  
  /* Identify the starting and ending */
  ierr = VecGetOwnershipRange(x,&rstart,&rend);CHKERRQ(ierr);
  ierr = VecGetLocalSize(x,&nlocal);CHKERRQ(ierr);

  /* FPRINTF(stdout, "rstart=%d rend=%d nlocal=%d\n", rstart, rend, nlocal); */

#ifdef STAGE_ASSEMBLY
  /**********************************/
  /* Assembly of the matrix         */
  /**********************************/
  {
    int j;
    int iloc;
    int *d_nnz, *o_nnz;

    myPreLoadStage("Assembly the Matrix");
    FPRINTF(stdout, "* Assembly of the matrix\n");
    /* nnztab : array containing the number of nonzeros in the various rows */
    d_nnz = (int*)malloc(sizeof(int)*nlocal);
    o_nnz = (int*)malloc(sizeof(int)*nlocal);
    for(i=rstart, iloc=0; i<rend; i++, iloc++) {
      d_nnz[iloc] = 0;
      o_nnz[iloc] = 0;
      
      for(j=ia[i];j<ia[i+1];j++) {
	if ((ja[i] >= rstart) && (ja[i] < rend)) {
	  d_nnz[iloc] += 1;
	} else {
	  o_nnz[iloc] += 1;
	}
      }
      assert(d_nnz[iloc]+o_nnz[iloc] == ia[i+1]-ia[i]);
      if (nproc==1) { assert(i == iloc); assert(o_nnz[iloc] == 0); /* seq */ }
    }
 

/* 2694: @*\/ */
/* 2695: PetscErrorCode  MatCreateSeqAIJ(MPI_Comm comm,PetscInt m,PetscInt n,PetscInt nz,const PetscInt nnz[],Mat *A) */
/* 2696: { */

/* 2700:   MatCreate(comm,A); */
/* 2701:   MatSetSizes(*A,m,n,m,n); */
/* 2702:   MatSetType(*A,MATSEQAIJ); */
/* 2703:   MatSeqAIJSetPreallocation_SeqAIJ(*A,nz,(PetscInt*)nnz); */
/* 2704:   return(0); */
/* 2705: } */

/* MatCreateMPIAIJWithArrays avec CSR locale */

    /* ierr = MatCreateSeqAIJ(PETSC_COMM_WORLD, n, n, 0,d_nnz,&A);CHKERRQ(ierr); */
    if (sym_matrix)
      ierr = MatCreateMPISBAIJ(PETSC_COMM_WORLD, 1, nlocal, nlocal, n, n, 0, d_nnz, 0, o_nnz, &A);
    else
      ierr = MatCreateMPIAIJ(PETSC_COMM_WORLD, nlocal, nlocal, n, n, 0, d_nnz, 0, o_nnz, &A);
    CHKERRQ(ierr);
    
    free(d_nnz);
    free(o_nnz);
  }
#endif /* STAGE_ASSEMBLY */

  MatSetFromOptions(A);

#ifdef LOAD_GRID /* or LOAD */
  if (sym_matrix) /* gen5pt => lower + upper part of the sym matrix ... */
    MatSetOption(A,MAT_IGNORE_LOWER_TRIANGULAR, PETSC_TRUE);
#endif

  /* useful ? */
  ierr = MatSetOption(A, MAT_SYMMETRIC, sym_matrix); CHKERRQ(ierr);
  /* ierr = MatSetOption(A, MAT_STRUCTURALLY_SYMMETRIC, sym_pattern); CHKERRQ(ierr); */

#ifdef STAGE_ASSEMBLY
  int j1, jf;
#ifdef TEST
  if (proc_id == 0) for(i=0; i<n; i++) {
#else
  for(i=rstart; i<rend; i++) {
#endif
    j1 = ia[i];
    jf = ia[i+1]-1;
    ierr = MatSetValues(A,1,&i,jf-j1+1,ja+j1,a+j1,INSERT_VALUES);CHKERRQ(ierr);
  }

  free(ia); free(ja); free(a);

  t1 = dwalltime();

  ierr = MatAssemblyBegin(A,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  ierr = MatAssemblyEnd  (A,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);

  t2 = dwalltime(); /* ttotal += t2-t1; no */
  FPRINTF(stdout, "  MatAssemblyBegin/End in %g seconds\n", t2-t1);
#endif /* STAGE_ASSEMBLY */


#ifdef HIPS_ORDERING
    if (ordering == HIPS_MATRIX || ordering == HIPS_GRID) {
    
#ifdef USEFULTEST    
    /** useful test */
    IS rperm, cperm;
    MatGetOrdering(A, /* MATORDERING_NATURAL */MATORDERING_ND, &rperm, &cperm);
    /* ISView(rperm,PETSC_VIEWER_STDOUT_SELF); */
#endif /* USEFULTEST */ 

/* #ifdef ISgis */
/*     Mat B; */
/*     MatConvert(A, MATSAME, MAT_INITIAL_MATRIX, &B); */
/*     ierr = MatPermute(A, gis, gis, &B);CHKERRQ(ierr); */
/*     MatCopy(B, A, DIFFERENT_NONZERO_PATTERN); /\* gruik *\/ */
/*     /\* ISDestroy(gis); *\/ */
/* #endif /\* ISgis *\/ */

    }
#endif /* HIPS_ORDERING */
   

#ifdef SAVE_PETSC
  {
    PetscViewer fd;
    char savename[PETSC_MAX_PATH_LEN];

    /* TODO : -DLOAD_PETSC + -DSAVE_PETSC_FORMAT pb. */
    sprintf(savename, "%s.petsc", matrixfile);

    printf("savename = %s\n", savename);

    ierr = PetscViewerBinaryOpen(PETSC_COMM_WORLD,savename,FILE_MODE_WRITE,&fd);CHKERRQ(ierr);
    MatView(A, fd);
    PetscViewerDestroy(fd);

    exit(1);
  }
#endif

  if (nproc==1) {
    PetscTruth flg;
    if (sym_matrix) {
      ierr=MatIsSymmetric(A, 0.0, &flg);CHKERRQ(ierr);
      assert(flg == PETSC_TRUE);
    }
  }
  
  /**********************************/
  /* Set exact solution; then compute right-hand-side vector. */
  /**********************************/
  myPreLoadStage("Set exact solution");
  ierr = VecSet(u,one);CHKERRQ(ierr);
  ierr = MatMult(A,u,b);CHKERRQ(ierr);

  /**********************************/
  /* Create the linear solver and set various options */
  /**********************************/
  myPreLoadStage("Create the linear solver");
  ierr = KSPCreate(PETSC_COMM_WORLD,&ksp);CHKERRQ(ierr);
  ierr = KSPSetOperators(ksp,A,A,DIFFERENT_NONZERO_PATTERN);CHKERRQ(ierr);

  ierr = KSPGetPC(ksp,&pc);CHKERRQ(ierr);
  ierr = PCSetType(pc,PCASM);CHKERRQ(ierr);
  ierr = KSPSetTolerances(ksp,1.e-7,PETSC_DEFAULT,PETSC_DEFAULT,PETSC_DEFAULT);CHKERRQ(ierr);
  ierr = KSPSetFromOptions(ksp);CHKERRQ(ierr); /* Set runtime options */

  /***********************************/
  /* Setting user-defined subdomains */
  /***********************************/
  FPRINTF(stdout, "Setting subdomains\n");

  switch (ordering)
    {
    case PETSC_BASIC:
      {
	PCASMSetOverlap(pc,overlap);
	break;
      } /* case PETSC_BASIC */

    case PETSC_2DGRID: 	/* a ne pas appeller sur une matrice (pas de test) */
      {
	assert(cube==0);
	
	/* assert 2D */
	if (nproc != 1) SETERRQ(1,"PCASMCreateSubdomains() is currently a uniprocessor routine only!"); 
	printf("n=%d, N=%d\n", n , N);
	assert(nc == sqrt(n));
	PCASMCreateSubdomains2D(nc,nc,N,N,1,overlap,&Nsub,&is);
	
	/* printf("n=%d, N=%d Nsub=%d\n", nc , N, Nsub); */
	/* for(i=0; i<Nsub; i++) */
	/*  ISView(is[i],PETSC_VIEWER_STDOUT_SELF); */
	
	PCASMSetLocalSubdomains(pc,Nsub,is);
	
	break;
    } /* case PETSC_2D */
  
    case HIPS_GRID:
    case HIPS_MATRIX:
      { 
#ifndef LOCAL_IS
	/* PCASMSetLocalSubdomains(pc,Nsub,is); */ /* pour 1 proc */
	/* else : */

	/* test balance bidon */
	int div=Nsub/nproc;
	int nlocal;
	if (proc_id != nproc-1) nlocal=div; else nlocal=div+Nsub%nproc;
	printf("*** warning : balance bidon\n");
	printf("proc_id=%d nlocal=%d from=%d\n", proc_id, nlocal, proc_id*div);
	PCASMSetLocalSubdomains(pc,nlocal,is + proc_id*div);
#else
    if(ordering != HIPS_MATRIX) assert(0); /* desactiver LOCAL_IS */

    PCASMSetLocalSubdomains(pc,Nlocalsub,localis);
#endif
		
	break;	
      } /* case HIPS_ORDERING */
      
    default:
      {
	printf("ordering value pb\n");
	exit(1);
      } /* default */
    }
   
  
  FPRINTF(stdout, " done (setting subdomains)\n");



#ifdef RUN
  /**********************************/
  /* ASM                            */
  /**********************************/
  {
    PetscTruth isasm;
    ierr = PetscTypeCompare((PetscObject)pc,PCASM,&isasm);CHKERRQ(ierr);
    
    if (isasm) {
      KSP        *subksp;       /* array of KSP contexts for local subblocks */
      PetscInt   nlocal,first;  /* number of local subblocks, first local subblock */
      PC         subpc;         /* PC context for subblock */

      ierr = KSPSetUp(ksp);CHKERRQ(ierr);
      ierr = PCASMGetSubKSP(pc,&nlocal,&first,&subksp);CHKERRQ(ierr);

/*       FPRINTF(stdout, "%d domains (sur proc 1)\n", nlocal); */
      for (i=0; i<nlocal; i++) {
	ierr = KSPGetPC(subksp[i],&subpc);CHKERRQ(ierr);

	if (sym_matrix)
	  ierr = PCSetType(subpc,PCCHOLESKY);
	else
	  ierr = PCSetType(subpc,PCLU);
	CHKERRQ(ierr);

	if (ismumps)
	  PCFactorSetMatSolverPackage(subpc, MAT_SOLVER_MUMPS);

/* 	KSPSetFromOptions(subksp[i]); /\* -sub_pc_factor_mat_solver_package mumps *\/ */

	/* -sub_pc_factor_mat_ordering_type */
	/* BAIJ => natural  */
	/* Development Version :
	   For Cholesky and ICC and the SBAIJ format reorderings are not available,
	   since only the upper triangular part of the matrix is stored.
	   You can use the SeqAIJ format in this case to get reorderings.  */
	/* PCFactorSetMatOrderingType(subpc, ordering); */

	ierr = KSPSetType(subksp[i],KSPPREONLY);CHKERRQ(ierr);
	ierr = KSPSetTolerances(subksp[i],1.e-7,PETSC_DEFAULT,PETSC_DEFAULT,PETSC_DEFAULT);CHKERRQ(ierr);
      }

    }

    /* Set runtime options (override previous config if used) */
/*     KSPSetFromOptions(ksp);CHKERRQ(ierr); */
  }



/*   exit(1); */


  /**********************************/
  /* Solve the linear system */
  /**********************************/
  PreLoadStage("SetUp");
  FPRINTF(stdout, "* SetUp\n");
  t1 = dwalltime(); 

#ifdef STAGE_PREC
  ierr = KSPSetUp(ksp);

  {
    PetscTruth isasm;
    ierr = PetscTypeCompare((PetscObject)pc,PCASM,&isasm);CHKERRQ(ierr);
    
    if (isasm) {
      KSP        *subksp;       /* array of KSP contexts for local subblocks */
      PetscInt   nlocal,first;  /* number of local subblocks, first local subblock */
      
      ierr = PCASMGetSubKSP(pc,&nlocal,&first,&subksp);CHKERRQ(ierr);
      
      for (i=0; i<nlocal; i++) {
	KSPSetUp(subksp[i]);
      }
    }
  }
  
  t2 = dwalltime(); ttotal += t2-t1;
  FPRINTF(stdout, "  SetUp in %g seconds\n", t2-t1);
  FPRINTF(stdout, "  done\n");
  PreLoadStage("Main Stage");
#endif

#ifdef STAGE_SOLVE
  /**********************************/
  /* Solve the linear system */
  /**********************************/
  PreLoadStage("Solve");
  FPRINTF(stdout, "* Solve\n");
  t1  = dwalltime(); 
  ierr = KSPSolve(ksp,b,x);CHKERRQ(ierr); 
  t2  = dwalltime(); ttotal += t2-t1;
  FPRINTF(stdout, "  Solve in %g seconds\n", t2-t1);
  FPRINTF(stdout, "  done\n");
  PreLoadStage("Main Stage");

  /**********************************/
  /* Check the error */
  /**********************************/
  {
    PetscReal      norm;         /* norm of solution error */
    PetscInt       its;
    PetscScalar    neg_one = -1.0;

    myPreLoadStage("Check the error");
    
    ierr = VecAXPY(x,neg_one,u);CHKERRQ(ierr);
    ierr = VecNorm(x,NORM_2,&norm);CHKERRQ(ierr);
    
    ierr = KSPGetIterationNumber(ksp,&its);CHKERRQ(ierr);

    FPRINTF(stdout, "* View\n");
    if (!PetscPreLoadingOn)
      PetscPrintf(PETSC_COMM_WORLD, "  - Norm of error %A, Iterations %D\n", norm, its);
  }
#endif

  /**********************************/
  /* View                           */
  /**********************************/
  /* -ksp_view */
  /* if (! PetscPreLoadingOn) */
  /*     ierr = KSPView(ksp,PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr); */

  if (! PetscPreLoadingOn) {
    PetscTruth isasm;
    ierr = PetscTypeCompare((PetscObject)pc,PCASM,&isasm);CHKERRQ(ierr);
    
    if (isasm) {
      int tot_nnzA=0, tot_nnzL=0;
      PetscTruth islu, iscc;

      KSP        *subksp;       /* array of KSP contexts for local subblocks */
      PetscInt   nlocal,first;  /* number of local subblocks, first local subblock */
      PC         subpc;         /* PC context for subblock */

      PetscInt nlocal_A; Mat* local_A;

      ierr = PCASMGetSubKSP(pc,&nlocal,&first,&subksp);CHKERRQ(ierr);
      ierr = PCASMGetLocalSubmatrices(pc, &nlocal_A, &local_A);CHKERRQ(ierr);
      assert(nlocal == nlocal_A);

      for (i=0; i<nlocal; i++) {
	ierr = KSPGetPC(subksp[i],&subpc);CHKERRQ(ierr);

	if (proc_id == 0 && i==0) {
	  /* PCView(subpc, PETSC_VIEWER_STDOUT_WORLD); */
	  const PCType     cstr;
	  PCGetType(subpc,&cstr);
	  ierr = PetscSynchronizedPrintf(PETSC_COMM_WORLD, "  - precond type : asm/%s\n",cstr);CHKERRQ(ierr);
	}

	ierr = PetscTypeCompare((PetscObject)subpc,PCLU,&islu);CHKERRQ(ierr);
	ierr = PetscTypeCompare((PetscObject)subpc,PCCHOLESKY,&iscc);CHKERRQ(ierr);
	/* etc. */

	if(islu || iscc) {
	  Mat local_L;
	  MatInfo infoL; const MatType type;
	  REAL  fillA; int nnzA, nnzL;
	  MatInfo infoA; 

	  /* Does not increase the reference count for the matrix so DO NOT destroy it */
	  ierr = PCFactorGetMatrix(subpc, &local_L);CHKERRQ(ierr);

	  ierr = MatGetInfo(local_A[i], MAT_LOCAL, &infoA);CHKERRQ(ierr);

	  ierr = MatGetInfo(local_L, MAT_LOCAL, &infoL);CHKERRQ(ierr);
	  ierr = MatGetType(local_L, &type);CHKERRQ(ierr);

	  if (!ismumps) {

	    nnzA = infoA.nz_allocated; /* nnzL/fillA; */
	    nnzL = infoL.nz_allocated;     
	    fillA = nnzL/(REAL)nnzA; /* infoL.fill_ratio_needed; */

	  } else {
	    
	    Mat_MUMPS      *lu=(Mat_MUMPS*)local_L->spptr;

	    /* PetscSynchronizedPrintf(PETSC_COMM_WORLD, "  -> MUMPS\n"); */
	    if (lu->id.sym == 0) type="unsym mumps";
	    else
	      if (lu->id.sym == 1) type="spd mumps"; else type="sym mumps";

	    /* printf("  INFOG(20) (estimated number of entries in the factors): %d \n",lu->id.INFOG(20)); */
	    /* printf("  INFOG(3) (estimated real workspace for factors on all processors after analysis): %d \n",
	       lu->id.INFOG(3)); */
	    
	    nnzA = infoA.nz_allocated;
	    nnzL = lu->id.INFOG(20);
	    fillA = nnzL/(REAL)nnzA; 

	  }

	  tot_nnzA += nnzA;
	  tot_nnzL += nnzL;
	  
	  ierr = PetscSynchronizedPrintf(PETSC_COMM_WORLD, 
					 "  | proc %2d / %2d - block %2d : nnzA=%8d nnzL=%8d fill=%lf (%s)\n", 
					 proc_id+1, nproc, i, nnzA, nnzL, fillA, type);CHKERRQ(ierr);
	  
	}

      }

      ierr = PetscSynchronizedPrintf(PETSC_COMM_WORLD,     
				     "  - proc %2d / %2d - total    : nnzA=%8d nnzL=%8d fill=%lf\n", 
				     proc_id+1, nproc, tot_nnzA, tot_nnzL, tot_nnzL/(REAL)tot_nnzA);CHKERRQ(ierr);

    }
  }
  
  ierr = PetscSynchronizedFlush(PETSC_COMM_WORLD);CHKERRQ(ierr);

  /* PetscMemoryGetMaximumUsage -memory_info */

#endif /* RUN */

  FPRINTF(stdout, "  -  Total time : %g seconds\n", ttotal);
  
  /**********************************/
  /* Free work space.  */
  /**********************************/
  myPreLoadStage("Free work space");
#ifdef RUN
  ierr = KSPDestroy(ksp); CHKERRQ(ierr);
#endif
  ierr = MatDestroy(A);   CHKERRQ(ierr);
  ierr = VecDestroy(b);   CHKERRQ(ierr);
  ierr = VecDestroy(u);   CHKERRQ(ierr);
  ierr = VecDestroy(x);   CHKERRQ(ierr);

  if (ordering == HIPS_MATRIX || ordering == HIPS_GRID) {
    for (i=0; i<Nsub; i++) {
      ISDestroy(is[i]);
    }
    /* PetscFree(is); todo, PetscMalloc(is) pour -hips_matrix et -hips_grid */
  }
  
  PreLoadEnd();

  PetscFinalize(); 
  return 0; 
} 

# !/bin/bash

#MTX_PATH="/home/jeremie/travail/matrices/"
#MTX_PATH="/home/bordeaux/jgaidamo/matrices/"
MTX_PATH="$HOME/matrices/" #hephaistos

OPTMACHINE=""
#OPTMACHINE="-machinefile $OAR_NODE_FILE" #borderline

###############################################

MTX="audi.rsa inline.rsa NICE20.mm NICE25.mm"
MTX_TYPE="-sym -mat_ignore_lower_triangular"

###############################################

DOMSIZE="4000 8000 10000 20000"
PROC="1 2 4 8"

###############################################

OPTGENERAL="-preload off -mumps -ksp_gmres_restart 1000 -ksp_max_it 1000 -ksp_monitor -ksp_converged_reason"
OPTORDERING="-hips_matrix_ordering"
OPTPARALL="-pc_asm_type basic"

#ulimit -s "30000000k"

for matrix in $MTX
do
    EXEC="./real/testPETSc-LOAD.ex -f $MTX_PATH/$matrix.petsc -fhips $MTX_PATH/$matrix"

    for proc in $PROC
    do
	for domsize in $DOMSIZE
	do
	    rep="_results/$matrix/$domsize/$proc"
	    mkdir -p  $rep

	    date=`date`
	    echo "_DATE_ = $date" > $rep/stdout
            echo "_MATFILE_ = $matrix" > $rep/stdout
            echo "_NBPROC_ = $proc" >> $rep/stdout
	    echo "_DOMSIZE_ = $domsize" >> $rep/stdout		   
	    
	    echo $matrix - $proc - $domsize
	    mpirun $OPTMACHINE -n $proc $EXEC $OPTGENERAL $OPTPARALL $MTX_TYPE $OPTORDERING -domsize $domsize >> $rep/stdout 2> $rep/stderr

	done
    done
done


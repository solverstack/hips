#!/usr/bin/perl -w



%parser_conf = (
		'matfile'          => ["_MATFILE_ =", 'TXT'],  
		'nproc'            => ["_NBPROC_ =",  'NUM'],        
		'ndom'             => ["_NDOM_ =",    'NUM'],        
		'overlap'          => ["_OVERLAP_ =", 'NUM'],        

		"facto"            => ["SetUp in"], 
		"solve"            => ["Solve in"], 
		"it"               => ["Linear solve converged due to CONVERGED_RTOL iterations"],
		"fill"             => ["fill=", ],
	      );

%parser_conf_regexp = ('NUM'     => ['(_TXT_)([^\d]*)([\d\.]*)', 3],
		       'TXT'     => ['(_TXT_)([ ]*)([-:a-zA-Z0-9]*)', 3]);

$parser_conf_regexp_default = 'NUM';

1;

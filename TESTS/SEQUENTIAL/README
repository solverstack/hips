######################## HOW TO RUN ######################################
Edit the "inputs" file and set the path to your matrix and the parameters you want to use (see the inputs file description in this file)
Then :
./testPHIDAL.ex  <domsize>
where <domsize> is the average number of unknowns you want in each interior domain.
For example:
./testPHIDAL.ex 300 will to build a hierarchical decomposition where each small subdomain has a 
size around 300. The <domsize> value is only a "wish"; depending on the matrix structure the Phidal ordering 
algorithm may decide to build some more bigger subdomain.



######################## INPUTS FILE DESCRIPTION ##########################
../MATRICES/OILPAN   ==> the path to your matrix                  
2		#1#  0=unsymmetric matrix, 1=symmetric pattern, 2=symmetric matrix in RSA (lower triangular part stored)
2		#2#  verbose level: [0, 1, 2, 3, 4] 0 is the weakest level
2		#3#  Normalisation for numerical dropping: 0=no, 1=unsym, 2=symmetric
100		#4#  number of locally consistent level 
1e-8		#5#  eps1(tolerance for outer iteration)
300		#6#  im (krylov subspace size for outer iteration)
100		#7#  maxits (outer fgmres)
1		#8#  number of forward recursion 
0		#9#  number of backward recursion in last recursion level
1		#10# 0=no smoothing   1=smoothing (smoother = fgmres)
5  		#11# maximum number of smoothing iter. in the forward recursion sublevel 
1.0		#12# ratio for maximum smoothing iter. in recursion step > 1
0		#13# backward smooth iter
0.0		#14# backward smooth iter ratio
0.0000   	#15# numerical threshold in ILUT for interior domain
0.0001		#16# numerical threshold in ILUT for the first recursion step
0.0000  	#17# droptol in Schur complements at each recursion level

#1#   this parameter indicates the type of the matrix: 
	0 the matrix is unsymmetric (numerically) 
          and its non zero pattern is not symmetric
        1 the matrix is unsymmetric but has a symmetric pattern
        2 the matrix is symmetric (rsa format)
     When 0 or 1 is used the PHIDAL preconditioner is based upon ILUT
     when 2 is used it is based on ICC (ILUC for the lower triangular part)

#2#   verbose level: this parameter indicates which level of verbose you want in the test:
                 0 is the weakest;  4 is the strongest level of verbose.

#3#   what kind of normalisation do you want for applying the numerical dropping in ILUT 
         0 : no scaling at all;
         1 : unsymmetric scaling  
         2 : symmetric scaling
      NOTE: you cannot use an unsymmetric scaling if your matrix is symmetric (parameter #1# == 2)
           in this case it is automatically replaced by a symmetric scaling for dropping.

#4#   Set the number of consistent connector level you want to use.

#5#   the stopping criterion for the outer fgmres : the algorithm stop when ||r||/||b||
       in norm2 is < to this tolerance

#6#   the restart parameter in fgmres

#7#   the maximum number of iterations you want to use in the outer fgmres

#8#   the number of forward Schur complement recursion you want to use in the preconditioner.
      NOTE  if you set to -1 the recursion is made at each connector level available

#9#   the number of backward recursion you want to use in the last schur complement of the forward recursion.

#10#  if set to 1 then an inner fgmres is applied at each level of the recursion : this obliged 
	to keep the schur complement at each forward recursion step (increase the storage needed)

#11#  the maximum number of iteration you allowed in the forward recursion (#10# must be set to 1)

#12#  the ratio to decrease the number of iteration at each forward recursion step:
      example if #11# = 10 and #12# = 0.2 then there will be 10 iteration using the schur complement 
	of the first forward recursion (fgmres) and 10*0.2= 5 in the second recursion step

#13# and #14# are the same than #11# and #12# but for the backward recursion smoothing;
         allowing iteration in the backward recursion can be costly and almost never profitable
         better set these parameters to 0; 

#15#  the numerical threshold used in the ILUT factorization at step 1 :
      if no forward recursion is set then the whole matrix is factorized using this threshold inside the 
      PHIDAL block pattern;
      if at least one level of forward recursion is used then it corresponds to the threshold applied to 
      the factorization of the interior domains matrices. If you use a small <domsize> parameter then you can 
      set this parameter at a low value (even 0.0) to obtain a good (or exact) schur complement 
      on the interface 

#16#  the numerical threshold applied in the factorization at each forward recursion level. Usually 
	this parameter is greater than #15# to minimize the fill in the factorization of the Schur complement.
#17#  This parameter is used to drop some small numerical entries in the Schur complement in the preconditioner
      It is usually better to keep the most precision as possible in the Schur complement and set this parameter to 0.


################## EXAMPLE of INPUTS FILE ###############################################
We want to solve the matrix OILPAN (rsa format) using a direct factorization in the interior 
of the subdomain grid and iterate only in the schur complement of the interface then the parameter has
to be:

../MATRICES/OILPAN   ==> the path to your matrix                  
2		#1#  0=unsymmetric matrix, 1=symmetric pattern, 2=symmetric matrix in RSA (lower triangular part stored)
2		#2#  verbose level: [0, 1, 2, 3, 4] 0 is the weakest level
2		#3#  Normalisation for numerical dropping: 0=no, 1=unsym, 2=symmetric
100		#4#  number of locally consistent level 
1e-8		#5#  eps1(tolerance for outer iteration)
300		#6#  im (krylov subspace size for outer iteration)
300		#7#  maxits (outer fgmres)
1		#8#  number of forward recursion 
0		#9#  number of backward recursion in last recursion level
1		#10# 0=no smoothing   1=smoothing (smoother = fgmres)
300  		#11# maximum number of smoothing iter. in the forward recursion sublevel 
1.0		#12# ratio for maximum smoothing iter. in recursion step > 1
0		#13# backward smooth iter
0.0		#14# backward smooth iter ratio
0.0000   	#15# numerical threshold in ILUT for interior domain
0.001		#16# numerical threshold in ILUT for the first recursion step
0.0000  	#17# droptol in Schur complements at each recursion level
    

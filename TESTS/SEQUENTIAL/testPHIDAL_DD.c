/* @authors J. GAIDAMOUR, P. HENON */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "math.h"


#include "localdefs.h"
#include "phidal_sequential.h"

/*#define METIS_ORDER

#ifdef METIS_ORDER
#include "metis.h"
#endif*/


#ifndef TYPE_REAL
#error Ne marche qu en DOUBLE
#endif


#define BUFLEN 200


int main(int argc, char *argv[])
{
  /*
   * num    --  working scalar
   * iov    --  overlap
   * unsym  --  symmetric pattern(0), nonsym pattern(1)
   */
  int iov, unsym, ierr,len;

  /* declarations related to Harwell-boeing format for reading the HB
     matri. Second part is related to I/O parameters */
  char guesol[2], title[72], key[8], type[3];
  int nrhs, nc, n, nnz, tmp0,tmp,tmp2,tmp3,job;
  long nnzL;

  /* working array for reading matrix */
  REAL *a,*rhstmp,  res, dnnz_pre, dgprec_nnz;
  chrono_t t1,t2,t3,t4;
  int *ja, *ia,*jb,*ib, *jg, *ig, numflag;
  int nloc,*mapp,*mapptr, maxmp,
    *iwk,node,i1,i2,i,j,k; 

  /** Used to call METIS **/
  int volume;
  int wgtflag;
  int metisoption[8];

  csptr mat, L;
  int ndom;
  int *node2dom;
  REAL *x;
  REAL *b;
  PhidalMatrix *m;
  PhidalHID BL;

  /* working array for symmetrizing matrix */
  REAL *mc;
  int *jc, *ic;

  FILE *fp;

  /** Need these vectors for DBmatrix **/
  int *dom2cblktab = NULL;
  int *treetab = NULL;
  int *rangtab = NULL;

  /* 
   * buf is for read from 'inputs' file, 
   * matrix represents matrix name
   */
  char matrix[BUFLEN];
  char sfile_path[BUFLEN];
  char *substr;

  int *riord;
  int *perm, *iperm;
  int rsa;

  PhidalOptions phidaloptions;
  PhidalOptions* option= &phidaloptions;

  if(argc < 2)
    {
      fprintferr(stderr, "Illegal argument : testPHIDAL_DD.ex <number of domain> \n");
      exit(-1);
    }

  ndom = atoi(argv[1]);


  PhidalOptions_Init(&phidaloptions);

  /** Read parameters from the input file **/
	       /*if(argc == 0)*/
    /** Default: read from file "input" **/
  GENERAL_setpar(NULL, matrix, NULL, &unsym, &rsa, &phidaloptions);   
  

  /************************************************************************************************************/
  /****************************************** READ THE MATRIX *************************************************/
  /************************************************************************************************************/
  
  CSRread(matrix, &n, &nnz, &ia, &ja, &a, NULL, NULL);
  assert(phidaloptions.symmetric == rsa);

  /********************** SUPPRESS THE ZERO IN THE CSR MATRIX *****/
  /*numflag = 1;
  i = nnz;
  nnz = CSR_SuppressZeros(numflag, n, ia, ja, a);
  fprintfv(5, stdout, "Zeros removed from the matrix = %d \n", i-nnz); 
 
  fprintfv(5, stdout, "NNZ = %ld \n", (long) nnz);*/

  /************************ RSA matrix contains only the lower triangular part of the matrix ***************************/
  if(rsa == 1)
    {
      /** Symmetrize the matrix (in rsa we only have the lower triangular part) ****/
      ib = ia;
      jb = ja;
      b = a;
      numflag = 1;
      job = 2;
      PHIDAL_SymmetrizeMatrix(job, numflag, n, ib, jb, b, &ia, &ja, &a);
      nnz = 2*nnz-n;
   
      unsym = 0;
      free(ib);
      free(jb);
      free(b);
      fprintfv(5, stdout, "This matrix is in RSA format \n");
    }



  /*******************************************************************************************************************/
  /*******************************************************************************************************************/
  /*******************************************************************************************************************/
  fprintfv(5, stdout,"Matrix dimension is %d, Number of nonzeros is %d\n",n,nnz);



  /*****************************************************************************************************************/
  /***********************************Construct the symmetric graph G of the matrix ********************************/
  /*****************************************************************************************************************/
  ig = (int *)malloc(sizeof(int)*(n+1));
  jg = (int *)malloc(sizeof(int)*nnz);
  memcpy(ig ,ia, sizeof(int)*(n+1));
  memcpy(jg ,ja, sizeof(int)*nnz);

  
  if(unsym == 1)
    {
      
      ib = ig;
      jb = jg;
      numflag = 1;
      job = 0;
      PHIDAL_SymmetrizeMatrix(job, numflag, n, ib, jb, NULL, &ig, &jg, NULL);
      free(ib);
      free(jb);
    }
  /*****************************************************************************************************************/
  /*****************************************************************************************************************/


  /* Translate matrix into C numbering */
  /*fprintfv(5, stderr, "Convert FORTRAN to C indexing \n");*/
  CSR_Fnum2Cnum(ja, ia, n);
  CSR_Fnum2Cnum(jg, ig, n);
  numflag = 0;
  

  perm = (int *)malloc(sizeof(int)*n);
  iperm = (int *)malloc(sizeof(int)*n);


  /********************************************************/
  /* Compute a  vertex-based partition  using METIS       */
  /* then compute an edge-based partition wich ovelapps   */
  /* on the vertex separator                              */
  /********************************************************/
  node2dom = perm; /** use perm as a temporary working area **/


  if(ndom > 1) 
    {
      wgtflag = 0;
      metisoption[0] = 0;
      METIS_PartGraphVKway(&n, ig, jg, NULL, NULL, &wgtflag, &numflag, &ndom, metisoption, &volume, node2dom);
    }
  else 
    if(ndom == 1) 
      for(i = 0; i < n; i++) 
	node2dom[i] = 0;
  
  /** Transform the vertex-based partition into an edge-based partition **/
  PHIDAL_Partition2OverlappedPartition(0, ndom, n, ig, jg, node2dom, &mapp, &mapptr);
  



  {
    /*********************************************************/
    /*    DISPLAY SOME INFO ON THE OVERLAPPED PARTITION      */
    /*********************************************************/         
    
    int maxdom, mindom;
    REAL avgdom;


    /*fprintfv(5, stdout, "PARTITION: \n");
    for(i=0;i<ndom;i++)
    fprintfv(5, stdout, "Domain %d = %d nodes \n", i, mapptr[i+1]-mapptr[i]);*/
    
    maxdom = 0;
    mindom = mapptr[1]-mapptr[0];
    avgdom = 0.0;
    for(i=0;i<ndom;i++)
      {
	avgdom += mapptr[i+1]-mapptr[i];
	if(mapptr[i+1]-mapptr[i] > maxdom)
	  maxdom = mapptr[i+1]-mapptr[i];
	if(mapptr[i+1]-mapptr[i] < mindom)
	  mindom = mapptr[i+1]-mapptr[i];
      }
    avgdom /= ndom;

    fprintfv(5, stdout, "MAX DOMAIN = %d \n", maxdom);
    fprintfv(5, stdout, "MIN DOMAIN = %d \n", mindom);
    fprintfv(5, stdout, "AVG DOMAIN = %g \n", avgdom);
    /*fprintfv(5, stdout, "IMBALANCE = %g \n", ((float)(maxdom-mindom))*100.0/mindom );*/
  }


  numflag = 0;

    
  /*******************************************************************/
  /*******************************************************************/
  /**                                                               **/
  /** COMPUTE THE HIERARCHICAL DECOMPOSITION                        **/
  /**                                                               **/
  /*******************************************************************/
  /*******************************************************************/
  
  /** Compute the global hierarchical interface decomposition and the associated permutation **/ 
  PhidalHID_Init(&BL);

  t1  = dwalltime(); 
  PHIDAL_HierarchDecomp(phidaloptions.verbose, numflag, n, ig, jg, mapp, mapptr, ndom, &BL, perm, iperm);
  t2  = dwalltime(); 
  fprintfv(5, stdout, " Compute Phidal Hierarchical Decomposition in %g seconds \n\n", t2-t1);



  free(ig);
  free(jg);
 




  /****************************************************************/
  /****************************************************************/
  /**                                                            **/
  /**   REODER THE MATRIX ACCORDING TO THE PHIDAL PERMUTATION    **/
  /**                                                            **/
  /****************************************************************/
  /****************************************************************/
  /*---------------------------------------------------------------------*
    |  Convert the matrix in SparRow and permute it                       |
    *---------------------------------------------------------------------*/
  fprintfv(5, stdout, "Permuting the matrix \n");

  mat = (csptr)malloc(sizeof(struct SparRow));
  initCS(mat, n);
  CSRcs(n, a, ja, ia, mat);
  


  /** DO NOT NEED THESE VECTORS ANYMORE **/
  free(mapp);
  free(mapptr);
  free(a);
  free(ja);
  free(ia);
  
  /*** Permute the matrix according to the phidal ordering ***/
  /*dpermC(mat, perm);*/
  t1  = dwalltime(); 
  CS_Perm(mat, perm);
  t2  = dwalltime(); 
  fprintfv(5, stdout, " Permute the matrix in %g seconds \n\n", t2-t1);


  /*******************************************************************/
  /*******************************************************************/
  /**                                                               **/
  /** REORDER THE INTERIOR OF THE SUBDOMAIN TO REDUCE FILL-IN       **/
  /**                                                               **/
  /*******************************************************************/
  /*******************************************************************/
  fprintfv(5, stderr, "Reordering interior subdomain \n");
  t1  = dwalltime(); 
  PHIDAL_InteriorReorderND(0, mat, &BL, perm, iperm);
  /*PHIDAL_InteriorReorderRCM(1, mat, &BL, perm, iperm);*/
  t2  = dwalltime(); 
  fprintfv(5, stdout, " Reorder the interior subdomain in %g seconds \n\n", t2-t1);
  


  
  /*******************************************************************/
  /*******************************************************************/
  /**                                                               **/
  /** BUILD THE PHIDAL MATRIX                                       **/
  /**                                                               **/
  /*******************************************************************/
  /*******************************************************************/
  m = (PhidalMatrix *)malloc(sizeof(PhidalMatrix));

  t1  = dwalltime(); 
  PHIDAL_MatrixBuild(mat, m, &BL, &phidaloptions);
  t2  = dwalltime(); 
  fprintfv(5, stdout, " Build PhidalMatrix in %g seconds \n\n", t2-t1);

  fprintfv(5, stdout, "Number of NNZ in A = %ld \n", PhidalMatrix_NNZ(m));



  /********* Test multiply **********/
  x = (REAL *)malloc(sizeof(REAL)*mat->n);
  b = (REAL *)malloc(sizeof(REAL)*mat->n);

  for(i=0;i<mat->n;i++)
    x[i] = 1.0;


  {
    PhidalPrec P;
    REAL *r;

    bzero(b, sizeof(REAL)*mat->n);
    PHIDAL_MatVec(m, &BL, x, b);


    t1  = dwalltime();    

    PHIDAL_Precond(m, &P, &BL,  &phidaloptions);

    t2  = dwalltime(); 
    fprintfv(5, stdout, " PHIDAL_Precond in %g seconds \n\n", t2-t1);
    
    PhidalPrec_Info(&P);

    fprintfv(5, stdout, "Factor L row density = %g \n", PhidalMatrix_RowDensity(P.L));
    fprintfv(5, stdout, "Factor U row density = %g \n", PhidalMatrix_RowDensity(P.U));
    fprintfv(5, stdout, "PhidalPrec NNZ = %ld \n", PhidalPrec_NNZ(&P));
    fprintfv(5, stdout, "Fill Ratio of Preconditioner = %g \n\n", ((REAL)PhidalPrec_NNZ(&P))/((REAL)PhidalMatrix_NNZ(m)));
    /*fprintfv(5, stdout, "Fill Ratio for symmetrized matrix = %g \n\n", ((REAL)2*PhidalPrec_NNZ(&P)-mat->n)/((REAL)2*PhidalMatrix_NNZ(m)-mat->n));*/
    
    
    bzero(b, sizeof(REAL)*mat->n);
    PHIDAL_MatVec(m, &BL , x, b);
    
    bzero(x, sizeof(REAL)*mat->n);
    t1  = dwalltime();
    PHIDAL_Solve(m, &P, &BL, &phidaloptions, b, x, NULL);
    t2  = dwalltime(); 
    fprintfv(5, stdout, "\n Solve in %g seconds \n", t2-t1);
    PhidalPrec_Clean(&P);
    
    r = (REAL *)malloc(sizeof(REAL)*m->dim1);
    memcpy(r, b, sizeof(REAL)*m->dim1);
    PHIDAL_MatVecSub(m, &BL, x, r);
    /*matvecz(mat, x, r, r);*/
    fprintfv(5, stdout, "Relative residual norm = %g \n", norm2(r, m->dim1)/norm2(b, m->dim1));
    free(r);

  }

  
  free(x);
  free(b);
  

  PhidalMatrix_Clean(m);
  free(m);
 
  free(perm);
  free(iperm);

  PhidalOptions_Clean(&phidaloptions);
  cleanCS(mat);
  free(mat);
  
  PhidalHID_Clean(&BL); 
  fprintfv(5, stdout, "END \n");

}




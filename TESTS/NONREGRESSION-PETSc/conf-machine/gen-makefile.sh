#!/bin/bash

shopt -s extglob

function loop
{
    local i
    local var
    declare -i n    

    if (( $2 <= $3 ))
	then
	(( n=$2+1 ))
	
	var=`ls -d *-* | head -n $2 | tail -n 1`	
	for i in `ls $var | grep ".inc$"`
	do
	  name="$4"`echo $i | cut -d'.' -f1`"-"
	  files="$5 $var/$i"
	  loop $1 $n $3 "$name" "$files"
	done
    else
	gen $1 `echo $4 | sed -e 's/-$//'` "$5"
    fi
}

function gen
{
    machine=`echo $1| cut -d'.' -f1`
    name="$2"
    file1="$1"
    files="$3"

    mkdir ../$machine/$name 
    cat $files $file1 > ../$machine/$name/makefile.inc
}

cd generic

if [ $# -eq 1 ]
then 
    if [ -e $1".inc" ]
    then
	machinelist="$1".inc
    else
	echo usage : $0 [machine]
	exit 1
    fi
else 
    machinelist=`ls *.inc`
fi

for m in $machinelist
do
  machine=`echo $m| cut -d'.' -f1`
  mkdir ../$machine
  loop $m 1 `ls -d *-* | wc -l` # TODO : attention svn-commit.tmp

  def="double-int"

#   (cd  ../$machine/; 
#       ln -s scotch-optim-real-$def    scotch-double;
#       ln -s scotch-optim-complex-$def scotch-complex;

#       ln -s metis-optim-real-$def     metis-double;
#       ln -s metis-optim-complex-$def  metis-complex;
#       )
done

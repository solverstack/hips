
###
###  Compiler
###

ARCH       = -DLINUX

CC	   = gcc       # C compiler 
MPICC      = mpicc
FC         = gfortran  # Fortran compiler 
MPIFC      = mpif90
LD	   = $(FC)     # Linker
MPILD      = $(MPIFC)

CFLAGS	   =           # Additional C compiler flags
FFLAGS	   =           # Additional Fortran compiler flags
LFLAGS     =           # Additional linker flags

COPTFLAGS2 = -O3       # Optimization flags
FOPTFLAGS2 = -O3       # 

###
###  Library
###

IBLAS      =           # BLAS include path
#LBLAS      = -lblas    # BLAS linker flags
LBLAS      = -L$(BLAS_HOME) -lgoto    # BLAS linker flags


IMPI       =           # Additional MPI include path
LMPI       =           # Additional MPI linker flags

##   METIS_DIR : path to METIS
METIS_DIR  = $(HOME)/travail/lib/metis-4.0
IMETIS     = -I$(METIS_DIR)/Lib
LMETIS2    = -L$(METIS_DIR) -lmetis

##   SCOTCH_DIR : path to SCOTCH
SCOTCH_DIR = $(SCOTCH_HOME)
ISCOTCH    = -I$(SCOTCH_DIR)/include
LSCOTCH2   = -L$(SCOTCH_DIR)/lib -lscotch -lscotcherr

###
###  Misc
###

MAKE	   = make
AR	   = ar
ARFLAGS	   = -crs
LN	   = ln
CP	   = cp

##   PETSC
PETSC_DIR  = $(HOME)/lib/petsc/
PETSC_ARCH = $(PETSC_DEBUG)-$(PETSC_TYPE)

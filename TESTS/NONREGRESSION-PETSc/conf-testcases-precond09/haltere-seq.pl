#!/usr/bin/perl -w

use strict;
use Submit::Submit;

require 'global.pl';

our %global;

my %configuration = (
		     'name'          =>     'haltere-seq',
		     'descr'         =>     'haltere-seq pour PRECOND09',
		     
		     'prog'          =>     'testPETSc-LOAD.ex',
		     'compilversion' =>     'scotch-optim-complex-double-int',
		     'nbproc'        =>     '1',

		     'matfile'       =>     'Haltere.mm',
                     'sym'           =>     '1',

		     'param'         =>     '500 800 1000 2000 3000 5000 8000 10000 15000', # domsize
		     'overlap'       =>     '0 2',
		     
		     'prec'          =>     '1e-7',
		     'maxit'         =>     '1000',
		     'krylov'        =>     '1000',
		     
                     'ordering'      =>     'hips_matrix_ordering',
                     'monitor'       =>     'ksp_monitor',
                     'lu'            =>     'mumps',
                     'type'          =>     'basic',
                     'preload'       =>     'off',

#		     'verbose'       =>     '5',
		     'driver'        =>     '',

		     'memory'        =>     '0000',
		     'timelimit'     =>     '120',

		     		     'side'     =>     'ksp_right_pc');


my @sort = ('matfile', 'prec', 'param', 'overlap', 'nbproc');
my $sort_option = 2;

Submit::generateTestCases(\%global, \%configuration, \@sort, $sort_option);

#IMPORTANT: If you run with, for example, 3 blocks on 1 processor or 3
#blocks on 3 processors you will get a different convergence rate due
#to the default option of -pc_asm_type restrict. Use -pc_asm_type
#basic to use the standard ASM.

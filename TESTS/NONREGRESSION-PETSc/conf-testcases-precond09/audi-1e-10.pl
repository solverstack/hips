#!/usr/bin/perl -w

use strict;
use Submit::Submit;

require 'global.pl';

our %global;

my %configuration = (
		     'name'          =>     'audi-1e-10',
		     'descr'         =>     'audi-1e-10  pour PRECOND09',
		     
		     'prog'          =>     'testPETSc-LOAD.ex',
		     'compilversion' =>     'scotch-optim-real-double-int',
		     'nbproc'        =>     '1',

		     'matfile'       =>     'audi.rsa',
                     'sym'           =>     '1',

		     'param'         =>     '4000', # domsize
		     'overlap'       =>     '0 2',
		     
		     'prec'          =>     '1e-10',
		     'maxit'         =>     '500 1000',
		     'krylov'        =>     '1000',
		     
                     'ordering'      =>     'hips_matrix_ordering',
                     'monitor'       =>     'ksp_monitor',
                     'lu'            =>     'mumps',
                     'type'          =>     'basic',
                     'preload'       =>     'off',

#		     'verbose'       =>     '5',
		     'driver'        =>     '',

		     'memory'        =>     '0000',
		     'timelimit'     =>     '240',

		     'side'     =>     'ksp_right_pc');


my @sort = ('matfile', 'prec', 'param', 'overlap', 'nbproc');
my $sort_option = 2;

Submit::generateTestCases(\%global, \%configuration, \@sort, $sort_option);

#IMPORTANT: If you run with, for example, 3 blocks on 1 processor or 3
#blocks on 3 processors you will get a different convergence rate due
#to the default option of -pc_asm_type restrict. Use -pc_asm_type
#basic to use the standard ASM.

#!/usr/bin/perl -w

use strict;
use Submit::Submit;

require 'global.pl';

our %global;

my %configuration = (
		     'name'          =>     'matr5-mumps',
		     'descr'         =>     'real-unsym',
		     
		     'prog'          =>     'testPETSc-LOAD.ex',
		     'compilversion' =>     'scotch-optim-real-double-int',
		     'nbproc'        =>     '8',

		     'matfile'       =>     'matr5.rua', 
                     'sym'           =>     '0',

		     'param'         =>     '500 1000 2000 4000 8000', # domsize
		     'overlap'       =>     '0',
		     
		     'prec'          =>     '1e-7',
		     'maxit'         =>     '2000',
		     'krylov'        =>     '2000',
		     
                     'ordering'      =>     'hips_matrix_ordering',
                     'monitor'       =>     'ksp_monitor',
                     'lu'            =>     'rien',
                     'type'          =>     'basic',
                     'preload'       =>     'off',

#		     'verbose'       =>     '5',
		     'driver'        =>     '',

		     'memory'        =>     '0000',
		     'timelimit'     =>     '60',
                     'side'          =>     'ksp_right_pc'
		     );


my @sort = ('matfile', 'prec', 'param', 'overlap', 'nbproc');
my $sort_option = 2;

Submit::generateTestCases(\%global, \%configuration, \@sort, $sort_option);

#IMPORTANT: If you run with, for example, 3 blocks on 1 processor or 3
#blocks on 3 processors you will get a different convergence rate due
#to the default option of -pc_asm_type restrict. Use -pc_asm_type
#basic to use the standard ASM.

#!/usr/bin/perl -w

use strict;
use Submit::Submit;

require 'global.pl';

our %global;

my %configuration = (
		     'name'          =>     '----',
		     'descr'         =>     '',
		     
		     'prog'          =>     'testPETSc-LOAD.ex',
		     'compilversion' =>     '----',

		     'nbproc'        =>     '8',

		     'matfile'       =>     '----',
                     'sym'           =>     '----',

		     'param'         =>     '500 1000 2000 4000 8000', # domsize
		     'overlap'       =>     '0 1 2',
		     
		     'prec'          =>     '1e-7',
		     'maxit'         =>     '2000',
		     'krylov'        =>     '2000',
		     
                     'ordering'      =>     'hips_matrix_ordering',
                     'monitor'       =>     'ksp_monitor',
                     'lu'            =>     'mumps',
                     'type'          =>     'basic',
                     'preload'       =>     'off',
                     'side'          =>     'ksp_right_pc', #default de petsc=left

#		     'verbose'       =>     '5',
		     'driver'        =>     '',

		     'memory'        =>     '0000',
		     'timelimit'     =>     '120',

		     );

my @sort = ('matfile', 'prec', 'param', 'overlap', 'nbproc');
my $sort_option = 2;

$configuration{'name'}          = 'real-sym';
$configuration{'compilversion'} = 'scotch-optim-real-double-int';
$configuration{'matfile'}       = 'NICE20.mm NICE25.mm inline.rsa audi.rsa';
$configuration{'sym'}           = '1';
Submit::generateTestCases(\%global, \%configuration, \@sort, $sort_option);

$configuration{'name'}          = 'real-unsym';
$configuration{'compilversion'} = 'scotch-optim-real-double-int';
$configuration{'matfile'}       = 'matr5.rua matr6.rua ultrasound80.rua';
$configuration{'sym'}           = '0';
Submit::generateTestCases(\%global, \%configuration, \@sort, $sort_option);

$configuration{'name'}          = 'real-unsym-mchlnf';
$configuration{'compilversion'} = 'scotch-optim-real-double-int';
$configuration{'matfile'}       = 'mchlnf.rua';
$configuration{'sym'}           = '0';
my $tmp=$configuration{'param'};
$configuration{'param'}           = '500 1000 2000 4000';
Submit::generateTestCases(\%global, \%configuration, \@sort, $sort_option);
$configuration{'param'} = $tmp;

$configuration{'name'}          = 'complex-sym';
$configuration{'compilversion'} = 'scotch-optim-complex-double-int';
$configuration{'matfile'}       = 'Haltere.mm Amande.mm';
$configuration{'sym'}           = '1';
Submit::generateTestCases(\%global, \%configuration, \@sort, $sort_option);

#IMPORTANT: If you run with, for example, 3 blocks on 1 processor or 3
#blocks on 3 processors you will get a different convergence rate due
#to the default option of -pc_asm_type restrict. Use -pc_asm_type
#basic to use the standard ASM.

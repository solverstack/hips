#!/usr/bin/perl -w



%parser_conf = (
		'name'            => ["_NAME_ =", 'TXT'], 
		'descr'           => ["_DESCR_ =", 'TXT'], 
		
		'prog'            => ["_PROG_ =", 'TXT'],  
		'compilversion'   => ["_COMPILVERSION_ =", 'TXT'], 
		'nbproc'          => ["_NBPROC_ =", 'NUM'],        

		'matfile'  => ["_MATFILE_ =", 'TXT'],  #todo : rajouter autorisation du "."
		'sym'      => ["_SYM_ =", 'TXT'],  

		'param'    => ["_PARAM_ =", 'NUM'],         
		'overlap'  => ["_OVERLAP_ =", 'NUM'],         

		'prec'     => ["_PREC_ =", 'TXT'],   
		'maxit'    => ["_MAXIT_ ="],   
		'krylov'   => ["_KRYLOV_ ="],   

#                'ordering' =>     'hips_matrix_ordering',
                'monitor'  => ["_MONITOR_ =", 'TXT'],
#                'lu'       =>     'mumps',
#                'type'     =>     'basic',
#                'preload'  =>     'off',

#		'verbose'  => ["_VERBOSE_ ="],   
                'driver'          => ["_DRIVER_ =", 'TXT'],        
		
		
		'memory'   => ["_MEMORY_ =", 'TXT'],   
		'timelimit'=> ["_TIMELIMIT_ =", 'TXT'],   

		'launched' => ["_LAUNCHED_ =", 'NUM'],   

##
                'ndom'     => ["_NDOM_ =", 'NUM'],

		"facto"            => ["SetUp in"], 
		"solve"            => ["Solve in "], 
#		"itouter"          => [", Iterations"],
		"itinner"          => [", Iterations"], #en fait, outer
#		"itinner"          => ["inner iterations ="],
#		"itouter"          => ["Number of outer iterations "],

		"nnzA"             => ["total    : nnzA=", 'NUM'],
		"nnzP"             => ["total    :.* nnzP=", 'NUM'],
		"ratio"             => ["total    :.* fill=", 'NUM'],


#		"nnzA"             => ["Info..1.* nnzA = ", 'NUM'], # 1er proc
#		"nnzP"             => ["Info..1.* nnzP = ", 'NUM'],
#		"peak"             => ["Info..1.* peak = ", 'NUM'],

#		"ratio"            => ["Info..total. ratio = ", 'NUM'],
#		"peakratio"        => ["Info..total.* peak ratio = ", 'NUM'],

#		"oldfill"          => ["HIPS Fill Ratio of Preconditioner ="],
	      );

%parser_conf_regexp = ('NUM'     => ['(_TXT_)([^\d]*)([\d\.]*)', 3],
		       'TXT'     => ['(_TXT_)([ ]*)([:a-zA-Z0-9-_\.]*)', 3]);

$parser_conf_regexp_default = 'NUM';

1;

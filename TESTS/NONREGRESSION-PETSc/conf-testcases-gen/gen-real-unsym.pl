#!/usr/bin/perl -w

use strict;
use Submit::Submit;

require 'global.pl';

our %global;

my %configuration = (
		     'name'          =>     'gen-real-unsym',
		     'descr'         =>     'gen-real-unsym',
		     
		     'prog'          =>     'testPETSc-Mtx-SAVE.ex',
		     'compilversion' =>     'scotch-optim-real-double-int',
		     'nbproc'        =>     '1',

		     'matfile'       =>     'mchlnf.rua matr5.rua matr6.rua ultrasound80.rua', #TODO matr6.rua+ solultrasound80  
                     'sym'           =>     '0',

		     'param'         =>     '', # domsize
		     'overlap'       =>     '',
		     
		     'prec'          =>     '',
		     'maxit'         =>     '',
		     'krylov'        =>     '',
		     
                     'ordering'      =>     '',
                     'monitor'       =>     '',
                     'lu'            =>     '',
                     'type'          =>     '',
                     'preload'       =>     'off',

#		     'verbose'       =>     '5',
		     'driver'        =>     '',

		     'memory'        =>     '0000',
		     'timelimit'     =>     '10',

		     );


my @sort = ('matfile', 'prec', 'param', 'overlap', 'nbproc');
my $sort_option = 2;

Submit::generateTestCases(\%global, \%configuration, \@sort, $sort_option);

SELECT 
input, stdout, stderr, 
param, 

round(facto,2) as facto, 
round(solve,2) as solve, 
round(facto + solve,2) as total, 

itouter, maxit,

round(fill,2) as ratio, 

nnzA, nnzP, 
prec, ndom

FROM results

WHERE 
matfile  = '_ARG1_'
AND
prec     = '_ARG2_'
AND
overlap  = '_ARG3_'
AND
nbproc   = '_ARG4_'
AND
overlap  = '_ARG5_'
AND
prec     = '_ARG6_'

ORDER BY 
Cast(param as int), 
facto, solve
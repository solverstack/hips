/* @authors J. GAIDAMOUR, P. HENON */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

/** To run the scalar preconditioner **/
/*#define PHIDAL_SCAL      */

/*#define REGULAR_DOMAIN */ /** to get regular domain by fixing the number
			    of domains **/
/*#define REGULAR_SIZE_DOMAIN*/
/** to get regular domain by fixing the
    size of the domains **/

#ifdef REGULAR_SIZE_DOMAIN
#undef REGULAR_DOMAIN
#endif

/* #define MEMTRACE */
#ifdef MEMTRACE
#include <trace.h>
#endif

#include "type.h"
#include "io.h"
#include "localdefs.h"
#include "block.h" /* FLOAT */
#include "db_struct.h"
#include "phidal_sequential.h"

#include "base.h"

#define BUFLEN 200

int main(int argc, char *argv[])
{
  /* int UN = 1; */
  int i;
    
  /* declarations related to Harwell-boeing format for reading the HB
     matrix. Second part is related to I/O parameters */
  int job;
  /* working array for reading matrix */
  int *mapp,*mapptr; 
  int ndom;
  int domsize;
  PhidalMatrix *m;
  PhidalHID BL;

  /* MATRIX */
  int n, nnz;
  COEF *a;
  int *ja, *ia;
  int numflag;  /* C or Fortran */
  
  int *jg, *ig;


  /* Symmetrize the matrix */
  int *ib, *jb;
  
  int *iau;
  int cube, nc;

  /* 
   * buf is for read from 'inputs' file, 
   * matrix represents matrix name
   */
  int unsym; /* unsym  --  symmetric pattern(0), nonsym pattern(1) */
  char matrix[BUFLEN];
  char sfile_path[BUFLEN];
  int rsa;

  int *perm, *iperm;

  chrono_t t1,t2/* ,ttotal */;
  double nnzA, nnzL;
  

  /* Matrices */
  SymbolMatrix* symbmtx;
  csptr mat;

  /* Vectors */
  COEF *x, *b;
  /*   double ro; */
  COEF* r;

  PhidalOptions phidaloptions;
  PhidalOptions* option= &phidaloptions;

  PhidalPrec Ps;
  DBPrec P;

  if(argc < 2)
    {
      fprintferr(stderr, "Illegal argument : testPHIDAL.ex <domain size (in number of node)> \n");
      exit(-1);
    }
#ifndef REGULAR_DOMAIN
  domsize = atoi(argv[1]);
#else
  ndom = atoi(argv[1]);
#endif

#ifdef MEMTRACE
  trace_init();
#endif

  PhidalOptions_Init(&phidaloptions);
  
  /** Read parameters from the input file **/
  GRID_setpar(NULL, &nc, &cube, sfile_path, &unsym, &rsa, &phidaloptions);

  /************************************************************************************************************/
  /****************************************** READ THE MATRIX *************************************************/
  /************************************************************************************************************/
  /*CSRread(matrix, &n, &nnz, &ia, &ja, &a, NULL, NULL);*/

  if(cube == 0)
    n = nc*nc;
  else
    n = nc*nc*nc;


  /*---------------------------------------------------------------------*
   |  Section 1.5: Generate the coefficient matrix                       |
   *---------------------------------------------------------------------*/
   ia = (int *)malloc((n+1)*sizeof(int));
   iau = (int *)malloc((n+1)*sizeof(int));
   if(cube == 0)
      nnz = 10*(nc*nc+1);        
   else
     nnz = 10*(nc*nc*nc+1);
      
   ja = (int *)malloc(nnz*sizeof(int));
   a = (COEF *)malloc(nnz*sizeof(COEF));

   /* create local matrix */
   /*gen5loc(&nx, &ny, &nz, &nloc,dm->node,a,ja,ia,stencil);*/
   
   if(cube == 0)
     {
       int un=1;
       printf("Creating 5 point grid :%d x %d \n", nc, nc);
       gen5pt(&nc, &nc, &un, a, ja, ia, iau);
     }
   else
     {
       printf("Creating 7 point 3D cube :%d x %d x %d \n", nc, nc, nc);
       gen5pt(&nc, &nc, &nc, a, ja, ia, iau);
     }
   free(iau);
   
   nnz = ia[n]-ia[0];

   rsa = 1;

  assert(phidaloptions.symmetric == rsa);

  /* Translate matrix into C numbering */
  CSR_Fnum2Cnum(ja, ia, n);
  numflag = 0;

  /*dumpcsr(stderr, a, ja, ia, n);*/


  /********************** SUPPRESS THE ZERO IN THE CSR MATRIX *****/
#ifdef SUPPRESS_ZERO
  i = nnz;
  nnz = CSR_SuppressZeros(numflag, n, ia, ja, a);
  fprintf(stdout, "Zeros removed from the matrix = %d \n", i-nnz); 
 
  if ((rsa == 0) && (unsym == 0) && (i-nnz > 0))
    unsym = 1;
#endif
  /*   fprintf(stdout, "NNZ = %ld \n", (long) nnz); */
  /****************************************************************/

  nnzA = nnz; /* apres symetrize matrix = (ia[n]+n)/2.0; */
  fprintf(stdout, "Number of NNZ in A (CSR) = %g \n", nnzA);

  fprintf(stdout, "Needed memory for A : %d Mo (only coeftab storage)\n", (int)((nnzA*sizeof(COEF))/1048576));

  /************************ RSA matrix contains only the lower triangular part of the matrix ***************************/
  /** Symmetrize the matrix (in rsa we only have the lower triangular part) ****/
  /** Function gen5pt gives the whole matrix even if it is symmetric,
      so we do not need this ***/
  /*if(rsa == 1)
    {
    ib = ia;
    jb = ja;
    b = a;
    job = 2;
    PHIDAL_SymmetrizeMatrix(job, numflag, n, ib, jb, b, &ia, &ja, &a);
    nnz = 2*nnz-n;
    unsym = 0;
    free(ib);
    free(jb);
    free(b);
    }*/

  /*******************************************************************************************************************/
  /*******************************************************************************************************************/
  /*******************************************************************************************************************/
  fprintf(stdout,"Matrix dimension is %d, Number of nonzeros is %d\n",n,nnz);

  /*****************************************************************************************************************/
  /***********************************Construct the symmetric graph G of the matrix ********************************/
  /*****************************************************************************************************************/
  ig = (int *)malloc(sizeof(int)*(n+1));
  jg = (int *)malloc(sizeof(int)*nnz);
  memcpy(ig ,ia, sizeof(int)*(n+1));
  memcpy(jg ,ja, sizeof(int)*nnz);
  
  if(unsym == 1) 
    {
      ib = ig;
      jb = jg;
      job = 0;
      PHIDAL_SymmetrizeMatrix(job, numflag, n, ib, jb, NULL, &ig, &jg, NULL);
      free(ib);
      free(jb);
    }

  /*****************************************************************************************************************/
  /*****************************************************************************************************************/

  /************************************************************************************************************/
  /* Compute the reordering to minimize fill-in using METIS-4.0 ***********************************************/
  /************************************************************************************************************/
 
  perm = (int *)malloc(sizeof(int)*n);
  iperm = (int *)malloc(sizeof(int)*n);

  /** Delete the self edge in the graph (METIS_NodeND need that)**/
  PHIDAL_CsrDelDiag(numflag, n, ig, jg);

  /**** Compute the overlapped partition from the matrix reordering   *****/
  t1  = dwalltime(); 
  /*PHIDAL_Perm2OverlappedDomains(ndom, n, ig, jg, &mapptr, &mapp, perm, iperm);*/

#ifndef REGULAR_DOMAIN

#ifdef REGULAR_SIZE_DOMAIN
  if(cube == 1)
    PHIDAL_GridRegularSizeDomains(domsize, nc, nc, nc, n, ig, jg, &ndom, &mapptr, &mapp, perm, iperm);
  else
    PHIDAL_GridRegularSizeDomains(domsize, nc, nc, 1, n, ig, jg, &ndom, &mapptr, &mapp, perm, iperm);
#else
  
  PHIDAL_Perm2SizedDomains(domsize, n, ig, jg, &ndom, &mapptr, &mapp, perm, iperm);
  

#endif

#else
  /*assert(0);*/

  if(cube == 1)
    PHIDAL_GridRegularDomains(ndom, nc, nc, nc, n, ig, jg, &mapptr, &mapp, perm, iperm);
  else
    PHIDAL_GridRegularDomains(ndom, nc, nc, 1, n, ig, jg, &mapptr, &mapp, perm, iperm);
#endif

  t2  = dwalltime(); 

  fprintf(stdout, "Compute the grid of subdomain in %g \n", t2-t1);
#ifndef REGULAR_DOMAIN
  fprintf(stdout, "DOMSIZE = %d \n", domsize);
#endif
  fprintf(stdout, "Found %d domains \n", ndom);


  {
    /*********************************************************/
    /*    DISPLAY SOME INFO ON THE OVERLAPPED PARTITION      */
    /*********************************************************/         
    
    int maxdom, mindom;
    double avgdom;


    /* fprintf(stdout, "PARTITION: \n");
    for(i=0;i<ndom;i++)
    fprintf(stdout, "Domain %d = %d nodes \n", i, mapptr[i+1]-mapptr[i]);*/
    
    maxdom = 0;
    mindom = mapptr[1]-mapptr[0];
    avgdom = 0.0;
    for(i=0;i<ndom;i++)
      {
	avgdom += mapptr[i+1]-mapptr[i];
	if(mapptr[i+1]-mapptr[i] > maxdom)
	  maxdom = mapptr[i+1]-mapptr[i];
	if(mapptr[i+1]-mapptr[i] < mindom)
	  mindom = mapptr[i+1]-mapptr[i];
      }
    avgdom /= ndom;

    fprintf(stdout, "MAX DOMAIN = %d \n", maxdom);
    fprintf(stdout, "MIN DOMAIN = %d \n", mindom);
    fprintf(stdout, "AVG DOMAIN = %g \n", avgdom);
    /*fprintf(stdout, "IMBALANCE = %g \n", ((float)(maxdom-mindom))*100.0/mindom );*/
  }

  /*******************************************************************/
  /*******************************************************************/
  /**                                                               **/
  /** COMPUTE THE HIERARCHICAL DECOMPOSITION AND LOCAL DATA         **/
  /**                                                               **/
  /*******************************************************************/
  /*******************************************************************/
  
  /** Compute the global hierarchical interface decomposition and the associated permutation **/ 
  PhidalHID_Init(&BL);

  /*#define EXPORT*/

#ifdef EXPORT
  {
    FILE * fd = fopen("meshPartition.mesh", "w"); assert(fd != NULL);
    Draw_2Dmesh(fd, 0, nc, n, ig, jg, iperm, ndom, mapp, mapptr, &BL);
    fclose(fd);
  }
#endif


  t1  = dwalltime(); 
  PHIDAL_HierarchDecomp(phidaloptions.verbose, numflag, n, ig, jg, mapp, mapptr, ndom, &BL, perm, iperm);
  t2  = dwalltime(); 
  fprintf(stdout, " Compute Phidal Hierarchical Decomposition in %g seconds \n\n", t2-t1);
  if(option->verbose >= 2)
    {
      fprintfv(2, stdout, "\n Hierarchical Interface Decomposition \n");
      HID_Info(stdout, &BL);
    }
  
  /*HID_BuildCoarseLevel(0, &BL, perm, iperm);*/

  if(option->verbose >= 2)
    {
      fprintfv(2, stdout, "\n Hierarchical Interface Decomposition INFO AFTER COARSE LEVEL \n");
      HID_Info(stdout, &BL);
    }

#ifdef EXPORT
  {
    FILE * fd = fopen("meshLevel.mesh", "w"); assert(fd != NULL);
    Draw_2Dmesh(fd, 1, nc, n, ig, jg, iperm, ndom, mapp, mapptr, &BL);
    fclose(fd);
  }
  {
    FILE * fd = fopen("meshConnector.mesh", "w"); assert(fd != NULL);
    Draw_2Dmesh(fd, 2, nc, n, ig, jg, iperm, ndom, mapp, mapptr, &BL);
    fclose(fd);
  }

#endif


#ifndef PHIDAL_SCAL
  fprintf(stdout, "Build Symbolic Matrix \n");
  symbmtx = (SymbolMatrix*)malloc(sizeof(SymbolMatrix));
  if(phidaloptions.schur_method != 1)
    HIPS_SymbolMatrix(phidaloptions.verbose, numflag, 0.05, phidaloptions.locally_nbr, n,  ig,  jg, 
		      &BL, symbmtx, perm, iperm);
  else
    HIPS_SymbolMatrix(phidaloptions.verbose, numflag, 0.05, BL.nlevel, n,  ig,  jg, 
		      &BL, symbmtx, perm, iperm);
#endif


  free(mapp);
  free(mapptr);
  free(ig);
  free(jg);
 
  /*
  CSR -> SparRow
  */
  fprintf(stdout, "Permuting the matrix \n");

  mat = (csptr)malloc(sizeof(struct SparRow));
  
  if(initCS(mat, n)) 
    {
      printf(" ERROR SETTING UP bmat IN initCS \n") ;
      exit(0);
    }
  CSRcs(n, a, ja, ia, mat);
  
  /** DO NOT NEED THESE VECTORS ANYMORE **/
  free(a);
  free(ja);
  free(ia);
   
 
  /*** Permute the matrix according to the phidal ordering ***/
  t1  = dwalltime(); 
  CS_Perm(mat, perm);/* ! */
  t2  = dwalltime(); 
  fprintf(stdout, " Permute the matrix in %g seconds \n", t2-t1);

#define SOL_UN
  x = (COEF *)malloc(sizeof(COEF)*n);
  b = (COEF *)malloc(sizeof(COEF)*n); 
  for(i=0;i<n;i++) 
    x[i] = 1.0;
#ifndef SOL_UN  
  /**OIMBE Valable en 2D **/
  for(i=0;i<n;i++) 
    x[i] = nc;
  /** Condition limite **/
  for(i=0;i<nc;i++)
    x[i] = 0;

  for(i=0;i<nc;i++)
    {
      x[i*nc] = 0;
      x[i*nc + nc-1] = 0;
    }

  for(i=nc*(nc-1);i<nc*nc;i++)
    x[i] = 0;

  for(i=0;i<n;i++)
    b[i] = x[perm[i]];
  memcpy(x, b, sizeof(COEF)*n);
#endif

  free(perm);
  free(iperm); /**/
  
  t1  = dwalltime(); 
  ascend_column_reorder(mat);/* todo : utile ? */
  t2  = dwalltime(); 
  fprintf(stdout, " Ascend_column_reorder in %g seconds \n\n", t2-t1);

  /* fprintf(stdout, "Number of NNZ in A (SparRow) = %ld \n", CSnnz(mat)); */

  /************************************************************************************************************/
  /************************************************************************************************************/

  fprintf(stdout, "Build PhidalMatrix\n"); 
  t1  = dwalltime(); 
  m  = (PhidalMatrix *)malloc(sizeof(PhidalMatrix));
  PHIDAL_MatrixBuild(mat, m, &BL, &phidaloptions); /* MPROTECT */
  t2  = dwalltime(); 
  cleanCS(mat);
  free(mat);
  assert(m->dim1 == n);

  fprintf(stdout, " Build PhidalMatrix in %g seconds \n\n", t2-t1);
  /* fprintf(stdout, " Number of NNZ in A (Phidal) = %ld \n", PhidalMatrix_NNZ(m)); */

  assert(phidaloptions.forwardlev == 0 || phidaloptions.forwardlev == 1);

  /*phidaloptions.krylov_method = 1; */
  phidaloptions.krylov_method = 0; /**Force le fgmres **/
  /* **** */
  


#ifndef PHIDAL_SCAL
  DBPrec_Init(&P);  
  P.info = (PrecInfo *)malloc(sizeof(PrecInfo));
  PrecInfo_Init(P.info);
  PrecInfo_SetNNZA(P.info, (INTL)PhidalMatrix_NNZ(m));

  DBMATRIX_Precond(m, &P, symbmtx, &BL, &phidaloptions);
#else
  Ps.info = (PrecInfo *)malloc(sizeof(PrecInfo));
  PrecInfo_Init(Ps.info);
  PrecInfo_SetNNZA(Ps.info, (INTL)PhidalMatrix_NNZ(m));

  PHIDAL_Precond(m, &Ps, &BL,  &phidaloptions);
#endif
  /*   assert(P.E != NULL); /\* ? *\/ */

#ifndef PHIDAL_SCAL 
  /************************************************************************************************************/
  nnzL = DBPrec_NNZ(&P);
  fprintf(stdout, " OLD Number of NNZ in Preconditioner = %g\n", nnzL);
  fprintf(stdout, " OLD Fill Ratio of Preconditioner = %g\n\n", nnzL / nnzA);
  fprintfv(3, stdout, "NNZA = %ld, NNZPrec = %ld NNZPeak = %ld \n", (long)P.info->nnzA, (long)P.info->nnzP, (long)P.info->peak);
  fprintfv(3, stdout, "(struct info) Fill rat  = %g peak = %g \n", ((REAL)P.info->nnzP)/P.info->nnzA, ((REAL)P.info->peak)/P.info->nnzA);

  /* DBPrec_Info(&P); */
#else
  PhidalPrec_Info(&Ps);
  fprintfv(5, stdout, "OLD PhidalPrec NNZ = %ld \n", PhidalPrec_NNZ(&Ps)); /*TODO : variable !*/
  fprintfv(5, stdout, "OLD Fill Ratio of Preconditioner = %g \n\n", ((REAL)PhidalPrec_NNZ(&Ps))/((REAL)PhidalMatrix_NNZ(m)));
  /*fprintfv(5, stdout, "Fill Ratio for symmetrized matrix = %g \n\n", ((REAL)2*PhidalPrec_NNZ(&P)-mat->n)/((REAL)2*PhidalMatrix_NNZ(m)-mat->n));*/
  fprintfv(3, stdout, "NNZA = %ld, NNZPrec = %ld NNZPeak = %ld \n", (long)Ps.info->nnzA, (long)Ps.info->nnzP, (long)Ps.info->peak);
  fprintfv(3, stdout, "(struct info) Fill rat  = %g peak = %g \n", ((REAL)Ps.info->nnzP)/Ps.info->nnzA, ((REAL)Ps.info->peak)/Ps.info->nnzA);
#endif




#ifdef PIC
  /* DBPrec size infos */ 
  /* if (option->droptol1 == 0) { /\* else todo *\/ */
  /*     printf("Pic : NNZ Pic : %ld\n", DBPrec_NNZPic(&P)); */
  /*     printf("Pic : NNZ Pic Minimized : %ld\n", DBPrec_NNZMinPic(&P)); */
  /*     printf("Pic : (Before GEMM : %ld, After : %ld)\n", DBPrec_NNZBefore(&P), DBPrec_NNZ(&P)); */
  /*   } */
#endif

  fprintf(stdout, "Needed memory for preconditioner : %d Mo (only coeftab storage)\n", 
	  (int)((nnzL*sizeof(COEF))/1048576));
  /************************************************************************************************************/


 

  PHIDAL_MatVec(m, &BL, x, b);

#ifndef DEBUG_NOALLOCATION
  bzero(x, sizeof(COEF)*n);
#endif
  

  t1  = dwalltime();
#ifndef PHIDAL_SCAL

  DBMATRIX_Solve(m, &P, &BL, &phidaloptions, b, x, NULL);
#else

  PHIDAL_Solve(m, &Ps, &BL, &phidaloptions, b, x, NULL);
#endif
  
  t2  = dwalltime(); 
  fprintf(stdout, "\n Solve in %g seconds \n", t2-t1);
  
  /* for(i = 0; i < P.S->dim1PB_ ICI; i++) { */
  /*     x[i] = 1 - x[i]; */
  /*   } */
  
  /*   ro = DNRM2(P.S->dim1,x,UN); */
  /*   fprintf(stdout, " nrm2(sol-x)) = %e\n\n", ro); */
  
  

  /*  PhidalPrec_Clean(&P); */
  
  {
    int UN=1;  
    /*     ro = BLAS_NRM2(P.SL->dim1,x,UN); */
    /*     fprintf(stdout, " nrm2(sol-x)) = %e\n\n", ro); */
    /*  PhidalPrec_Clean(&P); */
    
    r = (COEF *)malloc(sizeof(COEF)*m->dim1);
    memcpy(r, b, sizeof(COEF)*m->dim1);
    PHIDAL_MatVecSub(m, &BL, x, r);
    /*matvecz(mat, x, r, r);*/

    fprintf(stdout, "Relative residual norm = %g \n", BLAS_NRM2(m->dim1, r, UN) / BLAS_NRM2(m->dim1, b, UN));
    free(r);
  }
 
  /******************************************************************/
  /* Free Memory                   **********************************/
  /******************************************************************/

  /* TODO : reste des free a faire */

  free(x);
  free(b);

  PhidalMatrix_Clean(m);
  free(m);
#ifndef PHIDAL_SCAL
#ifndef PIC 
  DBPrec_Clean(&P);
#endif
#else
  PhidalPrec_Clean(&Ps);

#endif
  PhidalHID_Clean(&BL); 
  PhidalOptions_Clean(&phidaloptions);

  fprintf(stdout, "END \n");

#ifdef MEMTRACE
  trace_info();
  trace_exit();
#endif

  return 0;
}




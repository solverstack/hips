! @release_exclude
!-----------------------------------------------------------------------
! contains the functions needed for defining the PDE problems.
!
! Extracted from SPARSEKIT
! problem is :
!
! format for the elliptic equation:
!       d    du    d    du    d    du      du     du     du
! L u = --(A --) + --(B --) + --(C --) + D -- + E -- + F -- + G u = ..
!       dx   dx    dy   dy    dz   dz      dx     dy     dz
!
! Following
!-----------------------------------------------------------------------
      function afun (x,y,z)
      real*8 afun, x,y, z
      afun = -1.0d0
      return
      end
!
      function bfun (x,y,z)
      real*8 bfun, x,y, z
      bfun = -1.0d0
      return
      end
!
      function cfun (x,y,z)
      real*8 cfun, x,y, z
      cfun = -1.0d0
      return
      end
!
      function dfun (x,y,z)
      real*8 dfun, x,y, z
      data gamma /100.0/
!      dfun = gamma*dexp(x*y)
      dfun = -0.0d0;
      return
      end
!
      function efun (x,y,z)
      real*8 efun, x,y, z
      data gamma /100.0/
!      efun = gamma*dexp(-x*y)
      efun = 0d0;
      return
      end
!
      function ffun (x,y,z)
      real*8 ffun, x,y, z
      ffun = 0.0
      return
      end
!
      function gfun (x,y,z)
      real*8 gfun, x,y, z
!
!     gfun negative may make the problem indefinite
!
!      gfun = 10.0d0
      gfun = 0.0d0
      return
      end
!-----------------------------------------------------------------------
! functions for the block PDE's
!-----------------------------------------------------------------------
      subroutine afunbl (nfree,x,y,z,coeff)
      real*8 x, y, z, coeff(100)
      do 2 j=1, nfree
         do 1 i=1, nfree
          coeff((j-1)*nfree+i) = 0.0d0
 1       continue
         coeff((j-1)*nfree+j) = -1.0d0
 2    continue
      return
      end
      subroutine bfunbl (nfree,x,y,z,coeff)
      real*8 x, y, z, coeff(100)
      do 2 j=1, nfree
         do 1 i=1, nfree
          coeff((j-1)*nfree+i) = 0.0d0
 1       continue
         coeff((j-1)*nfree+j) = -1.0d0
 2    continue
      return
      end
        subroutine cfunbl (nfree,x,y,z,coeff)
        real*8 x, y, z, coeff(100)
      do 2 j=1, nfree
         do 1 i=1, nfree
              coeff((j-1)*nfree+i) = 0.0d0
 1         continue
           coeff((j-1)*nfree+j) = -1.0d0
 2      continue
        return
      end
      subroutine dfunbl (nfree,x,y,z,coeff)
      real*8 x, y, z, coeff(100)
      do 2 j=1, nfree
         do 1 i=1, nfree
          coeff((j-1)*nfree+i) = 0.0d0
 1       continue
 2    continue
      return
      end
!
      subroutine efunbl (nfree,x,y,z,coeff)
      real*8 x, y, z, coeff(100)
      do 2 j=1, nfree
         do 1 i=1, nfree
          coeff((j-1)*nfree+i) = 0.0d0
 1       continue
 2    continue
      return
      end
!
      subroutine ffunbl (nfree,x,y,z,coeff)
      real*8 x, y, z, coeff(100)
      do 2 j=1, nfree
         do 1 i=1, nfree
            coeff((j-1)*nfree+i) = 0.0d0
 1       continue
 2    continue
      return
      end
!
      subroutine gfunbl (nfree,x,y,z,coeff)
      real*8 x, y, z, coeff(100)
      do 2 j=1, nfree
         do 1 i=1, nfree
          coeff((j-1)*nfree+i) = 0.0d0
 1       continue
 2    continue
      return
      end
!-----------------------------------------------------------------------
!     The material property function xyk for the
!     finite element problem
!-----------------------------------------------------------------------
      subroutine xyk(nel,xyke,x,y,ijk,node)
      implicit real*8 (a-h,o-z)
      dimension xyke(2,2), x(*), y(*), ijk(node,*)
!
!     this is the identity matrix.
!
      xyke(1,1) = 1.0d0
      xyke(2,2) = 1.0d0
      xyke(1,2) = 0.0d0
      xyke(2,1) = 0.0d0
!
      return
!-----------------------------------------------------------------------
      end

      subroutine setinit(x,n)
      implicit none
      double precision x(*)
      integer n,j

      do j=1, n
         x(j) = sin(4.0*3.1415926*j/n)
      enddo
      return
      end


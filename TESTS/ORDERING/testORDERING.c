/* @authors J. GAIDAMOUR, P. HENON */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "type.h"
#include "io.h"
#ifndef SCOTCH_PART
#include "metis.h"
#endif
#include "localdefs.h"
#include "phidal_ordering.h"



#ifndef SCOTCH_PART

#define METIS_ORDER 
#ifndef METIS_ORDER
#define METIS_PART 
#endif

#endif

#define BUFLEN 200


#ifndef TYPE_REAL
#error Ne marche qu en DOUBLE
#endif

int main(int argc, char *argv[])
{
  /*
   * num    --  working scalar
   * unsym  --  symmetric pattern(0), nonsym pattern(1)
   */
  int unsym;

  /* declarations related to Harwell-boeing format for reading the HB
     matri. Second part is related to I/O parameters */
  int n, nnz, job;

  /* working array for reading matrix */
  REAL *a, *b;
  int *ja, *ia,*jb,*ib, *jg, *ig, metisoption[10], numflag;
  int *mapp,*mapptr, i; 
  csptr mat;
  int ndom;

  /*DistMatrix dm;*/

  /* 
   * buf is for read from 'inputs' file, 
   * matrix represents matrix name
   */
  char matrix[BUFLEN];
  char sfile_path[BUFLEN];

  int *perm, *iperm;
  int rsa;

  PhidalHID BL;
  PhidalOptions phidaloptions;
  PhidalOptions* option= &phidaloptions;

  if(argc < 2)
    {
      fprintferr(stderr, "Illegal arguement : testORDERING <number of domains> \n");
      exit(-1);
    }

  ndom = atof(argv[1]);
  
  PhidalOptions_Init(&phidaloptions);

  /** Read parameters from the input file **/
	       /*if(argc == 0)*/
    /** Default: read from file "input" **/
  GENERAL_setpar(NULL, matrix, sfile_path, &unsym, &rsa, &phidaloptions);   
  

  


  /************************************************************************************************************/
  /****************************************** READ THE MATRIX *************************************************/
  /************************************************************************************************************/

  CSRread(matrix, &n, &nnz, &ia, &ja, &a, NULL, NULL);
  assert(phidaloptions.symmetric == rsa);

  /************************ RSA matrix contains only the lower triangular part of the matrix ***************************/
  if(rsa == 1)
    {
      /** Symmetrize the matrix (in rsa we only have the lower triangular part) ****/
      ib = ia;
      jb = ja;
      b = a;
      numflag = 1;
      job = 2;
      PHIDAL_SymmetrizeMatrix(job, numflag, n, ib, jb, b, &ia, &ja, &a);
      nnz = 2*nnz-n;
      unsym = 0;
    }

  /*******************************************************************************************************************/
  /*******************************************************************************************************************/
  /*******************************************************************************************************************/
  fprintfv(5, stdout,"Matrix dimension is %d, Number of nonzeros is %d\n",n,nnz);



  /*****************************************************************************************************************/
  /***********************************Construct the symmetric graph G of the matrix ********************************/
  /*****************************************************************************************************************/
  ig = (int *)malloc(sizeof(int)*(n+1));
  jg = (int *)malloc(sizeof(int)*nnz);
  memcpy(ig ,ia, sizeof(int)*(n+1));
  memcpy(jg ,ja, sizeof(int)*nnz);

  
  if(unsym == 1)
    {
      
      ib = ig;
      jb = jg;
      numflag = 1;
      job = 0;
      PHIDAL_SymmetrizeMatrix(job, numflag, n, ib, jb, NULL, &ig, &jg, NULL);
    
    }
  /*****************************************************************************************************************/
  /*****************************************************************************************************************/


  /* Translate matrix into C numbering */
  fprintfv(5, stderr, "Convert FORTRAN to C indexing \n");
  CSR_Fnum2Cnum(ja, ia, n);
  CSR_Fnum2Cnum(jg, ig, n);
  numflag = 0;
  

#ifdef METIS_PART /**** HOW TO CONSTRUCT AN OVERLAPPED PARTITION FROM A VERTEX PARTITION (e.g. that produces by METIS) *****/
  riord = (int *)malloc(n*sizeof(int));
  /********************************************************/
  /* Compute a  vertex-based partition  using METIS       */
  /* then compute an edge-based partition wich ovelapps   */
  /* on the vertex separator                              */
  /********************************************************/
  /* Compute an edge-partition using METIS */
  wgtflag = 0;
  metisoption[0] = 0;
  if(ndom > 1)  /** METIS crash for one domain **/
    METIS_PartGraphVKway(&n, ig, jg, NULL, NULL, &wgtflag, &numflag, &ndom, metisoption, &volume, riord);
  else 
    for(i = 0; i < n; i++) 
      riord[i] = 0;


  /*********************************************************************/
  /*********************************************************************/
  /**                                                                 **/
  /** Convert the vertex-based partition into an edge-based partition **/
  /** i.e. vertex separator is overlaped in the subdomain             **/ 
  /** (DO NOT USE THIS FUNCTION IF YOUR DOMAIN ARE ALREADY OVERLAPPED)**/
  /*********************************************************************/
  /*********************************************************************/
  PHIDAL_Partition2OverlappedPartition(numflag, ndom, n, ig, jg, riord, &mapp, &mapptr);

  /** Do not need riord anymore **/
  free(riord);
#endif /** METIS_PART **/


  perm = (int *)malloc(sizeof(int)*n);
  iperm = (int *)malloc(sizeof(int)*n);


#ifdef METIS_ORDER /**** HOW TO CONSTRUCT AN OVERLAPPED PARTITION FROM A MATRIX REORDERING *****/
  /*** Compute the reordering to minimize fill-in using METIS-4.0 ***/
  metisoption[0] = 0;

  /** Delete the self edge in the graph (METIS_NodeND need that)**/
  PHIDAL_CsrDelDiag(numflag, n, ig, jg);

  /**** Compute a matrix reordering that minimizes fill-in *******/
  METIS_NodeND(&n, ig, jg, &numflag, metisoption, perm, iperm);

  /**** Compute the overlapped partition from the matrix reordering   *****/
  PHIDAL_Perm2OverlappedDomains(ndom, n, ig, jg, &mapptr, &mapp, perm, iperm);

#endif /*METIS_ORDER*/


  {
    /*********************************************************/
    /*    DISPLAY SOME INFO ON THE OVERLAPPED PARTITION      */
    /*********************************************************/         
    
    int maxdom, mindom;
    REAL avgdom;


    fprintfv(5, stdout, "PARTITION: \n");
    for(i=0;i<ndom;i++)
      fprintfv(5, stdout, "Domain %d = %d nodes \n", i, mapptr[i+1]-mapptr[i]);
    
    maxdom = 0;
    mindom = mapptr[1]-mapptr[0];
    avgdom = 0.0;
    for(i=0;i<ndom;i++)
      {
	avgdom += mapptr[i+1]-mapptr[i];
	if(mapptr[i+1]-mapptr[i] > maxdom)
	  maxdom = mapptr[i+1]-mapptr[i];
	if(mapptr[i+1]-mapptr[i] < mindom)
	  mindom = mapptr[i+1]-mapptr[i];
      }
    avgdom /= ndom;

    fprintfv(5, stdout, "MAX DOMAIN = %d \n", maxdom);
    fprintfv(5, stdout, "MIN DOMAIN = %d \n", mindom);
    fprintfv(5, stdout, "AVG DOMAIN = %g \n", avgdom);
    fprintfv(5, stdout, "IMBALANCE = %g \n", ((float)(maxdom-mindom))*100.0/mindom );
  }

  /***********************************************************/
  /*           REORDERING IN MULTI BLOCK DIAGONAL LEVEL      */
  /***********************************************************/ 


  numflag = 0;

    
  /*******************************************************************/
  /*******************************************************************/
  /**                                                               **/
  /** COMPUTE THE HIERARCHICAL DECOMPOSITION AND LOCAL DATA         **/
  /**                                                               **/
  /*******************************************************************/
  /*******************************************************************/
  
  /** Compute the global hierarchical interface decomposition and the associated permutation **/ 
  PhidalHID_Init(&BL);
  PHIDAL_HierarchDecomp(1, numflag, n, ig, jg, mapp, mapptr, ndom, &BL, perm, iperm);

  free(ig);
  free(jg);
  free(mapp);
  free(mapptr);


  /****************************************************************/
  /****************************************************************/
  /**                                                            **/
  /**   REODER THE MATRIX ACCORDING TO THE PHIDAL PERMUTATION    **/
  /**                                                            **/
  /****************************************************************/
  /****************************************************************/
  /*---------------------------------------------------------------------*
    |  Convert the matrix in SparRow and permute it                       |
    *---------------------------------------------------------------------*/
  fprintfv(5, stdout, "Permuting the matrix \n");

  mat = (csptr)malloc(sizeof(struct SparRow));
  CSRcs(n, a, ja, ia, mat);
  
  /** DO NOT NEED THESE VECTORS ANYMORE **/
  free(a);
  free(ja);
  free(ia);
  
  /*** Permute the matrix according to the phidal ordering ***/
  /*dpermC(mat, perm);*/
  CS_Perm(mat, perm);

  
  free(perm);
  free(iperm);

  PhidalOptions_Clean(&phidaloptions);
  cleanCS(mat);
  free(mat);

  PhidalHID_Clean(&BL);
  fprintfv(5, stdout, "END testORDERING \n");

  return 0;
}

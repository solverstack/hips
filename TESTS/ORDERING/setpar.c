/* @authors J. GAIDAMOUR, P. HENON */

#include <stdlib.h>
#include <stdio.h>

#include "psparslib.h"
#include "mlbd_struct.h"

#define BUFLEN 100

void GENERAL_setpar(char *filename,  char *matrix, char *sfile_path, int *scale, int
	    *unsym, int *rsa, int *load_local_files, OPT_str *option) 
{
  /* setup parameters for preconditioning and iteration */
  FILE *fp;
  char buf[BUFLEN];
  int num;

  /* read parameters for preconditioner and iteration from file
     'input' */
    
  if(filename == NULL)
    {
      if((fp = fopen("inputs","r")) == NULL) 
	{
	  fprintfv(5, stderr, "Cannot open file inputs\n");
	  exit(1);
	}
    }
  else
    if( (fp = fopen(filename, "r")) == NULL )
      {
	fprintfv(5, stderr, "Cannot open file ARGV. Trying inputs file ... \n"); 
	if((fp = fopen("inputs","r")) == NULL) 
	  {
	    fprintfv(5, stderr, "Cannot open file inputs\n");
	    exit(1);
	  }
      }
  num = 0;

  while(fgets(buf, BUFLEN, fp) != NULL) {
    switch(num) {
    case 0:                             /* matrix */
      sscanf(buf, "%s", matrix); 
      break;
    case 1:                            
      sscanf(buf, "%s", sfile_path); /** if load_local_files = 1 then path_file is the file name prefix of the local data files of all processors **/ 
      break;
    case 2:
      sscanf(buf,"%d", load_local_files); /* = 1: charge precomputed local data files for all processor **/
      break; 
    case 3:                             /* scale */
      sscanf(buf, "%d", scale);
      break;
    case 4:                             /* unsym */
      sscanf(buf, "%d", unsym);
      break;
    case 5:                            /* Numer of level in locally droping strategy **/
      sscanf(buf, "%d", &(option->locally_nbr)); 
      break;
    case 6:                            /* Limite of level to shift to dense computation (>= 1)**/
      sscanf(buf, "%d", &(option->level_dense)); 
      break;
    case 7:                             /* outer tol */
      sscanf(buf,"%lf", &(option->tol));
      break;
    case 8:                             /* im (Outer Krylov subs dim) */
      sscanf(buf,"%d",&(option->krylov_im));
      break;
    case 9:                             /* max outer gmres steps */
      sscanf(buf,"%d", &(option->itmax));
      break;
    case 10:                             
      sscanf(buf, "%lf", &(option->dropL));
      option->droptol[5] = option->dropL;
      break;
    case 11:                             
      sscanf(buf, "%lf", &(option->dropU));
      option->droptol[6] = option->dropU;
      break;
    case 12:                             /*droptol for Schur complement  */
      sscanf(buf, "%lf", &(option->dropSchur));
      break;
    case 13:
      sscanf(buf, "%d", &(option->keep_GW));
      break;
    case 14:
      sscanf(buf,"%lf", &(option->comm_memratio));
      break;
    case 15:
      sscanf(buf,"%d", rsa); /* = 1 :rsa matrix format */
      if( (*rsa) == 1)
	(*unsym) = 1; /** Need to force the symmetrization of the matrix **/
      break; 
    case 16:
      sscanf(buf,"%d", &(option->srpd)); /* = 1 :symmetric real definite positive matrix  */
      break; 
   
    default:
      break;
    }
    num++;
  }
  fclose(fp);
  
}


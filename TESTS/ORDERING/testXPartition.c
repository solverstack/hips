/* @authors J. GAIDAMOUR */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <mpi.h>
#include "math.h"

#include "hips.h"
#include "io.h" /** io.h requires hips.h */

#include "base.h"
#include "io.h"
#include "block.h" /* FLOAT */
#include "db_struct.h"
#include "phidal_parallel.h"
#include "db_parallel.h"

/* rand */
#include <sys/types.h>
#include <time.h>

int main(int argc, char *argv[])
{  
  int n, nnz;
  int numflag;
  int *ia, *ja;
  COEF* a;
 
  int nc;
  {
    int /* nc, */ cube; /* grid type and size */

    /* config */
    cube=0; nc=100;
    
    if(cube == 0) n = nc*nc; else n = nc*nc*nc;
    
    {
      int cparam;
      int* iau = (int *)malloc((n+1)*sizeof(int));

      nnz = 10*(n+1);

      ia = (int *)malloc((n+1)*sizeof(int));      
      ja = (int *)malloc(nnz*sizeof(int));
      a  = (COEF *)malloc(nnz*sizeof(COEF));
   
      if(cube == 0) cparam=1; else cparam=nc;
      gen5pt(&nc, &nc, &cparam, a, ja, ia, iau);
      free(iau);
      free(a);
   
      nnz = ia[n]-ia[0];

      /** C : numbering starts from 0 (HIPS and PETSC lib use C numbering) **/
      CSR_Fnum2Cnum(ja, ia, n);
      numflag = 0;
    }

  }

  int tagnbr=3;
  int nodenbr=3*3;
  int* nodelist=malloc(sizeof(int)*nodenbr);
/*   nodelist[0]=0; */
/*   nodelist[1]=nc-1; */
/*   nodelist[2]=nc*(nc-1); */
/*   nodelist[3]=nc*nc-1; */

  {

    chrono_t t1;
    int i; 
    (void) time(&t1);
    
    srand((long) t1); /* use time in seconds to set seed */
    
    for (i=0;i<nodenbr;++i) 
      nodelist[i]=rand()%n;
  }

  int i,j,k=0;
  for (i=1;i<=3;i++) {
    for(j=1; j<=3; j++) {
      nodelist[k] = j*nc/4 + (i*nc/4)*nc;
      printf("%d %d\n", j*nc/4, (i*nc/4)*nc);

      k++;
    }
  }

/*   for(i=0; i<4*4; i++) */
/*     nodelist[i] = 25+25*100; */

/*   nodelist[1] = 2*25+25*100; */
/*   nodelist[2] = 3*25+25*100; */
/*   nodelist[3] = 4*25+25*100; */

  int ndom;

  int* mapptr;
  int* mapp;
  
  HIPS_XPartition(tagnbr,
		  nodenbr, nodelist,
		  n, ia, ja,
		  &ndom,
		  &mapptr, &mapp,
		  NULL, 0);

#ifdef DEBUG_M3
  int i,j;
  printf("ndom=%d\n", ndom);
  for(i=0; i<ndom; i++) {
    printf("dom n %d\n", i);
    for(j=mapptr[i]; j<mapptr[i+1]; j++) {
      printf("%d ", mapp[j]);
    }
    printf("\n");
  }
#endif 

  /* Grid 2D */
  /* set color */ 

  int* xx = (int*)malloc(sizeof(int)*n);
  int* yy = (int*)malloc(sizeof(int)*n);
  int* color = (int*)malloc(sizeof(int)*n);
  for(i=0; i<n; i++) color[i] = -1;

  {
    int i, j, k=0;
    
    /* set xx, yy */
    for(i=0; i<nc; i++) {
      for(j=0; j<nc; j++) {
	xx[k] = i;
	yy[k] = j;

/* 	if (i<nc/2) */
/* 	  color[k] = 1; */
/* 	else  */
/* 	  color[k] = 2; */

	k++;
      }
    }

    /* set color */
    for(i=0; i<ndom; i++) {
      for(j=mapptr[i]; j<mapptr[i+1]; j++) {
	if (color[mapp[j]] == -1) {
	  color[mapp[j]] = i+1 +1; /* +1 because label '0' is to mark the separation between domains */ /* +1 : X points */
	} else { color[mapp[j]] = 0; /* printf("j=%d dans interface\n", j); */ }
      }
    }
  }

  {
    FILE * fd = fopen("mesh.mesh", "w"); assert(fd != NULL);
    export_2Dmesh(fd, n, ia, ja, xx, yy, color, nodenbr, nodelist);
    fclose(fd);
  }

  free(nodelist);

  free(xx);
  free(yy);
  free(color);

  return 0;

}




/* @release_exclude */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <mpi.h>
#include "math.h"

#include "hips.h"
#include "io.h"
#include "trace.h"

#define BUFLEN   200
#define NALL     2
#define NGRAPH   2
#define NASSEMB  2
#define NASSEMB2 1
#define NRES     2
#define NRES2    1

void checkCSR(INTS n, INTL *ia, INTS *ja, COEF *a, INTS numflag);

int main(int argc, char *argv[])
{  

  /* to read parameters */
  INTS  sym_pattern, sym_matrix;
  char matrixfile[BUFLEN];
  char rhsfile   [BUFLEN];
  /* INTS method; */
  /*  */

  /*  */
  INTS id, idnbr, i, j, k, o;
  INTS *nodelist;
  COEF *x, *rhsloc;
  COEF *xx=NULL;
  INTS proc_id, n, ln;
  INTL *ia, nnz;
  INTS *ja;
  COEF *a;
  INTS domsize, nproc;
  INTS pbegin, pend;
  INTS ierr;
  /*  */

  trace_init();

/*   int pbegin, pend; */

  /*  */

  /** Init MPI environment **/
  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &proc_id);
  MPI_Comm_size(MPI_COMM_WORLD, &nproc);

  /***************************************/
  /* Initialize HIPS for one problem     */
  /***************************************/
  idnbr = 1; /* total */
  HIPS_Initialize(idnbr);

  /*trace_set_limit(32000/nproc);*/ /* megaoctets */

  id = 0; /** id of the linear system **/
  /******************************************/
  /* Read the parameters from "inputs" file */
  /******************************************/
  ierr = HIPS_ReadOptionsFromFile(id, NULL, &sym_pattern, &sym_matrix, matrixfile, rhsfile);
  HIPS_ExitOnError(ierr);
  
  /** parameter domsize is an argument of testHIPS.ex **/
  if(argc >= 2)
    {
      domsize = atoi(argv[1]);
      HIPS_SetOptionINT(id, HIPS_PARTITION_TYPE, 0);
      HIPS_SetOptionINT(id, HIPS_DOMSIZE, domsize);
    }
  else
    {
      HIPS_SetOptionINT(id, HIPS_DOMNBR, nproc);
    }


  /**********************************/
  /* Read the matrix from file      */
  /**********************************/
  CSRread(matrixfile, &n, &nnz, &ia, &ja, &a,  &sym_matrix);
  HIPS_SetOptionINT (id, HIPS_SYMMETRIC, sym_matrix);


  /** C : numbering starts from 0 **/
  CSR_Fnum2Cnum(ja, ia, n);
  HIPS_SetOptionINT(id, HIPS_FORTRAN_NUMBERING, 0);

#ifdef DEBUG_M
  /** Check the matrix **/
  checkCSR(n, ia, ja, a, 0);
#endif

 
  /***************************************************/
  /* ENTER THE GRAPH : PARALLEL INTERFACE            */
  /* Every processor deals with a part of the graph  */
  /***************************************************/
   /**** Each processor keep a part of the matrix between the row pbegin and pend *****/
  i = n/nproc;
  pbegin = i*proc_id;
  if(proc_id != nproc-1)
    pend   = i*(proc_id+1);
  else
    pend   = n;

  fprintf(stdout, "Processor %ld enter rows [%ld %ld] of the graph \n", (long)proc_id, (long)pbegin, (long)pend-1);

  
  /*************************************/
  /* Enter the adjacency graph         */
  /*************************************/
  for(o=0;o<NGRAPH;o++)
    {
      HIPS_GraphBegin(id, n, ia[pend]-ia[pbegin]);
      for(i=pbegin;i<pend;i++)
	for(j=ia[i];j<ia[i+1];j++)
	  HIPS_GraphEdge(id, i, ja[j]);
      HIPS_GraphEnd(id);
    }

  if(sym_pattern != 0 && sym_matrix != 1)
    {
      /** Disable the graph symmetrization (it saves memory).. **/
      /** If you are not sure that the graph is symmetric or not DO NOT  set this parameter to 0 **/
      HIPS_SetOptionINT(id, HIPS_GRAPH_SYM, 0);
    }

  /***************************************/
  /* Get the ordered list of local nodes */
  /***************************************/
  HIPS_GetLocalNodeNbr(id, &ln);

  nodelist = (INTS *)malloc(sizeof(INTS)*ln);

  HIPS_GetLocalNodeList(id,  nodelist);
  
  /***************************************/
  /* LOOP ON THE ASSEMBLY                */
  /***************************************/
  rhsloc = (COEF *)malloc(sizeof(COEF)*ln);
  x = (COEF *)malloc(sizeof(COEF)*ln);
  if(proc_id == 0)
    xx = (COEF *)malloc(sizeof(COEF)*n);
      
  for(o=0;o<NALL;o++)
    {

      /*if(o>=1 && o < 10)
	HIPS_SetOptionINT(id, HIPS_DISABLE_PRECOND, 1);
      else
      HIPS_SetOptionINT(id, HIPS_DISABLE_PRECOND, 0);*/


      for(k=0;k<NASSEMB;k++)
	{
	  
	  INTS job, mode, m;
	  INTL nnz;
	  job = 0; /**  **/
	  mode = HIPS_ASSEMBLY_FOOL; /** assembly only on the local node (i.e. in nodelist) **/
	  nnz = 0;
	  for(m=0;m<ln;m++)
	    nnz += ia[nodelist[m]+1] - ia[nodelist[m]];
	  
	  if(o == 0)
	    {
	      HIPS_MatrixReset(id);
	      HIPS_AssemblyBegin(id, nnz, HIPS_ASSEMBLY_ADD, HIPS_ASSEMBLY_OVW, mode, sym_matrix);
	      for(m=0;m<ln;m++)
		{
		  i = nodelist[m];
		  for(j=ia[i];j<ia[i+1];j++)
		    HIPS_AssemblySetValue(id, i, ja[j], a[j]);
		}
	      HIPS_AssemblyEnd(id);
	    }

	  /** Ajout **/
	  /*HIPS_AssemblyBegin(id, nnz, HIPS_ASSEMBLY_ADD, HIPS_ASSEMBLY_ADD, mode, sym_matrix);
	  for(m=0;m<ln;m++)
	    {
	      i = nodelist[m];
	      for(j=ia[i];j<ia[i+1];j++)
		{
		  if(ja[j] > 0)
		    HIPS_AssemblySetValue(id, i, ja[j]-1, -a[j]);
		}
	    }
	  
	    HIPS_AssemblyEnd(id);*/
	  if(o>0)
	    {
	      /** Ajout **/
	      HIPS_AssemblyBegin(id, nnz, HIPS_ASSEMBLY_ADD, HIPS_ASSEMBLY_ADD, mode, sym_matrix);
	      for(m=0;m<ln;m++)
		{
		  i = nodelist[m];
		  for(j=ia[i];j<ia[i+1];j++)
		    {
		      if(ja[j] == i)
			HIPS_AssemblySetValue(id, i, ja[j], a[j]);
		    }
		}
	      
	      HIPS_AssemblyEnd(id);
	    }

	  fprintf(stderr, "Proc %ld ASSEMBLY %ld : CURRENT = %ld MAX = %ld \n", (long)proc_id, (long)k, (long)trace_get_current(), (long)trace_get_max());
	}
      
      for(k=0;k<NASSEMB2;k++)
	{
	  /***************************************************/
	  /*                                                 */
	  /* ENTER THE MATRIX GRAPH : CENTRALIZED INTERFACE  */
	  /*                                                 */
	  /***************************************************/
	  /*HIPS_MatrixReset(id);*/
	  HIPS_MatrixGlobalCSR(id, n, ia, ja, a, 0, 0, sym_matrix);
	  fprintf(stderr, "Proc %ld ASSEMBLY GLOBAL_CSR  %ld : CURRENT = %ld MAX = %ld \n", (long)proc_id, (long)k, (long)trace_get_current(), (long)trace_get_max()); 
	}
      
   
      /****************************************/
      /* Set the local right hand side        */
      /****************************************/
     
      if(strcmp(rhsfile, "0") != 0)
	{
	  COEF* rhs = (COEF *)malloc(sizeof(COEF)*n);
	  VECread(rhsfile, n, rhs);
	  for(i=0;i<ln;i++)
	    rhsloc[i] = rhs[nodelist[i]];
	  free(rhs);
	}
      else
	{
	  for(i=0;i<ln;i++)
	    x[i] = nodelist[i];
	  HIPS_MatrixVectorProduct(id, x, rhsloc);
	}
      
      
      /***************************************/
      /* LOOP On the resolution              */
      /***************************************/
     
      for(i=0;i<NRES;i++)
	{
	  
	  fprintf(stderr, "Proc %d Resolution %d CURRENT MEM = %ld \n", proc_id, i, (long)trace_get_current());
	  /*trace_get_info(stderr);*/
	  
	  /****************************************************/
	  /* Set the local rhs                                */
	  /****************************************************/ 
	  HIPS_SetLocalRHS(id, rhsloc, 0, 0);
      
	  /****************************************************/
	  /* Get the local solution                           */
	  /****************************************************/ 
	  HIPS_GetLocalSolution(id, x);
	  HIPS_FreePrecond(id);
	  
	}
      
      for(i=0;i<NRES2;i++)
	{
	  
	  /****************************************************/
	  /* Set the local rhs                                */
	  /****************************************************/ 
	  HIPS_SetLocalRHS(id, rhsloc, 0, 0);
	  
	  /****************************************************/
	  /* Get the global solution on processor 0           */
	  /* Original ordering                                */
	  /****************************************************/ 
	  HIPS_GetGlobalSolution(id, xx, 0);
	  
	  
	  if(proc_id == 0)
	    {
	      double checks = 0;
	      int k;
	      for(k=0;k<n;k++)
		checks += coefabs(xx[k] - k);
	      fprintf(stdout, "Error with solution (x[i]=i) is %g \n", checks);
	    }
	  
	  fprintf(stderr, "Proc %d Resolution Global %d CURRENT MEM = %ld \n", proc_id, i, (long)trace_get_current());
	  /*trace_get_info(stdout);*/
	}
    }

  free(ia);
  free(ja);
  free(a);

      

  if(proc_id == 0)
    free(xx);
  /**************************************************/
  /* Free HIPS internal structure for problem "id"  */
  /**************************************************/
  HIPS_Clean(id);

  /**********************************/
  /* Free HIPS internal structures  */
  /**********************************/
  HIPS_Finalize();

  free(nodelist);
  free(x);
  free(rhsloc);

  /** End MPI **/
  MPI_Finalize();
  
  fprintf(stderr, "Proc %ld END : CURRENT MEM = %ld MAX = %ld \n", (long)proc_id,  (long)trace_get_current(),  (long)trace_get_max());
  

  trace_exit();

  return 0;
}

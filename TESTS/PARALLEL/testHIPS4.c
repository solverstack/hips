/* @authors J. GAIDAMOUR, P. HENON */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <mpi.h>
#include "math.h"

#include "hips.h"
#include "io.h"

#define BUFLEN 200

int main(int argc, char *argv[])
{  

  /* to read parameters */
  INTS  sym_pattern, sym_matrix;
  char matrixfile[BUFLEN];
  char rhsfile   [BUFLEN];
  /*  */

  /*  */
  INTS id, idnbr, i, j, k;
  INTS *unknownlist;
  COEF *x, *rhsloc;
  INTS proc_id, n, ln;
  INTL *ia;
  INTS *ja;
  COEF *a;
  INTS domsize, nproc;
  INTS *mapptr, *mapp;
  INTS overlap, numflag, mode;
  INTL * lia;
  INTS *lja;
  COEF *la;
  INTL nnz;
    

  INTS ierr;
  /*  */

  /*  */

  /** Init MPI environment **/
  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &proc_id);
  MPI_Comm_size(MPI_COMM_WORLD, &nproc);

  /***************************************/
  /* Initialize HIPS for one problem     */
  /***************************************/
  idnbr = 1; /* total */
  ierr = HIPS_Initialize(idnbr);
  HIPS_ExitOnError(ierr);

  id = 0; /** id of the linear system **/
  /******************************************/
  /* Read the parameters from "inputs" file */
  /******************************************/
  ierr = HIPS_ReadOptionsFromFile(id, NULL, &sym_pattern, &sym_matrix, matrixfile, rhsfile);
  HIPS_ExitOnError(ierr);
  
  /** parameter domsize is an argument of testHIPS.ex **/
  if(argc >= 2)
    {
      domsize = atoi(argv[1]);
      HIPS_SetOptionINT(id, HIPS_DOMSIZE, domsize);
    }
  else
    HIPS_SetOptionINT(id, HIPS_DOMNBR, nproc);


  /**********************************/
  /* Read the matrix from file      */
  /**********************************/
  CSRread(matrixfile, &n, &nnz, &ia, &ja, &a, &sym_matrix);
  HIPS_SetOptionINT (id, HIPS_SYMMETRIC, sym_matrix);


  /** C : numbering starts from 0 **/
  CSR_Fnum2Cnum(ja, ia, n);
  HIPS_SetOptionINT(id, HIPS_FORTRAN_NUMBERING, 0);
 
  

  numflag = 0;

  /** You can use either a good partition from metis or scotch ; we also give 
      a trivial partition example (convergence is worse) **/
#define PARTITION_METIS_SCOTCH
#ifdef PARTITION_METIS_SCOTCH
  /** Compute a partition from Metis or Scotch **/
  overlap = 0;
  HIPS_GraphPartition(nproc, overlap, numflag, n, ia, ja, 0, &mapptr, &mapp);
  HIPS_ExitOnError(ierr);
#else
  /** Trivial partition **/
  overlap = 0;
  {
    INTS p, pbegin, pend;
    i = n/nproc;
    pbegin = i*proc_id;
    if(proc_id != nproc-1)
      pend   = i*(proc_id+1);
    else
    pend   = n;
    mapptr = (INTS *)malloc(sizeof(INTS)*(nproc+1));
    mapp = (INTS *)malloc(sizeof(INTS)*n);
    
    i = n/nproc;
    for(p=0;p<nproc;p++)
      mapptr[p] = i*p;
    mapptr[nproc] = n;
    
    for(i=0;i<n;i++)
      mapp[i] = i;
  }
#endif
  /***********************************************/
  /* Build the local csr matrix corresponding to */
  /* the partition                               */
  /***********************************************/          
  /** number of local rows **/
  ln = mapptr[proc_id+1]-mapptr[proc_id];
  lia = (INTL *)malloc(sizeof(INTL)*(ln+1));
  unknownlist = (INTS *)malloc(sizeof(INTS)*ln);

  for(i=0;i<ln;i++)
    unknownlist[i] = mapp[mapptr[proc_id]+i];
  
  nnz = 0;
  for(i=0;i<ln;i++)
    {
      lia[i] = nnz;
      k = unknownlist[i];
      nnz += ia[k+1] - ia[k];
    }
  lia[ln] = nnz;
  
  lja = (INTS *)malloc(sizeof(INTS)*nnz);
  la = (COEF *)malloc(sizeof(COEF)*nnz);
  for(i=0;i<ln;i++)
    {
      k = unknownlist[i];
      memcpy(lja + lia[i], ja + ia[k], sizeof(INTS)*(ia[k+1]-ia[k]));
      memcpy(la + lia[i], a + ia[k], sizeof(COEF)*(ia[k+1]-ia[k]));
    }
  free(ia);
  free(ja);
  free(a);
  
  /***************************************************/
  /* ENTER THE GRAPH : PARALLEL INTERFACE            */
  /* Every processor deals with a part of the graph  */
  /***************************************************/
  /*** Now ln, lia, lja, la represents the part on this processor of the distributed CSR matrix **/
  ierr =  HIPS_GraphDistrCSR(id, n, ln, unknownlist, lia, lja);
  HIPS_ExitOnError(ierr);
 
  if(sym_pattern != 0 && sym_matrix != 1)
    {
      /** Disable the graph symmetrization (it saves memory).. **/
      /** If you are not sure that the graph is symmetric or not DO NOT  set this parameter to 0 **/
      HIPS_SetOptionINT(id, HIPS_GRAPH_SYM, 0);
    }

  /***************************************************/
  /*                                                 */
  /*            ENTER A USER PARTITION               */
  /*                                                 */
  /***************************************************/
  /** Only the master processor (0 by default) needs to enter the partition **/
  if(proc_id == 0 && argc < 2)
    {
      /** But you can use any overlaped or non-overlaped partition here**/
    
      /** I want to use my unknown distribution instead of the HIPS internal one **/
      ierr = HIPS_SetPartition(id, nproc, mapptr, mapp);
      HIPS_ExitOnError(ierr);
    }
 
  free(mapptr);
  free(mapp);


  /***************************************************/
  /*                                                 */
  /*          ENTER THE MATRIX COEFFICIENT           */
  /*                                                 */
  /***************************************************/
  mode = HIPS_ASSEMBLY_FOOL; /** Overlap = 0 **/

  /* Construct the local CSR matrix  */
  /* In a real code, each processor should compute its local matrix : there is no need 
     of the global matrix */
 
  ierr = HIPS_MatrixDistrCSR(id, ln, unknownlist, lia, lja, la, HIPS_ASSEMBLY_OVW, HIPS_ASSEMBLY_OVW,  mode, sym_matrix);
  HIPS_ExitOnError(ierr);
  
 
  free(lia);
  free(lja);
  free(la);

  /************************************dez***************/
  /*                                                 */
  /*          ENTER THE RIGHT-HAND-SIDE              */
  /*                                                 */
  /***************************************************/
  rhsloc = (COEF *)malloc(sizeof(COEF)*ln);
  x = (COEF *)malloc(sizeof(COEF)*ln);
  if(strcmp(rhsfile, "0") != 0)
    {
      COEF* rhs = (COEF *)malloc(sizeof(COEF)*n);
      VECread(rhsfile, n, rhs);
      for(i=0;i<ln;i++)
	rhsloc[i] = rhs[unknownlist[i]];
      free(rhs);
    }
  else
     {
      for(i=0;i<ln;i++)
	/*x[i] = unknownlist[i];*/
	rhsloc[i] = 1;
      /*ierr = HIPS_MatrixVectorProduct(id, x, rhsloc);
	HIPS_ExitOnError(ierr);*/
    }
  ierr = HIPS_SetRHS(id, ln, unknownlist, rhsloc, HIPS_ASSEMBLY_OVW, HIPS_ASSEMBLY_OVW, HIPS_ASSEMBLY_FOOL);
  HIPS_ExitOnError(ierr);

  
  /***************************************************/
  /*                                                 */
  /*               SOLVE                             */
  /*                                                 */
  /***************************************************/
  /****************************************************/
  /* Get the local solution                           */
  /****************************************************/ 
  ierr = HIPS_GetSolution(id, ln, unknownlist, x, HIPS_ASSEMBLY_FOOL);
  HIPS_ExitOnError(ierr);
  

  /**************************************************/
  /* Free HIPS internal structure for problem "id"  */
  /**************************************************/
  ierr = HIPS_Clean(id);
  HIPS_ExitOnError(ierr);


  /**********************************/
  /* Free HIPS internal structures  */
  /**********************************/
  ierr = HIPS_Finalize();
  HIPS_ExitOnError(ierr);

  free(unknownlist);
  free(x);
  free(rhsloc);

  /** End MPI **/
  MPI_Finalize();
  
  return 0;
}

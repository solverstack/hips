/* @authors J. GAIDAMOUR, P. HENON */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <mpi.h>
#include "math.h"

#include "hips.h"
#include "io.h" /** io.h requires hips.h */

#define BUFLEN 200

/* #define MATRICES_NOT_SAVED */

int main(int argc, char *argv[])
{  

  /* to read parameters */
  INTS  sym_pattern, sym_matrix;
  char matrixfile[BUFLEN];
  char rhsfile   [BUFLEN];
  /*  */

  /*  */
  INTS id, idnbr, i;
  INTS *nodelist;
  COEF *x, *rhsloc;
  INTS proc_id, n, ln;
  INTL *lia;
  INTS *lja;
  COEF *la;

#ifdef MATRICES_NOT_SAVED
  INTL *ia, nnz;
  INTS *ja;
  COEF *a, *la;
#endif

  INTS nproc;
  INTS ierr;
  INTL N;
  /*  */

  /** Init MPI environment **/
  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &proc_id);
  MPI_Comm_size(MPI_COMM_WORLD, &nproc);


  /***************************************/
  /* Initialize HIPS for one problem     */
  /***************************************/
  idnbr = 1; /* total */
  HIPS_Initialize(idnbr);

  id = 0; /** id of the linear system **/

  /**************************************************************************/
  /* Read parameter from the file "Inputs"                                  */
  /* The parameter are set up inside this functions                         */
  /* this function contains calls HIPS_SetOptionsINT and HIPS_SetOptionREAL */
  /**************************************************************************/
  ierr = HIPS_ReadOptionsFromFile(id, NULL, &sym_pattern, &sym_matrix, matrixfile, rhsfile);
  HIPS_ExitOnError(ierr);

  /************************/
  /* Some extra parameters*/
  /************************/
  /** These options requires to sort the graph and the matrix **/
  /** It takes a few more time but can prevent some errors !! **/
  /** To enable checking uncomment these line **/
  HIPS_SetOptionINT(id, HIPS_CHECK_GRAPH, 0);
  HIPS_SetOptionINT(id, HIPS_CHECK_MATRIX, 0);

#ifdef MATRICES_NOT_SAVED
  /**********************************/
  /* Read the matrix from file      */
  /**********************************/
  CSRread(matrixfile, &n, &nnz, &ia, &ja, &a, &sym_pattern, &sym_matrix);
  HIPS_SetOptionINT (id, HIPS_SYMMETRIC, sym_matrix);

  if (proc_id == 0)
    fprintf(stdout, "Matrix : dim=%ld nnz=%ld\n", (long)n, (long)nnz);

  /** C : numbering starts from 0 **/
  CSR_Fnum2Cnum(ja, ia, n);
#endif

  HIPS_SetOptionINT(id, HIPS_FORTRAN_NUMBERING, 0);

  /************************************************/
  /*                                              */
  /*         LOAD LOCAL DATA                      */
  /************************************************/
  ierr = HIPS_SetupLoad(id, "./hid"); 
  HIPS_ExitOnError(ierr);


  /*n = HIPS_GetMatrixGlobalDim(id);*/
  HIPS_GetInfoINT(id, HIPS_INFO_DIM, &N);
  n = N;

  /***************************************************/
  /*                                                 */
  /*            GET THE LOCAL UNKNOWN LIST           */
  /*                                                 */
  /***************************************************/
  ierr = HIPS_GetLocalUnknownNbr(id, &ln);
  HIPS_ExitOnError(ierr);

  nodelist = (int *)malloc(sizeof(int)*ln);
  ierr = HIPS_GetLocalUnknownList(id, nodelist);
  HIPS_ExitOnError(ierr);


  /***********************************/
  /* Get the local CSR matrix        */
  /* from disk                       */
  /***********************************/
 
#ifndef MATRICES_NOT_SAVED
  HIPS_LocalMatriceLoad(id, &sym_matrix, &ln, &lia, &lja, &la, "./mtx");
  HIPS_SetOptionINT (id, HIPS_SYMMETRIC, sym_matrix);
#include <assert.h>
  assert(lia[0] == 0);
#else
  HIPS_GetSubmatrix(ln, nodelist, n, ia, ja, a, &lia, &lja, &la);
  free(ia);
  free(ja);
  free(a);
#endif
  
  /**********************************/
  /* Create the local matrix        */
  /* lia, lja la is the local CSR   */
  /* matrix using nodelist ordering */
  /**********************************/
  ierr = HIPS_MatrixLocalCSR(id, ln, nodelist, lia, lja, la, HIPS_ASSEMBLY_OVW, HIPS_ASSEMBLY_OVW,  sym_matrix);
  HIPS_ExitOnError(ierr);
  free(lia);
  free(lja);
  free(la);




  /****************************************/
  /* Set the local right hand side        */
  /****************************************/
  rhsloc = (COEF *)malloc(sizeof(COEF)*ln);
  x = (COEF *)malloc(sizeof(COEF)*ln);
  if(strcmp(rhsfile, "0") != 0)
    {
      COEF* rhs = (COEF *)malloc(sizeof(COEF)*n);
      VECread(rhsfile, n, rhs);
      for(i=0;i<ln;i++)
	rhsloc[i] = rhs[nodelist[i]];
      free(rhs);
    }
  else
    {
      for(i=0;i<ln;i++)
	x[i] = 1.0;
      HIPS_MatrixVectorProduct(id, x, rhsloc);
    }

  /****************************************************/
  /* Set the local rhs                                */
  /****************************************************/ 
  ierr = HIPS_SetLocalRHS(id, rhsloc, HIPS_ASSEMBLY_OVW, HIPS_ASSEMBLY_OVW);
  HIPS_ExitOnError(ierr);


  /****************************************************/
  /* Get the local solution                           */
  /****************************************************/ 
  ierr = HIPS_GetLocalSolution(id, x);
  HIPS_ExitOnError(ierr);


  /************************************************/
  /* Free HIPS internal structure for problem id  */
  /************************************************/
  ierr = HIPS_Clean(id);
  HIPS_ExitOnError(ierr);


  /**********************************/
  /* Free HIPS internal structures  */
  /**********************************/
  HIPS_Finalize();

  free(nodelist);
  free(x);
  free(rhsloc);

  /** End MPI **/
  MPI_Finalize();
  
  return 0;
}

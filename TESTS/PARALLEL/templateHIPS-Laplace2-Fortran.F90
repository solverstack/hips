
#ifdef INTSIZE32
#define INTS           INTEGER(KIND=4)
#define INTL           INTEGER(KIND=4)
#elif (defined INTSIZE64)
#define INTS           INTEGER(KIND=4)
#define INTL           INTEGER(KIND=8)
#elif (defined INTSSIZE64)
#define INTS           INTEGER(KIND=8) 
#define INTL           INTEGER(KIND=8) 
#else

#define INTS           INTEGER
#define INTL           INTEGER

#endif

#ifndef  PREC_SIMPLE

#define REALF REAL(KIND=8) 

#ifdef    TYPE_COMPLEX
#define COEF COMPLEX(KIND=8)
#else  
#define COEF REAL(KIND=8)
#endif 

#else  

#define REALF REAL(KIND=4)
#ifdef    TYPE_COMPLEX
#define COEF COMPLEX(KIND=4)
#else  
#define COEF REAL(KIND=4)
#endif 

#endif 



PROGRAM main
  IMPLICIT NONE
  INCLUDE "mpif.h"
  INCLUDE "hips.inc"
  ! MPI Data
  INTS :: ierr
  INTERGER :: ierror
  INTEGER :: Me, NTasks
  ! HIPS Data


  ! INTSIZE64 or INTSIZE32 are defined (or undefined) in makefile.in

  INTFS :: id, m, job, mode
  ! CSC Data
  INTFS   :: n, dof
  INT   :: nnzeros, edgenbr
  REAL(KIND=8)  :: val
  ! Local Data
  INTFS :: totalnode,interior,taille
  INTFS           , DIMENSION(:) , POINTER :: nodelist

  REAL(KIND=8)      , DIMENSION(:) , POINTER :: lrhs
  REAL(KIND=8)      , DIMENSION(:) , POINTER :: globx
  ! Other data
  REAL(KIND=8)      , DIMENSION(:) , POINTER :: expand
  REAL(KIND=8) :: prec, xmin, xmax
  INTEGER :: NArgs, i, j, k, myfirstrow, mylastrow
  CHARACTER(LEN=100) :: args
  CHARACTER(LEN=20)  :: atester
  
  NArgs = command_argument_count()

  IF (NArgs >= 3) THEN
     CALL GETARG(1,args)
     READ(args,*) n
     ! degrees of freedom
     CALL GETARG(2,args)
     READ(args,*) dof
     ! Test to pass
     CALL GETARG(3,args)
     READ(args,*) atester
     PRINT *, "HIPS-Fortran test "// atester
  ELSE
     PRINT *, "You MUST specify the size of the matrix and the dof on the comand line"
     PRINT *, "The 3rd argument must be in add_over, supress_zeros , add_supress to be relevant"
     CALL Abort()
  END IF

  CALL MPI_INIT(ierror)
  CALL MPI_COMM_SIZE(MPI_Comm_world, NTasks,ierror )
  CALL MPI_COMM_RANK(MPI_Comm_world, Me, ierror)
  
  xmin = 0.
  xmax = 1.

  ! Starting HIPS
  CALL HIPS_INITIALIZE(1, ierr)
  id = 0

  ! Set Options
  prec = 1e-7
  CALL HIPS_Set_Default_Options(id, HIPS_ITERATIVE, ierr)
  CALL HIPS_Set_Option_INT(id, HIPS_SYMMETRIC, 0, ierr)
  CALL HIPS_Set_Option_REAL(id, HIPS_PREC, prec, ierr)
  CALL HIPS_Set_Option_INT(id, HIPS_LOCALLY, 0, ierr)
  CALL HIPS_Set_Option_INT(id, HIPS_ITMAX, 100, ierr)
  CALL HIPS_Set_Option_INT(id, HIPS_KRYLOV_RESTART, 50, ierr)
  CALL HIPS_Set_Option_INT(id, HIPS_VERBOSE, 2, ierr)
  CALL HIPS_Set_Option_INT(id, HIPS_DOMNBR, NTasks, ierr)
  CALL HIPS_Set_Option_INT(id, HIPS_CHECK_GRAPH, 1, ierr)
  CALL HIPS_Set_Option_INT(id, HIPS_CHECK_MATRIX, 1, ierr)

  ! Set the graph : all processor enter some edge of the 
  ! graph that corresponds to non-zeros location in the matrix


  ! ****************************************
  ! ** Enter the matrix non-zero pattern  **
  ! ** you can use any distribution       **
  ! ****************************************	

  ! this processor enters the A(myfirstrow:mylastrow, *) 
  ! part of the matrix non-zero pattern
  IF (me == 0) THEN
     PRINT *, "ENTERING GRAPH WITH HIPS_GRAPH_BEGIN"
     edgenbr = 3*n-4
     CALL HIPS_GRAPHBEGIN(id, n, edgenbr, dof, ierr)

     ! Dirichlet boundary condition
     CALL HIPS_GRAPH_EDGE(id, 1, 1, ierr)
     CALL HIPS_GRAPH_EDGE(id, n, n, ierr)

     ! Interior
     DO i = 2, n-1
        DO j = -1,1
           CALL HIPS_GRAPHEDGE(id, i, i+j, ierr)
        END DO
     END DO
  ELSE
     edgenbr = 0
     CALL HIPS_GRAPHBEGIN(id, n, edgenbr, dof, ierr)
  END IF
  CALL HIPS_GRAPH_END(id)

  ! Get Local nodes
  CALL HIPS_GET_LOCAL_NODE_NBR(id, totalnode,interior)
  ALLOCATE(nodelist(totalnode))
  
  CALL HIPS_GET_LOCAL_NODE_LIST(id, nodelist)

  IF (atester == 'add_over' .OR. atester == 'add_supress') THEN
     ! Test add_over
     mode = 1
  ELSE
     mode = 0
  END IF

  ! job = 0 : initialize the matrix with this coefficient  
  ! job = 1 : these coefficients are added to the matrix
  job = 0

  ! compute the number of non-zeros;
  nnzeros = 0
  DO m = 1, totalnode
     i = nodelist(m)
     IF (i == 1 .OR. i == n) THEN
        ! Boundaries
        nnzeros = nnzeros + 1
     ELSE
        ! Interior
        DO k = -1, 1 
           nnzeros = nnzeros + count(nodelist == (i+k))
        END DO
    END IF
  END DO

  ! We are using dof so a non zero is in fact a block of size dof**2
  nnzeros = nnzeros * dof**2

  ! You can enter only coefficient (i,j) that are in A(nodelist, nodelist)
  ! on this processor

  ! We enter the lower and upper triangular part of the matrix so sym = 0

  ! expand is the identity matrix of size 'dof' stored by line
  ALLOCATE(expand(dof**2))
  k = 1
  expand = 0.
  DO i = 1,dof
     DO j = 1,dof
        IF (i == j) expand(k) = 1.
        k = k + 1
     END DO
  END DO


  CALL HIPS_ASSEMBLY_BEGIN(id, job, mode, nnzeros, 0)
  DO m = 1, totalnode
     i = nodelist(m)
     IF (i == 1 .OR. i == n) THEN
        ! Boundaries
        CALL GetCoef(val,i,i,nodelist,xmin,xmax,n)
        CALL HIPS_ASSEMBLY_SET_VALUE(id, i, i, val*expand)
     ELSE
        DO k = -1,1
           IF (atester == 'supress_zeros' .OR. atester == 'add_supress') THEN
              IF (k == 0) THEN
                 val = 1.
              ELSE
                 val = 0.
              END IF
              IF ( COUNT(nodelist == (i+k)) > 0 ) THEN
                 CALL HIPS_ASSEMBLY_SET_VALUE(id, i, i+k, val*expand)                
              ENDIF
           ELSE
              IF ( COUNT(nodelist == (i+k)) > 0 ) THEN
                 CALL GetCoef(val,i,i+k,nodelist,xmin,xmax,n)
                 CALL HIPS_ASSEMBLY_SET_VALUE(id, i, i+k, val*expand)
              END IF
           END IF
        END DO
     END IF
  END DO			   
  CALL HIPS_ASSEMBLY_END(id)

  ! We expand the rhs
  ALLOCATE(lrhs(totalnode*dof))
  k = 1
  DO m = 1,totalnode
     CALL GetRhs(val,nodelist(m),nodelist,xmin,xmax,n)
     lrhs(k:k+dof-1) = val
     k = k + dof
  END DO

  CALL HIPS_SET_LOCAL_RHS(id, mode, lrhs)

  ! Get the global solution
  ALLOCATE(globx(n*dof))
  CALL HIPS_GET_GLOBAL_SOL(id, 0, globx)

  ! Store in a file
  IF (Me == 0) THEN
     CALL store(globx,xmin,xmax,dof)
  END IF

  ! I'm Free 
  CALL HIPS_CLEAN(id, ierr)
  CALL HIPS_FINALIZE(ierr)
  CALL MPI_FINALIZE(ierror)

  DEALLOCATE(nodelist,globx)

CONTAINS

  SUBROUTINE GetCoef(val,i,j,nodelist,xmin,xmax,n)
    REAL(KIND=8)           , INTENT(OUT) :: val
    INTEGER                , INTENT(IN)  :: i, j, n
    REAL(KIND=8)           , INTENT(IN)  :: xmin, xmax
    INTEGER , DIMENSION(:) , INTENT(IN)  :: nodelist

    INTEGER      :: c
    REAL(KIND=8) :: dx_1

    dx_1 = (n-1.)/ (xmax - xmin)

    val = 0.

    if (i==j) then
       IF (i==1 .OR. i == n) THEN
          ! Boundary Condition (Dirichlet)
          val = 1
       ELSE
          ! Interior diagonnal part
          c = COUNT(nodelist == (i+1) .OR. nodelist == (i-1))
          val = -c * dx_1
       END IF
    else
       val = dx_1
    end if

  END SUBROUTINE GetCoef



  SUBROUTINE GetRhs(val,i,nodelist,xmin,xmax,n)
    REAL(KIND=8)           , INTENT(OUT) :: val
    INTEGER                , INTENT(IN)  :: i, n
    REAL(KIND=8)           , INTENT(IN)  :: xmin, xmax
    INTEGER , DIMENSION(:) , INTENT(IN)  :: nodelist

    REAL(KIND=8) :: dx , x,Pi
    INTEGER :: c

    dx = (xmax - xmin) / (n-1.)
    x = xmin + (i-1)*dx
      
    Pi =acos(-1.)

    val = -4*Pi**2*sin(2*Pi*x)
    ! Boundary Condition (Dirichlet)
    if (i == n .OR. i==1) val = 0.

    c = COUNT(nodelist == (i+1) .OR. nodelist == (i-1))
    val = c*dx/2.*val

  END SUBROUTINE GetRhs


    SUBROUTINE store(sol,xmin,xmax,dof)
      REAL(KIND=8) , DIMENSION(:) , INTENT(IN) :: sol
      REAL(KIND=8)                , INTENT(IN) :: xmin,xmax
      INTFS                     , INTENT(IN) :: dof

      REAL(KIND=8)    :: x,dx,Pi2,s
      INT :: i,j,k, n
      CHARACTER(len=100) :: ecriture

      n = size(sol) / dof
      x = xmin
      dx = (xmax - xmin) / (n-1.)
      Pi2 = 2.*acos(-1.)
      
      WRITE(ecriture,*) '(i9,X,',dof+2,'(E15.8,X))'

      OPEN(unit = 20, file="result4")
      k = 1
      DO i = 1, n
         WRITE(20,ecriture) k,x,sin(Pi2*x),sol(k:k+dof-1)
         k = k + dof
         x = x + dx
      END DO
      CLOSE(20)
    END SUBROUTINE store

END PROGRAM main

/* @release_exclude */
/* @authors J. GAIDAMOUR, P. HENON */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <mpi.h>
#include "math.h"

#include <trace.h>

#include "math.h"

#include "hips.h"
#include "io.h" /** io.h requires hips.h */

#define BUFLEN 200

double dwalltime(); /* very important ! */

#define TIME(msg) \
  t2 = dwalltime(); ttotal += t2-t1; \
  printf("\n\n%s TIME : %g seconds (total = %g)\n", msg, t2-t1, ttotal); \
  printf("%s ", msg); trace_get_info(stdout); \
  t1 = dwalltime();


int main(int argc, char *argv[])
{  

  /* to read parameters */
  INTS  sym_pattern, sym_matrix;
  char matrixfile[BUFLEN];
  char rhsfile   [BUFLEN];
  INTS method;
  /*  */

  /*  */
  INTS id, idnbr, i;
  INTS numflag;
  INTS *nodelist;
  COEF *x, *rhsloc;
  INTS proc_id, n, ln;
  INTL *ia, *lia, nnz;
  INTS *ja, *lja;
  COEF *a, *la;
  INTS interior;
  INTS domsize, nproc;
  /*  */
  chrono_t t1, t2, ttotal=0;

  trace_init();

  t1 = dwalltime(); 

  /** Init MPI environment **/
  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &proc_id);
  MPI_Comm_size(MPI_COMM_WORLD, &nproc);

  trace_set_limit(32000/nproc); /* megaoctets */

  /***************************************/
  /* Initialize HIPS for one problem     */
  /***************************************/
  idnbr = 1; /* total */
  HIPS_Initialize(idnbr);

  id = 0; /** id of the linear system **/


  /******************************************/
  /* Read the parameters from "inputs" file */
  /******************************************/
  {
    INTS    options_int   [OPTIONS_INT_NBR];
    double options_double[OPTIONS_REAL_NBR];
    
    Read_options(NULL, matrixfile, &sym_pattern, &sym_matrix, rhsfile, &method, options_int, options_double);
    
    /*******************************/
    /* Put some specific options   */  
    /*******************************/
    
    HIPS_SetDefaultOptions(id, method);
    if(method == HIPS_HYBRID)
      {
	if(argc < 2)
	  {
	    fprintf(stderr, "You must use a domsize parameter with HYBRID strategy \n");
	    fprintf(stderr, "testHIPS.ex <domsize> \n");
	    exit(-1);
	  }
      }


    HIPS_SetOptionREAL(id, HIPS_PREC,           options_double[HIPS_PREC]);

    HIPS_SetOptionINT (id, HIPS_LOCALLY,        options_int[HIPS_LOCALLY]);

    if(method == HIPS_HYBRID)
      HIPS_SetOptionINT (id, HIPS_ITMAX_SCHUR, options_int[HIPS_ITMAX]);
    else
      HIPS_SetOptionINT (id, HIPS_ITMAX, options_int[HIPS_ITMAX]);

    HIPS_SetOptionINT (id, HIPS_KRYLOV_RESTART, options_int[HIPS_KRYLOV_RESTART]);
    if(method != HIPS_HYBRID)
      HIPS_SetOptionREAL(id, HIPS_DROPTOL0,       options_double[HIPS_DROPTOL0]);
    HIPS_SetOptionREAL(id, HIPS_DROPTOL1,       options_double[HIPS_DROPTOL1]);
    HIPS_SetOptionREAL(id, HIPS_DROPTOLE,       options_double[HIPS_DROPTOLE]);
    HIPS_SetOptionINT (id, HIPS_VERBOSE,        options_int[HIPS_VERBOSE]);
  }

  

  if(argc >= 2)
    {
      /** parameter domsize is an argument of testHIPS.ex **/
      domsize = atoi(argv[1]);
      HIPS_SetOptionINT(id, HIPS_PARTITION_TYPE, 0);
      HIPS_SetOptionINT(id, HIPS_DOMSIZE, domsize);
    }
  else
    {
      HIPS_SetOptionINT(id, HIPS_DOMNBR, nproc);
    }


  /************************/
  /* Some extra parameter */
  /************************/
  /** These options requires to sort the graph and the matrix **/
  /** It takes a few more time but can prevent some errors !! **/
  /** To enable checking comment these lines **/
  HIPS_SetOptionINT(id, HIPS_CHECK_GRAPH, 0);
  HIPS_SetOptionINT(id, HIPS_CHECK_MATRIX, 0);


  /**********************************/
  /* Read the matrix from file      */
  /**********************************/
  CSRread(matrixfile, &n, &nnz, &ia, &ja, &a, &sym_pattern, &sym_matrix);
  HIPS_SetOptionINT (id, HIPS_SYMMETRIC, sym_matrix);
  if (proc_id == 0)
    fprintf(stdout, "Matrix : dim=%ld nnz=%ld\n", (long)n, (long)nnz);

  /** C : numbering starts from 0 **/
  CSR_Fnum2Cnum(ja, ia, n);
  numflag = 0;


  /***************************************************/
  /*                                                 */
  /* ENTER THE MATRIX GRAPH : SEQUENTIEL INTERFACE   */
  /*                                                 */
  /***************************************************/
  HIPS_GraphSetGlobalCSR(id, numflag, n, ia, ja, 1);
  
  
  /*************************************/
  /*                                   */
  /*  Symmetrize the adj. graph        */
  /*                                   */
  /*************************************/
  if(sym_pattern == 0 || sym_matrix == 1)
    HIPS_GraphSymmetrize(id);

  TIME("Read         ");
  
  /************************************************/
  /* Compute the Hierarchical Graph Decomposition */
  /************************************************/
  HIPS_GraphBuildHID(id); /* CALLING THIS FUNCTION IS OPTIONAL */
  
  TIME("HierachGraphD");

  /*************************************************/
  /* Compute the local data and parallel structure */
  /* Set the MPI communicator for this system      */
  /*************************************************/
  HIPS_ParallelSetup(id); /* CALLING THIS FUNCTION IS OPTIONAL */

  TIME("ParallelSetup");

  /***************************************/
  /* Get the ordered list of local nodes */
  /***************************************/
  HIPS_GetLocalNodeNbr(id, &ln, &interior);

  /*fprintf(stdout, "Processor %ld : domain total size = %ld, overlap = %ld nodes \n", (long)proc_id, (long)ln, (long)(ln - interior));*/

  nodelist = (INTS *)malloc(sizeof(INTS)*ln);

  HIPS_GetLocalNodeList(id, numflag, nodelist);

  TIME("GetLocalNode ");

  /***********************************/
  /* Construct the local CSR matrix  */
  /* from the global matrix          */
  /***********************************/
  HIPS_GetSubmatrix(ln, nodelist, n, ia, ja, a, &lia, &lja, &la);
  
  free(ia);
  free(ja);
  free(a);

  TIME("GetSubmatrix ");

  /**********************************/
  /* Create the local matrix        */
  /* lia, lja la is the loca CSR    */
  /* matrix using nodelist ordering */
  /**********************************/
  HIPS_MatrixLocalCSR(id, ln, unknownlist, lia, lja, la, HIPS_ASSEMBLY_OVW, HIPS_ASSEMBLY_OVW,  sym_matrix);
  free(lia);
  free(lja);
  free(la);

  TIME("SetMatrixCoef");


  /******************************/
  /* Build HIPS preconditioner  */
  /******************************/
  HIPS_Precond(id); /* CALLING THIS FUNCTION IS OPTIONAL */

  TIME("Precond      ");

  /****************************************/
  /* Set the local right hand side        */
  /****************************************/
  rhsloc = (COEF *)malloc(sizeof(COEF)*ln);
  x = (COEF *)malloc(sizeof(COEF)*ln);
  if(strcmp(rhsfile, "0") != 0)
    {
      COEF* rhs = (COEF *)malloc(sizeof(COEF)*n);
      VECread(rhsfile, n, rhs);
      for(i=0;i<ln;i++)
	rhsloc[i] = rhs[nodelist[i]];
      free(rhs);
    }
  else
    {
      for(i=0;i<ln;i++)
	x[i] = 1.0;
      HIPS_MatrixVectorProduct(id, x, rhsloc);
    }

  TIME("Rhsloc       ");

  /****************************************************/
  /* Set the local rhs                                */
  /****************************************************/ 
  HIPS_SetLocalRHS(id, 0, rhsloc);


  /****************************************************/
  /* Solve the system                                 */             
  /****************************************************/ 
  HIPS_Solve(id);  /* CALLING THIS FUNCTION IS OPTIONAL */

  TIME("Solve        ");

  /****************************************************/
  /* Get the local solution                           */
  /****************************************************/ 
  HIPS_GetLocalSol(id, x);


  /************************************************/
  /* Free HIPS internal structure for problem id  */
  /************************************************/
  HIPS_Clean(id);


  /**********************************/
  /* Free HIPS internal structures  */
  /**********************************/
  HIPS_Finalize();

  free(nodelist);
  free(x);
  free(rhsloc);

  /** End MPI **/
  MPI_Finalize();
  
  TIME("Exit         ");
 
  trace_exit();

  t2 = dwalltime(); ttotal += t2-t1;
  printf("\n\n Total time : %g seconds\n\n", ttotal);
  trace_get_info(stdout);

  return 0;
}

/* @authors J. GAIDAMOUR, P. HENON */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <mpi.h>
#include "math.h"

#include "hips.h"
#include "io.h"

#define BUFLEN 200

int main(int argc, char *argv[])
{  

  /* to read parameters */
  int  sym_pattern, sym_matrix;
  char matrixfile[BUFLEN];
  char rhsfile   [BUFLEN];
  /*  */

  /*  */
  int id, idnbr/* , i */;
  int n/* , ln */;
  INTL nnz;
  INTL *ia; int *ja; /* INTL *lia; int *lja; */
  COEF *a/* , *la */;
  /*   int interior; */
  INTS domsize=-1, nbproc;
  INTS ierr;
  /*  */

  /** Init MPI environment **/
  MPI_Init(&argc, &argv);
  /*   MPI_Comm_rank(MPI_COMM_WORLD, &proc_id); */


  if(argc < 2)
    {
      fprintf(stderr, "Illegal argument : %s <nb proc> <domain size (in number of node)> \n", argv[0]);
      exit(-1);
    }
  
  nbproc = atoi(argv[1]);
  if(argc > 2)
    domsize = atoi(argv[2]);


  /************************************/
  /* Initialize HIPS for one problem  */
  /************************************/
  idnbr = 1; /* total */
  HIPS_Initialize(idnbr);

  id = 0; /** id of the linear system **/


  /******************************************/
  /* Read the parameters from "inputs" file */
  /******************************************/
  ierr = HIPS_ReadOptionsFromFile(id, NULL, &sym_pattern, &sym_matrix, matrixfile, rhsfile);
  HIPS_ExitOnError(ierr);

  /** parameter domsize is an argument of testHIPS.ex **/
  if(argc > 2)
    {
      HIPS_SetOptionINT(id, HIPS_PARTITION_TYPE, 0);
      HIPS_SetOptionINT(id, HIPS_DOMSIZE, domsize);
    }
  else
    {
      HIPS_SetOptionINT(id, HIPS_DOMNBR, nbproc);
    }


  /**********************************/
  /* Read the matrix from file      */
  /**********************************/
  ierr = CSRread(matrixfile, &n, &nnz, &ia, &ja, &a, &sym_matrix);
  HIPS_ExitOnError(ierr);


  HIPS_SetOptionINT (id, HIPS_SYMMETRIC, sym_matrix);

  /** C : numbering starts from 0 **/
  CSR_Fnum2Cnum(ja, ia, n);
  HIPS_SetOptionINT(id, HIPS_FORTRAN_NUMBERING, 0);

  /***************************************************/
  /*                                                 */
  /* ENTER THE MATRIX GRAPH                          */
  /*                                                 */
  /***************************************************/
  HIPS_GraphGlobalCSR(id, n, ia, ja, 0);
  
  
 

  /***************************************/
  /* Dump to disk */
  /***************************************/
  HIPS_SetupSave(id, "./hid");

  HIPS_LocalMatricesSave(id, nbproc, n, ia, ja, a, "./mtx");


  free(ia);
  free(ja);
  free(a);

  /************************************************/
  /* Free HIPS internal structure for problem id  */
  /************************************************/
  HIPS_Clean(id);


  /**********************************/
  /* Free HIPS internal structures  */
  /**********************************/
  HIPS_Finalize();


  /** End MPI **/
  MPI_Finalize();

  fprintf(stdout, "End\n");  
  
  return 0;
}

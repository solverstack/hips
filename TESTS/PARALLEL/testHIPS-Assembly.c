/* @release_exclude */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <mpi.h>
#include "math.h"

#include "hips.h"
#include "io.h"

#define BUFLEN 200

#define DEBUG_MODE_1


void checkCSR(INTS n, INTL *ia, INTS *ja, COEF *a, INTS numflag);

int main(int argc, char *argv[])
{  

  /* to read parameters */
  INTS  sym_pattern, sym_matrix;
  char matrixfile[BUFLEN];
  char rhsfile   [BUFLEN];
  /* INTS method; */
  /*  */

  /*  */
  INTS id, idnbr, i, j;
  INTS *nodelist, *unknownlist;
  COEF *x, *rhsloc;
  COEF *xx=NULL;
  INTS proc_id, n, na, ln, lna;
  INTL *ia, nnz;
  INTS *ja;
  COEF *a;
  INTS domsize, nproc;
  INTS pbegin, pend;
  INTS ierr;
  /*  */
  /*#define CSR_ASSEMBLY*/
#ifdef CSR_ASSEMBLY
  INTL   *lia;
  INTS *lja;
  COEF  *la;
#endif

  /*   int pbegin, pend; */

  /*  */

  /** Init MPI environment **/
  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &proc_id);
  MPI_Comm_size(MPI_COMM_WORLD, &nproc);

  /***************************************/
  /* Initialize HIPS for one problem     */
  /***************************************/
  idnbr = 1; /* total */
  HIPS_Initialize(idnbr);

  id = 0; /** id of the linear system **/


  /**************************************************************************/
  /* Read parameter from the file "Inputs"                                  */
  /* The parameter are set up inside this functions                         */
  /* this function contains calls HIPS_SetOptionsINT and HIPS_SetOptionREAL */
  /**************************************************************************/
  ierr = HIPS_ReadOptionsFromFile(id, NULL, &sym_pattern, &sym_matrix, matrixfile, rhsfile);
  HIPS_ExitOnError(ierr);

  /** parameter domsize is an argument of testHIPS.ex **/
  if(argc >= 2)
    {
      domsize = atoi(argv[1]);
      HIPS_SetOptionINT(id, HIPS_PARTITION_TYPE, 0);
      HIPS_SetOptionINT(id, HIPS_DOMSIZE, domsize);
    }
  else
    {
      HIPS_SetOptionINT(id, HIPS_DOMNBR, nproc);
    }



  /**********************************/
  /* Read the matrix from file      */
  /**********************************/
  CSRread(matrixfile, &n, &nnz, &ia, &ja, &a, &sym_matrix);
  HIPS_SetOptionINT (id, HIPS_SYMMETRIC, sym_matrix);

#ifdef DEBUG_M
  HIPS_SetOptionINT(id, HIPS_DUMP_CSR, 1);
  n = 6;
  ia[0] = 1;
  ia[1] = 4;
  ia[2] = 6;
  ia[3] = 9;
  ia[4] = 11;
  ia[5] = 13;
  ia[6] = 14;

  ja[0] = 1;
  ja[1] = 2;
  ja[2] = 5;

  ja[3] = 2;
  ja[4] = 6;

  ja[5] = 3;
  ja[6] = 4;
  ja[7] = 5;

  ja[8] = 4;
  ja[9] = 6;

  ja[10] = 5;
  ja[11] = 6;

  ja[12] = 6;

  a[0] = 4;
  a[1] = -1;
  a[2] = -1;

  a[3] = 4;
  a[4] = -1;

  a[5] = 4;
  a[6] = -1;
  a[7] = -1;

  a[8] = 4;
  a[9] = -1;

  a[10] = 4;
  a[11] = -1;

  a[12] = 4;
  HIPS_SetOptionINT (id, HIPS_SYMMETRIC, 1);
#endif  


  /** C : numbering starts from 0 **/
  CSR_Fnum2Cnum(ja, ia, n);
  HIPS_SetOptionINT(id, HIPS_FORTRAN_NUMBERING, 0);

#ifdef DEBUG_M
  /** Check the matrix **/
  checkCSR(n, ia, ja, a, 0);
#endif

 
  /***************************************************/
  /* ENTER THE GRAPH : PARALLEL INTERFACE            */
  /* Every processor deals with a part of the graph  */
  /***************************************************/
   /**** Each processor keep a part of the matrix between the row pbegin and pend *****/
  i = n/nproc;
  pbegin = i*proc_id;
  if(proc_id != nproc-1)
    pend   = i*(proc_id+1);
  else
    pend   = n;

  fprintf(stdout, "Processor %ld enter rows [%ld %ld] of the graph \n", (long)proc_id, (long)pbegin, (long)pend-1);

  /*HIPS_SetOptionINT(id, HIPS_SCALE, 0);*/
  
#ifdef DEBUG_PARTITION
  if(proc_id == 0)
    {
      INTS *mapptr;
      INTS *mapp;
      mapptr = (INTS *)malloc(sizeof(INTS)*nproc+1);
      mapp = (INTS *)malloc(sizeof(INTS)*n);
      for(i=0;i<nproc;i++)
	{
	  
	}
      
      HIPS_SetPartition(id, nproc, mapptr, mapp);
      free(mapp);
      free(mapptr);
    }
#endif

  /*************************************/
  /* Enter the adjacency graph         */
  /*************************************/

  na = n;
  HIPS_GraphBegin(id,  n, ia[pend]-ia[pbegin]);

  for(i=pbegin;i<pend;i++)
    for(j=ia[i];j<ia[i+1];j++)
      HIPS_GraphEdge(id, i, ja[j]);
  HIPS_GraphEnd(id);

  if(sym_pattern != 0 && sym_matrix != 1)
    {
      /** Disable the graph symmetrization (it saves memory).. **/
      /** If you are not sure that the graph is symmetric or not DO NOT  set this parameter to 0 **/
      HIPS_SetOptionINT(id, HIPS_GRAPH_SYM, 0);
    }

  /***************************************/
  /* Get the ordered list of local nodes */
  /***************************************/
  HIPS_GetLocalNodeNbr(id, &ln);
  HIPS_GetLocalUnknownNbr(id, &lna);

  nodelist = (INTS *)malloc(sizeof(INTS)*ln);
  unknownlist = (INTS *)malloc(sizeof(INTS)*lna);

  HIPS_GetLocalNodeList(id,  nodelist);
  HIPS_GetLocalUnknownList(id,  unknownlist);

  fprintf(stderr, "Proc %ld unknwons : \n", (long)proc_id);
  for(i=0;i<lna;i++)
    fprintf(stderr, "%ld ", (long)unknownlist[i]);
  fprintf(stderr, "\n");

#ifdef DEBUG_GET_DOMAINS
  {
    INTS locdomnbr;
    INTS size;
    INTS *mapptr, *mapp;
    HIPS_GetLocalDomainNbr(id, &locdomnbr, &size);
    fprintf(stderr, "Proc %ld has %ld domain \n", (long)proc_id, (long)locdomnbr);
    
    mapp = (INTS *)malloc(sizeof(INTS)*size);
    mapptr = (INTS *)malloc(sizeof(INTS)*(locdomnbr+1));
    HIPS_GetLocalDomainList(id, mapptr, mapp);
    
    for(i=0;i<locdomnbr;i++)
      fprintf(stderr, "Domain %ld size = %ld \n", (long)i, (long)(mapptr[i+1]-mapptr[i]));

    free(mapp);
    free(mapptr);
  }
#endif


#ifdef DEBUG_MODE_1
  {
    INTS job, mode, i/* , m */;
    INTL k, nnz;
    job = 0; /**  **/
    mode = HIPS_ASSEMBLY_FOOL; /** assembly only on the local node (i.e. in nodelist) **/
    nnz =  ia[pend]-ia[pbegin];

    /*nnz = ia[n] - ia[0];*/
    fprintf(stderr, "Assembly ln = %ld nnz = %ld \n", (long)ln, (long)nnz);

    ierr = HIPS_AssemblyBegin(id,  nnz, job, HIPS_ASSEMBLY_OVW, mode, sym_matrix);
    HIPS_ExitOnError(ierr);

    fprintf(stderr, "Proc %ld enter row [%ld %ld] \n", (long)proc_id, (long)pbegin, (long)(pend-1)); 

    for(i=pbegin;i<pend;i++)
      {
	for(k=ia[i];k<ia[i+1];k++)
	  {
	    ierr = HIPS_AssemblySetValue(id, i, ja[k], a[k]);
	    HIPS_ExitOnError(ierr);
	  }	  
      }
    ierr = HIPS_AssemblyEnd(id);
    HIPS_ExitOnError(ierr);

#else
  {
    INTS job, mode, i, m;
    INTL k, nnz;
    COEF atab[4];

    job = 1; /**  **/
    mode = 0; /** assembly only on the local node (i.e. in nodelist) **/
    nnz = 0;
    for(m=0;m<ln;m++)
      nnz += ia[nodelist[m]+1] - ia[nodelist[m]];

    /*nnz = ia[n] - ia[0];*/
    fprintf(stderr, "Assembly ln = %ld nnz = %ld \n", (long)ln, (long)nnz);

    ierr = HIPS_AssemblyBegin(id, nnz, job, HIPS_ASSEMBLY_OVW, mode, sym_matrix);
    
    for(m=0;m<ln;m++)
      {
	i = nodelist[m];
	for(k=ia[i];k<ia[i+1];k++)
	  {
	    HIPS_AssemblySetValue(id, i, ja[k], a[k]);
	  }	  
      }
    HIPS_AssemblyEnd(id);
#endif /*DEBUG_MODE_1*/

    free(ia);
    free(ja);
    free(a);
  }


  /****************************************/
  /* Set the local right hand side        */
  /****************************************/
  rhsloc = (COEF *)malloc(sizeof(COEF)*lna);
  x = (COEF *)malloc(sizeof(COEF)*lna);
  if(strcmp(rhsfile, "0") != 0)
    {
      COEF* rhs = (COEF *)malloc(sizeof(COEF)*na);
      VECread(rhsfile, na, rhs);
      for(i=0;i<lna;i++)
	rhsloc[i] = rhs[unknownlist[i]];
      free(rhs);
    }
  else
    {
      /*for(i=0;i<lna;i++)
	x[i] = unknownlist[i];
	HIPS_MatrixVectorProduct(id, x, rhsloc);*/
      for(i=0;i<lna;i++)
	rhsloc[i] = unknownlist[i];
    }


  /****************************************************/
  /* Set the local rhs                                */
  /****************************************************/ 
  HIPS_SetLocalRHS(id, rhsloc,0, 0);


  /****************************************************/
  /* Get the local solution                           */
  /****************************************************/ 
  HIPS_GetLocalSolution(id, x);


  /****************************************************/
  /* Get the global solution on processor 0           */
  /* Original ordering                                */
  /****************************************************/ 
  if(proc_id == 0)
    xx = (COEF *)malloc(sizeof(COEF)*na);
  
  HIPS_GetGlobalSolution(id, xx, 0);
  
  if(proc_id == 0)
    {
      REAL checks = 0;
      for(i=0;i<na;i++)
	checks += coefabs(xx[i] - i);
      fprintf(stdout, "Error with solution (x[i]=i) is %g \n", checks);
      free(xx);
    }


  /**************************************************/
  /* Free HIPS internal structure for problem "id"  */
  /**************************************************/
  HIPS_Clean(id);


  /**********************************/
  /* Free HIPS internal structures  */
  /**********************************/
  HIPS_Finalize();

  free(nodelist);
  free(unknownlist);
  free(x);
  free(rhsloc);

  /** End MPI **/
  MPI_Finalize();
  
  return 0;
  }

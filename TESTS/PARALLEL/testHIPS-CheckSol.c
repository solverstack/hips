
/* @release_exclude */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <mpi.h>
#include "math.h"


#include "hips.h"
#include "io.h" /** io.h requires hips.h */

#define BUFLEN 200

void CSR_Cnum2Fnum(dim_t *ja, INTL *ia, dim_t n);

void GLOBAL_GRAPH(INTS id, INTS n,  INTL *ia, INTS *ja);
void GLOBAL_COEF(INTS id, INTS n,  INTL *ia, INTS *ja, COEF *a, INTS sym_matrix);
void SOLVE_CHECK(INTS id, INTS proc_id, INTS n, INTL *ia, INTS *ja, COEF *a, char *rhsfile, INTS sym_matrix);
void DISTRIB_GRAPH(INTS id, INTS proc_id, INTS nproc, INTS n, INTL *ia, INTS *ja, INTS sym_pattern, INTS sym_matrix);
void DISTRIB_COEF(INTS id, INTS proc_id, INTS nproc, INTS n, INTL *ia, INTS *ja, COEF *a, INTS sym_matrix, INTS userpart, INTS mode);
void SETOPTIONS(INTS id, INTS proc_id, INTS nproc, INTS domsize, INTS numflag, INTS sym_pattern, INTS sym_matrix, INTS n, INTL *ia, INTS *ja);
void RESET(INTS id, INTS numflag, INTS n, INTL *ia, INTS *ja);


int main(int argc, char *argv[])
{  

  /* to read parameters */
  INTS  sym_pattern, sym_matrix;
  char matrixfile[BUFLEN];
  char rhsfile   [BUFLEN];
  INTS numflag;

  INTS id, idnbr/* , i */;
  INTS proc_id, n;
  INTL *ia, nnz;
  INTS *ja;
  COEF *a;
  INTS domsize, nproc;
  INTS ierr;
  /*  */

  /** Init MPI environment **/
  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &proc_id);
  MPI_Comm_size(MPI_COMM_WORLD, &nproc);


  /***************************************/
  /* Initialize HIPS for one problem     */
  /***************************************/
  idnbr = 1; /* total */

  ierr = HIPS_Initialize(idnbr);
  HIPS_ExitOnError(ierr);

  id = 0; /** id of the linear system **/

  if(argc >= 2)
      /** parameter domsize is an argument of testHIPS.ex **/
      domsize = atoi(argv[1]);
  else
    domsize = -1;

  /**************************************************************************/
  /* Read parameter from the file "Inputs"                                  */
  /* The parameter are set up inside this functions                         */
  /* this function contains calls HIPS_SetOptionsINT and HIPS_SetOptionREAL */
  /**************************************************************************/
  ierr = HIPS_ReadOptionsFromFile(id, NULL, &sym_pattern, &sym_matrix, matrixfile, rhsfile);
  HIPS_ExitOnError(ierr);

 
  /**********************************/
  /* Read the matrix from file      */
  /**********************************/
  CSRread(matrixfile, &n, &nnz, &ia, &ja, &a, &sym_matrix);
 
  

  numflag = 0;


  /* Global Interface */
  if(proc_id == 0)
    fprintf(stderr, "\n GLOBAL INTERFACE \n");
  SETOPTIONS(id, proc_id, nproc, domsize, numflag, sym_pattern, sym_matrix, n, ia, ja);
  GLOBAL_GRAPH(id, n,  ia, ja);
  GLOBAL_COEF(id, n,  ia, ja, a, sym_matrix);
  SOLVE_CHECK(id, proc_id, n, ia, ja, a, rhsfile, sym_matrix);
  RESET(id, numflag, n, ia, ja);

  /* Distributed Interface; respect HIPS distribution */
  if(proc_id == 0)
    fprintf(stderr, "\n DISTRIBUTED INTERFACE : distribution respect \n");
  SETOPTIONS(id, proc_id, nproc, domsize, numflag, sym_pattern, sym_matrix, n, ia, ja);
  DISTRIB_GRAPH(id, proc_id, nproc, n, ia, ja, sym_pattern, sym_matrix);
  DISTRIB_COEF(id,  proc_id, nproc, n, ia, ja, a, sym_matrix, 0, HIPS_ASSEMBLY_RESPECT);
  SOLVE_CHECK(id, proc_id, n, ia, ja, a, rhsfile, sym_matrix);
  RESET(id, numflag, n, ia, ja);


  /* Distributed Interface; user partition */
  if(proc_id == 0)
    fprintf(stderr, "\n DISTRIBUTED INTERFACE : user partition , distribution fool \n");
  SETOPTIONS(id, proc_id, nproc, domsize, numflag, sym_pattern, sym_matrix, n, ia, ja);                                                
  DISTRIB_GRAPH(id, proc_id, nproc, n, ia, ja, sym_pattern, sym_matrix);
  DISTRIB_COEF(id,  proc_id, nproc, n, ia, ja, a, sym_matrix, 1, HIPS_ASSEMBLY_FOOL);
  SOLVE_CHECK(id, proc_id, n, ia, ja, a, rhsfile, sym_matrix);
  RESET(id, numflag, n, ia, ja);

  /* Distributed Interface; hips partition, distribution fool */
  if(proc_id == 0)
    fprintf(stderr, "\n DISTRIBUTED INTERFACE : HIPS partition , distribution fool \n");
  SETOPTIONS(id, proc_id, nproc, domsize, numflag, sym_pattern, sym_matrix, n, ia, ja);                                                
  DISTRIB_GRAPH(id, proc_id, nproc, n, ia, ja, sym_pattern, sym_matrix);
  DISTRIB_COEF(id,  proc_id, nproc, n, ia, ja, a, sym_matrix, 0, HIPS_ASSEMBLY_FOOL);
  SOLVE_CHECK(id, proc_id, n, ia, ja, a, rhsfile, sym_matrix);
  RESET(id, numflag, n, ia, ja);


  /**********************************/
  /* Free HIPS internal structures  */
  /**********************************/
  ierr = HIPS_Finalize();
  HIPS_ExitOnError(ierr);

  free(ia);
  free(ja);
  free(a);

  /** End MPI **/
  MPI_Finalize();
  
  return 0;
}



void SOLVE_CHECK(INTS id, INTS proc_id, INTS n, INTL *ia, INTS *ja, COEF *a, char *rhsfile, INTS sym_matrix) 
{
  INTS ierr;
  COEF *xx = NULL, *rhs;
  INTS i;
  INTL iter;
  REAL facttime, solvtime;

  if(proc_id == 0)
    xx = (COEF *)malloc(sizeof(COEF)*n);
  

  /****************************************/
  /* Set the right hand side (from proc 0)*/
  /****************************************/
  if(proc_id == 0)
    {
      rhs = (COEF *)malloc(sizeof(COEF)*n);
      if(strcmp(rhsfile, "0") != 0)
	VECread(rhsfile, n, rhs);
      else
	for(i=0;i<n;i++)
	  rhs[i] = 1; 
    }
  else
    rhs = NULL;
  
  /****************************************************/
  /* Set the global rhs                               */
  /* rhs is only significant on the master processor  */
  /****************************************************/ 
  ierr = HIPS_SetGlobalRHS(id, rhs, 0, 0);
  HIPS_ExitOnError(ierr);

  /****************************************************/
  /* Get the global solution on processor 0           */
  /* Original ordering                                */
  /****************************************************/ 
 
  ierr = HIPS_GetGlobalSolution(id, xx, 0);
  HIPS_ExitOnError(ierr);

  /****************************************************/
  /* Check the solution                               */
  /* Original ordering                                */
  /****************************************************/ 
  
  if(proc_id == 0)
    {
      HIPS_GetInfoINT(id, HIPS_INFO_ITER, &iter);
      fprintf(stderr, "Number of iterations = %ld \n", (long)iter);
      HIPS_GetInfoREAL(id, HIPS_INFO_PRECOND_TIME, &facttime);
      fprintf(stderr, "Time Precond = %g \n", facttime);
      HIPS_GetInfoREAL(id, HIPS_INFO_SOLVE_TIME, &solvtime);
      fprintf(stderr, "Time Solve = %g \n", solvtime);
    }

  if(proc_id == 0)
    {
      ierr  = HIPS_CheckSolution(id, n, ia, ja, a, xx, rhs, sym_matrix);
      HIPS_ExitOnError(ierr);
    }

  if(proc_id == 0)
    {
      free(xx);
      free(rhs);
    }
}


void GLOBAL_GRAPH(INTS id, INTS n,  INTL *ia, INTS *ja)
{
  INTS ierr;
 /* ENTER THE MATRIX GRAPH : CENTRALIZED INTERFACE  */
  ierr = HIPS_GraphGlobalCSR(id, n, ia, ja, 0);
  HIPS_ExitOnError(ierr);
}

void GLOBAL_COEF(INTS id, INTS n,  INTL *ia, INTS *ja, COEF *a, INTS sym_matrix)
{
  INTS ierr;
  /* ENTER THE MATRIX COEFFICIENTS : CENTRALIZED INTERFACE  */
  ierr = HIPS_MatrixGlobalCSR(id, n, ia, ja, a, 0, HIPS_ASSEMBLY_OVW, sym_matrix);
  HIPS_ExitOnError(ierr);
}

void DISTRIB_GRAPH(INTS id, INTS proc_id, INTS nproc, INTS n, INTL *ia, INTS *ja, INTS sym_pattern, INTS sym_matrix) 
{
  INTS ierr;
  INTS i, j;
  INTS pbegin, pend;

  /**** Each processor keep a part of the matrix between the row pbegin and pend *****/
  i = n/nproc;
  pbegin = i*proc_id;
  if(proc_id != nproc-1)
    pend   = i*(proc_id+1);
  else
    pend   = n;

  ierr = HIPS_GraphBegin(id, n, ia[pend]-ia[pbegin]);
  HIPS_ExitOnError(ierr);

  for(i=pbegin;i<pend;i++)
    for(j=ia[i];j<ia[i+1];j++)
      {
	ierr = HIPS_GraphEdge(id, i, ja[j]);
	HIPS_ExitOnError(ierr);
      }

  ierr = HIPS_GraphEnd(id);
  HIPS_ExitOnError(ierr);

  if(sym_pattern != 0 && sym_matrix != 1)
    {
      /** Disable the graph symmetrization (it saves memory).. **/
      /** If you are not sure that the graph is symmetric or not DO NOT  set this parameter to 0 **/
      HIPS_SetOptionINT(id, HIPS_GRAPH_SYM, 0);
    }

}


void DISTRIB_COEF(INTS id, INTS proc_id, INTS nproc, INTS n, INTL *ia, INTS *ja, COEF *a, INTS sym_matrix, INTS userpart, INTS mode)
{
  INTL k, nnz;
  INTS ln, *unknownlist;
  INTS ierr;
  INTS i/* , j */, m;
  INTS pbegin, pend;

  /**** Each processor keep a part of the matrix between the row pbegin and pend *****/
  i = n/nproc;
  pbegin = i*proc_id;
  if(proc_id != nproc-1)
    pend   = i*(proc_id+1);
  else
    pend   = n;


  /*            ENTER A USER PARTITION               */
  /** Only the master processor (0 by default) needs to enter the partition **/
  if(proc_id == 0 && userpart == 1)
    {
      /** In this example we enter a non overlapped partition **/
      INTS *mapptr, *mapp;
      INTS p;
      mapptr = (INTS *)malloc(sizeof(INTS)*(nproc+1));
      mapp = (INTS *)malloc(sizeof(INTS)*n);

      i = n/nproc;
      for(p=0;p<nproc;p++)
	mapptr[p] = i*p;
      mapptr[nproc] = n;

      /*for(p=0;p<nproc;p++)
	fprintf(stderr, "Proc %ld (naive) partition  = [%ld %ld] \n", (long)p, (long)mapptr[p], (long)(mapptr[p+1]-1));*/
      for(i=0;i<n;i++)
	mapp[i] = i;
      
      
      ierr = HIPS_SetPartition(id, nproc, mapptr, mapp);
      HIPS_ExitOnError(ierr);
      
      free(mapptr);
      free(mapp);
    }
  
  
  if(mode == HIPS_ASSEMBLY_FOOL)
    {
      ln = pend - pbegin;
      unknownlist = (INTS *)malloc(sizeof(INTS)*ln);
      for(i=pbegin;i<pend;i++)
	unknownlist[i-pbegin] = i;
    }
  else
    {
      ierr = HIPS_GetLocalUnknownNbr(id, &ln);
      HIPS_ExitOnError(ierr);
      unknownlist = (INTS *)malloc(sizeof(INTS)*ln);
      ierr = HIPS_GetLocalUnknownList(id, unknownlist);
      HIPS_ExitOnError(ierr);
      
      {
	INTS *tmp;
	int score;
	tmp = (INTS *)malloc(sizeof(INTS)*n);
	bzero(tmp, sizeof(INTS)*n);
	for(i=0;i<ln;i++)
	  tmp[unknownlist[i]]=1;
	
	score = 0;
	for(i=pbegin;i<pend;i++)
	  if(tmp[i] == 1)
	    score++;
	
	/*fprintf(stderr, "partition match = %g \n",((double)score)/(pend-pbegin)); 
	  fprintf(stderr, "partition overlap = %ld \n",(long)ln - (long)(pend-pbegin)); */
	
	free(tmp);
      }
    }

  nnz = 0;
  for(m=0;m<ln;m++)
    nnz += ia[unknownlist[m]+1] - ia[unknownlist[m]];

  ierr = HIPS_AssemblyBegin(id, nnz, HIPS_ASSEMBLY_OVW, HIPS_ASSEMBLY_OVW, mode, sym_matrix);
  HIPS_ExitOnError(ierr);
    
  for(m=0;m<ln;m++)
    {
      i = unknownlist[m];
      for(k=ia[i];k<ia[i+1];k++)
	{
	  ierr = HIPS_AssemblySetValue(id, i, ja[k], a[k]);
	  HIPS_ExitOnError(ierr);
	}
    }

  ierr = HIPS_AssemblyEnd(id);
  HIPS_ExitOnError(ierr);
  free(unknownlist);
  

}


void SETOPTIONS(INTS id, INTS proc_id, INTS nproc, INTS domsize, INTS numflag, INTS sym_pattern, INTS sym_matrix, INTS n, INTL *ia, INTS *ja)
{ 
  
  /**************************************************************************/
  /* Read parameter from the file "Inputs"                                  */
  /* The parameter are set up inside this functions                         */
  /* this function contains calls HIPS_SetOptionsINT and HIPS_SetOptionREAL */
  /**************************************************************************/
  char matrixfile[BUFLEN];
  char rhsfile   [BUFLEN];
  INTS ierr;
  INTS sym1, sym2;

  ierr = HIPS_ReadOptionsFromFile(id, NULL, &sym1, &sym2, matrixfile, rhsfile);
  HIPS_ExitOnError(ierr);

  if(domsize > 0)
    {
      /** parameter domsize is an argument of testHIPS.ex **/
      HIPS_SetOptionINT(id, HIPS_PARTITION_TYPE, 0);
      HIPS_SetOptionINT(id, HIPS_DOMSIZE, domsize);
    }
  else
    HIPS_SetOptionINT(id, HIPS_DOMNBR, nproc);


  /*HIPS_SetOptionINT (id, HIPS_VERBOSE, 0);*/
  HIPS_SetOptionINT (id, HIPS_SYMMETRIC, sym_matrix);
  if(sym_pattern != 0 && sym_matrix != 1)
    /** Disable the graph symmetrization to save memory **/
    HIPS_SetOptionINT(id, HIPS_GRAPH_SYM, 0);

  /** C : numbering starts from 0 **/
  if(numflag == 0)
    CSR_Fnum2Cnum(ja, ia, n);

  HIPS_SetOptionINT(id, HIPS_FORTRAN_NUMBERING, numflag);

  /*HIPS_SetOptionINT(id, HIPS_VERBOSE, 0);*/
}



void RESET(INTS id, INTS numflag, INTS n, INTL *ia, INTS *ja)
{  
  INTS ierr;
  ierr = HIPS_Clean(id);
  HIPS_ExitOnError(ierr);
  /** C : numbering starts from 0 **/
  if(numflag == 0)
    CSR_Cnum2Fnum(ja, ia, n);
}

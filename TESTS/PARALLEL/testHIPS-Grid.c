/* @release_exclude */
/* @authors J. GAIDAMOUR, P. HENON */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <mpi.h>
#include "math.h"


#include "hips.h"
#include "io.h" /** io.h requires hips.h */

#define BUFLEN 200


#ifdef TYPE_REAL  /** Only in double **/
#ifndef INTSIZE64   /** gen5pt prototype **/

#define GRID
#ifdef GRID

#define GRID_REGULAR

/* #define gen5pt gen5pt_ /\* TODO *\/ */
#include "localdefs.h"

#define HIPS_DEFINE_H
#include "../../SRC/INCLUDE/phidal_struct.h"
void GRID_setpar(char *filename,  int *nc, int *cube, char *sfile_path, int *unsym, int *rsa,  PhidalOptions *option);
extern void gen5pt(int *nx,int *ny,int *nz, REAL *a,int *ja,int *ia, int *iau);
#endif /* GRID */

#endif
#endif

int main(int argc, char *argv[])
{  

#ifdef TYPE_REAL  /** Only in double **/
#ifndef INTSIZE64   /** gen5pt prototype **/

  int  nc, cube; /* grid type and size */

  /* to read parameters */
  INTS  sym_pattern, sym_matrix;
  char matrixfile[BUFLEN];
  char rhsfile   [BUFLEN];
  /* INTS method; */
  /*  */

  /*  */
  INTS id, idnbr, i;
  INTS numflag;
  INTS *nodelist;
  COEF *x, *rhsloc;
  COEF *xx=NULL;
  INTS proc_id, n, ln;
  INTL *ia, *lia, nnz;
  INTS *ja, *lja;
  COEF *a, *la;
  INTS domsize, nproc;
  INTS ierr;
  /*  */

  /** Init MPI environment **/
  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &proc_id);
  MPI_Comm_size(MPI_COMM_WORLD, &nproc);


  /***************************************/
  /* Initialize HIPS for one problem     */
  /***************************************/
  idnbr = 1; /* total */
  HIPS_Initialize(idnbr);

  id = 0; /** id of the linear system **/


  /******************************************/
  /* Read the parameters from "inputs" file */
  /******************************************/
  ierr = HIPS_ReadOptionsFromFile(id, NULL, &sym_pattern, &sym_matrix, matrixfile, rhsfile);
  HIPS_ExitOnError(ierr);

  if(argc >= 2)
    {
      /** parameter domsize is an argument of testHIPS.ex **/
      domsize = atoi(argv[1]);
      HIPS_SetOptionINT(id, HIPS_PARTITION_TYPE, 0); /* if GRID_REGULAR defined, changed to -1 later */
      HIPS_SetOptionINT(id, HIPS_DOMSIZE, domsize);
    }
  /* else todo */
/*     { */
/*       HIPS_SetOptionINT(id, HIPS_DOMNBR, nproc); */
/*     } */
  /*********************************************/
 

  /**********************************/
  /* Read the matrix from file      */
  /**********************************/
  /* CSRread(matrixfile, &n, &nnz, &ia, &ja, &a, &sym_pattern, &sym_matrix); */
#ifdef GRID
  {

    /******************************************/
    /* Read the parameters from "inputs" file */
    /******************************************/
    {
      /* get cube and nc from inputs file */
/*       char sfile_path[500]; */
/*       int unsym, rsa; */
/*       PhidalOptions options; */
      
/*       /\* only to get nc and cube *\/ */
/*       GRID_setpar(NULL, &nc, &cube, sfile_path, &unsym, &rsa, &options); */
      
/*       assert(unsym==1); assert(rsa==1); sym_matrix=1; */
/*       assert(options.symmetric == rsa); */
    
      nc = atoi(argv[2]);
      cube = atoi(argv[3]);

      if(cube == 0) n = nc*nc; else n = nc*nc*nc;

      printf("grid-%s-%d\n", (cube==0) ? "2D" : "3D", nc);
      sprintf(matrixfile, "grid-%s-%d", (cube==0) ? "2D" : "3D", nc);
    }

    /**********************************/
    /* Generate the matrix            */
    /**********************************/
    {
      int cparam;
      int* iau = (int *)malloc((n+1)*sizeof(int));

      nnz = 10*(n+1);

      ia = (INTL *)malloc((n+1)*sizeof(INTL));      
      ja = (INTS *)malloc(nnz*sizeof(INTS));
      a  = (COEF *)malloc(nnz*sizeof(COEF));
   
      if(cube == 0) cparam=1; else cparam=nc;
      gen5pt(&nc, &nc, &cparam, a, ja, ia, iau);
      free(iau);
   
      nnz = ia[n]-ia[0];

      CSR_Fnum2Cnum(ja, ia, n);
      numflag = 0;

      /* Get Lower Part */
#include <assert.h>
      assert(numflag == 0);
      int ind=0;
      /* int nnz2 = nnz-((nnz-n)/2); */
      
      INTL *ia2;
      INTS *ja2;
      COEF *a2;

      ia2 = (INTL *)malloc((n+1)*sizeof(INTL));
/*       ja2 = (int *)malloc(nnz2*sizeof(int)); */
/*       a2  = (COEF *)malloc(nnz2*sizeof(COEF)); */

/*       ia2 = ia; */
      ja2 = ja;
      a2  = a;

      for(i=0;i<n;i++)
	{
	  int j;
	  ia2[i] = ind;
	  for(j=ia[i];j<ia[i+1];j++) {
	    if(ja[j] >= i) {
	      assert(ind<=j);
	      ja2[ind] = ja[j];
	      a2[ind]  = a[j];
	      ind++;
	    }
	  }
	}
      ia2[n]=ind;

      


      sym_matrix = 1;
      free(ia);
/*       free(ja);     */
/*       free(a); */
      ia = ia2;
/*       ja = ja2; */
/*       a = a2; */

      nnz = ia[n]-ia[0];

    }
  }

#ifdef GRID_REGULAR  
  HIPS_SetOptionINT (id, HIPS_GRID_DIM, nc);
  HIPS_SetOptionINT (id, HIPS_GRID_3D, cube);
#endif

#endif /* GRID */





/* #define DUMP_GRID */
#ifdef DUMP_GRID


  /* ATTENTION, NUMEROTATION !!! */

 {
    /* not enough for MHD       */
    /*  char Ptrfmt[]="(13I6)"; */
    /*  char Indfmt[]="(16I5)"; */
    char Ptrfmt[]="(11I10)";
    char Indfmt[]="(11I10)";
    char Valfmt[]="(3E26.18)";
    char Rhsfmt[]="(3E26.18)";
    /****************************/
    writeHB_mat_double("grid", n, n, 
		       nnz, ia, ja, 
		       a, 0, NULL, 
		       NULL, NULL,
		       "GRID", "Grid", "RSA", 
		       Ptrfmt,Indfmt,Valfmt,Rhsfmt, /* NULL, NULL, NULL, NULL,		      */
		       "FGN");
    /****************************/
  }

 exit(1);
#endif






  
  HIPS_SetOptionINT (id, HIPS_SYMMETRIC, sym_matrix);
  
  if (proc_id == 0)
    fprintf(stdout, "Matrix : dim=%ld nnz=%ld\n", (long)n, (long)nnz);

  /** C : numbering starts from 0 **/
/*   CSR_Fnum2Cnum(ja, ia, n); */
    numflag = 0;
    HIPS_SetOptionINT(id, HIPS_FORTRAN_NUMBERING, 0);
/*   numflag = 1; */
  
 /*  for(i=0;i<nnz; i++) { */
/*     printf("%d %g\n", ja[i], a[i]); */
/*   } */


  /***************************************************/
  /*                                                 */
  /* ENTER THE MATRIX GRAPH : SEQUENTIEL INTERFACE   */
  /*                                                 */
  /***************************************************/
    HIPS_GraphGlobalCSR(id, n, ia, ja, 0);
  
  if(sym_pattern != 0 && sym_matrix != 1)
    {
      /** Disable the graph symmetrization **/
      HIPS_SetOptionINT(id, HIPS_GRAPH_SYM, 0);
    }


  /***************************************/
  /* Get the ordered list of local nodes */
  /***************************************/
  HIPS_GetLocalNodeNbr(id, &ln);

  nodelist = (INTS *)malloc(sizeof(INTS)*ln);

  HIPS_GetLocalNodeList(id, nodelist);
 
  /***********************************/
  /* Construct the local CSR matrix  */
  /* from the global matrix          */
  /***********************************/
  HIPS_GetSubmatrix(ln, numflag, nodelist, n, ia, ja, a, &lia, &lja, &la);
  
  free(ia);
  free(ja);
  free(a);

  /**********************************/
  /* Create the local matrix        */
  /* lia, lja la is the loca CSR    */
  /* matrix using nodelist ordering */
  /**********************************/
  HIPS_MatrixLocalCSR(id, ln, nodelist, lia, lja, la, HIPS_ASSEMBLY_OVW, HIPS_ASSEMBLY_OVW,  sym_matrix);
  free(lia);
  free(lja);
  free(la);

  /******************************/
  /* Build HIPS preconditioner  */
  /******************************/
  /*HIPS_Precond(id); */ /* CALLING THIS FUNCTION IS OPTIONAL */

  /****************************************/
  /* Set the local right hand side        */
  /****************************************/
  rhsloc = (COEF *)malloc(sizeof(COEF)*ln);
  x = (COEF *)malloc(sizeof(COEF)*ln);
  if(strcmp(rhsfile, "0") != 0)
    {
      COEF* rhs = (COEF *)malloc(sizeof(COEF)*n);
      VECread(rhsfile, n, rhs);
      for(i=0;i<ln;i++)
	rhsloc[i] = rhs[nodelist[i]];
      free(rhs);
    }
  else
    {
      for(i=0;i<ln;i++)
	x[i] = nodelist[i]; 

      HIPS_MatrixVectorProduct(id, x, rhsloc);
    }

  /****************************************************/
  /* Set the local rhs                                */
  /****************************************************/ 
  HIPS_SetLocalRHS(id, rhsloc, 0, 0);


  /****************************************************/
  /* Get the local solution on each processor         */
  /****************************************************/ 
  HIPS_GetLocalSolution(id, x);

  /****************************************************/
  /* Get the global solution on processor 0           */
  /* Original ordering                                */
  /****************************************************/ 
  if(proc_id == 0)
    xx = (COEF *)malloc(sizeof(COEF)*n);
  HIPS_GetGlobalSolution(id, xx, 0);
  
  

  /************************************************/
  /* Free HIPS internal structure for problem id  */
  /************************************************/
  HIPS_Clean(id);

  /**********************************/
  /* Free HIPS internal structures  */
  /**********************************/
  HIPS_Finalize();

  free(nodelist);
  free(x);
  if(proc_id == 0)
    free(xx);
  free(rhsloc);

  /** End MPI **/
  MPI_Finalize();

#endif  
#endif  
  return 0;
}


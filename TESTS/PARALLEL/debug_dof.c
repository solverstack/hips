/* @release_exclude */


  /**/
  if (dof == 2) {
    int i,j,jj;
    int tmpnnz;
    nnz = (ia[n]-ia[0]);
    
    int   nn = n*2;
    int nnnz = nnz+n;
    int*  iia = (int*) malloc(sizeof(int)*n*2);
    int*  jja = (int*) malloc(sizeof(int)*nnnz);
    COEF* aa  = (COEF*)malloc(sizeof(COEF)*nnnz);

    /* add cols */
    tmpnnz=0;
    iia[0] = 0;
    for(i=0;i<n;i++)
    { 
      tmpnnz += ia[i+1]-ia[i];
      iia[i*2   +1] = tmpnnz;
      iia[i*2+1 +1] = tmpnnz+1; tmpnnz++;
    }

    /* add terms */
    j=0;
    jj=0;
    for(i=0;i<n;i++)
    { 
      for(;j<ia[i+1];j++, jj++) {
	aa[jj] = a[j];
	jja[jj] = ja[j]*2;
      }
      aa[jj] = 1;
      jja[jj] = i*2+1;
      jj++;
    }

    /* print */
#ifdef PRINT
    for(i=0;i<3;i++)
    { 
      printfv(5, "i=%d :", i);
      for(j=ia[i];j<ia[i+1];j++) {
	printfv(5, "(%d %0.1e) ", ja[j], a[j]);
      }
      printfv(5, "\n");
    }
    
    free(ia);
    free(ja);
    free(a);
#endif

    n = nn;
    ia = iia;
    ja = jja;
    a = aa;   
 
#ifdef PRINT
    printfv(5, "\n");
    for(i=0;i<6;i++)
      { 
	printfv(5, "i=%d :", i);
	for(j=ia[i];j<ia[i+1];j++) {
	  printfv(5, "(%d %0.1e) ", ja[j], a[j]);
	}
	printfv(5, "\n");
      }
    
    /* exit(1); */
#endif
  }
  /**/


#ifdef INTSIZE32
#define INTS           INTEGER(KIND=4)
#define INTL           INTEGER(KIND=4)
#elif (defined INTSIZE64)
#define INTS           INTEGER(KIND=4)
#define INTL           INTEGER(KIND=8)
#elif (defined INTSSIZE64)
#define INTS           INTEGER(KIND=8) 
#define INTL           INTEGER(KIND=8) 
#else

#define INTS           INTEGER
#define INTL           INTEGER

#endif

#ifndef  PREC_SIMPLE

#define REALF REAL(KIND=8) 

#ifdef    TYPE_COMPLEX
#define COEF COMPLEX(KIND=8)
#else  
#define COEF REAL(KIND=8)
#endif 

#else  

#define REALF REAL(KIND=4)
#ifdef    TYPE_COMPLEX
#define COEF COMPLEX(KIND=4)
#else  
#define COEF REAL(KIND=4)
#endif 

#endif 



PROGRAM main
  IMPLICIT NONE
  INCLUDE "mpif.h"
  INCLUDE "hips.inc"
  ! MPI Data
  INTS :: ierr
  INTEGER :: ierror
  INTEGER :: Me, NTasks
  ! HIPS Data


  ! INTSIZE64 or INTSIZE32 are defined (or undefined) in makefile.in

  INTS :: id, m, job, mode
  ! CSC Data
  INTS   :: n, dof
  INTL   :: nnzeros, edgenbr
  INTL   , DIMENSION(:) , POINTER :: ia
  INTS   , DIMENSION(:) , POINTER :: ja
  COEF      , DIMENSION(:) , POINTER :: avals,rhs
  COEF      , DIMENSION(:) , POINTER :: block_rhs
  ! Local Data
  INTS :: totalnode,interior,taille
  INTS           , DIMENSION(:) , POINTER :: nodelist
  INTL           , DIMENSION(:) , POINTER :: lia
  INTS           , DIMENSION(:) , POINTER :: lja

  COEF      , DIMENSION(:) , POINTER :: lavals,lrhs
  COEF      , DIMENSION(:) , POINTER :: globx
  ! Other data
  COEF      , DIMENSION(:) , POINTER :: expand
  REALF :: prec
  REAL(KIND=8) ::xmin, xmax 
  INTEGER :: NArgs, i, j, k, myfirstrow, mylastrow
  CHARACTER(LEN=100) :: args
  CHARACTER(LEN=10)  :: test

  NArgs = command_argument_count()

  IF (NArgs >= 2) THEN
     CALL GETARG(1,args)
     READ(args,*) n
     ! degrees of freedom
     CALL GETARG(2,args)
     READ(args,*) dof
     IF (NArgs == 3) THEN
        CALL GETARG(3,test)
     ELSE
        test = ''
     END IF
  ELSE
     PRINT *, "You MUST specify the size of the matrix and the dof on the comand line"
     CALL Abort()
  END IF

  CALL MPI_INIT(ierror)
  CALL MPI_COMM_SIZE(MPI_Comm_world, NTasks,ierror )
  CALL MPI_COMM_RANK(MPI_Comm_world, Me, ierror)
  
  xmin = 0.
  xmax = 1.
  CALL genlaplacian(n, nnzeros, ia, ja, avals, rhs, xmin,xmax, ierror)

  ! Starting HIPS
  CALL HIPS_INITIALIZE(1, ierr)
  id = 0

  ! Set Options
  prec = 1e-7
  CALL HIPS_SetDefaultOptions(id, HIPS_ITERATIVE, ierr)
  CALL HIPS_SetOptionINT(id, HIPS_SYMMETRIC, 0, ierr)
  CALL HIPS_SetOptionREAL(id, HIPS_PREC, prec, ierr)
  CALL HIPS_SetOptionINT(id, HIPS_LOCALLY, 0, ierr)
  CALL HIPS_SetOptionINT(id, HIPS_ITMAX, 100, ierr)
  CALL HIPS_SetOptionINT(id, HIPS_KRYLOV_RESTART, 50, ierr)
  CALL HIPS_SetOptionINT(id, HIPS_VERBOSE, 2, ierr)
  CALL HIPS_SetOptionINT(id, HIPS_DOMNBR, NTasks, ierr)
  CALL HIPS_SetOptionINT(id, HIPS_CHECK_GRAPH, 1, ierr)
  CALL HIPS_SetOptionINT(id, HIPS_CHECK_MATRIX, 1, ierr)


  ! Set the graph : all processor enter some edge of the 
  ! graph that corresponds to non-zeros location in the matrix

  i = n/NTasks
  myfirstrow = i*Me+1
  IF (Me == NTasks-1) THEN
    mylastrow   = n
  ELSE
    mylastrow   = i*(Me+1)
  END IF	


  ! ****************************************
  ! ** Enter the matrix non-zero pattern  **
  ! ** you can use any distribution       **
  ! ****************************************	

  ! this processor enters the A(myfirstrow:mylastrow, *) 
  ! part of the matrix non-zero pattern
  edgenbr =  ia(mylastrow+1)-ia(myfirstrow)
 
  IF (test /= '1') THEN

     CALL HIPS_GraphBegin(id, n, edgenbr, ierr)
     DO i = myfirstrow, mylastrow
        DO j = ia(i), ia(i+1)-1
           CALL HIPS_GraphEdge(id, i, ja(j), ierr)
        END DO
     END DO
     CALL HIPS_GraphEnd(id, ierr)

  ELSE

     CALL HIPS_GraphGlobalCSR(id, n, ia, ja, 0, ierr)    

  END IF

  ! Get Local nodes
  CALL HIPS_GetLocalNodeNbr(id, totalnode, ierr)

  ALLOCATE(nodelist(totalnode))
  
  CALL HIPS_GetLocalNodeList(id, nodelist, ierr)

  ! Enter the local part of the matrix ( A(nodelist, nodelist) )

  ! mode must be equal to zero non local assembly not implemented yet
  ! this means that any local coeeficient set into the matrix has to 
  ! be in A(nodelist, nodelist)
  mode = 0

  ! job = 0 : initialize the matrix with this coefficient  
  ! job = 1 : these coefficients are added to the matrix
  job = 0

  ! compute the number of non-zeros;
  nnzeros = 0
  DO m = 1, totalnode
     nnzeros = nnzeros + ia(nodelist(m)+1) - ia(nodelist(m))
  END DO
  ! We are using dof so a non zero is in fact a block of size dof**2
  nnzeros = nnzeros * dof**2

  ! You can enter only coefficient (i,j) that are in A(nodelist, nodelist)
  ! on this processor

  ! We enter the lower and upper triangular part of the matrix so sym = 0

  ! expand is the identity matrix of size 'dof' stored by line : we use this to test the case where a 
  ! node represents several unknown (dof>1)

  ALLOCATE(expand(dof**2))
  k = 1
  expand = 0.
  DO i = 1,dof
     DO j = 1,dof
        IF (i == j) expand(k) = 1.
        k = k + 1
     END DO
  END DO

  CALL HIPS_AssemblyBegin(id, nnzeros, HIPS_ASSEMBLY_OVW, HIPS_ASSEMBLY_OVW, HIPS_ASSEMBLY_RESPECT, 0, ierr)
  DO m = 1, totalnode
     i = nodelist(m)
     DO k = ia(i), ia(i+1)-1 
        ! Test the interface (scalar or array value)
        IF (dof == 1) THEN
          CALL HIPS_AssemblySetValue(id, i, ja(k), avals(k), ierr)
       ELSE
          CALL HIPS_AssemblySetNodeValues(id, i, ja(k), avals(k)*expand, ierr)
       END IF
     END DO			   
  END DO			   
  CALL HIPS_AssemblyEnd(id, ierr)
  DEALLOCATE(expand)


  ! We expand the rhs
  ALLOCATE(lrhs(totalnode*dof))
  k = 1
  DO m = 1,totalnode
     lrhs(k:k+dof-1) = rhs(nodelist(m))
     k = k + dof
  END DO
  CALL HIPS_SetLocalRHS(id, lrhs, HIPS_ASSEMBLY_OVW, HIPS_ASSEMBLY_OVW, ierr)
  DEALLOCATE(lrhs)
  DEALLOCATE(rhs)


  DEALLOCATE(ia)	
  DEALLOCATE(ja)
  DEALLOCATE(avals)

  ! Solve my damn Problem !!!

  ! Get the global solution
  ALLOCATE(globx(n*dof))
  CALL HIPS_GetGlobalSolution(id, globx, 0, ierr)

  ! Store in a file
  IF (Me == 0) THEN
     CALL store(globx,xmin,xmax,dof)
  END IF

  ! I'm Free 
  CALL HIPS_CLEAN(id, ierr)
  CALL HIPS_FINALIZE(ierr)
  CALL MPI_FINALIZE(ierror)

  DEALLOCATE(nodelist,globx)

CONTAINS

!!  Function: genlaplacien
!!
!!  Generate a laplacien of size *n*
!!
!!  Parameters:
!!    n       - Size of the wanted matrix
!!    nnzeros - Number of non zeros in the produced matrice
!!    ia      - Index of first element of each column in *row* and *val*
!!    ja      - Row of eah element				       
!!    avals   - Value of each element				       
!!    rhs     - Right-hand-side member
!!    type    - Type of the matrix				       	 
!!    rhstype - Type of the right hand side.			       	 

    SUBROUTINE genlaplacian(n, nnzeros, ia, ja, avals, rhs,xmin,xmax,success)
      INTS   , INTENT(in)                    :: n
      INTL   , INTENT(out)                   :: nnzeros
      INTL   , DIMENSION(:) , POINTER        :: ia
      INTS   , DIMENSION(:) , POINTER        :: ja
      COEF   , DIMENSION(:) , POINTER        :: avals,rhs
      REAL(KIND=8)      , INTENT(IN)                    :: xmin,xmax
      INTEGER   , INTENT(OUT)                   :: success
      ! Local values
      INTEGER                                   :: i, j
      REAL(KIND=8)                                      :: Pi,dx_2,x,dx

      dx = (xmax - xmin) / (n-1.)
      dx_2 = 1./ dx**2 ! 1.
      x = xmin
      
      Pi =acos(-1.)

      nnzeros = 3*n - 2
      ! Allocating
      ALLOCATE( ia     (n+1)    )
      ALLOCATE( ja     (nnzeros))
      ALLOCATE( avals  (nnzeros))
      ALLOCATE( rhs    (n)      )

      ! Building ia, ja and avals and rhs
      j=1
      ia(1) = 1
      ja(1) = 1
      avals(1) = 1.
      ja(2) = 2
      avals(2) = 0.
      j = 3
      DO i = 2, n-1
         ia(i) = j
         IF (i /= 1) THEN
            ja(j)    = i-1 
            avals(j) = 1.*dx_2
            j = j + 1
         END IF
         ja(j)    = i
         avals(j) = -2.*dx_2
         j=j+1
         IF (i /= n) THEN
            ja(j)    = i+1
            avals(j) = 1.*dx_2
            j=j + 1
         END IF
         x = x + dx
         rhs(i) = -4*Pi**2*sin(2*Pi*x) !0.
      END DO
      ia(n) = j
      ja(j) = n-1
      avals(j) = 0.
      ja(j+1) = n
      avals(j+1) = 1.
      
      ia(n+1) = j+2
      
      rhs(1)  = 0. !1.
      rhs(n)  = 0. !1.
      success = 0

    END SUBROUTINE genlaplacian


    SUBROUTINE store(sol,xmin,xmax,dof)
      COEF , DIMENSION(:) , INTENT(IN) :: sol
      REAL(KIND=8)                , INTENT(IN) :: xmin,xmax
      INTS                     , INTENT(IN) :: dof

      REAL(KIND=8)    :: x,dx,Pi2,s
      INTL :: i,j,k, n
      CHARACTER(len=100) :: ecriture

      n = size(sol) / dof
      x = xmin
      dx = (xmax - xmin) / (n-1.)
      Pi2 = 2.*acos(-1.)
      
      WRITE(ecriture,*) '(i9,X,',dof+2,'(E15.8,X))'

      OPEN(unit = 20, file="result3")
      k = 1
      DO i = 1, n
         WRITE(20,ecriture) k,x,sin(Pi2*x),sol(k:k+dof-1)
         k = k + dof
         x = x + dx
      END DO
      CLOSE(20)
    END SUBROUTINE store

END PROGRAM main

#!/usr/bin/perl -w

$matrixlist = 'MATRIX';
$inputsT = 'Inputs.template';

#---
#$machine = 'CCRT';
#$machine = 'M3PEC';
$machine = 'LOCAL';

@nbproc = (1,2,4);

$prog = '../testHIPS.ex';

$method = 'HYBRID';
#---

$droptol0 = 0.0;
@droptol1list = (0.0);

#---

@levellist = (0);
@ndomlist  = (1000);

#---

$mem = 27000;
$temps = '04:00:00';
if($machine =~ /CCRT/)
{
    $temps = '29'; #minutes
 #   $temps = '240';
}
#---

$sym = 2;
$driver = "";

#---

#if($machine =~ /M3PEC/)
#{
    $submitT = 'submit'.$machine.'.template';
#}

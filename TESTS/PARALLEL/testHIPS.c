/* @authors J. GAIDAMOUR, P. HENON */
/* @release_exclude */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <mpi.h>
#include "math.h"


#include "hips.h"
#include "io.h" /** io.h requires hips.h */

#define BUFLEN 200

int main(int argc, char *argv[])
{  

  /* to read parameters */
  INTS  sym_pattern, sym_matrix;
  char matrixfile[BUFLEN];
  char rhsfile   [BUFLEN];

  /*  */

  /*  */
  INTS id, idnbr, i;
  INTS *unknownlist;
  COEF *x, *rhsloc;
  COEF *xx=NULL;
  INTS proc_id, n, ln;
  INTL *ia, *lia, nnz;
  INTS *ja, *lja;
  COEF *a, *la;
  INTS domsize, nproc;
  INTS ierr;
  /*  */

  /** Init MPI environment **/
  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &proc_id);
  MPI_Comm_size(MPI_COMM_WORLD, &nproc);


  /***************************************/
  /* Initialize HIPS for one problem     */
  /***************************************/
  idnbr = 1; /* total */
  HIPS_Initialize(idnbr);

  id = 0; /** id of the linear system **/

  /**************************************************************************/
  /* Read parameter from the file "Inputs"                                  */
  /* The parameter are set up inside this functions                         */
  /* this function contains calls HIPS_SetOptionsINT and HIPS_SetOptionREAL */
  /**************************************************************************/
  ierr = HIPS_ReadOptionsFromFile(id, NULL, &sym_pattern, &sym_matrix, matrixfile, rhsfile);
  HIPS_ExitOnError(ierr);

  if(argc >= 2)
    {
      /** parameter domsize is an argument of testHIPS.ex **/
      domsize = atoi(argv[1]);
      HIPS_SetOptionINT(id, HIPS_PARTITION_TYPE, 0);
      HIPS_SetOptionINT(id, HIPS_DOMSIZE, domsize);
    }
  else
    HIPS_SetOptionINT(id, HIPS_DOMNBR, nproc);


  /**********************************/
  /* Read the matrix from file      */
  /**********************************/
  CSRread(matrixfile, &n, &nnz, &ia, &ja, &a, &sym_matrix);
  HIPS_SetOptionINT (id, HIPS_SYMMETRIC, sym_matrix);

  fprintf(stderr, "Sym matrix = %d pattern = %d \n", sym_matrix, sym_pattern);

  if(sym_pattern != 0 && sym_matrix != 1)
    /** Disable the graph symmetrization to save memory **/
    HIPS_SetOptionINT(id, HIPS_GRAPH_SYM, 0);


  if (proc_id == 0)
    fprintf(stdout, "Matrix : dim=%ld nnz=%ld\n", (long)n, (long)nnz);

  /** C : numbering starts from 0 **/
  CSR_Fnum2Cnum(ja, ia, n);
  HIPS_SetOptionINT(id, HIPS_FORTRAN_NUMBERING, 0);


  /***************************************************/
  /*                                                 */
  /* ENTER THE MATRIX GRAPH : SEQUENTIEL INTERFACE   */
  /*                                                 */
  /***************************************************/
  ierr = HIPS_GraphGlobalCSR(id, n, ia, ja, 0);
  HIPS_ExitOnError(ierr);

  /***************************************************/
  /*                                                 */
  /*            GET THE LOCAL UNKNOWN LIST           */
  /*                                                 */
  /***************************************************/
  ierr = HIPS_GetLocalUnknownNbr(id, &ln);
  HIPS_ExitOnError(ierr);

  unknownlist = (INTS *)malloc(sizeof(INTS)*ln);
  ierr = HIPS_GetLocalUnknownList(id, unknownlist);
  HIPS_ExitOnError(ierr);
 


  /***************************************************/
  /*                                                 */
  /*          ENTER THE MATRIX COEFFICIENT           */
  /*                                                 */
  /***************************************************/

  /* Construct the local CSR matrix  */
  /* from the global matrix          */

  /* numflag = 0 */
  ierr = HIPS_GetSubmatrix(ln, 0, unknownlist, n, ia, ja, a, &lia, &lja, &la);
  HIPS_ExitOnError(ierr);

  free(ia);
  free(ja);
  free(a);

  /**********************************/
  /* Create the local matrix        */
  /* lia, lja la is the loca CSR    */
  /* matrix using nodelist ordering */
  /**********************************/
  ierr = HIPS_MatrixLocalCSR(id, ln, unknownlist, lia, lja, la, HIPS_ASSEMBLY_OVW, HIPS_ASSEMBLY_OVW,  sym_matrix);
  HIPS_ExitOnError(ierr);

  free(lia);
  free(lja);
  free(la);



  /***************************************************/
  /*                                                 */
  /*          ENTER THE RIGHT-HAND-SIDE              */
  /*                                                 */
  /***************************************************/

  rhsloc = (COEF *)malloc(sizeof(COEF)*ln);
  x = (COEF *)malloc(sizeof(COEF)*ln);
  if(strcmp(rhsfile, "0") != 0)
    {
      COEF* rhs = (COEF *)malloc(sizeof(COEF)*n);
      VECread(rhsfile, n, rhs);

      for(i=0;i<ln;i++)
	rhsloc[i] = rhs[unknownlist[i]];
      free(rhs);
    }
  else
    {
      for(i=0;i<ln;i++)
	/*x[i] = unknownlist[i]; */
	x[i] = 1;
      ierr = HIPS_MatrixVectorProduct(id, x, rhsloc);
      HIPS_ExitOnError(ierr);
      /*for(i=0;i<ln;i++)
	rhsloc[i] = 1;*/
    }

  ierr = HIPS_SetLocalRHS(id, rhsloc, HIPS_ASSEMBLY_OVW, HIPS_ASSEMBLY_OVW);
  HIPS_ExitOnError(ierr);



  /***************************************************/
  /*                                                 */
  /*               SOLVE                             */
  /*                                                 */
  /***************************************************/
  ierr = HIPS_GetLocalSolution(id, x);
  HIPS_ExitOnError(ierr);


  /* Varaint : get the global solution on processor 0 */
  /* Original ordering                                */
  if(proc_id == 0)
    xx = (COEF *)malloc(sizeof(COEF)*n);

  ierr = HIPS_GetGlobalSolution(id, xx, 0);
  HIPS_ExitOnError(ierr);


  if(proc_id == 0)
    {
      REAL checks = 0;
      /*for(i=0;i<n;i++)
	checks += coefabs(xx[i] - i);
	  fprintf(stdout, "Error with solution (x[i]=i) is %g \n", checks);*/
      for(i=0;i<n;i++)
	checks += coefabs(xx[i] - 1);
      fprintf(stdout, "Error with solution (x[i]=1) is %g \n", checks);
    }

  /***************************************************/
  /*                                                 */
  /*               CLEAN                             */
  /*                                                 */
  /***************************************************/
  /* Free HIPS internal structures  for problem id */
  ierr = HIPS_Clean(id);
  HIPS_ExitOnError(ierr);

  /* Free HIPS internal structures for context */
  ierr = HIPS_Finalize();
  HIPS_ExitOnError(ierr);

  free(unknownlist);
  free(x);
  if(proc_id == 0)
    free(xx);
  free(rhsloc);

  /** End MPI **/
  MPI_Finalize();
  
  return 0;
}

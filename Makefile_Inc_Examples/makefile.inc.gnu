###
###  HIPS specific compilation flags
###

##   Arithmetic
##    - default is -DTYPE_REAL in double precision (-DPREC_DOUBLE)
##    - use -DTYPE_COMPLEX to build Complex version of HIPS
##    - use -DPREC_SIMPLE to compute in single precision

COEFTYPE     =

#COEFTYPE    = -DTYPE_REAL
#COEFTYPE    = -DTYPE_COMPLEX

#COEFTYPE    = -DTYPE_REAL    -DPREC_SIMPLE
#COEFTYPE    = -DTYPE_COMPLEX -DPREC_SIMPLE


##   Partitionner
##    - default partitioner is METIS
##    - use -DSCOTCH_PART to use SCOTCH

PARTITIONER  =
#PARTITIONER = -DSCOTCH_PART

# To enable pastix, uncomment the following line
#IPASTIX = -DWITH_PASTIX

##   Integer size
##    - default int type is    : INTS = INTL = int (C type length)
##    - use -DINTSIZE32 to set : INTS = INTEGER(4) and INTL = INTEGER(4)
##    - use -DINTSIZE64 to set : INTS = INTEGER(4) and INTL = INTEGER(8)

INTSIZE      =
#INTSIZE     = -DINTSIZE64
#INTSIZE     = -DINTSIZE32


###
###  Compiler
###

ARCH       = -DLINUX

CC	   = gcc       # C compiler
MPICC      = mpicc
FC         = gfortran  # Fortran compiler
MPIFC      = mpif90
LD	   = $(FC)     # Linker
MPILD      = $(MPIFC)

CFLAGS	   =           # Additional C compiler flags
FFLAGS	   =           # Additional Fortran compiler flags
LFLAGS     =           # Additional linker flags




COPTFLAGS  = -O3       # Optimization flags
FOPTFLAGS  = -O3       #

###
###  Library
###

IBLAS      =           # BLAS include path
LBLAS      = -lblas    # BLAS linker flags

IMPI       =           # Additional MPI include path
LMPI       =           # Additional MPI linker flags

##   METIS_DIR : path to METIS
METIS_DIR  = $(HOME)/lib/metis-4.0
IMETIS     = -I$(METIS_DIR)/Lib
LMETIS     = -L$(METIS_DIR) -lmetis

##   SCOTCH_DIR : path to SCOTCH
SCOTCH_DIR = $(HOME)/lib/scotch_5.1
ISCOTCH    = -I$(SCOTCH_DIR)/include
LSCOTCH    = -L$(SCOTCH_DIR)/lib -lscotch -lscotcherr

##   PASTIX_DIR : path to PASTIX
# to avoid linking with pastix, please comment the 3 following lines
PASTIX_DIR = $(HOME)/ricar/install
IPASTIX = $(IPASTIX) -I$(PASTIX_DIR)
LPASTIX = -L$(PASTIX_DIR) -lpastix -lgfortran -lm -lrt -L/opt/scotch-5.1.8a-mpich2/int64/lib -lscotch -lscotcherrexit -lpthread

###
###  Misc
###

MAKE	   = make
AR	   = ar
ARFLAGS	   = -crs
LN	   = ln
CP	   = cp

###

##   Uncomment that to run in DEBUG mode
#DEBUG     = -g -DDEBUG_M

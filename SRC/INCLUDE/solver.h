/* @authors J. GAIDAMOUR, P. HENON */

/************************************************************/
/**                                                        **/
/**   NAME       : solver.h                                **/
/**                                                        **/
/**   AUTHORS    : David GOUDIN                            **/
/**                Pascal HENON                            **/
/**                Francois PELLEGRINI                     **/
/**                Pierre RAMET                            **/
/**                                                        **/
/**   FUNCTION   : Part of a parallel direct block solver. **/
/**                These lines are the data declarations   **/
/**                for the solver matrix.                  **/
/**                                                        **/
/**   DATES      : # Version 0.0  : from : 22 jul 1998     **/
/**                                 to     28 oct 1998     **/
/**                # Version 1.0  : from : 06 jun 2002     **/
/**                                 to     06 jun 2002     **/
/**                                                        **/
/************************************************************/
#ifndef SOLVER_H
#define SOLVER_H

#include "hips_define.h"
#include "type.h"
#include "symbol.h"

/*+ Solver block structure. +*/
typedef struct SolverBlok_ {
  LONG                       coefind;              /*+ Index in coeftab +*/
} SolverBlok;
/*TODO : unneeded struct*/


/*+ Solver matrix structure. +*/
typedef struct SolverMatrix_ {
  SymbolMatrix              symbmtx;              /*+ Symbolic matrix                           +*/
 
  LONG                       coefnbr;              /*+ Number of coefficients                    +*/
  LONG                       coefmax;              /*+ Working block max size (cblk coeff 1D)    +*/

  /* int                       bloknbr; */ /*utile?*/              
  SolverBlok *              bloktab;              /*+ Array of solver blocks                    +*/
  COEF *                    coeftab;              /*+ Coefficients access vector                +*/

  flag_t                    virtual;
} SolverMatrix;



typedef struct VSolverMatrix_ {
  SymbolMatrix              symbmtx;              /*+ Symbolic matrix                           +*/
  SolverMatrix*             solvmtx;
} VSolverMatrix;

#endif

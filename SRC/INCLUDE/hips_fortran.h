/* @authors J. GAIDAMOUR, P. HENON */

#ifndef HIPS_FORTRAN_H

#define HIPS_FORTRAN_H

#define DISABLE_C
#include "hips.h"

/*#if defined(SP2) || defined(RS6000) || defined(AIX) 
#define FORTRAN_NAME(name) name  
#else
#define FORTRAN_NAME(name) name ## _

#endif*/


/** Macro from Scotch **/

#define FORTRAN_NAME(nu,nl,pl,pc)                     \
void nu pl;                                      \
void nl pl                                       \
{ nu pc; }                                       \
void nl##_ pl                                    \
{ nu pc; }                                       \
void nl##__ pl                                   \
{ nu pc; }                                       \
void nu pl


/** NE PAS METTRE
void FORTRAN_NAME(HIPS_INITIALIZE)(INTS *nid);
void FORTRAN_NAME(HIPS_SET_DEFAULT_OPTIONS)(INTS *id, INTS *stratnum);
void FORTRAN_NAME(HIPS_SET_OPTION_INT)(INTS *id, INTS *number, INTS *value);
void FORTRAN_NAME(HIPS_SET_OPTION_INT)(INTS *id, INTS *number, INTS *value);
void FORTRAN_NAME(HIPS_SET_OPTION_REAL)(INTS *id, INTS *number, double *value);
void FORTRAN_NAME(HIPS_GRAPH_SET_GLOBAL_CSR)(INTS *id, INTS *n, INTL *ig, INTS *jg, INTS *dof);
void FORTRAN_NAME(HIPS_GRAPH_BUILD_HID)(INTS *id);
void FORTRAN_NAME(HIPS_SAVE_HIERACH_GRAPH_DECOMP)(INTS *id, char* sfile_path);
void FORTRAN_NAME(HIPS_LOAD_HIERACH_GRAPH_DECOMP)(INTS *id, char* sfile_path);
void FORTRAN_NAME(HIPS_PARALLEL_SETUP)(INTS *id);
void FORTRAN_NAME(HIPS_SET_COMMUNICATOR)(INTS *id, MPI_Fint *commid);
void FORTRAN_NAME(HIPS_SET_MATRIX_COEF)(INTS *id, INTS *job, INTS *over_add, INTS *ln, INTL *lia, INTS *lja, COEF *la, INTS *sym_matrix, INTS *nodelist);
void FORTRAN_NAME(HIPS_SET_MATRIX_COEF_DG)(INTS *id, INTS *job, INTS *over_add, INTS *ln, INTL *lia, INTS *ja, COEF *la, INTS *sym_matrix, INTS *nodelist);
void FORTRAN_NAME(HIPS_MATRIX_RESET)(INTS *id);
void FORTRAN_NAME(HIPS_PRECOND)(INTS *id);
void FORTRAN_NAME(HIPS_MATRIX_VECTOR_PRODUCT)(INTS *id, COEF *x, COEF *y);
void FORTRAN_NAME(HIPS_SET_LOCAL_RHS)(INTS *id, INTS *job, COEF *b);
void FORTRAN_NAME(HIPS_SOLVE)(INTS *id);
void FORTRAN_NAME(HIPS_GET_LOCAL_SOL)(INTS *id, COEF *x);
void FORTRAN_NAME(HIPS_GET_GLOBAL_SOL)(INTS *id, INTS *proc_root, COEF *globx);

void FORTRAN_NAME(HIPS_SET_PARTITION)(INTS *id, INTS *overlap, INTS *ndom, INTS *mapptr, INTS *mapp);
void FORTRAN_NAME(HIPS_CLEAN)(INTS *id);
void FORTRAN_NAME(HIPS_FINALIZE)();
**/
#endif

/* @authors J. GAIDAMOUR, P. HENON */

#ifndef _BASE_HEADER_D
#define _BASE_HEADER_D

#include "base_tmp.h" /* TODO */

/************************************************************/
/* BLAS C Interfaces                                        */
/*                                                          */
/* Example of interface to use :                            */
/*  BLAS_DDOT(int n, double *x, int incx, double *y,        */
/*                                              int incy);  */
/*                                                          */
/* For portability you *must* always use variables in       */
/*   arguments : int UN=1; BLAS_DDOT(n, x, UN, y, UN);      */
/*                                                          */
/************************************************************/

/************************************************************/
/* $Id: base.template.h,v 1.1 2006/06/01 11:55:43 gaidamou Exp $ */
/************************************************************/

/************************************************************/
/* Help to read this file :                                 */
/*                                                          */
/* If C interface is avaible on the architecture :          */
/*  BLAS_DDOT is redefined as ddot/ddot_/...                */ 
/*                                                          */
/* Else                                                     */
/*  BLAS_DDOT macro calls BLAS_FDDOT and converts argument  */
/*            types for the FORTRAN interface.              */
/*  BLAS_FDDOT is redefined as ddot/ddot_/...               */
/*                                                          */
/* For CBLAS, BLAS_DDOT macro calls cblas_ddot.             */
/************************************************************/

#ifndef DEBUG_NOBLAS



/********************** CBLAS ATLAS *************************/
#if defined(CBLAS)
#define _C_INTERFACE_
#include <cblas.h>

/********************** SP2 RS6000 **************************/
#elif defined(IBM) || defined(SP2) || defined(RS6000)
#define _C_INTERFACE_
#include <essl.h>
/*  EX : #define BLAS_DDOT ddot */
#define BLAS_DDOT ddot
#define BLAS_DNRM2 dnrm2
#define BLAS_DASUM dasum
#define BLAS_DSWAP dswap
#define BLAS_DCOPY dcopy
#define BLAS_DAXPY daxpy
#define BLAS_DSCAL dscal
#define BLAS_DGEMV dgemv
#define BLAS_DGBMV dgbmv
#define BLAS_DTRMV dtrmv
#define BLAS_DTBMV dtbmv
#define BLAS_DTPMV dtpmv
#define BLAS_DTRSV dtrsv
#define BLAS_DTBSV dtbsv
#define BLAS_DTPSV dtpsv
#define BLAS_DSYMV dsymv
#define BLAS_DSBMV dsbmv
#define BLAS_DSPMV dspmv
#define BLAS_DGER dger
#define BLAS_DSYR dsyr
#define BLAS_DSPR dspr
#define BLAS_DSYR2 dsyr2
#define BLAS_DSPR2 dspr2
#define BLAS_DGEMM dgemm
#define BLAS_DSYMM dsymm
#define BLAS_DSYRK dsyrk
#define BLAS_DSYR2K dsyr2k
#define BLAS_DTRMM dtrmm
#define BLAS_DTRSM dtrsm


/********************** SGI *********************************/
#elif defined(SGI)
#define _C_INTERFACE_
#include <scsl_blas.h>
/*  EX : #define BLAS_DDOT ddot */
#define BLAS_DDOT ddot
#define BLAS_DNRM2 dnrm2
#define BLAS_DASUM dasum
#define BLAS_DSWAP dswap
#define BLAS_DCOPY dcopy
#define BLAS_DAXPY daxpy
#define BLAS_DSCAL dscal
#define BLAS_DGEMV dgemv
#define BLAS_DGBMV dgbmv
#define BLAS_DTRMV dtrmv
#define BLAS_DTBMV dtbmv
#define BLAS_DTPMV dtpmv
#define BLAS_DTRSV dtrsv
#define BLAS_DTBSV dtbsv
#define BLAS_DTPSV dtpsv
#define BLAS_DSYMV dsymv
#define BLAS_DSBMV dsbmv
#define BLAS_DSPMV dspmv
#define BLAS_DGER dger
#define BLAS_DSYR dsyr
#define BLAS_DSPR dspr
#define BLAS_DSYR2 dsyr2
#define BLAS_DSPR2 dspr2
#define BLAS_DGEMM dgemm
#define BLAS_DSYMM dsymm
#define BLAS_DSYRK dsyrk
#define BLAS_DSYR2K dsyr2k
#define BLAS_DTRMM dtrmm
#define BLAS_DTRSM dtrsm


/********************** CRAY ********************************/
#elif defined(CRAY)
/*  EX : #define BLAS_FDDOT DDOT */
#define BLAS_FDDOT DDOT
#define BLAS_FDNRM2 DNRM2
#define BLAS_FDASUM DASUM
#define BLAS_FDSWAP DSWAP
#define BLAS_FDCOPY DCOPY
#define BLAS_FDAXPY DAXPY
#define BLAS_FDSCAL DSCAL
#define BLAS_FDGEMV DGEMV
#define BLAS_FDGBMV DGBMV
#define BLAS_FDTRMV DTRMV
#define BLAS_FDTBMV DTBMV
#define BLAS_FDTPMV DTPMV
#define BLAS_FDTRSV DTRSV
#define BLAS_FDTBSV DTBSV
#define BLAS_FDTPSV DTPSV
#define BLAS_FDSYMV DSYMV
#define BLAS_FDSBMV DSBMV
#define BLAS_FDSPMV DSPMV
#define BLAS_FDGER DGER
#define BLAS_FDSYR DSYR
#define BLAS_FDSPR DSPR
#define BLAS_FDSYR2 DSYR2
#define BLAS_FDSPR2 DSPR2
#define BLAS_FDGEMM DGEMM
#define BLAS_FDSYMM DSYMM
#define BLAS_FDSYRK DSYRK
#define BLAS_FDSYR2K DSYR2K
#define BLAS_FDTRMM DTRMM
#define BLAS_FDTRSM DTRSM

/********************** MKL *******************************/
#elif defined(MKL)
/* EX : #define BLAS_FDDOT ddot */
#define BLAS_FDDOT ddot
#define BLAS_FDNRM2 dnrm2
#define BLAS_FDASUM dasum
#define BLAS_FDSWAP dswap
#define BLAS_FDCOPY dcopy
#define BLAS_FDAXPY daxpy
#define BLAS_FDSCAL dscal
#define BLAS_FDGEMV dgemv
#define BLAS_FDGBMV dgbmv
#define BLAS_FDTRMV dtrmv
#define BLAS_FDTBMV dtbmv
#define BLAS_FDTPMV dtpmv
#define BLAS_FDTRSV dtrsv
#define BLAS_FDTBSV dtbsv
#define BLAS_FDTPSV dtpsv
#define BLAS_FDSYMV dsymv
#define BLAS_FDSBMV dsbmv
#define BLAS_FDSPMV dspmv
#define BLAS_FDGER dger
#define BLAS_FDSYR dsyr
#define BLAS_FDSPR dspr
#define BLAS_FDSYR2 dsyr2
#define BLAS_FDSPR2 dspr2
#define BLAS_FDGEMM dgemm
#define BLAS_FDSYMM dsymm
#define BLAS_FDSYRK dsyrk
#define BLAS_FDSYR2K dsyr2k
#define BLAS_FDTRMM dtrmm
#define BLAS_FDTRSM dtrsm

/********************** LINUX *******************************/
#elif defined(LINUX)
/* EX : #define BLAS_FDDOT ddot_ */
#define BLAS_FDDOT ddot_
#define BLAS_FDNRM2 dnrm2_
#define BLAS_FDASUM dasum_
#define BLAS_FDSWAP dswap_
#define BLAS_FDCOPY dcopy_
#define BLAS_FDAXPY daxpy_
#define BLAS_FDSCAL dscal_
#define BLAS_FDGEMV dgemv_
#define BLAS_FDGBMV dgbmv_
#define BLAS_FDTRMV dtrmv_
#define BLAS_FDTBMV dtbmv_
#define BLAS_FDTPMV dtpmv_
#define BLAS_FDTRSV dtrsv_
#define BLAS_FDTBSV dtbsv_
#define BLAS_FDTPSV dtpsv_
#define BLAS_FDSYMV dsymv_
#define BLAS_FDSBMV dsbmv_
#define BLAS_FDSPMV dspmv_
#define BLAS_FDGER dger_
#define BLAS_FDSYR dsyr_
#define BLAS_FDSPR dspr_
#define BLAS_FDSYR2 dsyr2_
#define BLAS_FDSPR2 dspr2_
#define BLAS_FDGEMM dgemm_
#define BLAS_FDSYMM dsymm_
#define BLAS_FDSYRK dsyrk_
#define BLAS_FDSYR2K dsyr2k_
#define BLAS_FDTRMM dtrmm_
#define BLAS_FDTRSM dtrsm_

#endif /* defined(..arch..) */

#ifndef _C_INTERFACE_
/* FORTRAN subroutine declarations */
/* EX : extern double BLAS_FDDOT(int *n, double *x, int *incx, double *y, int *incy);  */
extern double BLAS_FDDOT(int *N, const double *X, int *incX, const double *Y, int *incY);
extern double BLAS_FDNRM2(int *N, const double *X, int *incX);
extern double BLAS_FDASUM(int *N, const double *X, int *incX);
extern void BLAS_FDSWAP(int *N, double *X, int *incX, double *Y, int *incY);
extern void BLAS_FDCOPY(int *N, const double *X, int *incX, double *Y, int *incY);
extern void BLAS_FDAXPY(int *N, const double* alpha, const double *X, int *incX, double *Y, int *incY);
extern void BLAS_FDSCAL(int *N, const double* alpha, double *X, int *incX);
extern void BLAS_FDGEMV(char *TransA, int *M, int *N, const double* alpha, const double *A, int *lda, const double *X, int *incX, const double* beta, double *Y, int *incY);
extern void BLAS_FDGBMV(char *TransA, int *M, int *N, int *KL, int *KU, const double* alpha, const double *A, int *lda, const double *X, int *incX, const double* beta, double *Y, int *incY);
extern void BLAS_FDTRMV(char *Uplo, char *TransA, char *Diag, int *N, const double *A, int *lda, double *X, int *incX);
extern void BLAS_FDTBMV(char *Uplo, char *TransA, char *Diag, int *N, int *K, const double *A, int *lda, double *X, int *incX);
extern void BLAS_FDTPMV(char *Uplo, char *TransA, char *Diag, int *N, const double *Ap, double *X, int *incX);
extern void BLAS_FDTRSV(char *Uplo, char *TransA, char *Diag, int *N, const double *A, int *lda, double *X, int *incX);
extern void BLAS_FDTBSV(char *Uplo, char *TransA, char *Diag, int *N, int *K, const double *A, int *lda, double *X, int *incX);
extern void BLAS_FDTPSV(char *Uplo, char *TransA, char *Diag, int *N, const double *Ap, double *X, int *incX);
extern void BLAS_FDSYMV(char *Uplo, int *N, const double* alpha, const double *A, int *lda, const double *X, int *incX, const double* beta, double *Y, int *incY);
extern void BLAS_FDSBMV(char *Uplo, int *N, int *K, const double* alpha, const double *A, int *lda, const double *X, int *incX, const double* beta, double *Y, int *incY);
extern void BLAS_FDSPMV(char *Uplo, int *N, const double* alpha, const double *Ap, const double *X, int *incX, const double* beta, double *Y, int *incY);
extern void BLAS_FDGER(int *M, int *N, const double* alpha, const double *X, int *incX, const double *Y, int *incY, double *A, int *lda);
extern void BLAS_FDSYR(char *Uplo, int *N, const double* alpha, const double *X, int *incX, double *A, int *lda);
extern void BLAS_FDSPR(char *Uplo, int *N, const double* alpha, const double *X, int *incX, double *Ap);
extern void BLAS_FDSYR2(char *Uplo, int *N, const double* alpha, const double *X, int *incX, const double *Y, int *incY, double *A, int *lda);
extern void BLAS_FDSPR2(char *Uplo, int *N, const double* alpha, const double *X, int *incX, const double *Y, int *incY, double *A);
extern void BLAS_FDGEMM(char *TransA, char *TransB, int *M, int *N, int *K, const double* alpha, const double *A, int *lda, const double *B, int *ldb, const double* beta, double *C, int *ldc);
extern void BLAS_FDSYMM(char *Side, char *Uplo, int *M, int *N, const double* alpha, const double *A, int *lda, const double *B, int *ldb, const double* beta, double *C, int *ldc);
extern void BLAS_FDSYRK(char *Uplo, char *Trans, int *N, int *K, const double* alpha, const double *A, int *lda, const double* beta, double *C, int *ldc);
extern void BLAS_FDSYR2K(char *Uplo, char *Trans, int *N, int *K, const double* alpha, const double *A, int *lda, const double *B, int *ldb, const double* beta, double *C, int *ldc);
extern void BLAS_FDTRMM(char *Side, char *Uplo, char *TransA, char *Diag, int *M, int *N, const double* alpha, const double *A, int *lda, double *B, int *ldb);
extern void BLAS_FDTRSM(char *Side, char *Uplo, char *TransA, char *Diag, int *M, int *N, const double* alpha, const double *A, int *lda, double *B, int *ldb);


/* Call FORTRAN subroutines by using C like interface */
/* EX : #define BLAS_DDOT(n,x,incx,y,incy) BLAS_FDDOT(&(n),(x),&(incx),(y),&(incy)) */
#define   BLAS_DDOT(N, X, incX, Y, incY) \
   BLAS_FDDOT(&(N), (X), &(incX), (Y), &(incY))
#define   BLAS_DNRM2(N, X, incX) \
   BLAS_FDNRM2(&(N), (X), &(incX))
#define   BLAS_DASUM(N, X, incX) \
   BLAS_FDASUM(&(N), (X), &(incX))
#define   BLAS_DSWAP(N, X, incX, Y, incY) \
   BLAS_FDSWAP(&(N), (X), &(incX), (Y), &(incY))
#define   BLAS_DCOPY(N, X, incX, Y, incY) \
   BLAS_FDCOPY(&(N), (X), &(incX), (Y), &(incY))
#define   BLAS_DAXPY(N, alpha, X, incX, Y, incY) \
   BLAS_FDAXPY(&(N), &(alpha), (X), &(incX), (Y), &(incY))
#define   BLAS_DSCAL(N, alpha, X, incX) \
   BLAS_FDSCAL(&(N), &(alpha), (X), &(incX))
#define   BLAS_DGEMV(TransA, M, N, alpha, A, lda, X, incX, beta, Y, incY) \
   BLAS_FDGEMV((TransA), &(M), &(N), &(alpha), (A), &(lda), (X), &(incX), &(beta), (Y), &(incY))
#define   BLAS_DGBMV(TransA, M, N, KL, KU, alpha, A, lda, X, incX, beta, Y, incY) \
   BLAS_FDGBMV((TransA), &(M), &(N), &(KL), &(KU), &(alpha), (A), &(lda), (X), &(incX), &(beta), (Y), &(incY))
#define   BLAS_DTRMV(Uplo, TransA, Diag, N, A, lda, X, incX) \
   BLAS_FDTRMV((Uplo), (TransA), (Diag), &(N), (A), &(lda), (X), &(incX))
#define   BLAS_DTBMV(Uplo, TransA, Diag, N, K, A, lda, X, incX) \
   BLAS_FDTBMV((Uplo), (TransA), (Diag), &(N), &(K), (A), &(lda), (X), &(incX))
#define   BLAS_DTPMV(Uplo, TransA, Diag, N, Ap, X, incX) \
   BLAS_FDTPMV((Uplo), (TransA), (Diag), &(N), (Ap), (X), &(incX))
#define   BLAS_DTRSV(Uplo, TransA, Diag, N, A, lda, X, incX) \
   BLAS_FDTRSV((Uplo), (TransA), (Diag), &(N), (A), &(lda), (X), &(incX))
#define   BLAS_DTBSV(Uplo, TransA, Diag, N, K, A, lda, X, incX) \
   BLAS_FDTBSV((Uplo), (TransA), (Diag), &(N), &(K), (A), &(lda), (X), &(incX))
#define   BLAS_DTPSV(Uplo, TransA, Diag, N, Ap, X, incX) \
   BLAS_FDTPSV((Uplo), (TransA), (Diag), &(N), (Ap), (X), &(incX))
#define   BLAS_DSYMV(Uplo, N, alpha, A, lda, X, incX, beta, Y, incY) \
   BLAS_FDSYMV((Uplo), &(N), &(alpha), (A), &(lda), (X), &(incX), &(beta), (Y), &(incY))
#define   BLAS_DSBMV(Uplo, N, K, alpha, A, lda, X, incX, beta, Y, incY) \
   BLAS_FDSBMV((Uplo), &(N), &(K), &(alpha), (A), &(lda), (X), &(incX), &(beta), (Y), &(incY))
#define   BLAS_DSPMV(Uplo, N, alpha, Ap, X, incX, beta, Y, incY) \
   BLAS_FDSPMV((Uplo), &(N), &(alpha), (Ap), (X), &(incX), &(beta), (Y), &(incY))
#define   BLAS_DGER(M, N, alpha, X, incX, Y, incY, A, lda) \
   BLAS_FDGER(&(M), &(N), &(alpha), (X), &(incX), (Y), &(incY), (A), &(lda))
#define   BLAS_DSYR(Uplo, N, alpha, X, incX, A, lda) \
   BLAS_FDSYR((Uplo), &(N), &(alpha), (X), &(incX), (A), &(lda))
#define   BLAS_DSPR(Uplo, N, alpha, X, incX, Ap) \
   BLAS_FDSPR((Uplo), &(N), &(alpha), (X), &(incX), (A)p)
#define   BLAS_DSYR2(Uplo, N, alpha, X, incX, Y, incY, A, lda) \
   BLAS_FDSYR2((Uplo), &(N), &(alpha), (X), &(incX), (Y), &(incY), (A), &(lda))
#define   BLAS_DSPR2(Uplo, N, alpha, X, incX, Y, incY, A) \
   BLAS_FDSPR2((Uplo), &(N), &(alpha), (X), &(incX), (Y), &(incY), (A))
#define   BLAS_DGEMM(TransA, TransB, M, N, K, alpha, A, lda, B, ldb, beta, C, ldc) \
   BLAS_FDGEMM((TransA), (TransB), &(M), &(N), &(K), &(alpha), (A), &(lda), (B), &(ldb), &(beta), (C), &(ldc))
#define   BLAS_DSYMM(Side, Uplo, M, N, alpha, A, lda, B, ldb, beta, C, ldc) \
   BLAS_FDSYMM((Side), (Uplo), &(M), &(N), &(alpha), (A), &(lda), (B), &(ldb), &(beta), (C), &(ldc))
#define   BLAS_DSYRK(Uplo, Trans, N, K, alpha, A, lda, beta, C, ldc) \
   BLAS_FDSYRK((Uplo), (Trans), &(N), &(K), &(alpha), (A), &(lda), &(beta), (C), &(ldc))
#define   BLAS_DSYR2K(Uplo, Trans, N, K, alpha, A, lda, B, ldb, beta, C, ldc) \
   BLAS_FDSYR2K((Uplo), (Trans), &(N), &(K), &(alpha), (A), &(lda), (B), &(ldb), &(beta), (C), &(ldc))
#define   BLAS_DTRMM(Side, Uplo, TransA, Diag, M, N, alpha, A, lda, B, ldb) \
   BLAS_FDTRMM((Side), (Uplo), (TransA), (Diag), &(M), &(N), &(alpha), (A), &(lda), (B), &(ldb))
#define   BLAS_DTRSM(Side, Uplo, TransA, Diag, M, N, alpha, A, lda, B, ldb) \
   BLAS_FDTRSM((Side), (Uplo), (TransA), (Diag), &(M), &(N), &(alpha), (A), &(lda), (B), &(ldb))

#endif /* _C_INTERFACE_ */

#if defined(CBLAS)
/* Translate common blas interface to cblas interface */
/* #define   BLAS_DDOT(N, X, incX, Y, incY) \
      cblas_ddot((N), (X), (incX), (Y), (incY)) */
#define   BLAS_DDOT(N, X, incX, Y, incY) \
   cblas_ddot((N), (X), (incX), (Y), (incY))
#define   BLAS_DNRM2(N, X, incX) \
   cblas_dnrm2((N), (X), (incX))
#define   BLAS_DASUM(N, X, incX) \
   cblas_dasum((N), (X), (incX))
#define   BLAS_DSWAP(N, X, incX, Y, incY) \
   cblas_dswap((N), (X), (incX), (Y), (incY))
#define   BLAS_DCOPY(N, X, incX, Y, incY) \
   cblas_dcopy((N), (X), (incX), (Y), (incY))
#define   BLAS_DAXPY(N, alpha, X, incX, Y, incY) \
   cblas_daxpy((N), (alpha), (X), (incX), (Y), (incY))
#define   BLAS_DSCAL(N, alpha, X, incX) \
   cblas_dscal((N), (alpha), (X), (incX))
#define   BLAS_DGEMV(TransA, M, N, alpha, A, lda, X, incX, beta, Y, incY) \
   cblas_dgemv(CblasColMajor, (TransA=="N")?CblasNoTrans:((TransA=="T")?CblasTrans:CblasConjTrans), (M), (N), (alpha), (A), (lda), (X), (incX), (beta), (Y), (incY))
#define   BLAS_DGBMV(TransA, M, N, KL, KU, alpha, A, lda, X, incX, beta, Y, incY) \
   cblas_dgbmv(CblasColMajor, (TransA=="N")?CblasNoTrans:((TransA=="T")?CblasTrans:CblasConjTrans), (M), (N), (KL), (KU), (alpha), (A), (lda), (X), (incX), (beta), (Y), (incY))
#define   BLAS_DTRMV(Uplo, TransA, Diag, N, A, lda, X, incX) \
   cblas_dtrmv(CblasColMajor, (Uplo=="U")?CblasUpper:CblasLower, (TransA=="N")?CblasNoTrans:((TransA=="T")?CblasTrans:CblasConjTrans), (Diag=="N")?CblasNonUnit:CblasUnit, (N), (A), (lda), (X), (incX))
#define   BLAS_DTBMV(Uplo, TransA, Diag, N, K, A, lda, X, incX) \
   cblas_dtbmv(CblasColMajor, (Uplo=="U")?CblasUpper:CblasLower, (TransA=="N")?CblasNoTrans:((TransA=="T")?CblasTrans:CblasConjTrans), (Diag=="N")?CblasNonUnit:CblasUnit, (N), (K), (A), (lda), (X), (incX))
#define   BLAS_DTPMV(Uplo, TransA, Diag, N, Ap, X, incX) \
   cblas_dtpmv(CblasColMajor, (Uplo=="U")?CblasUpper:CblasLower, (TransA=="N")?CblasNoTrans:((TransA=="T")?CblasTrans:CblasConjTrans), (Diag=="N")?CblasNonUnit:CblasUnit, (N), (Ap), (X), (incX))
#define   BLAS_DTRSV(Uplo, TransA, Diag, N, A, lda, X, incX) \
   cblas_dtrsv(CblasColMajor, (Uplo=="U")?CblasUpper:CblasLower, (TransA=="N")?CblasNoTrans:((TransA=="T")?CblasTrans:CblasConjTrans), (Diag=="N")?CblasNonUnit:CblasUnit, (N), (A), (lda), (X), (incX))
#define   BLAS_DTBSV(Uplo, TransA, Diag, N, K, A, lda, X, incX) \
   cblas_dtbsv(CblasColMajor, (Uplo=="U")?CblasUpper:CblasLower, (TransA=="N")?CblasNoTrans:((TransA=="T")?CblasTrans:CblasConjTrans), (Diag=="N")?CblasNonUnit:CblasUnit, (N), (K), (A), (lda), (X), (incX))
#define   BLAS_DTPSV(Uplo, TransA, Diag, N, Ap, X, incX) \
   cblas_dtpsv(CblasColMajor, (Uplo=="U")?CblasUpper:CblasLower, (TransA=="N")?CblasNoTrans:((TransA=="T")?CblasTrans:CblasConjTrans), (Diag=="N")?CblasNonUnit:CblasUnit, (N), (Ap), (X), (incX))
#define   BLAS_DSYMV(Uplo, N, alpha, A, lda, X, incX, beta, Y, incY) \
   cblas_dsymv(CblasColMajor, (Uplo=="U")?CblasUpper:CblasLower, (N), (alpha), (A), (lda), (X), (incX), (beta), (Y), (incY))
#define   BLAS_DSBMV(Uplo, N, K, alpha, A, lda, X, incX, beta, Y, incY) \
   cblas_dsbmv(CblasColMajor, (Uplo=="U")?CblasUpper:CblasLower, (N), (K), (alpha), (A), (lda), (X), (incX), (beta), (Y), (incY))
#define   BLAS_DSPMV(Uplo, N, alpha, Ap, X, incX, beta, Y, incY) \
   cblas_dspmv(CblasColMajor, (Uplo=="U")?CblasUpper:CblasLower, (N), (alpha), (Ap), (X), (incX), (beta), (Y), (incY))
#define   BLAS_DGER(M, N, alpha, X, incX, Y, incY, A, lda) \
   cblas_dger(CblasColMajor, (M), (N), (alpha), (X), (incX), (Y), (incY), (A), (lda))
#define   BLAS_DSYR(Uplo, N, alpha, X, incX, A, lda) \
   cblas_dsyr(CblasColMajor, (Uplo=="U")?CblasUpper:CblasLower, (N), (alpha), (X), (incX), (A), (lda))
#define   BLAS_DSPR(Uplo, N, alpha, X, incX, Ap) \
   cblas_dspr(CblasColMajor, (Uplo=="U")?CblasUpper:CblasLower, (N), (alpha), (X), (incX), (A)p)
#define   BLAS_DSYR2(Uplo, N, alpha, X, incX, Y, incY, A, lda) \
   cblas_dsyr2(CblasColMajor, (Uplo=="U")?CblasUpper:CblasLower, (N), (alpha), (X), (incX), (Y), (incY), (A), (lda))
#define   BLAS_DSPR2(Uplo, N, alpha, X, incX, Y, incY, A) \
   cblas_dspr2(CblasColMajor, (Uplo=="U")?CblasUpper:CblasLower, (N), (alpha), (X), (incX), (Y), (incY), (A))
#define   BLAS_DGEMM(TransA, TransB, M, N, K, alpha, A, lda, B, ldb, beta, C, ldc) \
   cblas_dgemm(CblasColMajor, (TransA=="N")?CblasNoTrans:((TransA=="T")?CblasTrans:CblasConjTrans), (TransB=="N")?CblasNoTrans:((TransB=="T")?CblasTrans:CblasConjTrans), (M), (N), (K), (alpha), (A), (lda), (B), (ldb), (beta), (C), (ldc))
#define   BLAS_DSYMM(Side, Uplo, M, N, alpha, A, lda, B, ldb, beta, C, ldc) \
   cblas_dsymm(CblasColMajor, (Side=="L")?CblasLeft:CblasRight, (Uplo=="U")?CblasUpper:CblasLower, (M), (N), (alpha), (A), (lda), (B), (ldb), (beta), (C), (ldc))
#define   BLAS_DSYRK(Uplo, Trans, N, K, alpha, A, lda, beta, C, ldc) \
   cblas_dsyrk(CblasColMajor, (Uplo=="U")?CblasUpper:CblasLower, (Trans =="N")?CblasNoTrans:((Trans =="T")?CblasTrans:CblasConjTrans), (N), (K), (alpha), (A), (lda), (beta), (C), (ldc))
#define   BLAS_DSYR2K(Uplo, Trans, N, K, alpha, A, lda, B, ldb, beta, C, ldc) \
   cblas_dsyr2k(CblasColMajor, (Uplo=="U")?CblasUpper:CblasLower, (Trans =="N")?CblasNoTrans:((Trans =="T")?CblasTrans:CblasConjTrans), (N), (K), (alpha), (A), (lda), (B), (ldb), (beta), (C), (ldc))
#define   BLAS_DTRMM(Side, Uplo, TransA, Diag, M, N, alpha, A, lda, B, ldb) \
   cblas_dtrmm(CblasColMajor, (Side=="L")?CblasLeft:CblasRight, (Uplo=="U")?CblasUpper:CblasLower, (TransA=="N")?CblasNoTrans:((TransA=="T")?CblasTrans:CblasConjTrans), (Diag=="N")?CblasNonUnit:CblasUnit, (M), (N), (alpha), (A), (lda), (B), (ldb))
#define   BLAS_DTRSM(Side, Uplo, TransA, Diag, M, N, alpha, A, lda, B, ldb) \
   cblas_dtrsm(CblasColMajor, (Side=="L")?CblasLeft:CblasRight, (Uplo=="U")?CblasUpper:CblasLower, (TransA=="N")?CblasNoTrans:((TransA=="T")?CblasTrans:CblasConjTrans), (Diag=="N")?CblasNonUnit:CblasUnit, (M), (N), (alpha), (A), (lda), (B), (ldb))

#endif /* defined(CBLAS) */



#else /* DEBUG_NOBLAS */
/* For debug purpose : Skip every BLAS calls */
/* #define   BLAS_DDOT(N, X, incX, Y, incY)  1 */
#define   BLAS_DDOT(N, X, incX, Y, incY)  1
#define   BLAS_DNRM2(N, X, incX)  1
#define   BLAS_DASUM(N, X, incX)  1
#define   BLAS_DSWAP(N, X, incX, Y, incY)  1
#define   BLAS_DCOPY(N, X, incX, Y, incY)  1
#define   BLAS_DAXPY(N, alpha, X, incX, Y, incY)  1
#define   BLAS_DSCAL(N, alpha, X, incX)  1
#define   BLAS_DGEMV(TransA, M, N, alpha, A, lda, X, incX, beta, Y, incY)  1
#define   BLAS_DGBMV(TransA, M, N, KL, KU, alpha, A, lda, X, incX, beta, Y, incY)  1
#define   BLAS_DTRMV(Uplo, TransA, Diag, N, A, lda, X, incX)  1
#define   BLAS_DTBMV(Uplo, TransA, Diag, N, K, A, lda, X, incX)  1
#define   BLAS_DTPMV(Uplo, TransA, Diag, N, Ap, X, incX)  1
#define   BLAS_DTRSV(Uplo, TransA, Diag, N, A, lda, X, incX)  1
#define   BLAS_DTBSV(Uplo, TransA, Diag, N, K, A, lda, X, incX)  1
#define   BLAS_DTPSV(Uplo, TransA, Diag, N, Ap, X, incX)  1
#define   BLAS_DSYMV(Uplo, N, alpha, A, lda, X, incX, beta, Y, incY)  1
#define   BLAS_DSBMV(Uplo, N, K, alpha, A, lda, X, incX, beta, Y, incY)  1
#define   BLAS_DSPMV(Uplo, N, alpha, Ap, X, incX, beta, Y, incY)  1
#define   BLAS_DGER(M, N, alpha, X, incX, Y, incY, A, lda)  1
#define   BLAS_DSYR(Uplo, N, alpha, X, incX, A, lda)  1
#define   BLAS_DSPR(Uplo, N, alpha, X, incX, Ap)  1
#define   BLAS_DSYR2(Uplo, N, alpha, X, incX, Y, incY, A, lda)  1
#define   BLAS_DSPR2(Uplo, N, alpha, X, incX, Y, incY, A)  1
#define   BLAS_DGEMM(TransA, TransB, M, N, K, alpha, A, lda, B, ldb, beta, C, ldc)  1
#define   BLAS_DSYMM(Side, Uplo, M, N, alpha, A, lda, B, ldb, beta, C, ldc)  1
#define   BLAS_DSYRK(Uplo, Trans, N, K, alpha, A, lda, beta, C, ldc)  1
#define   BLAS_DSYR2K(Uplo, Trans, N, K, alpha, A, lda, B, ldb, beta, C, ldc)  1
#define   BLAS_DTRMM(Side, Uplo, TransA, Diag, M, N, alpha, A, lda, B, ldb)  1
#define   BLAS_DTRSM(Side, Uplo, TransA, Diag, M, N, alpha, A, lda, B, ldb)  1

#endif /* DEBUG_NOBLAS */

#endif /* _BASE_HEADER_D */

#ifndef _BASE_HEADER_S
#define _BASE_HEADER_S

#include "base_tmp.h" /* TODO */

/************************************************************/
/* BLAS C Interfaces                                        */
/*                                                          */
/* Example of interface to use :                            */
/*  BLAS_DDOT(int n, double *x, int incx, double *y,        */
/*                                              int incy);  */
/*                                                          */
/* For portability you *must* always use variables in       */
/*   arguments : int UN=1; BLAS_DDOT(n, x, UN, y, UN);      */
/*                                                          */
/************************************************************/

/************************************************************/
/* $Id: base.template.h,v 1.1 2006/06/01 11:55:43 gaidamou Exp $ */
/************************************************************/

/************************************************************/
/* Help to read this file :                                 */
/*                                                          */
/* If C interface is avaible on the architecture :          */
/*  BLAS_DDOT is redefined as ddot/ddot_/...                */ 
/*                                                          */
/* Else                                                     */
/*  BLAS_DDOT macro calls BLAS_FDDOT and converts argument  */
/*            types for the FORTRAN interface.              */
/*  BLAS_FDDOT is redefined as ddot/ddot_/...               */
/*                                                          */
/* For CBLAS, BLAS_DDOT macro calls cblas_ddot.             */
/************************************************************/

#ifndef DEBUG_NOBLAS



/********************** CBLAS ATLAS *************************/
#if defined(CBLAS)
#define _C_INTERFACE_
#include <cblas.h>

/********************** SP2 RS6000 **************************/
#elif defined(SP2) || defined(RS6000)
#define _C_INTERFACE_
#include <essl.h>
/*  EX : #define BLAS_DDOT ddot */
#define BLAS_SDOT sdot
#define BLAS_SNRM2 snrm2
#define BLAS_SASUM sasum
#define BLAS_SSWAP sswap
#define BLAS_SCOPY scopy
#define BLAS_SAXPY saxpy
#define BLAS_SSCAL sscal
#define BLAS_SGEMV sgemv
#define BLAS_SGBMV sgbmv
#define BLAS_STRMV strmv
#define BLAS_STBMV stbmv
#define BLAS_STPMV stpmv
#define BLAS_STRSV strsv
#define BLAS_STBSV stbsv
#define BLAS_STPSV stpsv
#define BLAS_SSYMV ssymv
#define BLAS_SSBMV ssbmv
#define BLAS_SSPMV sspmv
#define BLAS_SGER sger
#define BLAS_SSYR ssyr
#define BLAS_SSPR sspr
#define BLAS_SSYR2 ssyr2
#define BLAS_SSPR2 sspr2
#define BLAS_SGEMM sgemm
#define BLAS_SSYMM ssymm
#define BLAS_SSYRK ssyrk
#define BLAS_SSYR2K ssyr2k
#define BLAS_STRMM strmm
#define BLAS_STRSM strsm


/********************** SGI *********************************/
#elif defined(SGI)
#define _C_INTERFACE_
#include <scsl_blas.h>
/*  EX : #define BLAS_DDOT ddot */
#define BLAS_SDOT sdot
#define BLAS_SNRM2 snrm2
#define BLAS_SASUM sasum
#define BLAS_SSWAP sswap
#define BLAS_SCOPY scopy
#define BLAS_SAXPY saxpy
#define BLAS_SSCAL sscal
#define BLAS_SGEMV sgemv
#define BLAS_SGBMV sgbmv
#define BLAS_STRMV strmv
#define BLAS_STBMV stbmv
#define BLAS_STPMV stpmv
#define BLAS_STRSV strsv
#define BLAS_STBSV stbsv
#define BLAS_STPSV stpsv
#define BLAS_SSYMV ssymv
#define BLAS_SSBMV ssbmv
#define BLAS_SSPMV sspmv
#define BLAS_SGER sger
#define BLAS_SSYR ssyr
#define BLAS_SSPR sspr
#define BLAS_SSYR2 ssyr2
#define BLAS_SSPR2 sspr2
#define BLAS_SGEMM sgemm
#define BLAS_SSYMM ssymm
#define BLAS_SSYRK ssyrk
#define BLAS_SSYR2K ssyr2k
#define BLAS_STRMM strmm
#define BLAS_STRSM strsm


/********************** CRAY ********************************/
#elif defined(CRAY)
/*  EX : #define BLAS_FDDOT DDOT */
#define BLAS_FSDOT SDOT
#define BLAS_FSNRM2 SNRM2
#define BLAS_FSASUM SASUM
#define BLAS_FSSWAP SSWAP
#define BLAS_FSCOPY SCOPY
#define BLAS_FSAXPY SAXPY
#define BLAS_FSSCAL SSCAL
#define BLAS_FSGEMV SGEMV
#define BLAS_FSGBMV SGBMV
#define BLAS_FSTRMV STRMV
#define BLAS_FSTBMV STBMV
#define BLAS_FSTPMV STPMV
#define BLAS_FSTRSV STRSV
#define BLAS_FSTBSV STBSV
#define BLAS_FSTPSV STPSV
#define BLAS_FSSYMV SSYMV
#define BLAS_FSSBMV SSBMV
#define BLAS_FSSPMV SSPMV
#define BLAS_FSGER SGER
#define BLAS_FSSYR SSYR
#define BLAS_FSSPR SSPR
#define BLAS_FSSYR2 SSYR2
#define BLAS_FSSPR2 SSPR2
#define BLAS_FSGEMM SGEMM
#define BLAS_FSSYMM SSYMM
#define BLAS_FSSYRK SSYRK
#define BLAS_FSSYR2K SSYR2K
#define BLAS_FSTRMM STRMM
#define BLAS_FSTRSM STRSM


/********************** LINUX *******************************/
#elif defined(LINUX)
/* EX : #define BLAS_FDDOT ddot_ */
#define BLAS_FSDOT sdot_
#define BLAS_FSNRM2 snrm2_
#define BLAS_FSASUM sasum_
#define BLAS_FSSWAP sswap_
#define BLAS_FSCOPY scopy_
#define BLAS_FSAXPY saxpy_
#define BLAS_FSSCAL sscal_
#define BLAS_FSGEMV sgemv_
#define BLAS_FSGBMV sgbmv_
#define BLAS_FSTRMV strmv_
#define BLAS_FSTBMV stbmv_
#define BLAS_FSTPMV stpmv_
#define BLAS_FSTRSV strsv_
#define BLAS_FSTBSV stbsv_
#define BLAS_FSTPSV stpsv_
#define BLAS_FSSYMV ssymv_
#define BLAS_FSSBMV ssbmv_
#define BLAS_FSSPMV sspmv_
#define BLAS_FSGER sger_
#define BLAS_FSSYR ssyr_
#define BLAS_FSSPR sspr_
#define BLAS_FSSYR2 ssyr2_
#define BLAS_FSSPR2 sspr2_
#define BLAS_FSGEMM sgemm_
#define BLAS_FSSYMM ssymm_
#define BLAS_FSSYRK ssyrk_
#define BLAS_FSSYR2K ssyr2k_
#define BLAS_FSTRMM strmm_
#define BLAS_FSTRSM strsm_

#endif /* defined(..arch..) */

#ifndef _C_INTERFACE_
/* FORTRAN subroutine declarations */
/* EX : extern double BLAS_FDDOT(int *n, double *x, int *incx, double *y, int *incy);  */
extern float BLAS_FSDOT(int *N, const float *X, int *incX, const float *Y, int *incY);
extern float BLAS_FSNRM2(int *N, const float *X, int *incX);
extern float BLAS_FSASUM(int *N, const float *X, int *incX);
extern void BLAS_FSSWAP(int *N, float *X, int *incX, float *Y, int *incY);
extern void BLAS_FSCOPY(int *N, const float *X, int *incX, float *Y, int *incY);
extern void BLAS_FSAXPY(int *N, const float* alpha, const float *X, int *incX, float *Y, int *incY);
extern void BLAS_FSSCAL(int *N, const float* alpha, float *X, int *incX);
extern void BLAS_FSGEMV(char *TransA, int *M, int *N, const float* alpha, const float *A, int *lda, const float *X, int *incX, const float* beta, float *Y, int *incY);
extern void BLAS_FSGBMV(char *TransA, int *M, int *N, int *KL, int *KU, const float* alpha, const float *A, int *lda, const float *X, int *incX, const float* beta, float *Y, int *incY);
extern void BLAS_FSTRMV(char *Uplo, char *TransA, char *Diag, int *N, const float *A, int *lda, float *X, int *incX);
extern void BLAS_FSTBMV(char *Uplo, char *TransA, char *Diag, int *N, int *K, const float *A, int *lda, float *X, int *incX);
extern void BLAS_FSTPMV(char *Uplo, char *TransA, char *Diag, int *N, const float *Ap, float *X, int *incX);
extern void BLAS_FSTRSV(char *Uplo, char *TransA, char *Diag, int *N, const float *A, int *lda, float *X, int *incX);
extern void BLAS_FSTBSV(char *Uplo, char *TransA, char *Diag, int *N, int *K, const float *A, int *lda, float *X, int *incX);
extern void BLAS_FSTPSV(char *Uplo, char *TransA, char *Diag, int *N, const float *Ap, float *X, int *incX);
extern void BLAS_FSSYMV(char *Uplo, int *N, const float* alpha, const float *A, int *lda, const float *X, int *incX, const float* beta, float *Y, int *incY);
extern void BLAS_FSSBMV(char *Uplo, int *N, int *K, const float* alpha, const float *A, int *lda, const float *X, int *incX, const float* beta, float *Y, int *incY);
extern void BLAS_FSSPMV(char *Uplo, int *N, const float* alpha, const float *Ap, const float *X, int *incX, const float* beta, float *Y, int *incY);
extern void BLAS_FSGER(int *M, int *N, const float* alpha, const float *X, int *incX, const float *Y, int *incY, float *A, int *lda);
extern void BLAS_FSSYR(char *Uplo, int *N, const float* alpha, const float *X, int *incX, float *A, int *lda);
extern void BLAS_FSSPR(char *Uplo, int *N, const float* alpha, const float *X, int *incX, float *Ap);
extern void BLAS_FSSYR2(char *Uplo, int *N, const float* alpha, const float *X, int *incX, const float *Y, int *incY, float *A, int *lda);
extern void BLAS_FSSPR2(char *Uplo, int *N, const float* alpha, const float *X, int *incX, const float *Y, int *incY, float *A);
extern void BLAS_FSGEMM(char *TransA, char *TransB, int *M, int *N, int *K, const float* alpha, const float *A, int *lda, const float *B, int *ldb, const float* beta, float *C, int *ldc);
extern void BLAS_FSSYMM(char *Side, char *Uplo, int *M, int *N, const float* alpha, const float *A, int *lda, const float *B, int *ldb, const float* beta, float *C, int *ldc);
extern void BLAS_FSSYRK(char *Uplo, char *Trans, int *N, int *K, const float* alpha, const float *A, int *lda, const float* beta, float *C, int *ldc);
extern void BLAS_FSSYR2K(char *Uplo, char *Trans, int *N, int *K, const float* alpha, const float *A, int *lda, const float *B, int *ldb, const float* beta, float *C, int *ldc);
extern void BLAS_FSTRMM(char *Side, char *Uplo, char *TransA, char *Diag, int *M, int *N, const float* alpha, const float *A, int *lda, float *B, int *ldb);
extern void BLAS_FSTRSM(char *Side, char *Uplo, char *TransA, char *Diag, int *M, int *N, const float* alpha, const float *A, int *lda, float *B, int *ldb);


/* Call FORTRAN subroutines by using C like interface */
/* EX : #define BLAS_DDOT(n,x,incx,y,incy) BLAS_FDDOT(&(n),(x),&(incx),(y),&(incy)) */
#define   BLAS_SDOT(N, X, incX, Y, incY) \
   BLAS_FSDOT(&(N), (X), &(incX), (Y), &(incY))
#define   BLAS_SNRM2(N, X, incX) \
   BLAS_FSNRM2(&(N), (X), &(incX))
#define   BLAS_SASUM(N, X, incX) \
   BLAS_FSASUM(&(N), (X), &(incX))
#define   BLAS_SSWAP(N, X, incX, Y, incY) \
   BLAS_FSSWAP(&(N), (X), &(incX), (Y), &(incY))
#define   BLAS_SCOPY(N, X, incX, Y, incY) \
   BLAS_FSCOPY(&(N), (X), &(incX), (Y), &(incY))
#define   BLAS_SAXPY(N, alpha, X, incX, Y, incY) \
   BLAS_FSAXPY(&(N), &(alpha), (X), &(incX), (Y), &(incY))
#define   BLAS_SSCAL(N, alpha, X, incX) \
   BLAS_FSSCAL(&(N), &(alpha), (X), &(incX))
#define   BLAS_SGEMV(TransA, M, N, alpha, A, lda, X, incX, beta, Y, incY) \
   BLAS_FSGEMV((TransA), &(M), &(N), &(alpha), (A), &(lda), (X), &(incX), &(beta), (Y), &(incY))
#define   BLAS_SGBMV(TransA, M, N, KL, KU, alpha, A, lda, X, incX, beta, Y, incY) \
   BLAS_FSGBMV((TransA), &(M), &(N), &(KL), &(KU), &(alpha), (A), &(lda), (X), &(incX), &(beta), (Y), &(incY))
#define   BLAS_STRMV(Uplo, TransA, Diag, N, A, lda, X, incX) \
   BLAS_FSTRMV((Uplo), (TransA), (Diag), &(N), (A), &(lda), (X), &(incX))
#define   BLAS_STBMV(Uplo, TransA, Diag, N, K, A, lda, X, incX) \
   BLAS_FSTBMV((Uplo), (TransA), (Diag), &(N), &(K), (A), &(lda), (X), &(incX))
#define   BLAS_STPMV(Uplo, TransA, Diag, N, Ap, X, incX) \
   BLAS_FSTPMV((Uplo), (TransA), (Diag), &(N), (Ap), (X), &(incX))
#define   BLAS_STRSV(Uplo, TransA, Diag, N, A, lda, X, incX) \
   BLAS_FSTRSV((Uplo), (TransA), (Diag), &(N), (A), &(lda), (X), &(incX))
#define   BLAS_STBSV(Uplo, TransA, Diag, N, K, A, lda, X, incX) \
   BLAS_FSTBSV((Uplo), (TransA), (Diag), &(N), &(K), (A), &(lda), (X), &(incX))
#define   BLAS_STPSV(Uplo, TransA, Diag, N, Ap, X, incX) \
   BLAS_FSTPSV((Uplo), (TransA), (Diag), &(N), (Ap), (X), &(incX))
#define   BLAS_SSYMV(Uplo, N, alpha, A, lda, X, incX, beta, Y, incY) \
   BLAS_FSSYMV((Uplo), &(N), &(alpha), (A), &(lda), (X), &(incX), &(beta), (Y), &(incY))
#define   BLAS_SSBMV(Uplo, N, K, alpha, A, lda, X, incX, beta, Y, incY) \
   BLAS_FSSBMV((Uplo), &(N), &(K), &(alpha), (A), &(lda), (X), &(incX), &(beta), (Y), &(incY))
#define   BLAS_SSPMV(Uplo, N, alpha, Ap, X, incX, beta, Y, incY) \
   BLAS_FSSPMV((Uplo), &(N), &(alpha), (Ap), (X), &(incX), &(beta), (Y), &(incY))
#define   BLAS_SGER(M, N, alpha, X, incX, Y, incY, A, lda) \
   BLAS_FSGER(&(M), &(N), &(alpha), (X), &(incX), (Y), &(incY), (A), &(lda))
#define   BLAS_SSYR(Uplo, N, alpha, X, incX, A, lda) \
   BLAS_FSSYR((Uplo), &(N), &(alpha), (X), &(incX), (A), &(lda))
#define   BLAS_SSPR(Uplo, N, alpha, X, incX, Ap) \
   BLAS_FSSPR((Uplo), &(N), &(alpha), (X), &(incX), (A)p)
#define   BLAS_SSYR2(Uplo, N, alpha, X, incX, Y, incY, A, lda) \
   BLAS_FSSYR2((Uplo), &(N), &(alpha), (X), &(incX), (Y), &(incY), (A), &(lda))
#define   BLAS_SSPR2(Uplo, N, alpha, X, incX, Y, incY, A) \
   BLAS_FSSPR2((Uplo), &(N), &(alpha), (X), &(incX), (Y), &(incY), (A))
#define   BLAS_SGEMM(TransA, TransB, M, N, K, alpha, A, lda, B, ldb, beta, C, ldc) \
   BLAS_FSGEMM((TransA), (TransB), &(M), &(N), &(K), &(alpha), (A), &(lda), (B), &(ldb), &(beta), (C), &(ldc))
#define   BLAS_SSYMM(Side, Uplo, M, N, alpha, A, lda, B, ldb, beta, C, ldc) \
   BLAS_FSSYMM((Side), (Uplo), &(M), &(N), &(alpha), (A), &(lda), (B), &(ldb), &(beta), (C), &(ldc))
#define   BLAS_SSYRK(Uplo, Trans, N, K, alpha, A, lda, beta, C, ldc) \
   BLAS_FSSYRK((Uplo), (Trans), &(N), &(K), &(alpha), (A), &(lda), &(beta), (C), &(ldc))
#define   BLAS_SSYR2K(Uplo, Trans, N, K, alpha, A, lda, B, ldb, beta, C, ldc) \
   BLAS_FSSYR2K((Uplo), (Trans), &(N), &(K), &(alpha), (A), &(lda), (B), &(ldb), &(beta), (C), &(ldc))
#define   BLAS_STRMM(Side, Uplo, TransA, Diag, M, N, alpha, A, lda, B, ldb) \
   BLAS_FSTRMM((Side), (Uplo), (TransA), (Diag), &(M), &(N), &(alpha), (A), &(lda), (B), &(ldb))
#define   BLAS_STRSM(Side, Uplo, TransA, Diag, M, N, alpha, A, lda, B, ldb) \
   BLAS_FSTRSM((Side), (Uplo), (TransA), (Diag), &(M), &(N), &(alpha), (A), &(lda), (B), &(ldb))

#endif /* _C_INTERFACE_ */

#if defined(CBLAS)
/* Translate common blas interface to cblas interface */
/* #define   BLAS_DDOT(N, X, incX, Y, incY) \
      cblas_ddot((N), (X), (incX), (Y), (incY)) */
#define   BLAS_SDOT(N, X, incX, Y, incY) \
   cblas_sdot((N), (X), (incX), (Y), (incY))
#define   BLAS_SNRM2(N, X, incX) \
   cblas_snrm2((N), (X), (incX))
#define   BLAS_SASUM(N, X, incX) \
   cblas_sasum((N), (X), (incX))
#define   BLAS_SSWAP(N, X, incX, Y, incY) \
   cblas_sswap((N), (X), (incX), (Y), (incY))
#define   BLAS_SCOPY(N, X, incX, Y, incY) \
   cblas_scopy((N), (X), (incX), (Y), (incY))
#define   BLAS_SAXPY(N, alpha, X, incX, Y, incY) \
   cblas_saxpy((N), (alpha), (X), (incX), (Y), (incY))
#define   BLAS_SSCAL(N, alpha, X, incX) \
   cblas_sscal((N), (alpha), (X), (incX))
#define   BLAS_SGEMV(TransA, M, N, alpha, A, lda, X, incX, beta, Y, incY) \
   cblas_sgemv(CblasColMajor, (TransA=="N")?CblasNoTrans:((TransA=="T")?CblasTrans:CblasConjTrans), (M), (N), (alpha), (A), (lda), (X), (incX), (beta), (Y), (incY))
#define   BLAS_SGBMV(TransA, M, N, KL, KU, alpha, A, lda, X, incX, beta, Y, incY) \
   cblas_sgbmv(CblasColMajor, (TransA=="N")?CblasNoTrans:((TransA=="T")?CblasTrans:CblasConjTrans), (M), (N), (KL), (KU), (alpha), (A), (lda), (X), (incX), (beta), (Y), (incY))
#define   BLAS_STRMV(Uplo, TransA, Diag, N, A, lda, X, incX) \
   cblas_strmv(CblasColMajor, (Uplo=="U")?CblasUpper:CblasLower, (TransA=="N")?CblasNoTrans:((TransA=="T")?CblasTrans:CblasConjTrans), (Diag=="N")?CblasNonUnit:CblasUnit, (N), (A), (lda), (X), (incX))
#define   BLAS_STBMV(Uplo, TransA, Diag, N, K, A, lda, X, incX) \
   cblas_stbmv(CblasColMajor, (Uplo=="U")?CblasUpper:CblasLower, (TransA=="N")?CblasNoTrans:((TransA=="T")?CblasTrans:CblasConjTrans), (Diag=="N")?CblasNonUnit:CblasUnit, (N), (K), (A), (lda), (X), (incX))
#define   BLAS_STPMV(Uplo, TransA, Diag, N, Ap, X, incX) \
   cblas_stpmv(CblasColMajor, (Uplo=="U")?CblasUpper:CblasLower, (TransA=="N")?CblasNoTrans:((TransA=="T")?CblasTrans:CblasConjTrans), (Diag=="N")?CblasNonUnit:CblasUnit, (N), (Ap), (X), (incX))
#define   BLAS_STRSV(Uplo, TransA, Diag, N, A, lda, X, incX) \
   cblas_strsv(CblasColMajor, (Uplo=="U")?CblasUpper:CblasLower, (TransA=="N")?CblasNoTrans:((TransA=="T")?CblasTrans:CblasConjTrans), (Diag=="N")?CblasNonUnit:CblasUnit, (N), (A), (lda), (X), (incX))
#define   BLAS_STBSV(Uplo, TransA, Diag, N, K, A, lda, X, incX) \
   cblas_stbsv(CblasColMajor, (Uplo=="U")?CblasUpper:CblasLower, (TransA=="N")?CblasNoTrans:((TransA=="T")?CblasTrans:CblasConjTrans), (Diag=="N")?CblasNonUnit:CblasUnit, (N), (K), (A), (lda), (X), (incX))
#define   BLAS_STPSV(Uplo, TransA, Diag, N, Ap, X, incX) \
   cblas_stpsv(CblasColMajor, (Uplo=="U")?CblasUpper:CblasLower, (TransA=="N")?CblasNoTrans:((TransA=="T")?CblasTrans:CblasConjTrans), (Diag=="N")?CblasNonUnit:CblasUnit, (N), (Ap), (X), (incX))
#define   BLAS_SSYMV(Uplo, N, alpha, A, lda, X, incX, beta, Y, incY) \
   cblas_ssymv(CblasColMajor, (Uplo=="U")?CblasUpper:CblasLower, (N), (alpha), (A), (lda), (X), (incX), (beta), (Y), (incY))
#define   BLAS_SSBMV(Uplo, N, K, alpha, A, lda, X, incX, beta, Y, incY) \
   cblas_ssbmv(CblasColMajor, (Uplo=="U")?CblasUpper:CblasLower, (N), (K), (alpha), (A), (lda), (X), (incX), (beta), (Y), (incY))
#define   BLAS_SSPMV(Uplo, N, alpha, Ap, X, incX, beta, Y, incY) \
   cblas_sspmv(CblasColMajor, (Uplo=="U")?CblasUpper:CblasLower, (N), (alpha), (Ap), (X), (incX), (beta), (Y), (incY))
#define   BLAS_SGER(M, N, alpha, X, incX, Y, incY, A, lda) \
   cblas_sger(CblasColMajor, (M), (N), (alpha), (X), (incX), (Y), (incY), (A), (lda))
#define   BLAS_SSYR(Uplo, N, alpha, X, incX, A, lda) \
   cblas_ssyr(CblasColMajor, (Uplo=="U")?CblasUpper:CblasLower, (N), (alpha), (X), (incX), (A), (lda))
#define   BLAS_SSPR(Uplo, N, alpha, X, incX, Ap) \
   cblas_sspr(CblasColMajor, (Uplo=="U")?CblasUpper:CblasLower, (N), (alpha), (X), (incX), (A)p)
#define   BLAS_SSYR2(Uplo, N, alpha, X, incX, Y, incY, A, lda) \
   cblas_ssyr2(CblasColMajor, (Uplo=="U")?CblasUpper:CblasLower, (N), (alpha), (X), (incX), (Y), (incY), (A), (lda))
#define   BLAS_SSPR2(Uplo, N, alpha, X, incX, Y, incY, A) \
   cblas_sspr2(CblasColMajor, (Uplo=="U")?CblasUpper:CblasLower, (N), (alpha), (X), (incX), (Y), (incY), (A))
#define   BLAS_SGEMM(TransA, TransB, M, N, K, alpha, A, lda, B, ldb, beta, C, ldc) \
   cblas_sgemm(CblasColMajor, (TransA=="N")?CblasNoTrans:((TransA=="T")?CblasTrans:CblasConjTrans), (TransB=="N")?CblasNoTrans:((TransB=="T")?CblasTrans:CblasConjTrans), (M), (N), (K), (alpha), (A), (lda), (B), (ldb), (beta), (C), (ldc))
#define   BLAS_SSYMM(Side, Uplo, M, N, alpha, A, lda, B, ldb, beta, C, ldc) \
   cblas_ssymm(CblasColMajor, (Side=="L")?CblasLeft:CblasRight, (Uplo=="U")?CblasUpper:CblasLower, (M), (N), (alpha), (A), (lda), (B), (ldb), (beta), (C), (ldc))
#define   BLAS_SSYRK(Uplo, Trans, N, K, alpha, A, lda, beta, C, ldc) \
   cblas_ssyrk(CblasColMajor, (Uplo=="U")?CblasUpper:CblasLower, (Trans =="N")?CblasNoTrans:((Trans =="T")?CblasTrans:CblasConjTrans), (N), (K), (alpha), (A), (lda), (beta), (C), (ldc))
#define   BLAS_SSYR2K(Uplo, Trans, N, K, alpha, A, lda, B, ldb, beta, C, ldc) \
   cblas_ssyr2k(CblasColMajor, (Uplo=="U")?CblasUpper:CblasLower, (Trans =="N")?CblasNoTrans:((Trans =="T")?CblasTrans:CblasConjTrans), (N), (K), (alpha), (A), (lda), (B), (ldb), (beta), (C), (ldc))
#define   BLAS_STRMM(Side, Uplo, TransA, Diag, M, N, alpha, A, lda, B, ldb) \
   cblas_strmm(CblasColMajor, (Side=="L")?CblasLeft:CblasRight, (Uplo=="U")?CblasUpper:CblasLower, (TransA=="N")?CblasNoTrans:((TransA=="T")?CblasTrans:CblasConjTrans), (Diag=="N")?CblasNonUnit:CblasUnit, (M), (N), (alpha), (A), (lda), (B), (ldb))
#define   BLAS_STRSM(Side, Uplo, TransA, Diag, M, N, alpha, A, lda, B, ldb) \
   cblas_strsm(CblasColMajor, (Side=="L")?CblasLeft:CblasRight, (Uplo=="U")?CblasUpper:CblasLower, (TransA=="N")?CblasNoTrans:((TransA=="T")?CblasTrans:CblasConjTrans), (Diag=="N")?CblasNonUnit:CblasUnit, (M), (N), (alpha), (A), (lda), (B), (ldb))

#endif /* defined(CBLAS) */



#else /* DEBUG_NOBLAS */
/* For debug purpose : Skip every BLAS calls */
/* #define   BLAS_DDOT(N, X, incX, Y, incY)  1 */
#define   BLAS_SDOT(N, X, incX, Y, incY)  1
#define   BLAS_SNRM2(N, X, incX)  1
#define   BLAS_SASUM(N, X, incX)  1
#define   BLAS_SSWAP(N, X, incX, Y, incY)  1
#define   BLAS_SCOPY(N, X, incX, Y, incY)  1
#define   BLAS_SAXPY(N, alpha, X, incX, Y, incY)  1
#define   BLAS_SSCAL(N, alpha, X, incX)  1
#define   BLAS_SGEMV(TransA, M, N, alpha, A, lda, X, incX, beta, Y, incY)  1
#define   BLAS_SGBMV(TransA, M, N, KL, KU, alpha, A, lda, X, incX, beta, Y, incY)  1
#define   BLAS_STRMV(Uplo, TransA, Diag, N, A, lda, X, incX)  1
#define   BLAS_STBMV(Uplo, TransA, Diag, N, K, A, lda, X, incX)  1
#define   BLAS_STPMV(Uplo, TransA, Diag, N, Ap, X, incX)  1
#define   BLAS_STRSV(Uplo, TransA, Diag, N, A, lda, X, incX)  1
#define   BLAS_STBSV(Uplo, TransA, Diag, N, K, A, lda, X, incX)  1
#define   BLAS_STPSV(Uplo, TransA, Diag, N, Ap, X, incX)  1
#define   BLAS_SSYMV(Uplo, N, alpha, A, lda, X, incX, beta, Y, incY)  1
#define   BLAS_SSBMV(Uplo, N, K, alpha, A, lda, X, incX, beta, Y, incY)  1
#define   BLAS_SSPMV(Uplo, N, alpha, Ap, X, incX, beta, Y, incY)  1
#define   BLAS_SGER(M, N, alpha, X, incX, Y, incY, A, lda)  1
#define   BLAS_SSYR(Uplo, N, alpha, X, incX, A, lda)  1
#define   BLAS_SSPR(Uplo, N, alpha, X, incX, Ap)  1
#define   BLAS_SSYR2(Uplo, N, alpha, X, incX, Y, incY, A, lda)  1
#define   BLAS_SSPR2(Uplo, N, alpha, X, incX, Y, incY, A)  1
#define   BLAS_SGEMM(TransA, TransB, M, N, K, alpha, A, lda, B, ldb, beta, C, ldc)  1
#define   BLAS_SSYMM(Side, Uplo, M, N, alpha, A, lda, B, ldb, beta, C, ldc)  1
#define   BLAS_SSYRK(Uplo, Trans, N, K, alpha, A, lda, beta, C, ldc)  1
#define   BLAS_SSYR2K(Uplo, Trans, N, K, alpha, A, lda, B, ldb, beta, C, ldc)  1
#define   BLAS_STRMM(Side, Uplo, TransA, Diag, M, N, alpha, A, lda, B, ldb)  1
#define   BLAS_STRSM(Side, Uplo, TransA, Diag, M, N, alpha, A, lda, B, ldb)  1

#endif /* DEBUG_NOBLAS */

#endif /* _BASE_HEADER_S */

#ifndef _BASE_HEADER_Z
#define _BASE_HEADER_Z

#include "base_tmp.h" /* TODO */

/************************************************************/
/* BLAS C Interfaces                                        */
/*                                                          */
/* Example of interface to use :                            */
/*  BLAS_DDOT(int n, double *x, int incx, double *y,        */
/*                                              int incy);  */
/*                                                          */
/* For portability you *must* always use variables in       */
/*   arguments : int UN=1; BLAS_DDOT(n, x, UN, y, UN);      */
/*                                                          */
/************************************************************/

/************************************************************/
/* $Id: base.template.h,v 1.1 2006/06/01 11:55:43 gaidamou Exp $ */
/************************************************************/

/************************************************************/
/* Help to read this file :                                 */
/*                                                          */
/* If C interface is avaible on the architecture :          */
/*  BLAS_DDOT is redefined as ddot/ddot_/...                */ 
/*                                                          */
/* Else                                                     */
/*  BLAS_DDOT macro calls BLAS_FDDOT and converts argument  */
/*            types for the FORTRAN interface.              */
/*  BLAS_FDDOT is redefined as ddot/ddot_/...               */
/*                                                          */
/* For CBLAS, BLAS_DDOT macro calls cblas_ddot.             */
/************************************************************/

#ifndef DEBUG_NOBLAS

#include <complex.h>
/* GCC http://gcc.gnu.org/onlinedocs/gcc-3.2.3/gcc/Complex.html */
/* lesser than ISO C99 #define complex _Complex double */


/********************** CBLAS ATLAS *************************/
#if defined(CBLAS)
#define _C_INTERFACE_
#include <cblas.h>

/********************** SP2 RS6000 **************************/
#elif defined(SP2) || defined(RS6000)
#define _C_INTERFACE_
#include <essl.h>
/*  EX : #define BLAS_DDOT ddot */
#define BLAS_CZDOTC zdotc
#define BLAS_CDZNRM2 dznrm2
#define BLAS_CZSWAP zswap
#define BLAS_CZCOPY zcopy
#define BLAS_CZAXPY zaxpy
#define BLAS_CZSCAL zscal
#define BLAS_CZDSCAL zdscal
#define BLAS_CZGEMV zgemv
#define BLAS_CZGBMV zgbmv
#define BLAS_CZTRMV ztrmv
#define BLAS_CZTBMV ztbmv
#define BLAS_CZTPMV ztpmv
#define BLAS_CZTRSV ztrsv
#define BLAS_CZTBSV ztbsv
#define BLAS_CZTPSV ztpsv
#define BLAS_CZSYMV zsymv
#define BLAS_CZHBMV zhbmv
#define BLAS_CZHPMV zhpmv
#define BLAS_CZHER zher
#define BLAS_CZHPR zhpr
#define BLAS_CZHYR2 zhyr2
#define BLAS_CZHPR2 zhpr2
#define BLAS_CZGEMM zgemm
#define BLAS_CZSYMM zsymm
#define BLAS_CZSYRK zsyrk
#define BLAS_CZSYR2K zsyr2k
#define BLAS_CZTRMM ztrmm
#define BLAS_CZTRSM ztrsm


/********************** SGI *********************************/
#elif defined(SGI)
#define _C_INTERFACE_
#include <scsl_blas.h>
/*  EX : #define BLAS_DDOT ddot */
#define BLAS_ZDOTC zdotc
#define BLAS_DZNRM2 dznrm2
#define BLAS_ZSWAP zswap
#define BLAS_ZCOPY zcopy
#define BLAS_ZAXPY zaxpy
#define BLAS_ZSCAL zscal
#define BLAS_ZDSCAL zdscal
#define BLAS_ZGEMV zgemv
#define BLAS_ZGBMV zgbmv
#define BLAS_ZTRMV ztrmv
#define BLAS_ZTBMV ztbmv
#define BLAS_ZTPMV ztpmv
#define BLAS_ZTRSV ztrsv
#define BLAS_ZTBSV ztbsv
#define BLAS_ZTPSV ztpsv
#define BLAS_ZSYMV zsymv
#define BLAS_ZHBMV zhbmv
#define BLAS_ZHPMV zhpmv
#define BLAS_ZHER zher
#define BLAS_ZHPR zhpr
#define BLAS_ZHYR2 zhyr2
#define BLAS_ZHPR2 zhpr2
#define BLAS_ZGEMM zgemm
#define BLAS_ZSYMM zsymm
#define BLAS_ZSYRK zsyrk
#define BLAS_ZSYR2K zsyr2k
#define BLAS_ZTRMM ztrmm
#define BLAS_ZTRSM ztrsm


/********************** CRAY ********************************/
#elif defined(CRAY)
/*  EX : #define BLAS_FDDOT DDOT */
#define BLAS_FZDOTC ZDOTC
#define BLAS_FDZNRM2 DZNRM2
#define BLAS_FZSWAP ZSWAP
#define BLAS_FZCOPY ZCOPY
#define BLAS_FZAXPY ZAXPY
#define BLAS_FZSCAL ZSCAL
#define BLAS_FZDSCAL ZDSCAL
#define BLAS_FZGEMV ZGEMV
#define BLAS_FZGBMV ZGBMV
#define BLAS_FZTRMV ZTRMV
#define BLAS_FZTBMV ZTBMV
#define BLAS_FZTPMV ZTPMV
#define BLAS_FZTRSV ZTRSV
#define BLAS_FZTBSV ZTBSV
#define BLAS_FZTPSV ZTPSV
#define BLAS_FZSYMV ZSYMV
#define BLAS_FZHBMV ZHBMV
#define BLAS_FZHPMV ZHPMV
#define BLAS_FZHER ZHER
#define BLAS_FZHPR ZHPR
#define BLAS_FZHYR2 ZHYR2
#define BLAS_FZHPR2 ZHPR2
#define BLAS_FZGEMM ZGEMM
#define BLAS_FZSYMM ZSYMM
#define BLAS_FZSYRK ZSYRK
#define BLAS_FZSYR2K ZSYR2K
#define BLAS_FZTRMM ZTRMM
#define BLAS_FZTRSM ZTRSM


/********************** LINUX *******************************/
#elif defined(LINUX)
/* EX : #define BLAS_FDDOT ddot_ */
#define BLAS_FZDOTC zdotc_
#define BLAS_FDZNRM2 dznrm2_
#define BLAS_FZSWAP zswap_
#define BLAS_FZCOPY zcopy_
#define BLAS_FZAXPY zaxpy_
#define BLAS_FZSCAL zscal_
#define BLAS_FZDSCAL zdscal_
#define BLAS_FZGEMV zgemv_
#define BLAS_FZGBMV zgbmv_
#define BLAS_FZTRMV ztrmv_
#define BLAS_FZTBMV ztbmv_
#define BLAS_FZTPMV ztpmv_
#define BLAS_FZTRSV ztrsv_
#define BLAS_FZTBSV ztbsv_
#define BLAS_FZTPSV ztpsv_
#define BLAS_FZSYMV zsymv_
#define BLAS_FZHBMV zhbmv_
#define BLAS_FZHPMV zhpmv_
#define BLAS_FZHER zher_
#define BLAS_FZHPR zhpr_
#define BLAS_FZHYR2 zhyr2_
#define BLAS_FZHPR2 zhpr2_
#define BLAS_FZGEMM zgemm_
#define BLAS_FZSYMM zsymm_
#define BLAS_FZSYRK zsyrk_
#define BLAS_FZSYR2K zsyr2k_
#define BLAS_FZTRMM ztrmm_
#define BLAS_FZTRSM ztrsm_

#endif /* defined(..arch..) */

#ifndef _C_INTERFACE_
/* FORTRAN subroutine declarations */
/* EX : extern double BLAS_FDDOT(int *n, double *x, int *incx, double *y, int *incy);  */
extern double complex BLAS_FZDOTC(int *N, const double complex *X, int *incX, const double complex *Y, int *incY);
extern double BLAS_FDZNRM2(int *N, const double complex *X, int *incX);
extern void BLAS_FZSWAP(int *N, double complex *X, int *incX, double complex *Y, int *incY);
extern void BLAS_FZCOPY(int *N, const double complex *X, int *incX, double complex *Y, int *incY);
extern void BLAS_FZAXPY(int *N, const double complex* alpha, const double complex *X, int *incX, double complex *Y, int *incY);
extern void BLAS_FZSCAL(int *N, const double complex* alpha, double complex *X, int *incX);
extern void BLAS_FZDSCAL(int *N, const double* alpha, double complex *X, int *incX);
extern void BLAS_FZGEMV(char *TransA, int *M, int *N, const double complex* alpha, const double complex *A, int *lda, const double complex *X, int *incX, const double complex* beta, double complex *Y, int *incY);
extern void BLAS_FZGBMV(char *TransA, int *M, int *N, int *KL, int *KU, const double complex* alpha, const double complex *A, int *lda, const double complex *X, int *incX, const double complex* beta, double complex *Y, int *incY);
extern void BLAS_FZTRMV(char *Uplo, char *TransA, char *Diag, int *N, const double complex *A, int *lda, double complex *X, int *incX);
extern void BLAS_FZTBMV(char *Uplo, char *TransA, char *Diag, int *N, int *K, const double complex *A, int *lda, double complex *X, int *incX);
extern void BLAS_FZTPMV(char *Uplo, char *TransA, char *Diag, int *N, const double complex *Ap, double complex *X, int *incX);
extern void BLAS_FZTRSV(char *Uplo, char *TransA, char *Diag, int *N, const double complex *A, int *lda, double complex *X, int *incX);
extern void BLAS_FZTBSV(char *Uplo, char *TransA, char *Diag, int *N, int *K, const double complex *A, int *lda, double complex *X, int *incX);
extern void BLAS_FZTPSV(char *Uplo, char *TransA, char *Diag, int *N, const double complex *Ap, double complex *X, int *incX);
extern void BLAS_FZSYMV(char *Uplo, int *N, const double complex* alpha, const double complex *A, int *lda, const double complex *X, int *incX, const double complex* beta, double complex *Y, int *incY);
extern void BLAS_FZHBMV(char *Uplo, int *N, int *K, const double complex* alpha, const double complex *A, int *lda, const double complex *X, int *incX, const double complex* beta, double complex *Y, int *incY);
extern void BLAS_FZHPMV(char *Uplo, int *N, const double complex* alpha, const double complex *Ap, const double complex *X, int *incX, const double complex* beta, double complex *Y, int *incY);
extern void BLAS_FZHER(int *M, int *N, const double complex* alpha, const double complex *X, int *incX, const double complex *Y, int *incY, double complex *A, int *lda);
extern void BLAS_FZHPR(char *Uplo, int *N, const double complex* alpha, const double complex *X, int *incX, double complex *Ap);
extern void BLAS_FZHYR2(char *Uplo, int *N, const double complex* alpha, const double complex *X, int *incX, const double complex *Y, int *incY, double complex *A, int *lda);
extern void BLAS_FZHPR2(char *Uplo, int *N, const double complex* alpha, const double complex *X, int *incX, const double complex *Y, int *incY, double complex *A);
extern void BLAS_FZGEMM(char *TransA, char *TransB, int *M, int *N, int *K, const double complex* alpha, const double complex *A, int *lda, const double complex *B, int *ldb, const double complex* beta, double complex *C, int *ldc);
extern void BLAS_FZSYMM(char *Side, char *Uplo, int *M, int *N, const double complex* alpha, const double complex *A, int *lda, const double complex *B, int *ldb, const double complex* beta, double complex *C, int *ldc);
extern void BLAS_FZSYRK(char *Uplo, char *Trans, int *N, int *K, const double complex* alpha, const double complex *A, int *lda, const double complex* beta, double complex *C, int *ldc);
extern void BLAS_FZSYR2K(char *Uplo, char *Trans, int *N, int *K, const double complex* alpha, const double complex *A, int *lda, const double complex *B, int *ldb, const double complex* beta, double complex *C, int *ldc);
extern void BLAS_FZTRMM(char *Side, char *Uplo, char *TransA, char *Diag, int *M, int *N, const double complex* alpha, const double complex *A, int *lda, double complex *B, int *ldb);
extern void BLAS_FZTRSM(char *Side, char *Uplo, char *TransA, char *Diag, int *M, int *N, const double complex* alpha, const double complex *A, int *lda, double complex *B, int *ldb);


/* Call FORTRAN subroutines by using C like interface */
/* EX : #define BLAS_DDOT(n,x,incx,y,incy) BLAS_FDDOT(&(n),(x),&(incx),(y),&(incy)) */
#define   BLAS_ZDOTC(N, X, incX, Y, incY) \
   BLAS_FZDOTC(&(N), (X), &(incX), (Y), &(incY))
#define   BLAS_DZNRM2(N, X, incX) \
   BLAS_FDZNRM2(&(N), (X), &(incX))
#define   BLAS_ZSWAP(N, X, incX, Y, incY) \
   BLAS_FZSWAP(&(N), (X), &(incX), (Y), &(incY))
#define   BLAS_ZCOPY(N, X, incX, Y, incY) \
   BLAS_FZCOPY(&(N), (X), &(incX), (Y), &(incY))
#define   BLAS_ZAXPY(N, alpha, X, incX, Y, incY) \
   BLAS_FZAXPY(&(N), &(alpha), (X), &(incX), (Y), &(incY))
#define   BLAS_ZSCAL(N, alpha, X, incX) \
   BLAS_FZSCAL(&(N), &(alpha), (X), &(incX))
#define   BLAS_ZDSCAL(N, alpha, X, incX) \
   BLAS_FZDSCAL(&(N), &(alpha), (X), &(incX))
#define   BLAS_ZGEMV(TransA, M, N, alpha, A, lda, X, incX, beta, Y, incY) \
   BLAS_FZGEMV((TransA), &(M), &(N), &(alpha), (A), &(lda), (X), &(incX), &(beta), (Y), &(incY))
#define   BLAS_ZGBMV(TransA, M, N, KL, KU, alpha, A, lda, X, incX, beta, Y, incY) \
   BLAS_FZGBMV((TransA), &(M), &(N), &(KL), &(KU), &(alpha), (A), &(lda), (X), &(incX), &(beta), (Y), &(incY))
#define   BLAS_ZTRMV(Uplo, TransA, Diag, N, A, lda, X, incX) \
   BLAS_FZTRMV((Uplo), (TransA), (Diag), &(N), (A), &(lda), (X), &(incX))
#define   BLAS_ZTBMV(Uplo, TransA, Diag, N, K, A, lda, X, incX) \
   BLAS_FZTBMV((Uplo), (TransA), (Diag), &(N), &(K), (A), &(lda), (X), &(incX))
#define   BLAS_ZTPMV(Uplo, TransA, Diag, N, Ap, X, incX) \
   BLAS_FZTPMV((Uplo), (TransA), (Diag), &(N), (Ap), (X), &(incX))
#define   BLAS_ZTRSV(Uplo, TransA, Diag, N, A, lda, X, incX) \
   BLAS_FZTRSV((Uplo), (TransA), (Diag), &(N), (A), &(lda), (X), &(incX))
#define   BLAS_ZTBSV(Uplo, TransA, Diag, N, K, A, lda, X, incX) \
   BLAS_FZTBSV((Uplo), (TransA), (Diag), &(N), &(K), (A), &(lda), (X), &(incX))
#define   BLAS_ZTPSV(Uplo, TransA, Diag, N, Ap, X, incX) \
   BLAS_FZTPSV((Uplo), (TransA), (Diag), &(N), (Ap), (X), &(incX))
#define   BLAS_ZSYMV(Uplo, N, alpha, A, lda, X, incX, beta, Y, incY) \
   BLAS_FZSYMV((Uplo), &(N), &(alpha), (A), &(lda), (X), &(incX), &(beta), (Y), &(incY))
#define   BLAS_ZHBMV(Uplo, N, K, alpha, A, lda, X, incX, beta, Y, incY) \
   BLAS_FZHBMV((Uplo), &(N), &(K), &(alpha), (A), &(lda), (X), &(incX), &(beta), (Y), &(incY))
#define   BLAS_ZHPMV(Uplo, N, alpha, Ap, X, incX, beta, Y, incY) \
   BLAS_FZHPMV((Uplo), &(N), &(alpha), (Ap), (X), &(incX), &(beta), (Y), &(incY))
#define   BLAS_ZHER(M, N, alpha, X, incX, Y, incY, A, lda) \
   BLAS_FZHER(&(M), &(N), &(alpha), (X), &(incX), (Y), &(incY), (A), &(lda))
#define   BLAS_ZHPR(Uplo, N, alpha, X, incX, Ap) \
   BLAS_FZHPR((Uplo), &(N), &(alpha), (X), &(incX), (A)p)
#define   BLAS_ZHYR2(Uplo, N, alpha, X, incX, Y, incY, A, lda) \
   BLAS_FZHYR2((Uplo), &(N), &(alpha), (X), &(incX), (Y), &(incY), (A), &(lda))
#define   BLAS_ZHPR2(Uplo, N, alpha, X, incX, Y, incY, A) \
   BLAS_FZHPR2((Uplo), &(N), &(alpha), (X), &(incX), (Y), &(incY), (A))
#define   BLAS_ZGEMM(TransA, TransB, M, N, K, alpha, A, lda, B, ldb, beta, C, ldc) \
   BLAS_FZGEMM((TransA), (TransB), &(M), &(N), &(K), &(alpha), (A), &(lda), (B), &(ldb), &(beta), (C), &(ldc))
#define   BLAS_ZSYMM(Side, Uplo, M, N, alpha, A, lda, B, ldb, beta, C, ldc) \
   BLAS_FZSYMM((Side), (Uplo), &(M), &(N), &(alpha), (A), &(lda), (B), &(ldb), &(beta), (C), &(ldc))
#define   BLAS_ZSYRK(Uplo, Trans, N, K, alpha, A, lda, beta, C, ldc) \
   BLAS_FZSYRK((Uplo), (Trans), &(N), &(K), &(alpha), (A), &(lda), &(beta), (C), &(ldc))
#define   BLAS_ZSYR2K(Uplo, Trans, N, K, alpha, A, lda, B, ldb, beta, C, ldc) \
   BLAS_FZSYR2K((Uplo), (Trans), &(N), &(K), &(alpha), (A), &(lda), (B), &(ldb), &(beta), (C), &(ldc))
#define   BLAS_ZTRMM(Side, Uplo, TransA, Diag, M, N, alpha, A, lda, B, ldb) \
   BLAS_FZTRMM((Side), (Uplo), (TransA), (Diag), &(M), &(N), &(alpha), (A), &(lda), (B), &(ldb))
#define   BLAS_ZTRSM(Side, Uplo, TransA, Diag, M, N, alpha, A, lda, B, ldb) \
   BLAS_FZTRSM((Side), (Uplo), (TransA), (Diag), &(M), &(N), &(alpha), (A), &(lda), (B), &(ldb))

#endif /* _C_INTERFACE_ */

#if defined(CBLAS)
/* Translate common blas interface to cblas interface */
/* #define   BLAS_DDOT(N, X, incX, Y, incY) \
      cblas_ddot((N), (X), (incX), (Y), (incY)) */
#define   BLAS_ZDOTC(N, X, incX, Y, incY) \
   cblas_zdotc((N), (X), (incX), (Y), (incY))
#define   BLAS_DZNRM2(N, X, incX) \
   cblas_dznrm2((N), (X), (incX))
#define   BLAS_ZSWAP(N, X, incX, Y, incY) \
   cblas_zswap((N), (X), (incX), (Y), (incY))
#define   BLAS_ZCOPY(N, X, incX, Y, incY) \
   cblas_zcopy((N), (X), (incX), (Y), (incY))
#define   BLAS_ZAXPY(N, alpha, X, incX, Y, incY) \
   cblas_zaxpy((N), (alpha), (X), (incX), (Y), (incY))
#define   BLAS_ZSCAL(N, alpha, X, incX) \
   cblas_zscal((N), (alpha), (X), (incX))
#define   BLAS_ZDSCAL(N, alpha, X, incX) \
   cblas_zdscal((N), (alpha), (X), (incX))
#define   BLAS_ZGEMV(TransA, M, N, alpha, A, lda, X, incX, beta, Y, incY) \
   cblas_zgemv(CblasColMajor, (TransA=="N")?CblasNoTrans:((TransA=="T")?CblasTrans:CblasConjTrans), (M), (N), (alpha), (A), (lda), (X), (incX), (beta), (Y), (incY))
#define   BLAS_ZGBMV(TransA, M, N, KL, KU, alpha, A, lda, X, incX, beta, Y, incY) \
   cblas_zgbmv(CblasColMajor, (TransA=="N")?CblasNoTrans:((TransA=="T")?CblasTrans:CblasConjTrans), (M), (N), (KL), (KU), (alpha), (A), (lda), (X), (incX), (beta), (Y), (incY))
#define   BLAS_ZTRMV(Uplo, TransA, Diag, N, A, lda, X, incX) \
   cblas_ztrmv(CblasColMajor, (Uplo=="U")?CblasUpper:CblasLower, (TransA=="N")?CblasNoTrans:((TransA=="T")?CblasTrans:CblasConjTrans), (Diag=="N")?CblasNonUnit:CblasUnit, (N), (A), (lda), (X), (incX))
#define   BLAS_ZTBMV(Uplo, TransA, Diag, N, K, A, lda, X, incX) \
   cblas_ztbmv(CblasColMajor, (Uplo=="U")?CblasUpper:CblasLower, (TransA=="N")?CblasNoTrans:((TransA=="T")?CblasTrans:CblasConjTrans), (Diag=="N")?CblasNonUnit:CblasUnit, (N), (K), (A), (lda), (X), (incX))
#define   BLAS_ZTPMV(Uplo, TransA, Diag, N, Ap, X, incX) \
   cblas_ztpmv(CblasColMajor, (Uplo=="U")?CblasUpper:CblasLower, (TransA=="N")?CblasNoTrans:((TransA=="T")?CblasTrans:CblasConjTrans), (Diag=="N")?CblasNonUnit:CblasUnit, (N), (Ap), (X), (incX))
#define   BLAS_ZTRSV(Uplo, TransA, Diag, N, A, lda, X, incX) \
   cblas_ztrsv(CblasColMajor, (Uplo=="U")?CblasUpper:CblasLower, (TransA=="N")?CblasNoTrans:((TransA=="T")?CblasTrans:CblasConjTrans), (Diag=="N")?CblasNonUnit:CblasUnit, (N), (A), (lda), (X), (incX))
#define   BLAS_ZTBSV(Uplo, TransA, Diag, N, K, A, lda, X, incX) \
   cblas_ztbsv(CblasColMajor, (Uplo=="U")?CblasUpper:CblasLower, (TransA=="N")?CblasNoTrans:((TransA=="T")?CblasTrans:CblasConjTrans), (Diag=="N")?CblasNonUnit:CblasUnit, (N), (K), (A), (lda), (X), (incX))
#define   BLAS_ZTPSV(Uplo, TransA, Diag, N, Ap, X, incX) \
   cblas_ztpsv(CblasColMajor, (Uplo=="U")?CblasUpper:CblasLower, (TransA=="N")?CblasNoTrans:((TransA=="T")?CblasTrans:CblasConjTrans), (Diag=="N")?CblasNonUnit:CblasUnit, (N), (Ap), (X), (incX))
#define   BLAS_ZSYMV(Uplo, N, alpha, A, lda, X, incX, beta, Y, incY) \
   cblas_zsymv(CblasColMajor, (Uplo=="U")?CblasUpper:CblasLower, (N), (alpha), (A), (lda), (X), (incX), (beta), (Y), (incY))
#define   BLAS_ZHBMV(Uplo, N, K, alpha, A, lda, X, incX, beta, Y, incY) \
   cblas_zhbmv(CblasColMajor, (Uplo=="U")?CblasUpper:CblasLower, (N), (K), (alpha), (A), (lda), (X), (incX), (beta), (Y), (incY))
#define   BLAS_ZHPMV(Uplo, N, alpha, Ap, X, incX, beta, Y, incY) \
   cblas_zhpmv(CblasColMajor, (Uplo=="U")?CblasUpper:CblasLower, (N), (alpha), (Ap), (X), (incX), (beta), (Y), (incY))
#define   BLAS_ZHER(M, N, alpha, X, incX, Y, incY, A, lda) \
   cblas_zher(CblasColMajor, (M), (N), (alpha), (X), (incX), (Y), (incY), (A), (lda))
#define   BLAS_ZHPR(Uplo, N, alpha, X, incX, Ap) \
   cblas_zhpr(CblasColMajor, (Uplo=="U")?CblasUpper:CblasLower, (N), (alpha), (X), (incX), (A)p)
#define   BLAS_ZHYR2(Uplo, N, alpha, X, incX, Y, incY, A, lda) \
   cblas_zhyr2(CblasColMajor, (Uplo=="U")?CblasUpper:CblasLower, (N), (alpha), (X), (incX), (Y), (incY), (A), (lda))
#define   BLAS_ZHPR2(Uplo, N, alpha, X, incX, Y, incY, A) \
   cblas_zhpr2(CblasColMajor, (Uplo=="U")?CblasUpper:CblasLower, (N), (alpha), (X), (incX), (Y), (incY), (A))
#define   BLAS_ZGEMM(TransA, TransB, M, N, K, alpha, A, lda, B, ldb, beta, C, ldc) \
   cblas_zgemm(CblasColMajor, (TransA=="N")?CblasNoTrans:((TransA=="T")?CblasTrans:CblasConjTrans), (TransB=="N")?CblasNoTrans:((TransB=="T")?CblasTrans:CblasConjTrans), (M), (N), (K), (alpha), (A), (lda), (B), (ldb), (beta), (C), (ldc))
#define   BLAS_ZSYMM(Side, Uplo, M, N, alpha, A, lda, B, ldb, beta, C, ldc) \
   cblas_zsymm(CblasColMajor, (Side=="L")?CblasLeft:CblasRight, (Uplo=="U")?CblasUpper:CblasLower, (M), (N), (alpha), (A), (lda), (B), (ldb), (beta), (C), (ldc))
#define   BLAS_ZSYRK(Uplo, Trans, N, K, alpha, A, lda, beta, C, ldc) \
   cblas_zsyrk(CblasColMajor, (Uplo=="U")?CblasUpper:CblasLower, (Trans =="N")?CblasNoTrans:((Trans =="T")?CblasTrans:CblasConjTrans), (N), (K), (alpha), (A), (lda), (beta), (C), (ldc))
#define   BLAS_ZSYR2K(Uplo, Trans, N, K, alpha, A, lda, B, ldb, beta, C, ldc) \
   cblas_zsyr2k(CblasColMajor, (Uplo=="U")?CblasUpper:CblasLower, (Trans =="N")?CblasNoTrans:((Trans =="T")?CblasTrans:CblasConjTrans), (N), (K), (alpha), (A), (lda), (B), (ldb), (beta), (C), (ldc))
#define   BLAS_ZTRMM(Side, Uplo, TransA, Diag, M, N, alpha, A, lda, B, ldb) \
   cblas_ztrmm(CblasColMajor, (Side=="L")?CblasLeft:CblasRight, (Uplo=="U")?CblasUpper:CblasLower, (TransA=="N")?CblasNoTrans:((TransA=="T")?CblasTrans:CblasConjTrans), (Diag=="N")?CblasNonUnit:CblasUnit, (M), (N), (alpha), (A), (lda), (B), (ldb))
#define   BLAS_ZTRSM(Side, Uplo, TransA, Diag, M, N, alpha, A, lda, B, ldb) \
   cblas_ztrsm(CblasColMajor, (Side=="L")?CblasLeft:CblasRight, (Uplo=="U")?CblasUpper:CblasLower, (TransA=="N")?CblasNoTrans:((TransA=="T")?CblasTrans:CblasConjTrans), (Diag=="N")?CblasNonUnit:CblasUnit, (M), (N), (alpha), (A), (lda), (B), (ldb))

#endif /* defined(CBLAS) */

#if defined(SP2) || defined(RS6000)
/* Solve problems between C99 complex type and ESSL internal complex type */

typedef union { dcmplx _essl; complex double _c99; } intermed;

#define   BLAS_ZDOTC(N, X, incX, Y, incY) \
   BLAS_CZDOTC((N), (dcmplx*)(X), (incX), (dcmplx*)(Y), (incY))
#define   BLAS_DZNRM2(N, X, incX) \
   BLAS_CDZNRM2((N), (dcmplx*)(X), (incX))
#define   BLAS_ZSWAP(N, X, incX, Y, incY) \
   BLAS_CZSWAP((N), (dcmplx*)(X), (incX), (dcmplx*)(Y), (incY))
#define   BLAS_ZCOPY(N, X, incX, Y, incY) \
   BLAS_CZCOPY((N), (dcmplx*)(X), (incX), (dcmplx*)(Y), (incY))
#define   BLAS_ZAXPY(N, alpha, X, incX, Y, incY) \
 { intermed _alpha; _alpha._c99 = alpha;  BLAS_CZAXPY((N), _alpha._essl, (dcmplx*)(X), (incX), (dcmplx*)(Y), (incY)); }
#define   BLAS_ZSCAL(N, alpha, X, incX) \
 { intermed _alpha; _alpha._c99 = alpha;  BLAS_CZSCAL((N), _alpha._essl, (dcmplx*)(X), (incX)); }
#define   BLAS_ZDSCAL(N, alpha, X, incX) \
   BLAS_CZDSCAL((N), alpha, (dcmplx*)(X), (incX))
#define   BLAS_ZGEMV(TransA, M, N, alpha, A, lda, X, incX, beta, Y, incY) \
 { intermed _alpha, _beta; _alpha._c99 = alpha; _beta._c99 = beta;  BLAS_CZGEMV((TransA), (M), (N), _alpha._essl, (dcmplx*)(A), (lda), (dcmplx*)(X), (incX), _beta._essl, (dcmplx*)(Y), (incY)); }
#define   BLAS_ZGBMV(TransA, M, N, KL, KU, alpha, A, lda, X, incX, beta, Y, incY) \
 { intermed _alpha, _beta; _alpha._c99 = alpha; _beta._c99 = beta;  BLAS_CZGBMV((TransA), (M), (N), (KL), (KU), _alpha._essl, (dcmplx*)(A), (lda), (dcmplx*)(X), (incX), _beta._essl, (dcmplx*)(Y), (incY)); }
#define   BLAS_ZTRMV(Uplo, TransA, Diag, N, A, lda, X, incX) \
   BLAS_CZTRMV((Uplo), (TransA), (Diag), (N), (dcmplx*)(A), (lda), (dcmplx*)(X), (incX))
#define   BLAS_ZTBMV(Uplo, TransA, Diag, N, K, A, lda, X, incX) \
   BLAS_CZTBMV((Uplo), (TransA), (Diag), (N), (K), (dcmplx*)(A), (lda), (dcmplx*)(X), (incX))
#define   BLAS_ZTPMV(Uplo, TransA, Diag, N, Ap, X, incX) \
   BLAS_CZTPMV((Uplo), (TransA), (Diag), (N), (dcmplx*)(Ap), (dcmplx*)(X), (incX))
#define   BLAS_ZTRSV(Uplo, TransA, Diag, N, A, lda, X, incX) \
   BLAS_CZTRSV((Uplo), (TransA), (Diag), (N), (dcmplx*)(A), (lda), (dcmplx*)(X), (incX))
#define   BLAS_ZTBSV(Uplo, TransA, Diag, N, K, A, lda, X, incX) \
   BLAS_CZTBSV((Uplo), (TransA), (Diag), (N), (K), (dcmplx*)(A), (lda), (dcmplx*)(X), (incX))
#define   BLAS_ZTPSV(Uplo, TransA, Diag, N, Ap, X, incX) \
   BLAS_CZTPSV((Uplo), (TransA), (Diag), (N), (dcmplx*)(Ap), (dcmplx*)(X), (incX))
#define   BLAS_ZSYMV(Uplo, N, alpha, A, lda, X, incX, beta, Y, incY) \
 { intermed _alpha, _beta; _alpha._c99 = alpha; _beta._c99 = beta;  BLAS_CZSYMV((Uplo), (N), _alpha._essl, (dcmplx*)(A), (lda), (dcmplx*)(X), (incX), _beta._essl, (dcmplx*)(Y), (incY)); }
#define   BLAS_ZHBMV(Uplo, N, K, alpha, A, lda, X, incX, beta, Y, incY) \
 { intermed _alpha, _beta; _alpha._c99 = alpha; _beta._c99 = beta;  BLAS_CZHBMV((Uplo), (N), (K), _alpha._essl, (dcmplx*)(A), (lda), (dcmplx*)(X), (incX), _beta._essl, (dcmplx*)(Y), (incY)); }
#define   BLAS_ZHPMV(Uplo, N, alpha, Ap, X, incX, beta, Y, incY) \
 { intermed _alpha, _beta; _alpha._c99 = alpha; _beta._c99 = beta;  BLAS_CZHPMV((Uplo), (N), _alpha._essl, (dcmplx*)(Ap), (dcmplx*)(X), (incX), _beta._essl, (dcmplx*)(Y), (incY)); }
#define   BLAS_ZHER(M, N, alpha, X, incX, Y, incY, A, lda) \
 { intermed _alpha; _alpha._c99 = alpha;  BLAS_CZHER((M), (N), _alpha._essl, (dcmplx*)(X), (incX), (dcmplx*)(Y), (incY), (dcmplx*)(A), (lda)); }
#define   BLAS_ZHPR(Uplo, N, alpha, X, incX, Ap) \
 { intermed _alpha; _alpha._c99 = alpha;  BLAS_CZHPR((Uplo), (N), _alpha._essl, (dcmplx*)(X), (incX), (dcmplx*)(A)p); }
#define   BLAS_ZHYR2(Uplo, N, alpha, X, incX, Y, incY, A, lda) \
 { intermed _alpha; _alpha._c99 = alpha;  BLAS_CZHYR2((Uplo), (N), _alpha._essl, (dcmplx*)(X), (incX), (dcmplx*)(Y), (incY), (dcmplx*)(A), (lda)); }
#define   BLAS_ZHPR2(Uplo, N, alpha, X, incX, Y, incY, A) \
 { intermed _alpha; _alpha._c99 = alpha;  BLAS_CZHPR2((Uplo), (N), _alpha._essl, (dcmplx*)(X), (incX), (dcmplx*)(Y), (incY), (dcmplx*)(A)); }
#define   BLAS_ZGEMM(TransA, TransB, M, N, K, alpha, A, lda, B, ldb, beta, C, ldc) \
 { intermed _alpha, _beta; _alpha._c99 = alpha; _beta._c99 = beta;  BLAS_CZGEMM((TransA), (TransB), (M), (N), (K), _alpha._essl, (dcmplx*)(A), (lda), (dcmplx*)(B), (ldb), _beta._essl, (dcmplx*)(C), (ldc)); }
#define   BLAS_ZSYMM(Side, Uplo, M, N, alpha, A, lda, B, ldb, beta, C, ldc) \
 { intermed _alpha, _beta; _alpha._c99 = alpha; _beta._c99 = beta;  BLAS_CZSYMM((Side), (Uplo), (M), (N), _alpha._essl, (dcmplx*)(A), (lda), (dcmplx*)(B), (ldb), _beta._essl, (dcmplx*)(C), (ldc)); }
#define   BLAS_ZSYRK(Uplo, Trans, N, K, alpha, A, lda, beta, C, ldc) \
 { intermed _alpha, _beta; _alpha._c99 = alpha; _beta._c99 = beta;  BLAS_CZSYRK((Uplo), (Trans), (N), (K), _alpha._essl, (dcmplx*)(A), (lda), _beta._essl, (dcmplx*)(C), (ldc)); }
#define   BLAS_ZSYR2K(Uplo, Trans, N, K, alpha, A, lda, B, ldb, beta, C, ldc) \
 { intermed _alpha, _beta; _alpha._c99 = alpha; _beta._c99 = beta;  BLAS_CZSYR2K((Uplo), (Trans), (N), (K), _alpha._essl, (dcmplx*)(A), (lda), (dcmplx*)(B), (ldb), _beta._essl, (dcmplx*)(C), (ldc)); }
#define   BLAS_ZTRMM(Side, Uplo, TransA, Diag, M, N, alpha, A, lda, B, ldb) \
 { intermed _alpha; _alpha._c99 = alpha;  BLAS_CZTRMM((Side), (Uplo), (TransA), (Diag), (M), (N), _alpha._essl, (dcmplx*)(A), (lda), (dcmplx*)(B), (ldb)); }
#define   BLAS_ZTRSM(Side, Uplo, TransA, Diag, M, N, alpha, A, lda, B, ldb) \
 { intermed _alpha; _alpha._c99 = alpha;  BLAS_CZTRSM((Side), (Uplo), (TransA), (Diag), (M), (N), _alpha._essl, (dcmplx*)(A), (lda), (dcmplx*)(B), (ldb)); }
#endif


#else /* DEBUG_NOBLAS */
/* For debug purpose : Skip every BLAS calls */
/* #define   BLAS_DDOT(N, X, incX, Y, incY)  1 */
#define   BLAS_ZDOTC(N, X, incX, Y, incY)  1
#define   BLAS_DZNRM2(N, X, incX)  1
#define   BLAS_ZSWAP(N, X, incX, Y, incY)  1
#define   BLAS_ZCOPY(N, X, incX, Y, incY)  1
#define   BLAS_ZAXPY(N, alpha, X, incX, Y, incY)  1
#define   BLAS_ZSCAL(N, alpha, X, incX)  1
#define   BLAS_ZDSCAL(N, alpha, X, incX)  1
#define   BLAS_ZGEMV(TransA, M, N, alpha, A, lda, X, incX, beta, Y, incY)  1
#define   BLAS_ZGBMV(TransA, M, N, KL, KU, alpha, A, lda, X, incX, beta, Y, incY)  1
#define   BLAS_ZTRMV(Uplo, TransA, Diag, N, A, lda, X, incX)  1
#define   BLAS_ZTBMV(Uplo, TransA, Diag, N, K, A, lda, X, incX)  1
#define   BLAS_ZTPMV(Uplo, TransA, Diag, N, Ap, X, incX)  1
#define   BLAS_ZTRSV(Uplo, TransA, Diag, N, A, lda, X, incX)  1
#define   BLAS_ZTBSV(Uplo, TransA, Diag, N, K, A, lda, X, incX)  1
#define   BLAS_ZTPSV(Uplo, TransA, Diag, N, Ap, X, incX)  1
#define   BLAS_ZSYMV(Uplo, N, alpha, A, lda, X, incX, beta, Y, incY)  1
#define   BLAS_ZHBMV(Uplo, N, K, alpha, A, lda, X, incX, beta, Y, incY)  1
#define   BLAS_ZHPMV(Uplo, N, alpha, Ap, X, incX, beta, Y, incY)  1
#define   BLAS_ZHER(M, N, alpha, X, incX, Y, incY, A, lda)  1
#define   BLAS_ZHPR(Uplo, N, alpha, X, incX, Ap)  1
#define   BLAS_ZHYR2(Uplo, N, alpha, X, incX, Y, incY, A, lda)  1
#define   BLAS_ZHPR2(Uplo, N, alpha, X, incX, Y, incY, A)  1
#define   BLAS_ZGEMM(TransA, TransB, M, N, K, alpha, A, lda, B, ldb, beta, C, ldc)  1
#define   BLAS_ZSYMM(Side, Uplo, M, N, alpha, A, lda, B, ldb, beta, C, ldc)  1
#define   BLAS_ZSYRK(Uplo, Trans, N, K, alpha, A, lda, beta, C, ldc)  1
#define   BLAS_ZSYR2K(Uplo, Trans, N, K, alpha, A, lda, B, ldb, beta, C, ldc)  1
#define   BLAS_ZTRMM(Side, Uplo, TransA, Diag, M, N, alpha, A, lda, B, ldb)  1
#define   BLAS_ZTRSM(Side, Uplo, TransA, Diag, M, N, alpha, A, lda, B, ldb)  1

#endif /* DEBUG_NOBLAS */

#endif /* _BASE_HEADER_Z */

#ifndef _BASE_HEADER_C
#define _BASE_HEADER_C

#include "base_tmp.h" /* TODO */

/************************************************************/
/* BLAS C Interfaces                                        */
/*                                                          */
/* Example of interface to use :                            */
/*  BLAS_DDOT(int n, double *x, int incx, double *y,        */
/*                                              int incy);  */
/*                                                          */
/* For portability you *must* always use variables in       */
/*   arguments : int UN=1; BLAS_DDOT(n, x, UN, y, UN);      */
/*                                                          */
/************************************************************/

/************************************************************/
/* $Id: base.template.h,v 1.1 2006/06/01 11:55:43 gaidamou Exp $ */
/************************************************************/

/************************************************************/
/* Help to read this file :                                 */
/*                                                          */
/* If C interface is avaible on the architecture :          */
/*  BLAS_DDOT is redefined as ddot/ddot_/...                */ 
/*                                                          */
/* Else                                                     */
/*  BLAS_DDOT macro calls BLAS_FDDOT and converts argument  */
/*            types for the FORTRAN interface.              */
/*  BLAS_FDDOT is redefined as ddot/ddot_/...               */
/*                                                          */
/* For CBLAS, BLAS_DDOT macro calls cblas_ddot.             */
/************************************************************/

#ifndef DEBUG_NOBLAS

#include <complex.h>
/* GCC http://gcc.gnu.org/onlinedocs/gcc-3.2.3/gcc/Complex.html */
/* lesser than ISO C99 #define complex _Complex double */


/********************** CBLAS ATLAS *************************/
#if defined(CBLAS)
#define _C_INTERFACE_
#include <cblas.h>

/********************** SP2 RS6000 **************************/
#elif defined(SP2) || defined(RS6000)
#define _C_INTERFACE_
#include <essl.h>
/*  EX : #define BLAS_DDOT ddot */
#define BLAS_CDOTC cdotc
#define BLAS_SCNRM2 scnrm2
#define BLAS_CSWAP cswap
#define BLAS_CCOPY ccopy
#define BLAS_CAXPY caxpy
#define BLAS_CSCAL cscal
#define BLAS_CSSCAL csscal
#define BLAS_CGEMV cgemv
#define BLAS_CGBMV cgbmv
#define BLAS_CTRMV ctrmv
#define BLAS_CTBMV ctbmv
#define BLAS_CTPMV ctpmv
#define BLAS_CTRSV ctrsv
#define BLAS_CTBSV ctbsv
#define BLAS_CTPSV ctpsv
#define BLAS_CSYMV csymv
#define BLAS_CHBMV chbmv
#define BLAS_CHPMV chpmv
#define BLAS_CHER cher
#define BLAS_CHPR chpr
#define BLAS_CHYR2 chyr2
#define BLAS_CHPR2 chpr2
#define BLAS_CGEMM cgemm
#define BLAS_CSYMM csymm
#define BLAS_CSYRK csyrk
#define BLAS_CSYR2K csyr2k
#define BLAS_CTRMM ctrmm
#define BLAS_CTRSM ctrsm


/********************** SGI *********************************/
#elif defined(SGI)
#define _C_INTERFACE_
#include <scsl_blas.h>
/*  EX : #define BLAS_DDOT ddot */
#define BLAS_CDOTC cdotc
#define BLAS_SCNRM2 scnrm2
#define BLAS_CSWAP cswap
#define BLAS_CCOPY ccopy
#define BLAS_CAXPY caxpy
#define BLAS_CSCAL cscal
#define BLAS_CSSCAL csscal
#define BLAS_CGEMV cgemv
#define BLAS_CGBMV cgbmv
#define BLAS_CTRMV ctrmv
#define BLAS_CTBMV ctbmv
#define BLAS_CTPMV ctpmv
#define BLAS_CTRSV ctrsv
#define BLAS_CTBSV ctbsv
#define BLAS_CTPSV ctpsv
#define BLAS_CSYMV csymv
#define BLAS_CHBMV chbmv
#define BLAS_CHPMV chpmv
#define BLAS_CHER cher
#define BLAS_CHPR chpr
#define BLAS_CHYR2 chyr2
#define BLAS_CHPR2 chpr2
#define BLAS_CGEMM cgemm
#define BLAS_CSYMM csymm
#define BLAS_CSYRK csyrk
#define BLAS_CSYR2K csyr2k
#define BLAS_CTRMM ctrmm
#define BLAS_CTRSM ctrsm


/********************** CRAY ********************************/
#elif defined(CRAY)
/*  EX : #define BLAS_FDDOT DDOT */
#define BLAS_FCDOTC CDOTC
#define BLAS_FSCNRM2 SCNRM2
#define BLAS_FCSWAP CSWAP
#define BLAS_FCCOPY CCOPY
#define BLAS_FCAXPY CAXPY
#define BLAS_FCSCAL CSCAL
#define BLAS_FCSSCAL CSSCAL
#define BLAS_FCGEMV CGEMV
#define BLAS_FCGBMV CGBMV
#define BLAS_FCTRMV CTRMV
#define BLAS_FCTBMV CTBMV
#define BLAS_FCTPMV CTPMV
#define BLAS_FCTRSV CTRSV
#define BLAS_FCTBSV CTBSV
#define BLAS_FCTPSV CTPSV
#define BLAS_FCSYMV CSYMV
#define BLAS_FCHBMV CHBMV
#define BLAS_FCHPMV CHPMV
#define BLAS_FCHER CHER
#define BLAS_FCHPR CHPR
#define BLAS_FCHYR2 CHYR2
#define BLAS_FCHPR2 CHPR2
#define BLAS_FCGEMM CGEMM
#define BLAS_FCSYMM CSYMM
#define BLAS_FCSYRK CSYRK
#define BLAS_FCSYR2K CSYR2K
#define BLAS_FCTRMM CTRMM
#define BLAS_FCTRSM CTRSM


/********************** LINUX *******************************/
#elif defined(LINUX)
/* EX : #define BLAS_FDDOT ddot_ */
#define BLAS_FCDOTC cdotc_
#define BLAS_FSCNRM2 scnrm2_
#define BLAS_FCSWAP cswap_
#define BLAS_FCCOPY ccopy_
#define BLAS_FCAXPY caxpy_
#define BLAS_FCSCAL cscal_
#define BLAS_FCSSCAL csscal_
#define BLAS_FCGEMV cgemv_
#define BLAS_FCGBMV cgbmv_
#define BLAS_FCTRMV ctrmv_
#define BLAS_FCTBMV ctbmv_
#define BLAS_FCTPMV ctpmv_
#define BLAS_FCTRSV ctrsv_
#define BLAS_FCTBSV ctbsv_
#define BLAS_FCTPSV ctpsv_
#define BLAS_FCSYMV csymv_
#define BLAS_FCHBMV chbmv_
#define BLAS_FCHPMV chpmv_
#define BLAS_FCHER cher_
#define BLAS_FCHPR chpr_
#define BLAS_FCHYR2 chyr2_
#define BLAS_FCHPR2 chpr2_
#define BLAS_FCGEMM cgemm_
#define BLAS_FCSYMM csymm_
#define BLAS_FCSYRK csyrk_
#define BLAS_FCSYR2K csyr2k_
#define BLAS_FCTRMM ctrmm_
#define BLAS_FCTRSM ctrsm_

#endif /* defined(..arch..) */

#ifndef _C_INTERFACE_
/* FORTRAN subroutine declarations */
/* EX : extern double BLAS_FDDOT(int *n, double *x, int *incx, double *y, int *incy);  */
extern float complex BLAS_FCDOTC(int *N, const float complex *X, int *incX, const float complex *Y, int *incY);
extern float BLAS_FSCNRM2(int *N, const float complex *X, int *incX);
extern void BLAS_FCSWAP(int *N, float complex *X, int *incX, float complex *Y, int *incY);
extern void BLAS_FCCOPY(int *N, const float complex *X, int *incX, float complex *Y, int *incY);
extern void BLAS_FCAXPY(int *N, const float complex* alpha, const float complex *X, int *incX, float complex *Y, int *incY);
extern void BLAS_FCSCAL(int *N, const float complex* alpha, float complex *X, int *incX);
extern void BLAS_FCSSCAL(int *N, const float* alpha, float complex *X, int *incX);
extern void BLAS_FCGEMV(char *TransA, int *M, int *N, const float complex* alpha, const float complex *A, int *lda, const float complex *X, int *incX, const float complex* beta, float complex *Y, int *incY);
extern void BLAS_FCGBMV(char *TransA, int *M, int *N, int *KL, int *KU, const float complex* alpha, const float complex *A, int *lda, const float complex *X, int *incX, const float complex* beta, float complex *Y, int *incY);
extern void BLAS_FCTRMV(char *Uplo, char *TransA, char *Diag, int *N, const float complex *A, int *lda, float complex *X, int *incX);
extern void BLAS_FCTBMV(char *Uplo, char *TransA, char *Diag, int *N, int *K, const float complex *A, int *lda, float complex *X, int *incX);
extern void BLAS_FCTPMV(char *Uplo, char *TransA, char *Diag, int *N, const float complex *Ap, float complex *X, int *incX);
extern void BLAS_FCTRSV(char *Uplo, char *TransA, char *Diag, int *N, const float complex *A, int *lda, float complex *X, int *incX);
extern void BLAS_FCTBSV(char *Uplo, char *TransA, char *Diag, int *N, int *K, const float complex *A, int *lda, float complex *X, int *incX);
extern void BLAS_FCTPSV(char *Uplo, char *TransA, char *Diag, int *N, const float complex *Ap, float complex *X, int *incX);
extern void BLAS_FCSYMV(char *Uplo, int *N, const float complex* alpha, const float complex *A, int *lda, const float complex *X, int *incX, const float complex* beta, float complex *Y, int *incY);
extern void BLAS_FCHBMV(char *Uplo, int *N, int *K, const float complex* alpha, const float complex *A, int *lda, const float complex *X, int *incX, const float complex* beta, float complex *Y, int *incY);
extern void BLAS_FCHPMV(char *Uplo, int *N, const float complex* alpha, const float complex *Ap, const float complex *X, int *incX, const float complex* beta, float complex *Y, int *incY);
extern void BLAS_FCHER(int *M, int *N, const float complex* alpha, const float complex *X, int *incX, const float complex *Y, int *incY, float complex *A, int *lda);
extern void BLAS_FCHPR(char *Uplo, int *N, const float complex* alpha, const float complex *X, int *incX, float complex *Ap);
extern void BLAS_FCHYR2(char *Uplo, int *N, const float complex* alpha, const float complex *X, int *incX, const float complex *Y, int *incY, float complex *A, int *lda);
extern void BLAS_FCHPR2(char *Uplo, int *N, const float complex* alpha, const float complex *X, int *incX, const float complex *Y, int *incY, float complex *A);
extern void BLAS_FCGEMM(char *TransA, char *TransB, int *M, int *N, int *K, const float complex* alpha, const float complex *A, int *lda, const float complex *B, int *ldb, const float complex* beta, float complex *C, int *ldc);
extern void BLAS_FCSYMM(char *Side, char *Uplo, int *M, int *N, const float complex* alpha, const float complex *A, int *lda, const float complex *B, int *ldb, const float complex* beta, float complex *C, int *ldc);
extern void BLAS_FCSYRK(char *Uplo, char *Trans, int *N, int *K, const float complex* alpha, const float complex *A, int *lda, const float complex* beta, float complex *C, int *ldc);
extern void BLAS_FCSYR2K(char *Uplo, char *Trans, int *N, int *K, const float complex* alpha, const float complex *A, int *lda, const float complex *B, int *ldb, const float complex* beta, float complex *C, int *ldc);
extern void BLAS_FCTRMM(char *Side, char *Uplo, char *TransA, char *Diag, int *M, int *N, const float complex* alpha, const float complex *A, int *lda, float complex *B, int *ldb);
extern void BLAS_FCTRSM(char *Side, char *Uplo, char *TransA, char *Diag, int *M, int *N, const float complex* alpha, const float complex *A, int *lda, float complex *B, int *ldb);


/* Call FORTRAN subroutines by using C like interface */
/* EX : #define BLAS_DDOT(n,x,incx,y,incy) BLAS_FDDOT(&(n),(x),&(incx),(y),&(incy)) */
#define   BLAS_CDOTC(N, X, incX, Y, incY) \
   BLAS_FCDOTC(&(N), (X), &(incX), (Y), &(incY))
#define   BLAS_SCNRM2(N, X, incX) \
   BLAS_FSCNRM2(&(N), (X), &(incX))
#define   BLAS_CSWAP(N, X, incX, Y, incY) \
   BLAS_FCSWAP(&(N), (X), &(incX), (Y), &(incY))
#define   BLAS_CCOPY(N, X, incX, Y, incY) \
   BLAS_FCCOPY(&(N), (X), &(incX), (Y), &(incY))
#define   BLAS_CAXPY(N, alpha, X, incX, Y, incY) \
   BLAS_FCAXPY(&(N), &(alpha), (X), &(incX), (Y), &(incY))
#define   BLAS_CSCAL(N, alpha, X, incX) \
   BLAS_FCSCAL(&(N), &(alpha), (X), &(incX))
#define   BLAS_CSSCAL(N, alpha, X, incX) \
   BLAS_FCSSCAL(&(N), &(alpha), (X), &(incX))
#define   BLAS_CGEMV(TransA, M, N, alpha, A, lda, X, incX, beta, Y, incY) \
   BLAS_FCGEMV((TransA), &(M), &(N), &(alpha), (A), &(lda), (X), &(incX), &(beta), (Y), &(incY))
#define   BLAS_CGBMV(TransA, M, N, KL, KU, alpha, A, lda, X, incX, beta, Y, incY) \
   BLAS_FCGBMV((TransA), &(M), &(N), &(KL), &(KU), &(alpha), (A), &(lda), (X), &(incX), &(beta), (Y), &(incY))
#define   BLAS_CTRMV(Uplo, TransA, Diag, N, A, lda, X, incX) \
   BLAS_FCTRMV((Uplo), (TransA), (Diag), &(N), (A), &(lda), (X), &(incX))
#define   BLAS_CTBMV(Uplo, TransA, Diag, N, K, A, lda, X, incX) \
   BLAS_FCTBMV((Uplo), (TransA), (Diag), &(N), &(K), (A), &(lda), (X), &(incX))
#define   BLAS_CTPMV(Uplo, TransA, Diag, N, Ap, X, incX) \
   BLAS_FCTPMV((Uplo), (TransA), (Diag), &(N), (Ap), (X), &(incX))
#define   BLAS_CTRSV(Uplo, TransA, Diag, N, A, lda, X, incX) \
   BLAS_FCTRSV((Uplo), (TransA), (Diag), &(N), (A), &(lda), (X), &(incX))
#define   BLAS_CTBSV(Uplo, TransA, Diag, N, K, A, lda, X, incX) \
   BLAS_FCTBSV((Uplo), (TransA), (Diag), &(N), &(K), (A), &(lda), (X), &(incX))
#define   BLAS_CTPSV(Uplo, TransA, Diag, N, Ap, X, incX) \
   BLAS_FCTPSV((Uplo), (TransA), (Diag), &(N), (Ap), (X), &(incX))
#define   BLAS_CSYMV(Uplo, N, alpha, A, lda, X, incX, beta, Y, incY) \
   BLAS_FCSYMV((Uplo), &(N), &(alpha), (A), &(lda), (X), &(incX), &(beta), (Y), &(incY))
#define   BLAS_CHBMV(Uplo, N, K, alpha, A, lda, X, incX, beta, Y, incY) \
   BLAS_FCHBMV((Uplo), &(N), &(K), &(alpha), (A), &(lda), (X), &(incX), &(beta), (Y), &(incY))
#define   BLAS_CHPMV(Uplo, N, alpha, Ap, X, incX, beta, Y, incY) \
   BLAS_FCHPMV((Uplo), &(N), &(alpha), (Ap), (X), &(incX), &(beta), (Y), &(incY))
#define   BLAS_CHER(M, N, alpha, X, incX, Y, incY, A, lda) \
   BLAS_FCHER(&(M), &(N), &(alpha), (X), &(incX), (Y), &(incY), (A), &(lda))
#define   BLAS_CHPR(Uplo, N, alpha, X, incX, Ap) \
   BLAS_FCHPR((Uplo), &(N), &(alpha), (X), &(incX), (A)p)
#define   BLAS_CHYR2(Uplo, N, alpha, X, incX, Y, incY, A, lda) \
   BLAS_FCHYR2((Uplo), &(N), &(alpha), (X), &(incX), (Y), &(incY), (A), &(lda))
#define   BLAS_CHPR2(Uplo, N, alpha, X, incX, Y, incY, A) \
   BLAS_FCHPR2((Uplo), &(N), &(alpha), (X), &(incX), (Y), &(incY), (A))
#define   BLAS_CGEMM(TransA, TransB, M, N, K, alpha, A, lda, B, ldb, beta, C, ldc) \
   BLAS_FCGEMM((TransA), (TransB), &(M), &(N), &(K), &(alpha), (A), &(lda), (B), &(ldb), &(beta), (C), &(ldc))
#define   BLAS_CSYMM(Side, Uplo, M, N, alpha, A, lda, B, ldb, beta, C, ldc) \
   BLAS_FCSYMM((Side), (Uplo), &(M), &(N), &(alpha), (A), &(lda), (B), &(ldb), &(beta), (C), &(ldc))
#define   BLAS_CSYRK(Uplo, Trans, N, K, alpha, A, lda, beta, C, ldc) \
   BLAS_FCSYRK((Uplo), (Trans), &(N), &(K), &(alpha), (A), &(lda), &(beta), (C), &(ldc))
#define   BLAS_CSYR2K(Uplo, Trans, N, K, alpha, A, lda, B, ldb, beta, C, ldc) \
   BLAS_FCSYR2K((Uplo), (Trans), &(N), &(K), &(alpha), (A), &(lda), (B), &(ldb), &(beta), (C), &(ldc))
#define   BLAS_CTRMM(Side, Uplo, TransA, Diag, M, N, alpha, A, lda, B, ldb) \
   BLAS_FCTRMM((Side), (Uplo), (TransA), (Diag), &(M), &(N), &(alpha), (A), &(lda), (B), &(ldb))
#define   BLAS_CTRSM(Side, Uplo, TransA, Diag, M, N, alpha, A, lda, B, ldb) \
   BLAS_FCTRSM((Side), (Uplo), (TransA), (Diag), &(M), &(N), &(alpha), (A), &(lda), (B), &(ldb))

#endif /* _C_INTERFACE_ */

#if defined(CBLAS)
/* Translate common blas interface to cblas interface */
/* #define   BLAS_DDOT(N, X, incX, Y, incY) \
      cblas_ddot((N), (X), (incX), (Y), (incY)) */
#define   BLAS_CDOTC(N, X, incX, Y, incY) \
   cblas_cdotc((N), (X), (incX), (Y), (incY))
#define   BLAS_SCNRM2(N, X, incX) \
   cblas_scnrm2((N), (X), (incX))
#define   BLAS_CSWAP(N, X, incX, Y, incY) \
   cblas_cswap((N), (X), (incX), (Y), (incY))
#define   BLAS_CCOPY(N, X, incX, Y, incY) \
   cblas_ccopy((N), (X), (incX), (Y), (incY))
#define   BLAS_CAXPY(N, alpha, X, incX, Y, incY) \
   cblas_caxpy((N), (alpha), (X), (incX), (Y), (incY))
#define   BLAS_CSCAL(N, alpha, X, incX) \
   cblas_cscal((N), (alpha), (X), (incX))
#define   BLAS_CSSCAL(N, alpha, X, incX) \
   cblas_csscal((N), (alpha), (X), (incX))
#define   BLAS_CGEMV(TransA, M, N, alpha, A, lda, X, incX, beta, Y, incY) \
   cblas_cgemv(CblasColMajor, (TransA=="N")?CblasNoTrans:((TransA=="T")?CblasTrans:CblasConjTrans), (M), (N), (alpha), (A), (lda), (X), (incX), (beta), (Y), (incY))
#define   BLAS_CGBMV(TransA, M, N, KL, KU, alpha, A, lda, X, incX, beta, Y, incY) \
   cblas_cgbmv(CblasColMajor, (TransA=="N")?CblasNoTrans:((TransA=="T")?CblasTrans:CblasConjTrans), (M), (N), (KL), (KU), (alpha), (A), (lda), (X), (incX), (beta), (Y), (incY))
#define   BLAS_CTRMV(Uplo, TransA, Diag, N, A, lda, X, incX) \
   cblas_ctrmv(CblasColMajor, (Uplo=="U")?CblasUpper:CblasLower, (TransA=="N")?CblasNoTrans:((TransA=="T")?CblasTrans:CblasConjTrans), (Diag=="N")?CblasNonUnit:CblasUnit, (N), (A), (lda), (X), (incX))
#define   BLAS_CTBMV(Uplo, TransA, Diag, N, K, A, lda, X, incX) \
   cblas_ctbmv(CblasColMajor, (Uplo=="U")?CblasUpper:CblasLower, (TransA=="N")?CblasNoTrans:((TransA=="T")?CblasTrans:CblasConjTrans), (Diag=="N")?CblasNonUnit:CblasUnit, (N), (K), (A), (lda), (X), (incX))
#define   BLAS_CTPMV(Uplo, TransA, Diag, N, Ap, X, incX) \
   cblas_ctpmv(CblasColMajor, (Uplo=="U")?CblasUpper:CblasLower, (TransA=="N")?CblasNoTrans:((TransA=="T")?CblasTrans:CblasConjTrans), (Diag=="N")?CblasNonUnit:CblasUnit, (N), (Ap), (X), (incX))
#define   BLAS_CTRSV(Uplo, TransA, Diag, N, A, lda, X, incX) \
   cblas_ctrsv(CblasColMajor, (Uplo=="U")?CblasUpper:CblasLower, (TransA=="N")?CblasNoTrans:((TransA=="T")?CblasTrans:CblasConjTrans), (Diag=="N")?CblasNonUnit:CblasUnit, (N), (A), (lda), (X), (incX))
#define   BLAS_CTBSV(Uplo, TransA, Diag, N, K, A, lda, X, incX) \
   cblas_ctbsv(CblasColMajor, (Uplo=="U")?CblasUpper:CblasLower, (TransA=="N")?CblasNoTrans:((TransA=="T")?CblasTrans:CblasConjTrans), (Diag=="N")?CblasNonUnit:CblasUnit, (N), (K), (A), (lda), (X), (incX))
#define   BLAS_CTPSV(Uplo, TransA, Diag, N, Ap, X, incX) \
   cblas_ctpsv(CblasColMajor, (Uplo=="U")?CblasUpper:CblasLower, (TransA=="N")?CblasNoTrans:((TransA=="T")?CblasTrans:CblasConjTrans), (Diag=="N")?CblasNonUnit:CblasUnit, (N), (Ap), (X), (incX))
#define   BLAS_CSYMV(Uplo, N, alpha, A, lda, X, incX, beta, Y, incY) \
   cblas_csymv(CblasColMajor, (Uplo=="U")?CblasUpper:CblasLower, (N), (alpha), (A), (lda), (X), (incX), (beta), (Y), (incY))
#define   BLAS_CHBMV(Uplo, N, K, alpha, A, lda, X, incX, beta, Y, incY) \
   cblas_chbmv(CblasColMajor, (Uplo=="U")?CblasUpper:CblasLower, (N), (K), (alpha), (A), (lda), (X), (incX), (beta), (Y), (incY))
#define   BLAS_CHPMV(Uplo, N, alpha, Ap, X, incX, beta, Y, incY) \
   cblas_chpmv(CblasColMajor, (Uplo=="U")?CblasUpper:CblasLower, (N), (alpha), (Ap), (X), (incX), (beta), (Y), (incY))
#define   BLAS_CHER(M, N, alpha, X, incX, Y, incY, A, lda) \
   cblas_cher(CblasColMajor, (M), (N), (alpha), (X), (incX), (Y), (incY), (A), (lda))
#define   BLAS_CHPR(Uplo, N, alpha, X, incX, Ap) \
   cblas_chpr(CblasColMajor, (Uplo=="U")?CblasUpper:CblasLower, (N), (alpha), (X), (incX), (A)p)
#define   BLAS_CHYR2(Uplo, N, alpha, X, incX, Y, incY, A, lda) \
   cblas_chyr2(CblasColMajor, (Uplo=="U")?CblasUpper:CblasLower, (N), (alpha), (X), (incX), (Y), (incY), (A), (lda))
#define   BLAS_CHPR2(Uplo, N, alpha, X, incX, Y, incY, A) \
   cblas_chpr2(CblasColMajor, (Uplo=="U")?CblasUpper:CblasLower, (N), (alpha), (X), (incX), (Y), (incY), (A))
#define   BLAS_CGEMM(TransA, TransB, M, N, K, alpha, A, lda, B, ldb, beta, C, ldc) \
   cblas_cgemm(CblasColMajor, (TransA=="N")?CblasNoTrans:((TransA=="T")?CblasTrans:CblasConjTrans), (TransB=="N")?CblasNoTrans:((TransB=="T")?CblasTrans:CblasConjTrans), (M), (N), (K), (alpha), (A), (lda), (B), (ldb), (beta), (C), (ldc))
#define   BLAS_CSYMM(Side, Uplo, M, N, alpha, A, lda, B, ldb, beta, C, ldc) \
   cblas_csymm(CblasColMajor, (Side=="L")?CblasLeft:CblasRight, (Uplo=="U")?CblasUpper:CblasLower, (M), (N), (alpha), (A), (lda), (B), (ldb), (beta), (C), (ldc))
#define   BLAS_CSYRK(Uplo, Trans, N, K, alpha, A, lda, beta, C, ldc) \
   cblas_csyrk(CblasColMajor, (Uplo=="U")?CblasUpper:CblasLower, (Trans =="N")?CblasNoTrans:((Trans =="T")?CblasTrans:CblasConjTrans), (N), (K), (alpha), (A), (lda), (beta), (C), (ldc))
#define   BLAS_CSYR2K(Uplo, Trans, N, K, alpha, A, lda, B, ldb, beta, C, ldc) \
   cblas_csyr2k(CblasColMajor, (Uplo=="U")?CblasUpper:CblasLower, (Trans =="N")?CblasNoTrans:((Trans =="T")?CblasTrans:CblasConjTrans), (N), (K), (alpha), (A), (lda), (B), (ldb), (beta), (C), (ldc))
#define   BLAS_CTRMM(Side, Uplo, TransA, Diag, M, N, alpha, A, lda, B, ldb) \
   cblas_ctrmm(CblasColMajor, (Side=="L")?CblasLeft:CblasRight, (Uplo=="U")?CblasUpper:CblasLower, (TransA=="N")?CblasNoTrans:((TransA=="T")?CblasTrans:CblasConjTrans), (Diag=="N")?CblasNonUnit:CblasUnit, (M), (N), (alpha), (A), (lda), (B), (ldb))
#define   BLAS_CTRSM(Side, Uplo, TransA, Diag, M, N, alpha, A, lda, B, ldb) \
   cblas_ctrsm(CblasColMajor, (Side=="L")?CblasLeft:CblasRight, (Uplo=="U")?CblasUpper:CblasLower, (TransA=="N")?CblasNoTrans:((TransA=="T")?CblasTrans:CblasConjTrans), (Diag=="N")?CblasNonUnit:CblasUnit, (M), (N), (alpha), (A), (lda), (B), (ldb))

#endif /* defined(CBLAS) */



#else /* DEBUG_NOBLAS */
/* For debug purpose : Skip every BLAS calls */
/* #define   BLAS_DDOT(N, X, incX, Y, incY)  1 */
#define   BLAS_CDOTC(N, X, incX, Y, incY)  1
#define   BLAS_SCNRM2(N, X, incX)  1
#define   BLAS_CSWAP(N, X, incX, Y, incY)  1
#define   BLAS_CCOPY(N, X, incX, Y, incY)  1
#define   BLAS_CAXPY(N, alpha, X, incX, Y, incY)  1
#define   BLAS_CSCAL(N, alpha, X, incX)  1
#define   BLAS_CSSCAL(N, alpha, X, incX)  1
#define   BLAS_CGEMV(TransA, M, N, alpha, A, lda, X, incX, beta, Y, incY)  1
#define   BLAS_CGBMV(TransA, M, N, KL, KU, alpha, A, lda, X, incX, beta, Y, incY)  1
#define   BLAS_CTRMV(Uplo, TransA, Diag, N, A, lda, X, incX)  1
#define   BLAS_CTBMV(Uplo, TransA, Diag, N, K, A, lda, X, incX)  1
#define   BLAS_CTPMV(Uplo, TransA, Diag, N, Ap, X, incX)  1
#define   BLAS_CTRSV(Uplo, TransA, Diag, N, A, lda, X, incX)  1
#define   BLAS_CTBSV(Uplo, TransA, Diag, N, K, A, lda, X, incX)  1
#define   BLAS_CTPSV(Uplo, TransA, Diag, N, Ap, X, incX)  1
#define   BLAS_CSYMV(Uplo, N, alpha, A, lda, X, incX, beta, Y, incY)  1
#define   BLAS_CHBMV(Uplo, N, K, alpha, A, lda, X, incX, beta, Y, incY)  1
#define   BLAS_CHPMV(Uplo, N, alpha, Ap, X, incX, beta, Y, incY)  1
#define   BLAS_CHER(M, N, alpha, X, incX, Y, incY, A, lda)  1
#define   BLAS_CHPR(Uplo, N, alpha, X, incX, Ap)  1
#define   BLAS_CHYR2(Uplo, N, alpha, X, incX, Y, incY, A, lda)  1
#define   BLAS_CHPR2(Uplo, N, alpha, X, incX, Y, incY, A)  1
#define   BLAS_CGEMM(TransA, TransB, M, N, K, alpha, A, lda, B, ldb, beta, C, ldc)  1
#define   BLAS_CSYMM(Side, Uplo, M, N, alpha, A, lda, B, ldb, beta, C, ldc)  1
#define   BLAS_CSYRK(Uplo, Trans, N, K, alpha, A, lda, beta, C, ldc)  1
#define   BLAS_CSYR2K(Uplo, Trans, N, K, alpha, A, lda, B, ldb, beta, C, ldc)  1
#define   BLAS_CTRMM(Side, Uplo, TransA, Diag, M, N, alpha, A, lda, B, ldb)  1
#define   BLAS_CTRSM(Side, Uplo, TransA, Diag, M, N, alpha, A, lda, B, ldb)  1

#endif /* DEBUG_NOBLAS */

#endif /* _BASE_HEADER_C */


/* @authors J. GAIDAMOUR, P. HENON */
#ifndef TYPE_H
#define TYPE_H


/* Set default TYPE_ */
#if !( defined(TYPE_REAL) || defined(TYPE_COMPLEX) )
#define TYPE_REAL
#endif

/* Set default PREC_ */
#if !( defined(PREC_SIMPLE) || defined(PREC_DOUBLE) )
#define PREC_DOUBLE
#endif

/* mutual exclusion */
#ifdef TYPE_REAL
#undef TYPE_COMPLEX
#endif

/* mutual exclusion */
#ifdef PREC_SIMPLE
#undef PREC_DOUBLE
#endif

/**/
#include "redefine_functions.h"
#include "hips.h"

#define CC(arg) (arg)
#define LENC 1    
/****************************************************************/
/****************************************************************/
/* todo : struct complex */
/* GCC http://gcc.gnu.org/onlinedocs/gcc-3.2.3/gcc/Complex.html */
/* lesser than ISO C99 #define complex _Complex double */

/***************************************************************/
#if defined(TYPE_REAL)

#define CONJ(arg) arg
#define CREAL(arg) arg
#if defined(PREC_SIMPLE)
#define CIMAG(arg) (float)0
#else
#define CIMAG(arg) (double)0
#endif


#define coefsqrt(arg) sqrt((arg))
#define coefabs(arg)  fabs((arg))

#if defined(PREC_SIMPLE)
#define MPI_COEF_TYPE MPI_FLOAT
#define MPI_REAL_TYPE MPI_FLOAT
#else
#define MPI_COEF_TYPE MPI_DOUBLE
#define MPI_REAL_TYPE MPI_DOUBLE
#endif

#define _coef_ "%g"
#define pcoef(arg) (arg)

/***************************************************************/
/* #elif defined(TYPE_FLOAT) */

/* #define CONJ(arg) arg */
/* #define CREAL(arg) arg */
/* #if defined(PREC_SIMPLE) */
/* #define CIMAG(arg) (float)0 */
/* #else */
/* #define CIMAG(arg) (double)0 */
/* #endif */


/* #define coefsqrt(arg) sqrt((arg)) */
/* #define coefabs(arg)  fabs((arg)) */

/* #if defined(PREC_SIMPLE) */
/* #define MPI_COEF_TYPE MPI_FLOAT */
/* #define MPI_REAL_TYPE MPI_FLOAT */
/* #else */
/* #define MPI_COEF_TYPE MPI_FLOAT */
/* #define MPI_REAL_TYPE MPI_FLOAT */
/* #endif */

/* #define _coef_ "%g" */
/* #define pcoef(arg) (arg) */

/***************************************************************/
#elif defined(TYPE_COMPLEX)

#define CONJ(arg) conj(arg)
#define CREAL(arg) creal((arg))
#define CIMAG(arg) cimag((arg))

#define coefsqrt(arg) csqrt((arg))
#define coefabs(arg)  cabs((arg))

#if defined(PREC_SIMPLE)
#ifdef MPI_FLOAT_COMPLEX
#define MPI_COEF_TYPE MPI_FLOAT_COMPLEX
#else
#define MPI_COEF_TYPE MPI_FLOAT
#undef CC
#undef LENC
#define CC(arg) (arg)*2
#define LENC 2
#endif
#define MPI_REAL_TYPE MPI_FLOAT

#else /* PREC_SIMPLE */

#ifdef MPI_DOUBLE_COMPLEX
#define MPI_COEF_TYPE MPI_DOUBLE_COMPLEX
#else
#define MPI_COEF_TYPE MPI_DOUBLE
#undef CC
#undef LENC
#define CC(arg) (arg)*2
#define LENC 2
#endif
#define MPI_REAL_TYPE MPI_DOUBLE

#endif

#define _coef_ "%g+%gi"
#define pcoef(arg) CREAL((arg)), CIMAG((arg))

#if defined(TYPE_COMPLEX_IBM)

#ifndef  _DCMPLX
#define  _DCMPLX 1
#ifndef  _REIM
#define _REIM   1
#endif
typedef union { struct { double _re, _im; } _data; double _align; } dcmplx;
#endif

#ifdef  _REIM
#define RE(x)   ((x)._data._re)
#define IM(x)   ((x)._data._im)
#endif

#endif /* TYPE_COMPLEX_IBM */

/***************************************************************/
/* #elif defined(TYPE_COMPLEX_FLOAT) */
/* #include <complex.h> */
/* #define REAL float */
/* #define COEF float complex */

/* #define CREAL(arg) crealf((arg)) */
/* #define CIMAG(arg) cimagf((arg)) */

/* #define coefsqrt(arg) csqrtf((arg)) */
/* #define coefabs(arg)  cabsf((arg)) */

/* #define MPI_COEF_TYPE MPI_COMPLEX */

/* /\* #define _coef_ "%gi%g" *\/ */
/* #define _coef_ "%g+%gi" */
/* #define pcoef(arg) CREAL((arg)), CIMAG((arg)) */

/***************************************************************/
#else
/** TYPE_REAL or TYPE_COMPLEX macro mandatory (makefile.in) **/
#error TYPE_REAL (or TYPE_COMPLEX) macro undefined

#endif

#ifndef INT_TYPE_H
#define INT_TYPE_H




#ifdef COMM_INT
#warning COMM_INT already defined
#undef COMM_INT
#endif

#ifdef COMM_BIG_INT
#warning COMM_BIG_INT already defined
#undef COMM_BIG_INT
#endif

#ifdef EE
#warning EE already defined
#undef EE
#endif

#ifdef FF
#warning FF already defined
#undef FF
#endif

#include <stdlib.h>

/** default if INTSSIZE64, INTSIZE64 or INTSIZE32 are not defined **/ 
/*#if !defined(INTSIZE_64) && !defined(INTSIZE_32)*/
#ifndef INTSSIZE64
#ifndef INTSIZE64
#ifndef INTSIZE32
/*#define INTL int
  #define INTS int*/
#define COMM_INT MPI_INT
#define COMM_BIG_INT MPI_INT
#define _int_ "%d"
#define _ints_ "%d"
#define EE(arg) (arg)
#define FF(arg) (arg)
#endif
#endif
#endif

#ifdef INTSSIZE64

/*#define INTL int64_t*/
#define _int_ "%lld"
#define _ints_ "%lld"

#ifdef MPI_INTEGER8
#define COMM_BIG_INT MPI_INTEGER8
#define FF(arg) (arg)
#else
#define COMM_BIG_INT MPI_BYTE
#define FF(arg) (arg)*8
#endif

#ifdef MPI_INTEGER8
#define COMM_INT MPI_INTEGER8
#define EE(arg) (arg)
#else
#define COMM_INT MPI_BYTE
#define EE(arg) (arg)*8
#endif

#endif /* INTSSIZE64 */



#ifdef INTSIZE64

/*#define INTL int64_t*/
#define _int_ "%lld"
#define _ints_ "%d"
/*#define INTS int32_t*/

#ifdef MPI_INTEGER8
#define COMM_BIG_INT MPI_INTEGER8
#define FF(arg) (arg)
#else
#define COMM_BIG_INT MPI_BYTE
#define FF(arg) (arg)*8
#endif

#ifdef MPI_INTEGER4
#define COMM_INT MPI_INTEGER4
#define EE(arg) (arg)
#else
#define COMM_INT MPI_BYTE
#define EE(arg) (arg)*4
#endif

#endif /* INTSIZE64 */

#ifdef INTSIZE32

/*#define INTL int32_t*/
#define _int_ "%d"
#define _ints_ "%d"
/*#define INTS int32_t*/

#ifdef MPI_INTEGER4
#define COMM_BIG_INT MPI_INTEGER4
#define FF(arg) (arg)
#else
#define COMM_BIG_INT MPI_BYTE
#define FF(arg) (arg)*4
#endif

#ifdef MPI_INTEGER4
#define COMM_INT MPI_INTEGER4
#define EE(arg) (arg)
#else
#define COMM_INT MPI_BYTE
#define EE(arg) (arg)*4
#endif

#endif /* INTSIZE32 */

#undef LONG
#define LONG long
#endif

#if defined(PREC_SIMPLE)
#define _scan_f_ "%f"
#define _scan_g_ "%g"
#else
#define _scan_f_ "%lf"
#define _scan_g_ "%lg"
#endif

typedef double chrono_t;

#endif /* TYPE_H */

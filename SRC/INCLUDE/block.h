/* @authors J. GAIDAMOUR */
#ifndef BLOCK_H
#define BLOCK_H
#include "type.h"

/* double */
#ifndef TEST
#include "phidal_sequential.h"
#endif

#include "symbol.h"
#include "solver.h"
#include "db_struct.h"

typedef struct Prec_  DBPrec;

#ifdef PARALLEL


/* /\* #define GEMM_LEFT_LOOKING *\/ */

/* #ifdef GEMM_LEFT_LOOKING */
/* #define ALLOC_NOREC BLK */
/* #define ALLOC_L     ONE */
/* #define ALLOC_E     ONE */
/* #define ALLOC_S     BLK */
/* #define ALLOC_S2    BLK */
/* #define ALLOC_FILL  INGEMM */

/* #else  */

/* #define GEMM_RIGHT_LOOKING */

/* #define ALLOC_NOREC BLK */
/* #define ALLOC_L     ONE */

/* #define ALLOC_E     CBLK */
/* #define ALLOC_S     BLK */
/* #define ALLOC_S2    BLK */
/* #define ALLOC_FILL  INGEMM */

/* #endif */


/* DROPE */
#define ALLOC_NOREC ONE
#define ALLOC_L     ONE

#define ALLOC_E     CBLK
#define ALLOC_S     ONE
#define ALLOC_S2    ONE
#define ALLOC_FILL  INGEMM

#else
/***********************************************************/
/***********************************************************/


/* #define GEMM_LEFT_LOOKING /\* option *\/ */

#ifdef GEMM_LEFT_LOOKING
/* a commenter pour ./test.sh */
/* #define NO_PIC_DECREASE */

#define ALLOC_NOREC ONE
#define ALLOC_L     ONE
/* #define ALLOC_S2    ONE */

/* rÃ©duction du pic mÃ©moire */
#ifndef NO_PIC_DECREASE
#define ALLOC_E     RBLK
#define ALLOC_S     CBLK
#define ALLOC_FILL  INGEMM
#else
/* sans rÃ©duction du pic memoire */
#define ALLOC_E     ONE
#define ALLOC_S     ONE
#define ALLOC_FILL  BEFORE
#endif

#else 

#define GEMM_RIGHT_LOOKING

#define ALLOC_NOREC ONE
#define ALLOC_L     ONE

#define ALLOC_E     CBLK
#define ALLOC_S     ONE
#define ALLOC_S2    ONE
#define ALLOC_FILL  INGEMM

#endif

#endif /* PARALLEL */

/* TODO : if avec ALLOC_ => defined() */

/*TODO*/
/* #ifdef DEBUG_J */
#ifndef NOBLK
#ifdef USE_DB_PRECOND
#undef ALLOC_S
#define ALLOC_S BLK
#endif
#endif
/* #endif */

/*TODO*/
#undef ALLOC_S2
#define ALLOC_S2 ALLOC_S


/***********************************************************/
/***********************************************************/


/*#ifdef stride
  #warning STRIDE MACRO DEFINED !
  #undef stride
  #endif*/

/* #define Dprintfv(5, ...) printf(__VA_ARGS__) */
/* void Dprintfv(5, const char *format, ...) { static int i=0; if (i==0) printf("Warning\n : DEBUG COMMENTS\n"); i=1; } */
/* #define Dprintfv(5, args...) */
/* #define printLD2(arg1,arg2) */
/* void Dprintfv(5, const char *format, ...); */

enum walloc   {BEFORE, INGEMM};

void pvec(char* bla, REAL* vec, int n, int ex);
int calcCoefmax(SymbolMatrix* symbmtx1 /*M*/, SymbolMatrix* symbmtx /*L*/) ;
int DBMatrix_calcCoefmax(DBMatrix* M /*M*/, DBMatrix* L /*L*/);

void DBMatrix_HID2SymbolMatrix(DBMatrix* m, SymbolMatrix* symbmtx, PhidalHID* BL, flag_t facedecal, dim_t tli);


INTS DBMATRIX_PrecSolve(REAL tol, DBPrec *P, COEF *x, COEF *b, PhidalHID *BL, PhidalOptions *option, dim_t *itertab, REAL *resnormtab);
void DBMATRIX_Precond(PhidalMatrix *A, DBPrec *P, SymbolMatrix* symbmtx, PhidalHID *BL, PhidalOptions *option);
void DBMATRIX_MLICCPrec(PhidalMatrix *A, DBPrec *P, SymbolMatrix* symbmtx, PhidalHID *BL, PhidalOptions *option);
void DBMATRIX_MLILUPrec(PhidalMatrix *A, DBPrec *P, SymbolMatrix* symbmtx, PhidalHID *BL, PhidalOptions *option);

REAL SymbolMatrix_NNZ(SymbolMatrix *symbmtx);
void DBPrec_Info(DBPrec *p);
long DBPrec_NNZ_Before(DBPrecMem *m);
long DBPrec_NNZ_In(DBPrecMem *m);
long DBPrec_NNZ_After(flag_t schur_method, DBPrecMem *m);
long DBPrec_NNZ_LL(DBPrecMem *m);
long DBPrecPart_NNZ(char* TYPE, flag_t symmetric, DBMatrix* m);
long DBM_NNZ(char* TYPE, DBMatrix* m);

void DBPrec_SchurProd(DBPrec *P, PhidalHID *BL, COEF *x, COEF *y);

void DBMatrix_Lsolv(flag_t unitdiag, DBMatrix *L, COEF *x, COEF *b, PhidalHID *BL);
void DBMatrix_Usolv(flag_t unitdiag, DBMatrix *U, COEF *x, COEF *b, PhidalHID *BL);
void DBMatrix_Dsolv(DBMatrix *D, COEF* b);

/* void DBMatrix_Lsolv2(flag_t unitdiag, DBMatrix *L, COEF *x, COEF *b, PhidalHID *BL); */
/* void DBMatrix_Usolv2(flag_t unitdiag, DBMatrix *U, COEF *x, COEF *b, PhidalHID *BL); */
void DBMatrix_Dsolv2(DBMatrix *D, COEF* b);

void DBMATRIX_MatVec(DBMatrix *L, DBMatrix *U, PhidalHID *BL, COEF *x, COEF *y);
void DBMATRIX_MatVec2(DBMatrix *L, DBMatrix *U, PhidalHID *BL, COEF *x, COEF *y);

/**********/
void DBPrec_Init(DBPrec *p);
void DBPrec_Print(DBPrec *p);
void DBPrec_Clean(DBPrec *p);

void print_size(unsigned long size);
unsigned long DBMatrix_size(DBMatrix* m, PhidalHID* BL, flag_t verbose);
unsigned long SolverMatrix_size(SolverMatrix* m, flag_t verbose);
unsigned long SymbolMatrix_size(SymbolMatrix* m, flag_t verbose);

void freeMatrixStruct(MatrixStruct* mtxstr, dim_t cblknbr);
void freeSolverMatrix(SolverMatrix* solvmtx);
void freeSolverMatrix(SolverMatrix* solvmtx);
void freeSymbolMatrix(SymbolMatrix* symbmtx);
void DBMatrix_Clean(DBMatrix* m);
void SymbolMatrix_Init(SymbolMatrix* symbmtx);
void SymbolMatrix_Clean(SymbolMatrix* symbmtx);
void SolverMatrix_Clean(SolverMatrix* solvmtx);

void SymbolMatrix_Clean(SymbolMatrix* symbmtx);
void SolverMatrix_Clean(SolverMatrix* solvmtx);


/**/
void DBMatrix_Setup(dim_t tli, dim_t tlj, dim_t bri, dim_t brj, char *UPLO, char *DIAG, 
		    int_t locally_nbr, DBMatrix* m, 
		    SymbolMatrix* symbmtx, PhidalHID *BL, 
		    int_t levelnum, int_t levelnum_l, alloc_t alloc);
void DBMatrix_Setup_HID(dim_t tli, dim_t tlj, dim_t bri, dim_t brj, char *UPLO, char *DIAG, 
			int_t locally_nbr, DBMatrix* m, PhidalHID *BL);
void DBMatrix_Setup_SYMBOL(DBMatrix* m, SymbolMatrix* symbmtx, 
			   PhidalHID *BL, int_t levelnum, int_t levelnum_l);
/* void DBMatrix_Setup_cblktosolvmtx(DBMatrix* m); */

void DBMatrix_SetupV(DBMatrix *M, DBMatrix *Cpy);
/**/

void DBMatrix_Copy(PhidalMatrix* mat, PhidalHID* BL, 
		   PhidalMatrix* Phidal, DBMatrix* DBM,
		   char* UPLO, int_t levelnum, int_t levelnum_l, flag_t filling);


/**/


INTS DBMATRIX_Solve(PhidalMatrix *A, DBPrec *P, PhidalHID *BL, PhidalOptions *option, COEF *rhs, COEF * x, dim_t *itertab, REAL *resnormtab);



void DBMatrix_VirtualCpy(DBMatrix *M, DBMatrix *Cpy);
void DBMatrix_Transpose(DBMatrix *M);

/* void printSymbol(SymbolMatrix* symbmtx); */
/* void testEgal(SolverMatrix* solvmtx, SolverMatrix* solvmtx2, char* UPLO); */

/* void testEgalParcours(SolverMatrix* solvmtx, SymbolMatrix* symbmtx); */
/* void testEgal_Symbol(SymbolMatrix* symbmtx, SymbolMatrix* symbmtx2); */
/* void printCSR(csptr cs, char* filename) ; */

void cpy_Symbol(SymbolMatrix* symbmtx, SymbolMatrix* symbmtx2);

void DBMatrix_Cpy(DBMatrix *M, DBMatrix *Cpy); /*  TODO : nom trop ressemblant */
void DBMatrix_Copy2(DBMatrix* M, DBMatrix* Copy);

void DBMatrix_Symb2SolvMtx(SolverMatrix* solvmtx);
void DBMatrix_Symb2SolvMtx_(SolverMatrix* solvmtx, COEF* coeftab);

void DBMatrix_AllocSymbmtx(int i0, int i, DBMatrix* m, alloc_t alloc,
			   INTL *ia, dim_t *ja, VSolverMatrix **a,
			   INTL *Pia, dim_t *Pja, struct SparRow **Pa, 
			   SolverMatrix* solver, char* TRANS, PhidalHID* BL);

void DBMatrix_Init(DBMatrix *a);
void SymbolMatrix_VirtualCopy(SymbolMatrix *M, SymbolMatrix *Cpy);

void SparRow2SolverMatrix(dim_t tli, dim_t tlj, csptr mat, SymbolMatrix* symbmtx, SolverMatrix* solvmtx);

void print(PhidalMatrix* m, PhidalHID* BL);

void PHIDAL_SymbolMatrix(csptr mat, SymbolMatrix* symbmtx, dim_t **rangtab_pt, dim_t *treetab, dim_t cblknbr_l1, PhidalHID* BL, int_t locally_nbr);

void symbolicBlok(MatrixStruct* mtxstr, csptr mat, dim_t cblknbr, dim_t *rangtab);
void symbolicFacto(MatrixStruct* mtxstr, dim_t cblknbr, dim_t *treetab, dim_t *rangtab);

void MatrixStruct2SymbolMatrix(SymbolMatrix* symbmtx, MatrixStruct* mtxstr, dim_t cblknbr, dim_t *rangtab);
void csrSolverMatrix(SolverMatrix* solvmtx, csptr mat);

void numericFacto(SolverMatrix* solvmtx);
void DBMatrix_FACTO(UDBMatrix* m);
void DBMatrix_FACTO2(UDBMatrix* m);
void DBMatrix_FACTOu(UDBMatrix* LU);

void DBMatrix_TRSM(flag_t unitdiag, DBMatrix* L, DBMatrix* M);

void Lsolv(flag_t unitdiag, SolverMatrix* solvmtx, COEF *b, COEF *x);
void Usolv(flag_t unitdiag, SolverMatrix* solvmtx, COEF *b, COEF *x);
void Ltsolv(flag_t unitdiag, SolverMatrix* solvmtx, COEF *b, COEF *x);
void Dsolv(SolverMatrix* solvmtx, COEF* b);
void bmatvec(SolverMatrix* solvmtx, COEF* x, COEF* y, char* UPLO);

#ifndef TEST
int_t fgmresd_blok(mpi_t proc_id, csptr A, SolverMatrix* L,  PhidalOptions *option, 
		   COEF *rhs, COEF * x, FILE *fp);

void SolverMatrix_TRSM(flag_t unitdiag, SolverMatrix* L, SolverMatrix* M);
void SolverMatrix_Div(SolverMatrix* A, SolverMatrix* D);

/* void SymbolMatrix_Cut(SymbolMatrix* symbA, SymbolMatrix* symbB, */
/* 		      dim_t tli, dim_t tlj, dim_t bri, dim_t brj, /\* alloc_t alloc, *\/ SymbolBCblk* pAtab); */

/* void SymbolMatrix_Cut2(SymbolMatrix* symbA, SymbolMatrix* symbB,/\* todo : verifier que c les meme conventions sur phidalseq *\/ */
/* 		       dim_t tli, dim_t tlj, dim_t bri, dim_t brj, alloc_t alloc,  SymbolBCblk* pAtab); */


void SymbolMatrix_Cut(SymbolMatrix* symbA, SymbolMatrix* symbB,
		      dim_t tli, dim_t tlj, dim_t bri, dim_t brj);

void SymbolMatrix_Cut2(SymbolMatrix* symbA, SymbolMatrix* symbB,/* todo : verifier que c les meme conventions sur phidalseq */
		       dim_t tli, dim_t tlj, dim_t bri, dim_t brj, alloc_t alloc);

void tlj2cblktlj(SymbolMatrix* symbA, dim_t tlj, dim_t brj, dim_t *cblktlj, dim_t *cblkbrj);
void initpAtab(SymbolMatrix* symbA, SymbolBCblk* pAtab);
void setpAtab(SymbolMatrix* symbA, dim_t cblktlj, dim_t cblkbrj, dim_t tli, dim_t bri, SymbolBCblk * pAtab);
int bloknbr(SymbolMatrix* symbA, dim_t cblktlj, dim_t cblkbrj, dim_t tli, dim_t bri, int use_pAtab, SymbolBCblk * pAtab);


void DcpyMatrixToDiag(SolverMatrix* solvmtx, COEF* Diag);
void DcpyDiagToMatrix(SolverMatrix* solvmtx, COEF* Diag);

void SolverMatrix_MatrixBuild(SolverMatrix* M, PhidalMatrix* PhidalM, PhidalHID* BL, int_t levelnum, alloc_t alloc);



void DBMatrix_Setup1(dim_t tli, dim_t tlj, dim_t bri, dim_t brj, char *UPLO, char *DIAG, int_t locally_nbr, DBMatrix* m, PhidalHID *BL);
void DBMatrix_Setup2(DBMatrix* m, SymbolMatrix* symbmtx, PhidalHID *BL, int_t levelnum, int_t levelnum_l);

void SF_Direct(csptr A, dim_t cblknbr, dim_t *rangtab, dim_t *treetab, csptr P);
void amalgamate(REAL rat, int nrow, csptr P, int snodenbr, int *snodetab, dim_t *treetab, 
		int *cblknbr, dim_t **rangtab, dim_t *nodetab);
void HIPS_SymbolMatrix(flag_t verbose, int numflag, REAL rat, int_t locally_nbr, int n,  INTL *ia,  dim_t *ja, 
		       PhidalHID *BL, SymbolMatrix *symbmtx, dim_t *perm, dim_t *iperm);

void symbolicBlok2(MatrixStruct* mtxstr, csptr P, dim_t cblknbr, dim_t *rangtab);

void HID2MatrixStruct(dim_t *rangtab, MatrixStruct* mtxstruct, PhidalHID* BL, int startlevel, int_t locally_nbr);

/* void DBMatrix_Setup_solvermatrix(SolverMatrix* solvmtx); */
void DBMatrix_Setup_solvermatrix(SolverMatrix* solvmtx, SymbolMatrix* symbmtx);
void DBMatrix_Setup_cblktosolvmtx(DBMatrix* m);

void DBMatrix2PhidalMatrix(UDBMatrix* L, PhidalMatrix* Copy, int_t locally_nbr,  PhidalHID *BL, flag_t inarow, flag_t unitdiag);
void DBMatrix_GetDiag(DBMatrix* M, COEF* D);

void DBMatrix2PhidalMatrix_Unsym(UDBMatrix* LU, PhidalMatrix* Copy, int_t locally_nbr,  PhidalHID *BL, flag_t inarow);

void DBPrec_SchurProd(DBPrec *P, PhidalHID *BL, COEF *x, COEF *y);
#endif /* TEST*/

int cblksize(DBMatrix* db);
void cblkbuffer_size(DBMatrix* A, int *W, int *F);

void VS_Lsol(flag_t unitdiag, VSolverMatrix* vsolvmtx, COEF* b, COEF* x);
void VS_LMATVEC(VSolverMatrix* vsolvmtx, COEF* b, COEF* x);
void TR1(flag_t unitdiag, VSolverMatrix* vsolvmtx, COEF* b, COEF* x, COEF* xtmp);
void DBMATVEC21(VSolverMatrix* vsolvmtx, COEF* b, COEF* x, COEF* xtmp);
void VSolverMatrix_CleanTmp(VSolverMatrix* a);
void VSolverMatrix_CleanTmpNODIAG(VSolverMatrix* a);

void VS_ICCT(VSolverMatrix* vsolvmtx, COEF* F, COEF* W);
void VS_InvLT(flag_t unitdiag, VSolverMatrix* L, VSolverMatrix* M, COEF* W);
void VS2_InvLT(flag_t unitdiag, SolverMatrix* L, SymbolMatrix* symbL, SolverMatrix* M, SymbolMatrix* symbM, COEF* W);
void VS_RowDiv(VSolverMatrix* L, VSolverMatrix* M);

void DB_ICCprod(int iB, COEF alpha, 
		DBMatrix* A,
		int nk, dim_t *jak, VSolverMatrix** rak,
		int *tabA, int *tabC, 
		COEF* E, COEF* F, COEF* W);
			
void DBMatrix_GEMMpart(dim_t jj, DBMatrix* C, COEF alpha, DBMatrix* A, /* DBMatrix* B_OLD, */ DBMatrix* D, 
		       /* flag_t fill, */ /* PhidalMatrix* Phidal, PhidalHID *BL,  */
		       COEF* E, COEF* F, COEF* W, int *tabA, int *tabC);

void DBMatrix_GEMMpartu(dim_t jj, DBMatrix* CL, DBMatrix* CU, COEF alpha, DBMatrix* A, DBMatrix* B,
			COEF* E, COEF* F, COEF* W, int *tabA, int *tabC);


int LDLt_piv(int n, blas_t stride, COEF *coefftab, REAL epsilon);
void LU(int n, COEF *coeftabL, blas_t strideL, COEF *coeftabU, blas_t strideU, REAL epsilon);

int calc_Emax(DBMatrix* E);
long DBPrec_NNZ_RL(DBPrecMem *m);
long DBPrec_NNZ_RL_DROPE(DBPrecMem* mem);

void DBMatrix_FACTO_Lu(UDBMatrix* LU);
void VS_ICCTu(VSolverMatrix* vsolvmtxL, VSolverMatrix* vsolvmtxU, COEF* W);

void PhidalMatrix_Init_fromDB(DBMatrix* M, PhidalMatrix* Copy, char* UPLO, int_t locally_nbr, PhidalHID *BL);

INTS HIPS_Fgmresd_DB_PH(flag_t verbose, REAL tol, dim_t itmax, 
			DBPrec *Q, PhidalPrec *P, 
			PhidalHID *BL, PhidalOptions *option, 
			COEF *rhs, COEF * x, FILE *fp, dim_t *itertab, REAL *resnormtab);

INTS HIPS_Fgmresd_PH_DB(flag_t verbose, REAL tol, dim_t itmax, 
			PhidalMatrix *A, DBPrec *P, 
			PhidalHID *BL, PhidalOptions *option, 
			COEF *rhs, COEF *x, FILE *fp, dim_t *itertab, REAL *resnormtab);

INTS HIPS_Fgmresd_DB   (flag_t verbose, REAL tol, dim_t itmax, 
			DBPrec *P, 
			PhidalHID *BL, PhidalOptions *option, 
			COEF *rhs, COEF *x, FILE *fp, dim_t *itertab, REAL *resnormtab);

INTS HIPS_Fgmresd_DB_DB(flag_t verbose, REAL tol, dim_t itmax, 
			DBMatrix *L, DBMatrix *U, DBPrec *P, 
			PhidalHID *BL, PhidalOptions *option, 
			COEF *rhs, COEF *x, FILE *fp, dim_t *itertab, REAL *resnormtab);


INTS HIPS_PCG_DB_PH(flag_t verbose, REAL tol, dim_t itmax, 
		    DBPrec *Q, PhidalPrec *P, 
		    PhidalHID *BL, PhidalOptions *option, 
		    COEF *rhs, COEF * x, FILE *fp, dim_t *itertab, REAL *resnormtab);

INTS HIPS_PCG_PH_DB(flag_t verbose, REAL tol, dim_t itmax, 
		    PhidalMatrix *A, DBPrec *P, 
		    PhidalHID *BL, PhidalOptions *option, 
		    COEF *rhs, COEF *x, FILE *fp, dim_t *itertab, REAL *resnormtab);

INTS HIPS_PCG_DB   (flag_t verbose, REAL tol, dim_t itmax, 
		    DBPrec *P, 
		    PhidalHID *BL, PhidalOptions *option, 
		    COEF *rhs, COEF *x, FILE *fp, dim_t *itertab, REAL *resnormtab);

INTS HIPS_PCG_DB_DB(flag_t verbose, REAL tol, dim_t itmax, 
		    DBMatrix *L, DBMatrix *U, DBPrec *P, 
		    PhidalHID *BL, PhidalOptions *option, 
		    COEF *rhs, COEF *x, FILE *fp, dim_t *itertab, REAL *resnormtab);

void DBMatrix_Copy3(UDBMatrix* L, UDBMatrix* Lcopy, int_t levelnum, int_t locally_nbr, PhidalHID* BL);
void DBMatrix_Copy3_unsym(UDBMatrix* LU, UDBMatrix* LUcopy, int_t levelnum, int_t locally_nbr, PhidalHID* BL);


void DBPrec_Unscale(REAL *scaletab, REAL *iscaletab, DBPrec *P, PhidalHID *BL);
void DBPrec_Unscale_Unsym(REAL *scalerowtab, REAL *iscalerowtab, REAL *scalecoltab, REAL *iscalecoltab, DBPrec *P, PhidalHID *BL);
void DBMatrix_ColMult(REAL *b, DBMatrix *D, PhidalHID *BL);
void DBMatrix_RowMult(REAL *b, DBMatrix *D, PhidalHID *BL);
void DBMatrix_DiagMult(REAL *b, DBMatrix *D, PhidalHID *BL);

void fix(SymbolMatrix* symbmtx, alloc_t alloc);
void HDIM_tmp_avant(DBMatrix* M);
void HDIM_tmp_apres(DBMatrix* M);
void PhidalMatrix_InitScale(PhidalMatrix* A, REAL* scaletab, REAL* iscaletab, PhidalHID* BL);
void PhidalMatrix_InitScale_Unsym(PhidalMatrix* A, 
				  REAL* scalerow, REAL* iscalerow, 
				  REAL* scalecol, REAL* iscalecol, PhidalOptions* option, PhidalHID* BL);
void PhidalMatrix_Scale(PhidalMatrix* A, REAL* scaletab, PhidalHID* BL);
void PhidalMatrix_Scale_Unsym(PhidalMatrix* A, REAL* scalerow, REAL* scalecol, PhidalHID* BL);
void DBMatrix_SetDiag(DBMatrix* D, COEF* b);
void VSolverMatrix2SparRow(SolverMatrix* solvmtx, SymbolMatrix* symbmtx, csptr cs, flag_t inarow, flag_t diag, flag_t unitdiag);
void VSolverMatrix2SparRowDrop(SolverMatrix* solvmtx, SymbolMatrix* symbmtx, csptr cs, flag_t inarow, flag_t diag, flag_t unitdiag, REAL droptol);
void VSolverMatrix_Clean(VSolverMatrix* vs);

COEF my_zdotc(int n, COEF *x, blas_t incx, COEF *y, blas_t incy);

#include "macro.h"

void DBPrecMem_Init(DBPrecMem* mem); 
void DBPrecMem_SetA(PhidalMatrix* m, DBPrecMem *mem);
void CMD(_DBPrecMem,Set0)(_DBDistrPrec *P, DBPrecMem *mem, flag_t dropE);
void CMD(_DBPrecMem,SetPhidalS)(_DBDistrPrec *P, DBPrecMem *mem);
void CMD(_DBPrecMem,SetDropE)(_PhidalDistrPrec *P, DBPrecMem *mem);
void CMD(_DBPrecMem,SetDropEAfter)(_PhidalDistrPrec *P, DBPrecMem *mem);
void CMD(_DBPrecMem,Set1)(_DBDistrPrec *P, DBPrecMem *mem);
void CMD(_DBPrecMem,print)(_DBDistrPrec *P, DBPrecMem *mem, _PhidalDistrHID* DBL, int verbose);

long CMD(_DBDistrPrec,NNZ)(_DBDistrPrec *p);

#define PrecInfo_AddNNZ_(arg1,arg2,msg) {\
    PrecInfo_AddNNZ((arg1), (arg2));\
    /*   printf("AddNNZ : %s (%ld -> %ld)\n", msg, (long)arg2, (long)arg1->nnzP); */ \
  }

#define PrecInfo_SubNNZ_(arg1,arg2,msg) {\
    PrecInfo_SubNNZ((arg1), (arg2));\
    /*   printf("SubNNZ : %s (-%ld -> %ld)\n", msg, (long)arg2, (long)arg1->nnzP);  */ \
  }
  /* #define OLDCOUNT */


#if defined(SYMMETRIC)
#define SYMVAL 1

#define DUP(func, a, b) func(a)

#elif defined(UNSYMMETRIC)
#define SYMVAL 0

#define DUP(func, a, b) { func(a); func(b); }

#endif /*OK*/


#define _TO_STRING(arg) #arg
#define TO_STRING(arg) _TO_STRING(arg)

#ifndef PARALLEL

#if defined(SYMMETRIC)
#define _DBMATRIX_MLILUPrec     DBMATRIX_MLICCPrec
#elif defined(UNSYMMETRIC)
#define _DBMATRIX_MLILUPrec     DBMATRIX_MLILUPrec
#endif

#else /* PARALLEL */

#if defined(SYMMETRIC)
#define _DBMATRIX_MLILUPrec     DBDISTRMATRIX_MLICCPrec
#elif defined(UNSYMMETRIC)
#define _DBMATRIX_MLILUPrec     DBDISTRMATRIX_MLILUPrec
#endif

#endif /* PARALLEL */


#if defined(SYMMETRIC)

#define IFUNSYM(args...) 
#define IFSYM(args...) args

#elif defined(UNSYMMETRIC)

#define IFUNSYM(args...) args
#define IFSYM(args...)

#define _DBPrec_Unscale           DBPrec_Unscale_Unsym

#endif /* SYMMETRIC */



#ifndef PARALLEL
#if defined(SYMMETRIC)
#define _DBMatrix2PhidalMatrix DBMatrix2PhidalMatrix
#else
#define _DBMatrix2PhidalMatrix DBMatrix2PhidalMatrix_Unsym
#endif

#else /* PARALLEL */

#if defined(SYMMETRIC)
#define _DBMatrix2PhidalMatrix DBDistrMatrix2PhidalDistrMatrix
#else
#define _DBMatrix2PhidalMatrix DBDistrMatrix2PhidalDistrMatrix_Unsym
#endif

#endif /* PARALLEL */


#if defined(SYMMETRIC)
#define SYMLET L
#define SYMMET CC

#define _DBMatrix_Copy3 DBMatrix_Copy3

#define _PhidalPrec_MLILUT      PhidalPrec_MLICCT

#elif defined(UNSYMMETRIC)

#define SYMLET N
#define SYMMET LU

#define _DBMatrix_Copy3           DBMatrix_Copy3_unsym


#ifndef PARALLEL
#define _PhidalPrec_MLILUT      PhidalPrec_MLILUT
#endif /* PARALLEL */


#endif /* SYMMETRIC */

void CMDu(_DBDistrMatrix,FACTOFACTO)(_UDBDistrMatrix* LU, PhidalOptions* option, _PhidalDistrHID* DBL);

void _CMD(_DBDistrPrec,FACTO_TRSM_GEMM_RL)(_DBDistrPrec* P, flag_t fillE, PhidalHID* BL, PhidalOptions *option/*verbose*/);

void PHIDAL_WriteSymbolMatrix(FILE *fp, SymbolMatrix *symbmtx);      void PHIDAL_LoadSymbolMatrix(FILE *fp, SymbolMatrix* symbmtx);

void HIPS_CountNNZ_L(flag_t job, SymbolMatrix *symbmtx, PhidalHID *BL);void HIPS_CountNNZ_E(dim_t n,  INTL *ia,  dim_t *ja, dim_t *perm, PhidalHID *BL);

long CMD(_DBDistrPrec,NNZ)(_DBDistrPrec *p);

long DBPrec_NNZ_RL_DROPEAfter(DBPrecMem* mem);


void CMDu(_DBDistrPrec,FACTO_TRSM_GEMM_LL)(_DBDistrPrec* P, _UPhidalDistrMatrix* PhidalS, flag_t fillS, PhidalHID* BL, chrono_t* ttotal);

void CMDu(_DBDistrPrec,FACTO_TRSM_GEMM_RL)(_DBDistrPrec* P, flag_t fillE, _PhidalDistrHID* BL, PhidalOptions *option/*verbose*/);

void CMDu(_DBDistrPrec,FACTO_TRSM_GEMM_RL_DROPE)(_DBDistrPrec* P, _PhidalDistrPrec* phidalPrecL0, flag_t fillE, PhidalOptions* option, _PhidalDistrHID* _DBL, chrono_t* ttotal, DBPrecMem* mem);

void _CMD(_DBDistrMatrix,FACTO)(_DBDistrMatrix* L, _DBDistrMatrix* U);

void CMDu(DBMatrix,GEMM)(UDBMatrix* C, COEF alpha, DBMatrix* A, DBMatrix* B, DBMatrix* D,       flag_t filling, UPhidalMatrix* phidal, PhidalHID *BL);

void CMDu(DBPrec,FACTO_TRSM_GEMM)(DBPrec* L, flag_t fill, PhidalHID* BL);      

void CMDu(DBPrec,FACTO_TRSM_DROP)(DBPrec* P, PhidalPrec* phidalPrecL0,  PhidalOptions* option, REAL *droptab,   flag_t fill, PhidalHID* BL);      

void CMD(_UDBDistrMatrix,Build)(PhidalMatrix* mat, 
				dim_t tli, dim_t tlj, dim_t bri, dim_t brj, 
				char *UPLO_L, char *UPLO_U, int_t locally_nbr, 
				PhidalMatrix* PhidalL,  PhidalMatrix* PhidalU, 
				_UDBDistrMatrix* DBM, 
				SymbolMatrix* symbmtx, _PhidalDistrHID *DBL,
				int_t levelnum, int_t levelnum_l,
				alloc_t alloc, flag_t filling);


void SymbolMatrix_Expand(SymbolMatrix* symbol, dim_t dof);

void DBMatrix_ICCT(DBMatrix *L, PhidalHID *BL, PhidalOptions *option);

struct CellDB
{
  dim_t nnz;
  dim_t *ja;
  VSolverMatrix **ma;
};

typedef struct CellDB CellDB;


void CellDB_ListUpdate(CellDB *celltab, int num,  int n,  dim_t *ja);
			       void CellDB_ListCopy(CellDB *src, CellDB *dest,  int n,  dim_t *ja);
void CellDB_GetCSList(int num, CellDB *celltab, int n,  dim_t *ja, VSolverMatrix* *ma, int *nnb, VSolverMatrix* *csrtab1, VSolverMatrix* *csrtab2);

void CellDB_IntersectList(CellDB *celltab, int ncol,  int *colja, VSolverMatrix* *colma, int nrow, int *rowja, 
			  int *nnztab, VSolverMatrix* *listA, VSolverMatrix* *listB);

#endif

/* @authors J. GAIDAMOUR, P. HENON */

#ifndef DB_PARALLEL_STRUCT_H
#define DB_PARALLEL_STRUCT_H

#include "hips_define.h"
#include "phidal_parallel_struct.h"

typedef struct PhidalCommVec_  DBMatrixCommVec;

struct DBBlockComm_{
  flag_t posted;  /** flag **/
  mpi_t rqnbr;  /** Number of requests **/
  mpi_t *proctab; /** list of the processor **/
  MPI_Request *irqtab; /** Array of incoming  message requests for
			      coeff. indices**/
  MPI_Request *crqtab; /** Array of incoming message requests
				for coeff. values**/
/*   MPI_Status  *status;       /\** Status array for receive requests **\/ */


  mpi_t buffsize;     /** Size of a receive buffer **/
  /*   mpi_t **ibufftab;       /\* Buffers for receive (matrice indices) *\/ */
  COEF **cbufftab;       /* Buffers for receive (matrice coefficients) */
};
typedef struct DBBlockComm_ DBBlockComm;


/** Define the structure needed to communication during the factorization **/
struct DBMatrixFactComm_{

  int_t bcomnbr;
  DBBlockComm *bcomtab;
  mpi_t proc_id;
  mpi_t maxproc; /** maximum number of processor that can share a same
		     block **/

  /** These are temporary vector that are needed to add the
      contribution received for a block **/
/*   dim_t *tmpj; */
/*   COEF *tmpa; */
/*   INt *jrev; */
  VSolverMatrix* cstab;
  MPI_Status *status;
  MPI_Comm mpicom; /* MPI communicator */
  
  /*-------------------------------------------------/
  / PARAMETERS FOR COMMUNICATION                     /
  /   BUFFER MANAGEMENT                              /
  /-------------------------------------------------*/
  REAL comm_memratio;
  int_t posted_max; /** maximum number of messages that can simultaneously been posted  **/
  int_t posted;     /** Number of messages currently posted **/
  INTL bufmem_max; /** Maximum memory allowed for MPI Irecv buffer **/
  INTL bufmem; /** Memory currently allowed for MPI Irecv buffer **/
/*   dim_t colind; */
/*   dim_t rowind; */

};

typedef struct DBMatrixFactComm_ DBMatrixFactComm;




struct CellDBDistr
{
  dim_t nnz;
  dim_t *ja;
  INTL *pind;
  VSolverMatrix **ma;
};

typedef struct CellDBDistr CellDBDistr;

void DBBlockComm_Init(DBBlockComm *b, int srcnbr);
void DBBlockComm_Clean(DBBlockComm *b);
void DBMatrixCommVec_Init(DBMatrixCommVec *commvec);
void DBMatrixCommVec_Clean(DBMatrixCommVec *commvec);
void DBMatrixFactComm_Init(DBMatrixFactComm *FC);
void DBMatrixFactComm_Clean(DBMatrixFactComm *FC);


struct DBDistrMatrix_{
  mpi_t proc_id;
  mpi_t nproc;

  mpi_t *clead; /** Leader of the block **/
  INTL   *cind;  /** Index to find the processor set of a block **/
              /** -1 means the block is only map on this processor **/
  mpi_t *rlead; /** Leader of the block **/
  INTL   *rind;  /** Index to find the processor set of a block **/

  /* Describe the processor set of the blocks */
  INTL *pset_index;
  mpi_t *pset;

  int_t sharenbr; /** Number of block matrices shares by several
		    processors **/
  
  mpi_t maxproc; /** Maximum number of processors sharing a block in the
		     matrix **/
  DBMatrix M;
  COEF*    extDiag; /* ! */
  DBMatrixCommVec commvec;     /* Communicator for matrix-vector or
				  triangular solve */
};
/* typedef struct DBDistrMatrix_  DBDistrMatrix; */

void DBDistrMatrix_Init(DBDistrMatrix *a);
void DBDistrMatrix_Clean(DBDistrMatrix *a);

/* typedef struct DBDistrPrec_  DBDistrPrec; */
/* struct DBDistrPrec_ { */
/*   flag_t symmetric;  */
/*   dim_t dim;           /\** The dimension of the system preconditonned **\/ */
/*   int_t levelnum; */
/*   int_t forwardlev; */
/*   UDBDistrMatrix LU; */

/*   UDBDistrMatrix EF_DB; */
/* /\*   DBDistrMatrix *E_DB; *\/ */
/* /\*   DBDistrMatrix *F_DB; *\/ */

/*   PhidalDistrMatrix *B; */
/*   PhidalDistrMatrix *E; */
/*   PhidalDistrMatrix *F; */

/* /\*   Phidal/\\* Distr *\\/Matrix *B; *\/ */
/* /\*   Phidal/\\* Distr *\\/Matrix *E; *\/ */
/* /\*   Phidal/\\* Distr *\\/Matrix *F; *\/ */

/*   UDBDistrMatrix S; */

/*   DBDistrPrec *prevprec; */
/*   DBDistrPrec *nextprec; */
/*   flag_t schur_method;  /\** Indicate the smoothing method in the multi-level; */
/* 			  0 := no smoothing; in this case the schur complement S is not stored */
/* 			  1 := iteration in the schur complement with storage of the schur complement */
/* 			  2 := iteration in the schur complement with implicit schur complement (schur not stored) *\/ */

/*   PhidalDistrMatrix *phidalS; */
/*   PhidalDistrPrec   *phidalPrec; */

/*   PrecInfo* info; */
/* }; */

#include "prec.h"
typedef struct DistrPrec_ DBDistrPrec;


#endif

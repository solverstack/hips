#ifndef PASTIX_CALLS_H
#define PASTIX_CALLS_H

#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#include <string.h>

#include <assert.h>
#include "solver.h"

#include "phidal_common.h"
#include "base.h"

//#include "mpi.h"
#include "pastix.h"

#ifndef SCOTCH_FIRST_INCLUDE
#define SCOTCH_FIRST_INCLUDE
#include "scotch.h"
#endif

#define WITH_SCOTCH

typedef struct PastixOrder_ {
  PASTIX_INT                       cblknbr;              /*+ Number of column blocks             +*/
  PASTIX_INT *                     rangtab;              /*+ Column block range array [based,+1] +*/
  PASTIX_INT *                     permtab;              /*+ Permutation array [based]           +*/
  PASTIX_INT *                     peritab;              /*+ Inverse permutation array [based]   +*/
} PastixOrder;

typedef struct PastixSopalinParam_ {
  double   epsilonraff;         /*+ epsilon to stop reffinement                      +*/
  double   rberror;             /*+ ||r||/||b||                                      +*/
  double   espilondiag;         /*+ epsilon critere for diag control                 +*/
  PASTIX_FLOAT   *b;                   /*+ b vector (RHS and solution)                      +*/
  PASTIX_FLOAT   *transcsc;            /*+ transpose csc                                    +*/
  PASTIX_INT      itermax;             /*+ max number of iteration                          +*/
  PASTIX_INT      diagchange;          /*+ number of change of diag                         +*/
  PASTIX_INT      gmresim;	        /*+ Krylov subspace size for GMRES                   +*/
  PASTIX_INT      fakefact;	        /*+ Flag indicating if we want fake factorisation    +*/  
  PASTIX_INT      usenocsc;	        /*+ Flag indicating if we want to use the intern CSC +*/
  int      factotype;           /*+ Type of factorization                            +*/
  int      symmetric;           /*+ Symmetric                                        +*/
  MPI_Comm pastix_comm;         /*+ MPI communicator                                 +*/
  int      type_comm;           /*+ Communication mode                               +*/
  int      nbthrdcomm;          /*+ Communication's thread number                    +*/
  PASTIX_INT     *iparm;               /*+ In/Out integer parameters                        +*/
  double  *dparm;               /*+ In/Out float parameters                          +*/
  int     *bindtab;             /*+ Define where to bin threads                      +*/
#ifdef THREAD_COMM
  int      stopthrd;
#endif
  int      schur;               /*+ If API_YES won't compute last diag               +*/
  PASTIX_INT      n;                   /*+ size of the matrix                               +*/
  PASTIX_INT      gN;
} PastixSopalinParam;

typedef struct PastixSymbolCblk_ {
  PASTIX_INT                       fcolnum;              /*+ First column index               +*/
  PASTIX_INT                       lcolnum;              /*+ Last column index (inclusive)    +*/
  PASTIX_INT                       bloknum;              /*+ First block in column (diagonal) +*/
} PastixSymbolCblk;

typedef struct PastixSymbolBlok_ {
  PASTIX_INT                       frownum;              /*+ First row index            +*/
  PASTIX_INT                       lrownum;              /*+ Last row index (inclusive) +*/
  PASTIX_INT                       cblknum;              /*+ Facing column block        +*/
  PASTIX_INT                       levfval;              /*+ Level-of-fill value        +*/
} PastixSymbolBlok;

typedef struct PastixSymbolMatrix_ {
  PASTIX_INT                       baseval;              /*+ Base value for numberings         +*/
  PASTIX_INT                       cblknbr;              /*+ Number of column blocks           +*/
  PASTIX_INT                       bloknbr;              /*+ Number of blocks                  +*/
  PastixSymbolCblk * /*restrict*/     cblktab;              /*+ Array of column blocks [+1,based] +*/
  PastixSymbolBlok * /*restrict*/     bloktab;              /*+ Array of blocks [based]           +*/
  PASTIX_INT                       nodenbr;              /*+ Number of nodes in matrix         +*/
} PastixSymbolMatrix;

typedef struct PastixCscFormat_ {
  PASTIX_INT   colnbr;
  PASTIX_INT * coltab;
} PastixCscFormat;

typedef struct PastixCscMatrix_ {
  PASTIX_INT         cscfnbr;
  PastixCscFormat * cscftab;
  PASTIX_INT       * rowtab;
  PASTIX_FLOAT     * valtab;
} PastixCscMatrix;

typedef struct PastixSolverCblk_  {
  PASTIX_INT                       stride;               /*+ Column block stride                    +*/
  PASTIX_INT			    color;		  /*+ Color of column block (PICL trace)     +*/
  PASTIX_INT                       procdiag;             /*+ Processor owner of diagonal block      +*/
  PASTIX_INT                       cblkdiag;             /*+ Column block owner of diagonal block   +*/
} PastixSolverCblk; 


typedef struct PastixSolverBlok_ {
  PASTIX_INT                       coefind;              /*+ Index in coeftab +*/
} PastixSolverBlok;

typedef struct PastixSolverMatrix_ {
  PastixSymbolMatrix              symbmtx;              /*+ Symbolic matrix                           +*/
  PastixCscMatrix		    cscmtx;		  /*+ Compress Sparse Column matrix             +*/
  PastixSolverCblk * /*restrict*/     cblktab;              /*+ Array of solver column blocks             +*/
  PastixSolverBlok * /*restrict*/     bloktab;              /*+ Array of solver blocks                    +*/
  PASTIX_INT                       coefnbr;              /*+ Number of coefficients                    +*/
  PASTIX_INT                       ftgtnbr;              /*+ Number of fanintargets                    +*/
  PASTIX_INT                       ftgtcnt;              /*+ Number of fanintargets to receive         +*/
#ifdef PASTIX_NUMA_ALLOC
  PASTIX_FLOAT ** /*restrict*/         coeftab;              /*+ Coefficients access vector                +*/
  PASTIX_FLOAT ** /*restrict*/         ucoeftab;             /*+ Coefficients access vector                +*/
#else
  PASTIX_FLOAT *  /*restrict*/         coeftab;              /*+ Coefficients access vector                +*/
  PASTIX_FLOAT *  /*restrict*/         ucoeftab;             /*+ Coefficients access vector                +*/
#endif
  PASTIX_INT otherfields[70];
} PastixSolverMatrix;



typedef struct my_pastix_data_t {
  PastixSolverMatrix     solvmatr; 
  PastixSopalinParam     sopar;            /*+ Sopalin parameters                                                  +*/
  PastixOrder            ordemesh;         /*+ Order                                                               +*/
  SCOTCH_Graph     grafmesh;         /*+ Graph                                                               +*/
  int              malgrf;           /*+ boolean indicating if grafmesh has been allocated                   +*/
  PASTIX_INT              gN;               /*+ global column number                                                +*/
  PASTIX_INT              n;                /*+ local column number                                                 +*/
  int              procnbr;          /*+ Number of MPI tasks                                                 +*/
  int              procnum;          /*+ Local MPI rank                                                      +*/
  PASTIX_INT             *iparm;            /*+ Vecteur de parametres entiers                                       +*/
  double          *dparm;            /*+ Vecteur de parametres floattant                                     +*/
  PASTIX_INT              n2;               /*+ Number of local columns                                             +*/
  PASTIX_INT             *col2;             /*+ column tabular for the CSC matrix                                   +*/
				     /*+ (index of first element of each col in row and values tabulars)     +*/
  PASTIX_INT             *row2;             /*+ tabular containing row number of each element of                    +*/
				     /*+  the CSC matrix, ordered by column.                                 +*/
  int              bmalcolrow;       /*+ boolean indicating if col2 ans row2 have been allocated             +*/
  int              malord;           /*+ boolean indicating if ordemesh has been allocated                   +*/
  int              malcsc;           /*+ boolean indicating if solvmatr->cscmtx has beek allocated           +*/
  int              malsmx;           /*+ boolean indicating if solvmatr->updovct.sm2xtab has been allocated  +*/
  int              malslv;           /*+ boolean indicating if solvmatr has been allocated                   +*/
  int              malcof;           /*+ boolean indicating if coeficients tabular(s) has(ve) been allocated +*/
  MPI_Comm         pastix_comm;      /*+ PaStiX MPI communicator                                             +*/
  int             *bindtab;          /*+ Tabular giving for each thread a CPU to bind it too                 +*/
  PASTIX_INT              nschur;           /*+ Number of entries for the Schur complement.                         +*/
  PASTIX_INT             *listschur;        /*+ List of entries for the schur complement.                           +*/
  PASTIX_FLOAT           *schur_tab;
  PASTIX_INT              schur_tab_set;
  int              cscInternFilled;
  int              scaling;          /*+ Indicates if the matrix has been scaled                             +*/
  PASTIX_FLOAT           *scalerowtab;      /*+ Describes how the matrix has been scaled                            +*/
  PASTIX_FLOAT           *iscalerowtab;
  PASTIX_FLOAT           *scalecoltab;
  PASTIX_FLOAT           *iscalecoltab;
} my_pastix_data_t;

typedef struct pastix_struct {
  pastix_int_t iparm[IPARM_SIZE];
  double dparm[DPARM_SIZE];
  my_pastix_data_t *pastix_data;
} pastix_struct;


void pastix_init(pastix_struct *p, SymbolMatrix *symbmtx);
void pastix_free(pastix_struct *p);
void pastix_factorization(pastix_struct *p, SolverMatrix *solvL, SolverMatrix *solvU, SymbolMatrix *symbmtx, int sym);
void pastix_solve(pastix_struct *p, COEF *b);
void pastix_TRSM(pastix_struct *p, flag_t unitdiag, SolverMatrix* M, SymbolMatrix* symbM);

#ifdef MPI_Comm
#undef MPI_Comm
#endif

#endif /* PASTIX_CALLS_H */

/* @authors J. GAIDAMOUR */

#ifndef DB_PARALLEL_H
#define DB_PARALLEL_H
#include <mpi.h>

#include "hips_define.h"

#include "phidal_ordering.h"
#include "phidal_sequential.h"
#include "db_struct.h"
#include "db_parallel_struct.h"

void DBDISTRMATRIX_Precond(PhidalDistrMatrix *A, DBDistrPrec *P, SymbolMatrix* symbmtx, PhidalDistrHID *DBL, PhidalOptions *option);

void DBDistrMatrix_Setup(dim_t tli, dim_t tlj, dim_t bri, dim_t brj, char *UPLO, char *DIAG, 
			 int_t locally_nbr, DBDistrMatrix* DM, 
			 SymbolMatrix* symbmtx, PhidalDistrHID *BL, 
			 int_t levelnum, int_t levelnum_l, alloc_t alloc);

void DBDistrMatrix_SetupV(DBDistrMatrix *DM, DBDistrMatrix *DCpy);

void DBDistrMatrix_Copy(PhidalMatrix* mat, PhidalDistrHID* DBL, 
		   PhidalMatrix* Phidal, DBDistrMatrix* DBM,
		   char* UPLO, int_t levelnum, int_t levelnum_l, flag_t fill);

void DBDistrMatrix_CleanNonLocalBLock(DBDistrMatrix *M);
int DBDistrMatrix_Check(DBDistrMatrix *M, PhidalDistrHID *DBL);
void DBDistrMatrix_Transpose(DBDistrMatrix *DM);
void DBDistrMatrix_VirtualCpy(DBDistrMatrix *M, DBDistrMatrix *Cpy);

/* void PhidalFactComm_Setup(PhidalFactComm *FC, DBDistrMatrix *M, PhidalDistrHID *DBL); */
/* void PhidalFactComm_Clean(PhidalFactComm *FC); */

void DBsend_matrix(VSolverMatrix* mat, int indnum, mpi_t tag, DBMatrixFactComm *FC);
flag_t DBreceive_contrib(flag_t mode, VSolverMatrix* mat, int indnum, DBMatrixFactComm *FC);
flag_t DBreceive_gather(flag_t mode, VSolverMatrix* mat, int indnum, DBMatrixFactComm *FC);
flag_t DBreceive_matrix(flag_t mode, VSolverMatrix* mat, int indnum, DBMatrixFactComm *FC);
void DBposte_block_receive(int indnum, mpi_t tag, DBMatrixFactComm *FC);
/* void PhidalFactComm_PosteCtrbReceive(PhidalFactComm *FC, DBDistrMatrix *M, dim_t tli, dim_t tlj, dim_t bri, dim_t brj); */

void CellDBDistr_ListUpdate(CellDBDistr *celltab, int num,  int n,  dim_t *ja);
void CellDBDistr_ListCopy(CellDBDistr *src, CellDBDistr *dest,  int n,  dim_t *ja);
void CellDBDistr_GetVSList(int num, CellDBDistr *celltab, int n,  dim_t *ja, VSolverMatrix* *ma, int *nnb, VSolverMatrix* *csrtab1, VSolverMatrix* *csrtab2);
void CellDBDistr_IntersectList(mpi_t proc_id, CellDBDistr *celltab, INTL *pset_index, mpi_t *pset,
			       int ncol,  int *colja, int *colpind, VSolverMatrix* *colma,
			       int nrow, int *rowja,
			       int *nnztab, VSolverMatrix* *listA, VSolverMatrix* *listB);

void DBDISTRMATRIX_MLICCPrec(PhidalDistrMatrix *m, DBDistrPrec *P, SymbolMatrix* symbmtx, 
			     PhidalDistrHID *DBL, PhidalOptions *option);



void DBDistrMatrix_GEMM(DBDistrMatrix* C, COEF alpha, DBDistrMatrix* A, DBDistrMatrix* B, DBDistrMatrix* D, 
			flag_t fill, PhidalMatrix* Phidal, PhidalDistrHID *BL);


void DBDistrMatrix_Setup_HID(dim_t tli, dim_t tlj, dim_t bri, dim_t brj, char *UPLO, char *DIAG, 
			int_t locally_nbr, DBDistrMatrix* m, PhidalDistrHID *BL);

void DBDistrMatrix_Setup_SYMBOL(DBDistrMatrix* m, SymbolMatrix* symbmtx, 
			   PhidalDistrHID *BL, int_t levelnum, int_t levelnum_l);

void DBDistrMatrix_Init(DBDistrMatrix *a);
void DBDistrMatrix_Clean(DBDistrMatrix* a);
void DBDistrPrec_Init(DBDistrPrec *p);
void DBDistrPrec_Print(DBDistrPrec *p);
void DBDistrPrec_Clean(DBDistrPrec *p);

void DBDistrMatrix_Copy2(DBDistrMatrix* M, DBDistrMatrix* Copy);


void DBMatrixCommVec_Setup(flag_t job, DBDistrMatrix *DM, PhidalDistrHID *DBL);





void DBMatrixFactComm_Setup(DBMatrixFactComm *FC, DBDistrMatrix *M, PhidalDistrHID *DBL);
void DBMatrixFactComm_PosteCtrbReceive(DBMatrixFactComm *FC, DBDistrMatrix *M, dim_t tlj, dim_t brj);


void DBDistrMatrix2PhidalDistrMatrix(UDBDistrMatrix* L, PhidalDistrMatrix* DM, 
                                     int_t locally_nbr,  
                                     PhidalDistrHID *DBL, flag_t inarow, flag_t unitdiag);

void DBDistrMatrix2PhidalDistrMatrix_Unsym(UDBDistrMatrix* LU, PhidalDistrMatrix* Copy,
                                      int_t locally_nbr,  PhidalDistrHID *BL, flag_t inarow);



void DBDistrMatrix_Lsolv(flag_t unitdiag, DBDistrMatrix *L, COEF *x, COEF *b, PhidalDistrHID *BL);
void DBDistrMatrix_Usolv(flag_t unitdiag, DBDistrMatrix *U, COEF *x, COEF *b, PhidalDistrHID *BL);
void DBDistrMatrix_Dsolv(DBDistrMatrix *D, COEF* b);

void DBDistrMatrix_ICCT(flag_t job, DBDistrMatrix *L, PhidalDistrHID *DBL, PhidalOptions *option);

void DBDistrPrec_SchurProd(DBDistrPrec *P, PhidalDistrHID *BL, COEF *x, COEF *y);

INTS DBDISTRMATRIX_PrecSolve(REAL tol, DBDistrPrec *P, COEF *x, COEF *b, PhidalDistrHID *DBL, PhidalOptions *option, dim_t *itertab, REAL *resnormtab);


INTS HIPS_DistrFgmresd_DB_PH(flag_t verbose, REAL tol, dim_t itmax, 
			    DBDistrPrec *Q, PhidalDistrPrec *P, 
			    PhidalDistrHID *DBL, PhidalOptions *option, 
			    COEF *rhs, COEF * x, FILE *fp, dim_t *itertab, REAL *resnormtab);

INTS HIPS_DistrFgmresd_PH_DB(flag_t verbose, REAL tol, dim_t itmax, 
			    PhidalDistrMatrix *DA, DBDistrPrec *P, 
			    PhidalDistrHID *DBL, PhidalOptions *option, 
			    COEF *rhs, COEF * x, FILE *fp, dim_t *itertab, REAL *resnormtab);

INTS HIPS_DistrFgmresd_DB (flag_t verbose, REAL tol, dim_t itmax, 
			  DBDistrPrec *P, 
			  PhidalDistrHID *DBL, PhidalOptions *option, 
			  COEF *rhs, COEF *x, FILE *fp, dim_t *itertab, REAL *resnormtab);

INTS HIPS_DistrFgmresd_DB_DB(flag_t verbose, REAL tol, dim_t itmax, 
			    DBDistrMatrix *L, DBDistrMatrix *U, DBDistrPrec *P, 
			    PhidalDistrHID *DBL, PhidalOptions *option, 
			    COEF *rhs, COEF *x, FILE *fp, dim_t *itertab, REAL *resnormtab);

INTS HIPS_DistrPCG_DB_PH(flag_t verbose, REAL tol, dim_t itmax, 
			DBDistrPrec *Q, PhidalDistrPrec *P, 
			PhidalDistrHID *DBL, PhidalOptions *option, 
			COEF *rhs, COEF * x, FILE *fp, dim_t *itertab, REAL *resnormtab);

INTS HIPS_DistrPCG_PH_DB(flag_t verbose, REAL tol, dim_t itmax, 
			PhidalDistrMatrix *DA, DBDistrPrec *P, 
			PhidalDistrHID *DBL, PhidalOptions *option, 
			COEF *rhs, COEF * x, FILE *fp, dim_t *itertab, REAL *resnormtab);

void DBDistrMatrix_ColMult(REAL *vec, DBDistrMatrix *A, PhidalDistrHID *DBL);
void DBDistrMatrix_RowMult(REAL *vec, DBDistrMatrix *A, PhidalDistrHID *DBL);
void DBDistrMatrix_DiagMult(REAL *b, DBDistrMatrix *D, PhidalDistrHID *DBL);


void PhidalDistrMatrix_InitScale(PhidalDistrMatrix* DA, REAL* scaletab, REAL* iscaletab, PhidalDistrHID* DBL);
void PhidalDistrMatrix_Scale(PhidalDistrMatrix* DA, REAL* scaletab, PhidalDistrHID* DBL);
void DBDistrPrec_Unscale(REAL *scaletab, REAL *iscaletab, DBDistrPrec *P, PhidalDistrHID *DBL);


void PhidalDistrMatrix_InitScale_Unsym(PhidalDistrMatrix* DA, 
				       REAL* scalerow, REAL* iscalerow, 
				       REAL* scalecol, REAL* iscalecol, PhidalOptions* option, PhidalDistrHID* DBL);
void PhidalDistrMatrix_Scale_Unsym(PhidalDistrMatrix* DA, REAL* scalerow, REAL* scalecol, PhidalDistrHID* DBL);
void DBDistrPrec_Unscale_Unsym(REAL *scalerowtab, REAL *iscalerowtab, REAL *scalecoltab, REAL *iscalecoltab, DBDistrPrec *P, PhidalDistrHID *BL);

void PhidalDistrMatrix_Init_fromDBDistr(DBDistrMatrix* Orig, PhidalDistrMatrix* DM, 
					char* UPLO, int_t locally_nbr, PhidalDistrHID *DBL);


void DBDistrPrecMem_reduce(DBDistrPrec* P, DBPrecMem *mem, DBPrecMem *memsum, MPI_Comm comm);

void DBMatrixCommVec_ReceiveVecAdd(int num, COEF *y, DBMatrixCommVec *commvec);
void DBDISTRMATRIX_MatVec2(DBDistrMatrix *L, DBDistrMatrix *U, PhidalDistrHID *DBL, COEF *x, COEF *y);

void DBDISTRMATRIX_MLICCPrec(PhidalDistrMatrix *A, DBDistrPrec *P, SymbolMatrix* symbmtx, PhidalDistrHID *BL, PhidalOptions *option);
void DBDISTRMATRIX_MLILUPrec(PhidalDistrMatrix *A, DBDistrPrec *P, SymbolMatrix* symbmtx, PhidalDistrHID *BL, PhidalOptions *option);
INTS DBMATRIX_DistrSolve(PhidalDistrMatrix *A, DBDistrPrec *P, PhidalDistrHID *DBL, PhidalOptions *option, COEF *rhs, COEF * x, dim_t *itertab, REAL *resnormtab);

long DBDistrPrec_NNZ_All(DBDistrPrec *P, MPI_Comm mpicom);

#endif

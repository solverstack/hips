/* @authors P. HENON */


#ifndef QUEUE_H
#define QUEUE_H


#include "hips_define.h"
/*#ifndef INT
#define INTL int
#endif */
#ifndef FLOAT
#define FLOAT float
#endif
/*
**  The type and structure definitions.
*/

typedef struct Queue_ {
  dim_t        size;                  /*+ Allocated memory size             +*/ 
  dim_t        used;                  /*+ Number of element in the queue    +*/
  dim_t    *   elttab;                /*+ Array of the element              +*/
  FLOAT  *   keytab;                /*+ Array of keys                     +*/
  dim_t    *   keytab2;               /*+ Another array of keys             +*/
} Queue;


#define static

int     queueInit       (Queue *, dim_t size);
void    queueExit       (Queue *);
Queue * queueCopy       (Queue *dst, Queue *src);
void    queueAdd        (Queue *, dim_t, FLOAT);
void    queueAdd2       (Queue *, dim_t, FLOAT, dim_t);
dim_t     queueGet        (Queue *);
void    queueGet2       (Queue *, dim_t *, FLOAT *);
void    queueGet3       (Queue *, dim_t *, dim_t *);
dim_t     queueSize       (Queue *);
void    queueClear      (Queue *);
dim_t     queueRead       (Queue *);
dim_t     queueReadKey    (Queue *);
static int compWith2keys(Queue *, dim_t, dim_t);
#undef static
#endif


#ifndef HEAP_H
#define HEAP_H

/*
**  The type and structure definitions.
*/

typedef struct Heap_ {
  dim_t        size;                  /*+ Allocated memory size             +*/ 
  dim_t        used;                  /*+ Number of element in the queue    +*/
  dim_t    *   elttab;                /*+ Array of the element              +*/
} Heap;


#define static

int     Heap_Init       (Heap *, dim_t size);
void    Heap_Exit       (Heap *);
Heap  * Heap_Copy       (Heap *dst, Heap *src);
void    Heap_Add        (Heap *, dim_t);
dim_t     Heap_Get        (Heap *);
dim_t     Heap_Size       (Heap *);
void    Heap_Clear      (Heap *);
dim_t     Heap_Read       (Heap *);


#endif

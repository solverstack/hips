#ifndef MACRO_H
#define MACRO_H

#define CMD_(type,name) type ## _ ## name
#define CMD(type,name) CMD_(type,name) 

#define _CMD_2(arg) arg /* utile ? */
#define _CMD_(type,name) _CMD_2(_ ## type ## _ ## name)
#define _CMD(type,name) _CMD_(type,name) 


#if defined(SYMMETRIC)
#define CMDu_(type,name) CMD_(type, name)
#else
#define CMDu_(type,name) type ## _ ## name ## u
#endif
#define CMDu(type,name) CMDu_(type,name) 

/* tmp */
#if defined(SYMMETRIC)
#define CMDus_(type,name) CMD_(type, name)
#else
#define CMDus_(type,name) type ## _ ## name ## _Unsym
#endif
#define CMDus(type,name) CMDus_(type,name) 

#ifndef PARALLEL

#define _DBDistrPrec        DBPrec
#define _DBDistrMatrix      DBMatrix
#define _PhidalDistrPrec    PhidalPrec
#define _PhidalDistrMatrix  PhidalMatrix
#define _PhidalDistrHID     PhidalHID
#define _DBL                BL
#define _DBPrecMem          DBPrecMem
#define _UDBDistrMatrix     UDBMatrix
#define _UPhidalDistrMatrix UPhidalMatrix

#define S(A) A
#define LHID(A) A

#else /* PARALLEL */

#include <mpi.h>
#include "phidal_parallel.h"
#include "db_parallel.h"

#define _DBDistrPrec        DBDistrPrec
#define _DBDistrMatrix      DBDistrMatrix
#define _PhidalDistrPrec    PhidalDistrPrec
#define _PhidalDistrMatrix  PhidalDistrMatrix
#define _PhidalDistrHID     PhidalDistrHID
#define _DBL                DBL
#define _DBPrecMem          DBDistrPrecMem
#define _UDBDistrMatrix     UDBDistrMatrix
#define _UPhidalDistrMatrix UPhidalDistrMatrix

#define S(A) (&A->M)
#define LHID(A) (&A->LHID)

#endif /* PARALLEL */

#endif /* MACRO_H */

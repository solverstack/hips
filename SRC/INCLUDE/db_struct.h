/* @authors J. GAIDAMOUR, P. HENON */

#ifndef DB_STRUCT_H
#define DB_STRUCT_H

#include "phidal_struct.h"
#include "solver.h"
#ifdef WITH_PASTIX
#include "pastix_calls.h"
#endif

enum alloc   {ONE, CBLK, RBLK, BLK};
enum virtual {NOVIRTUAL=0, VIRTUAL=1, COPY=2};

typedef int alloc_t;
typedef int virtual_t;

struct DBMatrix_ {
  
  flag_t symmetric; /** This filed indicates :
		     0: the matrix is not symmetrix symmetric
  		     1: the matrix is symmetric, only the lower triangular part of the matrix is stored **/
  flag_t  csc;  /** 0: the block matrices are in CSR format;   1: they are in CSC format **/       /* == Transposed or not */

  /*** This fields describe the sparse block pattern of the matrix ***/
  dim_t dim1; /** Dimension of the matrix (rows)**/
  dim_t dim2; /** Dimension of the matrix (columns)**/
  dim_t tli, tlj, bri, brj; /** The matrix is the bloc area between the top left block (tli, tlj)
			      and the bottom right block (bri, brj) of the phidal block pattern **/

  dim_t                       nodenbr;
  dim_t                       cblknbr;
  SymbolCCblk              *ccblktab;        /* Array of solver column blocks, only in RBLK   */

  blas_t                      *hdim; /* Utile ? Fractionner ? */

  /** The sparse block pattern is symmetric: we store its structure in CSR and CSR format for the lower triangular part **/
  /* The triangular lower part of the sparse block pattern in CSC format */
  INTL *cia;
  dim_t *cja;
  VSolverMatrix **ca;
  /* The triangular lower part of the sparse block pattern in CSR format */
  INTL *ria;
  dim_t *rja;
  VSolverMatrix **ra;

  /* The array of sparse matrices (one for each block of the matrix) listed by rows */
  INTL bloknbr;
  VSolverMatrix *bloktab;

  /* SymbolMatrix symbmtx; */
  LONG coefmax;

  alloc_t   alloc;
  virtual_t virtual; /** TODOTODOTDO ==+>>>> if == 1 then it means that the SparRow struct pointer are not allocated in this DBMatrix
		         but in another DBMatrix so blotab == NULL **/

  SolverMatrix*  a;

  SolverMatrix** cblktosolvmtx;
#ifdef WITH_PASTIX
  pastix_struct *pastix_str; /* one pastix_str for each domain */
#endif
};

/* typedef struct DBMatrix_ DBMatrix; */

/* #define PREC_L(P) (P)->LU.L */
/* #define PREC_U(P) (P)->LU.U */

/* #define PREC_EDB(P) (P)->EF_DB.L */
/* #define PREC_FDB(P) (P)->EF_DB.U */

/* #define PREC_SL(P) (P)->S.L */
/* #define PREC_SU(P) (P)->S.U */

/* #define PREC_E(P) (P)->LU.U FAUX */
/* #define PREC_F(P) (P)->LU.U */

/* #define PREC_E(P) (P)->LU.U */
/* #define PREC_F(P) (P)->LU.U */

/* typedef struct DBPrec_  DBPrec; */
/* struct DBPrec_ { */
/*   flag_t symmetric;  */
/*   dim_t dim;           /\** The dimension of the system preconditonned **\/ */
/*   int_t levelnum; */
/*   int_t forwardlev; */

/*   UDBMatrix LU; */

/*   UDBMatrix EF_DB; */
/* /\*   DBMatrix *E_DB; *\/ */
/* /\*   DBMatrix *F_DB; *\/ */

/*   PhidalMatrix *B; */
/*   PhidalMatrix *E; */
/*   PhidalMatrix *F; */

/*   UDBMatrix S; */

/*   DBPrec *prevprec; */
/*   DBPrec *nextprec; */
/*   flag_t schur_method;  /\** Indicate the smoothing method in the multi-level; */
/* 			 0 := no smoothing; in this case the schur complement S is not stored */
/* 			  1 := iteration in the schur complement with storage of the schur complement */
/* 			  2 := iteration in the schur complement with implicit schur complement (schur not stored) *\/ */
/*   PhidalMatrix *phidalS; */
/*   PhidalPrec   *phidalPrec; */
  
/*   PrecInfo* info; */
/* }; */



/* IMPORTANT : Must contains only long !!!! */
typedef struct DBPrecMem_  DBPrecMem;
struct DBPrecMem_ {
  long A;

  long L;
/*   long L_NOREC; */
  long C;
  long E;
  /* long U; */

  long E_DB;
  long Emax;
  /* long F_DB; */

  /* long B; */
  /* long E; */
  /* long F; */

  long SL;
  /* long *SU; */

  /* DBPrecMem *nextprec; */
  /*longphidalPrec;*/
  long LS; /* Seulement 2 niveau ... */

  long phidalS;

  long ll;

  /* E en phidal dans dropE */
  long dropE;
  long dropEAfter;
  
};


#endif

/* @authors J. GAIDAMOUR, P. HENON */

#ifndef SYMBOL_H
#define SYMBOL_H

#define SYMBOL_VERSION              1

/** Need to include base.h here because it contains essl.h
    and there is trouble on AIX with "stride" defined as a
    macro if essl.h is not include **/
#include "hips_define.h"
#include "type.h"
#include "base.h"

/*
**  The type and structure definitions.
*/

/*+ The column block structure. +*/

typedef struct SymbolCCblk_ {
  dim_t                       fcolnum;              /*+ First column index               +*/
  dim_t                       lcolnum;              /*+ Last column index (inclusive)    +*/ /* TODO : utilité de cela ? */
} SymbolCCblk;


typedef struct SymbolBCblk_ {
  INTL                       fbloknum;             /*+ First block in column (diagonal) +*/
  INTL                       lbloknum;             /*+ Last  block in column            +*/
} SymbolBCblk;

/*+ The column block structure. +*/
typedef struct SymbolBlok_ {
  dim_t                     frownum;              /*+ First row index            +*/
  dim_t                     lrownum;              /*+ Last row index (inclusive) +*/
  dim_t                     cblknum;              /*+ Facing column block        +*/
} SymbolBlok;

/*+ The symbolic block matrix. +*/
typedef struct SymbolMatrix_ {
  dim_t                     cblknbr;              /*+ Number of column blocks           +*/

  SymbolCCblk *             ccblktab;             /*+ Array of column blocks            +*/
  SymbolBCblk *             bcblktab;             /*+ Array of column blocks            +*/
  blas_t *                  stride; 
  blas_t *                  hdim;

  INTL                       bloknbr;              /*+ Number of blocks                  +*/
  SymbolBlok *              bloktab;              /*+ Array of blocks [based]           +*/

  dim_t                     nodenbr;              /*+ Number of nodes in matrix         +*/

  dim_t                     tli;
  dim_t                     facedecal;
  dim_t                     cblktlj; /* todo : n'existe qu'apres SymbolMatrixCut1 */
  flag_t                    virtual;
} SymbolMatrix;


/* // Interval */
typedef struct BlokStruct_ {
  dim_t                     frownum;              /*+ First column index               +*/
  dim_t                     lrownum;              /*+ Last column index (inclusive)    +*/
} BlokStruct;

typedef struct CblkStruct_ {
  INTL bloknbr;                                       /* Number of blocks    */
  BlokStruct* bloktab;                              /* Array of blocks     */
} CblkStruct;

typedef CblkStruct MatrixStruct;                 /* Array of column blocks */





#endif

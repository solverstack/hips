/* @authors P. HENON */

#ifndef PHIDAL_STRUCT_H
#define PHIDAL_STRUCT_H
#include "type.h"

#include "hips_define.h"
/*#include "heads.h"*/

typedef struct SparRow {
/*--------------------------------------------- 
| C-style CSR format - used internally
| for all matrices in CSR format 
|---------------------------------------------*/
  dim_t n;
  /*  length of each row */
  dim_t *nnzrow;
  /* pointer-to-pointer to store nonzero entries */
  COEF **ma;
  /* pointer-to-pointer to store column indices  */
  dim_t **ja;
  dim_t nnzr; /* number of non zero rows */
  dim_t *nzrtab; /* nnzrtab[i] is the i_th non zero row */
  dim_t *inzrtab; /* inzrtab[i] is the next non zero row after row i : 
		   can be equal to i */

  flag_t inarow; /** This flag means the matrix has been allocated as a single block 
		  of memory; it must be desallocated in consequence **/

  /** Used if inarow == 1 to store the the matrix in two contigue block of memory **/
  dim_t *jatab;
  COEF *matab;

} SparMat;
typedef struct SparRow *csptr;



/** Define a structure to store the various option **/
struct PhidalOptions{
  flag_t verbose;    /* to get some messages ... */
  FILE *fd;       /* File descriptor for output message */
  flag_t krylov_method; /*0: gmres;   1: PCG */
  flag_t symmetric;
  flag_t scale;      /* 0 := no scaling 1:= scaling (row and column) */
  int_t scalenbr;      /* number of time the scaling is applied */
  flag_t pivoting;   /* 0: no column pivoting allowed in ILUTP, 1: column pivoting allowed in ILUTP */ 
  REAL pivot_ratio; /** The diagonal term Dii is replaced by the greater term M in the row iff M > Dii * pivot_ratio **/
  flag_t count_nnz;  /* to count the nnz(ILU)/nnz(A) */
  int_t locally_nbr; /* the first "locally_nbr" level of the interface will be eliminated in a locally dropping strategy ***/
                   /* A good choice is often locally_nbr = 1, if does not converge try a higher value */
  REAL fillrat;   /** Control the fill-in during the factorization **/
  REAL Schur_fillrat; /** Control the fill-in to create schur complement **/

  REAL amalg_rat; /** Ratio for amalgation **/

  REAL dropSchur; /** dropping in the schur complement usually set this value = 0.1*dropU */

  REAL tol;         /** tolerance to stop iteration in fgmres **/
  dim_t krylov_im;    /** Krylov subspace dimemsion in FGMRES **/


  flag_t schur_method; /** 0: no iteration on schur;   1: iterations in schur complement (store the schur complement) 2: implicit schur iteration **/
  REAL droptol0;   /** Dropping threshold for level 0 grid (interior subdomains) **/
  REAL droptol1;   /** Dropping threshold for level >= 1 (connectors of the interface) **/
  REAL droptolrat; /** Ratio to increase the dropping tolerance from level n to level n+1: droptol(n+1) = droptolrat*droptol(n) **/
  REAL droptolE;   /** Dropping threshold for E.U^-1, L^-1.F**/

  dim_t itmax;         /** Maximum number of iteration in fgmres **/
  dim_t itmaxforw;    /** Number of iteration allowed in forward schur complement level **/
  REAL itforwrat; /** Level l+1 itmax = level l itmax * itforwrat **/
  int_t forwardlev;   /** Number of recursive schur complement level **/
  flag_t shiftdiag; /** ==1 means a shift is applied on the diagonal of the incomplete factorizationn **/
  int_t use_pastix; /** 0 : do not use PaStiX ; 1 : use PaStiX **/
};
typedef struct PhidalOptions PhidalOptions; 


/** Define the block level structure **/
struct PhidalHID_{
  dim_t ndom;                    /* Total number of domain in parallel */
  dim_t locndom;                    /* Total number of domain for the
				     local processor */
  dim_t n;                       /* dimension of local matrix          */
  int_t nblock;                  /* Number of blocks */
  int_t nlevel;                  /* Number of levels */
  dim_t *block_index;            /* the unknowns in the ith block are in [block_index[i]:block_index[i+1]-1] */
  dim_t *block_levelindex;       /* the ith levels is composed with blocks 
				  [block_levelindex[i]:block_levelindex[i+1]-1] */
  dim_t *block_keyindex;         /* the key of the ith block is .. */
  dim_t *block_key;              /* .. block_key[ block_keyindex[i]:block_keyindex[i+1]-1 ] */   

  INTL *block_domwgt;           /* nnz of a domain (used for equilibration in parallel) */
  /*dim_t *perm;
    dim_t *iperm;*/

  dim_t dof;                     /* degree of freedom (wgt) */
};

typedef struct PhidalHID_  PhidalHID;



/** This structure contains informations about connector and their neighbor **/
struct CONNECTOR_str{
  int_t *vlist;  /** List of the connector neighbors **/
  int_t nvert;   /** Size of vlist **/
  int_t *key;   /** Key of the connector **/
  int_t nkey;  /** Size of key **/
  int_t *listn; /** List of the neighbor **/
  int_t numn;  /** Size of listn **/
};
typedef struct CONNECTOR_str CONNECTOR_str;






struct cell_int
{
  dim_t elt;
  struct cell_int *next;

  /** version 2 **/
  dim_t nnz;
  dim_t *ja;
  COEF *ma;

};

typedef struct cell_int cell_int;



struct CellCS
{
  dim_t nnz;
  dim_t *ja;
  csptr *ma;
  
};
typedef struct CellCS CellCS;


struct PhidalMatrix_ {
  
  flag_t symmetric; /** This field indicates :
		     0: the matrix is not symmetrix symmetric
		     1: the matrix is symmetric, only the lower triangular part of the matrix is stored **/
  flag_t  csc;  /** 0: the block matrices are in CSR format;   1: they are in CSC format **/       
  /*** This fields describe the sparse block pattern of the matrix ***/
  dim_t dim1; /** Dimension of the matrix (rows)**/
  dim_t dim2; /** Dimension of the matrix (columns)**/
  dim_t tli, tlj, bri, brj; /** The matrix is the bloc area between the top left block (tli, tlj)
			      and the bottom right block (bri, brj) of the phidal block pattern **/
  /** The sparse block pattern is symmetric: we store its structure in CSR and CSR format for the lower triangular part **/
  /* The triangular lower part of the sparse block pattern in CSC format */
  INTL *cia;
  dim_t *cja;
  struct SparRow **ca;
  /* The triangular lower part of the sparse block pattern in CSR format */
  INTL *ria;
  dim_t *rja;
  struct SparRow **ra;

  flag_t virtual; /** if == 1 then it means that the SparRow struct pointer are not allocated in this PhidalMatrix
		   but in another PhidalMatrix so blotab == NULL **/
  /* The array of sparse matrices (one for each block of the matrix) listed by rows */
  INTL bloknbr;
  struct SparRow *bloktab;
};
/* typedef struct PhidalMatrix_  PhidalMatrix; */

typedef struct PrecInfo_  PrecInfo;
struct PrecInfo_ {
  INTL nnzA;
  INTL nnzP;
  INTL peak;
};

#include "phidal_prec.h"

void PhidalHID_Init(PhidalHID *BS);  
void PhidalHID_Clean(PhidalHID *BS);

void initCONNECTOR(CONNECTOR_str *C);
void cleanCONNECTOR(CONNECTOR_str *C);


void PhidalOptions_Init(PhidalOptions *option);
void PhidalOptions_Clean(PhidalOptions *option);

flag_t initCS(csptr amat, dim_t len);
flag_t reinitCS(csptr mat); 
flag_t cleanCS(csptr mat);


void PhidalMatrix_Init(PhidalMatrix *a);
void PhidalMatrix_Clean(PhidalMatrix *a);
void PhidalPrec_Init(PhidalPrec *p);
void PhidalPrec_Clean(PhidalPrec *p);

void PrecInfo_Init(PrecInfo* info);
void PrecInfo_Clean(PrecInfo* info);

#endif

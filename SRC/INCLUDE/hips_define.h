/* @authors J. GAIDAMOUR, P. HENON */

#ifndef HIPS_DEFINE_H
#define HIPS_DEFINE_H


#include "base.h"
#include "localdefs.h"

/*#define PARALLEL_LOCAL_ORDERING problem A REVOIR EN iteratif ***/

/*#define PIC_INITIAL **/  /** decommenter pour avoir le "pic initial" **/
/*#define ILUT_WHOLE_SCHUR */ /** To factorize the Schur in 
			      a single CS matrices instead 
			      of using a PhidalMatrix block pattern **/
/*#define ILUT_SOLVE*/  /** The schur preconditioner is transformed into a
			single SparRow matrix for the solve    **/


/*#define OPTIM_FEW_NZR  NOT GOOD ON TESTS **/


#define REALLOC_BLOCK 
/** This corresponds to a page of 2Mo BETTER ON AIX 64b **/
#define SMALLBLOCK 232144  


/*********************************/
#define BALANCE_NNZ  /** The domain distribution on the
			processors are made in order to balance the number of 
			nnz in each domains; if commented then the distribution is made to
			balance the number of unknowns in each domains **/


/*#define HAZARDOUS_BALANCE*/ /** If defined this desactivate the second level of load balancing
  for the connector in the interface ; it is replace by a random function **/

/*#define COMM_ONE */  /** If defined == all communication Irecv are posted at once in
		       the parallel factorization (ILUT) **/

/*#define OLD_COMM_AHEAD*/
#define COMM_AHEAD 4 /** Number of column block ahead for which the receive requests are posted 
			 MINIMUM = 1 ; if COMM_ONE is defined then no effect **/ 
/*#define COMM_MEM  */

#define FACT_POSTED_MAX 10
#define FACT_BUFMEM_MAX 1024*1024*10;
#define VEC_POSTED_MAX 100
#define VEC_BUFMEM_MAX 1024*1024*10;

/**********************************/
/* Alloc in block ILUT subroutine */
/**********************************/
/*#define ALLOC_COMPACT_ILUT    */
#define ALLOC_COMPACT_RATIO 1.5
#define ALLOC_COMPACT_INIT 2


/*#define SCOTCH_OVERLAP*/

/** This corresponds to a page of 1Mo **/
/*#define SMALLBLOCK 131072  */

/** To activate ready send for vectors communication **/
/*#define VEC_READY_SEND */

/** Minimum buffer size (in nnz) under which the comm mem. ratio is not applied
    **/
/*#define MIN_BUFF_SIZE  88036 */
#define MIN_BUFF_SIZE 2500

#define SYMMETRIC_DROP

/*#define SCALE_ALL*/    /** PUT THIS OPTION TO BE SURE IN DBMATRIX + ILUT
			 IN SCHUR TO RESCALE THE SCHUR **/
/*#define EPSILON 1e-20*/
#define EPSILON 1e-16

#define ZERO    10e-30
#define EPSMAC   1.0e-16   /** Used in gmres **/

#ifndef TRUE
#define TRUE 1
#endif

#ifndef FALSE
#define FALSE 0
#endif

/** Macros used in the communication layer **/
#define LEAD_TAG 1
#define CTRB_TAG 2
#define CTRB_TAG_L 3
#define CTRB_TAG_U 4
#define LEAD_TAG_L 5
#define LEAD_TAG_U 6
#define DIAG_TAG 7
#define SYNCHRONOUS 0
#define ASYNCHRONOUS 1
#define NOT_READY 0
#define READY 1

/*#ifdef DEBUG_M
#define free(a)  {assert(a); free(a); a=NULL;}
#define malloc(b) ((b)>0?(malloc((b))):(NULL))
#endif*/

/*#define TRACE_COM */ /** print communication for debugging purpose **/

#ifdef DEBUG_M
#define free(a) MonFree(a)
#define malloc(a) MonMalloc(a)
#define realloc(p, a) MonRealloc(p, a)
#endif

#ifndef MAX /* conflicting with PETsc lib */
#define MAX(x,y) (((x)<(y))?(y):(x))
#endif

#ifndef MIN /* conflicting with PETsc lib */
#define MIN(x,y) (((x)>(y))?(y):(x))
#endif



#ifdef DEBUG_M
#define CHECK_MALLOC(a) {if(a == NULL) assert(0);};
#define CHECK_RETURN(a) {INTS ret; ret = a; HIPS_PrintError(ret); if(ret != HIPS_SUCCESS) assert(0);}
#define FSCANF(args...) {int m; m = fscanf(args); if(m == 0) assert(0);}
#define FGETS(args...) {char *m; m = fgets(args); if(m == NULL) assert(0);}
#define CHECK_MODE  {if(mode != HIPS_ASSEMBLY_RESPECT && mode != HIPS_ASSEMBLY_FOOL) assert(0);};
#define CHECK_OP  {if(op != HIPS_ASSEMBLY_OVW && op != HIPS_ASSEMBLY_ADD) assert(0);};
#define CHECK_OP2  {if(op2 != HIPS_ASSEMBLY_OVW && op2 != HIPS_ASSEMBLY_ADD) assert(0);};
#define CHECK_NUMFLAG {if(numflag != 0 && numflag != 1) assert(0);};
#else
#define CHECK_MALLOC(a) {if(a == NULL) return HIPS_ERR_ALLOCATE;};
#define CHECK_RETURN(a) {INTS ret; ret = a; HIPS_PrintError(ret); if(ret != HIPS_SUCCESS) return ret;}
#define FSCANF(args...) {int m; m = fscanf(args); if(m == 0) assert(0);}
#define FGETS(args...) {char * m; m = fgets(args); if(m == NULL) assert(0);}
#define CHECK_MODE  {if(mode != HIPS_ASSEMBLY_RESPECT && mode != HIPS_ASSEMBLY_FOOL) return HIPS_ERR_PARAMETER;};
#define CHECK_OP  {if(op != HIPS_ASSEMBLY_OVW && op != HIPS_ASSEMBLY_ADD) return HIPS_ERR_PARAMETER;};
#define CHECK_OP2  {if(op2 != HIPS_ASSEMBLY_OVW && op2 != HIPS_ASSEMBLY_ADD) return HIPS_ERR_PARAMETER;};
#define CHECK_NUMFLAG {if(numflag != 0 && numflag != 1) HIPS_ERR_PARAMETER;};

#endif

#endif

/* @authors P. HENON */

#ifndef PHIDAL_SEQUENTIAL_H
#define PHIDAL_SEQUENTIAL_H
#include "hips_define.h"
#include "phidal_ordering.h"


void CellCS_ListUpdate(CellCS *celltab, int num,  int n,  dim_t *ja);
void CellCS_ListCopy(CellCS *src, CellCS *dest,  int n,  dim_t *ja);
void CellCS_GetCSList(int num, CellCS *celltab, int n,  dim_t *ja, csptr *ma, int *nnb, csptr *csrtab1, csptr *csrtab2);
void CellCS_IntersectList(CellCS *celltab, int ncol,  int *colja, csptr *colma, int nrow, int *rowja, 
			  int *listindex, csptr *listA, csptr *listB);


void PHIDAL_MatrixBuild(csptr A, PhidalMatrix *M, PhidalHID *BL, PhidalOptions *option);
void PHIDAL_Precond(PhidalMatrix *A, PhidalPrec *P, PhidalHID *BL, PhidalOptions *option);
INTS PHIDAL_Solve( PhidalMatrix *A, PhidalPrec *P, PhidalHID *BL, PhidalOptions *option, COEF *rhs, COEF * x, dim_t *itertab, REAL *resnormtab);

void PhidalMatrix_Reinit(PhidalMatrix *M);
void PhidalMatrix_Setup(dim_t tli, dim_t tlj, dim_t bri, dim_t brj, char *UPLO, char *DIAG, flag_t locally_levelnbr, PhidalMatrix *m, PhidalHID *BL);
void PhidalMatrix_Setup2(dim_t tli, dim_t tlj, dim_t bri, dim_t brj, char *UPLO, char *DIAG, flag_t locally_levelnbr, PhidalMatrix *m, PhidalHID *BL);
void PhidalMatrix_Build(flag_t symmetric, char *UPLO, csptr a, PhidalMatrix *m, PhidalHID *BL);
void PhidalMatrix_CsrCopy(flag_t job, csptr a, char *DIAG, PhidalMatrix *m, PhidalHID *BL);
void PhidalMatrix_CscCopy(flag_t job, csptr a, char *DIAG, PhidalMatrix *m, PhidalHID *BL);
void PhidalMatrix_Copy(PhidalMatrix *a, PhidalMatrix *b, PhidalHID *BL);
void PhidalMatrix_Add(PhidalMatrix *a, PhidalMatrix *b, PhidalHID *BL);
void PhidalMatrix_Cut(PhidalMatrix *a, PhidalMatrix *b, PhidalHID *BL);
void PhidalMatrix_BuildVirtualMatrix(dim_t tli, dim_t tlj, dim_t bri, dim_t brj, PhidalMatrix *a, PhidalMatrix *v, PhidalHID *BL);
long PhidalMatrix_NNZ(PhidalMatrix *m);
long PhidalPrec_NNZ(PhidalPrec *p);
void PhidalPrec_Info(PhidalPrec *p);
void PhidalMatrix2SparRow(flag_t job, PhidalMatrix *M, csptr mat, PhidalHID *BL);

void PhidalMatrix_RowNormINF(PhidalMatrix *A, PhidalHID *BL, REAL *normtab);
void PhidalMatrix_RowNorm1(PhidalMatrix *A, PhidalHID *BL, REAL *normtab);
void PhidalMatrix_RowNorm2(PhidalMatrix *A, PhidalHID *BL, REAL *normtab);
void PhidalMatrix_ColNormINF(PhidalMatrix *A, PhidalHID *BL, REAL *normtab);
void PhidalMatrix_ColNorm1(PhidalMatrix *A, PhidalHID *BL, REAL *normtab);
void PhidalMatrix_ColNorm2(PhidalMatrix *A, PhidalHID *BL, REAL *normtab);
REAL PhidalMatrix_RowDensity(PhidalMatrix *m);
void PhidalMatrix_SymScale(PhidalMatrix *A, PhidalHID *BL, REAL *scaletab, REAL *iscaletab);
void PhidalMatrix_UnsymScale(int nbr, PhidalMatrix *A, PhidalHID *BL, REAL *scalerow, REAL *scalecol, REAL *iscalerow, REAL *iscalecol);

void PhidalMatrix_RowMult(COEF *vec, PhidalMatrix *A, PhidalHID *BL);
void PhidalMatrix_RowMult2(REAL *vec, PhidalMatrix *A, PhidalHID *BL);

void PhidalMatrix_ColMult(COEF *vec, PhidalMatrix *A, PhidalHID *BL);
void PhidalMatrix_ColMult2(REAL *vec, PhidalMatrix *A, PhidalHID *BL);


void PhidalPrec_SymmetricUnscale(REAL *scaletab, REAL *iscaletab, PhidalPrec *P, PhidalHID *BL);
void PhidalPrec_UnsymmetricUnscale(REAL *scalerowtab, REAL *iscalerowtab, REAL *scalecoltab, REAL *iscalecoltab, PhidalPrec *P, PhidalHID *BL);

void PhidalMatrix_GetUdiag(PhidalMatrix *U, COEF *diag);
void PhidalMatrix_SetUdiag(PhidalMatrix *U, COEF *diag);

void PhidalMatrix_ColPerm(PhidalMatrix *m, dim_t *permtab, PhidalHID *BL);
void PhidalMatrix_RowPerm(PhidalMatrix *m, dim_t *permtab, PhidalHID *BL);
void PhidalMatrix_Transpose(PhidalMatrix *M);
void PhidalMatrix_Transpose_SparMat(PhidalMatrix* m, flag_t job, char *UPLO, PhidalHID* BL);

void PhidalMatrix_DropT(PhidalMatrix *A, REAL droptol, REAL *droptab, PhidalHID *BL);
void PhidalMatrix_DropTCol_Sym  (int j, PhidalMatrix *A, REAL droptol, REAL *dropptr);
void PhidalMatrix_DropTCol_Unsym(int i, PhidalMatrix *A, REAL droptol, REAL *dropptr);

REAL  PhidalMatrix_NormFrob(PhidalMatrix *m); /*@@ OIMBE a revoir ca fait de drole de truc ***/
REAL  PhidalMatrix_Norm1(PhidalMatrix *m);
void PhidalMatrix_Realloc(PhidalMatrix *m);
void PHIDAL_MatVec(PhidalMatrix *m, PhidalHID *BL, COEF *x, COEF *y);
void PHIDAL_MatVecSub(PhidalMatrix *M, PhidalHID *BL, COEF *x, COEF *y);
void PHIDAL_MatVecAdd(PhidalMatrix *M, PhidalHID *BL, COEF *x, COEF *y);

void PhidalPrec_SchurProd(PhidalPrec *P, PhidalHID *BL, COEF *x, COEF *y);
void PhidalPrec_GEMM_ILUCT(int_t levelnum, REAL *droptab, PhidalMatrix *G, PhidalMatrix *W, PhidalMatrix *A, 
			   PhidalPrec *P, PhidalHID *BL, PhidalOptions *option);
void PhidalPrec_GEMM_ICCT(int_t levelnum, REAL *droptab, PhidalMatrix *G, PhidalMatrix *A, COEF *DG, 
		      PhidalPrec *P, PhidalHID *BL, PhidalOptions *option);
void PHIDAL_ILUTP(flag_t job, int pivoting, PhidalMatrix *A, PhidalMatrix *L, PhidalMatrix *U, REAL droptol, REAL *droptab, REAL fillrat, dim_t *permtab, PhidalHID *BL, PhidalOptions *option, PrecInfo *info);
void PHIDAL_GEMM(char *TRANSA, char *TRANSB, char *TRANSC, REAL alpha,  PhidalMatrix *a, PhidalMatrix *b, PhidalMatrix *c, PhidalHID *BL, int *wki1, int *wki2, COEF *wkd);
void PHIDAL_GEMMT(REAL droptol, REAL *droptab, REAL fillrat, char *TRANSA, char *TRANSB, char *TRANSC, REAL alpha, PhidalMatrix *a, PhidalMatrix *b, PhidalMatrix *c, PhidalHID *BL, int *wki1, int *wki2, COEF *wkd);
void PHIDAL_InvLT(PhidalMatrix *L, PhidalMatrix *M, REAL droptol, REAL *droptab, REAL fillrat,  PhidalHID *BL, int *wki1, int *wki2, COEF *wkd);
void PHIDAL_InvUT(PhidalMatrix *U, PhidalMatrix *M, REAL droptol, REAL *droptab,  REAL fillrat, PhidalHID *BL, Heap *heap, int *wki1, int *wki2, COEF *wkd);
void PHIDAL_ICCT_InvLT(PhidalMatrix *L, PhidalMatrix *M, REAL droptol, REAL *droptab, REAL fillrat, PhidalHID *BL, int *wki1, int *wki2, COEF *wkd);

void PhidalMatrix_ILUTP(int pivoting, PhidalMatrix *L, PhidalMatrix *U, REAL droptol, REAL *droptab, REAL fillrat,  dim_t *permtab, PhidalHID *BL, PhidalOptions *option, PrecInfo *info);
void PhidalMatrix_ILUCT(PhidalMatrix *L, PhidalMatrix *U, REAL droptol, REAL *droptab, REAL fillrat, PhidalHID *BL, PhidalOptions *option, PrecInfo *info);
void PhidalMatrix_ILUCT_Restrict(int START, PhidalMatrix *L, PhidalMatrix *U, REAL droptol, REAL *droptab, REAL fillrat, PhidalHID *BL, PhidalOptions *option, PrecInfo *info);
void PhidalMatrix_Lsolve(flag_t unitdiag, PhidalMatrix *L, COEF *x, COEF *b, PhidalHID *BL);
void PhidalMatrix_Usolve(flag_t unitdiag, PhidalMatrix *U, COEF *x, COEF *b, PhidalHID *BL);
void PhidalMatrix_ILUTSchur(flag_t job, PhidalMatrix *G, PhidalMatrix *W, PhidalMatrix *S, PhidalHID *BL, PhidalOptions *option);
void PhidalMatrix_ICCTSchur(flag_t job, PhidalMatrix *G, COEF *D, PhidalMatrix *S, PhidalHID *BL, PhidalOptions *option);

void PHIDAL_MLILUTPrec(PhidalMatrix *A, PhidalPrec *P, PhidalHID *BL, PhidalOptions *option);
INTS PHIDAL_PrecSolve(REAL tol, PhidalPrec *P, COEF *x, COEF *b, PhidalHID *BL, PhidalOptions *option, dim_t *itertab, REAL *resnormtab);

void PHIDAL_ICCT(flag_t job, PhidalMatrix *A, PhidalMatrix *L, COEF *D, REAL droptol, REAL *droptab, REAL fillrat, PhidalHID *BL, PhidalOptions *option, PrecInfo *info);
void PhidalMatrix_ICCT(PhidalMatrix *L, COEF *D, REAL droptol, REAL *droptab, REAL fillrat, PhidalHID *BL, PhidalOptions *option, PrecInfo *info);
void PhidalMatrix_ICCT_Restrict(int START, PhidalMatrix *L, COEF *D, REAL droptol, REAL *droptab, REAL fillrat, PhidalHID *BL, PhidalOptions *option, PrecInfo *info);


void PHIDAL_MLICCTPrec(PhidalMatrix *A, PhidalPrec *P, PhidalHID *BL, PhidalOptions *option);

void  PhidalPrec_MLICCT(int_t levelnum, int forwardlev, REAL *droptab,  PhidalMatrix *A, PhidalPrec *P, PhidalHID *BL, PhidalOptions *option);

void PHIDAL_MLILUCPrec(PhidalMatrix *A, PhidalPrec *P, PhidalHID *BL, PhidalOptions *option);

void  PhidalPrec_MLILUT(int_t levelnum, int forwardlev, REAL *droptab,  PhidalMatrix *A, PhidalPrec *P, PhidalHID *BL, PhidalOptions *option);

void PHIDAL_ILUC(flag_t job, int pivoting, PhidalMatrix *A, PhidalMatrix *L, PhidalMatrix *U, 
		 REAL droptol, REAL *droptab, REAL fillrat, dim_t *permtab, PhidalHID *BL, PhidalOptions *option, PrecInfo *info);

INTS HIPS_Fgmresd_PH_PH(flag_t verbose, REAL tol, dim_t itmax, 
		       PhidalMatrix *A, PhidalPrec *P, 
		       PhidalHID *BL, PhidalOptions *option, 
		       COEF *rhs, COEF *x, FILE *fp, dim_t *itertab, REAL *resnormtab);

INTS HIPS_Fgmresd_PH   (flag_t verbose, REAL tol, dim_t itmax, 
		       PhidalPrec *P, 
		       PhidalHID *BL, PhidalOptions *option, 
		       COEF *rhs, COEF *x, FILE *fp, dim_t *itertab, REAL *resnormtab);

INTS HIPS_PCG_PH_PH(flag_t verbose, REAL tol, dim_t itmax, 
		       PhidalMatrix *A, PhidalPrec *P, 
		       PhidalHID *BL, PhidalOptions *option, 
		       COEF *rhs, COEF *x, FILE *fp, dim_t *itertab, REAL *resnormtab);

INTS HIPS_PCG_PH   (flag_t verbose, REAL tol, dim_t itmax, 
		       PhidalPrec *P, 
		       PhidalHID *BL, PhidalOptions *option, 
		       COEF *rhs, COEF *x, FILE *fp, dim_t *itertab, REAL *resnormtab);

REAL PhidalMatrix_SchurError(PhidalMatrix *G, PhidalMatrix *W, PhidalMatrix *S, PhidalHID *BL);

void PhidalMatrix_ReorderInterior(PhidalMatrix *A, PhidalHID *BL, dim_t *permtab, dim_t *ipermtab);

void PhidalMatrix_PrintBLockNNZ(FILE *file, PhidalMatrix *A);
void PhidalMatrix_Print(FILE *file, PhidalMatrix *A);


void PhidalPrec_PrintInfo(PhidalPrec *P, PhidalHID *BL);
REAL PhidalMatrix_RowDensity(PhidalMatrix *m);
void VecInvPermute(dim_t tli, dim_t bri, COEF *x, dim_t *permtab, PhidalHID *BL);
#endif

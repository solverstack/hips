/* @authors P. HENON */

#ifndef PHIDAL_ORDERING_H
#define PHIDAL_ORDERING_H
#include "hips_define.h"

#include "phidal_struct.h"
#include "phidal_common.h"

#ifdef SCOTCH_PART
void PHIDAL_ScotchOrder2OverlappedDomains(dim_t ndom, dim_t n, INTL *ia,dim_t *ja, int **mapptr,  int **mapp, dim_t *perm, dim_t *iperm);
#endif
void PHIDAL_GridRegularDomains(dim_t ndom, dim_t nx, dim_t ny, dim_t nz, dim_t n, INTL *ia, dim_t *ja, int **mapptr, int **mapp, dim_t *perm, dim_t *iperm);
void PHIDAL_GridRegularSizeDomains(int domsize, dim_t nx, dim_t ny, dim_t nz, dim_t n, INTL *ia, dim_t *ja, int *ndom, int **mapptr, int **mapp, dim_t *perm, dim_t *iperm);

void PHIDAL_InteriorReorderND(int halo, csptr mat, PhidalHID *BL, dim_t *perm, dim_t *iperm);
void PHIDAL_InteriorReorderRCM(int halo, csptr mat, PhidalHID *BL, dim_t *perm, dim_t *iperm);
void PHIDAL_Perm2OverlappedDomains(dim_t ndom, dim_t n, INTL *ia,dim_t *ja, dim_t **mapptr,  dim_t **mapp, dim_t *perm, dim_t *iperm);
void PHIDAL_Perm2SizedDomains(dim_t domsize, dim_t n, INTL *ia,dim_t *ja, dim_t *ndom, dim_t **mapptr,  dim_t **mapp, dim_t *perm, dim_t *iperm);
void PHIDAL_Perm2SizedDomains_SAVE(dim_t domsize, dim_t n, INTL *ia,dim_t *ja, dim_t *ndom, dim_t **mapptr,  dim_t **mapp, dim_t *perm, dim_t *iperm);
void PHIDAL_Perm2SizedDomains_LOAD(dim_t domsize, dim_t n, INTL *ia,dim_t *ja, dim_t *ndom, dim_t **mapptr,  dim_t **mapp, dim_t *perm, dim_t *iperm);

void PHIDAL_Partition2OverlappedPartition(flag_t numflag, dim_t ndom, dim_t n, INTL *ia, dim_t *ja, int *node2dom, int **mapp, int **mapptr);
void PHIDAL_GetOverlappedPartitions(int numflag, int ndom, int n, INTL *ia, dim_t *ja, int **mapp, int **mapptr);
void PHIDAL_HierarchDecomp(flag_t verbose, flag_t numflag, dim_t n,  INTL *ia,  dim_t *ja, int *mapp, 
				  int *mapptr,  dim_t ndom,  PhidalHID *BL, dim_t *perm, dim_t *iperm);

void PHIDAL_GetSupernodes(flag_t verbose, flag_t numflag, dim_t n,  INTL *ia,  dim_t *ja,  
			  PhidalHID *BL, dim_t *perm, dim_t *iperm,
			  int **dom2cblktab, int **cblktab, int **cblktreetab);

void PHIDAL_SymmetrizeMatrix(flag_t job, flag_t numflag, dim_t n, INTL *ia, dim_t *ja, COEF *a, INTL **gia, dim_t **gja, COEF **ga);
void PHIDAL_CsrDelDiag(flag_t numflag, dim_t n, INTL *ia, dim_t *ja);

void PHIDAL_WriteHID(FILE *fp, PhidalHID *BL);
void PHIDAL_LoadHID(FILE *fp, PhidalHID *BL);

void setuplevels(dim_t *mapp, dim_t *mapptr, int overlap_flag, int ndom, dim_t n,
			INTL *ia, dim_t *ja, dim_t *perm, int *rperm, PhidalHID *BL);
void setup_class(dim_t n, INTL *ia, dim_t *ja, int *rperm, PhidalHID *BL);	
void block_elimination_ordering(const dim_t ndom, PhidalHID *BL, int *rperm);
int vec_balanced_partition(dim_t ndom, dim_t n, REAL *W, int *elt2dom);
int get_interior_domains(dim_t ndom, dim_t *perm, dim_t cblknbr, int *cblktab, dim_t *treetab, dim_t *mapp, dim_t *mapptr);
int get_interior_grid(int domsize, dim_t cblknbr, int *cblktab, dim_t *treetab, int *ndom, int *cblk2dom);

void get_overlap(dim_t ndom, dim_t n, dim_t **mapptr, dim_t **mapp, INTL * const ia, dim_t * const ja);
void build_connector_struct(dim_t n, INTL *ia, dim_t *ja, int *rperm, PhidalHID *BL, CONNECTOR_str *Ctab);
void find_MIS(flag_t job, dim_t n, INTL *ia, dim_t *ja, INTL *sizetab, flag_t *mask, flag_t maskval, dim_t *MISsize, dim_t *MIS);
void connector_graph(dim_t nc, CONNECTOR_str *Ctab, int **c_ia, int **c_ja, int *mask, int maskval);
int fix_unassigned_node(dim_t ndom, dim_t n, dim_t **mapptr, dim_t **mapp, INTL *ia, dim_t *ja);
int fix_false_interior_node(dim_t ndom, dim_t n, dim_t **mapptr, dim_t **mapp, INTL *ia, dim_t *ja);

void  find_supernodes(dim_t nfirst, dim_t n, INTL *ia, dim_t *ja, dim_t *perm, dim_t *iperm, int *snodenbr, int *snodetab, dim_t *treetab);
int check_interior_node(dim_t ndom, dim_t *mapptr, dim_t *mapp, INTL *ia, dim_t *ja, dim_t n);
void PHIDAL_MinimizeFill(int *fillopt, int ln, INTL *lig, int *ljg, PhidalHID *BL, dim_t *iperm);

/*void PHIDAL_WriteIperm(FILE *fp, dim_t n, dim_t *iperm);
  void PHIDAL_LoadIperm(FILE *fp, dim_t n, dim_t **iperm);*/

void  PhidalHID_Expand(PhidalHID *BL);
void HID_BuildCoarseLevel(flag_t job, PhidalHID *BL, dim_t *perm, dim_t *iperm);
void HID_PatchLevel(PhidalHID *BL);
void HID_Info(FILE *file, PhidalHID *BL);
REAL HID_quality(PhidalHID *BL);
void Improve_Partition(flag_t verbose, dim_t tagnbr, INTL n, INTL *ig, dim_t *jg, dim_t **mapp, dim_t **mapptr, dim_t *ndom, PhidalHID *BL, dim_t *perm, dim_t *iperm);


void Draw_2Dmesh(FILE* fd, int job, int nc, int n, int* ia, int* ja, int *iperm, int ndom, int *mapp, int *mapptr, PhidalHID *BL);
void HIPS_XPartition(int tagnbr,                    
		     int nodenbr, int* nodelist,    
		     int n, int* ia, int* ja,       
		     int* ndom_out,                 
		     int** mapptr_out, int** mapp_out,
		     int* mask, int maskval);

void export_2Dmesh(FILE* fd, int n, int* ia, int* ja, int* xx, int* yy, int* color, int nodenbr, int* nodelist);

#endif

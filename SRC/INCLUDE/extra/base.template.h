#ifndef _BASE_HEADER__PRE_
#define _BASE_HEADER__PRE_

#include "base_tmp.h" /* TODO */

/************************************************************/
/* BLAS C Interfaces                                        */
/*                                                          */
/* Example of interface to use :                            */
/*  BLAS_DDOT(int n, double *x, int incx, double *y,        */
/*                                              int incy);  */
/*                                                          */
/* For portability you *must* always use variables in       */
/*   arguments : int UN=1; BLAS_DDOT(n, x, UN, y, UN);      */
/*                                                          */
/************************************************************/

/************************************************************/
/* $Id: base.template.h,v 1.1 2006/06/01 11:55:43 gaidamou Exp $ */
/************************************************************/

/************************************************************/
/* Help to read this file :                                 */
/*                                                          */
/* If C interface is avaible on the architecture :          */
/*  BLAS_DDOT is redefined as ddot/ddot_/...                */ 
/*                                                          */
/* Else                                                     */
/*  BLAS_DDOT macro calls BLAS_FDDOT and converts argument  */
/*            types for the FORTRAN interface.              */
/*  BLAS_FDDOT is redefined as ddot/ddot_/...               */
/*                                                          */
/* For CBLAS, BLAS_DDOT macro calls cblas_ddot.             */
/************************************************************/

#ifndef DEBUG_NOBLAS

_INCLUDE_

/********************** CBLAS ATLAS *************************/
#if defined(CBLAS)
#define _C_INTERFACE_
#include <cblas.h>

/********************** SP2 RS6000 **************************/
#elif defined(SP2) || defined(RS6000)
#define _C_INTERFACE_
#include <essl.h>
/*  EX : #define BLAS_DDOT ddot */
_TYPE_UP_LOW_IBM

/********************** SGI *********************************/
#elif defined(SGI)
#define _C_INTERFACE_
#include <scsl_blas.h>
/*  EX : #define BLAS_DDOT ddot */
_TYPE_UP_LOW_

/********************** CRAY ********************************/
#elif defined(CRAY)
/*  EX : #define BLAS_FDDOT DDOT */
_TYPE_FUP_UP_

/********************** LINUX *******************************/
#elif defined(LINUX)
/* EX : #define BLAS_FDDOT ddot_ */
_TYPE_FUP_LOWT_
#endif /* defined(..arch..) */

#ifndef _C_INTERFACE_
/* FORTRAN subroutine declarations */
/* EX : extern double BLAS_FDDOT(int *n, double *x, int *incx, double *y, int *incy);  */
_FORTRAN_DECLARATION_

/* Call FORTRAN subroutines by using C like interface */
/* EX : #define BLAS_DDOT(n,x,incx,y,incy) BLAS_FDDOT(&(n),(x),&(incx),(y),&(incy)) */
_FORTRAN_MACRO_
#endif /* _C_INTERFACE_ */

#if defined(CBLAS)
/* Translate common blas interface to cblas interface */
/* #define   BLAS_DDOT(N, X, incX, Y, incY) \
      cblas_ddot((N), (X), (incX), (Y), (incY)) */
_CBLAS_MACRO_
#endif /* defined(CBLAS) */

_CESSL_MACRO_

#else /* DEBUG_NOBLAS */
/* For debug purpose : Skip every BLAS calls */
/* #define   BLAS_DDOT(N, X, incX, Y, incY)  1 */
_TYPE_UP_NULL_
#endif /* DEBUG_NOBLAS */

#endif /* _BASE_HEADER__PRE_ */


#!/usr/bin/perl -w

use strict;

open(FOUT, ">../base.h");
print FOUT '/* @authors J. GAIDAMOUR, P. HENON */'."\n\n";
close FOUT;

################################################################

{
    my $inputs  = 'blas.double.template.h';
    my $inputs2 = 'base.template.h';
    my $out     = ">>../base.h";
#   my $out     = ">../base.double.h";
    
    my $prev = 'BLAS_';
    my $pre = 'd';
    my $type = 'double';
    my $coef = "$type";
    
    my $include = '';
    
    generate($inputs, $inputs2, $out, $prev, $pre, $type, $coef, $include);
}


################################################################

{
    my $inputs  = 'blas.double.template.h';
    my $inputs2 = 'base.template.h';
    my $out     = ">>../base.h";
#   my $out     = ">../base.float.h";
    
    my $prev = 'BLAS_';
    my $pre = 's';
    my $type = 'float';
    my $coef = "$type";
    
    my $include = '';
    
    generate($inputs, $inputs2, $out, $prev, $pre, $type, $coef, $include);
}

################################################################

{
    my $inputs  = 'blas.complex.template.h';
    my $inputs2 = 'base.template.h';
    my $out     = ">>../base.h";
#   my $out     = ">../base.zcomplex.h";
    
    my $prev = 'BLAS_';
    my $pre = 'z';
    my $type = 'double';
    my $coef = "$type complex";
#   my $coef = "complex $type";
    
    my $include  = "#include <complex.h>\n";
    $include .= "/* GCC http://gcc.gnu.org/onlinedocs/gcc-3.2.3/gcc/Complex.html */\n";
    $include .= "/* lesser than ISO C99 #define complex _Complex double */\n";
	
    generate($inputs, $inputs2, $out, $prev, $pre, $type, $coef, $include);
}

################################################################

{
    my $inputs  = 'blas.complex.template.h';
    my $inputs2 = 'base.template.h';
    my $out     = ">>../base.h";
#   my $out     = ">../base.zcomplex.h";
    
    my $prev = 'BLAS_';
    my $pre = 'c';
    my $type = 'float';
    my $coef = "$type complex";
    
    my $include  = "#include <complex.h>\n";
    $include .= "/* GCC http://gcc.gnu.org/onlinedocs/gcc-3.2.3/gcc/Complex.html */\n";
    $include .= "/* lesser than ISO C99 #define complex _Complex double */\n";#todo float
	
    generate($inputs, $inputs2, $out, $prev, $pre, $type, $coef, $include);
}

################################################################
################################################################
################################################################

sub generate {

my $inputs = $_[0];
my $inputs2 = $_[1];
my $out = $_[2];

my $prev = $_[3];
my $pre = $_[4];
my $type = $_[5];
my $coef = $_[6];

my $include  = $_[7];
   
my @query = 
    ('_ex_','_pre_', '\(', 
     'type', 'coef', 'void',
     '_Order_, ', 
     '_Side_', 
     '_Uplo_', 
     '_Trans_', 
     '_TransA_', 
     '_TransB_', 
     '_Diag_',
     '_M_', '_N_', '_K_', '_KU_', '_KL_',
     '_alpha_', '_beta_', '_ralpha_',
     '_cA_', '_A_', '_lda_', 
     '_cB_', '_B_', '_ldb_', 
     '_C_', '_ldc_',
     '_cX_', '_X_', '_incX_',
     '_cY_', '_Y_', '_incY_',
     '_cAp_',
     '\);'
     );

#todo : int  * en const int * (9) et const double (2) ?
my @query_fortran_declaration = 
    ('extern', $prev.'F'.'_MAJD_'.$pre, '_MAJF_'.'(', 
     $type, $coef, 'void',
     '', 
     'char *Side',
     'char *Uplo', 
     'char *Trans', 
     'char *TransA', 
     'char *TransB', 
     'char *Diag',
     
     'int *M', 'int *N', 'int *K', 'int *KU' , 'int *KL',
     "const $coef* alpha", "const $coef* beta", "const $type* alpha",
     "const $coef *A", "$coef *A", 'int *lda', 
     "const $coef *B", "$coef *B", 'int *ldb', 
     "$coef *C", 'int *ldc',
     "const $coef *X", "$coef *X", 'int *incX',
     "const $coef *Y", "$coef *Y", 'int *incY',
     "const $coef *Ap",
     ');'
     );

my @query_fortran_macro1 = 
    ('', $prev.'_MAJD_'.$pre, '_MAJF_(', 
     '', '', '',
     '', 
     'Side', 
     'Uplo', 
     'Trans', 
     'TransA', 
     'TransB', 
     'Diag',
     'M', 'N', 'K', 'KU', 'KL',
     'alpha', 'beta', 'alpha',
     'A', 'A', 'lda', 
     'B', 'B', 'ldb', 
     'C', 'ldc',
     'X', 'X', 'incX',
     'Y', 'Y', 'incY',
     'Ap',
     ') '
     );

my @query_fortran_macro2 = 
    ('', $prev.'F_MAJD_'.$pre, '_MAJF_(', 
     '', '', '',
     '', 
     '(Side)', 
     '(Uplo)', 
     '(Trans)', 
     '(TransA)', 
     '(TransB)', 
     '(Diag)',
     '&(M)', '&(N)', '&(K)', '&(KU)', '&(KL)',
     '&(alpha)', '&(beta)', '&(alpha)',
     '(A)', '(A)', '&(lda)', 
     '(B)', '(B)', '&(ldb)', 
     '(C)', '&(ldc)',
     '(X)', '(X)', '&(incX)',
     '(Y)', '(Y)', '&(incY)',
     '(Ap)',
     ')'
     );

my @query_cblas_macro1 = 
    ('', $prev.'_MAJD_'.$pre, '_MAJF_(', 
     '', '', '',
     '', 
     'Side', 
     'Uplo', 
     'Trans', 
     'TransA', 
     'TransB', 
     'Diag',
     'M', 'N', 'K', 'KU', 'KL',
     'alpha', 'beta', 'alpha',
     'A', 'A', 'lda', 
     'B', 'B', 'ldb', 
     'C', 'ldc',
     'X', 'X', 'incX',
     'Y', 'Y', 'incY',
     'Ap',
     ') '
     );

#todo: faire une version avec unitdiag+1
my @query_cblas_macro2 = 
    ('', 'cblas_'.$pre, '(', 
     '', '', '',
     'CblasColMajor, ', 

#     '(Side=="L")?141:142', 
#     '(Uplo=="U")?121:122', 
#     '(Trans =="N")?111:((Trans =="T")?112:113)', 
#     '(TransA=="N")?111:((TransA=="T")?112:113)', 
#     '(TransB=="N")?111:((TransB=="T")?112:113)', 
#     '(Diag=="N")?131:132',

     '(Side=="L")?CblasLeft:CblasRight', 
     '(Uplo=="U")?CblasUpper:CblasLower', 
     '(Trans =="N")?CblasNoTrans:((Trans =="T")?CblasTrans:CblasConjTrans)', 
     '(TransA=="N")?CblasNoTrans:((TransA=="T")?CblasTrans:CblasConjTrans)', 
     '(TransB=="N")?CblasNoTrans:((TransB=="T")?CblasTrans:CblasConjTrans)', 
     '(Diag=="N")?CblasNonUnit:CblasUnit',

     '(M)', '(N)', '(K)', '(KU)', '(KL)',
     '(alpha)', '(beta)', '(alpha)',
     '(A)', '(A)', '(lda)', 
     '(B)', '(B)', '(ldb)', 
     '(C)', '(ldc)',
     '(X)', '(X)', '(incX)',
     '(Y)', '(Y)', '(incY)',
     '(Ap)',
     ')'
     );

# @querycblas = 
#     ('extern', 'cblas_d', '(', 
#      'double', 'todo', 'void',
#      'const enum CBLAS_ORDER Order, ', 
#      'const enum CBLAS_SIDE Side',
#      'const enum CBLAS_UPLO Uplo', 
#      'const enum CBLAS_TRANSPOSE Trans', 
#      'const enum CBLAS_TRANSPOSE TransA', 
#      'const enum CBLAS_TRANSPOSE TransB', 
#      'const enum CBLAS_DIAG Diag',
     
#      'const int M', 'const int N', 'const int K', 'const int KU' , 'const int KL',
#      'const double alpha',  'const double beta',  'const double alpha',
#      'const double *A', 'double *A', 'const int lda', 
#      'const double *B', 'double *B', 'const int ldb', 
#      'double *C', 'const int ldc',
#      'const double *X', 'double *X', 'const int incX',
#      'const double *Y', 'double *Y', 'const int incY',
#      'const double *Ap',
#      ');'
#      );

#todo : cblas macro N U T

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
my @query_cessl_macro1; 
my @query_cessl_macro2; 
my @query_cessl;
my @query_cessl_rien;
my @query_cessl_alpha;
my @query_cessl_beta;

if ($pre eq "z") {

# en premier
@query_cessl = ('_ex_','\);');
@query_cessl_rien  = ('', ')');
@query_cessl_alpha  = ('{ intermed _alpha; _alpha._c99 = alpha;','); }');
@query_cessl_beta  = ('{ intermed _alpha, _beta; _alpha._c99 = alpha; _beta._c99 = beta;','); }');

# ensuite
@query_cessl_macro1 = 
    ('', $prev.'_MAJD_'.$pre, '_MAJF_(', 
     '', '', '',
     '', 
     'Side', 
     'Uplo', 
     'Trans', 
     'TransA', 
     'TransB', 
     'Diag',
     'M', 'N', 'K', 'KU', 'KL',
     'alpha', 'beta', 'alpha',
     'A', 'A', 'lda', 
     'B', 'B', 'ldb', 
     'C', 'ldc',
     'X', 'X', 'incX',
     'Y', 'Y', 'incY',
     'Ap',
     ') '
     );

@query_cessl_macro2 = 
    ('', $prev.'C_MAJD_'.$pre, '_MAJF_(', 
     '', '', '',
     '', 
     '(Side)', 
     '(Uplo)', 
     '(Trans)', 
     '(TransA)', 
     '(TransB)', 
     '(Diag)',
     '(M)', '(N)', '(K)', '(KU)', '(KL)',
     '_alpha._essl', '_beta._essl', 'alpha',
     '(dcmplx*)(A)', '(dcmplx*)(A)', '(lda)', 
     '(dcmplx*)(B)', '(dcmplx*)(B)', '(ldb)', 
     '(dcmplx*)(C)', '(ldc)',
     '(dcmplx*)(X)', '(dcmplx*)(X)', '(incX)',
     '(dcmplx*)(Y)', '(dcmplx*)(Y)', '(incY)',
     '(dcmplx*)(Ap)',
     ');'
     );

}
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

my @inp;
my @inp1;
my @inp2;
my $line;
my @args;
my $i;
my $q;

my $lowcase;
my $upcase;

my $type_up_null;
my $type_up_low;
my $type_up_low_ibm;
my $type_fup_up;
my $type_fup_lowt;

my $type_fortran_macro;
my $query_fortran_macro1;
my $query_fortran_macro2;

my $type_fortran_declaration;

my $type_cblas_macro;
my $type_cblas_macro1;
my $type_cblas_macro2;

my $type_cessl_macro;
my $type_cessl_macro1;
my $type_cessl_macro2;

################################################################
#define DDOT dot_

open(CMD, "<$inputs");
@inp = <CMD>;
close CMD;

foreach $line (@inp)
{

    $line =~ s/_/ /g; $line =~ s/\(/ /g; # _ en ' ' et ( en ' ' pour le split
    @args = split(/ /, $line);

    $lowcase = $pre.$args[6];
    $upcase  = $pre.$args[6]; 
    #$upcase =~ s/^d//g; 
    $upcase = "\U$upcase";

    $type_up_low     .= "#define $prev"."$upcase $lowcase\n";
    if ($pre eq "z") {
	$type_up_low_ibm .= "#define $prev"."C$upcase $lowcase\n";
    } else {
	$type_up_low_ibm .= "#define $prev"."$upcase $lowcase\n";
    }
    $type_fup_up     .= "#define $prev"."F$upcase $upcase\n";
    $type_fup_lowt   .= "#define $prev"."F$upcase $lowcase\_\n";

}

#print $type_up_low;

################################################################
# FORTRAN DECLARATION

open(CMD, "<$inputs");
@inp = <CMD>;
close CMD;

foreach (@inp)
{
    $i=0;
    foreach $q (@query)
    {
	s/$q/$query_fortran_declaration[$i]/g;
	s/_MAJD_(.*)_MAJF_/\U$1/g; #passage en majuscule de ce qui est entre  _MAJD_ et _MAJF_
	$i++;
    }
}

# a simplifier
foreach $line (@inp)
{
    $type_fortran_declaration .= "$line";
}

################################################################
# FORTRAN MACRO

open(CMD, "<$inputs");
@inp1 = <CMD>;
close CMD;

foreach (@inp1)
{
    $i=0;
    foreach $q (@query)
    {
	s/$q/$query_fortran_macro1[$i]/g;
	s/_MAJD_(.*)_MAJF_/\U$1/g; #passage en majuscule de ce qui est entre  _MAJD_ et _MAJF_
	$i++;
    }
}

# FORTRAN MACRO2

open(CMD, "<$inputs");
@inp2 = <CMD>;
close CMD;

foreach (@inp2)
{
    $i=0;
    foreach $q (@query)
    {
	s/$q/$query_fortran_macro2[$i]/g;
	s/_MAJD_(.*)_MAJF_/\U$1/g; #passage en majuscule de ce qui est entre  _MAJD_ et _MAJF_
	$i++;
    }
}

$i=0;
foreach $line (@inp1)
{
    $line =~ s/\n//g;
    $type_fortran_macro .= "#define $line\\\n"." $inp2[$i]";
    $i++;
}

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# DEBUG MACRO
$i=0;
foreach $line (@inp1)
{
    $line =~ s/\n/\\\n/g;
    $type_up_null .= "#define $line 1\n";
    $i++;
}

################################################################
# CBLAS MACRO

open(CMD, "<$inputs");
@inp1 = <CMD>;
close CMD;

foreach (@inp1)
{
    $i=0;
    foreach $q (@query)
    {
	s/$q/$query_cblas_macro1[$i]/g;
	s/_MAJD_(.*)_MAJF_/\U$1/g; #passage en majuscule de ce qui est entre  _MAJD_ et _MAJF_
	$i++;
    }
}

# CBLAS MACRO2

open(CMD, "<$inputs");
@inp2 = <CMD>;
close CMD;

foreach (@inp2)
{
    $i=0;
    foreach $q (@query)
    {
	s/$q/$query_cblas_macro2[$i]/g;
	s/_MAJD_(.*)_MAJF_/\U$1/g; #passage en majuscule de ce qui est entre  _MAJD_ et _MAJF_
	$i++;
    }
}

$i=0;
foreach $line (@inp1)
{
    $line =~ s/\n//g;
    $type_cblas_macro .= "#define $line\\\n"." $inp2[$i]";
    $i++;
}

################################################################
# COMPLEX ESSL MACRO
if ($pre eq "z") {

open(CMD, "<$inputs");
@inp1 = <CMD>;
close CMD;

foreach (@inp1)
{
    $i=0;
    foreach $q (@query)
    {
	s/$q/$query_cessl_macro1[$i]/g;
	s/_MAJD_(.*)_MAJF_/\U$1/g; #passage en majuscule de ce qui est entre  _MAJD_ et _MAJF_
	$i++;
    }
}

# COMPLEX ESSL MACRO2

open(CMD, "<$inputs");
@inp2 = <CMD>;
close CMD;

foreach (@inp2)
{
    if (/_beta_/) {
	$i=0; foreach $q (@query_cessl) { s/$q/$query_cessl_beta[$i]/g; $i++; }
    } else { 
	if (/_alpha_/) {
	    $i=0; foreach $q (@query_cessl) { s/$q/$query_cessl_alpha[$i]/g; $i++; }
	} else {
	    $i=0; foreach $q (@query_cessl) { s/$q/$query_cessl_rien[$i]/g; $i++; }
	}
    }

    $i=0;
    foreach $q (@query)
    {
	s/$q/$query_cessl_macro2[$i]/g;
	s/_MAJD_(.*)_MAJF_/\U$1/g; #passage en majuscule de ce qui est entre  _MAJD_ et _MAJF_
	$i++;
    }
}


$type_cessl_macro = "#if defined(SP2) || defined(RS6000)\n";
$type_cessl_macro .= "/* Solve problems between C99 complex type and ESSL internal complex type */\n\n";
$type_cessl_macro .= "typedef union { dcmplx _essl; complex double _c99; } intermed;\n\n";

$i=0;
foreach $line (@inp1)
{
    $line =~ s/\n//g;
    $type_cessl_macro .= "#define $line\\\n"." $inp2[$i]";
    $i++;
}

$type_cessl_macro .= "#endif\n";

} # ($pre eq "z")
################################################################"

#foreach $line (@inp)
#{
#
#    $line =~ s/\(/ /g;
#    $line =~ s/Order\blabl/ /g;
#    $line =~ s/\);//g;
#    $line =~ s/\,//g;#

#    print "$line";esvdznrm2((P->dim), (dcmplx*)(b), (UN));
 
#    @args = split(/_ap_/, $line);
#    @args = split(/ /, $args[1]);
    
#    foreach $arg (@args)
#    {
#	print "$arg ";
#    }

#}


#matlist = split(/ /, $ma);


################################################################"
open(CMD, "<$inputs2");
@inp = <CMD>;
close CMD;

foreach (@inp)
{
    s/_PRE_/\U$pre/g;
    s/_INCLUDE_/$include/g;
    s/_TYPE_UP_LOW_IBM/$type_up_low_ibm/g;
    s/_TYPE_UP_LOW_/$type_up_low/g;
    s/_TYPE_FUP_UP_/$type_fup_up/g;
    s/_TYPE_FUP_LOWT_/$type_fup_lowt/g;

    s/_TYPE_UP_NULL_/$type_up_null/g;

    s/_FORTRAN_DECLARATION_/$type_fortran_declaration/g;
    s/_FORTRAN_MACRO_/$type_fortran_macro/g;

    s/_CBLAS_MACRO_/$type_cblas_macro/g;

    if ($pre eq "z") {
	s/_CESSL_MACRO_/$type_cessl_macro/g;
    } else {
	s/_CESSL_MACRO_//g;
    }

#hack
    s/zdnrm2/dznrm2/g;
    s/ZDNRM2/DZNRM2/g;
    s/cdnrm2/scnrm2/g;
    s/CDNRM2/SCNRM2/g;

    s/cdscal/csscal/g;
    s/CDSCAL/CSSCAL/g;

}

open(FOUT, $out);
print FOUT @inp;
close FOUT;

}

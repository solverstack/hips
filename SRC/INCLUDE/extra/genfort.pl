#!/usr/bin/perl

###############################################################################
#
#   Script: genfort.pl
#
###############################################################################
#
#   A scrip that converts <murge.h> file into a Fortran include file.
#
#   Usage:
#    > ./genfort.pl -f murge.h
#    >    converts the file murge.h into a fortran header.
#    >     -h          Shows this help
#    >     -c <0,1>    Defines if COEF ar complexes (1) or reals (0)
#    >     -r <size>   Defines COEF/REAL kind (4 or 8 usually)
#    >     -s <size>   Defines INTS kind (4 or 8 usually)
#    >     -l <size>   Defines INTL kind (4 or 8 usually)
#
#   Authors:
#     Xavier Lacoste - lacoste@labri.fr
#
###############################################################################
use POSIX;
use strict;
use Getopt::Std;

###############################################################################
# Group: Variables

#
#   integer: real
#     Kind of the reals/complex
#   
#   integer: ints
#     Kind of the INTS
#
#   integer: intl
#     Kind of the INTL
#
#   integer: complex
#     Indicate if COEFs are complex or real
#
#   string: fichier
#     Path to the <murge.h> file
#
#   string: format
#     Format to print PARAMETERs.
#
#   hash: opts
#     Options given to the script
my $real     = 0;
my $ints     = 0;
my $intl     = 0;
my $complex  = 0; 
my $fichier;
my $format = "INTS, PARAMETER :: %-30s = %d\n";
my %opts;

###############################################################################
# Group: Functions

#
# Function: Usage
#
# Prints usage.
#
sub Usage {

    print "./genfort.pl -f hips.h\n";
    print "  converts the file murge.h into a fortran header.\n";
    print "   -h          Shows this help\n";
    print "   -c <0,1>    Defines if COEF ar complexes (1) or reals (0)\n";
    print "   -r <size>   Defines COEF/REAL kind (4 or 8 usually)\n";
    print "   -s <size>   Defines INTS kind (4 or 8 usually)\n";
    print "   -l <size>   Defines INTL kind (4 or 8 usually)\n";
}
#
# Function: printTab
# 
# Print *chaine* with *tabcount* indentations.
#
# If *comm* is 0, it will also replace INTS, INTL, REAL and COEF by the 
# Correct value.
#
# Parameters:
#   chaine   - String to print
#   tabcount - Number of indentations to add.
#   comm     - Indicate if we are in a comments section.
#
sub printTab # ($chaine, $tabcount, $comm)
  {
    my ($chaine, $tabcount, $comm) = @_;
    for (my $i = 0; $i < $tabcount; $i++)
    {
	$chaine = sprintf("   %s",$chaine);
    }
    if ($comm == 0)
    {
	if ($ints != 0)
	{
	    $chaine =~ s/INTS,/INTEGER(KIND=$ints),/g;
	}
# 	else
# 	{
# 	    $chaine =~ s/INTS,/INTEGER,/g;
# 	}
	if ($intl != 0)
	{
	    $chaine =~ s/INTL,/INTEGER(KIND=$intl),/g;
	}
#	else
#	{
#	    $chaine =~ s/INTL,/INTEGER,/g;
#	}
	if ($complex)
	{
	    if ($real != 0)
	    {
		$chaine =~ s/COEF,/COMPLEX(KIND=$real),/g;
		$chaine =~ s/REAL,/REAL(KIND=$real),   /g;
	    }
       else
	    {
#		$chaine =~ s/COEF,/COMPLEX,/g;
		$chaine =~ s/REAL,/REALF,   /g;
	    }
	}
	else
	{
	    if ($real != 0)
	    {
		$chaine =~ s/COEF,/REAL(KIND=$real),   /g;
		$chaine =~ s/REAL,/REAL(KIND=$real),   /g;
	    }
	    else
	    {
#		$chaine =~ s/COEF,/REAL,   /g;
		$chaine =~ s/REAL,/REALF,   /g;
	    }
	}
	$chaine =~ s/MPI_COMM,/MPI_COMM,           /g; 
	$chaine =~ s/CHARACTER\(len=\*\),/CHARACTER(len=*),           /g; 
    }
    print $chaine;

}
#
# Function: Convert 
#
# Main function.
#
# Converts the header <hips.h> file into a Fortran include template.
#
sub Convert {

    my $startcom  = 0;
    my $startenum = 0;
    my $countenum;
    my $chaine;
    my $tabcount = 0;

    $chaine = "INTERFACE\n";
    printTab($chaine, $tabcount);
    $tabcount = 1;
    open (APIc, $fichier); 
    
    foreach (<APIc> )
    {
	if ($startcom == 0)
	{
	    if ($startenum == 0)
	    {
		if (/\/\*(.*)\*\//)
		{
		    $startcom = 0;
		    $chaine = sprintf("! %s\n", $1, 1);
		    printTab( $chaine, $tabcount);
		}
		elsif (/\/\*(.*)/)
		{
		    $startcom = 1;
		    $chaine = sprintf("! %s\n", $1, 1);
		    printTab( $chaine, $tabcount);
		}
		elsif(/enum/)
		{
		    $startenum = 1;
		    $countenum  = 0;		    
		    #$countenum = $countenum + 1 if (/PARAM/);
		    
		}
		

	    }
	    else 
	    {
		if (/}/)
		{
		    $startenum = 0;
		    $countenum  = 0;
		}
		elsif(/[ ]*([^ ]*)[ ]*=[ ]*([0-9]*),?/)
		{
		    $countenum = $2;
		    my $key       = $1;
		    #$countenum = $countenum + 1 if ($key =~ /PARAM/);
		    $chaine = sprintf($format, $key, $countenum);
		    printTab($chaine,$tabcount, 0);
		    $countenum++;
		}
		elsif(/[ ]*([^ |^,]*)[ ]*,?/)
		{
		    my $key = $1;
		    chomp $key;
		    $chaine = sprintf($format, $key, $countenum);
		    printTab($chaine,$tabcount, 0);
		    $countenum++;
		}
		
		
		    
	    }
	}
	else
	{ 
	    if (/^[ ]*> (.*)/)
	    {
		$chaine = sprintf("%s\n", $1);
		printTab($chaine, $tabcount, 0);
	    }
	    elsif (/(.*)\*\//)
	    {
		$startcom = 0;
		$chaine = sprintf("! %s\n", $1);
		printTab($chaine, $tabcount, 1);
	    }
	    elsif(/(.*)/)
	    {

		
		$chaine = sprintf("! %s\n", $1);
		if (/HIPS's constants/)
		{
		    my $chaine2 = "END INTERFACE\n\n";
		    $tabcount --;
		    printTab($chaine2, $tabcount);
		}

		printTab($chaine, $tabcount, 1);
	    }

	}
      
	
    }
    close APIc;
}


getopts("hf:c:r:s:l:",\%opts);

if ( defined $opts{c} ){
    $complex = $opts{c};
}
if ( defined $opts{r} ){
    $real = $opts{r};
}
if ( defined $opts{s} ){
    $ints = $opts{s};
}
if ( defined $opts{l} ){
    $intl = $opts{l};
}

if ( defined $opts{f} ){
    $fichier = $opts{f};
}
else {
  Usage();
  exit;
}

if ( defined $opts{h} ){
    Usage();
    exit;
}

Convert();

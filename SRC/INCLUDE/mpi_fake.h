/* @authors J. GAIDAMOUR, P. HENON */

#ifndef MPI_FALSE_H
#define MPI_FALSE_H

#define MPI_Datatype int
#define MPI_Op int
#define MPI_Request int
#define MPI_Aint int
#define MPI_Comm int

#define MPI_SUCCESS 1
#define MPI_COMM_WORLD 0
#define MPI_BYTE 1
#define MPI_ANY_SOURCE 2
#define MPI_ANY_TAG 3
#define MPI_REQUEST_NULL 4
#define MPI_UNDEFINED 5
#define MPI_LONG 6
#define MPI_DOUBLE 7
#define MPI_SUM 8
#define MPI_MAX 9
#define MPI_INT 10
#define MPI_BOTTOM NULL
#define MPI_THREAD_MULTIPLE 11
#define MPI_THREAD_SINGLE 12
#define MPI_THREAD_FUNNELED 13
#define MPI_THREAD_SERIALIZED 14

#ifndef FLOAT
#define FLOAT REAL
#endif
#ifndef INT
#define INTL int
#endif

typedef void (MPI_User_function) ( void *, void *, int *, MPI_Datatype * );

typedef struct MPI_Status{
  int MPI_SOURCE;
  int MPI_TAG;
  int MPI_ERROR;
} MPI_Status;

#define MPI_Send(buf, count, datatype, dest, tag, comm)
#define MPI_Recv(buf, count, datatype, source, tag, comm, status)
#define MPI_Isend(buf, count, datatype, dest, tag, comm, request)
#define MPI_Wait(request, status)
#define MPI_Test(request, flag, status)
#define MPI_Testany(count, array_of_requests, index, flag, array_of_statuses)
#define MPI_Iprobe(source, tag, comm, flag, status)
#define MPI_Type_contiguous(count, oldtype, newtype)
#define MPI_Type_struct(count, array_of_blocklengths, array_of_displacement, oldtype, newtype)
#define MPI_Address(location, newtype)
#define MPI_Type_commit(datatype)
#define MPI_Type_free(datatype)
#define MPI_Barrier(comm)
#define MPI_Op_create(function, commute, op)
#define MPI_Init(argc, argv)
#define MPI_Finalize()

#define MPI_Get_count(status, datatype ,count) *(count) = 0;
#define MPI_Comm_size(comm, size) *(size)=1;
#define MPI_Comm_rank(comm, rank) *(rank)=0;

#define MPI_Allreduce(sendbuf, recvbuf, count, datatype, op, comm) if(datatype==MPI_DOUBLE){memcpy(recvbuf,sendbuf,count*sizeof(FLOAT));}else{if(datatype == MPI_INT)memcpy(recvbuf,sendbuf,count*sizeof(INTL));}

#define MPI_Init_thread(argc, argv,required,provided) 

#endif /* MPI_FALSE_H */

/* @authors P. HENON */

#ifndef PHIDAL_PARALLEL_H
#define PHIDAL_PARALLEL_H
#include <mpi.h>

#include "hips_define.h"

#include "phidal_ordering.h"
#include "phidal_sequential.h"
#include "phidal_parallel_struct.h"


/**OIMBE  Temporaire a remettre dans block.h **/
#include "symbol.h"


int hazardous(int n, int seed);

INTS PHIDAL_DistrSolve( PhidalDistrMatrix *A, PhidalDistrPrec *P, PhidalDistrHID *DBL, PhidalOptions *option, COEF *rhs, COEF * x, dim_t *itertab, REAL *resnormtab);
void SymbolMatrix_GetLocal(SymbolMatrix *symbglob, SymbolMatrix *symbloc, PhidalHID *globBL, PhidalDistrHID *DBL);
void SymbolMatrix_FillCheck(int n, INTL *ia, dim_t *ja, SymbolMatrix *symbmtx);
void SymbolMatrix_Extract(SymbolMatrix *symbin, int *mask, SymbolMatrix *symbout);
void PHIDAL_DistrICCT(flag_t job, PhidalDistrMatrix *A, PhidalDistrMatrix *L, COEF *D, REAL droptol, REAL *droptab, REAL fillrat, PhidalDistrHID *DBL, PhidalOptions *option, PrecInfo *info);
void PHIDAL_DistrILUCT(flag_t job, PhidalDistrMatrix *A, PhidalDistrMatrix *L, PhidalDistrMatrix *U, REAL droptol, REAL *droptab, REAL fillrat, PhidalDistrHID *DBL, PhidalOptions *option, PrecInfo *info);


void PhidalDistrPrec_GEMM_ICCT(int_t levelnum, REAL *droptab, PhidalDistrMatrix *G, PhidalDistrMatrix *A, COEF* DG, 
			       PhidalDistrPrec *P, PhidalDistrHID *BL, PhidalOptions *option);
void PhidalDistrPrec_GEMM_ILUCT(int_t levelnum, REAL *droptab, PhidalDistrMatrix *G, PhidalDistrMatrix *W, PhidalDistrMatrix *A, PhidalDistrPrec *P, PhidalDistrHID *BL, PhidalOptions *option);

void PhidalDistrHID_Setup(mpi_t proc_id, mpi_t nproc, mpi_t master, PhidalHID *BL, PhidalDistrHID *DBL, dim_t *iperm, MPI_Comm mpicom);
void PhidalDistrMatrix_Setup(dim_t tli, dim_t tlj, dim_t bri, dim_t brj, char *UPLO, char *DIAG, int_t locally_nbr, PhidalDistrMatrix *m, PhidalDistrHID *DBL);

void PhidalDistrMatrix_ColMult2(REAL *vec, PhidalDistrMatrix *A, PhidalDistrHID *DBL);
void PhidalDistrMatrix_RowMult2(REAL *vec, PhidalDistrMatrix *A, PhidalDistrHID *DBL);

void PhidalDistrMatrix_Copy(PhidalDistrMatrix *A, PhidalDistrMatrix *B, PhidalDistrHID *DBL);
void PhidalDistrMatrix_Cut(PhidalDistrMatrix *A, PhidalDistrMatrix *B, PhidalDistrHID *DBL);
void PhidalDistrMatrix_CleanNonLocalBLock(PhidalDistrMatrix *M);
void PhidalDistrMatrix_Build(flag_t symmetric, char *UPLO, csptr A, PhidalDistrMatrix *M, PhidalDistrHID *DBL);
void PHIDAL_SetMatrixCoef(flag_t op, flag_t overlap_add, flag_t numflag, dim_t n, INTL *ia, dim_t *ja, COEF *ma, dim_t *nodelist,
			  flag_t symmetric,  flag_t rsa, PhidalDistrMatrix *A, PhidalDistrHID *DBL);
void PhidalDistrMatrix_GatherCoef(flag_t job, int op, PhidalDistrMatrix *M, PhidalDistrHID *DBL);
int PhidalDistrMatrix_Check(PhidalDistrMatrix *DM, PhidalDistrHID *DBL);
void PhidalDistrMatrix_ICCT(flag_t job, PhidalDistrMatrix *L, COEF *D, REAL droptol, REAL *droptab, REAL fillrat, PhidalDistrHID *DBL, PhidalOptions *option, PrecInfo *info);
void PhidalDistrMatrix_ICCT_Restrict(int START, flag_t job, PhidalDistrMatrix *L, COEF *D, REAL droptol, REAL *droptab, REAL fillrat, PhidalDistrHID *DBL, PhidalOptions *option, PrecInfo *info);
void PhidalDistrMatrix_ILUCT_Restrict(int START, flag_t job, PhidalDistrMatrix *L, PhidalDistrMatrix *U, REAL droptol, REAL *droptab, REAL fillrat, PhidalDistrHID *DBL, PhidalOptions *option, PrecInfo *info);
void PhidalDistrMatrix_ILUCT(flag_t job, PhidalDistrMatrix *L, PhidalDistrMatrix *U, REAL droptol, REAL *droptab, REAL fillrat, PhidalDistrHID *DBL, PhidalOptions *option, PrecInfo *info);
void PhidalDistrMatrix_Transpose(PhidalDistrMatrix *DM);
int PhidalDistrMatrix_NB(PhidalDistrMatrix *A, MPI_Comm mpicom);
int PhidalDistrMatrix_NumberContrib(PhidalDistrMatrix *L, PhidalDistrHID *DBL);
void PhidalDistrMatrix_RowNorm2(PhidalDistrMatrix *DA, PhidalDistrHID *DBL, REAL *normtab);
void PhidalDistrMatrix_ColNorm2(PhidalDistrMatrix *DA, PhidalDistrHID *DBL, REAL *normtab);

void PhidalFactComm_Setup(PhidalFactComm *FC, PhidalDistrMatrix *M, PhidalDistrHID *DBL);

void send_diagonal(COEF *diag,  int size, int indnum, PhidalFactComm *FC);
void send_matrix(csptr mat, int indnum, mpi_t tag, PhidalFactComm *FC);
mpi_t buffer_size(int nnzB, int nbcoli, int nbcolj, REAL commratio);
void add_contrib(csptr mat, BlockComm *bc, csptr cstab, int *tmpj, COEF *tmpa, int *jrev);
void gather_contrib(csptr mat, BlockComm *bc, csptr cstab, int *tmpj, COEF *tmpa, int *jrev);
int pack_matrix(csptr mat, dim_t *index_buff, COEF *coeff_buff, int_t *index_size_packed, int_t *coeff_size_packed, int_t limisize);
void unpack_matrix(csptr amat, dim_t *index_buff, COEF *coeff_buff);
int receive_contrib(flag_t mode, csptr mat, int indnum, PhidalFactComm *FC);
int receive_gather(flag_t mode, csptr mat, int indnum, PhidalFactComm *FC);
int receive_matrix(flag_t mode, csptr mat, int indnum, PhidalFactComm *FC);
void poste_block_receive(int indnum, mpi_t tag, PhidalFactComm *FC);
void PhidalFactComm_PosteCtrbReceive(PhidalFactComm *FC, PhidalDistrMatrix *M, dim_t tlj, dim_t brj);
void PhidalFactComm_PosteCtrbReceiveMem(PhidalFactComm *FC, PhidalDistrMatrix *D);
void PhidalFactComm_PosteCtrbReceiveLU(PhidalFactComm *FCL, PhidalFactComm *FCU, 
				       PhidalDistrMatrix *DL, PhidalDistrMatrix *DU,
				       dim_t tli, dim_t bri);
void PhidalFactComm_PosteCtrbReceiveLUMem(PhidalFactComm *FCL, PhidalFactComm *FCU, 
				       PhidalDistrMatrix *DL, PhidalDistrMatrix *DU);



void PhidalCommVec_Setup(flag_t job, PhidalDistrMatrix *DM, PhidalCommVec *commvec, PhidalDistrHID *DBL);
void PhidalCommVec_ReceiveVecAdd(flag_t job, int num, COEF *y, PhidalCommVec *commvec);

void PhidalDistrMatrix_MatVec(flag_t job, PhidalDistrMatrix *DM, PhidalDistrHID *DBL, COEF *x, COEF *y);
void PhidalDistrMatrix_MatVecAdd(flag_t job, PhidalDistrMatrix *DM, PhidalDistrHID *DBL, COEF *x, COEF *y);
void PhidalDistrMatrix_MatVecSub(flag_t job, PhidalDistrMatrix *DM, PhidalDistrHID *DBL, COEF *x, COEF *y);

void PhidalDistrPrec_SchurProd(PhidalDistrPrec *P, PhidalDistrHID *BL, COEF *x, COEF *y);

void PhidalDistrMatrix_Lsolve(flag_t unitdiag, PhidalDistrMatrix *DL, COEF *x, COEF *b, PhidalDistrHID *DBL);
void PhidalDistrMatrix_Usolve(flag_t unitdiag, PhidalDistrMatrix *DL, COEF *x, COEF *b, PhidalDistrHID *DBL);

void CellCSDistr_ListUpdate(CellCSDistr *celltab, int num,  int n,  dim_t *ja);
void CellCSDistr_ListCopy(CellCSDistr *src, CellCSDistr *dest,  int n,  dim_t *ja);
void CellCSDistr_GetCSList(int num, CellCSDistr *celltab, int n,  dim_t *ja, csptr *ma, int *nnb, csptr *csrtab1, csptr *csrtab2);
void CellCSDistr_IntersectList(mpi_t proc_id, CellCSDistr *celltab, 
			       int ncol,  int *colja, int *colpind, csptr *colma, INTL *pset_index1, int *pset1,
			       int nrow, int *rowja,  INTL *pset_index2, int *pset2,
			       int *nnztab, csptr *listA, csptr *listB);


COEF dist_ddot(mpi_t proc_id, COEF *x, COEF *y, dim_t tli, dim_t bri, PhidalDistrHID *DBL);
int choose_leader(int l1, mpi_t *pset1, int l2, mpi_t *pset2);

INTS HIPS_DistrFgmresd_PH_PH(flag_t verbose, REAL tol, dim_t itmax, 
			    PhidalDistrMatrix *DA, PhidalDistrPrec *P,
			    PhidalDistrHID *DBL, PhidalOptions *option, 
			     COEF *rhs, COEF * x, FILE *fp, dim_t *itertab, REAL *resnormtab);

INTS HIPS_DistrFgmresd_PH(flag_t verbose, REAL tol, dim_t itmax, 
			 PhidalDistrPrec *P, 
			 PhidalDistrHID *DBL, PhidalOptions *option, 
			  COEF *rhs, COEF * x, FILE *fp, dim_t *itertab, REAL *resnormtab);

INTS HIPS_DistrPCG_PH_PH(flag_t verbose, REAL tol, dim_t itmax, 
			    PhidalDistrMatrix *DA, PhidalDistrPrec *P,
			    PhidalDistrHID *DBL, PhidalOptions *option, 
			 COEF *rhs, COEF * x, FILE *fp, dim_t *itertab, REAL *resnormtab);

INTS HIPS_DistrPCG_PH(flag_t verbose, REAL tol, dim_t itmax, 
			 PhidalDistrPrec *P, 
			 PhidalDistrHID *DBL, PhidalOptions *option, 
		      COEF *rhs, COEF * x, FILE *fp, dim_t *itertab, REAL *resnormtab);


void PhidalDistrMatrix_BuildVirtualMatrix(dim_t tli, dim_t tlj, dim_t bri, dim_t brj, PhidalDistrMatrix *DA, 
					  PhidalDistrMatrix *DV, PhidalDistrHID *DBL);

void PHIDAL_DistrPrecond(PhidalDistrMatrix *DA, PhidalDistrPrec *P, PhidalDistrHID *DBL, PhidalOptions *option);
INTS PHIDAL_DistrPrecSolve(REAL tol, PhidalDistrPrec *P, COEF *x, COEF *b, PhidalDistrHID *DBL, PhidalOptions *option, dim_t *itertab, REAL *resnormtab);

void PHIDAL_DistrMLICCT(PhidalDistrMatrix *DA, PhidalDistrPrec *P, PhidalDistrHID *DBL, PhidalOptions *option);
void  PhidalDistrPrec_MLICCT(int_t levelnum, int forwardlev, REAL *droptab, PhidalDistrMatrix *DA, PhidalDistrPrec *P, PhidalDistrHID *DBL, PhidalOptions *option);
void  PhidalDistrPrec_MLICCTForward(int_t levelnum, REAL *droptab, PhidalDistrMatrix *DA, PhidalDistrPrec *P, PhidalDistrHID *DBL, PhidalOptions *option);

void PhidalHID_MapDomains(dim_t nproc, PhidalHID *BL, dim_t *dom2proc);
void PhidalDistrHID_MapDomains(mpi_t proc_id, mpi_t nproc, mpi_t master, PhidalHID *BL, PhidalDistrHID *DBL, MPI_Comm mpicom);void PhidalDistrHID_GenereLocalHID(mpi_t procnum, PhidalHID *BL, PhidalDistrHID *DBL, dim_t *iperm);

void PHIDAL_DistrMLILUT(PhidalDistrMatrix *DA, PhidalDistrPrec *P, PhidalDistrHID *DBL, PhidalOptions *option);
void  PhidalDistrPrec_MLILUT(int_t levelnum, int forwardlev, REAL *droptab, PhidalDistrMatrix *DA, PhidalDistrPrec *P, PhidalDistrHID *DBL, PhidalOptions *option);
void  PhidalDistrPrec_MLILUTForward(int_t levelnum, REAL *droptab, PhidalDistrMatrix *DA, PhidalDistrPrec *P, PhidalDistrHID *DBL, PhidalOptions *option);

void PhidalDistrPrec_SymmetricUnscale(REAL *scaletab, REAL *iscaletab, PhidalDistrPrec *P, PhidalDistrHID *BL);
void PhidalDistrPrec_UnsymmetricUnscale(REAL *scalerowtab, REAL *iscalerowtab, REAL *scalecoltab, REAL *iscalecoltab, PhidalDistrPrec *P, PhidalDistrHID *DBL);
void PhidalDistrMatrix_UnsymScale(int nbr, PhidalDistrMatrix *DA, PhidalDistrHID *DBL, REAL *scalerow, REAL *scalecol, REAL *iscalerow, REAL *iscalecol);
void PhidalCommVec_PrecSetup(PhidalDistrPrec *P, PhidalDistrHID *DBL);
void PhidalDistrMatrix_GetUdiag(PhidalDistrMatrix *U, COEF *diag);
void PhidalDistrMatrix_SetUdiag(PhidalDistrMatrix *U, COEF *diag);

void PhidalDistrMatrix_Clean(PhidalDistrMatrix *a);
void PhidalDistrPrec_Info(PhidalDistrPrec *p);
long PhidalDistrMatrix_NNZ(PhidalDistrMatrix *M);
long PhidalDistrMatrix_NNZ_All(PhidalDistrMatrix *A, MPI_Comm mpicom);
long PhidalDistrPrec_NNZ_All(PhidalDistrPrec *p, MPI_Comm mpicom);
long PhidalDistrPrec_NNZ(PhidalDistrPrec *p);

void PhidalDistrHID_Expand(PhidalDistrHID *DBL);
/* long PhidalDistrPrec_NNZ(PhidalDistrPrec *p); */
void CSR_Reduce(flag_t job, flag_t op, mpi_t master, dim_t n, INTL *ia, dim_t *ja, COEF *a, INTL **iar, dim_t **jar, COEF **ar, MPI_Comm mpicom);
void PhidalDistrVec_Reduce(flag_t op, COEF *y, PhidalDistrMatrix *DM, PhidalDistrHID *DBL);
void PhidalDistrVec_Gather(mpi_t proc_root, COEF *y, COEF *yloc, PhidalDistrHID *DBL);

void PHIDAL_WriteDistrHID(FILE *fp, PhidalDistrHID *DBL);
void PHIDAL_LoadDistrHID(FILE *fp, PhidalDistrHID *DBL);
void GetGlobalConnectorMapping(dim_t nproc, mpi_t *dom2proc, PhidalHID *BL, dim_t *block_psetindex, mpi_t *block_pset);
void GetProcUnknowns(mpi_t procnum, mpi_t *dom2proc, PhidalHID *BL, dim_t *block_psetindex, mpi_t *block_pset, dim_t *iperm, dim_t *unknownnbr,  dim_t **unknownlist);

INTS DistrCSR_Loc2Glob(dim_t ln, dim_t *unknownlist, INTL *lia, dim_t *ja, COEF *a, dim_t n, INTL **ia2R, dim_t **ja2R, COEF **a2R);
#endif

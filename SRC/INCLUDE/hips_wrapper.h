/* @authors J. GAIDAMOUR, P. HENON */
#ifndef HIPS_WRAPPER_H
#define HIPS_WRAPPER_H


#include "block.h"
#include "db_struct.h"
#include "phidal_parallel.h"
#include "db_parallel.h"

#define HIPS_INFOINTNBR 50
#define HIPS_INFOREALNBR 50

struct HIPS_Context{
  PhidalHID BL;
  PhidalDistrHID DBL;
  PhidalOptions options;
  SymbolMatrix *symbmtx, *symbloc;
  PhidalDistrMatrix A;
  /*  PhidalDistrMatrix T;*/

  PhidalDistrPrec Pscal;
  DBDistrPrec Pblock;
  DBPrec      PblockSEQ;
  int_t idmax;
  mpi_t nproc;
  mpi_t proc_id;
  dim_t domsize;
  flag_t partition_type;
  flag_t improve_partition;
  flag_t state;
  flag_t flagrhs;  /** new RHS ? **/
  flag_t flagmat;  /** PhidalMatrix matrix initialized ? **/
  flag_t precond;  /** PhidalMatrix matrix has allready been precond ? **/
  flag_t donotprecond; /** ==1 disable the preconditioning when new matrix coefficient are entered **/
  flag_t assemb_sym; /** use in AssemblyBegin **/
  flag_t hybrid;  /** 0->iterative 1->hybrid **/

  dim_t tagnbr;
  dim_t *perm, *iperm;


  flag_t reorder;
  dim_t ndom;
  dim_t *mapptr;
  dim_t *mapp;
  dim_t ng; /** number of nodes **/
  dim_t na; /** number of unknowns nodes * dof **/
  INTL *ig;
  dim_t *jg;
  COEF *a;

  dim_t *block_psetindex;
  mpi_t *block_pset;

  mpi_t master;
  flag_t coarse_grid;
  flag_t check_graph;
  flag_t check_matrix;
  MPI_Comm mpicom;
  flag_t graph_sym;

  flag_t dumpcsr;

  /*** New Interface ***/
  dim_t ln;
  dim_t dof;
  COEF *x, *b, *t;

  flag_t mode;
  flag_t job, job2;
  LONG ind;
  INTL edgenbr;
  INTL nnz;
  flag_t numflag;
  dim_t *ii;
  dim_t *jj;
  COEF *val;

  /** for grid test **/
  dim_t nc; /** grid size (= one dimension of the square grid) **/
  flag_t cube;

  REAL inforeal[HIPS_INFOREALNBR];
  INTL infoint[HIPS_INFOINTNBR];
};

typedef struct HIPS_Context HIPS_Context;


enum state {INIT=0, GRAPH_BEGIN, GRAPH_END, HIERARCH, SETUP, ASSEMBLY_BEGIN, COEFFICIENT, PRECOND}; 


void PHIDAL_WriteDistrHID(FILE *fp, PhidalDistrHID *DBL);
void PHIDAL_LoadDistrHID(FILE *fp, PhidalDistrHID *DBL);
void PHIDAL_WriteHID(FILE *fp, PhidalHID *BL);
void PHIDAL_LoadHID(FILE *fp, PhidalHID *BL);
void PHIDAL_WriteSymbolMatrix(FILE *fp, SymbolMatrix *symbmtx);
void PHIDAL_LoadSymbolMatrix(FILE *fp, SymbolMatrix* symbmtx);
void PHIDAL_WriteIperm(FILE *fp, dim_t n, dim_t *iperm);
void PHIDAL_LoadIperm(FILE *fp, dim_t n, dim_t **pt_iperm);
int_t PHIDAL_HIDSize(PhidalHID *BL);
INTL* PHIDAL_SerializeHID(INTL *buf, PhidalHID *BL);
INTL* PHIDAL_UnSerializeHID(INTL *buf, PhidalHID *BL);
int_t PHIDAL_SymbolMatrixSize(SymbolMatrix *symbmtx);
INTL* PHIDAL_SerializeSymbolMatrix(INTL *buf, SymbolMatrix *symbmtx);
INTL* PHIDAL_UnSerializeSymbolMatrix(INTL *buf, SymbolMatrix** symbptr);
dim_t PHIDAL_IpermSize(dim_t n, dim_t *iperm);
INTL* PHIDAL_SerializeIperm(INTL *buf, dim_t n, dim_t *iperm);
INTL* PHIDAL_UnSerializeIperm(INTL *buf, dim_t n, dim_t **pt_iperm);



INTS HIPS_Context_Reinit(HIPS_Context *context);
INTS HIPS_Context_Init(HIPS_Context *context);

INTS HIPS_ParallelSetup(INTS id);

INTS HIPS_Precond(INTS id);
INTS HIPS_Solve(INTS id);
INTS HIPS_GraphBuildHID(INTS id);

INTS HIPS_GetGlobalPartitionNbr(INTS id, INTS *domnbr, INTS *partnbr, INTS *domkeynbr);
INTS HIPS_GetGlobalPartition(INTS id, INTS *iperm, INTS *parttab, INTS *domkeyindex, INTS *domkey);
INTS HIPS_GetLocalPartitionNbr(INTS id, INTS *domnbr, INTS *unknownnbr, INTS *partnbr, INTS *partkeynbr);
INTS HIPS_GetLocalPartition(INTS id, INTS *unknownlist,  INTS *parttab, INTS *prockeyindex, INTS *prockey);

INTS Read_options(char *filename,  char *matrix, INTS *sym_pattern, INTS *sym_matrix, char* rhs, INTS* method, INTS *options_int, REAL *options_double);
#endif

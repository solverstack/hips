/* typedef struct PhidalDistrPrec_  PhidalDistrPrec; */
struct PhidalDistrPrec_ {
  flag_t symmetric; 
  dim_t dim; /** The dimension of the system preconditoned **/
  int_t levelnum;
  int_t forwardlev;
  Matrix *LU;
  COEF *D;
  Matrix *EF;
  Matrix *S;
  Matrix *B;
  /*PhidalDistrMatrix *C;  OPTIMREC forward */
  PhidalDistrPrec *prevprec;
  PhidalDistrPrec *nextprec;
  flag_t schur_method;  /** Indicate the smoothing method in the multi-level;
			  0 := no smoothing; in this case the schur complement S is not stored
			  1 := iteration in the schur complement with storage of the schur complement
			  2 := iteration in the schur complement with implicit schur complement (schur not stored) */

  flag_t pivoting;  /** 1 := column pivoting in ILUTP */
  dim_t *permtab;

  /* REAL tol_schur; */
  PrecInfo* info;
};

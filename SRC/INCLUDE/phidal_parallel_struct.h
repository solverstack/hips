/* @authors P. HENON */

#ifndef PHIDAL_PARALLEL_STRUCT_H
#define PHIDAL_PARALLEL_STRUCT_H

/** Macros used in the communication layer **/
/*#define LEAD_TAG 1
#define CTRB_TAG 2
#define DIAG_TAG 3
#define SYNCHRONOUS 0
#define ASYNCHRONOUS 1
#define NOT_READY 0
#define READY 1*/


/** Communication structure used in the parallel matrix vector product and the forward and backward sweep **/
struct PhidalCommVec_ {
  flag_t init;         /* == 1 when it has been set up by PhidalCommVec_Setup */
  mpi_t maxproc;      /* Number of processor involved in the matrix communications */
  mpi_t rqnbr;        /* length of recv_rqtab, send_rqtab ... */

  dim_t *veclen;      /* size of the vector part facing the ith block
		       row of the matrix associated to the commvec */
  int_t *out_ctrb;    /* out_ctrb[i] = number of contributions waited from other processors 
		       to part of solution vector corresponding to
		       block[i][i] (local num) 
		       if out_ctrb[i] == -1 that means the processor
		       is not leader on this part of the vector
		       out_ctrb[i] == 0 does NOT mean that the vector
		       is purely local it just means that their is no
		       non block in the row i that are mapped on
		       others processor  
		    */
  mpi_t **out_proctab; /** out_proctab[i] contains the "out_ctrb[i]"
			  processors that contributes to this vector **/

  int_t *out_cnt;   /* Counter of received contribution */
  
  int_t *loc_ctrb; /** Number of local contribution to add before send
		  **/
  int_t *loc_cnt; /* Counter  of local contribution */
  
  MPI_Request **recv_rqtab; /** Idem than for the send requests but for receives in the forward sweep **/
  
  COEF *t;             /** Vector use to aggregate rhs contribution in the forward/backward sweep 
			     It is also use for the distributed matrix-vector product **/
                              

  COEF **recv_buffer; /** Receive buffers **/
  
  MPI_Comm     mpicom; /* MPI communicator */
  MPI_Status  *status; 


  int_t posted_max; /** maximum number of messages that can simultaneously been posted  **/
  int_t posted;     /** Number of messages currently posted **/
  INTL bufmem_max; /** Maximum memory allowed for MPI Irecv buffer **/
  INTL bufmem; /** Memory currently allowed for MPI Irecv buffer **/

};
typedef struct PhidalCommVec_  PhidalCommVec;


/** Define the block level structure **/
struct PhidalDistrHID_{

  PhidalHID LHID;             /* HID structure of the local domain */
  mpi_t proc_id;
  mpi_t nproc;                   /* number of procs */
  dim_t globn;                      /* dimension of the global matrix */
  mpi_t *node2proc; /** distribution (based on row_leader) of the unknowns **/
  int_t gnblock;                   /* global number of blocks (connectors) */
  dim_t *block_psetindex;         /* the set processors that shares connector i is  */
  mpi_t *block_pset;              /* .. block_pset[ block_psetindex[i]:block_psetindex[i+1]-1 ] */   
  dim_t *orig2loc;               /* global original numbering to local unknown numbering */
  dim_t *loc2orig;               /* local to global original numbering */ /* todo : change orig by glob */
  int_t *loc2glob_blocknum;          /* local to global connector numbering                         */ /* todo : bloCknum*/
  int_t *glob2loc_blocknum;          /* global to local connector numbering                         */ 
  mpi_t *dom2proc;               /* dom2proc[i] = processor on which domain i is mapped         */
  mpi_t *row_leader; /* row_leader[i] is the processor responsible for
		      the vector part corresponding to the ith block_row of the matrix */
  mpi_t adjpnbr; /* Number of processors that share some block with
		       the local processor **/
  mpi_t *adjptab; /* List of adjacent processors **/
  
  MPI_Comm mpicom; /* MPI communicator */
};

typedef struct PhidalDistrHID_ PhidalDistrHID;




struct PhidalDistrMatrix_{
  mpi_t proc_id;
  mpi_t nproc;

  mpi_t *clead; /** Leader of the block **/
  INTL   *cind;  /** Index to find the processor set of a block **/
                /** -1 means the block is only map on this processor **/
  mpi_t *rlead; /** Leader of the block **/
  INTL   *rind;  /** Index to find the processor set of a block **/

  /* Describe the processor set of the blocks */
  INTL *pset_index;
  mpi_t *pset;

  int_t sharenbr; /** Number of block matrices shares by several
		    processors **/
  PhidalMatrix M;
  PhidalCommVec  commvec;     /* Communicator for matrix-vector or
				 triangular solve */
};
/* typedef struct PhidalDistrMatrix_  PhidalDistrMatrix; */



#include "phidal_parallel_prec.h"


struct BlockComm_{
  flag_t posted;  /** flag **/
  mpi_t rqnbr;  /** Number of requests **/
  mpi_t *proctab; /** list of the processor **/
  MPI_Request *irqtab; /** Array of incoming  message requests for
			      coeff. indices**/
  MPI_Request *crqtab; /** Array of incoming message requests
				for coeff. values**/
  /*MPI_Status  *status;*/       /** Status array for receive requests **/


  mpi_t buffsize;     /** Size of a receive buffer **/
  dim_t **ibufftab;       /* Buffers for receive (matrice indices) */
  COEF **cbufftab;       /* Buffers for receive (matrice coefficients) */
};
typedef struct BlockComm_ BlockComm;


/** Define the structure needed to communication during the factorization **/
struct PhidalFactComm_{

  int_t bcomnbr;
  BlockComm *bcomtab;
  mpi_t proc_id;
  mpi_t maxproc; /** maximum number of processor that can share a same
		   block **/

  /** These are temporary vector that are needed to add the
      contribution received for a block **/
  dim_t *tmpj;
  COEF *tmpa;
  dim_t *jrev;
  csptr cstab;
  MPI_Status *status;
  MPI_Comm mpicom; /* MPI communicator */

  /*-------------------------------------------------/
  / PARAMETERS FOR COMMUNICATION                     /
  /   BUFFER MANAGEMENT                              /
  /-------------------------------------------------*/
  REAL comm_memratio;

  PrecInfo* info;

  int_t posted_max; /** maximum number of messages that can simultaneously been posted  **/
  int_t posted;     /** Number of messages currently posted **/
  INTL bufmem_max; /** Maximum memory allowed for MPI Irecv buffer **/
  INTL bufmem; /** Memory currently allowed for MPI Irecv buffer **/
  dim_t colind;
  dim_t rowind;

};
typedef struct PhidalFactComm_ PhidalFactComm;

struct CellCSDistr
{
  dim_t nnz;
  dim_t *ja;
  INTL   *pind;
  csptr *ma;
  
};
typedef struct CellCSDistr CellCSDistr;

void PhidalCommVec_Init(PhidalCommVec *commvec);
void PhidalCommVec_Clean(PhidalCommVec *commvec);
void BlockComm_Init(BlockComm *b, mpi_t srcnbr);
void BlockComm_Clean(BlockComm *b);
void PhidalDistrHID_Init(PhidalDistrHID *DBL);
void PhidalDistrHID_Clean(PhidalDistrHID *DBL);
void PhidalDistrMatrix_Init(PhidalDistrMatrix *a);
void PhidalDistrMatrix_Clean(PhidalDistrMatrix *a);
void PhidalCommVec_Init(PhidalCommVec *commvec);
void PhidalCommVec_Clean(PhidalCommVec *commvec);
void PhidalFactComm_Init(PhidalFactComm *FC);
void PhidalFactComm_Clean(PhidalFactComm *FC);

void PhidalDistrPrec_Init(PhidalDistrPrec *p);
void PhidalDistrPrec_Clean(PhidalDistrPrec *p);
void PhidalDistrPrec_Fake(PhidalDistrPrec* P, PhidalDistrMatrix* L, PhidalDistrMatrix* U, COEF* D, flag_t rsa, PhidalOptions* option);

void PrecInfo_Max(PrecInfo * info, REAL MnnzA, REAL *ratio_max,
		  REAL *peakratio_max, MPI_Comm comm);

#endif

/* @authors P. HENON */
#ifndef SCOTCH_METIS_WRAPPER_H
#define SCOTCH_METIS_WRAPPER_H

/*** wrapper for scotch and metis (solve the problem of int types 32/64) ***/
#ifdef SCOTCH_PART
#define restrict
#ifndef SCOTCH_FIRST_INCLUDE
#define SCOTCH_FIRST_INCLUDE
#include "scotch.h"
#endif

int SCOTCH_graphOrder_WRAPPER( SCOTCH_Graph *  graphdatptr,  SCOTCH_Strat *  stratdatptr, dim_t n, dim_t *perm, dim_t *rperm, dim_t *cblknbr, dim_t *rangtab, dim_t *treetab);

int SCOTCH_graphBuild_WRAPPER(SCOTCH_Graph *  grafdat, flag_t numflag, dim_t n, INTL *ia,  INTL *vwgt, dim_t *ja, INTL *ewgt);

int SCOTCH_graphPart_WRAPPER(dim_t n, SCOTCH_Graph *  grafdat, dim_t ndom,  SCOTCH_Strat *  grafstrat, mpi_t *node2dom);


int SCOTCH_graphPartOvl_WRAPPER( dim_t n, SCOTCH_Graph *  grafdat, dim_t ndom,  SCOTCH_Strat *  grafstrat, mpi_t *node2dom);

#else

#include "metis.h"

void METIS_NodeWND_WRAPPER(dim_t n, INTL *ia, dim_t *ja, INTL *wgt, flag_t numflag, int *options, dim_t *iperm, dim_t *perm);
void METIS_NodeND_WRAPPER(dim_t n,  INTL *ia, dim_t *ja, flag_t numflag, int *options, dim_t *rperm, dim_t *perm);
void METIS_PartGraphKway_WRAPPER(dim_t n, INTL *ia, dim_t *ja, INTL *vwgt, INTL *ewgt, flag_t wgtflag, flag_t numflag, int ndom, int *options, int *edgecutptr, dim_t *mpi2proc);
void METIS_PartGraphRecursive_WRAPPER(dim_t n, INTL *ia, dim_t *ja, INTL *vwgt, INTL *ewgt, flag_t wgtflag, flag_t numflag, int ndom, int *options, int *edgecutptr, mpi_t *dom2proc);
void METIS_PartGraphVKway_WRAPPER(dim_t n, INTL *ia, dim_t *ja, INTL *vwgt, INTL *ewgt, flag_t wgtflag, flag_t numflag, int ndom, int *options, int *edgecutptr, mpi_t *dom2proc);
#endif

#endif

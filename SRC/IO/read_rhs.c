/* @authors J. GAIDAMOUR, P. HENON */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "phidal_common.h"

#define BUFLEN 200

void VECread(char *filename, INTS n, COEF *rhs)
{
  /***********************************************/
  /* Read a vector from the file filename        */
  /* num=1 Fortran numbering                     */
  /* rhs must have been allocated with at least  */
  /* n element                                   */
  /***********************************************/

  FILE *fp;
  char buf[BUFLEN];
  int i;/* , ind; */
  double tmp1;
#ifdef TYPE_COMPLEX
  double tmp2;
#endif

  if((fp = fopen(filename,"r")) == NULL) 
    {
      fprintfd(stderr, "read_rhs: Cannot open file %s \n", filename);
      exit(1);
    }
  i = 0;
  while(fgets(buf, BUFLEN, fp) != NULL) 
    {
      if(i>= n)
	{
	  fprintf(stderr, "WARNING : file %s contains a vector of dimension > n = %ld \n", filename, (long)n);
	  break;
	}
      /*assert(i<n);*/

      /*sscanf(buf, "%d %lf", &ind, &tmp1); Pour IFP **/
#ifdef TYPE_REAL
      sscanf(buf, "%lf", &tmp1);
      rhs[i] = tmp1;
#else
      sscanf(buf, "%lf %lf", &tmp1, &tmp2);
      rhs[i] = tmp1 + I*tmp2;
#endif
      i++;
    }

  if(i<n)
    {
      fprintf(stderr, "ERROR : file %s contains a vector of dimension %ld < n = %ld \n", filename, (long)i, (long)n);
      assert(n == i);
    }
  fclose(fp);

  /*fprintfv(5, stderr, "nnz = %d \n", i);*/
}

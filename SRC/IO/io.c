/* @authors J. GAIDAMOUR, P. HENON */

#include <stdlib.h> /* EXIT_SUCCESS */
#include <string.h> /* strcpy */
#include <assert.h>

#include "io_hb.h"
#include "io_mm.h"      /* pb d'include */
#include "io_ijv.h"
#include "localdefs.h"  /* userread wreadmtc */


#include "hips.h"
#include "phidal_common.h"
#include "io.h" 

/* TODO : tous présenter de la meme manière
   actuellement : 
    - IOHB / IOMM / SPKIT et USER en 2 fonctions
    - les ijk en 1 fonction
*/


#ifndef SPKIT_F
#define DRIVER_HB DRIVER_IOHB; /* default driver */
#else
#define DRIVER_HB DRIVER_SPKIT; /* default driver */
#endif

/*#define RUA_CSR */ /** The matrix rua are stored in CSR format : by default Harwell-Boeing are stored in CSC format **/

/************************************************************************************************************/
/****************************************** READ THE MATRIX *************************************************/
/************************************************************************************************************/

/*   CSRread(matrix, &n, &nnz, &ia, &ja, &a, &sym_pattern, &sym_matrix); */
/*   assert(phidaloptions.symmetric == sym_matrix); */

int CSRread(char* matrix, INTS *t_n, INTL* t_nnz, INTL** t_ia, INTS** t_ja, COEF** t_a, INTS* sym_matrix) {

  Matrix_Read(0, t_n, t_nnz, NULL, NULL, NULL, sym_matrix, matrix); 

  fprintfd(stderr, "Matrix Read n = %ld nnz = %ld \n", (long)(*t_n), (long)(*t_nnz));

  *t_ia = (INTL *)malloc(sizeof(INTL)*((*t_n)+1));
  assert(*t_ia != NULL);
  *t_ja = (INTS *)malloc(sizeof(INTS)* (*t_nnz));
  assert(*t_ja != NULL);
  *t_a = (COEF *)malloc(sizeof(COEF)* (*t_nnz));
  assert(*t_a != NULL);
    
  Matrix_Read(1, t_n, t_nnz, *t_ia, *t_ja, *t_a, sym_matrix, matrix); 
  return EXIT_SUCCESS;
}

void Matrix_Read(INTS job, INTS *t_n, INTL* t_nnz, INTL *ia, INTS *ja, COEF *a, INTS* sym_matrix, char* matrix) {
  dim_t n;
  INTL nnz;
  
  char *filename;
  int driver = strtol(matrix, &filename,10);

  INTS detected_sym_matrix=-1;

  /* Set default */
  if (driver == 0) driver = DRIVER_DEFAULT;   /* default driver */ 

  /*if ((sym_pattern != NULL) && (*sym_pattern == -1)) (*sym_pattern) = 0; *//* default, 
									    we guest that the pattern 
									    is not symmetric */
  if (driver == DRIVER_AUTO) {
    /* Select input driver according to the extension of the matrixfile*/
    char* ext = strrchr(matrix, '.'); 
    if (ext == NULL) {
      fprintferr(stderr,"Please select a driver to read the matrix\n");
      exit(1);
    }
    ext++; /* TODO : check */
    /* fprintf(stdout, "ext = %s\n", ext); */

    if ((! strcmp("rsa", ext)) ||
	(! strcmp("rua", ext)) ||
	(! strcmp("csa", ext)) ||
	(! strcmp("cua", ext))) {
    
      /* fprintf(stdout, "I/O Driver : Harwell-Boeing format\n"); */
      driver = DRIVER_HB;

    } else if ( (! strcmp("mtx", ext)) || (! strcmp("mm", ext))) {
      /* fprintf(stdout, "I/O Driver : MatrixMarket format\n"); */
      driver = DRIVER_IOMM;

    } else{
     
      fprintferr(stderr, "Matrix format not specified : using Harwell Boeing driver by default \n");
      driver = DRIVER_HB;
    }

  }

  switch (driver) {
    
  case DRIVER_IOHB:{

    /************************************************************************************************************/
    /****************************************** IOHB ************************************************************/
    /************************************************************************************************************/

    dim_t m, Nrhs;    
    char *Type;
    
    /* Get information about the array stored in the file specified in the  */
    /* argument list:                                                       */
      
    fprintfd(stderr,"Reading matrix info from %s...\n",filename);
    readHB_info(filename, &m, &n, &nnz, &Type, &Nrhs);
      
    fprintfd(stderr,"Matrix in file %s is %d x %d, with %ld nonzeros with type %s;\n",
	    filename, m, n, (long)nnz, Type);
    /*     fprintfd(stderr,"%d right-hand-side(s) available.\n",Nrhs); */
    assert(n == m);      
    /* Read the matrix information, generating the associated storage arrays  */ 
    fprintfd(stderr,"Reading the matrix from %s...\n",filename);
    if(job == 1)
      {
	/*readHB_newmat_double(filename, &m, &n, &nnz, &ia, &ja, (double**)&a);*/
	readHB_mat_double(filename, ia, ja, (double *)a);
	
	/* int readHB_newmat_char(const char* filename, int* M, int* N, int* nonzeros,  */
	/* 		       int** colptr, int** rowind, char** val, char** Valfmt)     */
	
	/* /\* If a rhs is specified in the file, read one, generating the associate storage *\/ */
	/*   if (Nrhs > 0) { */
	/*     fprintfd(stderr,"Reading right-hand-side vector(s) from %s...\n",argv[1]); */
	/*     readHB_newaux_double(argv[1], 'F', &rhs); */
	/*   } */
	
	
#ifdef RUA_CSR	
	if      (Type[1] == 'S' || Type[1] == 's') detected_sym_matrix = 1;
	else if (Type[1] == 'U' || Type[1] == 'u') detected_sym_matrix = 0;
	else { printf("driver unknow type : %c\n", Type[1]); assert(0); }
#else
	if      (Type[1] == 'S') detected_sym_matrix = 1;
	else if (Type[1] == 'U') 
	  {
	    INTL *ia2;
	    dim_t *ja2;
	    COEF *a2;

	    detected_sym_matrix = 0;
	    /*** In HIPS we need CSR matrix so we have to transpose the matrix since it is a column
		 storage ***/
	    ia2 = ia;
	    ja2 = ja;
	    a2  = a;
	    memcpy(ia2, ia, sizeof(INTL)*(n+1));
	    memcpy(ja2, ja, sizeof(dim_t)*nnz);
	    memcpy(a2, a, sizeof(COEF)*nnz);

	    fprintfd(stderr, "DRIVER_IO_HB \n");
	    fprintfd(stderr, "TRANSPOSE THE MATRIX ia[0] = %ld \n", (long)ia[0]);

	    CSR_Fnum2Cnum(ja2, ia2, n);
	    csr2csc(n, 1, a2, ja2, ia2,  a, ja, ia);
	    CSR_Cnum2Fnum(ja, ia, n);

	    /*fprintfd(stderr, "TRANSPOSE ia[0] = %ld ia[n] = %ld nnz = %ld \n", (long)ia[0], (long)ia[n], (long)nnz);*/

	    free(ia2);
	    free(ja2);
	    free(a2);
	  }
	else assert(0);

#endif

#ifdef TYPE_REAL
	assert(Type[0] == 'R');
#endif
    
#ifdef TYPE_COMPLEX
	/* assert(Type[0] == 'C'); */

	if(Type[0] == 'R') {
	  dim_t i;
	  printferr("*** warning : real matrix, solver compiled for complex\n");
	  REAL* atmp = (REAL*)a;
	  COEF* anew = (COEF *) malloc(nnz*sizeof(COEF));
	  for(i=0;i<nnz; i++) {
	    anew[i] = atmp[i];
	  }
	  free(a);
	  a = anew;
	}

#ifdef DEBUG_M
	{ 
	  dim_t i;
	  REAL* atmp = (REAL*)a;
      
	  for(i=0;i<nnz; i++) {
	    /* printfd("i=%d "_coef_" = %gi%g\n", i, pcoef(a[i]),atmp[i*2],atmp[i*2+1]); */
	    assert(CREAL(a[i]) == atmp[i*2]);
	    assert(CIMAG(a[i]) == atmp[i*2+1]);
	  }
	}
#endif /* DEBUG_M */

#endif
	free(Type);
      }
    break;
  }
   
  case DRIVER_IOMM:{
 
    /************************************************************************************************************/
    /****************************************** IOMM ************************************************************/
    /************************************************************************************************************/
    char *buff;
    dim_t i;
    int ret_code;
    MM_typecode matcode;
    FILE *f;
#ifdef TYPE_COMPLEX
    REAL ax, ay; /* complex */
#endif
    dim_t m;   
    dim_t *ii, *jj;
    COEF *val;
    INTL ind;
    flag_t numflag;

    if ((f = fopen(filename, "r")) == NULL) {
      printferr("Could not read file %s\n", filename);
      exit(1);
    }

    if (mm_read_banner(f, &matcode) != 0)
      {
        printferr("Could not process Matrix Market banner.\n");

	exit(1);
      }

    if      (mm_is_symmetric(matcode))  detected_sym_matrix = 1;
    else if (mm_is_general(matcode))    detected_sym_matrix = 0;
    else assert(0);

    /*  This is how one can screen matrix types if their application */
    /*  only supports a subset of the Matrix Market data types.      */

#ifdef TYPE_COMPLEX
    if ((!mm_is_complex(matcode)) && mm_is_matrix(matcode) && 
	mm_is_sparse(matcode) )
#else
    if ((!mm_is_real(matcode)) && mm_is_matrix(matcode) && 
	mm_is_sparse(matcode) )
#endif
      {

	buff = mm_typecode_to_str(matcode);

        printferr("Sorry, this application does not support ");
        printferr("Market Market type: [%s]\n", buff);
	exit(1);

      }

    buff =  mm_typecode_to_str(matcode);
    printfd("Matrix Market type : [%s]\n", buff);
    free(buff);

    /* find out size of sparse matrix .... */

    if ((ret_code = mm_read_mtx_crd_size(f, &m, &n, &nnz)) !=0)
      exit(1);

    assert(n == m);
    
    if(job == 1)
      {

	/*fprintfd(stderr, "NNZ : %ld\n", (long) nnz); */

	/* TODO : factoriser avec driver ijv ! */

	/* reserve memory for matrices */
	ii = (dim_t *) malloc(nnz * sizeof(dim_t));
	jj = (dim_t *) malloc(nnz * sizeof(dim_t));
	val = (COEF *) malloc(nnz * sizeof(COEF));


	/* NOTE: when reading in doubles, ANSI C requires the use of the "l"  */
	/*   specifier as in "%lg", "%lf", "%le", otherwise errors will occur */
	/*  (ANSI C X3.159-1989, Sec. 4.9.6.2, p. 136 lines 13-15)            */

	for (i=0; i<nnz; i++)
	  {
#ifdef TYPE_COMPLEX
	    int tii, tjj;
	    FSCANF(f, "%d %d "_scan_g_" "_scan_g_"\n", &tii, &tjj, &ax, &ay);
	    ii[i] = (dim_t)tii;
	    jj[i] = (dim_t)tjj;
	    val[i] = ax +I*ay;
#else
	    FSCANF(f, "%d %d "_scan_g_"\n", ii+i, jj+i, val+i);
#endif
	    /* ia[i]--;  /\* adjust from 1-based to 0-based *\/ /\* TODO : BLAS ? *\/ */
	    /* ja[i]--; */
	  }

	fclose(f);

	
	/**** OIMBE : cet transposition n'a pas à etre la (cas non symetrique) ****/
	/*{
	  dim_t* tmp;
	  tmp = ii;
	  ii=jj;
	  jj=tmp;
	  }*/

	/* a factoriser : */
    
	/* a supprimer (la double conversion) */

	numflag = 1;
	for(i=0;i<nnz;i++)
	  if(ii[i] == 0 || jj[i] == 0)
	    {
	      numflag = 0;
	      break;
	    }

	if(numflag == 1)
	  {
	    fprintfd(stderr, "Fortran to C  RENUMBERING \n");
	    for(i=0;i<nnz;i++)
	      {
		ii[i]--;
		jj[i]--;
#ifdef DEBUG_M
		assert(ii[i] >= 0);
		assert(jj[i] >= 0);
#endif
	      }
	  }
    
    
	/** count number of non zero per row **/
	bzero(ia, sizeof(INTL)*(n+1));
	for(i=0;i<nnz;i++)
	  ia[ii[i]+1]++;
    

	for(i=1;i<=n;i++)
	  ia[i] += ia[i-1];

#ifdef DEBUG_M  
	for(i=1;i<=n;i++)
	  assert(ia[i]-ia[i-1] > 0);
#endif
    
	for(i=0;i<nnz;i++)
	  {
	    ind = ia[ii[i]];
	    ja[ind] = jj[i];
	    a[ind] = val[i];
	    ia[ii[i]]++;
	  }
    
    
	for(i=n;i>=1;i--)
	  ia[i] = ia[i-1];
	ia[0] = 0;
    
	/*for(i=ia[0]; i<ia[1];i++)
	  fprintfd(stderr, "(%d %g) ", ja[i], a[i]);
	  exit(0);*/
    
	CSR_Cnum2Fnum(ja, ia, n);
    
	free(ii);
	free(jj);
	free(val);
      }
    /** fin a factoriser */

    break;
  }
    
  case DRIVER_IJV:{
    /************************************************************************************************************/
    /****************************************** IJV *************************************************************/
    /************************************************************************************************************/
    /*read_ijv(filename, &n, &nnz, &ia, &ja, &a);*/
    read_ijv(job, filename, &n, &nnz, ia, ja, a);

    break;
  }
   
  case DRIVER_IJV3FILES:{
    /************************************************************************************************************/
    /****************************************** IJV3FILES *******************************************************/
    /************************************************************************************************************/
    assert(0); /** TO finish **/
    read_ijv3(job, filename, &n, &nnz, ia, ja, a);
    detected_sym_matrix = 0;
    break;
  }
    
  case DRIVER_IAJAMA : {
    /*read_iajama(filename, &n, &nnz, &ia, &ja, &a);*/
    read_iajama(job, filename, &n, &nnz, ia, ja, a);
    detected_sym_matrix = 0;
    break;
  }

 
  case DRIVER_USER:{
   
    /************************************************************************************************************/
    /****************************************** USER ************************************************************/
    /************************************************************************************************************/
#ifdef SPKIT_F
#ifdef TYPE_REAL
    int job2 = 0; 
    int ierr, len = strlen(filename);
    REAL *rhstmp;
    dim_t nrhs = 0;

    /* If the matrix name starts with integer in the input file,
       | User-defined matrix input is provided. The filenames for the
       | user-defined input are stored in the file specified
       | following the integer in the variable "matrix". NOTE: there
       | should be no space between integer and the filename substr
       | contains the name of the file with filenames for the matrix
	 | input
    */
    
    /*read only the sizes of matrix arrays first == call user_read
      with job=0*/
    if(job == 0)
      {
	a = NULL; ja = NULL; ia = NULL; rhstmp = NULL;
      }

    userread(filename,&len,&job2,&n,&nnz,a,ja,ia,&nrhs,rhstmp,&ierr);
    if(job == 1)
      {

	/* read the (a, ja, ia) values call userread with job=1 */
    
	/*   Read matrix; using user-defined function (routine user_read) */
	/************************************************/
	/* READ THE MATRIX FROM DISK (IN USER FORMAT)   */
	/************************************************/
	nrhs = 0; /* To prevent from reading rhs */
	
	userread(filename,&len,&job2,&n,&nnz,a,ja,ia,&nrhs,rhstmp,&ierr);
	if(ierr != 0) {
	  fprintferr(stderr, "cannot read matrix\n");
	  exit(1);
	}
	fprintfd(stdout, "Read matrix %s\n", filename);
      }
#else
    fprintferr(stderr, "The matrix can not be read in complex version with this driver \n");
#endif
#else
    fprintferr(stderr, "Must compile with -DSPKIT_F to use this matrix driver \n");
    assert(0);
#endif
    break;
  }

#ifdef SPKIT_F
  case DRIVER_SPKIT:{

    /************************************************************************************************************/
    /****************************************** SPKIT ***********************************************************/
    /************************************************************************************************************/

#ifdef TYPE_REAL
    
#if defined(INTSIZE64) || defined(INTSSIZE64)
    /* wreadmtc is full 64 bits ... */
    INTL job2 = 0; 
    INTL ierr, len;
    REAL *rhstmp;
    INTL nrhs = 0;    

    INTL* ja64;
    INTL  n64;
#else
    int job2 = 0; 
    int ierr, len;
    REAL *rhstmp;
    int nrhs = 0;
#endif

    /* declarations related to Harwell-boeing format for reading the HB
       matrix. Second part is related to I/O parameters */
    char guesol[2], title[72], key[8], type[3];
    INTL nc, tmp, tmpn,tmpnnz;
    
    /*   Read matrix; using SPARSKIT function for reading Harwell-Boeieng matrices */
    /* variable "matrix" stores the name of the file in HB format
       |
       |   Read a Harwell-Boeing matrix. using wreadmtc c-version of
       |      sparsit routine - call wreadmtc a first time to determine sizes
       |      of arrays. read in values on the second call.
    */
    len = (INTL) strlen(filename);
    if(job == 0)
      {
	a = NULL; ja = NULL; ia = NULL; rhstmp = NULL;
      }


#if defined(INTSIZE64) || defined(INTSSIZE64)
    ja64=NULL;
    wreadmtc64(&tmp,&tmp,&job2,&len,a,ja64,ia,rhstmp,&nrhs,
	     guesol,&n64,&nc,&nnz,title,key,type,&ierr,filename);
    n = n64;
#else
    wreadmtc(&tmp,&tmp,&job2,filename,&len,a,ja,ia,rhstmp,&nrhs,
	       guesol,&n,&nc,&nnz,title,key,type,&ierr);
#endif
    

    /*** Need this for job == 0 **/
    if      (type[1] == 'S' || type[1] == 's') detected_sym_matrix = 1;
    else if (type[1] == 'U' || type[1] == 'u') detected_sym_matrix = 0;
    else { printf("driver unknow type : %c\n", type[1]); assert(0); }
    


    if(job == 1)
      {
	job2 = 2;
	tmpn = n; /* useful ? */
	tmpnnz = nnz;
	/*********************************/
	/* READ THE MATRIX FROM DISK     */
	/*********************************/
	/* Array sizes determined. Now call wreadmtc again for really
	   reading */
	nrhs = 0; /** To prevent from reading rhs **/

#if defined(INTSIZE64) || defined(INTSSIZE64)	
	ja64 = (INTL *) malloc(nnz*sizeof(INTL));
	wreadmtc64(&tmpn,&tmpnnz,&job2,&len,a,ja64,ia,rhstmp,&nrhs,
		   guesol,&n64,&nc,&nnz,title,key,type,&ierr,filename);
	
	/* convert 64 bits to 32 bits */
	{
	  INTL i;
	  for(i=0; i<nnz; i++)
	    ja[i] = ja64[i];
	}
	free(ja64);
	
	n = n64;
#else
	wreadmtc(&tmpn,&tmpnnz,&job2,filename,&len,a,ja,ia,rhstmp,&nrhs,
		 guesol,&n,&nc,&nnz,title,key,type,&ierr);
#endif

	if(ierr != 0)
	  fprintferr(stderr, "cannot read matrix\n");
	
	fprintfd(stdout,"READ the matrix %.*s %.*s \n",8,key,3,type);

#ifdef RUA_CSR	
	if      (type[1] == 'S' || type[1] == 's') detected_sym_matrix = 1;
	else if (type[1] == 'U' || type[1] == 'u') detected_sym_matrix = 0;
	else { printf("driver unknow type : %c\n", type[1]); assert(0); }
#else
	if      (type[1] == 'S' || type[1] == 's') detected_sym_matrix = 1;
	else if (type[1] == 'U' || type[1] == 'u') 
	  {
	    INTL *ia2;
	    dim_t *ja2;
	    COEF *a2;
	    detected_sym_matrix = 0;
	    /*** In HIPS we need CSR matrix so we have to transpose the matrix since it is a column
		 storage ***/

	    ia2 = (INTL *)malloc(sizeof(INTL)*(n+1));
	    ja2 = (dim_t *)malloc(sizeof(dim_t)*(nnz+1));
	    
	    printf("nnz=%ld\n", (long)nnz);

	    a2 = (COEF *)malloc(sizeof(COEF)*nnz);
	    memcpy(ia2, ia, sizeof(INTL)*(n+1));
	    memcpy(ja2, ja, sizeof(dim_t)*nnz);
	    memcpy(a2, a, sizeof(COEF)*nnz);

	    fprintfd(stderr, "DRIVER_SPKIT \n");
	    fprintfd(stderr, "TRANSPOSE THE MATRIX ia[0] = %ld \n", (long)ia[0]);

	    CSR_Fnum2Cnum(ja2, ia2, n);
	    csr2csc(n, 1, a2, ja2, ia2,  a, ja, ia);
	    CSR_Cnum2Fnum(ja, ia, n);

	    /*fprintfd(stderr, "TRANSPOSE ia[0] = %ld ia[n] = %ld nnz = %ld \n", (long)ia[0], (long)ia[n], (long)nnz);*/

	    free(ia2);
	    free(ja2);
	    free(a2);
	  }
	else { printf("driver unknow type : %c\n", type[1]); assert(0); }
#endif

      }
#endif
      
    break;
  }
#endif
    /************************************************************************************************************/
    /************************************************************************************************************/
    /************************************************************************************************************/
  default:
    fprintferr(stdout, "*** error : input driver problem\n");
    exit(1);
  }

  /* printfd("n = %d, nnz = %d, ia[n]-1 = %d\n", n, nnz, ia[n]-1); */
  if(job == 1)
    {
      if(ia[0] == 0)
	{
	  /** Convert to Fortran Numbering **/
	  if(ia[n] != nnz)
	    {
	      fprintferr(stderr, "ERROR ia[n](=%ld) != nnz (=%ld) \n", (long)ia[n], (long)nnz);
	      assert(ia[n] == nnz);
	    }
	  
	  CSR_Cnum2Fnum(ja, ia, n);
	}
      else
	{
	  assert(ia[0] == 1);
	}
      
      if(nnz != ia[n]-1)
	{
	  fprintferr(stderr, "ERROR : nnz = %ld, ia[n]-1 == %ld \n", (long)nnz, (long)(ia[n]-1));   
	  /*ia[n] = nnz+1;*/
	  assert(nnz == ia[n]-1); /* fortran or C numbering ? */
	}
    }
  *t_n = n;
  *t_nnz = nnz;
  

  /*if(job == 1)*/
    {
      if (sym_matrix != NULL) {
	
	if (*sym_matrix == -1) {
	  *sym_matrix = detected_sym_matrix;
	} else {
	  if (*sym_matrix != detected_sym_matrix) {
	    fprintferr(stderr, "Warning : matrix %s has detected_matrix ==  %d but sym_matrix == %d\n", matrix, 
		       detected_sym_matrix, *sym_matrix);
	    
	  }
	}
	
      }
    }

    return;
  }




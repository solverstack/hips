/* @authors J. GAIDAMOUR, P. HENON */

#ifndef MM_IJK_H
#define MM_IJK_H

#include "type.h"

void read_ijv(INTS job, char *filename, INTS *n, INTL *nnz, INTL *ia, dim_t *ja, COEF *a);
void read_ijv3(INTS job, char *filename, INTS *n, INTL *nnz, INTL *ia, dim_t *ja, COEF *a);
void read_iajama(INTS job, char *filename, INTS *nr, INTL *nnzr, INTL *ia, dim_t *ja, COEF *a);
#endif

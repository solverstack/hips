# File: sphere1.py

# import python module
import sys
import vtk
import time

# create polygonal cube geometry
#   here a procedural source object is used,
#   a source can also be, e.g., a file reader
cube = vtk.vtkCubeSource()
cube.SetBounds(0,int(sys.argv[2]), 0, int(sys.argv[2]), 0, 0)

# map to graphics library
#   a mapper is the interface between the visualization pipeline
#   and the graphics model
mapper = vtk.vtkPolyDataMapper()
mapper.SetInput(cube.GetOutput()); # connect source and mapper

aCube = vtk.vtkActor()
aCube.SetMapper(mapper);
aCube.GetProperty().SetColor(1,1,1); # cube color green
aCube.GetProperty().SetOpacity(0.1);

###

linePoints = vtk.vtkPoints()
linePoints.SetNumberOfPoints(2)
linePoints.InsertPoint(0, 0, 0, 0)
linePoints.InsertPoint(1, 1, 1, 0)
aLine = vtk.vtkLine()
aLine.GetPointIds().SetId(0, 0)
aLine.GetPointIds().SetId(1, 1)
aLineGrid = vtk.vtkUnstructuredGrid()
aLineGrid.Allocate(1, 1)
aLineGrid.InsertNextCell(aLine.GetCellType(), aLine.GetPointIds())
aLineGrid.SetPoints(linePoints)
aLineMapper = vtk.vtkDataSetMapper()
aLineMapper.SetInput(aLineGrid)
aLineActor = vtk.vtkActor()
aLineActor.SetMapper(aLineMapper)
aLineActor.AddPosition(0, 0, 4)
aLineActor.GetProperty().SetDiffuseColor(.2, 1, 1)



CubeModel = vtk.vtkCubeSource()
CubeModel.SetBounds(0,4884,0,4884,0,-1)

Edges = vtk.vtkExtractEdges()
Edges.SetInput(CubeModel.GetOutput())
Tubes = vtk.vtkTubeFilter()
Tubes.SetInput(Edges.GetOutput())
Tubes.SetRadius(10)
Tubes.SetNumberOfSides(6)
Tubes.UseDefaultNormalOn()
Tubes.SetDefaultNormal(.577, .577, .577)
# Create the mapper and actor to display the cube edges.
TubeMapper = vtk.vtkPolyDataMapper()
TubeMapper.SetInput(Tubes.GetOutput())
CubeEdges = vtk.vtkActor()
CubeEdges.SetMapper(TubeMapper)

###




# our data source, a PolyDataReader
reader = vtk.vtkPolyDataReader()
reader.SetFileName(sys.argv[1])
reader.Update() # by calling Update() we read the file

# find the range of the point scalars
a,b = reader.GetOutput().GetPointData().GetScalars().GetRange()
nm =  reader.GetOutput().GetPointData().GetScalars().GetName()

# show how to print a string in python, it is similar to C-style sprintf
print "Range of %s: %4.2f-%4.2f" %(nm,a,b)

# transfer function (lookup table) for mapping point scalar data
# to colors (parent class is vtkScalarsToColors)
lut = vtk.vtkColorTransferFunction()
lut.AddRGBPoint(a, 0.0, 0.0, 1.0)
lut.AddRGBPoint(a+(b-a)/4, 0.0, 0.5, 0.5)
lut.AddRGBPoint(a+(b-a)/2, 0.0, 1.0, 0.0)
lut.AddRGBPoint(b-(b-a)/4, 0.5, 0.5, 0.0)
lut.AddRGBPoint(b, 1.0, 0.0, 0.0)

# the mapper that will use the lookup table 
mapper = vtk.vtkPolyDataMapper()
mapper.SetLookupTable(lut)
mapper.SetScalarRange(a, b)
mapper.SetInput(reader.GetOutput()) # connection
# important! tell which data you want to use
# here we use the point data
mapper.SetScalarModeToUsePointData() 

# glyphs for vector data
glyphs = vtk.vtkGlyph3D()
glyphs.SetInput(reader.GetOutput())
#arrow = vtk.vtkArrowSource() # the geometry used
#arrow.SetTipRadius(0.05)
#arrow.SetShaftRadius(0.025)
#glyphs.SetSource(0,arrow.GetOutput())
glyphs.SetVectorModeToUseVector()     # use vector magnitude
glyphs.SetScaleModeToScaleByVector()  # for scaling 
glyphs.SetScaleFactor(0.5)            # global scaling
glyphs.SetColorModeToColorByScalar()  # color by point scalars
glyphMapper = vtk.vtkPolyDataMapper()
glyphMapper.SetLookupTable(lut)
glyphMapper.SetInput(glyphs.GetOutput())
glyphActor = vtk.vtkActor()
glyphActor.SetMapper(glyphMapper)


# the actor
myActor = vtk.vtkActor()
myActor.SetMapper( mapper )

# a colorbar to display the colormap
scalarBar = vtk.vtkScalarBarActor()
scalarBar.SetLookupTable( mapper.GetLookupTable() )
#scalarBar.SetTitle("Value")
#scalarBar.SetOrientationToHorizontal()
scalarBar.GetLabelTextProperty().SetColor(0,0,1)
scalarBar.GetTitleTextProperty().SetColor(0,0,1)

# position it in window
coord = scalarBar.GetPositionCoordinate()
coord.SetCoordinateSystemToNormalizedViewport()
#coord.SetValue(0.1,0.05)
scalarBar.SetWidth(.04)
scalarBar.SetHeight(.8)

# renderer and render window 
ren = vtk.vtkRenderer()
ren.SetBackground(1, 1, 1)

# add the actors to the renderer
ren.AddActor( glyphActor )
#ren.AddActor( myActor )
#ECHELLEren.AddActor( scalarBar )
ren.AddActor(aCube)
#ren.AddActor(CubeEdges)

#ren.GetActiveCamera().SetFocalPoint(24,24,0)
#ren.GetActiveCamera().SetPosition(24, 24, 200)

ren.GetActiveCamera().Zoom(1.5)
#NON ren.GetActiveCamera().Roll(-90)
ren.GetActiveCamera().Elevation(-50)

#ren.GetActiveCamera().SetClippingRange(20000, 50000) 	
ren.ResetCameraClippingRange();

renWin = vtk.vtkRenderWindow()
renWin.SetSize(1024, 1024)
renWin.AddRenderer( ren )

# render window interactor
iren = vtk.vtkRenderWindowInteractor()
iren.SetRenderWindow( renWin )
  
ren.ResetCamera()

#reader.Modified()
#reader.Update()

#ren.GetActiveCamera().Render(ren)

# render
renWin.Render()

# initialize and start the interactor
iren.Initialize()
iren.Start()

#print "Fin"

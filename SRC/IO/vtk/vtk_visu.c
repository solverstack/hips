/* @authors J. GAIDAMOUR */

#include <stdio.h>
#include <block.h>
#include <math.h>

#define Z_MAX 400

/*  todo : visualiser sans la diag */
/*  visu des 0 ou pas, d'un seuil de valeur ... */

/*  todo : vérifier rotation, CSR / RSR */

FILE* header(char* filename, char* desc) {
  FILE* fd = fopen(filename, "w");

  fprintfv(5, fd, "# vtk DataFile Version 2.0\n");
  fprintfv(5, fd, "Desc : %s\n", desc);
  fprintfv(5, fd, "ASCII\n");
  fprintfv(5, fd, "DATASET POLYDATA\n");

  return fd;
}

void matrix(FILE* fd, int n, int nnz, int* cia, int* cja, REAL* ca) {
  int i,j;
  int z_max = MIN(Z_MAX,0.2*n);

  fprintfv(5, fd, "POINTS %d int\n", nnz); /* todo : integer */

  for(i=0; i<n; i++) {
    for(j=cia[i]; j<cia[i+1]; j++) {
      fprintfv(5, fd, "%d %d 0\n", i, n-cja[j]);
    }
  }

  /* TODO : a faire en python ?*/
  REAL max=ca[0]/* , min=ca[0] */;
  for(i=1; i<nnz; i++) {
    if(max < fabs(ca[i]))
      max = fabs(ca[i]);
  }

  fprintfv(5, fd, "\n");

  fprintfv(5, fd, "POINT_DATA %d\n", nnz);
  fprintfv(5, fd, "SCALARS my_scalars float\n");
  fprintfv(5, fd, "LOOKUP_TABLE default\n");

  for(i=0; i<nnz; i++) {
    fprintfv(5, fd, "%lf\n", ca[i]);
  }
  fprintfv(5, fd, "\n");

  fprintfv(5, fd, "VECTORS my_vectors float\n");
  REAL val;
  for(i=0; i<nnz; i++) {
    assert(max >= fabs(ca[i]));

    val = (z_max) * (ca[i] / max);
    
    if (val > 0 && val < 1)  val = 1;    
    if (val < 0 && val > -1) val = -1;
    
    fprintfv(5, fd, "0 0 %lf\n", val);
  }

  fprintfv(5, fd, "\n");

}

void matrix_solvermatrix(FILE* fd, SolverMatrix* solvmtx, SymbolMatrix* symbmtx) {
  int k, p, i,j, i0;
  REAL* ind;
  
  int stride;
  int trp;
  
  int decal;

  int n =  symbmtx->nodenbr;
  REAL nnz = 0;/* solvmtx->coefnbr; SymbolMatrix_NNZ(symbmtx) */ /* + n */ /* = + diag */;

  int z_max = MIN(Z_MAX,0.2*n);

  for(k=0;k<symbmtx->cblknbr;k++) {
    trp = symbmtx->bcblktab[k].fbloknum;
    stride = symbmtx->stride[k];
    if (stride == 0) continue;

    for(i = symbmtx->ccblktab[k].fcolnum, i0=0; i<=symbmtx->ccblktab[k].lcolnum; i++, i0++) {
      ind = solvmtx->coeftab + solvmtx->bloktab[trp].coefind + i0*stride;
      assert(ind != NULL);

      for(p=trp;p<=symbmtx->bcblktab[k].lbloknum;p++) {
	for(j=symbmtx->bloktab[p].frownum; j<=symbmtx->bloktab[p].lrownum; j++) {
	  if (j>=i)
	  if (*ind != 0)
	    nnz++;

	  ind++;
	}
      }
      
    }
  }

  fprintfv(5, fd, "POINTS %d int\n", (int)nnz); /* todo : integer*/

  decal = symbmtx->ccblktab[0].fcolnum;
  
  for(k=0;k<symbmtx->cblknbr;k++) {
    trp = symbmtx->bcblktab[k].fbloknum;
    stride = symbmtx->stride[k];
    if (stride == 0) continue;

    for(i = symbmtx->ccblktab[k].fcolnum, i0=0; i<=symbmtx->ccblktab[k].lcolnum; i++, i0++) {
      ind = solvmtx->coeftab + solvmtx->bloktab[trp].coefind + i0*stride;
      assert(ind != NULL);

      for(p=trp;p<=symbmtx->bcblktab[k].lbloknum;p++) {
	for(j=symbmtx->bloktab[p].frownum; j<=symbmtx->bloktab[p].lrownum; j++) {
	  if (j>=i)
	  if (*ind != 0)
	    fprintfv(5, fd, "%d %d 0\n", i-decal, n-j-decal);

	  ind++;
	}
      }
      
    }
  }

  fprintfv(5, fd, "\n");

  fprintfv(5, fd, "POINT_DATA %d\n", (int)nnz);
  fprintfv(5, fd, "SCALARS my_scalars float\n");
  fprintfv(5, fd, "LOOKUP_TABLE default\n");

  /* TODO : a faire en python ?*/
  int max=fabs(*solvmtx->coeftab);
  for(k=0;k<symbmtx->cblknbr;k++) {
    trp = symbmtx->bcblktab[k].fbloknum;
    stride = symbmtx->stride[k];
    if (stride == 0) continue;

    for(i = symbmtx->ccblktab[k].fcolnum, i0=0; i<=symbmtx->ccblktab[k].lcolnum; i++, i0++) {
      ind = solvmtx->coeftab + solvmtx->bloktab[trp].coefind + i0*stride;
      assert(ind != NULL);

      for(p=trp;p<=symbmtx->bcblktab[k].lbloknum;p++) {
	for(j=symbmtx->bloktab[p].frownum; j<=symbmtx->bloktab[p].lrownum; j++) {
	  if (j>=i)
	  if (*ind != 0)
	    if(max < fabs(*ind))
	      max = fabs(*ind);	  

	  ind++;
	}
      }
      
    }
  }

  for(k=0;k<symbmtx->cblknbr;k++) {
    trp = symbmtx->bcblktab[k].fbloknum;
    stride = symbmtx->stride[k];
    if (stride == 0) continue;

    for(i = symbmtx->ccblktab[k].fcolnum, i0=0; i<=symbmtx->ccblktab[k].lcolnum; i++, i0++) {
      ind = solvmtx->coeftab + solvmtx->bloktab[trp].coefind + i0*stride;
      assert(ind != NULL);

      for(p=trp;p<=symbmtx->bcblktab[k].lbloknum;p++) {
	for(j=symbmtx->bloktab[p].frownum; j<=symbmtx->bloktab[p].lrownum; j++) {
	  if (j>=i)
	  if (*ind != 0)
	    fprintfv(5, fd, "%0.14lf ", fabs(*ind));

	  ind++;
	}
      }
      
    }
  }

  fprintfv(5, fd, "\n");

  REAL val;

  fprintfv(5, fd, "VECTORS my_vectors float\n");
  for(k=0;k<symbmtx->cblknbr;k++) {
    trp = symbmtx->bcblktab[k].fbloknum;
    stride = symbmtx->stride[k];
    if (stride == 0) continue;

    for(i = symbmtx->ccblktab[k].fcolnum, i0=0; i<=symbmtx->ccblktab[k].lcolnum; i++, i0++) {
      ind = solvmtx->coeftab + solvmtx->bloktab[trp].coefind + i0*stride;
      assert(ind != NULL);

      for(p=trp;p<=symbmtx->bcblktab[k].lbloknum;p++) {
	for(j=symbmtx->bloktab[p].frownum; j<=symbmtx->bloktab[p].lrownum; j++) {
	  if (j>=i)
	    if (*ind != 0) {
	      
	      val = (z_max) * (*ind / max);
	      
	      if (val > 0 && val < 1)  val = 1;    
	      if (val < 0 && val > -1) val = -1;
	  
	      fprintfv(5, fd, "0.0 0.0 %0.14lf\n", val);

	    }

	  ind++;
	}
      }
      
    }
  }
  
  fprintfv(5, fd, "\n");
}

void matrix_dbmatrix(FILE* fd, DBMatrix* L) {
  matrix_solvermatrix(fd, &L->a[0], &L->a[0].symbmtx);  
}

void vtk_dump(char* filename, char* desc, int n, int nnz, int* cia, int* cja, REAL* ca) {
  FILE * fd = header(filename, desc);

  matrix(fd, n, nnz, cia, cja, ca);

  fclose(fd);
}

void vtk_dump_dbmatrix(char* filename, char* desc, DBMatrix* dbmatrix) {
  FILE * fd = header(filename, desc);
  
  matrix_dbmatrix(fd, dbmatrix);
  
  fclose(fd);
}

#ifdef TEST_MAIN
int main() {
  FILE * fd = header("test.vtk", "Fichier de test");

  int nnz = 9;
  int    cia[9] = {0, 0, 0, 1, 1, 1, 2, 2, 2};
  int    cja[9] = {0, 1, 2, 0, 1, 2, 0, 1, 2};
  REAL ca[9] = {3, 1, 1, 1, 3, 1, 1, 1, 3};

  matrix(fd, nnz, cia, cja, ca);
  fclose(fd);
 
  return 0;
}
#endif

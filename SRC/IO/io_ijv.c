/* @authors J. GAIDAMOUR, P. HENON */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "phidal_common.h"

/* TODO : fgets +sscanf ou fscanf */ 
/* virer le renumbering */
/* harmoniser le nom des variables (nr, ou n_t) avec io.c */

#define BUFLEN 200
void read_ijv(INTS job, char *filename, INTS *nr, INTL *nnzr, INTL *ia, INTS *ja, COEF *ma)
{
  /********************************************/
  /* Read an ijv matrix and return it as a CSR*/
  /* in Fortran numbering                     */
  /********************************************/

  FILE *fp;
  char buf[BUFLEN];
  int n, n1, n2;
  int nnz;
  int *ii, *jj;
  COEF *val;
  int i;
  int ind;
  int flag;

  if((fp = fopen(filename,"r")) == NULL) 
    {
      fprintferr(stderr, "read_ijv: Cannot open file %s \n", filename);
      exit(1);
    }
  
  /**** Read the dimension and number of zero *****/
  FGETS(buf, BUFLEN, fp);
  sscanf(buf, "%d %d %d", &n1, &n2, &nnz);

  fprintfd(stderr, "n1 = %ld n2 =%ld nnz = %ld \n" ,(long)n1, (long)n2, (long)nnz);
  
  if(n1 != n2)
    {
      fprintferr(stderr, "Error not a square matrix n1 = %d n2 = %d \n", n1, n2);
      exit(-1);
    }

  n=n1;

  if(job==0)
    {
      *nr = n;
      *nnzr = nnz;
      fclose(fp);
      return;
    }

  ii = (int *)malloc(sizeof(int)*nnz);
  assert(ii != NULL);
  jj = (int *)malloc(sizeof(int)*nnz);
  assert(jj != NULL);
  val = (COEF *)malloc(sizeof(COEF)*nnz);
  assert(val != NULL);

  i = 0;
  while(fgets(buf, BUFLEN, fp) != NULL) 
    {
#ifdef TYPE_REAL
      sscanf(buf, "%d %d "_scan_f_, ii+i, jj+i, val+i);
#endif
      i++;
    }
  fprintfd(stderr, "i = %d nnz = %d \n", i, nnz);
  /*assert(i == nnz);*/
  fclose(fp);

  flag = 0;
  for(i=0;i<nnz;i++)
    if(ii[i] == 0)
      {
	flag = 1;
	break;
      }

  /*if(ii[0] == 1)*/
  if(flag != 1)
    {
      fprintfd(stderr, "RENUMBERING \n");
      for(i=0;i<nnz;i++)
	{
	  ii[i]--;
	  jj[i]--;
#ifdef DEBUG_M
	  assert(ii[i] >= 0);
	  assert(jj[i] >= 0);
#endif

	}
    }
  

  /** count number of non zero per row **/
  bzero(ia, sizeof(int)*(n+1));
  for(i=0;i<nnz;i++)
    ia[ii[i]+1]++;

#ifdef DEBUG_M  
  for(i=1;i<=n;i++)
    assert(ia[i] > 0);
#endif


  for(i=1;i<=n;i++)
    ia[i] += ia[i-1];

  for(i=0;i<nnz;i++)
    {
      ind = ia[ii[i]];
      ja[ind] = jj[i];
      ma[ind] = val[i];
      ia[ii[i]]++;
    }


  for(i=n;i>=1;i--)
    ia[i] = ia[i-1];
  ia[0] = 0;

  /*for(i=ia[0]; i<ia[1];i++)
    fprintfd(stderr, "(%d %g) ", ja[i], ma[i]);
    exit(0);*/

  CSR_Cnum2Fnum(ja, ia, n);
  
  free(ii);
  free(jj);
  free(val);

  *nr = n;
  *nnzr = nnz;
  
}



void read_ijv3(INTS job, char *filename, INTS *nr, INTL *nnzr, INTL *ia, INTS *ja, COEF *ma)
{
  /********************************************/
  /* Read an ijv matrix and return it as a CSR*/
  /* in 3 files header ia.dat, ja.dat, a.dat  */
  /* the 3 files are in the directory filename*/
  /* in Fortran numbering                     */
  /********************************************/

  FILE *fp;
  char buf[BUFLEN];
  int n, n1, n2;
  int nnz;
  int *ii, *jj;
  COEF *val;
  int i;
  int ind;

  if((fp = fopen(filename,"r")) == NULL) 
    {
      fprintferr(stderr, "read_ijv: Cannot open file %s \n", filename);
      exit(1);
    }
  /* header file */
  sprintf (buf, "%s/header", filename);
  if ((fp = fopen(buf, "r")) == NULL)
    exit(1);
  /**** Read the dimension and number of zero *****/
  FGETS(buf, BUFLEN, fp);
  sscanf(buf, "%d %d %d", &n1, &n2, &nnz);

  fclose(fp);

  fprintfd(stderr, "n1 = %ld n2 =%ld nnz = %ld \n" ,(long)n1, (long)n2, (long)nnz);
  
  if(n1 != n2)
    {
      fprintferr(stderr, "Error not a square matrix n1 = %d n2 = %d \n", n1, n2);
      exit(-1);
    }

  n=n1;

  if(job==0)
    {
      *nr = n;
      *nnzr = nnz;

      return;
    }

  ii = (int *)malloc(sizeof(int)*nnz);
  assert(ii != NULL);
  jj = (int *)malloc(sizeof(int)*nnz);
  assert(jj != NULL);
  val = (COEF *)malloc(sizeof(COEF)*nnz);
  assert(val != NULL);

  i = 0;
  while(fgets(buf, BUFLEN, fp) != NULL) 
    {
#ifdef TYPE_REAL
      sscanf(buf, "%d %d "_scan_f_, ii+i, jj+i, val+i);
#endif
      i++;
    }
  fprintfd(stderr, "i = %d nnz = %d \n", i, nnz);
  /*assert(i == nnz);*/
  fclose(fp);

  if(ii[0] == 1)
    {
      fprintfd(stderr, "RENUMBERING \n");
      for(i=0;i<nnz;i++)
	{
	  ii[i]--;
	  jj[i]--;
#ifdef DEBUG_M
	  assert(ii[i] >= 0);
	  assert(jj[i] >= 0);
#endif

	}
    }
  

  /** count number of non zero per row **/
  bzero(ia, sizeof(int)*(n+1));
  for(i=0;i<nnz;i++)
    ia[ii[i]+1]++;

#ifdef DEBUG_M  
  for(i=1;i<=n;i++)
    assert(ia[i] > 0);
#endif


  for(i=1;i<=n;i++)
    ia[i] += ia[i-1];

  for(i=0;i<nnz;i++)
    {
      ind = ia[ii[i]];
      ja[ind] = jj[i];
      ma[ind] = val[i];
      ia[ii[i]]++;
    }


  for(i=n;i>=1;i--)
    ia[i] = ia[i-1];
  ia[0] = 0;

  /*for(i=ia[0]; i<ia[1];i++)
    fprintfd(stderr, "(%d %g) ", ja[i], ma[i]);
    exit(0);*/

  CSR_Cnum2Fnum(ja, ia, n);
  
  free(ii);
  free(jj);
  free(val);

  *nr = n;
  *nnzr = nnz;
  
}




void read_iajama(INTS job, char *filename, INTS *nr, INTL *nnzr, INTL *ia, INTS *ja, COEF *a) {
    int i;
    FILE* f;
    char file[BUFLEN];

    int n;
    INTL nnz;
    int rnnz;

#ifdef TYPE_COMPLEX
    REAL ax, ay;
#endif
    
    fprintfd(stderr, "Read the matrix (three files IJV) in directory %s \n", filename);

    /* ia file */
    sprintf (file, "%s/ia.dat", filename);
    if ((f = fopen(file, "r")) == NULL)
      exit(1);
    FSCANF(f, "%d \n", &n);

    if(job == 1)
      {
	assert(ia != NULL);
	for (i=0; i<=n; i++)
	  FSCANF(f, _int_" ", &ia[i]);
      }
    fclose(f);

    /* ja file */
    sprintf (file, "%s/ja.dat", filename);
    if ((f = fopen(file, "r")) == NULL)
      exit(1);

    FSCANF(f, "%d \n", &rnnz);
    nnz = (INTL)rnnz;

    if(job == 1)
      {
	assert(ja != NULL);
	for (i=0; i<nnz; i++)
	  FSCANF(f, "%d \n", &ja[i]);
      }
    fclose(f);

    if(job == 0)
      {
	*nr = n;
	*nnzr = nnz;
	return;
      }

    if(job == 1)
      {
	/* a file */
	sprintf (file, "%s/a.dat", filename);
	if ((f = fopen(file, "r")) == NULL)
	  exit(1);
	FSCANF(f, "%d \n", &rnnz);
	nnz = (INTL)rnnz;

	assert(a != NULL);
	for (i=0; i<nnz; i++)
	  {
#if defined(TYPE_REAL)
	    FSCANF(f, _scan_g_"\n", &a[i]);
#elif defined(TYPE_COMPLEX)
	    FSCANF(f, _scan_g_" "_scan_g_"\n", &ax, &ay);
	    a[i] = ax + I*ay;
#endif
	  }
	fclose(f);
      }



    *nr = n;
    *nnzr = nnz;
}




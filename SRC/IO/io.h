/* @authors J. GAIDAMOUR, P. HENON */

#ifndef IO_H
#define IO_H

#define DRIVER_AUTO -1
#define DRIVER_USER 1
#define DRIVER_IOHB 2
#define DRIVER_IOMM 3
#define DRIVER_IJV 4
#define DRIVER_IJV3FILES 5
#define DRIVER_SPKIT 6
#define DRIVER_IAJAMA 7

#define DRIVER_DEFAULT DRIVER_AUTO

int CSRread(char* matrix, INTS *t_n, INTL* t_nnz, INTL** t_ia, INTS** t_ja, COEF** t_a, INTS* sym_matrix);
void VECread(char *filename, INTS n, COEF *rhs);

void CSR_Fnum2Cnum(INTS *ja, INTL *ia, INTS n);


/* In hips.h du to fortran interface */
/*void Matrix_Read(INTS job, char* matrix, INTS *t_n, INTL* t_nnz, INTL * ia, INTS * ja, COEF* a, INTS* sym_matrix);*/

#endif



/* @authors J. GAIDAMOUR, P. HENON */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <mpi.h>
#include "math.h"

#include "hips.h"
#include "hips_wrapper.h"
#include "scotch_metis_wrapper.h"

#include "prec.h"
#include "macro.h"


extern int_t idnbr;
extern HIPS_Context *context;


INTS HIPS_ParallelSetup(INTS id)
{
  /*************************************************/
  /* Compute the local data and parallel structure */
  /* Set the MPI communicator for this system      */
  /*************************************************/

  PhidalHID *BL;
  PhidalDistrHID *DBL;
  HIPS_Context *c;
  c = context+id;

  if(c->state < HIERARCH)
    {
      HIPS_GraphBuildHID(id);
    }
 
  BL = &c->BL;
  DBL = &c->DBL;
  PhidalDistrHID_Setup(c->proc_id, c->nproc, c->master, BL, DBL, c->iperm, c->mpicom);

  /** Need iperm in HIPS_MatrixSetGlobalCSR **/
  /*free(c->iperm);
    c->iperm = NULL;*/

  /***** Compute the processor set of each connector ******/
  c->block_psetindex = (dim_t *)malloc(sizeof(dim_t)* (BL->nblock+1));
  c->block_pset = (mpi_t *)malloc(sizeof(mpi_t)*(BL->block_keyindex[BL->nblock]));
  GetGlobalConnectorMapping(DBL->nproc, DBL->dom2proc, BL, c->block_psetindex,c-> block_pset);

#ifdef PARALLEL_LOCAL_ORDERING
  if(c->partition_type != 0 && c->hybrid == 0)
    {
#ifdef DEBUG_M
      assert(c->nproc == c->ndom);
#endif
      if(c->reorder == 1)
	{
	  INTL ln;
	  dim_t i;
	  INTL *lig;
	  dim_t *ljg;
	  int fillopt[10];
	  ln = DBL->LHID.n;
	  CSR_GetSquareSubmatrix(ln, DBL->loc2orig, 
				 c->ng, c->ig, c->jg, NULL,
				 &lig, &ljg, NULL);
	  fillopt[0] = 0; /** 0 ND, 1 MD, 2 MF **/
	  PHIDAL_MinimizeFill(fillopt, ln, lig, ljg, &DBL->LHID, DBL->loc2orig);
	  for(i=0;i<ln;i++)
	    DBL->orig2loc[DBL->loc2orig[i]] = i;
	  
	  free(lig);
	  free(ljg);
	}
    }

  /* c->ig c->jg NULL if Load from Disk */
  if (c->ig != NULL) { free(c->ig); c->ig = NULL;  /** Important (tested elsewhere) **/}
  if (c->jg != NULL) { free(c->jg); c->jg = NULL;  /** Important (tested elsewhere) **/}
#endif

  if(DBL->LHID.n == 0)
    {
      fprintferr(stderr, "ERROR in HIPS_ParallelSetup : processor %ld DBL->LHID.n=0\n", (long)c->proc_id);
      return HIPS_ERR_PARASETUP;
    }

  if(c->hybrid == 1)
    {

      if(c->partition_type > 0)
	{
	  fprintf(stderr, "You have to set a domsize parameter (HIPS_SetOptionINT() with HIPS_DOMSIZE) to use the HYBRID solver \n");
	  return HIPS_ERR_PARAMETER;
	}


      /****************************************/
      /*** Compute the local symbolmatrix   ***/
      /****************************************/
      c->symbloc = (SymbolMatrix *)malloc(sizeof(SymbolMatrix));
      SymbolMatrix_GetLocal(c->symbmtx, c->symbloc, BL, DBL);
      
      SymbolMatrix_Clean(c->symbmtx);
      free(c->symbmtx); c->symbmtx = NULL;

      if(BL->dof > 1)
	SymbolMatrix_Expand(c->symbloc, BL->dof);

    }
  if(BL->dof > 1)
    {
      fprintfd(stderr, "Expand HID dof = %ld \n", (long)BL->dof);
      PhidalDistrHID_Expand(DBL);
    }
  
  c->ln = c->DBL.LHID.n;


  c->state = SETUP;
  return HIPS_SUCCESS;
}


INTS HIPS_Precond(INTS id)
{
  /******************************/
  /* Build HIPS preconditioner  */
  /******************************/
  HIPS_Context *c;
  chrono_t t1, t2;
  PrecInfo* info = NULL;
  PhidalOptions* option;

  c = context+id;
  option = &c->options;

  if(c->donotprecond == 1 && c->precond == 1)
    return HIPS_SUCCESS;


  if(c->state < COEFFICIENT)
    {
      fprintferr(stderr, "ERROR in HIPS_Precond : You must call enter matrix coefficients before calling HIPS_Precond \n");
      return HIPS_ERR_PRECOND;
    }
  
  
#ifdef DEBUG_MOMO
  {
      REAL *normtab;
      dim_t i;
      normtab = (REAL *)malloc(sizeof(REAL)*c->A.M.dim1);
      PhidalDistrMatrix_RowNorm2(&c->A, &c->DBL, normtab);
      for(i=0;i<c->A.M.dim1;i++)
	if(normtab[i] <= 0)
	  {
	    fprintferr(stderr, "Error in HIPS_Precond : input matrix, the norm of row %ld is %g \n", (long)i, normtab[i]);
	    MPI_Abort(MPI_COMM_WORLD, -1);
	  }

      PhidalDistrMatrix_ColNorm2(&c->A, &c->DBL, normtab);
      for(i=0;i<c->A.M.dim1;i++)
	if(normtab[i] <= 0)
	  {
	    fprintferr(stderr, "Error in HIPS_Precond : input matrix, the norm of col %ld is %g \n", (long)i, normtab[i]);
	    MPI_Abort(MPI_COMM_WORLD, -1);
	  }
      free(normtab);
  }
#endif

  if(c->options.verbose >= 2)
    {
      info = (PrecInfo*)malloc(sizeof(PrecInfo));
      PrecInfo_Init(info);
      PrecInfo_SetNNZA(info, PhidalDistrMatrix_NNZ(&c->A));
    }
  

  t1  = dwalltime(); 
  if(c->hybrid == 0)
    { 
      if(c->precond == 1)
	PhidalDistrPrec_Clean(&c->Pscal);  

      /*PhidalDistrPrec_Init(&c->Pscal); dans fonction phidal seulement c'etait la cause des fuites memoires*/
      c->Pscal.info = info;

      PHIDAL_DistrPrecond(&c->A, &c->Pscal, &c->DBL, &c->options);
    }
  else
    {

#ifdef USE_SEQ_PRECOND
      if(c->nproc == 1) {

	if(c->precond == 1)
	  DBPrec_Clean(&c->PblockSEQ);
	

	/** Ce DBPrec_Init doit etre redescendu dans les fonction MLILU par symmetrie avec les fonctions phidal ;
	    ceci etait la cause des fuite memoire **/
	DBPrec_Init(&c->PblockSEQ);  
	c->PblockSEQ.info = info;
	  
	fprintfd(stderr, "*** USE_SEQ_PRECOND\n");
	DBMATRIX_Precond(&c->A.M, &c->PblockSEQ, c->symbloc, &c->BL, &c->options);
      } else
#endif
	{
	  
	  if(c->precond == 1)
	    DBDistrPrec_Clean(&c->Pblock);
	  
	  DBDistrPrec_Init(&c->Pblock);  
	  c->Pblock.info = info;
	  
	  DBDISTRMATRIX_Precond(&c->A, &c->Pblock, c->symbloc, &c->DBL, &c->options);     
	  
	}

    }

  t2  = dwalltime(); 

  {

    /* if(c->options.verbose >= 1) */
    if(c->proc_id == 0)
      {
	/** METRIC **/
	fprintfv(1, stdout, "HIPS preconditioner computed in %g seconds \n\n", t2-t1);
      }
    
    c->inforeal[HIPS_INFO_PRECOND_TIME] = t2-t1;

    if (option->verbose >=2) {
      long nnzA, nnzP;
      
      nnzA = PhidalDistrMatrix_NNZ_All(&c->A, c->mpicom);
      
      if(c->hybrid == 0)
	nnzP = PhidalDistrPrec_NNZ_All(&c->Pscal, c->mpicom);
      else {
#ifdef USE_SEQ_PRECOND
	if(c->nproc == 1) {
	  nnzP = DBPrec_NNZ(&c->PblockSEQ); /* == DBPrec_NNZ_All(&c->PblockSEQ, c->mpicom); */
	} else
#endif
	  nnzP = DBDistrPrec_NNZ_All(&c->Pblock, c->mpicom);
      }
      
      c->infoint[HIPS_INFO_NNZ] = nnzP;
      c->infoint[HIPS_INFO_NNZ_PEAK] = info->peak;

      if(c->proc_id == 0) {
	/** METRIC **/
	fprintfv(5, stdout, "Global : HIPS nnz(A) = %ld nnz(Prec) = %ld \n", (long)nnzA, (long)nnzP);
	fprintfv(1, stdout, "Global : HIPS Fill Ratio of Preconditioner = %g \n\n", ((REAL)nnzP)/((REAL)nnzA));
      }
    }
  }
  
#ifndef USE_SEQ_PRECOND /*tmp*/
  if (info != NULL) {
    REAL MnnzA;
    PrecInfo_Print(info, c->proc_id);
    MnnzA =  ((REAL)PhidalDistrMatrix_NNZ_All(&c->A, c->mpicom)) / c->nproc;

    if(c->proc_id == 0) 
      {
	REAL ratio_max = 0;
	REAL peakratio_max = 0;
	PrecInfo_Max(info, MnnzA, &ratio_max, &peakratio_max,
		     context->mpicom);

	PrecInfo_PrintMax(ratio_max, peakratio_max, c->proc_id);

	REAL ratio_max_ref1 = 0;
	REAL peakratio_max_ref1 = 0;
	PrecInfo_MaxThese(info, c->nproc, &ratio_max_ref1, &peakratio_max_ref1,
		     context->mpicom);

	PrecInfo_PrintMaxThese(ratio_max_ref1, peakratio_max_ref1, c->proc_id);
      } 
    else {
      PrecInfo_Max(info, MnnzA, NULL, NULL, context->mpicom);
      PrecInfo_MaxThese(info, c->nproc, NULL, NULL, context->mpicom);
    }

    PrecInfo_Clean(info); 
    free(info); info = NULL;
  }
#endif

  c->precond = 1;
  c->state = PRECOND;

  return HIPS_SUCCESS;

}

INTS HIPS_MatrixVectorProduct(INTS id, COEF *x, COEF *y)
{
  HIPS_Context *c;
  c = context+id;
  PhidalDistrMatrix_MatVec(1, &c->A, &c->DBL, x, y);

  return HIPS_SUCCESS;
}


INTS HIPS_Solve(INTS id)
{
  /****************************************************/
  /* Get the local solution corresponding to nodelist */             
  /****************************************************/ 
  HIPS_Context *c;
  chrono_t t1, t2;
  PhidalOptions* option;
  dim_t *itertab;
  REAL *resnormtab;

  c = context+id;
  option = &c->options;

  if(c->b == NULL)
    {
      fprintferr(stderr, "ERROR in HIPS_Solve : there is no rhs \n");
      return HIPS_ERR_SOLVE;
    }

  if(c->flagrhs == 0) /* no new rhs */
    return HIPS_SUCCESS;

  //option->scale = 0;

  if(c->state < PRECOND)
    CHECK_RETURN(HIPS_Precond(id));
  
  t1  = dwalltime();

  if(c->x == NULL)
    {
      c->x = (COEF *)malloc(sizeof(COEF)*c->ln);
      CHECK_MALLOC(c->x);

      bzero(c->x, sizeof(COEF)*c->ln);
    }

  itertab = (dim_t *)malloc(sizeof(dim_t)*(c->BL.nlevel+1));
  CHECK_MALLOC(itertab);
  resnormtab = (REAL *)malloc(sizeof(REAL)*(c->BL.nlevel+1));
  CHECK_MALLOC(resnormtab);


  if(c->hybrid == 0)
    {
      CHECK_RETURN(PHIDAL_DistrSolve(&c->A, &c->Pscal, &c->DBL, &c->options, c->b, c->x, itertab, resnormtab));
    }
  else
    {



      /**** HE DIS DONC ON PEUT PAS UTILISER LE CG EN FAISANT COMME CA ! ***/
      /*IF FAUT METTRE CA CHECK_RETURN(DBMATRIX_DistrSolve(&c->A, &c->Pblock, &c->DBL, &c->options, c->b, c->x, itertab, resnormtab));*/

#ifdef USE_SEQ_PRECOND
      if (c->nproc == 1) {
	CHECK_RETURN(HIPS_Fgmresd_PH_DB(option->verbose, option->tol, option->itmax,
					&c->A.M, &c->PblockSEQ,
					&c->BL, option,
					c->b, c->x, stdout, itertab, resnormtab)); /* /!\ macro et point-virgule */
	  
      } else
#endif
	CHECK_RETURN(HIPS_DistrFgmresd_PH_DB(option->verbose, option->tol, option->itmax,
					     &c->A, &c->Pblock,
					     &c->DBL, option,
					     c->b, c->x, stdout, itertab, resnormtab));


    }
  t2  = dwalltime(); 

#ifdef WITH_PASTIX
  if(option->use_pastix == 1)
    free((&(&c->Pblock)->LU->m.db.L->M)->pastix_str);
#endif // WITH_PASTIX


  if((c->proc_id == 0))
    {

      fprintfv(1, stdout, "\n Number of outer iterations %ld \n", (long)itertab[0]);
      if(itertab != NULL &&  c->hybrid == 1)
	fprintfv(1, stdout, "\n Number of inner iterations %ld \n", (long)itertab[1]);
    }

  if(c->hybrid == 1 && c->BL.nlevel > 1)
    c->infoint[HIPS_INFO_INNER_ITER] = itertab[1];
  else
    c->infoint[HIPS_INFO_INNER_ITER] = 0;
  c->infoint[HIPS_INFO_OUTER_ITER] = itertab[0];
  c->inforeal[HIPS_INFO_RES_NORM] = resnormtab[0];
  c->inforeal[HIPS_INFO_SOLVE_TIME] = t2-t1;
  if(c->proc_id == 0)
    fprintfv(1, stdout, " HIPS : solved in %g seconds \n\n", t2-t1);
    
  free(itertab);
  free(resnormtab);
  c->flagrhs = 0;


  return HIPS_SUCCESS;
}


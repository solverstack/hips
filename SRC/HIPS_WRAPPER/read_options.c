/* @authors J. GAIDAMOUR, P. HENON */

#include <stdlib.h>
#include <stdio.h>
#include <limits.h>
#include <string.h> /* strcmp*/
#include "hips_define.h"
#include "hips.h" /* macros */
#include "phidal_common.h" /* printfd */

#define BUFLEN 200

INTS Read_options(char *filename,  char *matrix, INTS *sym_pattern, INTS *sym_matrix, char* rhs, INTS* method, INTS *options_int, REAL *options_double) 
{
  /* setup parameters for preconditioning and iteration */
  FILE *fp;
  char buf[BUFLEN];
  int num;


  /* read parameters for preconditioner and iteration from file
     'input' */
    
  if(filename == NULL)
    {
      if((fp = fopen("Inputs","r")) == NULL) 
	{
	  fprintfd(stderr, "Cannot open file Inputs \n");
	  return HIPS_ERR_IO;
	}
    }
  else
    if( (fp = fopen(filename, "r")) == NULL )
      {
	fprintfd(stderr, "Cannot open file ARGV. Trying inputs file ... \n"); 
	if((fp = fopen("Inputs","r")) == NULL) 
	  {
	    fprintfd(stderr, "Cannot open file inputs\n");
	    return HIPS_ERR_IO;
	  }
      }
  num = 0;

  while(fgets(buf, BUFLEN, fp) != NULL) {
    switch(num) {
    case 0:                             /* matrix */
      sscanf(buf, "%s", matrix); 
      break;
    case 1:                             /* sym_pattern */
      sscanf(buf, "%d", sym_matrix);
      switch(*sym_matrix){
      case 0:
	(*sym_matrix) = 0;
	(*sym_pattern) = 0;
	options_int[HIPS_SYMMETRIC] = 0;
	break;
      case 1:
	(*sym_matrix) = 0;
	(*sym_pattern) = 1;
	options_int[HIPS_SYMMETRIC] = 0;
	break;
      case 2:
	(*sym_matrix) = 1;
	(*sym_pattern) = 0;
	options_int[HIPS_SYMMETRIC] = 1;
      break;
      case -1:
	(*sym_matrix) = -1;
	(*sym_pattern) = -1;
	options_int[HIPS_SYMMETRIC] = -1;
      break;
      default:
	break;
      }
      break;
    case 2:                             
      sscanf(buf, "%s", rhs);
      if (strcmp(rhs, "0") == 0) rhs = "";
      break; 
    case 3:{                             
      char cmethod[20];
      *method = -1;
      sscanf(buf, "%s", cmethod);
      if (strcmp(cmethod, "DIRECT") == 0) *method = HIPS_DIRECT;
      if (strcmp(cmethod, "ITERATIVE") == 0)   *method = HIPS_ITERATIVE;
      if (strcmp(cmethod, "ILUT") == 0)   *method = HIPS_ITERATIVE; /** the same for compatibility **/
      if (strcmp(cmethod, "HYBRID") == 0) *method = HIPS_HYBRID;
      if (strcmp(cmethod, "BLOCK") == 0)  *method = HIPS_BLOCK;
    }
      break; 
    case 4:
      sscanf(buf,_scan_f_, options_double+HIPS_PREC);
      break;
    case 5:{
      char locally[20];
      sscanf(buf, "%s", locally);
      if (strcmp(locally, "ALL") == 0) 
	options_int[HIPS_LOCALLY] = INT_MAX;
      else
	options_int[HIPS_LOCALLY] = atoi(locally);
    }
      break;
    case 6:                             /* max outer gmres steps */
      sscanf(buf,"%d", options_int+HIPS_ITMAX);
      break;
    case 7:                             /* im (Outer Krylov subs dim) */
      sscanf(buf,"%d", options_int+HIPS_KRYLOV_RESTART);
      break;
    case 8:                             
      sscanf(buf, _scan_f_, options_double+HIPS_DROPTOL0);
      break;
    case 9:                             
      sscanf(buf, _scan_f_, options_double+HIPS_DROPTOL1);
      break; 
    case 10:                             
      sscanf(buf, _scan_f_, options_double+HIPS_DROPTOLE);
      break;
    case 11:                             
      sscanf(buf, "%d", options_int+HIPS_VERBOSE);
      break;
    case 12:
      sscanf(buf, "%d", options_int+HIPS_USE_PASTIX);
    default:
      break;
    }
    num++;

  }
  fclose(fp);
  return HIPS_SUCCESS;
}

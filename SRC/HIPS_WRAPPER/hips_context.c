/* @authors P. HENON */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <mpi.h>
#include "math.h"

#include "hips_wrapper.h"
#include "hips.h"

#include "scotch_metis_wrapper.h"

int_t idnbr;
HIPS_Context *context = NULL;
MPI_Op mpi_max;

void vec_max(void *invec, void *inoutvec, int *len, MPI_Datatype *datatype)
{

  int i;
  int size;
  COEF *vin, *vout;
  vin = (COEF*)invec;
  vout = (COEF *)inoutvec;
  size = *len/LENC;
  for(i=0;i< size;i++)
    if(coefabs(vin[i]) > coefabs(vout[i]))
      vout[i] = vin[i];
}


INTS HIPS_Context_Reinit(HIPS_Context *context)
{
  /** Reset the context but keep the option previoulsy configured **/
  int_t i;

  PhidalHID_Init(&context->BL);
  PhidalDistrHID_Init(&context->DBL);

  PhidalDistrMatrix_Init(&context->A);
  /*PhidalDistrMatrix_Init(&context->T);*/

  /** Done in HIPS_Precond **/
  /*PhidalDistrPrec_Init(&context->Pscal);*/

  context->perm = NULL;
  context->iperm = NULL;

  context->mapptr = NULL;
  context->mapp = NULL;
  context->state = INIT;
  context->flagrhs = 0;  /** new RHS ? **/
  context->flagmat = 0;  /** PhidalMatrix matrix initialized ? **/
  context->precond = 0;  /** Has Precond been done once ? **/
  context->symbmtx = NULL; /* set only if hybrid == 1 */
  context->symbloc = NULL; /* set only if hybrid == 1 */
  context->ig = NULL;
  context->jg = NULL;
  context->block_psetindex = NULL;
  context->block_pset = NULL;

  context->x = NULL;
  context->b = NULL;
  context->t = NULL;
  context->job = 0;
  context->job2 = 0;
  context->ind = 0;
  context->edgenbr = 0;
  context->nnz = 0;
  context->ii = NULL;
  context->jj = NULL;
  context->val = NULL;


  return HIPS_SUCCESS;
}


INTS HIPS_Context_Init(HIPS_Context *context)
{
  int_t i;
  HIPS_Context_Reinit(context);
  PhidalOptions_Init(&context->options);
  context->domsize = 0;
  context->idmax = 0;
  context->ndom = 1;
  context->reorder = 1;
  context->partition_type = 0;
  context->improve_partition = 0;
  context->donotprecond = 0;  /** = 1 disable the preconditioning when new matrix coefficient are entered **/
  context->assemb_sym = 0; /* used in AssemblyBegin **/
  context->hybrid = 0;
  context->tagnbr = 4;  /** 3 should be better in 2D --> improve_partition **/
  context->ng = 0;
  context->na = 0;
  context->coarse_grid = 0;
  context->check_graph = 1;
  context->check_matrix = 1;
  context->dumpcsr = 0;
  context->graph_sym = 1;

  context->mpicom = MPI_COMM_WORLD;
  context->master = 0;
  MPI_Comm_size(MPI_COMM_WORLD, &context->nproc);
  MPI_Comm_rank(MPI_COMM_WORLD, &context->proc_id);

  context->ln = 0;
  context->dof = 1;
  context->mode = 0;
  context->numflag = 1; /** Fortran by default **/

  context->cube = 0;
  context->nc = 0;

  for(i=0;i<HIPS_INFOINTNBR;i++)
    context->infoint[i] = -1;
  for(i=0;i<HIPS_INFOREALNBR;i++)
    context->inforeal[i] = -1;

  return HIPS_SUCCESS;
}

INTS HIPS_Initialize(INTS nid)
{
  int i;

  if(nid <= 0)
    return HIPS_ERR_PARAMETER;

  assert(nid > 0);
  idnbr = nid+1;  /** think to fortran user +1 **/
  context = (HIPS_Context *)malloc(sizeof(HIPS_Context)*idnbr);
  assert(context != NULL);

  MPI_Op_create(vec_max, 1, &mpi_max);

  for(i=0;i<idnbr;i++)
    {
      INTS ret;
      ret = HIPS_Context_Init(context+i);
      if(ret != HIPS_SUCCESS)
	return ret;
    }

  return HIPS_SUCCESS;
}


INTS HIPS_Clean(INTS id)
{
  /************************************************/
  /* Free HIPS internal structure for problem id  */
  /************************************************/
  HIPS_Context *c;
  c = context+id;

  if(c->state <= INIT)
    return HIPS_SUCCESS;

  /*if(c->mapptr != NULL)
    free(c->mapptr);
    if(c->mapp != NULL)
    free(c->mapp);*/

  PhidalCommVec_Clean(&c->A.commvec);
  PhidalDistrMatrix_Clean(&c->A);
  /*PhidalDistrMatrix_Clean(&c->T);*/

  c->flagmat = 0;


  if(c->b != NULL)
    free(c->b);

  if(c->t != NULL)
    free(c->t);

  c->flagrhs = 0;

  if(c->x != NULL)
    free(c->x);

  if(c->symbloc != NULL)
    {
      /* inside DBDistrPrec_Clean*/
      /*       SymbolMatrix_Clean(c->symbloc); */
      /*       free(c->symbloc); */

#ifdef USE_SEQ_PRECOND

/* #ifdef USE_SEQ_PRECOND */
/*       if (c->nproc == 1) { */
/*	DBPrec_Clean(&c->PblockSEQ); */
/*       } */
/*       else */
/* #endif */
#else
      if(c->precond == 1)
	DBDistrPrec_Clean(&c->Pblock);
#endif

      SymbolMatrix_Clean(c->symbloc);

      free(c->symbloc);

    }

  if(c->block_psetindex != NULL)
    free(c->block_psetindex);
  if(c->block_pset != NULL)
    free(c->block_pset);

  PhidalDistrHID_Clean(&c->DBL);
  PhidalHID_Clean(&c->BL);
  PhidalOptions_Clean(&c->options);

  if(c->hybrid ==0)
      if(c->precond == 1)
	  PhidalDistrPrec_Clean(&c->Pscal);

  c->precond = 0;

  if(c->iperm != NULL)
    free(c->iperm);



  CHECK_RETURN(HIPS_Context_Reinit(c));

  c->state = INIT;

  return HIPS_SUCCESS;
}

INTS HIPS_Finalize()
{
  /**********************************/
  /* Free HIPS internal structures  */
  /**********************************/
  dim_t i;
  for(i=0;i<idnbr;i++)
    {
      CHECK_RETURN(HIPS_Clean(i));
    }

  MPI_Op_free(&mpi_max);

  if(context != NULL)
    free(context);

  return HIPS_SUCCESS;
}

/* @authors J. GAIDAMOUR, P. HENON */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "phidal_ordering.h"
#include "symbol.h"
#include "block.h"
#include "phidal_parallel.h"


#define SIZE_INT(var)       { size++; }
#define WRITE_INT(var)       { ptbuf[0] = (INTL)(var); ptbuf++; }
#define READ_INT(type, var)  { (var) = (type)ptbuf[0]; ptbuf++; }


/**/
#define _begin_WRITE_TAB(tab, n) {            \
    if ((tab)==NULL) { WRITE_INT((-1)) } else { \
    WRITE_INT((n));                           \
    for(i=0;i<(n);i++) 
                       
#define _end_WRITE_TAB } }
/**/

#define _begin_SIZE_TAB(tab, n) {            \
    if ((tab)==NULL) {SIZE_INT((-1)) } else {	\
    SIZE_INT((n));                           \
    for(i=0;i<(n);i++)                        

#define _end_SIZE_TAB } }

/**/
#define _begin_READ_TAB(type, tab, n) {  \
  INTL scan;                             \
  READ_INT(INTL, scan);                   \
  assert(scan==-1 || scan==n);           \
  if (scan==-1) (tab) = NULL; else {     \
    (tab) = (type *)malloc(sizeof(type)*(n)); \
    assert((tab) != NULL);                    \
    for(i=0;i<(n);i++) 

#define _end_READ_TAB } } 
/**/

#define WRITE_TAB_INT(tab, n) \
 _begin_WRITE_TAB(tab, n)     \
  WRITE_INT((tab)[i])         \
 _end_WRITE_TAB

#define SIZE_TAB_INT(tab, n) \
 _begin_SIZE_TAB(tab, n)     \
 SIZE_INT((tab)[i])	     \
 _end_SIZE_TAB




#define READ_TAB_INT(type, tab, n) \
 _begin_READ_TAB(type, tab, n)     \
  READ_INT(type, (tab)[i])         \
 _end_READ_TAB



int_t PHIDAL_HIDSize(PhidalHID *BL)
{
  int_t size = 0;
  int i;

  SIZE_INT(BL->ndom);
  SIZE_INT(BL->locndom);
  SIZE_INT(BL->n);
  SIZE_INT(BL->nblock);
  SIZE_INT(BL->nlevel);
  SIZE_INT(BL->dof);

  SIZE_TAB_INT(BL->block_index,      (BL->nblock+1) );
  SIZE_TAB_INT(BL->block_levelindex, (BL->nlevel+1) );
  SIZE_TAB_INT(BL->block_keyindex,   (BL->nblock+1));
  SIZE_TAB_INT(BL->block_key,        (BL->block_keyindex[BL->nblock]));
  SIZE_TAB_INT(BL->block_domwgt,     (BL->block_levelindex[1]));

  return size;
}


INTL *PHIDAL_SerializeHID(INTL *buf, PhidalHID *BL)
{
  dim_t i;
  INTL* ptbuf;
  ptbuf = buf;
  /** If you modify this do not forget 
      to also modify PHIDAL_HIDSize **/

  WRITE_INT(BL->ndom);
  WRITE_INT(BL->locndom);
  WRITE_INT(BL->n);
  WRITE_INT(BL->nblock);
  WRITE_INT(BL->nlevel);
  WRITE_INT(BL->dof);

  WRITE_TAB_INT(BL->block_index,      (BL->nblock+1));
  WRITE_TAB_INT(BL->block_levelindex, (BL->nlevel+1));
  WRITE_TAB_INT(BL->block_keyindex,   (BL->nblock+1));
  WRITE_TAB_INT(BL->block_key,        (BL->block_keyindex[BL->nblock]));
  WRITE_TAB_INT(BL->block_domwgt,     (BL->block_levelindex[1]));

  return ptbuf;
}

INTL *PHIDAL_UnSerializeHID(INTL *buf, PhidalHID *BL)
{
  dim_t i;
  INTL* ptbuf = buf;  

  PhidalHID_Init(BL);

  READ_INT(dim_t, BL->ndom);
  READ_INT(dim_t, BL->locndom);
  READ_INT(dim_t, BL->n);
  READ_INT(int_t, BL->nblock);
  READ_INT(int_t, BL->nlevel);
  READ_INT(dim_t, BL->dof);
  
  READ_TAB_INT(dim_t, BL->block_index,      BL->nblock+1);
  READ_TAB_INT(dim_t, BL->block_levelindex, BL->nlevel+1);
  READ_TAB_INT(dim_t, BL->block_keyindex,   BL->nblock+1);
  READ_TAB_INT(dim_t, BL->block_key,        BL->block_keyindex[BL->nblock]);
  READ_TAB_INT(INTL,   BL->block_domwgt,     BL->block_levelindex[1]);

  return ptbuf;
}

int_t PHIDAL_SymbolMatrixSize(SymbolMatrix *symbmtx)
{
  int_t size = 0;
  int_t i;
  if(symbmtx == NULL)
    {
      SIZE_INT((-1));
      return size;
    }
  else
    SIZE_INT(1);

  SIZE_INT(symbmtx->cblknbr);
  SIZE_INT(symbmtx->bloknbr);
  SIZE_INT(symbmtx->nodenbr);
  SIZE_INT(0); /* symbmtx->tli,      */
  SIZE_INT(0); /* symbmtx->facedecal */
  SIZE_INT(0); /* symbmtx->cblktlj   */
  SIZE_INT(0); /* symbmtx->virtual   */
  
  _begin_SIZE_TAB(symbmtx->ccblktab, symbmtx->cblknbr) {
    SIZE_INT(symbmtx->ccblktab[i].fcolnum);
    SIZE_INT(symbmtx->ccblktab[i].lcolnum);
  } _end_SIZE_TAB;
    
  _begin_SIZE_TAB(symbmtx->bcblktab, symbmtx->cblknbr) {
    SIZE_INT(symbmtx->bcblktab[i].fbloknum);
    SIZE_INT(symbmtx->bcblktab[i].lbloknum);
  } _end_SIZE_TAB;
  
  _begin_SIZE_TAB(symbmtx->bloktab, symbmtx->bloknbr) {
    SIZE_INT(symbmtx->bloktab[i].frownum);
    SIZE_INT(symbmtx->bloktab[i].lrownum);
    SIZE_INT(symbmtx->bloktab[i].cblknum);
  } _end_SIZE_TAB
	


  return size;
}


INTL *PHIDAL_SerializeSymbolMatrix(INTL *buf, SymbolMatrix *symbmtx)
{
  dim_t i;
  INTL* ptbuf;
  ptbuf = buf;


  if(symbmtx == NULL)
    {
      WRITE_INT((-1));
      return buf;
    }
  else
    WRITE_INT(1);

  WRITE_INT(symbmtx->cblknbr);
  WRITE_INT(symbmtx->bloknbr);
  WRITE_INT(symbmtx->nodenbr);

  WRITE_INT(0); /* symbmtx->tli,      */
  WRITE_INT(0); /* symbmtx->facedecal */
  WRITE_INT(0); /* symbmtx->cblktlj   */
  WRITE_INT(0); /* symbmtx->virtual   */

  _begin_WRITE_TAB(symbmtx->ccblktab, symbmtx->cblknbr) {
    WRITE_INT(symbmtx->ccblktab[i].fcolnum);
    WRITE_INT(symbmtx->ccblktab[i].lcolnum);
  } _end_WRITE_TAB;
    
  _begin_WRITE_TAB(symbmtx->bcblktab, symbmtx->cblknbr) {
    WRITE_INT(symbmtx->bcblktab[i].fbloknum);
    WRITE_INT(symbmtx->bcblktab[i].lbloknum);
  } _end_WRITE_TAB;
  
  _begin_WRITE_TAB(symbmtx->bloktab, symbmtx->bloknbr) {
    WRITE_INT(symbmtx->bloktab[i].frownum);
    WRITE_INT(symbmtx->bloktab[i].lrownum);
    WRITE_INT(symbmtx->bloktab[i].cblknum);
   } _end_WRITE_TAB;
	
  /* for(i=0;i<=symbmtx->cblknbr;i++) */
  /*     fprintf(fp, "%d ", symbmtx->hdim[i]); */
  /*   fprintf(fp, "\n"); */
  assert(symbmtx->hdim == NULL);
  assert(symbmtx->stride == symbmtx->hdim);

  return ptbuf;
}

INTL *PHIDAL_UnSerializeSymbolMatrix(INTL *buf, SymbolMatrix** symbptr)
{
  dim_t i;
  INTL* ptbuf;
  flag_t exist;
  SymbolMatrix *symbmtx;


  ptbuf = buf;

  READ_INT(flag_t, exist);
  if(exist != -1)
    {
      *symbptr = (SymbolMatrix *)malloc(sizeof(SymbolMatrix));
      SymbolMatrix_Init(*symbptr);
    }
  else
    {
      *symbptr = NULL;
      return ptbuf;
    }
  

  symbmtx = *symbptr;

  READ_INT(dim_t, symbmtx->cblknbr);
  READ_INT(dim_t, symbmtx->bloknbr);
  READ_INT(dim_t, symbmtx->nodenbr);

  READ_INT(dim_t, symbmtx->tli);
  READ_INT(dim_t, symbmtx->facedecal);
  READ_INT(dim_t, symbmtx->cblktlj);
  READ_INT(flag_t, symbmtx->virtual);

  _begin_READ_TAB(SymbolCCblk, symbmtx->ccblktab, symbmtx->cblknbr) {
    READ_INT(dim_t, symbmtx->ccblktab[i].fcolnum);
    READ_INT(dim_t, symbmtx->ccblktab[i].lcolnum);
  } _end_READ_TAB;
    
  _begin_READ_TAB(SymbolBCblk, symbmtx->bcblktab, symbmtx->cblknbr) {
    READ_INT(INTL, symbmtx->bcblktab[i].fbloknum);
    READ_INT(INTL, symbmtx->bcblktab[i].lbloknum);
  } _end_READ_TAB;
  
  _begin_READ_TAB(SymbolBlok, symbmtx->bloktab, symbmtx->bloknbr) {
    READ_INT(dim_t, symbmtx->bloktab[i].frownum);
    READ_INT(dim_t, symbmtx->bloktab[i].lrownum);
    READ_INT(dim_t, symbmtx->bloktab[i].cblknum);
  } _end_READ_TAB;
	
  symbmtx->hdim = NULL;
  symbmtx->stride = symbmtx->hdim;

  return ptbuf;
}

dim_t PHIDAL_IpermSize(dim_t n, dim_t *iperm)
{
  dim_t size = 0;
  dim_t i;
  SIZE_TAB_INT(iperm,   n);
  return size;
}

INTL *PHIDAL_SerializeIperm(INTL *buf, dim_t n, dim_t *iperm)
{
  dim_t i;
  INTL* ptbuf = buf;

  WRITE_TAB_INT(iperm,   n);

  return ptbuf;
}

INTL *PHIDAL_UnSerializeIperm(INTL *buf, dim_t n, dim_t **pt_iperm)
{
  dim_t i;
  INTL* ptbuf = buf;

  dim_t* iperm;
  READ_TAB_INT(dim_t, iperm, n);
  *pt_iperm=iperm;

  return ptbuf;
}


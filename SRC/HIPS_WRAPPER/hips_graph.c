/* @authors P. HENON */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <mpi.h>
#include "math.h"

#include "hips_wrapper.h"
#include "hips.h"

#include "scotch_metis_wrapper.h"

extern int_t idnbr;
extern HIPS_Context *context;

/** local functions **/
INTS hips_GraphBroadcastHID(INTS id);
INTS hips_GraphSymmetrize(INTS id);


INTS HIPS_GraphPartition(INTS ndom, INTS overlap, INTS numflag, INTS n, INTL *ia, INTS *ja, INTS sym, INTS **mapptrR, INTS **mappR)
{ 
  chrono_t t1=-1, t2;
  dim_t *node2dom;
  INTS *mapptr, *mapp;
  INTL *ig;
  INTS *jg;

  CHECK_NUMFLAG;


  ig = (INTL *)malloc(sizeof(INTL)*(n+1));
  memcpy(ig, ia, sizeof(INTL)*(n+1));
  jg = (INTS *)malloc(sizeof(INTS)*ia[n]);
  memcpy(jg, ja, sizeof(INTS)*ia[n]);

  if(numflag != 0)
    CSR_Fnum2Cnum(jg, ig, n);
        
#ifdef DEBUG_M
  checkCSR(n, ig, jg, NULL, 0);
#endif
     

  if(ndom > 1) 
    {
      node2dom = (INTS *)malloc(sizeof(INTS)*n);

      if(sym == 0)
	{
	  flag_t job = 0;
	  INTL *ib;
	  dim_t *jb;
	  ib = ig;
	  jb = jg;
	  /** numflag = 0 **/
	  PHIDAL_SymmetrizeMatrix(job, 0, n, ib, jb, NULL, &ig, &jg, NULL);
	  /*fprintf(stderr, "navant %ld Napre %ld \n", (long)ib[n], (long)ig[n]);*/
	  free(ib);
	  free(jb);
	}

      PHIDAL_CsrDelDiag(0, n, ig, jg);
      
#ifdef SCOTCH_PART	  
      {
	SCOTCH_Graph        grafdat;
	SCOTCH_Strat        grafstrat;
	SCOTCH_graphInit  (&grafdat);
	/*SCOTCH_graphBuild (&grafdat, 0, n, ia, NULL, vwgt, NULL, ia[n], ja, ewgt);*/
	SCOTCH_graphBuild_WRAPPER (&grafdat, 0, n, ig, NULL,  jg, NULL);
	
	if(SCOTCH_graphCheck(&grafdat) != 0)
	  {
	    fprintfd(stderr," Error detected  by scotch graph check \n");
	    exit(-1);
	  }
	/*SCOTCH_graphSave (&grafdat, stdout);*/
	
	SCOTCH_stratInit (&grafstrat);
	
	
	/**SCOTCH_graphPart (&grafdat, c->ndom, &grafstrat, node2dom);**/
	SCOTCH_graphPart_WRAPPER(n, &grafdat, ndom, &grafstrat, node2dom);
	SCOTCH_graphExit (&grafdat);
	SCOTCH_stratExit (&grafstrat);
      } 
#else
      {
	int numflag = 0;
	int metisoption[10];
	int wgtflag = 0;
	int volume;
	wgtflag = 0;
	metisoption[0] = 0;

	assert(ig[0] == numflag);
	METIS_PartGraphVKway_WRAPPER(n, ig, jg, NULL, NULL, wgtflag, numflag, ndom, metisoption, &volume, node2dom);
	
      }
#endif

      if(overlap == 1)
	{
	  /***********************************************************************/
	  /** Transform the vertex-based partition into an edge-based partition **/
	  /***********************************************************************/
	  PHIDAL_Partition2OverlappedPartition(0, ndom, n, ig, jg, node2dom, &mapp, &mapptr);  
	}
      else
	{
	  dim_t i;
	  mapptr = (INTS *)malloc(sizeof(INTS)*(ndom+1));

	  bzero(mapptr, sizeof(INTS)*(ndom+1));
	  for(i=0;i<n;i++)
	    mapptr[node2dom[i]+1]++;
	  
	  for(i=1;i<=ndom;i++)
	    mapptr[i] += mapptr[i-1];

	  mapp = (INTS *)malloc(sizeof(INTS)*n);
	  for(i=0;i<n;i++)
	    mapp[mapptr[node2dom[i]]++] = i;

	  
	  for(i=ndom;i>0;i--)
	    mapptr[i] = mapptr[i-1]; 
	  mapptr[0] = 0;

#ifdef DEBUG_M
	  assert(mapptr[ndom] == n);
#endif
	  
	}

      free(node2dom);
    }
  else
    {
      dim_t i;
      /** ndom == 1 **/
      mapptr = (INTS *)malloc(sizeof(INTS)*2);
      mapp = (INTS *)malloc(sizeof(INTS)*n);
      mapptr[0] = numflag;
      mapptr[1] = n+numflag;
      for(i=0;i<n;i++)
	mapp[i] = i+numflag;
    }

 
  
  free(ig);
  free(jg);

  *mapptrR = mapptr;
  *mappR = mapp;
  return HIPS_SUCCESS;
}





INTS HIPS_GraphBuildHID(INTS id)
{
  dim_t *node2dom;
  INTL nnz;
  dim_t *perm, *iperm;
  INTL i, j, ind, nn;
  INTL *ig;
  dim_t *jg;

  PhidalOptions *option;
  PhidalHID *BL;
  HIPS_Context *c;
  chrono_t t1=-1, t2;
  dim_t n;

  c = context+id;

 
  if(c->graph_sym != 0)
    {
      /** Make sure the graph is symmetric **/
      CHECK_RETURN(hips_GraphSymmetrize(id));
    }
  
  if(c->proc_id != c->master)
    {
      /** Only the master processor has the graph **/
      CHECK_RETURN(hips_GraphBroadcastHID(id));

      return HIPS_SUCCESS;
    }
  
  if(c->ig == NULL || c->jg == NULL)
    {
      fprintferr(stderr, "ERROR in HIPS_GraphBuildHID : master does not own the graph \n");
      return HIPS_ERR_CALL;
    }
  
  
  BL =  &c->BL;
  option = &c->options;
  
  /*****************************************************************************************************************/
  /***********************************Construct the symmetric graph G of the matrix ********************************/
  /*****************************************************************************************************************/
  n = c->ng;
  ig = c->ig;
  jg = c->jg;
  nnz = c->ig[n];

  c->perm = (dim_t *)malloc(sizeof(dim_t)*n);
  c->iperm = (dim_t *)malloc(sizeof(dim_t)*n);
  CHECK_MALLOC(c->perm);
  CHECK_MALLOC(c->iperm);

  perm =  c->perm;
  iperm =  c->iperm;

  /********************************************************/
  /* Compute a  vertex-based partition                    */
  /* then compute an edge-based partition wich ovelapps   */
  /* on the vertex separator                              */
  /********************************************************/
  node2dom = perm; /** use perm as a temporary working area **/

  /** Delete the self edge in the graph (METIS_NodeND need that)**/
  /** OIMBE Put THAT IN Perm2SizedDomain ?? **/
  PHIDAL_CsrDelDiag(0, n, ig, jg);

  if(c->partition_type <= 0)
    {
      switch(c->partition_type)
	{
	case 0:
	  if(c->domsize <=0)
	    {
	      fprintf(stderr, "You are trying to use the HYBRID strategy with no domain size parameter (HIPS_SetOptionINT with value associated to HIPS_DOMSIZE) : \n you can do this ONLY in the ITERATIVE strategy.\n");
	      fprintf(stderr, "Another possible cause of error is to call HIPS_SetPartition() and then use the HYBRID solver strategy : the function HIPS_SetPartition() should only be called with the ITERATIVE solver for now.\n");
	      fprintf(stderr, "If you want to use a data distribution different from HIPS in the HYBRID solver, you should use HIPS_ASSEMBLY_FOOL in the interface \n");
	      return HIPS_ERR_PARAMETER;
	    }
	  /**** Compute the overlapped partition from a matrix reordering   *****/
	  PHIDAL_Perm2SizedDomains(c->domsize, n, ig, jg, &c->ndom,
				   &c->mapptr, &c->mapp, perm, iperm);
	  break;
	  
	  
	case -1:
	  if(c->cube == 1)
	    PHIDAL_GridRegularSizeDomains(c->domsize, c->nc, c->nc, c->nc, n, ig, jg, &c->ndom, &c->mapptr, &c->mapp, perm, iperm);
	  else
	    PHIDAL_GridRegularSizeDomains(c->domsize, c->nc, c->nc, 1, n, ig, jg, &c->ndom, &c->mapptr, &c->mapp, perm, iperm); 
	  
	  break;
	default :
	  fprintferr(stderr, "Error wrong partition type \n");
	  return HIPS_ERR_PARAMETER;
	}
    }
  else
    {
      dim_t *node2dom;
      node2dom = perm; /** use perm as a working vector **/
      
#ifdef DEBUG_M
      assert(c->ndom >= 1);
#endif
      /*********************************************/
      /** DOMAIN PARTITION not from reordering    **/
      /*********************************************/
      switch(c->partition_type)
	{
	case 1: /** Non overlapped partition **/
	  bzero(iperm, sizeof(dim_t)*n);
	  
	  /** Check the partition **/
#ifdef DEBUG_M
	  for(i=0;i<c->ndom;i++)
	    assert(c->mapptr[i+1]>c->mapptr[i]);
	  for(i=0;i<c->mapptr[c->ndom];i++)
	    assert(c->mapp[i] >= 0 && c->mapp[i]<n);
#endif

	  for(i=0;i<c->ndom;i++)
	    for(j=c->mapptr[i];j<c->mapptr[i+1];j++)
	      iperm[c->mapp[j]]++;


	  for(i=0;i<n;i++)
	    node2dom[i] = -1;
	  
	  for(i=0;i<c->ndom;i++)
	    for(j=c->mapptr[i];j<c->mapptr[i+1];j++)
	      {
#ifdef DEBUG_M
		assert(iperm[c->mapp[j]] == 1);
#endif
		node2dom[c->mapp[j]] = i;
	      }


	  free(c->mapptr);
	  c->mapptr = NULL;
	  free(c->mapp);
	  c->mapp = NULL;

	  break;
	case 2: /** Overlapped partition **/
	  /*** DO NOTHING SHOULD WORK BUT BIG SEPARATOR SO ...*****/
	  /** Modify mapp and mapptr to give only the interior domain **/

#ifdef DEBUG_M
	  for(i=0;i<c->ndom;i++)
	    assert(c->mapptr[i+1]>c->mapptr[i]);
	  for(i=0;i<c->mapptr[c->ndom];i++)
	    assert(c->mapp[i] >= 0 && c->mapp[i]<n);
#endif


	  bzero(iperm, sizeof(dim_t)*n);
	  for(i=0;i<c->ndom;i++)
	    for(j=c->mapptr[i];j<c->mapptr[i+1];j++)
	      iperm[c->mapp[j]]++;

	  ind = 0;
	  for(i=0;i<c->ndom;i++)
	    {
	      nn = ind;
	      for(j=c->mapptr[i];j<c->mapptr[i+1];j++)
		{
#ifdef DEBUG_M
		  assert(iperm[c->mapp[j]] >= 0 && iperm[c->mapp[j]]<=c->ndom);
#endif
		  if(iperm[c->mapp[j]] == 1)
		    c->mapp[ind++] = c->mapp[j];
		}
	      c->mapptr[i] = nn;
	    }
	  c->mapptr[c->ndom] = ind;

	  /** Construct the overlap **/
	  get_overlap(c->ndom, n, &c->mapptr, &c->mapp, ig, jg);
	  break;
	case 3: /** Compute the non overlapped partition **/
	  {
	    /******************************************/
	    /* Compute the non-overlapped partition   */
	    /******************************************/
	    if(c->ndom > 1) 
	      {

#ifdef SCOTCH_PART	  
		{
		  SCOTCH_Graph        grafdat;
		  SCOTCH_Strat        grafstrat;
		  SCOTCH_graphInit  (&grafdat);
		  /*SCOTCH_graphBuild (&grafdat, 0, n, ia, NULL, vwgt, NULL, ia[n], ja, ewgt);*/
		  SCOTCH_graphBuild_WRAPPER (&grafdat, 0, n, ig, NULL,  jg, NULL);
		  
		  if(SCOTCH_graphCheck(&grafdat) != 0)
		    {
		      fprintfd(stderr," Error detected  by scotch graph check \n");
		      exit(-1);
		    }
		  /*SCOTCH_graphSave (&grafdat, stdout);*/
		
		  SCOTCH_stratInit (&grafstrat);
		  if(option->verbose >= 2)
		    {
		      fprintfv(3, stdout, "Partition the graph using SCOTCH \n");
		      t1  = dwalltime(); 
		    }
		  

#ifdef SCOTCH_OVERLAP
		  SCOTCH_graphPartOvl_WRAPPER(n, &grafdat, c->ndom, &grafstrat, node2dom);
#else
		  /**SCOTCH_graphPart (&grafdat, c->ndom, &grafstrat, node2dom);**/
		  SCOTCH_graphPart_WRAPPER(n, &grafdat, c->ndom, &grafstrat, node2dom);
#endif
		  SCOTCH_graphExit (&grafdat);
		  SCOTCH_stratExit (&grafstrat);

		  if(option->verbose >= 2)
		    {
		      t2  = dwalltime(); 
		      fprintfv(3, stdout, "partition done in %g s \n", t2-t1);
		    }
		} 
#else
		{
		  int numflag = 0;
		  int metisoption[10];
		  int wgtflag = 0;
		  int volume;
		  wgtflag = 0;
		  metisoption[0] = 0;
		  
		  if(option->verbose >= 3)
		    {
		      t1  = dwalltime(); 
		      fprintfv(3, stdout, "Partition the graph using METIS \n");
		    }
		  assert(ig[0] == numflag);
		  /*METIS_PartGraphVKway(&n, ig, jg, NULL, NULL, &wgtflag, &numflag, &c->ndom, metisoption, &volume, node2dom);*/
		  METIS_PartGraphVKway_WRAPPER(n, ig, jg, NULL, NULL, wgtflag, numflag, c->ndom, metisoption, &volume, node2dom);

		  if(option->verbose >= 3)
		    {
		      t2  = dwalltime(); 
		      fprintfv(3, stdout, "partition done in %g s \n", t2-t1);
		    }
		}
#endif
	      }
	    else 
	      /** ndom == 1 **/
	      for(i = 0; i < n; i++) 
		node2dom[i] = 0;
#ifndef SCOTCH_OVERLAP	  
	    for(i=0;i<n;i++)
	      assert(node2dom[i] >= 0 && node2dom[i]<c->ndom);
#else
	    for(i=0;i<n;i++)
	      assert(node2dom[i] == -1 || (node2dom[i] >= 0 && node2dom[i]<c->ndom));
#endif
	  
	  }
	  break;
	default:
	  fprintfd(stderr, "ERROR : wrong partition type \n");
	  return HIPS_ERR_PARAMETER;
	}
      
      
      switch(c->partition_type)
	{
	case 1: 
	  PHIDAL_Partition2OverlappedPartition(0, c->ndom, n, ig, jg, node2dom, &c->mapp, &c->mapptr);  
	  break;
	case 3:
#if defined(SCOTCH_PART) && defined(SCOTCH_OVERLAP)
	  PHIDAL_Interior2OverlappedPartition(0, c->ndom, n, ig, jg, node2dom, &c->mapp, &c->mapptr);  
#else
	  PHIDAL_Partition2OverlappedPartition(0, c->ndom, n, ig, jg, node2dom, &c->mapp, &c->mapptr);  
#endif
	  break;
	}

      


      for(i=0;i<n;i++)
	{
	  perm[i] = i;
	  iperm[i] = i;
	} 
    }
  
  /************************************************/
  /* Compute the Hierarchical Graph Decomposition */
  /************************************************/

  /** Compute the global hierarchical interface decomposition and the associated permutation **/ 
  PhidalHID_Init(BL);
  if(c->dof <= 0)
    {
      fprintferr(stderr, "ERROR in HIPS_HierarchGraphDecomp : DOF == %ld is not valid \n", (long)c->dof);
      exit(-1);
    }
  BL->dof = c->dof;

  t1  = dwalltime(); 
  PHIDAL_HierarchDecomp(option->verbose, 0, n, ig, jg, c->mapp, c->mapptr, c->ndom, BL, perm, iperm);
  t2  = dwalltime(); 
  if(c->proc_id == 0)
    fprintfv(3, stdout, " Compute Hierarchical Interface Decomposition in %g seconds \n\n", t2-t1);
  

  if(c->improve_partition == 1)
    {
      /** En commentaire pour debug **/ 
      /*free(c->mapp);
	free(c->mapptr);*/
      t1  = dwalltime(); 
      Improve_Partition(option->verbose, c->tagnbr, n, ig, jg, &c->mapp, &c->mapptr, &c->ndom, BL, perm, iperm);
      t2  = dwalltime(); 
      if(c->proc_id == 0)
	fprintfv(3, stdout, " Improve partition in %g seconds \n\n", t2-t1);
  
    }


  if(c->proc_id == 0)
    if(option->verbose >= 4)
      {
	fprintfv(4, stdout, "\n Hierarchical Interface Decomposition INFO \n");
	HID_Info(stdout, BL);
      }

  
#ifndef PARALLEL_LOCAL_ORDERING
  if(c->partition_type != 0 && c->hybrid == 0)
    {
#ifdef DEBUG_M
      
      /*assert(c->nproc == c->ndom); non pour le save */
#endif
      if(c->reorder == 1)
	{
	  dim_t i;
	  int fillopt[10];
	  
	  /***OIMBE  VRAIMENT  BESOIN DE CA ???  A VERIFIER **/
	  CSR_Perm(n, ig, jg, NULL, perm);
	  /*fprintfd(stderr, "OIMBE PROBLEM A REGLER EN PARALLEL : ordering mauvais par rapport a ordering local sur chaque processeur \n");*/

	  /*** OIMBE je force en MD car en ND c'est pas bon : pb ???? ***/
	  fillopt[0] = 1; /** 0 ND, 1 MD, 2 MF **/

	  t1  = dwalltime(); 
	  PHIDAL_MinimizeFill(fillopt, n, ig, jg, BL, iperm);
	  
	  for(i=0;i<n;i++)
	    perm[iperm[i]] = i;

	  t2  = dwalltime(); 
	  
	  /** METRIC **/
	  fprintfv(4, stdout, "Reordering of interior domain in %g seconds \n\n", t2-t1);

	}
    }
#endif

  if(c->coarse_grid != 0)
    {
      HID_BuildCoarseLevel(c->coarse_grid-1, BL, perm, iperm);
    }
  
  /*if(c->proc_id == 0)
    if(option->verbose >= 2)
      {
	fprintfv(2, stdout, "\n Hierarchical Interface Decomposition INFO AFTER COARSE LEVEL \n");
	
	HID_Info(stdout, BL);
	}*/
  
  /* fprintfv(5, stderr, "BL->dof = %d \n",BL->dof); */

  /** DO NOT NEED THESE VECTORS ANYMORE **/
  if(c->mapp != NULL)
    free(c->mapp);
  if(c->mapptr != NULL)
    free(c->mapptr);


  /**/



  /* TMP */
  if ((option->symmetric == 0) && (c->hybrid == 1 || c->options.droptol0 == 0) && (BL->nlevel == 1)) {
    fprintferr(stderr, "Warning : BLAS version not yet implemented...\\ calling scalar version (options : unsym matrix + droptol0=0 + one level)\n");
    option->droptol0 = -1;
  }


  if(c->hybrid == 1)
    {
      /****************************************/
      /*** Compute the global symbol matrix ***/
      /****************************************/
      fprintfv(3, stdout, "Build Symbolic Matrix \n");
      t1  = dwalltime(); 
      c->symbmtx = (SymbolMatrix*)malloc(sizeof(SymbolMatrix));
      if(option->schur_method != 1)
	HIPS_SymbolMatrix(option->verbose, 0, option->amalg_rat, option->locally_nbr, n,  ig,  jg, 
			  BL, c->symbmtx, perm, iperm);
      else
	HIPS_SymbolMatrix(option->verbose, 0, option->amalg_rat, BL->nlevel, n,  ig,  jg, 
			  BL, c->symbmtx, perm, iperm);
      
      t2  = dwalltime(); 

      /** METRIC **/
      fprintfv(3, stdout, "symbolic factorization done in %g s\n", t2-t1); 
      
      /* **** */
      
      HIPS_CountNNZ_L(1, c->symbmtx, BL);
	  
      /** OIMBE : se debarrasser de ig, jg qui est dupliqué dans cette fonction !!
	  (transformée en Sparrow) **/
      /*HIPS_CountNNZ_E(n,  ig,  jg, perm, BL);*/
      /* **** */  
    }

  /** Indicate that the global pretreatment has been done **/
  c->state = HIERARCH;

#ifndef PARALLEL_LOCAL_ORDERING
  free(c->ig);
  c->ig = NULL; /** Important (tested elsewhere) **/
  free(c->jg); 
  c->jg = NULL; /** Important (tested elsewhere) **/
#endif
  free(c->perm); /** iperm is freed in Parallel Setup **/


  /*** Broadcast the hid, iperm and symbol matrix  **/
  CHECK_RETURN(hips_GraphBroadcastHID(id));

  return HIPS_SUCCESS;
}


INTS hips_GraphBroadcastHID(INTS id)
{
  /************************************************************/
  /* Send the HID and the symbol matrix to the non-master     */
  /* processors                                               */
  /************************************************************/
  HIPS_Context *c;
  INTL *buff;
  INTL *bufptr;
  mpi_t size;
  
  c = context+id;

  if(c->nproc <= 1)
    return HIPS_SUCCESS;


  if(c->proc_id == c->master)
    {

      assert(c->state >= HIERARCH);

      /** Pack the global HID and the symbol Matrix **/
      size = PHIDAL_HIDSize(&c->BL);
      size += PHIDAL_IpermSize(c->ng, c->iperm); /** iperm size **/
      size += (mpi_t)PHIDAL_SymbolMatrixSize(c->symbmtx);

      MPI_Bcast(&size, EE(1), COMM_INT, c->master, c->mpicom);
      /*fprintfd(stderr, "Proc %ld size = %ld \n", (long)c->proc_id, (long)size);*/
      
      buff = (INTL *)malloc(sizeof(INTL)*(size));

      CHECK_MALLOC(buff);

      bufptr  = buff;

      bufptr = PHIDAL_SerializeHID(bufptr, &c->BL);
      bufptr = PHIDAL_SerializeIperm(bufptr, c->ng, c->iperm);
      bufptr = PHIDAL_SerializeSymbolMatrix(bufptr, c->symbmtx); 

      MPI_Bcast(buff, FF(size), COMM_BIG_INT, c->master, c->mpicom);

      free(buff);
    }
  else
    {
      MPI_Bcast(&size, EE(1), COMM_INT, c->master, c->mpicom);
      
      buff = (INTL *)malloc(sizeof(INTL)*(size));
      /*fprintfd(stderr, "Proc %ld size = %ld \n", (long)c->proc_id, (long)FF(size));*/
      MPI_Bcast(buff, FF(size), COMM_BIG_INT, c->master, c->mpicom);


      bufptr = buff;
      bufptr = PHIDAL_UnSerializeHID(bufptr, &c->BL);
      c->ng = c->BL.n;
      c->dof = c->BL.dof;
      c->na = c->dof * c->ng;
      bufptr =  PHIDAL_UnSerializeIperm(bufptr, c->ng, &c->iperm);
      bufptr =  PHIDAL_UnSerializeSymbolMatrix(bufptr, &c->symbmtx);
      
      free(buff);

      /** Indicate that the global pretreatment has been done **/
      c->state = HIERARCH;
    }

  return HIPS_SUCCESS;

} 
  

INTS HIPS_GraphBegin(INTS id, INTS n, INTL edgenbr)
{
  HIPS_Context *c;
  c = context+id;

  /** Destroy the old graph **/
  if(c->ig != NULL)
    free(c->ig);

  if(c->jg != NULL)
    free(c->jg);
  
  if(c->state > INIT)
    {
      CHECK_RETURN(HIPS_Clean(id));
    }


  c->edgenbr = edgenbr;
  c->ind = 0;
  c->ng = n;
  c->na = n*c->dof;

  if(c->edgenbr>0)
    {
      c->ii = (dim_t *)malloc(sizeof(dim_t)*c->edgenbr);
      CHECK_MALLOC(c->ii);
      c->jj = (dim_t *)malloc(sizeof(dim_t)*c->edgenbr);
      CHECK_MALLOC(c->jj);
    }

  c->state = GRAPH_BEGIN;
  return HIPS_SUCCESS;
}


INTS HIPS_GraphEdge(INTS id, INTS i, INTS j)
{
  HIPS_Context *c;
  c = context+id;
  /*if((c->ii == NULL) || (c->jj == NULL))*/
  if((c->state < GRAPH_BEGIN ) || (c->state >= GRAPH_END))
    {
      fprintferr(stderr, "HIPS_GraphEdge ERROR : you must call HIPS_GraphBegin before \n");
      return HIPS_ERR_CALL;
    }

  if(c->ind >= c->edgenbr)
    {
      fprintferr(stderr, "HIPS_GraphEdge ERROR : edgenbr %ld is not large enough in HIPS_GraphBegin  \n", (long)c->edgenbr);
      return HIPS_ERR_CALL;
    }

  c->ii[c->ind] = i - c->numflag;
  c->jj[c->ind] = j - c->numflag;
  c->ind++;
  return HIPS_SUCCESS;
}

INTS HIPS_GraphEnd(INTS id)
{
  HIPS_Context *c;
  c = context+id;

  /** Convert ijv to csr (the double edge are suppressed in the process **/
  ijv2csr(c->ng, c->ind, c->ii, c->jj, NULL, &c->ig, &c->jg, NULL);

  if(c->ii != NULL)
    free(c->ii);
  
  if(c->jj != NULL)
    free(c->jj);
  
  /** Gather the graph on the master processor **/
  if(c->proc_id == c->master)
    CSR_Reduce(0, 1, c->master, c->ng, c->ig, c->jg, NULL, &c->ig, &c->jg, NULL, c->mpicom);
  else
    {
      /** c->ig and c->jg are deallocated in this function **/
      CSR_Reduce(0, 1, c->master, c->ng, c->ig, c->jg, NULL, NULL, NULL, NULL, c->mpicom);
      c->ig = NULL;
      c->jg = NULL;
    }

  c->state = GRAPH_END;
  return HIPS_SUCCESS;
}

INTS HIPS_GraphGlobalIJV(INTS id, INTS N, INTL NNZ, INTS *ROW, INTS *COL, INTS root)
{
  INTL *ig;
  INTS *jg;
  /** Convert ijv to csr (the double edge are suppressed in the process **/
  ijv2csr(N, NNZ, ROW, COL, NULL, &ig, &jg, NULL);
  CHECK_RETURN(HIPS_GraphGlobalCSR(id, N, ig, jg,  root));
  free(ig);
  free(jg);
  return HIPS_SUCCESS;

}


INTS HIPS_GraphDistrCSR(INTS id, INTS n, INTS ln, INTS *nodelist, INTL *lig, INTS *jg)
{
  INTL *ig2;
  INTS *jg2;
  HIPS_Context *c;
  dim_t i;
  c = context+id;

  c->na = n*c->dof;

  /** Put unknownlist in C numbering **/
  if(c->numflag != 0)
    {
      for(i=0;i<ln;i++)
	nodelist[i]--;

      CSR_Cnum2Fnum(jg, lig, ln);
    }
  
  c->ng = n;

  /** Convert the matrix to fit CSR_Reduce prototype **/
  /** OIMBE : it will be better to change CSR_Reduce ...one day**/
  CHECK_RETURN(DistrCSR_Loc2Glob(ln, nodelist, lig, jg, NULL, c->ng, &c->ig, &c->jg, NULL));

  /** Gather the graph on the master processor **/
  if(c->proc_id == c->master)
    CSR_Reduce(0, 1, c->master, c->ng, c->ig, c->jg, NULL, &c->ig, &c->jg, NULL, c->mpicom);
  else
    {
      /** c->ig and c->jg are deallocated in this function **/
      CSR_Reduce(0, 1, c->master, c->ng, c->ig, c->jg, NULL, NULL, NULL, NULL, c->mpicom);
      c->ig = NULL;
      c->jg = NULL;
    }


  if(c->numflag != 0)
    {
      for(i=0;i<ln;i++)
	nodelist[i]++;
      
      CSR_Fnum2Cnum(jg, lig, n);
    }

  c->state = GRAPH_END;
  return HIPS_SUCCESS;
}

INTS HIPS_GraphGlobalCSC(INTS id, INTS N, INTL *colptr, INTS *ROWS, INTS root)
{
  return HIPS_GraphGlobalCSR(id, N, colptr, ROWS, root);
}

INTS HIPS_GraphGlobalCSR(INTS id, INTS n, INTL *ig, INTS *jg,  INTS root)
{
  INTL nnz;
  HIPS_Context *c;
  INTL i;
  c = context+id;

  if(c->state > INIT)
    {
      CHECK_RETURN(HIPS_Clean(id));
    }
  
  c->master = root;
  if(c->proc_id != root)
    {
      /*if(ig != NULL || jg != NULL)
	fprintfd(stderr, "HIPS_GraphSetGlobalCSR : proc %ld the global graph is only need on the master proc %ld \n", (long) c->proc_id, (long) c->master);*/
      return HIPS_SUCCESS;
    }

  
  nnz = ig[n];
  c->ng = n;
  c->na = n*c->dof;
  c->ig = (INTL *)malloc(sizeof(INTL)*(n+1));
  CHECK_MALLOC(c->ig);
  c->jg = (dim_t *)malloc(sizeof(dim_t)*nnz);
  CHECK_MALLOC(c->jg);

  memcpy(c->ig , ig, sizeof(INTL)*(n+1));
  
  if(sizeof(dim_t) == sizeof(INTS))
    memcpy(c->jg , jg, sizeof(dim_t)*nnz);
  else
    for(i=0;i<nnz;i++)
      c->jg[i] = (dim_t)jg[i];
  
  /* Translate matrix into C numbering */
  if(c->numflag != 0)
    CSR_Fnum2Cnum(c->jg, c->ig, n);
  
  if(c->check_graph != 0)
    CSR_SuppressDouble(1, n, c->ig, c->jg, NULL);
  /** Realloc **/
  nnz = ig[n];
  c->jg = (dim_t *)realloc(c->jg, sizeof(dim_t)*nnz);
  CHECK_MALLOC(c->jg);
 
  c->state = GRAPH_END;

  return HIPS_SUCCESS;
}


INTS hips_GraphSymmetrize(INTS id)
{
  HIPS_Context *c;
  INTL *ib;
  dim_t *jb;
  int job;

  c = context+id;

  if(c->proc_id != c->master)
    return HIPS_SUCCESS;

  if(c->state < GRAPH_END)
    {
      fprintferr(stderr, "ERROR in HIPS_GraphSymmetrize : no graph to symmetrize \n");
      exit(-1);
    }

  ib = c->ig;
  jb = c->jg;
  job = 0;

#ifdef DEBUG_M
  checkCSR(c->ng, c->ig, c->jg, NULL, 0);
#endif

  /** numflag = 0 **/
  PHIDAL_SymmetrizeMatrix(job, 0, c->ng, ib, jb, NULL, &c->ig, &c->jg, NULL);

  free(ib);
  free(jb);

  return HIPS_SUCCESS;
}



/* @authors P. HENON */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <mpi.h>
#include "math.h"

#include "hips_wrapper.h"
#include "hips.h"

#include "scotch_metis_wrapper.h"

extern int_t idnbr;
extern HIPS_Context *context;
extern MPI_Op mpi_max;

INTS HIPS_SetLocalRHS(INTS id, COEF *b, INTS op, INTS op2)
{
  /*******************************************/
  /* op = 0 : do not add to previous rhs     */                      
  /* op = 1 : add to previous rhs            */           
  /* op2 = 0 : do not add on overlapped      */                      
  /* op2 = 1 : add on overlapp               */                      
  /*******************************************/
  HIPS_Context *c;
  dim_t i;
  c = context+id;

  if(c->state < SETUP)
    CHECK_RETURN(HIPS_ParallelSetup(id));

  if(c->b == NULL)
    {
      c->b = (COEF *)malloc(sizeof(COEF)*c->ln);
      CHECK_MALLOC(c->b);
    }

  if(c->t == NULL)
    {
      c->t = (COEF *)malloc(sizeof(COEF)*c->ln);
      CHECK_MALLOC(c->t);
    }

  memcpy(c->t, b, sizeof(COEF)*c->ln);


  /*PhidalDistrVec_Reduce(op2, c->t, &c->A, &c->DBL);*/
  
  /*** DEBUGGG ***/
  /*for(i=0;i<c->ln;i++)
    if(c->t[i] != b[i])
      {
	fprintferr(stderr, "PROC %d, i %d b = "_coef_" t= "_coef_"\n",c->proc_id, i, pcoef(b[i]), pcoef(c->t[i]));
	exit(0);
	}*/

  /***************/
  switch(op)
    {
    case HIPS_ASSEMBLY_OVW:
      memcpy(c->b, c->t, sizeof(COEF)*c->ln);
      break;
      
    case HIPS_ASSEMBLY_ADD:
      for(i=0;i<c->ln;i++)
	c->b[i] += c->t[i];
      break;
    default:
      return HIPS_ERR_PARAMETER;
    }
  
  c->flagrhs = 1;
  return HIPS_SUCCESS;
}


INTS HIPS_SetRHS(INTS id, INTS unknownnbr, INTS *unknownlist, COEF *b, 
		 INTS op, INTS op2, INTS mode)
{
  HIPS_Context *c;
  dim_t i;
  PhidalDistrHID *DBL;
  c = context+id;
  DBL = &c->DBL;
  
  if(c->state < SETUP)
    CHECK_RETURN(HIPS_ParallelSetup(id));

  if(c->b == NULL)
    {
      c->b = (COEF *)malloc(sizeof(COEF)*c->ln);
      CHECK_MALLOC(c->b);
    }
  
  if(c->t == NULL)
    {
      c->t = (COEF *)malloc(sizeof(COEF)*c->ln);
      CHECK_MALLOC(c->t);
    }
  
  if(c->numflag == 1)
    for(i=0;i<unknownnbr;i++)
      unknownlist[i]--;


  
  switch(mode)
    {
    case HIPS_ASSEMBLY_RESPECT:

      for(i=0;i<unknownnbr;i++)
	if(DBL->orig2loc[unknownlist[i]]<0)
	  {
	    if(c->numflag == 1)
	      for(i=0;i<unknownnbr;i++)
		unknownlist[i]++;
	    
	    return HIPS_ERR_PARAMETER;
	  }

      bzero(c->t, sizeof(COEF)*c->ln);

      for(i=0;i<unknownnbr;i++)
	c->t[DBL->orig2loc[unknownlist[i]]] = b[i];
      
      PhidalDistrVec_Reduce(op2, c->t, &c->A, &c->DBL);
      
      switch(op)
	{
	case HIPS_ASSEMBLY_OVW:
	  /** Only the value in unknownlist are modified **/
	  for(i=0;i<unknownnbr;i++)
	    c->b[DBL->orig2loc[unknownlist[i]]] =  c->t[DBL->orig2loc[unknownlist[i]]]; 
      	  break;
	  
	case HIPS_ASSEMBLY_ADD:
	  /** Only the value in unknownlist are modified **/
	  for(i=0;i<unknownnbr;i++)
	    c->b[DBL->orig2loc[unknownlist[i]]] += c->t[DBL->orig2loc[unknownlist[i]]]; 
	  break;
	default:
	  if(c->numflag == 1)
	    for(i=0;i<unknownnbr;i++)
	      unknownlist[i]++;

	  return HIPS_ERR_PARAMETER;
	}
  
      break;

    case HIPS_ASSEMBLY_FOOL:
      {
	COEF *bg, *bout;
	bg = (COEF *)malloc(sizeof(COEF)*c->na);
	CHECK_MALLOC(bg);
	bout = (COEF *)malloc(sizeof(COEF)*c->na);
	CHECK_MALLOC(bout);

	bzero(bg, sizeof(COEF)*c->na);
	for(i=0;i<unknownnbr;i++)
	  bg[unknownlist[i]] = b[i];
	
	switch(op2)
	  {
	  case HIPS_ASSEMBLY_ADD:
	    MPI_Allreduce(bg, bout, CC(c->na), MPI_COEF_TYPE, MPI_SUM, c->mpicom);
	    break;
	    
	  case HIPS_ASSEMBLY_OVW:
	    MPI_Allreduce(bg, bout, CC(c->na), MPI_COEF_TYPE, mpi_max, c->mpicom);
	    break;

	  default:
	    if(c->numflag == 1)
	      for(i=0;i<unknownnbr;i++)
		unknownlist[i]++;

	    return HIPS_ERR_PARAMETER;
	  }
	free(bg);

	switch(op)
	  {
	  case HIPS_ASSEMBLY_OVW:
	    for(i=0;i<c->DBL.LHID.n;i++)
	      c->b[i] = bout[DBL->loc2orig[i]];
	    break;

	  case HIPS_ASSEMBLY_ADD:
	    for(i=0;i<c->DBL.LHID.n;i++)
	      c->b[i] += bout[DBL->loc2orig[i]];
	    break;
	  }
	free(bout);
      }
      break;
    default:
      if(c->numflag == 1)
	for(i=0;i<unknownnbr;i++)
	  unknownlist[i]++;

      return HIPS_ERR_PARAMETER;
    }
  c->flagrhs = 1;

  if(c->numflag == 1)
    for(i=0;i<unknownnbr;i++)
      unknownlist[i]++;

  return HIPS_SUCCESS;
}



INTS HIPS_SetGlobalRHS(INTS id, COEF *b, INTS proc_root, INTS op)
{
  /**** Processor root has the global rhs and scatter it through other processors ****/
  HIPS_Context *c;
  dim_t i;
  mpi_t p;
  dim_t ln;
  c = context+id;

  if(c->state < SETUP)
    {
      CHECK_RETURN(HIPS_ParallelSetup(id));
    }

  if(c->b == NULL)
    {
      c->b = (COEF *)malloc(sizeof(COEF)*c->ln);
      CHECK_MALLOC(c->b);
    }


  if(c->proc_id == proc_root)
    {  
      for(p=0;p<c->nproc;p++)
	{
	  dim_t *unknownlist;
	  COEF *lrhs;

	  /** Build the list of unknown for processor p **/
	  GetProcUnknowns(p, c->DBL.dom2proc, &c->BL, c->block_psetindex, c->block_pset, c->iperm, &ln,  &unknownlist);

	  if(p != proc_root)
	    {
	      lrhs = (COEF *)malloc(sizeof(COEF)*ln);
	      CHECK_MALLOC(lrhs);
	      switch(op)
		{
		case HIPS_ASSEMBLY_OVW :
		  for(i=0;i<ln;i++)
		    lrhs[i] = b[unknownlist[i]];
		  break;
		case HIPS_ASSEMBLY_ADD :
		  for(i=0;i<ln;i++)
		    lrhs[i] += b[unknownlist[i]];
		  break;
		  /*case HIPS_ASSEMBLY_MAX :
		  for(i=0;i<ln;i++)
		    lrhs[i] = MAX(lrhs[i], b[unknownlist[i]]);
		  break;
		case HIPS_ASSEMBLY_MIN :
		  for(i=0;i<ln;i++)
		    lrhs[i] = MIN(lrhs[i], b[unknownlist[i]]);
		    break;*/
		default:
		  return HIPS_ERR_PARAMETER;
		}
		  
	      MPI_Send(lrhs, CC(ln), MPI_COEF_TYPE, p, 1, c->mpicom);
	      free(lrhs);
	    }
	  else
	    { 
	      switch(op)
		{
		case HIPS_ASSEMBLY_OVW :
		  for(i=0;i<ln;i++)
		    c->b[i] = b[unknownlist[i]];
		  break;
		case HIPS_ASSEMBLY_ADD :
		  for(i=0;i<ln;i++)
		    c->b[i] += b[unknownlist[i]];
		  break;
		  /*case HIPS_ASSEMBLY_MAX :
		  for(i=0;i<ln;i++)
		    c->b[i] = MAX(c->b[i], b[unknownlist[i]]);
		  break;
		case HIPS_ASSEMBLY_MIN :
		  for(i=0;i<ln;i++)
		    c->b[i] = MIN(c->b[i], b[unknownlist[i]]);
		    break;*/
		default:
		  return HIPS_ERR_PARAMETER;
		}
	    }
	  free(unknownlist);
	}
    }
  else
    {
       /** receive the local matrix **/
      MPI_Status status;
      ln = c->DBL.LHID.n;
      MPI_Recv(c->b, CC(ln), MPI_COEF_TYPE, proc_root, 1, c->mpicom, &status);
    }

  /*PhidalDistrVec_Scatter(proc_root, b, proc_id, c->b, &c->A, &c->DBL);*/

  c->flagrhs = 1;
  return HIPS_SUCCESS;
}


INTS HIPS_GetLocalSolution(INTS id, COEF *x)
{
  HIPS_Context *c;
  c = context+id;
  if(c->flagrhs == 1)
    {
      CHECK_RETURN(HIPS_Solve(id));
    }

  memcpy(x, c->x, sizeof(COEF)*c->ln);
  return HIPS_SUCCESS;
}


INTS HIPS_GetGlobalSolution(INTS id,  COEF *globx, INTS proc_root)
/*** Only the root proc gets the global sol ***/
{

  HIPS_Context *c;
  INTS i;
  c = context+id;
  
  if(c->flagrhs == 1)
    HIPS_Solve(id);

  if(c->nproc == 1)
    {
      for(i=0;i<c->na;i++)
	globx[c->DBL.loc2orig[i]] = c->x[i];
      return HIPS_SUCCESS;
    }

  if(proc_root != -1)
    PhidalDistrVec_Gather(proc_root, globx, c->x, &c->DBL);
  else
    {
      if(globx == NULL)
	return HIPS_ERR_PARAMETER;
      PhidalDistrVec_Gather(c->master, globx, c->x, &c->DBL);
      MPI_Bcast(globx, CC(c->na), MPI_COEF_TYPE, c->master, c->mpicom);
    }
  return HIPS_SUCCESS;
}


INTS HIPS_GetSolution(INTS id, INTS unknownnbr, INTS *unknownlist, COEF *x, INTS mode)
{
  HIPS_Context *c;
  dim_t i;
  PhidalDistrHID *DBL;
  c = context+id;
  DBL = &c->DBL;
  
  if(c->flagrhs == 1)
    HIPS_Solve(id);

  if(c->numflag == 1)
    for(i=0;i<unknownnbr;i++)
      unknownlist[i]--;

  switch(mode)
    {
    case HIPS_ASSEMBLY_RESPECT:
      for(i=0;i<unknownnbr;i++)
	if(DBL->orig2loc[unknownlist[i]]>0)
	  {
	    if(c->numflag == 1)
	      for(i=0;i<unknownnbr;i++)
		unknownlist[i]++;

	    return HIPS_ERR_PARAMETER;
	  }
      
      for(i=0;i<unknownnbr;i++)
	x[i] = c->x[DBL->orig2loc[unknownlist[i]]];
      break;
    
    case HIPS_ASSEMBLY_FOOL:
      {
	COEF *xg;
	xg = (COEF *)malloc(sizeof(COEF)*c->na);
	CHECK_MALLOC(xg);

	bzero(xg, sizeof(COEF)*c->na);

	CHECK_RETURN(HIPS_GetGlobalSolution(id, xg, -1));

	for(i=0;i<unknownnbr;i++)
	  x[i] = xg[unknownlist[i]];
	free(xg);
      }
      break;
    default:
      if(c->numflag == 1)
	for(i=0;i<unknownnbr;i++)
	  unknownlist[i]++;
      
      return HIPS_ERR_PARAMETER;
    }
  if(c->numflag == 1)
    for(i=0;i<unknownnbr;i++)
      unknownlist[i]++;
  
  return HIPS_SUCCESS;
}




INTS HIPS_CheckSolution(INTS id, INTS n, INTL *ia, INTS *ja, COEF *a, COEF *x, COEF *b, INTS sym)
{
  INTL *ib;
  INTS *jb;
  COEF *mb;
  COEF *y;
  REAL normb, normerr, err, hipserr;
  HIPS_Context *c;
  dim_t i;

  c = context+id;
  if(sym == 1)
    {
      flag_t job;
      /** Symmetrize the matrix (in rsa we only have the lower triangular part) ****/
      job = 2;
      PHIDAL_SymmetrizeMatrix(job, c->numflag, n, ia, ja, a, &ib, &jb, &mb);
    }
  else
    {
      ib = ia;
      jb = ja;
      mb = a;
    }
  
  y = (COEF *)malloc(sizeof(COEF)*n);
  bzero(y, sizeof(COEF)*n);
  csr_matvec_add(n, ib, jb, mb, x, y);

  for(i=0;i<n;i++)
    y[i] -= b[i];
  
  normerr = norm2(y, n);
  normb = norm2(b, n);
  err = normerr/normb;

  free(y);
  if(sym == 1)
    {
      free(ib);
      free(jb);
      free(mb);
    }

  HIPS_GetInfoREAL(id, HIPS_INFO_RES_NORM, &hipserr);

  fprintfd(stderr, "Check solution :\n     Error computed with original matrix = %g \n     Error computed in HIPS is  %g \n     Tolerance was %g \n", err, hipserr, c->options.tol);

  if(err > c->options.tol)
    {
      fprintferr(stderr, "CheckSolution ERROR :\nError computed with original matrix = %g \nError computed in HIPS is             %g \nTolerance was                         %g \n", err, hipserr, c->options.tol);
      return HIPS_ERR_CHECK;
    }
  else
    {
      return HIPS_SUCCESS;
    }

}

INTS HIPS_RHSReset(INTS id)
{
  return HIPS_SUCCESS;
}

/* @authors P. HENON */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <mpi.h>
#include "math.h"

#include "hips.h"
#include "hips_wrapper.h"
#include "scotch_metis_wrapper.h"

void HIPS_PrintError(INTS ierr)
{
  switch(ierr)
    {
    case HIPS_SUCCESS:
      break;
    case HIPS_ERR_ALLOCATE :
      fprintferr(stderr, "ERROR %ld : failed a memory allocation \n", (long)ierr);
      break;
    case HIPS_ERR_PARAMETER :
      fprintferr(stderr, "ERROR %ld : a parameter is wrong or a function is called at a wrong place \n", (long)ierr);
      break;
    case HIPS_ERR_CALL :
      fprintferr(stderr, "ERROR %ld : a function is called at a wrong place \n", (long)ierr);
      break;
    case HIPS_ERR_IO : 
      fprintferr(stderr, "ERROR %ld : IO problem on a file \n", (long)ierr);
      break;
    case HIPS_ERR_MATASSEMB :
      fprintferr(stderr, "ERROR %ld : problem in when setting the matrix coefficients \n", (long)ierr);
      break;
    case HIPS_ERR_RHSASSEMB :
      fprintferr(stderr, "ERROR %ld : problem in when setting the rhs coefficients \n", (long)ierr);
      break;
    case HIPS_ERR_PARASETUP :
      fprintferr(stderr, "ERROR %ld : problem in the parallel setup phase \n", (long)ierr);
      break;
    case HIPS_ERR_PRECOND :
      fprintferr(stderr, "ERROR %ld : problem in the preconditioner building \n", (long)ierr);
      break;
    case HIPS_ERR_SOLVE :
      fprintferr(stderr, "ERROR %ld : problem in the solve phase \n", (long)ierr);
      break;
    case HIPS_ERR_KRYLOV :
      fprintferr(stderr, "ERROR %ld : the krylov method failed \n", (long)ierr);
      break;
    case HIPS_ERR_CHECK :
      fprintferr(stderr, "ERROR %ld : a check point failed \n", (long)ierr);
      break;
    default:
      fprintferr(stderr, "ERROR in HIPS_PrintError : error %ld is not referenced \n", (long)ierr);
    }

}

#ifdef DEBUG_M
#define MY_EXIT assert(0)
#else
#define MY_EXIT exit(-1)
#endif

void HIPS_ExitOnError(INTS ierr)
{

  HIPS_PrintError(ierr);
  if(ierr != HIPS_SUCCESS)
    MY_EXIT;

}


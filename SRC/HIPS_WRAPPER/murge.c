/* @authors J. GAIDAMOUR, P. HENON */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <mpi.h>


#include "hips.h"
#include "murge.h"



INTS MURGE_Initialize(INTS idnbr)
{
  return HIPS_Initialize(idnbr);
}


INTS MURGE_SetDefaultOptions(INTS id, INTS stratnum)
{
  return HIPS_SetDefaultOptions(id, stratnum);
}


INTS MURGE_SetOptionINT(INTS id, INTS number, INTS value)
{
  return HIPS_SetOptionINT(id, number, value);
}

INTS MURGE_SetOptionREAL(INTS id, INTS number, REAL value)
{
  return HIPS_SetOptionREAL(id, number, value);
}
#ifdef MPI_SUCCESS

INTS MURGE_SetCommunicator(INTS id, MPI_Comm mpicom)
{
  return HIPS_SetCommunicator(id, mpicom);
}
#endif


INTS MURGE_GraphBegin(INTS id, INTS N, INTL edgenbr)
{
  return HIPS_GraphBegin(id, N, edgenbr);
}


INTS MURGE_GraphEdge(INTS id, INTS COL, INTS ROW)
{
  return HIPS_GraphEdge(id, COL, ROW);
}


INTS MURGE_GraphEnd(INTS id)
{
  return HIPS_GraphEnd(id);
}


INTS MURGE_GraphGlobalCSR(INTS id, INTS N, INTL *rowptr, INTS *COLS, INTS root)
{
  return HIPS_GraphGlobalCSR(id, N, rowptr, COLS, root);
}

INTS MURGE_GraphGlobalCSC(INTS id, INTS N, INTL *colptr, INTS *ROWS, INTS root)
{
  return HIPS_GraphGlobalCSC(id, N, colptr, ROWS, root);
}


INTS MURGE_GraphGlobalIJV(INTS id, INTS N, INTL NNZ, INTS *ROW, INTS *COL, INTS root)
{
  return HIPS_GraphGlobalIJV(id, N, NNZ, ROW, COL, root);
}


INTS MURGE_Save(INTS id, char* directory)
{
  return HIPS_SetupSave(id, directory);
}


INTS MURGE_Load(INTS id, char* directory)
{
  return HIPS_SetupLoad(id, directory);
}


INTS MURGE_GetLocalNodeNbr(INTS id, INTS *nodenbr)
{
  return HIPS_GetLocalNodeNbr(id, nodenbr);
}


INTS MURGE_GetLocalNodeList(INTS id, INTS *nodelist)
{
  return HIPS_GetLocalNodeList(id, nodelist);
}

INTS MURGE_GetLocalUnknownNbr(INTS id, INTS *unkownnbr)
{
  return HIPS_GetLocalUnknownNbr(id, unkownnbr);
}


INTS MURGE_GetLocalUnknownList(INTS id, INTS *unkownlist)
{
  return HIPS_GetLocalUnknownList(id, unkownlist);
}


INTS MURGE_AssemblyBegin(INTS id, INTL coefnbr, INTS op, INTS op2, INTS mode, INTS sym)
{
  return HIPS_AssemblyBegin(id, coefnbr, op, op2, mode, sym);
}

INTS MURGE_AssemblySetValue(INTS id, INTS ROW, INTS COL, COEF value)
{
  return HIPS_AssemblySetValue(id, ROW, COL, value);
}


INTS MURGE_AssemblySetNodeValues(INTS id, INTS ROW, INTS COL, COEF *values)
{
  return HIPS_AssemblySetNodeValues(id, ROW, COL, values);
}


INTS MURGE_AssemblySetBlockValues(INTS id, INTS nROW, INTS *ROWlist, INTS nCOL, 
				  INTS *COLlist, COEF *values)
{
  return HIPS_AssemblySetBlockValues(id, nROW, ROWlist, nCOL, 
				      COLlist, values);
}

INTS MURGE_AssemblyEnd(INTS id)
{
  return HIPS_AssemblyEnd(id);
}


INTS MURGE_MatrixReset(INTS id)
{
  return HIPS_MatrixReset(id);
}

INTS MURGE_MatrixGlobalCSR(INTS id, INTS N, INTL *rowptr, INTS *COLS, 
			   COEF *values, INTS root, INTS op, INTS sym)
{
  return HIPS_MatrixGlobalCSR(id, N, rowptr, COLS, 
			      values, root, op, sym);
}


INTS MURGE_MatrixGlobalCSC(INTS id, INTS N, INTL *COLPTR, INTS *ROWS, 
			   COEF *values, INTS root, INTS op, INTS sym)
{
  return HIPS_MatrixGlobalCSC(id, N, COLPTR, ROWS, 
			       values, root, op, sym);
}


INTS MURGE_MatrixGlobalIJV(INTS id, INTS N, INTL NNZ, INTS *ROWS, INTS *COLS, 
			   COEF *values, INTS root, INTS op, INTS sym)
{
  return HIPS_MatrixGlobalIJV(id, N,  NNZ, ROWS, COLS, values, root, op, sym);
}


INTS MURGE_SetGlobalRHS(INTS id, COEF *b, INTS root, INTS op)
{
  return HIPS_SetGlobalRHS(id, b, root, op);
} 


INTS MURGE_SetLocalRHS(INTS id, COEF *b, INTS op, INTS op2)
{
  return HIPS_SetLocalRHS(id, b,  op, op2);
}


INTS MURGE_SetRHS(INTS id, INTS n, INTS *coefsidx, COEF *b, 
		  INTS op, INTS op2, INTS mode)
{
  return HIPS_SetRHS(id, n, coefsidx, b, 
		     op, op2, mode);
}


INTS MURGE_RHSReset(INTS id)
{
  return HIPS_RHSReset(id);
}


INTS MURGE_GetGlobalSolution(INTS id, COEF *x, INTS root)
{
  return HIPS_GetGlobalSolution(id, x, root); 
}


INTS MURGE_GetLocalSolution(INTS id, COEF *x)
{
  return HIPS_GetLocalSolution(id, x);
}


INTS MURGE_GetSolution(INTS id,  INTS n, INTS *coefsidx, COEF *x, INTS mode)
{
  return HIPS_GetSolution(id, n, coefsidx, x, mode);
}


INTS MURGE_Clean(INTS id)
{
  return HIPS_Clean(id);
}


INTS MURGE_Finalize()
{
  return HIPS_Finalize();
}


INTS MURGE_GetSolver(INTS *solver)
{
  *solver = MURGE_SOLVER_HIPS;
  return HIPS_SUCCESS;
}

INTS MURGE_GetInfoINT(INTS id,  INTS metric, INTL * value)
{
  return HIPS_GetInfoINT(id,  metric, value);
}


INTS MURGE_GetInfoREAL(INTS id,  INTS metric, REAL * value)
{
  return HIPS_GetInfoREAL(id, metric, value);
}


INTS MURGE_PrintError(INTS ierr)
{
  HIPS_PrintError(ierr);
  return MURGE_SUCCESS;
}

INTS MURGE_ExitOnError(INTS ierr)
{
  HIPS_ExitOnError(ierr);
  return MURGE_SUCCESS;
}

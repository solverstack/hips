/* @authors P. HENON */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <mpi.h>
#include "math.h"

#include "hips_wrapper.h"
#include "hips.h"

#include "scotch_metis_wrapper.h"

extern int_t idnbr;
extern HIPS_Context *context;


INTS HIPS_GetLocalNodeNbr(INTS id, INTS *nodenbr)
{
  HIPS_Context *c;
  INTS ret;
  c = context+id;

  if(c->state < SETUP)
    {
      ret = HIPS_ParallelSetup(id);
      if(ret != HIPS_SUCCESS)
	return ret;
    }

  *nodenbr = c->DBL.LHID.n / c->dof; 
  /* *interior = c->DBL.LHID.block_index[c->DBL.LHID.block_levelindex[1]] / c->dof;*/

  return HIPS_SUCCESS;
}

INTS HIPS_GetLocalUnknownNbr(INTS id, INTS *total)
{
  HIPS_Context *c;
  INTS ret;
  c = context+id;

  if(c->state < SETUP)
    {
      ret = HIPS_ParallelSetup(id);
      if(ret != HIPS_SUCCESS)
	return ret;
    }
  

  *total = c->DBL.LHID.n; 
  return HIPS_SUCCESS;
}


INTS  HIPS_GetLocalNodeList(INTS id, INTS *nodelist)
{
  /***************************************/
  /* Get the ordered list of local nodes */
  /***************************************/
  dim_t i, ind;
  PhidalDistrHID *DBL;
  HIPS_Context *c;
  INTS ret;
  c = context+id;

  if(c->state < SETUP)
     {
      ret = HIPS_ParallelSetup(id);
      if(ret != HIPS_SUCCESS)
	return ret;
    }

  DBL = &context[id].DBL;
  if(nodelist == NULL)
    return HIPS_ERR_PARAMETER;
  /*assert(nodelist != NULL);*/
  
  if(c->dof == 1)
    {
      if(sizeof(INTS) == sizeof(dim_t))
	memcpy(nodelist, DBL->loc2orig, sizeof(INTS)*DBL->LHID.n);
      else
	for(i=0;i<DBL->LHID.n;i++)
	  nodelist[i] = (INTS)DBL->loc2orig[i];
    }
  else
    {
      for(i=0;i<DBL->LHID.n;i++)
	if(i%c->dof == 0)
	  {
	    ind = i/c->dof;
	    nodelist[ind] = DBL->loc2orig[i]/c->dof;
	  }
    }
     
  

  if(c->numflag == 1)
    for(i=0;i<DBL->LHID.n/c->dof;i++)
      nodelist[i]++;

  return HIPS_SUCCESS;
}


INTS  HIPS_GetLocalUnknownList(INTS id, INTS *nodelist)
{
  /***************************************/
  /* Get the ordered list of local nodes */
  /***************************************/
  dim_t i;
  PhidalDistrHID *DBL;
  HIPS_Context *c;
  c = context+id;

  if(c->state < SETUP)
    HIPS_ParallelSetup(id);

  DBL = &context[id].DBL;
  if(nodelist == NULL)
    return HIPS_ERR_PARAMETER;
  /*assert(nodelist != NULL);*/

  memcpy(nodelist, DBL->loc2orig, sizeof(INTS)*DBL->LHID.n);

  if(c->numflag == 1)
    for(i=0;i<DBL->LHID.n;i++)
      nodelist[i]++;
  return HIPS_SUCCESS;
}

INTS  HIPS_GetLocalDomainNbr(INTS id, INTS *domnbr, INTS *listsize)
{
  /*******************************************************/
  /* Return  the number of local domain  as well as      */
  /* the size of the vector needed in GetLocalDomainList */
  /*******************************************************/
  PhidalDistrHID *DBL;
  PhidalHID *BL;
  HIPS_Context *c;
  INTS size, i, j;
  dim_t *domglob2loc;
  c = context+id;

  if(c->state < SETUP)
    HIPS_ParallelSetup(id);

  DBL = &context[id].DBL;
  BL  = &(DBL->LHID);
  domglob2loc = (dim_t *)malloc(sizeof(dim_t)*BL->ndom);
  CHECK_MALLOC(domglob2loc);
  

  for(i=0;i<BL->ndom;i++)
    domglob2loc[i] = -1;


  for(i=0;i<BL->block_levelindex[1];i++)
    domglob2loc[BL->block_key[BL->block_keyindex[i]]] = i;

  *domnbr = BL->block_levelindex[1];

  size = 0;
  for(i=0;i<BL->nblock;i++)
    for(j=BL->block_keyindex[i];j<BL->block_keyindex[i+1];j++)
      if(domglob2loc[BL->block_key[j]] >= 0)
	size += BL->block_index[i+1]-BL->block_index[i];

  free(domglob2loc);

  *listsize = size;
  
  return HIPS_SUCCESS;
}

INTS  HIPS_GetLocalDomainList(INTS id, INTS *mapptr, INTS *mapp)
{
  PhidalDistrHID *DBL;
  PhidalHID *BL;
  HIPS_Context *c;
  dim_t i, j, k;
  dim_t *domglob2loc;
  INTS ret;

  c = context+id;

  if(c->state < SETUP)
    {
      ret = HIPS_ParallelSetup(id);
      if(ret != HIPS_SUCCESS)
	return ret;
    }

  DBL = &context[id].DBL;
  BL  = &(DBL->LHID);

  domglob2loc = (dim_t *)malloc(sizeof(dim_t)*BL->ndom);
  CHECK_MALLOC(domglob2loc);

  for(i=0;i<BL->ndom;i++)
    domglob2loc[i] = -1;
  
  for(i=0;i<BL->block_levelindex[1];i++)
    domglob2loc[BL->block_key[BL->block_keyindex[i]]] = i;



#ifdef DEBUG_M
  assert(BL->locndom == BL->block_levelindex[1]);
#endif
  bzero(mapptr, sizeof(dim_t)*(BL->locndom+1));

  for(i=0;i<BL->nblock;i++)
    for(j=BL->block_keyindex[i]; j < BL->block_keyindex[i+1];j++)
      {
	k = domglob2loc[BL->block_key[j]];
	if(k >= 0)
	  mapptr[k+1] += BL->block_index[i+1]-BL->block_index[i];
      }
  
  for(k=1;k<=BL->locndom;k++)
    mapptr[k] += mapptr[k-1];

  for(i=0;i<BL->nblock;i++)
    for(j=BL->block_keyindex[i]; j < BL->block_keyindex[i+1];j++)
      {
	k = domglob2loc[BL->block_key[j]];
	if(k>=0)
	  {
	    memcpy( mapp + mapptr[k], DBL->loc2orig + BL->block_index[i], sizeof(INTS)*(BL->block_index[i+1]-BL->block_index[i]));
	    mapptr[k] += BL->block_index[i+1]-BL->block_index[i];
	  }
      }

  /** reset mapptr **/
   for(k=BL->locndom;k>0;k--)
     mapptr[k] = mapptr[k-1];
   mapptr[0] = 0;

  free(domglob2loc);

  return HIPS_SUCCESS;
}



INTS  HIPS_GetGlobalPartitionNbr(INTS id, INTS *domnbr, INTS *partnbr, INTS *domkeynbr)
{
  /***************************************/

  /***************************************/
  PhidalHID *BL;
  HIPS_Context *c;
  c = context+id;

  if(c->state != HIERARCH)
    {
      fprintferr(stderr, "This function can not be called here \n");
      exit(-1);
    }

  BL = &context[id].BL;
  *partnbr =  BL->nblock;
  *domkeynbr = BL->block_keyindex[BL->nblock];
  *domnbr = BL->block_levelindex[1];

  return HIPS_SUCCESS;
}


INTS  HIPS_GetGlobalPartition(INTS id, INTS *iperm, INTS *parttab, INTS *domkeyindex, INTS *domkey)
{
  /***************************************/
  /***************************************/
  dim_t i;
  dim_t nkey;
  PhidalHID *BL;
  HIPS_Context *c;
  
  c = context+id;
  if(c->state != HIERARCH)
    {
      fprintferr(stderr, "HIPS_GetGlobalPartition ERROR : this function can not be called here \n");
      return HIPS_ERR_PARAMETER;
    }


  BL = &context[id].BL;

  nkey = BL->block_keyindex[BL->nblock];
  if(sizeof(INTS) == sizeof(dim_t))
    {
      memcpy(parttab, BL->block_index, sizeof(INTS)* (BL->nblock+1));
      memcpy(domkeyindex, BL->block_keyindex, sizeof(INTS)* (BL->nblock+1));
      memcpy(domkey, BL->block_key, sizeof(INTS)*nkey );
      memcpy(iperm, c->iperm, sizeof(INTS)*c->na);
    }
  else
    {
      for(i=0;i<BL->nblock+1;i++)
	{
	  parttab[i] =  (INTS)BL->block_index[i];
	  domkeyindex[i] =  (INTS)BL->block_keyindex[i];
	}
      for(i=0;i<nkey;i++)
	domkey[i] = (INTS)BL->block_key[i];

      for(i=0;i<c->na;i++)
	iperm[i] = (INTS)c->iperm[i];
    }



  if(c->numflag == 1)
    {
      for(i=0;i<BL->nblock+1;i++)
	parttab[i]++;
      for(i=0;i<BL->nblock+1;i++)
	domkeyindex[i]++;
      for(i=0;i<nkey;i++)
	domkey[i]++;
      for(i=0;i<c->na;i++)
	iperm[i]++;
    }

  return HIPS_SUCCESS;
}


INTS  HIPS_GetLocalPartitionNbr(INTS id, INTS *domnbr, INTS *unknownnbr, INTS *partnbr, INTS *prockeynbr)
{
  /***************************************/

  /***************************************/
  PhidalDistrHID *DBL;
  PhidalHID *BL;
  HIPS_Context *c;
  INTS ret;
  c = context+id;

  if(c->state < SETUP)
    {
      ret = HIPS_ParallelSetup(id);
      if(ret != HIPS_SUCCESS)
	return ret;
    }

  DBL = &context[id].DBL;
  BL  = &(DBL->LHID);
  *unknownnbr =  BL->n;  
  *partnbr =  BL->nblock;
  *prockeynbr = DBL->block_psetindex[BL->nblock];
  *domnbr = BL->block_levelindex[1];
  return HIPS_SUCCESS;
}


INTS HIPS_GetLocalPartition(INTS id, INTS *unknownlist,  INTS *parttab, INTS *prockeyindex, INTS *prockey)
{
  /***************************************/

  /***************************************/
  dim_t i;
  dim_t nkey;
  PhidalDistrHID *DBL;
  PhidalHID *BL;
  HIPS_Context *c;
  INTS ret;

  c = context+id;

  if(c->state < SETUP)
    {
      ret = HIPS_ParallelSetup(id);
      if(ret != HIPS_SUCCESS)
	return ret;
    }

  DBL = &context[id].DBL;
  BL  = &(DBL->LHID);
  assert(unknownlist != NULL);
  

  nkey = DBL->block_psetindex[BL->nblock];
  if(sizeof(INTS) == sizeof(dim_t))
    {
      memcpy(unknownlist, DBL->loc2orig, sizeof(INTS) * BL->n);
      memcpy(parttab, BL->block_index, sizeof(INTS)* (BL->nblock+1));
      memcpy(prockeyindex, DBL->block_psetindex, sizeof(INTS)* (BL->nblock+1));
      memcpy(prockey, DBL->block_pset, sizeof(INTS)*nkey );
    }
  else
    {
      for(i=0;i<BL->n;i++)
	unknownlist[i] = (INTS)DBL->loc2orig[i];
      
      for(i=0;i<BL->nblock+1;i++)
	{
	  parttab[i] =  (INTS)BL->block_index[i];
	  prockeyindex[i] =  (INTS)DBL->block_psetindex[i];
	}
      for(i=0;i<nkey;i++)
	prockey[i] = (INTS)DBL->block_pset[i];
    }

  if(c->numflag == 1)
    {
      for(i=0;i<DBL->LHID.n;i++)
	unknownlist[i]++;
      for(i=0;i<BL->nblock+1;i++)
	parttab[i]++;
      for(i=0;i<BL->nblock+1;i++)
	prockeyindex[i]++;
      for(i=0;i<nkey;i++)
	prockey[i]++;
    }
  
  return HIPS_SUCCESS;
}


/*
INTS HIPS_GetMatrixGlobalDim(INTS id, INTS *dim)
{
  HIPS_Context *c;
  c = context+id;
  
  *dim = (INTS)(c->na);
  return HIPS_SUCCESS;
  }*/

INTS HIPS_GetInfoINT(INTS id,  INTS metric, INTL * value)
{
  HIPS_Context *c;
  c = context+id;
  switch(metric){
  case HIPS_INFO_DIM:
    *value = (INTS)(c->na);
    break;
  case HIPS_INFO_NNZ:
    *value = c->infoint[HIPS_INFO_NNZ];
    break;
  case HIPS_INFO_INNER_ITER:
    *value = c->infoint[HIPS_INFO_INNER_ITER];
    break;
  case HIPS_INFO_OUTER_ITER:
    *value = c->infoint[HIPS_INFO_OUTER_ITER];
    break;
  case HIPS_INFO_ITER:
    if(c->hybrid==1)
      *value = c->infoint[HIPS_INFO_INNER_ITER];
    else
      *value = c->infoint[HIPS_INFO_OUTER_ITER];
    break;
    
  default:
    return HIPS_ERR_PARAMETER;
  }
  return HIPS_SUCCESS;
}

INTS HIPS_GetInfoREAL(INTS id,  INTS metric, REAL * value)
{
  HIPS_Context *c;
  c = context+id;
  switch(metric){
  case HIPS_INFO_PRECOND_TIME:
    *value = c->inforeal[HIPS_INFO_PRECOND_TIME];
    break;
  case HIPS_INFO_SOLVE_TIME:
   *value = c->inforeal[HIPS_INFO_SOLVE_TIME];
    break;
 case HIPS_INFO_RES_NORM:
   *value = c->inforeal[HIPS_INFO_RES_NORM];
    break;
  default:
    return HIPS_ERR_PARAMETER;
  }
  return HIPS_SUCCESS;
}

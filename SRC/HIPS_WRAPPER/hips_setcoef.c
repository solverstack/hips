/* @authors  P. HENON */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <mpi.h>
#include "math.h"

#include "hips_wrapper.h"
#include "hips.h"

#include "scotch_metis_wrapper.h"


extern int_t idnbr;
extern HIPS_Context *context;

INTS CSR_MatrixRedistr(INTS id, INTS job, INTS n, INTL *ia, INTS *ja, COEF *a, INTS op, INTS op2, INTS mode, INTS sym);


INTS HIPS_GetSubmatrix(INTS ln, INTS numflag, INTS *nodelist, INTS n, INTL *ia, INTS *ja, COEF *a, 
		       INTL **lia, INTS **lja, COEF **la)
{
  dim_t i;
  /* Translate matrix into C numbering */
  CHECK_NUMFLAG;

  if(numflag != 0)
    {
      for(i=0;i<ln;i++)
	nodelist[i]--;
      CSR_Fnum2Cnum(ja, ia, n);
    }

  CSR_GetSquareSubmatrix(ln, nodelist, n, ia, ja, a,
			 lia, lja, la);
  if(numflag != 0)
    {
      for(i=0;i<ln;i++)
	nodelist[i]++;
      CSR_Cnum2Fnum(ja, ia, n);
      CSR_Cnum2Fnum(*lja, *lia, ln);
    }
  return HIPS_SUCCESS;
}



INTS HIPS_SetSubmatrixCoef(INTS id, INTS op, INTS op2, INTS n, INTL *ia, INTS *ja, COEF *a, INTS rsa, INTS ln, INTS *unknownlist)
{
  INTS *lja;
  INTL *lia;
  COEF *la;
  HIPS_Context *c;
  c = context+id;  
 
  CHECK_RETURN(HIPS_GetSubmatrix(ln, c->numflag, unknownlist, n, ia, ja, a,
				 &lia, &lja, &la));
  
  /*CHECK_RETURN(HIPS_SetMatrixCoef(id, op, op2, ln, lia, lja, la, rsa, unknownlist));*/
  CHECK_RETURN(HIPS_MatrixLocalCSR(id, ln, unknownlist, lia, lja, la, op, op2, rsa));
  return HIPS_SUCCESS;
}


INTS HIPS_MatrixLocalCSR(INTS id, INTS ln, INTS *unknownlist, INTL *lia, INTS *lja, COEF *la,  INTS op, INTS op2, INTS rsa)
{
  /***********************************************/
  /* Create the local matrix                     */
  /* lia, lja la is the locas CSR matrix         */
  /* if the last argument is NULL                */
  /* then the matrix is in the unknownlist ordering */
  /* op = HIPS_ASSEMBLY_xx                       */
  /* op2= HIPS_ASSEMBLY_xx                       */
  /*                                             */
  /***********************************************/
  HIPS_Context *c;
  INTS i;
  INTS over_add;
  c = context+id;  
  
  CHECK_OP;
  CHECK_OP2;
 
  if(c->state < SETUP)
    CHECK_RETURN(HIPS_ParallelSetup(id));

  if (c->numflag != 0)
    {
      CSR_Fnum2Cnum(lja, lia, ln);
      for(i=0;i<ln;i++)
	unknownlist[i]--;
    }

  /** Mandatory (also eliminate -1 from SetMatrixCoef_DG **/
  CSR_SuppressDouble(1, ln, lia, lja, la);



  over_add = op2;

  if(c->flagmat == 0)
    {
      /*** Build the PHIDAL matrix ******/
      PHIDAL_SetMatrixCoef(0, over_add, 0, ln, lia, lja, la, unknownlist, c->options.symmetric, rsa,  &c->A, &c->DBL);
      PhidalCommVec_Setup(0, &c->A, &c->A.commvec, &c->DBL);
    }
  else
    {
      switch(op)
	{
	case 0:
	  PHIDAL_SetMatrixCoef(3, over_add, 0, ln, lia, lja, la, unknownlist, c->options.symmetric, rsa, &c->A, &c->DBL);
	  break;

	case 1:
	  PHIDAL_SetMatrixCoef(1, over_add, 0, ln, lia, lja, la, unknownlist, c->options.symmetric, rsa,  &c->A, &c->DBL);
	  break;
      
	default:
	  {
	    printferr("Error in HIPS_SetMatrixCoef : op = %ld not valid \n", (long)op);
	    return HIPS_ERR_PARAMETER;
	  }
	}
    }

  if(c->numflag != 0)
    {
      CSR_Cnum2Fnum(lja, lia, ln);
      for(i=0;i<ln;i++)
	unknownlist[i]++;
    }

  c->flagmat = 1;
  c->state = COEFFICIENT;
  
  return HIPS_SUCCESS;
}



INTS CSR_MatrixRedistr(INTS id, INTS job, INTS n, INTL *ia, INTS *ja, COEF *a, INTS op, INTS op2, INTS mode, INTS sym)
{
  /** job == 1 : free ia, ja, a **/

  HIPS_Context *c;
  flag_t numflag; /** need this for temporary change **/
  INTL *lia, *cia;
  dim_t ln, *lja, *cja;
  COEF *la, *ca;
  dim_t i;
  INTS *unknownlist;


  CHECK_OP;
  CHECK_OP2;
  c = context+id;
 
 /********************************************************/
  /* Get the ordered list of local unknwons (not nodes!!) */
  /********************************************************/

  CHECK_RETURN(HIPS_GetLocalUnknownNbr(id, &ln));
  unknownlist = (dim_t *)malloc(sizeof(dim_t)*ln);
  CHECK_MALLOC(unknownlist);
  CHECK_RETURN(HIPS_GetLocalUnknownList(id, unknownlist));
  if(c->numflag != 0)
    for(i=0;i<ln;i++)
      unknownlist[i]--;


  /***********************************/
  /* Construct the local CSR matrix  */
  /***********************************/
  /** numflag=0 */
  CHECK_RETURN(HIPS_GetSubmatrix(ln, 0, unknownlist,  n, ia, ja , a, &lia, &lja, &la));
  
  /** Get the matrix part that is not local **/
  if(mode == HIPS_ASSEMBLY_FOOL && c->nproc > 1)
    {
#ifdef DEBUG_M
      assert(ia[0] == 0);
#endif
      CSR_ComplementSubmatrix(ln, unknownlist, n, ia, ja, a,
			      &cia, &cja, &ca);
      
    }
  
  if(job == 1)
    {
      free(ia);
      free(ja);
      free(a);
    }

  /**********************************/
  /* Create the local matrix        */
  /* lia, lja la is the loca CSR    */
  /* matrix using unknownlist ordering */
  /**********************************/

  /** This was erroneous to use this instead of PHIDAL_SetCoefMatrix **/
  numflag = c->numflag;
  c->numflag = 0;

  CHECK_RETURN(HIPS_MatrixLocalCSR(id, ln, unknownlist, lia, lja, la,  op, op2, sym)); 
  c->numflag = numflag;

  free(lia);
  if(lja != NULL)
    free(lja);
  if(la != NULL)
    free(la);
  
  if(mode == HIPS_ASSEMBLY_FOOL && c->nproc > 1)
    {
      INTL *iar;
      dim_t *jar;
      COEF *ar;
      INTS job;
      
      /** NB cia, cja, ca are deallocated in CSR_Reduce **/
      CSR_Reduce(1, op2, c->master, n, cia, cja, ca, &iar, &jar, &ar, c->mpicom);


#ifdef DEBUG_M   
      if(c->proc_id == c->master)
	{
	  fprintfd(stderr, "Proc %ld GLOBAL COMPLEMENT CSR \n", (long)(c->proc_id));
	  /*dumpcsr(stderr, ar, jar ,iar, c->na);*/
	  fprintfd(stderr, "\n \n NON ZERO IN THE COMPLEMENT MATRICE : %ld \n \n", iar[c->na]-iar[0]);
	}
#endif

      /** numflag = 0 ; */
      numflag = c->numflag;
      c->numflag = 0;
      job = HIPS_ASSEMBLY_ADD;
      CHECK_RETURN(HIPS_MatrixGlobalCSR(id, n, iar, jar, ar, c->master, job, sym));
      
      c->numflag = numflag;

      if(c->proc_id == c->master)
	{
	  
	  if(iar[n]>0)
	    {
	      free(jar);
	      free(ar);
	    }
	  free(iar);
	}
    }

  free(unknownlist);
  return HIPS_SUCCESS;
}

INTS HIPS_MatrixDistrCSR(INTS id,  INTS ln, INTS *unknownlist, INTL *lia, INTS *ja, COEF *a, INTS op, INTS op2, INTS mode, INTS sym)
{
  /**************************************************************/
  /* Create the local matrix                                    */
  /* lia, ja, a is the local CSR matrix                         */
  /* unknownlist is the list of local rows in global numbering  */
  /* In this function : ja is given in GLOBAL NUMBERING         */
  /**************************************************************/

  HIPS_Context *c;
  INTL i;
  dim_t j;
  flag_t base;
 
  CHECK_MODE;
  CHECK_OP;
  CHECK_OP2;

  c = context+id;  

  
  if(c->state < SETUP)
    CHECK_RETURN(HIPS_ParallelSetup(id));

  base = 0;
  if(c->numflag == 1) base = 1;



  if(mode == HIPS_ASSEMBLY_RESPECT)
    {
      INTS *ja2; 
      INTL *lia2;
      lia2 = (INTS *)malloc(sizeof(INTL) * (ln+1));
      CHECK_MALLOC(lia2);
      ja2 = (INTS *)malloc(sizeof(INTS) * (lia[ln]-base));
      CHECK_MALLOC(ja2);

      memcpy(ja2, ja, sizeof(INTS)*  (lia[ln]-base));
      memcpy(lia2, lia, sizeof(INTL)* (ln+1));
      
      for(i=0;i<lia2[ln]-base;i++)
	{
	  j = c->DBL.orig2loc[ja2[i]-base];
	  
	  if(j == -1)
	    ja2[i] = -1; /** Not on local processor **/

	  ja2[i] = j + base;
	}
      
      CHECK_RETURN(HIPS_MatrixLocalCSR(id,  ln, unknownlist,  lia2, ja2, a, op, op2, sym));
      
      free(lia2);
      free(ja2);
    }
  else
    {
      INTL *ia2;
      INTS *ja2;
      COEF *a2;
      
      /** Put unknownlist in C numbering **/
      if(c->numflag != 0)
	{
	  for(i=0;i<ln;i++)
	    unknownlist[i]--;
	  
	  CSR_Cnum2Fnum(ja, lia, c->na);
	}
      /** Convert the matrix to fit CSR_Reduce prototype **/
      /** OIMBE : it will be better to change CSR_Reduce ...one day**/
     
      CHECK_RETURN(DistrCSR_Loc2Glob(ln, unknownlist, lia, ja, a, c->na, &ia2, &ja2, &a2));

      
      /** ATTENTION ia2, ja2, a2 are free in this function **/
      /** ATTENTION put ia2, ja2 in C numbering ! ***/
      CHECK_RETURN(CSR_MatrixRedistr(id, 1, c->na, ia2, ja2, a2,  op, op2, mode, sym));
      
      /** reset unknownlist **/
      if(c->numflag != 0)
	{
	  for(i=0;i<ln;i++)
	    unknownlist[i]++;

	  CSR_Cnum2Fnum(ja, lia, c->na);
	}
    }

  c->flagmat = 1;
  c->state = COEFFICIENT;

 
  return HIPS_SUCCESS;
}


INTS HIPS_MatrixGlobalIJV(INTS id, INTS N, INTL NNZ, INTS *ROWS, INTS *COLS, 
			   COEF *values, INTS root, INTS op, INTS sym)
{
  INTL *ia;
  INTS *ja;
  COEF *a;
  /** Convert ijv to csr (the double edge are suppressed in the process **/
  ijv2csr(N, NNZ, ROWS, COLS, values, &ia, &ja, &a);
  
  CHECK_RETURN(HIPS_MatrixGlobalCSR(id, N, ia, ja, a,  root, op, sym));
  free(ia);
  free(ja);
  free(a);

  return HIPS_SUCCESS;
}

INTS HIPS_MatrixGlobalCSC(INTS id, INTS n, INTL *ia, INTS *ja, COEF *a, INTS proc_root, INTS job, INTS rsa)
{
  /*** OIMBE : problem pour job = add **/
  if(job != HIPS_ASSEMBLY_OVW)
    {
      fprintferr(stderr, "You cannot only call this function with HIPS_ASSEMBLY_OVW \n");
      return HIPS_ERR_PARAMETER;
    }

  CHECK_RETURN(HIPS_MatrixGlobalCSR(id, n, ia, ja, a, proc_root, job, rsa));
  CHECK_RETURN(HIPS_TransposeMatrix(id));
  return HIPS_SUCCESS;
}

INTS HIPS_MatrixGlobalCSR(INTS id, INTS n, INTL *ia, INTS *ja, COEF *a, INTS proc_root, INTS op, INTS rsa)
{
  HIPS_Context *c;
  PhidalHID *BL;

  PhidalDistrHID* DBL; 
  PhidalOptions *option;

  mpi_t p;
  INTL *lia=NULL; 
  dim_t *lja=NULL; 
  COEF *la=NULL; 
  dim_t ln=-1;

  INTL nnz;
  
  c = context+id;
  option = &c->options;
  BL = &c->BL;
  DBL = &c->DBL;
 


  if(c->state < SETUP)
    CHECK_RETURN(HIPS_ParallelSetup(id));
  

  if(c->proc_id == proc_root)
    nnz = ia[n];
  MPI_Bcast(&nnz, FF(1), COMM_BIG_INT, proc_root, c->mpicom);


  if(nnz == 0)
    return HIPS_SUCCESS;
  

  if(c->proc_id == proc_root)
    {
      INTL *lia2 = NULL; 
      dim_t *lja2 = NULL; 
      COEF *la2 = NULL; 
      dim_t ln2;

      if(ia == NULL)
	{
	  fprintferr(stderr, "HIPS_MatrixGlobalCSR : ERROR : the global CSR matrix is not allocated on the processor root %ld \n", (long)proc_root);
	  return HIPS_ERR_PARAMETER;
	}
      
      if(c->numflag != 0)
	CSR_Fnum2Cnum(ja, ia, n);


      for(p=0;p<c->nproc;p++)
	{
	  dim_t *unknownlist;
	  /** Build the list of unknown for processor p **/
	  GetProcUnknowns(p, c->DBL.dom2proc, &c->BL, c->block_psetindex, c->block_pset, c->iperm, &ln2,  &unknownlist);

	  /** do not call HIPS_GetSub because c->numflag can be 1 ***/
	  CSR_GetSquareSubmatrix(ln2, unknownlist, n, ia, ja, a, &lia2, &lja2, &la2);

	  CSR_SuppressDouble(1, ln2, lia2, lja2, la2);
	  nnz = lia2[ln2];

	  if(p != proc_root)
	    {
	      /** Send matrix **/
	      MPI_Send(lia2, FF(ln2+1), COMM_BIG_INT, p, 1, c->mpicom);
	      if(lia2 != NULL)
		free(lia2);

	      if(nnz>0)
		{
		  MPI_Send(lja2, EE(nnz), COMM_INT, p, 2, c->mpicom);
		  MPI_Send(la2, CC(nnz), MPI_COEF_TYPE, p, 3, c->mpicom);
		}
	      
	      if(lja2 != NULL)
		free(lja2);

	      if(la2 != NULL)
		free(la2);
	    }
	  else
	    {
	      ln = ln2;
	      lia = lia2;
	      lja = lja2;
	      la = la2;
	    }
	  
	  free(unknownlist);
	}
   
    }
  else
    {
      /** receive the local matrix **/
      MPI_Status status;
      ln = c->DBL.LHID.n;


      lia = (INTL *)malloc(sizeof(INTL)*(ln+1));
      MPI_Recv(lia, FF(ln+1), COMM_BIG_INT, proc_root, 1, c->mpicom, &status);

      nnz = lia[ln];
      if(nnz>0)
	{
	  lja = (dim_t *)malloc(sizeof(dim_t)*nnz);
	  MPI_Recv(lja, EE(nnz), COMM_INT, proc_root, 2, c->mpicom, &status);

	  la = (COEF *)malloc(sizeof(COEF)*nnz);
	  MPI_Recv(la, CC(nnz), MPI_COEF_TYPE, proc_root, 3, c->mpicom, &status);
	}
      else
	{
	  lja = NULL;
	  la  = NULL;
	}
    }
  

  {
    flag_t numflag;
    numflag = c->numflag;
    c->numflag = 0;
    
    /** Do not add on overlap **/
    CHECK_RETURN(HIPS_MatrixLocalCSR(id, ln, DBL->loc2orig, lia, lja, la, op, HIPS_ASSEMBLY_OVW, rsa));
    c->numflag = numflag;
  }

  free(lia);
  if(lja != NULL)
    free(lja);
  if(la != NULL)
    free(la);


  if(c->numflag != 0 && c->proc_id == proc_root)
    CSR_Cnum2Fnum(ja, ia, n);
  


  return HIPS_SUCCESS;
  
}


INTS HIPS_AssemblyBegin(INTS id, INTL nnz, INTS op, INTS op2, INTS mode, INTS symmetric)
{
  HIPS_Context *c;
  c = context+id;
 
  CHECK_MODE;
  CHECK_OP;
  CHECK_OP2;
 
  /*** mode = 0 : only local values are assemblied ***/
  /*** mode = 1 : global assembly ***/
  c->job = op;
  c->job2 = op2;

  c->mode = mode;
  c->assemb_sym = symmetric;
  c->ind = 0;
  c->nnz = nnz;

  if(c->state < SETUP)
    CHECK_RETURN(HIPS_ParallelSetup(id));


  if(c->nnz>0)
    {
      c->ii = (dim_t *)malloc(sizeof(dim_t)*c->nnz);
      CHECK_MALLOC(c->ii);
      c->jj = (dim_t *)malloc(sizeof(dim_t)*c->nnz);
      CHECK_MALLOC(c->jj);
      c->val = (COEF *)malloc(sizeof(COEF)*c->nnz);
      CHECK_MALLOC(c->val);
    }

  c->state = ASSEMBLY_BEGIN;
  return HIPS_SUCCESS;
}


INTS HIPS_AssemblySetValue(INTS id, INTS i, INTS j, COEF v)
{
  HIPS_Context *c;
  c = context+id;
  if((c->state < ASSEMBLY_BEGIN ) || (c->state >= COEFFICIENT))
    {
      fprintferr(stderr, "HIPS_AssemblySetValue ERROR : you must call HIPS_AssemblyBegin before \n");
      return HIPS_ERR_MATASSEMB;
    }

  if(c->ind >= c->nnz)
    {
      fprintferr(stderr, "HIPS_AssemblySetValue Warning : nnz %ld is not large enough in HIPS_AssemblyBegin  \n Try to allocate 2*nnz \n ", (long)c->nnz);
      c->ii = (dim_t *)realloc(c->ii, sizeof(dim_t)*c->nnz*2);
      CHECK_MALLOC(c->ii);
      c->jj = (dim_t *)realloc(c->jj, sizeof(dim_t)*c->nnz*2);
      CHECK_MALLOC(c->jj);
      c->val = (COEF *)realloc(c->val, sizeof(COEF)*c->nnz*2);
      CHECK_MALLOC(c->val);
      c->nnz *= 2;
      
    }
  c->ii[c->ind] = i-c->numflag;
  c->jj[c->ind] = j-c->numflag;
  c->val[c->ind] = v;
  c->ind++;
  
  return HIPS_SUCCESS;
}


/** Coordinate in global numbering **/
INTS HIPS_AssemblySetNodeValues(INTS id, INTS i, INTS j, COEF *v)
{
  dim_t k1, k2;
  HIPS_Context *c;
  c = context+id;
  if((c->state < ASSEMBLY_BEGIN ) || (c->state >= COEFFICIENT))
    {
      fprintferr(stderr, "HIPS_AssemblySetNodeValues ERROR : you must call HIPS_AssemblyBegin before \n");
      return HIPS_ERR_MATASSEMB;
    }

  if(c->dof > 1)
    {
      /** Modified : storage by COLUMN **/
      for(k1=0;k1<c->dof;k1++)
	for(k2=0;k2<c->dof;k2++)
	  {    
	    if(c->ind >= c->nnz)
	      {
		fprintferr(stderr, "HIPS_AssemblySetNodeValues Warning : nnz %ld is not large enough in HIPS_AssemblyBegin  \n Try to allocate 2*nnz \n ", (long)c->nnz);
		c->ii = (dim_t *)realloc(c->ii, sizeof(dim_t)*c->nnz*2);
		CHECK_MALLOC(c->ii);
		c->jj = (dim_t *)realloc(c->jj, sizeof(dim_t)*c->nnz*2);
		CHECK_MALLOC(c->jj);
		c->val = (COEF *)realloc(c->val, sizeof(COEF)*c->nnz*2);
		CHECK_MALLOC(c->val);
		c->nnz *= 2;
	      } 
	    c->ii[c->ind] = (i-c->numflag)*c->dof + k1;
	    c->jj[c->ind] = (j-c->numflag)*c->dof + k2;
	    c->val[c->ind] = v[k2*c->dof+k1];
	    c->ind++;
	  }
    }
  else
    {
      if(c->ind >= c->nnz)
	{
	  fprintferr(stderr, "HIPS_AssemblySetNodeValues Warning : nnz %ld is not large enough in HIPS_AssemblyBegin  \n Try to allocate 2*nnz \n ", (long)c->nnz);
	  c->ii = (dim_t *)realloc(c->ii, sizeof(dim_t)*c->nnz*2);
	  CHECK_MALLOC(c->ii);
	  c->jj = (dim_t *)realloc(c->jj, sizeof(dim_t)*c->nnz*2);
	  CHECK_MALLOC(c->jj);
	  c->val = (COEF *)realloc(c->val, sizeof(COEF)*c->nnz*2);
	  CHECK_MALLOC(c->val);
	  c->nnz *= 2;
	}
      c->ii[c->ind] = i-c->numflag;
      c->jj[c->ind] = j-c->numflag;
      c->val[c->ind] = *v;
     
      c->ind++;
    }
  return HIPS_SUCCESS;
}


INTS HIPS_AssemblySetBlockValues(INTS id, INTS in, INTS *ilist, INTS jn, INTS *jlist, COEF *v)
{
  int k1, k2;
  HIPS_Context *c;
  c = context+id;
  if((c->state < ASSEMBLY_BEGIN ) || (c->state >= COEFFICIENT))
    {
      fprintferr(stderr, "HIPS_AssemblySetBlockValue ERROR : you must call HIPS_AssemblyBegin before \n");
      return HIPS_ERR_MATASSEMB;
    }

  /** Modified : storage by COLUMN **/
  for(k1=0;k1<in;k1++)
    for(k2=0;k2<jn;k2++)
      {     
	c->ii[c->ind] = ilist[k1]-c->numflag;
	c->jj[c->ind] = jlist[k2]-c->numflag;
	c->val[c->ind] = v[k2 * in + k1];

	if(c->ind >= c->nnz)
	  {
	    fprintferr(stderr, "HIPS_AssemblySetBlockValue ERROR : nnz %ld is not large enough in HIPS_GraphBegin  \n", (long)c->nnz);
	    return HIPS_ERR_PARAMETER;
	  }	
	c->ind++;
      }
  return HIPS_SUCCESS;
}


INTS HIPS_AssemblyEnd(INTS id)
{
  HIPS_Context *c;
  flag_t numflag; /** need this for temporary change **/
  INTL *ia;
  dim_t *ja;
  COEF *a;

  dim_t i;
  c = context+id;


  if(c->state != ASSEMBLY_BEGIN)
    {
      fprintf(stderr, "HIPS_AssemblyEnd : ERROR call does not match HIPS_AssemblyBegin \n");
      return HIPS_ERR_MATASSEMB;
    }


  /** Convert ijv to csr (the double edge are suppressed in the process) **/
  ijv2csr(c->na, c->ind, c->ii, c->jj, c->val, &ia, &ja, &a);

  if(c->ii != NULL)
    free(c->ii);
  
  if(c->jj != NULL)
    free(c->jj);

  if(c->val != NULL)
    free(c->val);

  if(c->dumpcsr == 1)
    {
      FILE *file;
      char filename[200]; 
      sprintf(filename, "matrix.%d", c->proc_id);
      if ((file = fopen(filename, "w")) == NULL)
	{

	  fprintferr(stderr, "ERROR unable to open file %s \n", filename);
	  return HIPS_ERR_IO;
	}
      else
	{
	  dumpcsr(file, a, ja ,ia, c->na);

	  fclose(file);
	}
    }

  /*** ATTENTION ia, ja, a are freed in this function ***/
  CHECK_RETURN(CSR_MatrixRedistr(id, 1, c->na, ia, ja, a, c->job, c->job2, c->mode, c->assemb_sym));
  

#ifdef DEBUG_M
  /*if(c->dumpcsr == 1)
    {
      PhidalMatrix_Print(stderr, &c->A.M);
      MPI_Barrier(c->mpicom);
      exit(0);
      }*/
#endif

  c->flagmat = 1;
  c->state = COEFFICIENT;


  return HIPS_SUCCESS;

}



INTS HIPS_TransposeMatrix(INTS id)
{
  HIPS_Context *c;
  c = context+id;

  PhidalDistrMatrix_Transpose(&c->A);
  c->state = COEFFICIENT;

  return HIPS_SUCCESS;

}




INTS HIPS_MatrixReset(INTS id)
{
  HIPS_Context *c;
  c = context+id;

  if(c->flagmat == 0)
    {
      return HIPS_SUCCESS;
    }
  PhidalMatrix_Reinit(&c->A.M);
  c->state = SETUP;
  return HIPS_SUCCESS;
}


INTS HIPS_FreePrecond(INTS id)
{
  HIPS_Context *c;
  c = context+id;

  if(c->precond == 0)
    return HIPS_SUCCESS;


  if(c->hybrid == 0)
    {
      PhidalDistrPrec_Clean(&c->Pscal);  
    }
  else
    {
#ifdef USE_SEQ_PRECOND
      if(c->nproc == 1) 
	DBPrec_Clean(&c->PblockSEQ);
      else
#endif
      DBDistrPrec_Clean(&c->Pblock);
      DBDistrPrec_Init(&c->Pblock);  
    }
	  
  c->precond = 0;
  c->state = COEFFICIENT;

  return HIPS_SUCCESS;
}

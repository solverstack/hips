/* @authors J. GAIDAMOUR, P. HENON */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <mpi.h>

#include "hips.h"
#include "hips_wrapper.h"
#include "hips_fortran.h"
#include "io.h"

/* fortran string to c string : NULL termination */
void Fstring2C(char *fortranstring , int len, char **cstring)
{
  if (len == 0)
      *cstring = NULL;
  else 
    {
      *cstring = (char *) malloc(sizeof(char) * (len+1));
      strncpy(*cstring , fortranstring, len);
      (*cstring)[len] = '\0';
    }
}

FORTRAN_NAME(
HIPS_INITIALIZE, hips_initialize,
(INTS *nid, INTS *ierr),
(nid, ierr))
{
  *ierr = HIPS_Initialize(*nid);
}

FORTRAN_NAME(
HIPS_SETDEFAULTOPTIONS, hips_setdefaultoptions,
(INTS *id, INTS *stratnum, INTS *ierr),
(id, stratnum, ierr))
{
  *ierr = HIPS_SetDefaultOptions(*id, *stratnum);
}


FORTRAN_NAME(
HIPS_SETOPTIONINT, hips_setoptionint,
(INTS *id, INTS *number, INTS *value, INTS *ierr),
(id, number, value, ierr))
{
  *ierr = HIPS_SetOptionINT(*id, *number, *value);
}


FORTRAN_NAME(
HIPS_SETOPTIONREAL, hips_setoptionreal,
(INTS *id, INTS *number, REAL *value, INTS *ierr),
(id, number, value, ierr))
{
  *ierr = HIPS_SetOptionREAL(*id, *number, *value);
}

FORTRAN_NAME(
HIPS_GRAPHBUILDHID, hips_graphbuildhid,
(INTS *id, INTS *ierr),
(id, ierr))
{
  *ierr = HIPS_GraphBuildHID(*id);
}

FORTRAN_NAME(
HIPS_GRAPHDISTRCSR, hips_graphdistrcsr,
(INTS *id, INTS *n, INTS *ln, INTS *nodelist, INTL *lrowptr, INTS *cols, INTS *ierr),
(id, n, ln, nodelist, lrowptr, cols, ierr))
{

  *ierr = HIPS_GraphDistrCSR(*id,  *n, *ln, nodelist, lrowptr, cols);
}

FORTRAN_NAME(
HIPS_GRAPHGLOBALCSR, hips_graphglobalcsr,
(INTS *id, INTS *n, INTL *ig, INTS *jg, INTS *root, INTS *ierr),
(id, n, ig, jg, root, ierr))
{
  *ierr = HIPS_GraphGlobalCSR(*id, *n, ig, jg, *root);
}

FORTRAN_NAME(
HIPS_GRAPHGLOBALCSC, hips_graphglobalcsc,
(INTS *id, INTS *n, INTL *ig, INTS *jg, INTS *root, INTS *ierr),
(id, n, ig, jg, root, ierr))
{
  *ierr = HIPS_GraphGlobalCSC(*id, *n, ig, jg, *root);
}

FORTRAN_NAME(
HIPS_SETUPSAVE, hips_setupsave,
(INTS *id, char* sfile_path, int pathlen, INTS *ierr),
(id, sfile_path, pathlen, ierr))
{
  char * good_path = NULL;
  Fstring2C(sfile_path , pathlen, &good_path);

  *ierr = HIPS_SetupSave(*id, good_path);
  if (good_path != NULL)
    free(good_path);
}

FORTRAN_NAME(
HIPS_SETUPLOAD, hips_setupload,
(INTS *id, char* sfile_path, int pathlen, INTS *ierr),
(id, sfile_path, pathlen, ierr))
{
  char * good_path = NULL;
  Fstring2C(sfile_path , pathlen, &good_path);

  *ierr = HIPS_SetupLoad(*id, sfile_path);
  if (good_path != NULL)
    free(good_path);
}

FORTRAN_NAME(
HIPS_LOCALMATRICESSAVE, hips_localmatricessave,
(INTS* id, INTS* nproc, INTS* n, INTL *ia, INTS *ja, COEF *a, char *sfile_path, INTS *ierr),
(id, nproc, n, ia, ja, a, sfile_path, ierr))
{
  *ierr = HIPS_LocalMatricesSave(*id, *nproc, *n, ia, ja, a, sfile_path);
}

FORTRAN_NAME(
HIPS_LOCALMATRICELOAD, hips_localmatriceload,
(INTS* id, INTS *symmetric, INTS* ln, INTL **lia, INTS **lja, COEF **la, char *sfile_path, INTS *ierr),
(id, symmetric, ln, lia, lja, la, sfile_path, ierr))
{
  *ierr = HIPS_LocalMatriceLoad(*id, symmetric, ln, lia, lja, la, sfile_path);
}

FORTRAN_NAME(
HIPS_PARALLELSETUP, hips_parallelsetup,
(INTS * id, INTS *ierr),
(id, ierr))
{
  *ierr = HIPS_ParallelSetup(*id);
}

FORTRAN_NAME(
HIPS_SETCOMMUNICATOR, hips_setcommunicator,
(INTS *id, MPI_Fint *commid, INTS *ierr),
(id, commid, ierr))
{
  MPI_Comm comm;
  comm  = MPI_Comm_f2c(*commid);
  *ierr = HIPS_SetCommunicator(*id, comm);
}



FORTRAN_NAME(
HIPS_MATRIXRESET, hips_matrixreset,
(INTS *id, INTS *ierr),
(id, ierr))
{
  *ierr = HIPS_MatrixReset(*id);
}

FORTRAN_NAME(
HIPS_FREEPRECOND, hips_freeprecond,
(INTS *id, INTS *ierr),
(id, ierr))
{
  *ierr = HIPS_FreePrecond(*id);
}

FORTRAN_NAME(
HIPS_MATRIXDISTRCSR, hips_matrixdistrcsr,
(INTS *id,  INTS *ln, INTS *unknownlist, INTL *lrowptr, INTS *cols, COEF *values, INTS *op, INTS *op2, INTS *mode, INTS *sym, INTS *ierr),
(id,  ln, unknownlist, lrowptr, cols, values, op, op2, mode, sym, ierr))
{

  *ierr = HIPS_MatrixDistrCSR(*id,  *ln, unknownlist, lrowptr, cols, values, *op, *op2, *mode, *sym);
}



FORTRAN_NAME(
HIPS_MATRIXLOCALCSR, hips_matrixlocalcsr,
(INTS *id, INTS *ln, INTS *unknownlist, INTL *lrowptr, INTS *lcols, COEF *values, INTS *op, INTS *op2, INTS *sym, INTS *ierr),
(id, ln, unknownlist, lrowptr, lcols, values, op, op2, sym, ierr))
{

  *ierr = HIPS_MatrixLocalCSR(*id, *ln, unknownlist, lrowptr, lcols, values, *op, *op2, *sym); 
}


FORTRAN_NAME(
HIPS_MATRIXGLOBALCSR, hips_matrixglobalcsr,
(INTS *id, INTS *n, INTL *ia, INTS *ja, COEF *a,  INTS *proc_root, INTS *job, INTS *sym, INTS *ierr),
(id, n, ia, ja, a, proc_root, job, sym, ierr))
{

  *ierr = HIPS_MatrixGlobalCSR(*id, *n, ia, ja, a, *proc_root, *job, *sym); 
}

FORTRAN_NAME(
HIPS_MATRIXGLOBALCSC, hips_matrixglobalcsc,
(INTS *id, INTS *n, INTL *ia, INTS *ja, COEF *a,  INTS *proc_root, INTS *job, INTS *sym, INTS *ierr),
(id, n, ia, ja, a, proc_root, job, sym, ierr))
{

  *ierr = HIPS_MatrixGlobalCSC(*id, *n, ia, ja, a, *proc_root, *job, *sym); 
}

FORTRAN_NAME(
HIPS_PRECOND, hips_precond,
(INTS *id, INTS *ierr),
(id, ierr))
{
  *ierr = HIPS_Precond(*id);
}

FORTRAN_NAME(
HIPS_MATRIXVECTORPRODUCT, hips_matrixvectorproduct,
(INTS *id, COEF *x, COEF *y, INTS *ierr),
(id, x, y, ierr))
{
  *ierr = HIPS_MatrixVectorProduct(*id, x, y);
}

FORTRAN_NAME(
HIPS_SETLOCALRHS, hips_setlocalrhs,
(INTS *id,  COEF *b, INTS *op, INTS *op2, INTS *ierr),
(id, b, op, op2, ierr))
{
  *ierr = HIPS_SetLocalRHS(*id, b, *op, *op2);
}


FORTRAN_NAME(
HIPS_SETGLOBALRHS, hips_setglobalrhs,
(INTS *id, COEF *b, INTS *root, INTS *op, INTS *ierr),
(id, b, root, op, ierr))
{
  *ierr = HIPS_SetGlobalRHS(*id, b, *root, *op);
}

FORTRAN_NAME(
HIPS_SETRHS, hips_setrhs,
(INTS *id, INTS *unknownnbr, INTS *unknownlist, COEF *b, 
 INTS *op, INTS *op2, INTS *mode, INTS *ierr),
(id, unknownnbr, unknownlist, b, op, op2, mode, ierr))
{
  *ierr = HIPS_SetRHS(*id, *unknownnbr, unknownlist, b, *op, *op2, *mode);
}

FORTRAN_NAME(
HIPS_SOLVE, hips_solve,
(INTS *id, INTS *ierr),
(id, ierr))
{
  *ierr = HIPS_Solve(*id);
}


FORTRAN_NAME(
HIPS_GETLOCALNODENBR, hips_getlocalnodenbr, 
(INTS *id, INTS * total, INTS *ierr),
(id, total, ierr))
{
  *ierr = HIPS_GetLocalNodeNbr(*id, total);
}

FORTRAN_NAME(
HIPS_GETLOCALNODELIST, hips_getlocalnodelist, 
(INTS *id, INTS *nodelist, INTS *ierr),
(id, nodelist, ierr))
{
  *ierr = HIPS_GetLocalNodeList(*id, nodelist);
}


FORTRAN_NAME(
HIPS_GETLOCALUNKNOWNNBR, hips_getlocalunknownnbr, 
(INTS *id, INTS * total, INTS *ierr),
(id, total, ierr))
{
  *ierr = HIPS_GetLocalUnknownNbr(*id, total);
}

FORTRAN_NAME(
HIPS_GETLOCALUNKNOWNLIST, hips_getlocalunknownlist, 
(INTS *id, INTS *nodelist, INTS *ierr),
(id, nodelist, ierr))
{
  *ierr = HIPS_GetLocalUnknownList(*id, nodelist);
}


FORTRAN_NAME(
HIPS_GETGLOBALPARTITIONNBR, hips_getglobalpartitionnbr, 
(INTS *id, INTS *domnbr, INTS *partnbr, INTS *domkeynbr, INTS *ierr),
(id, domnbr, partnbr, domkeynbr, ierr))
{
  *ierr = HIPS_GetGlobalPartitionNbr(*id, domnbr, partnbr, domkeynbr);
}


FORTRAN_NAME(
HIPS_GETGLOBALPARTITION, hips_getglobalpartition, 
(INTS *id, INTS *iperm, INTS *parttab, INTS *domkeyindex, INTS *domkey, INTS *ierr),
(id, iperm, parttab, domkeyindex, domkey, ierr))
{
  *ierr = HIPS_GetGlobalPartition(*id,  iperm, parttab, domkeyindex, domkey);
}


FORTRAN_NAME(
HIPS_GETLOCALPARTITIONNBR, hips_getlocalpartitionnbr, 
(INTS *id, INTS *domnbr, INTS *unknownnbr, INTS *partnbr, INTS *domkeynbr, INTS *ierr),
(id, domnbr, unknownnbr, partnbr, domkeynbr, ierr))
{
  *ierr = HIPS_GetLocalPartitionNbr(*id, domnbr, unknownnbr, partnbr, domkeynbr);
}

FORTRAN_NAME(
HIPS_GETLOCALPARTITION, hips_getlocalpartition, 
(INTS *id, INTS *unknownlist, INTS *parttab, INTS *domkeyindex, INTS *domkey, INTS *ierr),
(id, unknownlist, parttab, domkeyindex, domkey, ierr))
{
  *ierr = HIPS_GetLocalPartition(*id,  unknownlist, parttab, domkeyindex, domkey);
}




FORTRAN_NAME(
HIPS_GETLOCALDOMAINNBR, hips_getlocaldomainnbr, 
(INTS *id, INTS *domnbr, INTS *listsize, INTS *ierr),
(id, domnbr, listsize, ierr))
{
  *ierr = HIPS_GetLocalDomainNbr(*id, domnbr, listsize);
}



FORTRAN_NAME(
HIPS_GETLOCALDOMAINLIST, hips_getlocaldomainlist, 
(INTS *id, INTS *mapptr, INTS *mapp, INTS *ierr),
(id, mapptr, mapp, ierr))
{
  *ierr = HIPS_GetLocalDomainList(*id, mapptr, mapp);
}


FORTRAN_NAME(
HIPS_GETLOCALSOLUTION, hips_getlocalsolution, 
(INTS *id, COEF *x, INTS *ierr),
(id, x, ierr))
{
  *ierr = HIPS_GetLocalSolution(*id, x);
}


FORTRAN_NAME(
HIPS_GETGLOBALSOLUTION, hips_getglobalsolution, 
(INTS *id, COEF *globx, INTS *proc_root, INTS *ierr),
(id, globx, proc_root, ierr))
{
  *ierr = HIPS_GetGlobalSolution(*id, globx, *proc_root);
}

FORTRAN_NAME(
HIPS_GETSOLUTION, hips_getsolution, 
(INTS *id, INTS *n, INTS *nlist, COEF *x, INTS *mode, INTS *ierr),
(id, n, nlist, x, mode, ierr))
{
  *ierr = HIPS_GetSolution(*id, *n, nlist, x, *mode);
}

FORTRAN_NAME(
HIPS_SETPARTITION, hips_setpartition,
(INTS *id, INTS *ndom, INTS *mapptr, INTS *mapp, INTS *ierr),
(id, ndom, mapptr, mapp, ierr))
{
  *ierr = HIPS_SetPartition(*id,  *ndom, mapptr, mapp);
}

FORTRAN_NAME(
HIPS_SETSUBMATRIXCOEF, hips_setsubmatrixcoef, 
(INTS *id, INTS *op, INTS *op2, INTS *n, INTL *ia, INTS *ja, COEF *a, INTS *sym_matrix, INTS *ln, INTS *nodelist, INTS *ierr),
(id, op, op2, n, ia, ja, a, sym_matrix, ln, nodelist, ierr))
{

  *ierr = HIPS_SetSubmatrixCoef(*id,  *op, *op2, *n, ia, ja, a, *sym_matrix, *ln, nodelist);
}

FORTRAN_NAME(
HIPS_TRANSPOSEMATRIX, hips_transposematrix,
(INTS *id, INTS *ierr),
(id, ierr))
{
  *ierr = HIPS_TransposeMatrix(*id);
}

FORTRAN_NAME(
HIPS_CLEAN, hips_clean,
(INTS *id, INTS *ierr),
(id, ierr))
{
  *ierr = HIPS_Clean(*id);
}


FORTRAN_NAME(
HIPS_FINALIZE, hips_finalize, 
(INTS *ierr), 
(ierr))
{
  *ierr = HIPS_Finalize(); 
}


FORTRAN_NAME(
HIPS_CHECKSOLUTION, hips_checksolution, 
(INTS *id,  INTS *n, INTL *rowptr, INTS *cols, COEF *values, COEF *sol, COEF *rhs, INTS *sym, INTS *ierr),
(id,  n, rowptr, cols, values, sol, rhs, sym, ierr))
{
  *ierr = HIPS_CheckSolution(*id, *n, rowptr, cols, values, sol, rhs, *sym);
}

FORTRAN_NAME(
MATRIX_READ, matrix_read, 
(INTS *job, INTS *t_n, INTL* t_nnz, INTL *ia, INTS *ja, COEF *a, INTS* sym_matrix, char* matrix, int matrixlen),
(job, t_n, t_nnz, ia, ja, a, sym_matrix, matrix , matrixlen))
{
  char * good_matrix = NULL;
  Fstring2C(matrix , matrixlen, &good_matrix);

  fprintfd(stderr, "matrix %s \n", good_matrix);
  Matrix_Read(*job, t_n, t_nnz, ia, ja, a, sym_matrix, good_matrix);

  if (good_matrix != NULL)
    free(good_matrix);
}

FORTRAN_NAME(
VEC_READ, vec_read, 
(INTS *t_n, COEF *rhs, char* name, int namelen),
(t_n, rhs, name , namelen))
{
  char * cname = NULL;
  Fstring2C(name , namelen, &cname);

  VECread(cname, *t_n, rhs);

  if (cname != NULL)
    free(cname);
}


FORTRAN_NAME(
HIPS_READOPTIONSFROMFILE, hips_readoptionsfromfile,
(INTS *id, INTS *sym_pattern, INTS *sym_matrix, char *inputsname , char *matrixname, char *rhsname, int *ierr, int len , int len1, int len2),
(id, sym_pattern, sym_matrix,  inputsname, matrixname, rhsname, ierr, len, len1, len2))
{
  int fin;
  char *cinputsname = NULL;

  Fstring2C( inputsname , len, &cinputsname);

  *ierr = HIPS_ReadOptionsFromFile(*id,  cinputsname, sym_pattern, sym_matrix, matrixname, rhsname);

  /* Fortran strings are not NULL terminated, we have to fill them with blanks to be TRIM compliant : ET CA MA PRIS LA JOURNEE POUR TROUVER CETTE MEEERDE */
  fin = strlen(matrixname);
  memset( & matrixname[fin],' ',len1-fin);

  fin = strlen(rhsname);
  memset( & rhsname[fin],' ',len2-fin);

  if (cinputsname != NULL)
    free(cinputsname);
}

FORTRAN_NAME(
READ_OPTIONS, read_options,
(INTS *sym_pattern, INTS *sym_matrix, INTS *method, INTS *options_int, REAL *options_double, char *inputsname , char *matrixname, char *rhsname, int len , int len1, int len2),
(sym_pattern, sym_matrix, method, options_int, options_double, inputsname, matrixname, rhsname, len, len1, len2))
{
  int fin;
  char *cinputsname = NULL;

  Fstring2C( inputsname , len, &cinputsname);

  Read_options(cinputsname, matrixname, sym_pattern, sym_matrix, rhsname, method, options_int, options_double);

  fin = strlen(matrixname);
  memset( & matrixname[fin],' ',len1-fin);

  fin = strlen(rhsname);
  memset( & rhsname[fin],' ',len2-fin);

  if (cinputsname != NULL)
    free(cinputsname);
}



FORTRAN_NAME(
HIPS_GRAPHBEGIN, hips_graphbegin,
(INTS *id, INTS *n, INTL *edgenbr),
(id, n, edgenbr))
{
  HIPS_GraphBegin(*id, *n, *edgenbr);
}

FORTRAN_NAME(
HIPS_GRAPHEDGE, hips_graphedge,
(INTS *id, INTS *i, INTS *j),
(id, i, j))
{
  HIPS_GraphEdge(*id, *i, *j);
}

FORTRAN_NAME(
HIPS_GRAPHEND, hips_graphend,
(INTS *id),
(id))
{
  HIPS_GraphEnd(*id);
}



FORTRAN_NAME(
HIPS_ASSEMBLYBEGIN, hips_assemblybegin,
(INTS *id, INTL *nnz, INTS *op, INTS *op2, INTS *mode,  INTS *symmetric, INTS *ierr),
(id,  nnz, op, op2, mode, symmetric, ierr))
{
  *ierr = HIPS_AssemblyBegin(*id,  *nnz, *op, *op2, *mode, *symmetric);
}



FORTRAN_NAME(
HIPS_ASSEMBLYSETVALUE, hips_assemblysetvalue,
(INTS *id, INTS *i, INTS *j, COEF *v, INTS *ierr),
(id, i, j, v, ierr))
{
  *ierr = HIPS_AssemblySetValue(*id, *i, *j, *v);
}

FORTRAN_NAME(
HIPS_ASSEMBLYSETNODEVALUES, hips_assemblysetnodevalues,
(INTS *id, INTS *i, INTS *j, COEF *v, INTS *ierr),
(id, i, j, v, ierr))
{
  *ierr = HIPS_AssemblySetNodeValues(*id, *i, *j, v);
}

FORTRAN_NAME(
HIPS_ASSEMBLYSETBLOCKVALUES, hips_assemblysetblockvalues,
(INTS *id, INTS *ni, INTS *ilist, INTS *nj, INTS *jlist, COEF *v, INTS *ierr),
(id, ni, ilist, nj, jlist, v, ierr))
{
  *ierr = HIPS_AssemblySetBlockValues(*id, *ni, ilist, *nj, jlist, v);
}

FORTRAN_NAME(
HIPS_ASSEMBLYEND, hips_assemblyend,
(INTS *id, INTS *ierr),
(id, ierr))
{
  *ierr = HIPS_AssemblyEnd(*id);
}



FORTRAN_NAME(
HIPS_PRINTERROR, hips_printerror,
(INTS *ierr),
(ierr))
{
  HIPS_PrintError(*ierr);
}


FORTRAN_NAME(
HIPS_EXITONERROR, hips_exitonerror,
(INTS *ierr),
(ierr))
{
  HIPS_ExitOnError(*ierr);
}



FORTRAN_NAME(
HIPS_GETINFOINT, hips_getinfoint,
(INTS *id, INTS *metric, INTL *value, INTS *ierr),
(id, metric, value, ierr))
{
  *ierr = HIPS_GetInfoINT(*id, *metric, value);
}

FORTRAN_NAME(
HIPS_GETINFOREAL, hips_getinforeal,
(INTS *id, INTS *metric, REAL *value, INTS *ierr),
(id, metric, value, ierr))
{
  *ierr = HIPS_GetInfoREAL(*id, *metric, value);
}

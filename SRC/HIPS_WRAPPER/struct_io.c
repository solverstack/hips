/* @authors J. GAIDAMOUR, P. HENON */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "phidal_ordering.h"
#include "symbol.h"
#include "phidal_parallel.h"

#define WRITE_INT(var) { fprintf(fp, "%ld ", (long)((var))); }
#define READ_INT(type,var)  {int m;  long scan; m = fscanf(fp, "%ld ", &scan); if(m == 0) assert(0); (var) = (type)scan; }

/**/
#define WRITE_TAB(tab, n, format, args...) {  \
  if ((tab)==NULL) fprintf(fp, "-1 "); else { \
    fprintf(fp, "%ld ", (long)(n)); \
    for(i=0;i<(n);i++)          \
      fprintf(fp, format, args); \
   fprintf(fp, "\n"); \
  } \
} 
/**/

/**/
#define _begin_READ_TAB(type, tab, n) {  \
  long scan; \
  FSCANF(fp, "%ld ", &scan); \
  assert(scan==-1 || scan==n); \
  if (scan==-1) (tab) = NULL; else {\
    (tab) = (type *)malloc(sizeof(type)*(n)); \
    assert((tab) != NULL); \
    for(i=0;i<(n);i++) {
#define _end_READ_TAB } } }
/**/

#define WRITE_TAB_INT(tab, n) { WRITE_TAB((tab), (n), "%ld ", (long)tab[i]) }

#define READ_TAB_INT(type, tab, n) \
 _begin_READ_TAB(type, tab, n)     \
 FSCANF(fp, "%ld ", &scan);        \
 (tab)[i] = (type)scan; /* 32/64 bits */ \
 _end_READ_TAB



void PHIDAL_WriteDistrHID(FILE *fp, PhidalDistrHID *DBL)
{
  dim_t i;
  PhidalHID *LHID = &(DBL->LHID);

  PHIDAL_WriteHID(fp, &DBL->LHID);
  WRITE_INT(DBL->proc_id);
  WRITE_INT(DBL->nproc);
  WRITE_INT(DBL->globn);
  WRITE_INT(DBL->gnblock);
  WRITE_INT(DBL->adjpnbr);


  /* assert(DBL->node2proc == NULL); */ /* WRITE_TAB_INT(DBL->node2proc, ); */
  WRITE_TAB_INT(DBL->block_psetindex,   LHID->nblock+1);
  WRITE_TAB_INT(DBL->block_pset,        DBL->block_psetindex[LHID->nblock]);
  WRITE_TAB_INT(DBL->orig2loc,          DBL->globn);
  WRITE_TAB_INT(DBL->loc2orig,          LHID->n);
  WRITE_TAB_INT(DBL->loc2glob_blocknum, LHID->nblock);
  WRITE_TAB_INT(DBL->glob2loc_blocknum, DBL->gnblock);
  WRITE_TAB_INT(DBL->dom2proc,          LHID->ndom);
  WRITE_TAB_INT(DBL->row_leader,        LHID->nblock);
  WRITE_TAB_INT(DBL->adjptab,           DBL->adjpnbr);

  WRITE_INT(DBL->mpicom);
}

void PHIDAL_LoadDistrHID(FILE *fp, PhidalDistrHID *DBL)
{
  dim_t i;
  PhidalHID *LHID = &(DBL->LHID);

  PHIDAL_WriteHID(fp, &DBL->LHID);

  READ_INT(mpi_t, DBL->proc_id);
  READ_INT(mpi_t, DBL->nproc);
  READ_INT(dim_t, DBL->globn);
  READ_INT(int_t, DBL->gnblock);
  READ_INT(mpi_t, DBL->adjpnbr);

  DBL->node2proc = NULL; /* READ_TAB_INT(DBL->node2proc, ); */
  READ_TAB_INT(mpi_t, DBL->block_psetindex,   LHID->nblock+1);
  READ_TAB_INT(dim_t, DBL->block_pset,        DBL->block_psetindex[LHID->nblock]);
  READ_TAB_INT(dim_t, DBL->orig2loc,          DBL->globn);
  READ_TAB_INT(dim_t, DBL->loc2orig,          LHID->n);
  READ_TAB_INT(int_t, DBL->loc2glob_blocknum, LHID->nblock);
  READ_TAB_INT(int_t, DBL->glob2loc_blocknum, DBL->gnblock);
  READ_TAB_INT(mpi_t, DBL->dom2proc,          LHID->ndom);
  READ_TAB_INT(mpi_t, DBL->row_leader,        LHID->nblock);
  READ_TAB_INT(mpi_t, DBL->adjptab,           DBL->adjpnbr);

  READ_INT(MPI_Comm, DBL->mpicom);
}

void PHIDAL_WriteHID(FILE *fp, PhidalHID *BL)
{
  dim_t i;
  
  WRITE_INT(BL->ndom);
  WRITE_INT(BL->locndom);
  WRITE_INT(BL->n);
  WRITE_INT(BL->nblock);
  WRITE_INT(BL->nlevel);
  WRITE_INT(BL->dof);
  fprintf(fp, "\n");

  WRITE_TAB_INT(BL->block_index,      BL->nblock+1);
  WRITE_TAB_INT(BL->block_levelindex, BL->nlevel+1);
  WRITE_TAB_INT(BL->block_keyindex,   BL->nblock+1);
  WRITE_TAB_INT(BL->block_key,        BL->block_keyindex[BL->nblock]);
  WRITE_TAB_INT(BL->block_domwgt,     BL->block_levelindex[1]);
}

void PHIDAL_LoadHID(FILE *fp, PhidalHID *BL)
{
  dim_t i;
  
  PhidalHID_Init(BL);

  READ_INT(dim_t, BL->ndom);
  READ_INT(dim_t, BL->locndom);
  READ_INT(dim_t, BL->n);
  READ_INT(int_t, BL->nblock);
  READ_INT(int_t, BL->nlevel);
  READ_INT(dim_t, BL->dof);
  
  READ_TAB_INT(dim_t, BL->block_index,      BL->nblock+1);
  READ_TAB_INT(dim_t, BL->block_levelindex, BL->nlevel+1);
  READ_TAB_INT(dim_t, BL->block_keyindex,   BL->nblock+1);
  READ_TAB_INT(dim_t, BL->block_key,        BL->block_keyindex[BL->nblock]);
  READ_TAB_INT(INTL,   BL->block_domwgt,     BL->block_levelindex[1]);
}

void PHIDAL_WriteSymbolMatrix(FILE *fp, SymbolMatrix *symbmtx)
{
  dim_t i;
  
  WRITE_INT(symbmtx->cblknbr);
  WRITE_INT(symbmtx->bloknbr);
  WRITE_INT(symbmtx->nodenbr);
  WRITE_INT(0); /* symbmtx->tli,      */
  WRITE_INT(0); /* symbmtx->facedecal */
  WRITE_INT(0); /* symbmtx->cblktlj   */ 
  WRITE_INT(0); /* symbmtx->virtual   */

  WRITE_TAB(symbmtx->ccblktab, symbmtx->cblknbr, "%ld %ld ",   
	    (long)symbmtx->ccblktab[i].fcolnum, (long)symbmtx->ccblktab[i].lcolnum);
  
  WRITE_TAB(symbmtx->bcblktab, symbmtx->cblknbr,  
	    "%ld %ld ", (long)symbmtx->bcblktab[i].fbloknum, (long)symbmtx->bcblktab[i].lbloknum);
  WRITE_TAB(symbmtx->bloktab, symbmtx->bloknbr, 
	    "%ld %ld %ld ", (long)symbmtx->bloktab[i].frownum, (long)symbmtx->bloktab[i].lrownum,
	    (long)symbmtx->bloktab[i].cblknum);
  
  /* for(i=0;i<=symbmtx->cblknbr;i++) */
  /*     fprintf(fp, "%d ", symbmtx->hdim[i]); */
  /*   fprintf(fp, "\n"); */
  assert(symbmtx->hdim == NULL);
  assert(symbmtx->stride == symbmtx->hdim);
}

void PHIDAL_LoadSymbolMatrix(FILE *fp, SymbolMatrix* symbmtx)
{
  dim_t i;
  long s1, s2, s3;

  READ_INT(dim_t, symbmtx->cblknbr);
  READ_INT(dim_t, symbmtx->bloknbr);
  READ_INT(dim_t, symbmtx->nodenbr);
  READ_INT(dim_t, symbmtx->tli);
  READ_INT(dim_t, symbmtx->facedecal);
  READ_INT(dim_t, symbmtx->cblktlj);
  READ_INT(flag_t, symbmtx->virtual);

  _begin_READ_TAB(SymbolCCblk, symbmtx->ccblktab, symbmtx->cblknbr) {
    FSCANF(fp, "%ld %ld ", &s1, &s2);
    symbmtx->ccblktab[i].fcolnum = (dim_t)s1;
    symbmtx->ccblktab[i].lcolnum = (dim_t)s2;
  } _end_READ_TAB;
    
  _begin_READ_TAB(SymbolBCblk, symbmtx->bcblktab, symbmtx->cblknbr) {
    FSCANF(fp, "%ld %ld ", &s1, &s2);
    symbmtx->bcblktab[i].fbloknum = (INTL)s1;
    symbmtx->bcblktab[i].lbloknum = (INTL)s2;
  } _end_READ_TAB;
  
  _begin_READ_TAB(SymbolBlok, symbmtx->bloktab, symbmtx->bloknbr) {
    FSCANF(fp, "%ld %ld %ld ", &s1, &s2, &s3);
    symbmtx->bloktab[i].frownum = (dim_t)s1;
    symbmtx->bloktab[i].lrownum = (dim_t)s2;
    symbmtx->bloktab[i].cblknum = (dim_t)s3;
  } _end_READ_TAB
	
  symbmtx->hdim = NULL;
  symbmtx->stride = symbmtx->hdim;
}

void PHIDAL_WriteIperm(FILE *fp, dim_t n, dim_t *iperm)
{
  dim_t i;
  WRITE_TAB_INT(iperm,   n);
}

void PHIDAL_LoadIperm(FILE *fp, dim_t n, dim_t **pt_iperm)
{
  dim_t i;
  dim_t* iperm;
  READ_TAB_INT(dim_t, iperm, n);
  *pt_iperm=iperm;
}


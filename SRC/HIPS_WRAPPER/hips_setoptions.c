/* @authors P. HENON */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <mpi.h>
#include "math.h"

#include "hips_wrapper.h"
#include "hips.h"

#include "scotch_metis_wrapper.h"

extern int_t idnbr;
extern HIPS_Context *context;

#define BUFLEN 200

INTS HIPS_SetDefaultOptions(INTS id, INTS stratnum)
{
  HIPS_Context *c;
  c = context+id;
  
  /* Reinit */
  PhidalOptions_Init(&c->options);

  /* Only set option parameters that should be changed */
  switch(stratnum)
    {

    case HIPS_DIRECT:
      c->hybrid = 1;
      HIPS_SetOptionINT(id, HIPS_FORWARD, 0);
      HIPS_SetOptionINT(id, HIPS_SCHUR_METHOD, 0);
      HIPS_SetOptionINT(id, HIPS_SCALE, 0);
      HIPS_SetOptionINT(id, HIPS_DOMSIZE, INT_MAX);
      HIPS_SetOptionINT(id, HIPS_ITMAX, 1);
      HIPS_SetOptionINT(id, HIPS_KRYLOV_RESTART, 1);
      HIPS_SetOptionREAL(id, HIPS_DROPTOL0, 0.0);
      HIPS_SetOptionREAL(id, HIPS_DROPTOL1, 0.0);
      HIPS_SetOptionREAL(id, HIPS_DROPTOLE, 0.0);
      break;

    case HIPS_ITERATIVE:
      c->hybrid = 0;
      HIPS_SetOptionINT(id, HIPS_FORWARD, 1);
      HIPS_SetOptionINT(id, HIPS_SCHUR_METHOD, 0);
      HIPS_SetOptionREAL(id, HIPS_DROPTOL0, 0.001);
      HIPS_SetOptionREAL(id, HIPS_DROPTOL1, 0.001);
      HIPS_SetOptionREAL(id, HIPS_DROPTOLE, 0.001);
      HIPS_SetOptionINT(id, HIPS_PARTITION_TYPE, 3);
      break;

    case HIPS_HYBRID:
      /* it is currently the default strategy */
      c->hybrid = 1;
      HIPS_SetOptionREAL(id, HIPS_DROPTOL0, 0.0); 
      HIPS_SetOptionREAL(id, HIPS_DROPTOL1, 0.005); 
      HIPS_SetOptionREAL(id, HIPS_DROPTOLE, 0.005); 
      HIPS_SetOptionINT (id, HIPS_LOCALLY,  100); 
      HIPS_SetOptionINT(id, HIPS_FORWARD, 1);
      HIPS_SetOptionINT(id, HIPS_SCHUR_METHOD, 2);
      HIPS_SetOptionINT(id, HIPS_ITMAX, 1);
      HIPS_SetOptionINT(id, HIPS_ITMAX_SCHUR, 150);
      
      break;

    case HIPS_BLOCK:
      c->hybrid = 1;
      HIPS_SetOptionINT(id, HIPS_SCALE, 0);
      HIPS_SetOptionREAL(id, HIPS_DROPTOL0, 0.0);
      HIPS_SetOptionREAL(id, HIPS_DROPTOL1, 0.0);
      HIPS_SetOptionREAL(id, HIPS_DROPTOLE, 0.0);
      break;

    default:
      fprintferr(stderr, "ERROR in HIPS_SetDefaultOptions : stratnum not recognized \n");
      return HIPS_ERR_PARAMETER;
    }
  return HIPS_SUCCESS;
}

INTS HIPS_SetOptionINT(INTS id, INTS number, INTS value)
{
  HIPS_Context *c;
  PhidalOptions *options;
  c = context+id;

  options = &c->options;
  switch(number)
    {
    case HIPS_SYMMETRIC:
      options->symmetric = value;
      c->graph_sym = 1;
      break;
    case HIPS_VERBOSE:
      options->verbose = value;
      break;
    case HIPS_PIVOTING:
      options->pivoting = value;
      break;
    case HIPS_SCALE:
      options->scale = value;
      break;
    case HIPS_SCALENBR:
      options->scalenbr = value;
      break;
    case HIPS_LOCALLY:
      options->locally_nbr = value;
      break;
    case HIPS_KRYLOV_RESTART:
      options->krylov_im = value;
      break;
    case HIPS_ITMAX:
      options->itmax = value;
      break;
    case HIPS_ITMAX_SCHUR:
      options->itmaxforw = value;
      break;
    case HIPS_PARTITION_TYPE:
      c->partition_type = value;
      break;
    case HIPS_KRYLOV_METHOD:
      options->krylov_method = value;
      break;
    case HIPS_SCHUR_METHOD:
      options->schur_method = value;
      break;
    case HIPS_FORWARD:
      options->forwardlev = value;
      break;
    case HIPS_USE_PASTIX:
      options->use_pastix = value;
      break;
    case HIPS_DOMSIZE:
      c->domsize = value;
      c->partition_type = 0;
      break;
    case HIPS_DOMNBR:
      c->ndom = value;
      break;
    case HIPS_REORDER:
      c->reorder = value;
      break;
    case HIPS_MASTER:
      if(c->state <= HIERARCH)
	c->master = value;
      else
	{
	  fprintferr(stderr, "ERROR in HIPS_SetOptionINT : can't change the master processor at this stage \n");
	  return HIPS_ERR_PARAMETER;
	}
      break;
    case HIPS_COARSE_GRID:
      c->coarse_grid = value;
      break;
    case HIPS_CHECK_GRAPH:
      c->check_graph = value;
      break;
    case HIPS_CHECK_MATRIX:
      c->check_matrix = value;
      break;
    case HIPS_DUMP_CSR:
      c->dumpcsr = value;
      break;
    case HIPS_IMPROVE_PARTITION:
      c->improve_partition = value;
      break;
    case HIPS_TAGNBR:
      c->tagnbr = value;
      break;
    case HIPS_SHIFT_DIAG:
      options->shiftdiag = value;
      break;
    case HIPS_GRAPH_SYM:
      if(options->symmetric == 1 && value == 0)
	c->graph_sym = 1; /** Forbiden value = 0 when the matrix is symmetric **/
      else
	c->graph_sym = value;
      break;
    case HIPS_GRID_DIM:
      c->nc = value;
      c->partition_type = -1;
      break;
    case HIPS_GRID_3D:
      c->cube = value;
      assert(c->partition_type == -1);
      break;
    case HIPS_DISABLE_PRECOND:
      c->donotprecond = value;
      break;
    case HIPS_FORTRAN_NUMBERING:
      if(value != 0 && value != 1)
	{
	  fprintferr(stderr, "Error in HIPS_SetOptionINT : bad value for HIPS_FORTRAN_NUMBERING \n");
	  return HIPS_ERR_PARAMETER;
	}
      c->numflag = value;
      break;
    case HIPS_DOF:
      c->dof = value;
      break;

    default:
      fprintferr(stderr, "ERROR in HIPS_SetOptionINT : Integer option not recognized \n");
      return HIPS_ERR_PARAMETER;
    }
  return HIPS_SUCCESS;
}


INTS HIPS_SetOptionREAL(INTS id, INTS number, REAL value)
{
  HIPS_Context *c;
  PhidalOptions *options;

  c = context+id;
  options = &c->options;

  switch(number)
    {
    case HIPS_PREC:
      options->tol = value; /*todo : change name*/
      break;
    case HIPS_DROPTOL0:
      if(c->hybrid == 1 && value > 0)
	fprintferr(stderr, "WARNING in HIPS_SetOptionREAL : can not set HIPS_DROPTOL0 > 0 in the HYBRID strategy \n");


      if(c->hybrid == 0 && value <= 0)
	fprintferr(stderr, "WARNING in HIPS_SetOptionREAL : you should not set HIPS_DROPTOL0 <= 0 in the ITERATIVE strategy \n");

      options->droptol0 = value;
      break;
    case HIPS_DROPTOL1:
      options->droptol1 = value;
      break;
    case HIPS_DROPTOLE:
      options->droptolE = value;
      break;
    case HIPS_DROPSCHUR:
      options->dropSchur = value;
      break;
    case HIPS_AMALG:
      options->amalg_rat = value;
      break;
    default:
      fprintferr(stderr, "ERROR in HIPS_SetOptionREAL : Real option not recognized \n");
      return HIPS_ERR_PARAMETER;
    }
  return HIPS_SUCCESS;
}

INTS HIPS_ReadOptionsFromFile_Extra(INTS id,  char *filename);

INTS HIPS_ReadOptionsFromFile(INTS id,  char *inputsname, INTS *sym_pattern, INTS *sym_matrix, char *matrixname, char *rhsname)
{
  /** Read HIPS parameters from the file inputsname **/
  /** Set the parameters and return the path to the matrix file in matrixname **/
  /** Path to the rsh in rhsname **/
  

  /** If filename == NULL then the default filename used is "Inputs" **/
  INTS  method;
  INTS  options_int   [OPTIONS_INT_NBR];
  REAL  options_double[OPTIONS_REAL_NBR];
  
  Read_options(inputsname, matrixname, sym_pattern, sym_matrix, rhsname, &method, options_int, options_double);
  
  /*******************************/
  /* Put some specific options   */  
  /*******************************/
  CHECK_RETURN(HIPS_SetDefaultOptions(id, method));
/*   if(method == HIPS_HYBRID) */
/*     { */
/*       if(argc < 2) */
/* 	{ */
/* 	  fprintf(stderr, "You must use a domsize parameter with HYBRID strategy \n"); */
/* 	  fprintf(stderr, "testHIPS.ex <domsize> \n"); */
/* 	  exit(-1); */
/* 	} */
/*     } */
  
  CHECK_RETURN(HIPS_SetOptionREAL(id, HIPS_PREC,           options_double[HIPS_PREC]));
  CHECK_RETURN(HIPS_SetOptionINT (id, HIPS_LOCALLY,        options_int[HIPS_LOCALLY]));
  
  if(method == HIPS_HYBRID)
    {
      fprintferr(stderr, "Warning: by default in HYBRID method, maximum number of outer iteration is set to 1 (i.e. only iterates in Schur complement) \n");
      CHECK_RETURN(HIPS_SetOptionINT (id, HIPS_ITMAX, 1));
      CHECK_RETURN(HIPS_SetOptionINT (id, HIPS_ITMAX_SCHUR, options_int[HIPS_ITMAX]));
    }
  else
    {
      CHECK_RETURN(HIPS_SetOptionINT (id, HIPS_ITMAX, options_int[HIPS_ITMAX]));
    }
  
  CHECK_RETURN(HIPS_SetOptionINT (id, HIPS_KRYLOV_RESTART, options_int[HIPS_KRYLOV_RESTART]));
  if(method != HIPS_HYBRID)
    CHECK_RETURN(HIPS_SetOptionREAL(id, HIPS_DROPTOL0,       options_double[HIPS_DROPTOL0]));
  CHECK_RETURN(HIPS_SetOptionREAL(id, HIPS_DROPTOL1,       options_double[HIPS_DROPTOL1]));
  CHECK_RETURN(HIPS_SetOptionREAL(id, HIPS_DROPTOLE,       options_double[HIPS_DROPTOLE]));
  CHECK_RETURN(HIPS_SetOptionINT (id, HIPS_VERBOSE,        options_int[HIPS_VERBOSE]));
  CHECK_RETURN(HIPS_SetOptionINT (id, HIPS_USE_PASTIX,     options_int[HIPS_USE_PASTIX]));

  CHECK_RETURN(HIPS_ReadOptionsFromFile_Extra(id, "Inputs"));
  CHECK_RETURN(HIPS_ReadOptionsFromFile_Extra(id, "InputsExtra"));

  return HIPS_SUCCESS;
}

INTS HIPS_ReadOptionsFromFile_Extra(INTS id,  char *filename)
{
  FILE* fp;
  char buf[BUFLEN];

  if((fp = fopen(filename,"r")) == NULL) 
    {
      /* no additionnal settings */
      return HIPS_SUCCESS;
    }

  while(fgets(buf, BUFLEN, fp) != NULL) {
    /* printf("HIPS_ReadOptionsFromFile_Extra : %s", buf); */

    int   num = atoi(buf+2);
    char* pt  = strchr(buf, '=');
    if (pt == NULL) continue; /* not '=' on the line */

    pt++;
    /* useless : if (pt == '\n') continue; /\* end of line after '=' *\/ */
    
    /* printf("pt=%s\n", pt); */

    switch(buf[0]) {
    case 'D':{
      int val=-42;
      sscanf(pt+1, "%d", &val);
      
      if(val != -42) {
	CHECK_RETURN(HIPS_SetOptionINT(id, num, val));
	buf[strlen(buf)-1] ='\0'; printfd("ExtraOPT \"%s\"\n", buf);
      }

      break;
    }
    case 'R':{
      REAL val=-42;
      sscanf(pt, _scan_f_, &val);
      
      if(val != -42) {
	CHECK_RETURN(HIPS_SetOptionREAL(id, num, val));
	buf[strlen(buf)-1] ='\0'; printfd("ExtraOPT \"%s\"\n", buf);
      }

      break;
    }
    }

  }

  fclose(fp);

  return HIPS_SUCCESS;

}

INTS HIPS_SetCommunicator(INTS id, MPI_Comm mpicom)
{
  HIPS_Context *c;
  c = context+id;
  
  if(c->state > HIERARCH)
    {
      fprintferr(stderr, "ERROR in HIPS_SetCommunicator : cannot change the communicator now \n You must do it before \n");
      return HIPS_ERR_PARAMETER;
    }
  
  c->mpicom = mpicom;
  MPI_Comm_size(c->mpicom, &c->nproc);
  MPI_Comm_rank(c->mpicom, &c->proc_id);
  return HIPS_SUCCESS;
}


INTS HIPS_SetPartition(INTS id, INTS ndom, INTS *mapptr, INTS *mapp)
{
  INTL i, nn;
  HIPS_Context *c;
  flag_t overlap;

  c = context+id;

  
  if(c->proc_id != c->master)
    return HIPS_SUCCESS;

  
#ifdef DEBUG_M
  assert(c->numflag == 0 || c->numflag == 1);
  assert(mapptr[0] == c->numflag);
#endif


  nn = mapptr[ndom] - c->numflag;
  
  c->ndom = ndom;
  c->nproc = ndom;

  c->mapptr = (dim_t *)malloc(sizeof(dim_t)*(ndom+1));
  CHECK_MALLOC(c->mapptr);
  c->mapp = (dim_t *)malloc(sizeof(dim_t)*nn);
  CHECK_MALLOC(c->mapp);

  /** detect overlap **/
  overlap = 0;
  bzero(c->mapp, sizeof(INTS)*nn);
  for(i=0;i<nn;i++)
    if(c->mapp[mapp[i]-c->numflag] != 0)
      {
	overlap = 1;
	break;
      }
    else
      c->mapp[mapp[i]-c->numflag]=1;

  memcpy(c->mapptr, mapptr, sizeof(dim_t)*(ndom+1));
  memcpy(c->mapp, mapp, sizeof(dim_t)*nn);
  
  if(c->numflag == 1)
    {
      for(i=0;i<ndom+1;i++)
	c->mapptr[i]--;
      for(i=0;i<nn;i++)
	c->mapp[i]--;
    }



  if(overlap == 0)
    {
      fprintfd(stderr, "NO OVERLAP DETECTED IN HIPS_SetPartition \n");
      c->partition_type = 1;
    }
  else
    {
      fprintfd(stderr, "OVERLAP DETECTED IN HIPS_SetPartition \n");
      c->partition_type = 2;
    }
  return HIPS_SUCCESS;
}

/* @authors  J. GAIDAMOUR  */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <mpi.h>
#include "math.h"

#include "hips_wrapper.h"
#include "hips.h"

#include "scotch_metis_wrapper.h"

extern int_t idnbr;
extern HIPS_Context *context;

#define BUFLEN 2000
INTS HIPS_SetupSave(INTS id, char* sfile_path) 
{
  /*** @@OIMNE free ig, jg apres avoir sauvegarder les graphe locaux
       lorsque partition_type != 0 ****/

  HIPS_Context *c;
  PhidalOptions *option;
  c = context+id;
  option = &c->options;

  if(c->state < HIERARCH)
    {
      fprintferr(stderr, "ERROR in HIPS_ParallelSetupSave :  Call this function after Hierach. Decomp.  \n");
      exit(-1);
    }
    
  if (c->proc_id == 0) {
    
    FILE* fp;
    char filename[BUFLEN];  
    
    dim_t domsize         = c->domsize;
    PhidalHID* BL         = &c->BL;
    SymbolMatrix* symbmtx = c->symbmtx;
    
    /*** Write the Global Phidal decomposition and ordering structure ***/
    sprintf(filename, "%s_%d.hid", sfile_path, domsize);
    fp = fopen(filename, "w");
    if(fp == NULL)
      {
	fprintferr(stderr, "ERROR: Unable to create file %s\n", filename);
	return HIPS_ERR_IO;
      }
    
    fprintfv(3, stderr, "Save HID structure in file %s ..." , filename);
    PHIDAL_WriteHID(fp, BL);
    fprintfv(3, stderr, "done \n");
    
    fclose(fp);
    
    /***/

    /*** Write the iperm vector ***/
    sprintf(filename, "%s_%d.iperm", sfile_path, domsize);
    fp = fopen(filename, "w");
    if(fp == NULL)
      {
	fprintferr(stderr, "ERROR: Unable to create file %s\n", filename);
	return HIPS_ERR_IO;
      }
  
    fprintfv(3, stderr, "Save iperm vector in file %s ..." , filename);
    PHIDAL_WriteIperm(fp, BL->n, c->iperm);
    fprintfv(3, stderr, "done \n");
    fclose(fp);
    /***/

    if (symbmtx != NULL) {

      /*** Write the Global Symbol Matrix ***/
      sprintf(filename, "%s_%d.symbol", sfile_path, domsize);
      fp = fopen(filename, "w");
      if(fp == NULL)
	{
	  fprintferr(stderr, "ERROR: Unable to create file %s\n", filename);
	  return HIPS_ERR_IO;
	}
  
      fprintfv(3, stderr, "Save Symbol structure in file %s ..." , filename);
      PHIDAL_WriteSymbolMatrix(fp, symbmtx);
      fprintfv(3, stderr, "done \n");
      fclose(fp);

      SymbolMatrix_Clean(c->symbmtx);
      free(c->symbmtx); c->symbmtx = NULL;

    }
    assert(c->ig == NULL);
    assert(c->jg == NULL); 
  }

  /* c->state = SETUP; */
  return HIPS_SUCCESS;
}

INTS HIPS_LocalMatricesSave(INTS id, INTS nproc, INTS n, INTL *ia, INTS *ja, COEF *a, char *sfile_path)
{
  
  HIPS_Context *c;
  PhidalHID *BL;
 /*  PhidalDistrHID mDBL; */
  PhidalDistrHID* DBL;
  
  PhidalOptions *option;

  dim_t *nodelist;
  mpi_t p;
  INTL *lia; dim_t *lja; COEF *la; dim_t ln;
  FILE *fp; 
  char filename[400];
  mpi_t *dom2proc;
 

  c = context+id;
  option = &c->options;
  BL = &c->BL;
 
  if(c->state < HIERARCH)
    {
      fprintferr(stderr, "ERROR in HIPS_LocalMatricesSave :  Call this function after Hierach. Decomp.  \n");
      exit(-1);
    }
  /* Translate matrix into C numbering */
  if(c->numflag != 0)
    CSR_Fnum2Cnum(ja, ia, n);
  
  dom2proc = (mpi_t *)malloc(sizeof(mpi_t)*BL->ndom);
  PhidalHID_MapDomains(nproc, BL, dom2proc);

  for(p=0; p<nproc; p++) {
    mpi_t *dom2proc2;
    PhidalDistrHID mDBL;
    DBL = &mDBL;
    
    dom2proc2 = (mpi_t *)malloc(sizeof(mpi_t)*BL->ndom); /** free in PhidalDistrHID_Clean **/
    memcpy( dom2proc2, dom2proc, sizeof(mpi_t)*BL->ndom);

    PhidalDistrHID_Setup(p, nproc, -1, BL, DBL, c->iperm, c->mpicom);

    PhidalDistrHID_Init(DBL);
    DBL->proc_id = p;
    DBL->globn  = BL->n;
    DBL->gnblock = BL->nblock;
    DBL->nproc = nproc;
    DBL->dom2proc = dom2proc2;
    DBL->mpicom = c->mpicom;
    PhidalDistrHID_GenereLocalHID(p, BL, DBL, c->iperm);
  
    if(DBL->LHID.n == 0) {
      fprintferr(stderr, "ERROR in HIPS_LocalMatricesSave : too much processors \n");
      return HIPS_ERR_IO;
    }

    
    /*** Write the local HID ***/
    sprintf(filename, "%s_%d.mtx.%d-%d_hid", sfile_path, c->domsize, (int)p, nproc);
    fp = fopen(filename, "w");
    if(fp == NULL)
      {
	fprintferr(stderr, "ERROR: Unable to create file %s\n", filename);
	return HIPS_ERR_IO;
      }
    
    fprintfv(3, stderr, "Save DistrHID in file %s ..." , filename);
    PHIDAL_WriteDistrHID(fp, DBL);
    fprintfv(3, stderr, "done \n");
    fclose(fp);


    ln = DBL->LHID.n; 

    nodelist = DBL->loc2orig; DBL->loc2orig = NULL;    
    /* nodelist = (dim_t *)malloc(sizeof(dim_t)*ln); */
    /* memcpy(nodelist, DBL->loc2orig, sizeof(dim_t)*ln); */
    PhidalDistrHID_Clean(DBL);
    /*     PhidalDistrHID_Init(DBL); */
    /* assert(numflag == 0); */ /* cf HIPS_GetLocalNodeList */
    
    HIPS_GetSubmatrix(ln, 0, nodelist, n, ia, ja, a, &lia, &lja, &la);
    
    /*** Write the local matrix ***/
    sprintf(filename, "%s_%d.mtx.%d-%d", sfile_path, c->domsize, (int)p, nproc);
    fp = fopen(filename, "w");
    if(fp == NULL)
      {
	fprintferr(stderr, "ERROR: Unable to create file %s\n", filename);
	return HIPS_ERR_IO;
      }
    
    fprintfv(3, stderr, "Save matrix in file %s ..." , filename);
    CSR_Write(fp,  c->options.symmetric, ln, lia, lja, la);
    fprintfv(3, stderr, "done \n");
    fclose(fp);

    free(nodelist);

    free(lia);
    free(lja);
    free(la);
  }
  free(dom2proc);
  PhidalHID_Clean(BL);
  PhidalHID_Init(BL);
  /* Translate matrix into C numbering */
  if(c->numflag != 0)
    CSR_Cnum2Fnum(ja, ia, n);
  
  /** Need iperm in HIPS_MatrixSetGlobalCSR **/
  /*free(c->iperm); c->iperm = NULL;*/
  return HIPS_SUCCESS;
}

INTS HIPS_SetupLoad(INTS id, char* sfile_path) 
{
  HIPS_Context *c;
  PhidalOptions *option;

  c = context+id;
  option = &c->options;

  FILE* fp;
  char filename[BUFLEN];  

  dim_t n;

  /*************************/
  /** Load the global HID **/
  /*************************/

  sprintf(filename, "%s_%d.hid", sfile_path, c->domsize);
  /* fprintfd(stderr, "Proc %d: Load HID and domain decomposition from file %s ...\n", proc_id, filename); */
  fprintfd(stderr, "Load HID and domain decomposition from file %s ...\n", filename);
  fp = fopen(filename, "r");
  if(fp == NULL)
    { 
      /* fprintfd(, stderr, "Proc %d ERROR: Unable to open file %s \n", proc_id, filename); */
      fprintferr(stderr, "ERROR: Unable to open file %s\n", filename);
      MPI_Abort(c->mpicom, -1);
    }

  PHIDAL_LoadHID(fp, &c->BL);

  fclose(fp);


  /*************************/
  /** Load the iperm vector **/
  /*************************/

  sprintf(filename, "%s_%d.iperm", sfile_path, c->domsize);
  fprintferr(stderr, "Load iperm vector from file %s ...\n", filename);
  fp = fopen(filename, "r");
  if(fp == NULL)
    { 
      /* fprintfv(5, stderr, "Proc %d ERROR: Unable to open file %s \n", proc_id, filename); */
      fprintferr(stderr, "ERROR: Unable to open file %s\n", filename);
      return HIPS_ERR_IO;
    }

  n = c->BL.n;
  c->ng = n;



  PHIDAL_LoadIperm(fp, n, &c->iperm);

  fclose(fp);
  /**/

  if (c->hybrid == 1) { /* todo (c->partition_type == 0) */

    /**********************************/
    /** Load the global SymbolMatrix **/
    /**********************************/
    
    sprintf(filename, "%s_%d.symbol", sfile_path, c->domsize);
    /* fprintfd(stderr, "Proc %d: Load SymbolMatrix from file %s ...\n", proc_id, filename); */
    fprintferr(stderr, "Load SymbolMatrix from file %s ...\n", filename);
    fp = fopen(filename, "r");
    if(fp == NULL)
      { 
	/* fprintfv(5, stderr, "Proc %d ERROR: Unable to open file %s \n", proc_id, filename); */
	fprintferr(stderr, "ERROR: Unable to open file %s \n", filename);
	return HIPS_ERR_IO;
      }
    
    c->symbmtx = (SymbolMatrix*)malloc(sizeof(SymbolMatrix));
    PHIDAL_LoadSymbolMatrix(fp, c->symbmtx);

    fclose(fp);

  }
  
  /* */
  c->state = HIERARCH;
  /* HIPS_ParallelSetup(id); */
  /* */

  fprintfd(stderr, "done \n");

  return HIPS_SUCCESS;
}

INTS HIPS_LocalMatriceLoad(INTS id, INTS *symmetric, INTS* ln, INTL **lia, INTS **lja, COEF **la, char *sfile_path)
{
  HIPS_Context *c;
  PhidalOptions *option;
  dim_t domsize;
  c = context+id;
  option = &c->options;
  domsize = c->domsize;

  FILE* fp;
  char filename[BUFLEN];  

  /*** Load the local matrix ***/
  sprintf(filename, "%s_%d.mtx.%d-%d", sfile_path, domsize, c->proc_id, c->nproc);
  fp = fopen(filename, "r");
  if(fp == NULL) {
    fprintfd(stderr, "ERROR: Unable to open file %s\n", filename);
    return HIPS_ERR_IO;
  }
  
  fprintfd(stderr, "Load matrix (file %s)" , filename);
  CSR_Load(fp, symmetric, ln, lia, lja, la);
  fprintfd(stderr, "done \n");
  fclose(fp);
  return HIPS_SUCCESS;
}



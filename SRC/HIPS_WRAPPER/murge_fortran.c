#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <mpi.h>

#include "hips.h"
#include "murge.h"

/** Macro from Scotch **/

#define FORTRAN_NAME(nu,nl,pl,pc) \
void nu pl;                       \
void nl pl                        \
{ nu pc; }                        \
void nl##_ pl                     \
{ nu pc; }                        \
void nl##__ pl                    \
{ nu pc; }                        \
void nu pl

FORTRAN_NAME(MURGE_INITIALIZE,
             murge_initialize,
             (INTS *idnbr,  INTS *ierror),
             (idnbr,  ierror))
{
  *ierror = MURGE_Initialize(*idnbr);
}

FORTRAN_NAME(MURGE_SETDEFAULTOPTIONS,
             murge_setdefaultoptions,
             (INTS *id,  INTS *stratnum,  INTS *ierror),
             (id, stratnum,  ierror))
{
  *ierror = MURGE_SetDefaultOptions(*id, *stratnum);
}

FORTRAN_NAME(MURGE_SETOPTIONINT,
             murge_setoptionint,
             (INTS *id,  INTS *number,  INTS *value,  INTS *ierror),
             (id, number, value,  ierror))
{
  *ierror = MURGE_SetOptionINT(*id, *number, *value);
}

FORTRAN_NAME(MURGE_SETOPTIONREAL,
             murge_setoptionreal,
             (INTS *id,  INTS *number,  REAL *value,  INTS *ierror),
             (id, number, value,  ierror))
{
  *ierror = MURGE_SetOptionREAL(*id, *number, *value);
}

#ifdef MPI_SUCCESS
FORTRAN_NAME(MURGE_SETCOMMUNICATOR,
             murge_setcommunicator,
             (INTS *id,  MPI_Comm *mpicom,  INTS *ierror),
             (id, mpicom,  ierror))
{
  *ierror = MURGE_SetCommunicator(*id, *mpicom);
}
#endif /* MPI_SUCCESS */

FORTRAN_NAME(MURGE_GRAPHBEGIN,
             murge_graphbegin,
             (INTS *id,  INTS *N,  INTL *edgenbr,  INTS *ierror),
             (id, N, edgenbr,  ierror))
{
  *ierror = MURGE_GraphBegin(*id, *N, *edgenbr);
}

FORTRAN_NAME(MURGE_GRAPHEDGE,
             murge_graphedge,
             (INTS *id,  INTS *COL,  INTS *ROW,  INTS *ierror),
             (id, COL, ROW,  ierror))
{
  *ierror = MURGE_GraphEdge(*id, *COL, *ROW);
}

FORTRAN_NAME(MURGE_GRAPHEND,
             murge_graphend,
             (INTS *id,  INTS *ierror),
             (id,  ierror))
{
  *ierror = MURGE_GraphEnd(*id);
}

FORTRAN_NAME(MURGE_GRAPHGLOBALCSR,
             murge_graphglobalcsr,
             (INTS *id,  INTS *N,  INTL *rowptr,  INTS *COLS,  INTS *root,  INTS *ierror),
             (id, N, rowptr, COLS, root,  ierror))
{
  *ierror = MURGE_GraphGlobalCSR(*id, *N, rowptr, COLS, *root);
}

FORTRAN_NAME(MURGE_GRAPHGLOBALCSC,
             murge_graphglobalcsc,
             (INTS *id,  INTS *N,  INTL *colptr,  INTS *ROWS,  INTS *root,  INTS *ierror),
             (id, N, colptr, ROWS, root,  ierror))
{
  *ierror = MURGE_GraphGlobalCSC(*id, *N, colptr, ROWS, *root);
}

FORTRAN_NAME(MURGE_GRAPHGLOBALIJV,
             murge_graphglobalijv,
             (INTS *id,  INTS *N,  INTL *NNZ,  INTS *ROW,  INTS *COL,  INTS *root,  INTS *ierror),
             (id, N, NNZ, ROW, COL, root,  ierror))
{
  *ierror = MURGE_GraphGlobalIJV(*id, *N, *NNZ, ROW, COL, *root);
}

FORTRAN_NAME(MURGE_SAVE,
             murge_save,
             (INTS *id,  char* directory, INTS *strlen,  INTS *ierror),
             (id,  directory, strlen,  ierror))
{
  char * tmp = NULL;
  tmp = (char *) malloc ((*strlen+1)*sizeof(char));
  strncpy(tmp, directory, *strlen);
  tmp[*strlen] = '\0';
  *ierror = MURGE_Save(*id,  tmp);
}

FORTRAN_NAME(MURGE_LOAD,
             murge_load,
             (INTS *id,  char* directory, INTS *strlen,  INTS *ierror),
             (id,  directory, strlen,  ierror))
{
  char * tmp = NULL;
  tmp = (char *) malloc ((*strlen+1)*sizeof(char));
  strncpy(tmp, directory, *strlen);
  tmp[*strlen] = '\0';
  *ierror = MURGE_Load(*id,  tmp);
}

FORTRAN_NAME(MURGE_GETLOCALNODENBR,
             murge_getlocalnodenbr,
             (INTS *id,  INTS *nodenbr,  INTS *ierror),
             (id, nodenbr,  ierror))
{
  *ierror = MURGE_GetLocalNodeNbr(*id, nodenbr);
}

FORTRAN_NAME(MURGE_GETLOCALNODELIST,
             murge_getlocalnodelist,
             (INTS *id,  INTS *nodelist,  INTS *ierror),
             (id, nodelist,  ierror))
{
  *ierror = MURGE_GetLocalNodeList(*id, nodelist);
}

FORTRAN_NAME(MURGE_GETLOCALUNKNOWNNBR,
             murge_getlocalunknownnbr,
             (INTS *id,  INTS *unkownnbr,  INTS *ierror),
             (id, unkownnbr,  ierror))
{
  *ierror = MURGE_GetLocalUnknownNbr(*id, unkownnbr);
}

FORTRAN_NAME(MURGE_GETLOCALUNKNOWNLIST,
             murge_getlocalunknownlist,
             (INTS *id,  INTS *unkownlist,  INTS *ierror),
             (id, unkownlist,  ierror))
{
  *ierror = MURGE_GetLocalUnknownList(*id, unkownlist);
}

FORTRAN_NAME(MURGE_ASSEMBLYBEGIN,
             murge_assemblybegin,
             (INTS *id,  INTL *coefnbr,  INTS *op,  INTS *op2,  INTS *mode,  INTS *sym,  INTS *ierror),
             (id, coefnbr, op, op2, mode, sym,  ierror))
{
  *ierror = MURGE_AssemblyBegin(*id, *coefnbr, *op, *op2, *mode, *sym);
}

FORTRAN_NAME(MURGE_ASSEMBLYSETVALUE,
             murge_assemblysetvalue,
             (INTS *id,  INTS *ROW,  INTS *COL,  COEF *value,  INTS *ierror),
             (id, ROW, COL, value,  ierror))
{
  *ierror = MURGE_AssemblySetValue(*id, *ROW, *COL, *value);
}

FORTRAN_NAME(MURGE_ASSEMBLYSETNODEVALUES,
             murge_assemblysetnodevalues,
             (INTS *id,  INTS *ROW,  INTS *COL,  COEF *values,  INTS *ierror),
             (id, ROW, COL, values,  ierror))
{
  *ierror = MURGE_AssemblySetNodeValues(*id, *ROW, *COL, values);
}

FORTRAN_NAME(MURGE_ASSEMBLYSETBLOCKVALUES,
             murge_assemblysetblockvalues,
             (INTS *id,  INTS *nROW,  INTS *ROWlist,  INTS *nCOL, INTS *COLlist,  COEF *values,  INTS *ierror),
             (id, nROW, ROWlist, nCOL, COLlist, values,  ierror))
{
  *ierror = MURGE_AssemblySetBlockValues(*id, *nROW, ROWlist, *nCOL, COLlist, values);
}

FORTRAN_NAME(MURGE_ASSEMBLYEND,
             murge_assemblyend,
             (INTS *id,  INTS *ierror),
             (id,  ierror))
{
  *ierror = MURGE_AssemblyEnd(*id);
}

FORTRAN_NAME(MURGE_MATRIXRESET,
             murge_matrixreset,
             (INTS *id,  INTS *ierror),
             (id,  ierror))
{
  *ierror = MURGE_MatrixReset(*id);
}

FORTRAN_NAME(MURGE_MATRIXGLOBALCSR,
             murge_matrixglobalcsr,
             (INTS *id,  INTS *N,  INTL *rowptr,  INTS *COLS, COEF *values,  INTS *root,  INTS *op,  INTS *sym,  INTS *ierror),
             (id, N, rowptr, COLS, values, root, op, sym,  ierror))
{
  *ierror = MURGE_MatrixGlobalCSR(*id, *N, rowptr, COLS, values, *root, *op, *sym);
}

FORTRAN_NAME(MURGE_MATRIXGLOBALCSC,
             murge_matrixglobalcsc,
             (INTS *id,  INTS *N,  INTL *COLPTR,  INTS *ROWS, COEF *values,  INTS *root,  INTS *op,  INTS *sym,  INTS *ierror),
             (id, N, COLPTR, ROWS, values, root, op, sym,  ierror))
{
  *ierror = MURGE_MatrixGlobalCSC(*id, *N, COLPTR, ROWS, values, *root, *op, *sym);
}

FORTRAN_NAME(MURGE_MATRIXGLOBALIJV,
             murge_matrixglobalijv,
             (INTS *id,  INTS *N,  INTL *NNZ,  INTS *ROWS,  INTS *COLS, COEF *values,  INTS *root,  INTS *op,  INTS *sym,  INTS *ierror),
             (id, N, NNZ, ROWS, COLS, values, root, op, sym,  ierror))
{
  *ierror = MURGE_MatrixGlobalIJV(*id, *N, *NNZ, ROWS, COLS, values, *root, *op, *sym);
}

FORTRAN_NAME(MURGE_SETGLOBALRHS,
             murge_setglobalrhs,
             (INTS *id,  COEF *b,  INTS *root,  INTS *op,  INTS *ierror),
             (id, b, root, op,  ierror))
{
  *ierror = MURGE_SetGlobalRHS(*id, b, *root, *op);
}

FORTRAN_NAME(MURGE_SETLOCALRHS,
             murge_setlocalrhs,
             (INTS *id,  COEF *b,  INTS *op,  INTS *op2,  INTS *ierror),
             (id, b, op, op2,  ierror))
{
  *ierror = MURGE_SetLocalRHS(*id, b, *op, *op2);
}

FORTRAN_NAME(MURGE_SETRHS,
             murge_setrhs,
             (INTS *id,  INTS *n,  INTS *coefsidx,  COEF *b, INTS *op,  INTS *op2,  INTS *mode,  INTS *ierror),
             (id, n, coefsidx, b, op, op2, mode,  ierror))
{
  *ierror = MURGE_SetRHS(*id, *n, coefsidx, b, *op, *op2, *mode);
}

FORTRAN_NAME(MURGE_RHSRESET,
             murge_rhsreset,
             (INTS *id,  INTS *ierror),
             (id,  ierror))
{
  *ierror = MURGE_RHSReset(*id);
}

FORTRAN_NAME(MURGE_GETGLOBALSOLUTION,
             murge_getglobalsolution,
             (INTS *id,  COEF *x,  INTS *root,  INTS *ierror),
             (id, x, root,  ierror))
{
  *ierror = MURGE_GetGlobalSolution(*id, x, *root);
}

FORTRAN_NAME(MURGE_GETLOCALSOLUTION,
             murge_getlocalsolution,
             (INTS *id,  COEF *x,  INTS *ierror),
             (id, x,  ierror))
{
  *ierror = MURGE_GetLocalSolution(*id, x);
}

FORTRAN_NAME(MURGE_GETSOLUTION,
             murge_getsolution,
             (INTS *id,   INTS *n,  INTS *coefsidx,  COEF *x,  INTS *mode,  INTS *ierror),
             (id, n, coefsidx, x, mode,  ierror))
{
  *ierror = MURGE_GetSolution(*id, *n, coefsidx, x, *mode);
}

FORTRAN_NAME(MURGE_CLEAN,
             murge_clean,
             (INTS *id,  INTS *ierror),
             (id,  ierror))
{
  *ierror = MURGE_Clean(*id);
}

FORTRAN_NAME(MURGE_FINALIZE,
             murge_finalize,
             ( INTS *ierror),
             ( ierror))
{
  *ierror = MURGE_Finalize();
}

FORTRAN_NAME(MURGE_GETSOLVER,
             murge_getsolver,
             (INTS * solver,  INTS *ierror),
             ( solver,  ierror))
{
  *ierror = MURGE_GetSolver( solver);
}

FORTRAN_NAME(MURGE_GETINFOINT,
             murge_getinfoint,
             (INTS *id,   INTS *metric,  INTL * value,  INTS *ierror),
             (id, metric,  value,  ierror))
{
  *ierror = MURGE_GetInfoINT(*id, *metric,  value);
}

FORTRAN_NAME(MURGE_GETINFOREAL,
             murge_getinforeal,
             (INTS *id,   INTS *metric,  REAL * value,  INTS *ierror),
             (id, metric,  value,  ierror))
{
  *ierror = MURGE_GetInfoREAL(*id, *metric,  value);
}

FORTRAN_NAME(MURGE_PRINTERROR,
             murge_printerror,
             (INTS *error_number,  INTS *ierror),
             (error_number,  ierror))
{
  *ierror = MURGE_PrintError(*error_number);
}

FORTRAN_NAME(MURGE_EXITONERROR,
             murge_exitonerror,
             (INTS *error_number,  INTS *ierror),
             (error_number,  ierror))
{
  *ierror = MURGE_ExitOnError(*error_number);
}


/* @authors P. HENON */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>


#include "phidal_ordering.h"
#include "queue.h"

/*#define ORDER_DIRECT*/ /* To force setuplevels to upgrade node from the
			higher separator in priority ===> THIS IS NOT
			GOOD (TESTED) **/

#define UNCONSISTENT 2
#define UPGRADE      1

int tab_max(int *tab, int size);
void  setupblocks(int nlev, int *levelindex, dim_t *iperm, int *keyindex, int *key, 
		  PhidalHID *BL);

void sort_interior_nodes(int *nodelist, int nodenbr, int ndom, int *keyindex, int *key);

void computeNodeTraversalOrder(dim_t *perm, dim_t *iperm, int_t levelnum, int start_index, int ndom, int *keymask, int n, INTL *ia, dim_t *ja, int *keyindex, int *key, int *nodelist, int *nodenbr, int *mask);
void computeNodeGain(dim_t *iperm, int start_index, int_t levelnum, int *mask, int n, INTL *ia, dim_t *ja, int *keyindex, int *key, int *gain, int *gain2);
int upgradeNodeKey(flag_t job, int node, int_t levelnum, int ndom, int *key, int *keyindex, int *new_key, int *new_keyindex, INTL *ia, dim_t *ja, int *keymask, int *mask);
int is_consistentNodeKey(int node, int ndom, int *key, int *keyindex, INTL *ia, dim_t *ja, int *keymask, int *mask);



void setuplevels(int *mapp, int *mapptr, int overlap_flag, int ndom, int n,
		 INTL *ia, dim_t *ja, 
		 dim_t *perm, dim_t *iperm, PhidalHID *BL){
  /*--------------------------------------------------------------------------------
    / given a partition of the matrix into n subdomains
    / setuplevels compute a splitting of the matrix into 
    / levels based on the connectivity of nodes:
    / level(i) contains nodes that are connected to 
    / i differents subdomains.
    / each submatrix corresponding to the nodes of a level is 
    / ordered in a diagonal-block structure.
    / The matrix is permuted in place.
    /
    / ON ENTRY:
    / mapp, mapptr : description of the partition
    /                   mapp[ mapptr(i) : mapptr(i+1)-1] contains
    /                   node of the sudomain i
    / overlap_flag : if =0 there is no overlapping in the partition given
    /                if =1 the partition given has some overlapping between subdomains
    / ndom         : number of subdomains
    / n            : size of the matrix
    / ia, ja       : sparse pattern of the matrix (in csr format)
    / BL           : a pointer on a BL structure
    / 
    /
    / ON RETURN:
    / BL     : the block level structure containing the HID decomposition
    / iperm  : iperm[i] is the reverse permutation (iperm[perm[i]] == i)
    /
    / NOTE: FOR COHERENCE OF OVERLAP THE ADJACENCY GRAPH (ia, ja) MUST BE SYMMETRIC !!!
    /--------------------------------------------------------------------------------*/

  dim_t i, j, k;
  int index;
  int node;
  int *node2dom=NULL;
  int *tmp; 
  int *keylength;
  int *keyindex;
  int *key;
  int *new_key=NULL;
  int *new_keyindex;
  int *ptr1; 
  int *ptr2; 
  int *mask, *mask2;
  int *levindex;
  int_t levelnum;
  int start_index;
  int *nodelist;
  int nodenbr;

  /****************************************************************
  * Compute interface nodes and their keys
  * that is their adjacent subdomains list
  * here we have to consider  separatly the case with overlap and 
  * without overlapp
  *****************************************************************/
  if(overlap_flag == 0)
    {
      /*************************
       * WITHOUT OVERLAP       * 
       *************************/

      /* Compute array node2dom defined by node2dom[i] = label of the subdomain that owns node i*/
      node2dom = malloc(sizeof(int)*n);
      
#ifdef DEBUG_M
      memset(node2dom, -1, n);
#endif
      
      for(i=0;i<ndom;i++)
	{
	  for(j=mapptr[i];j<mapptr[i+1];j++)
	    node2dom[mapp[j]] = i;
	}
      
#ifdef DEBUG_M
      
  for(i=0;i<n;i++)
    assert(node2dom[i] >= 0);
#endif
      
      
      /*------------------------------------------------------------------*/
      /*  We compute for each node the list of adjacents subdomains       */
      /* The number of adjacent sudomains and this list gives a key       */ 
      /* that we will use to sort the node and prune the level structure  */
      /* (thank to the number of adj. subdomains) and the block structure */
      /* (thank to the list of adj. subdomains) of each levels            */
      /*------------------------------------------------------------------*/
      
      /* keylength[i] =  number of adjacents subdomains to node i (won't be true anymore after)*/
  tmp = (int *)malloc(sizeof(int)*ndom);
  keylength = (int *)malloc(sizeof(int)*n);
      for(i=0;i<n;i++)
	{
	  bzero(tmp, sizeof(int)*ndom);
	  for(j=ia[i];j<ia[i+1];j++)
	    tmp[ node2dom[ja[j]] ] = 1;
	  
	  keylength[i] = 0;
	  for(k=0;k<ndom;k++)
	    keylength[i] += tmp[k];
	  
#ifdef DEBUG_M
	  assert(keylength[i] >= 1);
#endif
	}  
      
      /** The list of adjacent subdomains for node i 
	is key[ keyindex[i] : keyindex[i+1]-1 ] 
	**/
      keyindex = malloc(sizeof(int)*(n+1));
      index = 0;
      for(i=0;i<n;i++)
	{
	  keyindex[i] = index;
	  index += keylength[i];
	}
      keyindex[n] = index;
      
      key = malloc(sizeof(int)*keyindex[n]);
      for(i=0;i<n;i++)
	{
	  bzero(tmp, sizeof(int)*ndom);
	  for(j=ia[i];j<ia[i+1];j++)
	    tmp[ node2dom[ja[j]] ] = 1;
	  
	  index=keyindex[i];
	  for(j=0;j<ndom;j++)
	    if(tmp[j] != 0)
	      {
		key[index] = j;
		index++;
	      }
#ifdef DEBUG_M
	  assert(index - keyindex[i] == keylength[i]);
#endif
	}
      free(tmp);
    }
  else
    {
      /**************************
       * INITIAL PARTITION GIVEN*
       * WITH    OVERLAP        * 
       **************************/ 
      /* keylength[i] =  number of subdomains in which is the node i (won't be true anymore after)*/
      keylength = malloc(sizeof(int)*n);
      bzero(keylength, sizeof(int)*n);
      
      for(i=0;i<ndom;i++)
	for(j=mapptr[i];j<mapptr[i+1];j++)
	  keylength[mapp[j]]++;



#ifdef DEBUG_M
      
      for(i=0;i<n;i++)
	/*assert(keylength[i] >= 1);*/
	if(keylength[i] <1)
	  fprintfd(stderr, "Node %ld keylength %ld \n", (long)i, (long)keylength[i]);
#endif
 
      /** The list of adjacent subdomains for node i 
	is key[ keyindex[i] : keyindex[i+1]-1 ] 
	**/
      keyindex = (int *)malloc(sizeof(int)*(n+1));
      index = 0;
      for(i=0;i<n;i++)
	{
	  keyindex[i] = index;
	  index += keylength[i];
	}
      keyindex[n] = index;
      
      key = (int *)malloc(sizeof(int)*keyindex[n]);
      tmp = (int *)malloc(sizeof(int)*n); /* here we use tmp as an index in the key for each node */
      bzero(tmp, sizeof(int)*n);
      
      for(i=0;i<ndom;i++)
	for(j=mapptr[i];j<mapptr[i+1];j++)
	  {
	    node = mapp[j];
	    key[ keyindex[node]+tmp[node] ] = i;
	    tmp[node]++;
	  }

#ifdef DEBUG_M
      for(i=0;i<n;i++)
	assert(tmp[i] == (keyindex[i+1] - keyindex[i]));
#endif

      free(tmp);
    }

#ifdef DEBUG_M
  for(i=0;i<ndom;i++)
    for(j=mapptr[i];j<mapptr[i+1];j++)
      {
	if(keylength[mapp[j]] == 1)
	  {
	    dim_t w;
	    for(w = ia[mapp[j]]; w < ia[mapp[j]+1];w++)
	      if(! is_in_key(key[keyindex[mapp[j]]], ja[w], keyindex, key) )
		{
		  dim_t q;
		  fprintfd(stdout , " node %d in dom %d has a neigh %d keylength %d \n", 
			  mapp[j],i, ja[w] , keylength[ja[w]]);
		  for(q = keyindex[ja[w]]; q < keyindex[ja[w]+1];q++)
		    fprintfd(stdout, "%d ", key[q]);
		  fprintfd(stdout, "\n");
		}
	  }
      }
  
#endif

  /** Now we separe interior node from interface node **/
  index = 0;
  
#define PRESERVE_ORDERING
#ifndef PRESERVE_ORDERING
  /*OIMBE in this version the ordering of the interior node was not preserve */
  for(i=0;i<n;i++)  
    if(keyindex[i+1] - keyindex[i] == 1)
      {
	iperm[index] = i; 
	index++;
	}
#else
  /* OIMBE: this version keeps the initial ordering of the interior node */
  for(i=0;i<mapptr[ndom];i++)
    {
      node = mapp[i];
      if(keyindex[node+1] - keyindex[node] == 1)
	{
	  iperm[index] = node; 
	  index++;
	}
    }
#endif



  /** Now we store the interface nodes**/
  start_index = index;
  
  for(i=0;i<n;i++)
    if(keyindex[i+1] - keyindex[i] > 1)
      {
	iperm[index] = i;
	index++;
      }

  mask = (int *)malloc(sizeof(int)*n);
  nodelist = (int *)malloc(sizeof(int)*n);
  mask2 = (int *)malloc(sizeof(int)*n); 
  levelnum = 1;

  new_keyindex = (int *)malloc(sizeof(int)*(n+1));
  tmp = (int *)malloc(sizeof(int)*ndom);


  while(start_index < n)
    {
      int node1;

      /** Compute the list (and traversal order) of node in this level which key has to be changed **/
      computeNodeTraversalOrder(perm, iperm, levelnum, start_index, ndom, tmp, n, ia, ja, keyindex, key, nodelist, &nodenbr, mask);   
      
      /*----------------------/
      /  COMPUTE NEW_KEYINDEX /
      /----------------------*/
      
      /** First compute the new_keylength **/
      bzero(mask2, sizeof(int)*n);
      for(i=0;i<nodenbr;i++)
	{
	  node = nodelist[i];
#ifdef DEBUG_M
	  assert(keylength[node] == keyindex[node+1]-keyindex[node]);
	  assert(levelnum+1 == keylength[node]);
#endif
	  keylength[node] = upgradeNodeKey(mask[node], node, levelnum, ndom, key, keyindex, new_key, new_keyindex, ia, ja, tmp, mask2);
	  mask2[node] = 1;
	}

      /** Compute the length of new_keyindex **/
      index = 0;
      for(i=0;i<n;i++)
	{
	  new_keyindex[i] = index;
	  index += keylength[i];
	}
      new_keyindex[n] = index;

      /*--------------------------------------------------------/
      / FILL NEW_KEY FOR NODES THAT HAVE NOT CHANGED THEIR KEY  /
      /--------------------------------------------------------*/
      new_key = (int *)malloc(sizeof(int)* new_keyindex[n] );
      for(i=0;i<n;i++)
	if(mask[i] == 0)
	  {
	    memcpy(new_key+new_keyindex[i], key+keyindex[i], sizeof(int)*keylength[i]);
#ifdef DEBUG_M
	    assert(new_keyindex[i+1] - new_keyindex[i] == keyindex[i+1]-keyindex[i]);
	    assert(keyindex[i+1]-keyindex[i] == keylength[i]);
#endif
	  }
	  


      /*--------------------------------------------------------/
      / COMPUTE NEW_KEY FOR NODES THAT HAVE CHANGED THEIR KEY   /
      /--------------------------------------------------------*/
      bzero(mask2, sizeof(int)*n);
      for(i=0;i<nodenbr;i++)
	{
	  node1 = nodelist[i];
#ifdef DEBUG_M
	  assert(keyindex[node1+1]-keyindex[node1] == levelnum +1);
#endif
	  
	  upgradeNodeKey(mask[node1], node1, levelnum, ndom, key, keyindex, new_key, new_keyindex, ia, ja, tmp, mask2);
	  /*** Mark that this node has changed its key **/
	  mask2[node1] = 1;


	  /*** Fill new_key ***/
	  index = new_keyindex[node1];
	  for(j=0;j<ndom;j++)
	    if(tmp[j] != 0)
	      {
		new_key[index] = j;
		index++;
	      }
#ifdef DEBUG_M
	  assert(index == new_keyindex[node1+1]);
#endif
	}
      

      /** Store in iperm the node of this level and move start_index **/
      index = start_index;
      for(i = start_index; i<n ; i++)
	{
	  /*fprintfd(stderr, "i %d iperm[i] %d n %d \n", i, iperm[i], n);*/
	  node1 = iperm[i];
	  if(keylength[node1] == levelnum+1)
	    {
#ifdef DEBUG_M
	      assert(mask[node1] == 0);
#endif
	      iperm[index] = node1;
	      index++;
	    }
	}
      /** move start_index **/
      start_index = index;

      /** Store in iperm from start_index the nodes such that level < keylength(node)) **/
      for(i = 0;i<n;i++)
	if(keylength[i] > levelnum+1)
	  {
	    iperm[index] = i;
	    index++;
	  }
#ifdef DEBUG_M
      if(index != n)
	{
	  fprintfd(stderr, "index %d n %d \n", index, n);
	  assert(n == index);
	}
#endif


      /*** Swap key, keyindex and newkey new_keyindex ***/ 
      ptr1 = key;
      key = new_key;
      free(ptr1);
      ptr2 = keyindex;
      keyindex = new_keyindex;
      new_keyindex = ptr2;

      /** Next level **/
      levelnum++;
    }

  free(new_keyindex);
  free(nodelist);

  /** Set up the levels index in iperm
      **/
#ifdef DEBUG_M
  assert(levelnum ==  tab_max(keylength, n));
#endif
  levelnum = tab_max(keylength, n);

  levindex = (int *)malloc(sizeof(int)*( levelnum + 1));
  
  index = 0;
  for(i=0;i<levelnum;i++)
    {
      levindex[i] = index;
      while( index <n && keylength[iperm[index]] == i+1)
	index++;
    }
  levindex[levelnum] = n;
#ifdef DEBUG_M
  assert (index == n);
#endif

      
  /** Second Phase of the ordering
      Inside each level we order the 
      node according to their key:
      for node i the key is composed 
      by the list of its adjacent domains 
      sorted in ascending order.
      If we denote by f the first label such that
      key1[f] != key2[f] then
      key1 <= key2  iff  key1[f] <= key2[f]
      **/
#ifdef DEBUG_M
  assert(checkHID(levelnum, levindex, iperm, keyindex, key, n, ia, ja) == 0);
#endif
 
  /** It is more efficient to avoid quicksort for the level 0 (interior node) 
    and quicksort may destroy the order of node that minimize fill-in **/
  sort_interior_nodes(iperm, levindex[1], ndom, keyindex, key);

  for(i=1;i<levelnum;i++)
    {
      /*fprintfd(stdout, "Reordering level %d = %d nodes \n", i, levindex[i+1]-levindex[i] );*/
      quicksort_node(iperm, levindex[i], levindex[i+1]-1, i+1, keyindex, key);
      /*fprintfd(stdout, "Quicksort in %d calls\n", j);*/
    }
  
#ifdef DEBUG_M
  assert(checkHID(levelnum, levindex, iperm, keyindex, key, n, ia, ja) == 0);
#endif





  /*-----------------------------------------------------------------------*
  |             get block (and level block) structure of the matrix         |
  *------------------------------------------------------------------------*/
  /*fprintfd(stdout, "Compute the block structure \n");*/

  PhidalHID_Init(BL);
  BL->n = n;
  BL->ndom = ndom;
  BL->locndom = ndom; /** In parallel it is overwritten in
			  PHIDAL_SetupLocal.. **/
  setupblocks(levelnum, levindex, iperm, 
	      keyindex, key, BL);
	      /*nblock, block_index, block_levelindex);*/
  /*fprintfd(stdout, "Done \n");*/


  /*-----------------------------------------------------------------------*
  |             compute the key of each block of the matrix                |
  *------------------------------------------------------------------------*/
  /** The key of a block is the same as the nodes that compose the block **/

  BL->block_keyindex  = (int *)malloc(sizeof(int)*(BL->nblock+1));
  index = 0;
  for(i=0;i<BL->nblock;i++)
    {
      BL->block_keyindex[i] = index;
      /* get the index of the first node of the block */
      node = iperm[ BL->block_index[i] ];

      index += keyindex[node+1] - keyindex[node];
    } 
  BL->block_keyindex[BL->nblock] = index;

#ifdef DEBUG_M
  for(i=0;i<levelnum;i++)
    {
      for(j=BL->block_levelindex[i];j<BL->block_levelindex[i+1];j++)
	if ( BL->block_keyindex[j+1] - BL->block_keyindex[j] != i+1)
	  {
	    fprintfd(stderr, "Block %ld keylength %ld but level %ld \n", (long)j, 
		     (long)(BL->block_keyindex[j+1] - BL->block_keyindex[j]), (long)i);
	    exit(-1);
	  }
    }
#endif
  
  BL->block_key = (int *)malloc(sizeof(int)*index);
  for(i=0;i<BL->nblock;i++)
    {
      /* get the index of the first node of the block */
      node = iperm[ BL->block_index[i] ];

      /* The key of this node is the key of the block */
      for(j= 0; j < BL->block_keyindex[i+1] - BL->block_keyindex[i];j++)
	BL->block_key[BL->block_keyindex[i] +j] = key[ keyindex[node] + j];
    } 

  
  
  /*** Set nlev ***/
  BL->nlevel = levelnum;
      

  /*****************************/
  /** Compute the perm vector **/
  /*****************************/
  for(i=0;i<n;i++)
    perm[iperm[i]] = i; 



  /** Free allocate memory **/
  free(tmp);
  free(mask);
  free(mask2);
  if(overlap_flag == 0)
    free(node2dom);
  free(keylength);
  free(keyindex);
  free(key);
  free(levindex);

 

      
}  


void  setupblocks(int nlev, int *levelindex, dim_t *iperm, int *keyindex, int *key, PhidalHID *BL)
{
  dim_t i;
  int index, current;
  int nblk;
  /** First we count the number of blocks **/
  index = 0;
  nblk = 0;

  
  for(i=0;i<nlev;i++)
    {
      index = levelindex[i];
      current = index;
      while(current < levelindex[i+1])
	{

	  while( index < levelindex[i+1] &&
		compare_node(iperm[index], iperm[current], i+1, keyindex, key) == 0)
	    index++;
	  current = index;
	  nblk++;
	}
    }

  
  /** Now we allocate the block_index vector and fill it **/
  
  BL->block_index =  (int *)malloc(sizeof(int)*(nblk+1));
  BL->nblock = nblk;
  BL->block_levelindex = (int *) malloc(sizeof(int)*(nlev+1));

  index = 0;
  nblk = 0;
  for(i=0;i<nlev;i++)
    {
      index = levelindex[i];
      current = index;
      
      BL->block_levelindex[i] = nblk;

      BL->block_index[0] = 0;
      while(current < levelindex[i+1])
	{
	  while( index < levelindex[i+1] &&
		compare_node(iperm[index], iperm[current], i+1, keyindex, key) == 0)
	    index++;
	
	  current = index;
	  nblk++;
	  BL->block_index[nblk] = index;
	}
    }
  BL->block_levelindex[nlev] = nblk;

#ifdef DEBUG_M
  assert(BL->block_index[nblk] == levelindex[nlev]);
#endif
}



int tab_max(int *tab, int size)
{
  /*---------------------------
  / return the maximum value of a vector of int
  /
  / ON ENTRY:
  / tab: vector of int
  / size: size of the vector
  /
  / ON RETURN:
  / int = maximum value found
  /---------------------------*/

  dim_t i;
  int max;
  max = tab[0];
  for(i=1;i<size;i++)
    {
      if(tab[i] > max)
	max = tab[i];
    }
  return max;
}


void sort_interior_nodes(int *nodelist, int nodenbr, int ndom, int *keyindex, int *key)
{
  dim_t i;
  int ind;
  int *tmp;
  int *indextab;
  int *start_indextab;

  /** Copy nodelist in tmp **/
  tmp = (int *)malloc(sizeof(int)*nodenbr);
  memcpy(tmp, nodelist, nodenbr * sizeof(int));

  /** Allocate a vector to contain indices **/
  indextab = (int *)malloc(sizeof(int)*ndom);

  bzero(indextab, sizeof(int)*ndom);
  /** Compute the number of interior nodes in each domain **/
  for(i=0;i<nodenbr;i++)
    indextab[ key[keyindex[tmp[i]]] ]++;


  /** Allocate a vector to contain the start indice of each subdomain node list  **/
  start_indextab = (int *)malloc(sizeof(int)*ndom);
  ind = 0;
  for(i=0;i<ndom;i++)
    {
      start_indextab[i] = ind;
      ind += indextab[i];
    }

  /** Sorted nodelist by domain **/
  for(i=0;i<nodenbr;i++)
    nodelist[ start_indextab[key[keyindex[tmp[i]]]]++ ] = tmp[i];


  free(tmp);
  free(indextab);
  free(start_indextab);
}

void computeNodeTraversalOrder(dim_t *perm, dim_t *iperm, int_t levelnum, int start_index, int ndom, int *keymask, int n, INTL *ia, dim_t *ja, int *keyindex, int *key, int *nodelist, int *nodenbr, int *mask)
{
  /*-----------------------------------------------------------------------------------------------/
  / This function compute the list of node in a given level which key has to be changed  to        /
  / comply with HID. The node wich key has to be changed are ordered to minimize the number of     /
  / levels that HID will produce                                                                   /
  / On entry:                                                                                      /
  /   iperm       : global list of node                                                            /
  /  levelnum     : function will deal with node of level levelnum                                 /
  / start_index   : search node of level levelnum in iperm[start_index:n]                          /
  /     n         : total number of nodes (matrix dimension)                                       /
  /  ia, ja       : adjacency graph (CSR format)                                                   /
  / keyindex, key : key(keyindex[i]:keyindex[i+1]-1) is the key of node i                          /
  /                                                                                                /
  / On return     :                                                                                /
  /  nodelist     : the list of node which key has to be recomputed                                /
  /  nodenbr      : the number of node in nodelist                                                 /
  /  mask         : mask[i] == TRUE if the key of node i has to be recomputed                      /
  /-----------------------------------------------------------------------------------------------*/
  
  dim_t i, j;
  int index;
  int node, neigh;
  int *gain, *gain2; /** gain[i] is the number of neighbors of node i whose key is different and in level levelnum**/
                     /** gain2[i] is the number of neighbors of node i whose key is less or equal to the key of node i**/
  int prev_gain;
  float val;
  Queue heap;

  gain = (int *)malloc(sizeof(int)*n);
  gain2 = (int *)malloc(sizeof(int)*n);
  bzero(gain, sizeof(int)*n);
  bzero(gain2, sizeof(int)*n);
  bzero(mask, sizeof(int)*n);

  /** We use mask to mark the nodes in level levelnum that have not a consistent key 
      (i.e. consistent == that contains any adjacent node key of a smaller size) **/
  index = 0;
  j = 0;
  for(i=start_index;i<n;i++)
    {
      node = iperm[i];
      if(keyindex[node+1]-keyindex[node] == levelnum +1)
	{
	  if(is_consistentNodeKey(node, ndom, key, keyindex, ia, ja, keymask, mask) == 0)
	    {
	      mask[node] = UNCONSISTENT; /** Mark this node as unconsistent **/
	      nodelist[index] = node;
	      index++;
	    }
	  else
	    j++;
	}
    }
  
  /*fprintfd(stderr, "LEVEL %d number of unconsistent %d upgrade %d \n", levelnum, index, j);*/
  
  queueInit(&heap, j+1);
  
  /** Compute gain array **/
  computeNodeGain(iperm, start_index, levelnum, mask, n, ia, ja, keyindex, key, gain, gain2);
  
  /** Put the node in the heap ordered by ascending gain **/
  for(i=start_index;i<n;i++)
    {
      node = iperm[i];
      if(gain[node] > 0)
#ifdef ORDER_DIRECT
	queueAdd2(&heap, node, -perm[node], -gain[node]);
#else
	queueAdd2(&heap, node, -gain[node], gain2[node]);
#endif
    }
      
  /*index = 0;*/
  while(queueSize(&heap)>0)
    {

#ifdef ORDER_DIRECT
      queueGet3(&heap, &node, &prev_gain);
#else
      queueGet2(&heap, &node, &val);
      prev_gain = (int )val;
#endif

#ifdef DEBUG_M
      assert(mask[node] != UNCONSISTENT);
#endif

      prev_gain = -prev_gain;

      if(gain[node] == 0 || mask[node] != 0) /** This node do not need to change its key anymore (all its neigthbors have changed their key)**/
	continue;

      if(prev_gain != gain[node])
	{
	  /** The gain of this node has changed since it has been entered in the heap **/
	  if(gain[node] > 0)
#ifdef ORDER_DIRECT
	    queueAdd2(&heap, node, -perm[node], -gain[node]);
#else
	    queueAdd2(&heap, node, -gain[node], gain2[node]);
#endif
	}
      else
	{
	  /*** Add this node to the list ***/
	  nodelist[index] = node;
	  index++;
	  mask[node] = UPGRADE;
	  
	  /** Update the gain of its neighbors **/
	  for(j=ia[node];j<ia[node+1];j++)
	    {
	      neigh = ja[j];
	      if(mask[neigh] == 0 && gain[neigh] > 0 &&  is_equal_key(key + keyindex[node], levelnum+1, key + keyindex[neigh], levelnum+1) != 1)
		gain[neigh]--;
	    }
	}
    }
  
  (*nodenbr) = index;

  /** Update mask **/
  /*bzero(mask, sizeof(int)*n);
  for(i=0;i<index;i++)
  mask[nodelist[i]] = 1;*/

#ifdef DEBUG_M
  for(i=0;i<index;i++)
    assert(keyindex[nodelist[i]+1]-keyindex[nodelist[i]] == levelnum+1);
#endif

 
  queueExit(&heap);
  free(gain);
  free(gain2);
}




void computeNodeGain(dim_t *iperm, int start_index, int_t levelnum, int *mask, int n, INTL *ia, dim_t *ja, int *keyindex, int *key, int *gain, int *gain2)
{
  dim_t i, j;
  int index, index2;
  int node, neigh; 
  for(i=start_index;i<n;i++)
    {
      node = iperm[i];
      if(keyindex[node+1]-keyindex[node] == levelnum+1 && mask[node] != UNCONSISTENT)
	{
	  index = 0;
	  index2 = 0;
	  for(j=ia[node];j<ia[node+1];j++)
	    {
	      neigh = ja[j];
	      if(mask[neigh] != UNCONSISTENT && keyindex[neigh+1]-keyindex[neigh] == levelnum+1)
		{
		  if(is_equal_key(key + keyindex[node], levelnum+1, key + keyindex[neigh], levelnum+1) != 1)
		    index++;
		  else
		    index2++;
		}
	    }
	  gain[node] = index;
	  gain2[node] = index2;
	}
    }
  
  
}



int upgradeNodeKey(flag_t job, int node, int_t levelnum, int ndom, int *key, int *keyindex, int *new_key, int *new_keyindex, INTL *ia, dim_t *ja, int *keymask, int *mask)
{
  /*****************************************************/
  /* This function computes a new key for a node:      */
  /* If job == 0                                       */
  /* the new key is the union of the node current key  */
  /* with the key of all its neighbors that have a     */
  /* shorter key (job = 0)                             */
  /* If job == 1                                       */
  /* union = adjacent different node key of same size  */
  /* and those that have a                             */
  /* keymask[i]==1 means domain number i is in the new */
  /* key                                               */
  /* This function returns the length of the new key   */
  /*****************************************************/
  

  int j,k, node2;
  int length;

#ifdef DEBUG_M
  assert(keyindex[node+1]-keyindex[node] == levelnum + 1);
  assert(mask[node] == 0);
#endif


  /** Initialize the keymask **/
  bzero(keymask, sizeof(int)*ndom);



  length = 0;
  if(job == 0) /*** Make a consistent key ***/
    for(j=ia[node];j<ia[node+1];j++)
      {
	node2 = ja[j];
	if(mask[node2] == 0)
	  if(keyindex[node2+1]-keyindex[node2] < levelnum+1)
	    for(k=keyindex[node2];k<keyindex[node2+1];k++)
	      if(keymask[key[k]] == 0)
		{
		  keymask[key[k]] = 1;
		  length++;
		}
      }
  else /*** Upgrade key ****/
    for(j=ia[node];j<ia[node+1];j++)
      {
	node2 = ja[j];
	if(mask[node2] == 0)
	  if(keyindex[node2+1]-keyindex[node2] <= levelnum+1)
	    for(k=keyindex[node2];k<keyindex[node2+1];k++)
	      if(keymask[key[k]] == 0)
		{
		  keymask[key[k]] = 1;
		  length++;
		}
      }

  /** Merge with the node key **/
  for(k=keyindex[node];k<keyindex[node+1];k++)
    if(keymask[key[k]] == 0)
      {
	keymask[key[k]] = 1;
	length++;
      }
  
  


#ifdef DEBUG_M
  if(length <= levelnum+1)
    {
      fprintfd(stderr, "JOB %d Length %d Levelnum + 1 %d \n", job, length, levelnum+1);
      fprintfd(stderr, "Node %d key : \n", node);
      for(k=keyindex[node];k<keyindex[node+1];k++)
	fprintfd(stderr, "%d ", key[k]);
      fprintfd(stderr, "\n");

      for(j=ia[node];j<ia[node+1];j++)
	{
	  node2 = ja[j];
	  if(mask[node2] == 0)
	    if(keyindex[node2+1]-keyindex[node2] <= levelnum+1)
	      {
		fprintfd(stderr, "Neighbor %ld key : \n", (long)node2);
		for(k=keyindex[node2];k<keyindex[node2+1];k++)
		  fprintfd(stderr, "%ld ", (long)key[k]);
		fprintfd(stderr, "\n");
	      }
	}
    }

  assert(length> levelnum+1);
#endif

  return length;

}



int is_consistentNodeKey(int node, int ndom, int *key, int *keyindex, INTL *ia, dim_t *ja, int *keymask, int *mask)
{
  /*****************************************************/
  /* This function check is a node key is consistent:  */
  /* a key is consistent if any adjacent node key      */
  /* whose length is smaller than the node key length  */
  /* is included in the key node                       */
  /*****************************************************/
  

  int j,k, node2;
  int length;


  bzero(keymask, sizeof(int)*ndom);
  for(k=keyindex[node];k<keyindex[node+1];k++)
    keymask[key[k]] = 1;

  length = keyindex[node+1]-keyindex[node];
  for(j=ia[node];j<ia[node+1];j++)
    {
      node2 = ja[j];
      if(node2 == node)
	continue;
      
      if(mask[node2] == 0)
	if(keyindex[node2+1]-keyindex[node2] < length)
	  for(k=keyindex[node2];k<keyindex[node2+1];k++)
	    if(keymask[key[k]] == 0)
	      return 0; /** The key is not consistent **/
    }


  return 1; /** The key is consistent **/
}

/* @authors P. HENON */

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>

#include "phidal_ordering.h"

void PHIDAL_CsrDelDiag(int numflag, int n, INTL *ia, dim_t *ja)
{
  /**********************************************************************/
  /** This function delete the diagonal entries in a CSR or CSC matrix **/
  /** In place algorithm                                               **/
  /**********************************************************************/

  int ind;
  dim_t i, j, s;
  
#ifdef DEBUG_M
  assert(numflag == 0 || numflag == 1);
  assert(ia[0] == numflag);
#endif
  
  ind = 0;
  for(i=0;i<n;i++)
    {
      s = ind;
      for(j=ia[i];j< ia[i+1];j++)
	if(ja[j-numflag] != i+numflag) 
	  ja[ind++] =ja[j-numflag];

      ia[i] = s+numflag;
    }
  
  ia[ n ] = ind+numflag;
}

/* @authors P. HENON */

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
/*#include <unistd.h> *//*write*/

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "queue.h"
#include "phidal_ordering.h"

#include "scotch_metis_wrapper.h"


/** Probleme de linkage en COMPLEX intel : 
duplication des fonctions writeVecInt et readVecInt qui sont dans PHIDAL/COMMON/dumpcsr.c **/
void writeVecInt2(FILE *fp, dim_t *vec, dim_t n)
{
  dim_t i;
  for(i=0;i<n;i++)
    fprintf(fp, "%ld ", (long)(vec[i]));
  fprintf(fp, "\n");
}

void readVecInt2(FILE *fp, dim_t *vec, dim_t n)
{
  dim_t i;
  for(i=0;i<n;i++)
    FSCANF(fp, _ints_ , vec+i);
}


INTS mysave(dim_t *data, dim_t n, char* file) {
  FILE *fd;
  /*#warning open remplace par fopen et write par writeVecInt*/
  /*fd = open(file, O_WRONLY | O_CREAT, S_IRWXU);*/
  fd = fopen(file, "rw");
  fprintfd(stderr, "save %d\n", n);
  /*write(fd, data, sizeof(int)*n); */
  writeVecInt2(fd, data, n);
  fclose(fd);

  return HIPS_SUCCESS;
}


INTS myload(dim_t *data, dim_t n, char* file) {
  FILE *fd;
  /*#warning open remplace par fopen*/
  /*  fd = open(file, O_RDONLY | O_CREAT);*/
  fd = fopen(file, "r");
  /*read(fd, data, sizeof(int)*n); */
  readVecInt2(fd, data, n); 
  fclose(fd);

  return HIPS_SUCCESS;
}


void PHIDAL_Perm2SizedDomains(dim_t domsize, dim_t n, INTL *ia, dim_t *ja, dim_t *ndom, dim_t **mapptr,  dim_t **mapp, dim_t *perm, dim_t *iperm)
{
  /******************************************************************************************/
  /* This function computes a decomposition of the matrix graph into overlapped subdomain   */
  /*                  using a permutation vector of the unknowns. The permutation vector    */
  /*       is produced by a reordering method based on a Nested Dissection technique        */
  /* by METIS or SCOTCH library                                                             */
  /* The function decomposes the graph in subgraph of at most domsize nodes.                */
  /* the resulting number of domain is returned in ndom                                     */
  /* This routine is very useful when ones want to produce a domain decomposition of the    */
  /* matrix graph such that the fill-in is minimized and the overlap  between  subdomains   */
  /* is thin (the overlapp will roughly consists of the separator used in the Nested        */
  /* Dissection.                                                                            */
  /* On entry:                                                                              */
  /*     domsize : the maximal domain size admissible                                       */
  /*     n, ia, ja : the matrix graph (symmetric graph !)                                   */
  /*                                                                                        */
  /* On return:                                                                             */
  /*     perm, iperm : the ordering vectors produced by METIS or SCOTCH                     */
  /*     ndom : number of domain obtained                                                   */
  /*   mapptr, mapp: pointer to the graph decomposition                                     */
  /*                                                                                        */
  /* NOTE: mapptr and mapp are allocated inside this function                               */
  /*       ndom is a pointer since the number of domain obtained can be different than      */
  /*       the one desired in some rare cases (a warning is displayed in this cased)        */
  /******************************************************************************************/        


  dim_t cblknbr;
  dim_t *rangtab; /* Size n+1 */
  dim_t *treetab;
  INTL ind, i, j;
  dim_t domind;
  dim_t *cblk2dom;
  



  rangtab = (dim_t *)malloc(sizeof(dim_t)*(n+1));
  treetab = (dim_t *)malloc(sizeof(dim_t)*(n+1));

#ifndef SCOTCH_PART
{
  int option[10];
  flag_t numflag = 0;
  option[0] = 0;
  
  /**** Compute a matrix reordering that minimizes fill-in *******/
  /** NOTE: METIS inverse the usual definition of perm and iperm
      (compared to SCOTCH or PHIDAL) **/
  /* metis_NodeND(&n, ia, ja, &numflag, option, iperm, perm);*/
  METIS_NodeND_WRAPPER(n, ia, ja, numflag, option, iperm, perm);

  find_supernodes(n, n, ia, ja, perm, iperm, &cblknbr, rangtab, treetab);
}
#else
 {
   SCOTCH_Graph        grafdat;
   SCOTCH_Strat        stratdat;
   char STRAT[1000];
   
   
   
   
   /** This strategy includes the ordering of the leaves i.e. the interior nodes **/

   /* BUG SCOTCH SI DOMSIZE TROP PETIT exemple: BCSSTK16 domsize = 30

   if(domsize < 120) 
     sprintf( STRAT, "n{sep=/(vert>%d)?m{vert=100,low=h{pass=10},asc=f{bal=0.2}};,ole=f{cmin=0,cmax=10000,frat=0.0},ose=g}", domsize);
     else*/
   /*sprintf(STRAT, "n{sep=/(vert>GRAPH_LIM)?m{vert=100,low=h{pass=10},asc=f{bal=0.2}};,ole=f{cmin=0,cmax=10000,frat=0.0},ose=g}"); */

   /*sprintf(STRAT, "c{rat=0.7,cpr=n{sep=/(vert>120)?m{type=h,rat=0.7,vert=100,low=h{pass=10},asc=b{width=3,bnd=f{bal=0.2},org=f{bal=0.2}}}|m{type=h,rat=0.7,vert=100,low=h{pass=10},asc=b{width=3,bnd=f{bal=0.2},org=f{bal=0.2}}};,ole=f{cmin=0,cmax=100000,frat=0.0},ose=g},unc=n{sep=/(vert>120)?m{type=h,rat=0.7,vert=100,low=h{pass=10},asc=b{width=3,bnd=f{bal=0.2},org=f{bal=0.2}}}|m{type=h,rat=0.7,vert=100,low=h{pass=10},asc=b{width=3,bnd=f{bal=0.2},org=f{bal=0.2}}};,ole=f{cmin=15,cmax=100000,frat=0.0},ose=g}}");*/
   
   

   /** DEFAULT STRAT THAT WAS IN HIPS **/
   sprintf(STRAT,
     "c{rat=0.7,cpr=n{sep=/(vert>240)?m{type=h,rat=0.7,vert=100,low=h{pass=10},asc=b{width=3,bnd=f{bal=0.2},org=f{bal=0.2}}}|m{type=h,rat=0.7,vert=100,low=h{pass=10},asc=b{width=3,bnd=f{bal=0.2},org=f{bal=0.2}}};,ole=f{cmin=0,cmax=100000,frat=0.0},ose=g},unc=n{sep=/(vert>240)?m{type=h,rat=0.7,vert=100,low=h{pass=10},asc=b{width=3,bnd=f{bal=0.2},org=f{bal=0.2}}}|m{type=h,rat=0.7,vert=100,low=h{pass=10},asc=b{width=3,bnd=f{bal=0.2},org=f{bal=0.2}}};,ole=f{cmin=15,cmax=100000,frat=0.0},ose=g}}");
     
   /** PASTIX STAT **/
   /*sprintf(STRAT,
     "c{rat=0.7,cpr=n{sep=/(vert>120)?m{type=h,rat=0.8,vert=100,low=h{pass=10},asc=f{bal=0.2}}|m{type=h,rat=0.8,vert=100,low=h{pass=10},asc=f{bal=0.2}};,ole=f{cmin=0,cmax=100000,frat=0.0},ose=g},unc=n{sep=/(vert>120)?(m{type=h,rat=0.8,vert=100,low=h{pass=10},asc=f{bal=0.2}})|m{type=h,rat=0.8,vert=100,low=h{pass=10},asc=f{bal=0.2}};,ole=f{cmin=15,cmax=100000,frat=0.08},ose=g}}");*/
     

   SCOTCH_stratInit (&stratdat);
   SCOTCH_stratGraphOrder (&stratdat, STRAT);
   
   SCOTCH_graphInit  (&grafdat);
   /*SCOTCH_graphBuild (&grafdat, 0, n, ia, NULL, NULL, NULL, ia[n], ja, NULL);*/
   SCOTCH_graphBuild_WRAPPER (&grafdat, 0, n, ia, NULL, ja, NULL);
   
   /** Do not free verttab and edgetab before SCOTCH_graphOrder **/
    /*SCOTCH_graphOrder (&grafdat, &stratdat, perm, iperm, &cblknbr, rangtab, treetab);*/
    SCOTCH_graphOrder_WRAPPER (&grafdat, &stratdat, n, perm, iperm, &cblknbr, rangtab, treetab);
    
    SCOTCH_graphExit (&grafdat);
    SCOTCH_stratExit (&stratdat);

    find_supernodes(n, n, ia, ja, perm, iperm, &cblknbr, rangtab, treetab);
    
 }
 
#endif

/*for(i=0;i<=cblknbr;i++)
  fprintfv(5, stderr, "rangtab[%d] = %d \n", i, rangtab[i]);*/
 fprintf(stderr, "Find supernodes = %ld \n", (long)cblknbr);


 /********************************************************************************************/
 /*                                                                                          */
 /*                  FIND THE intERIOR SUBDOMAINS DEFINED BY THE NESTED DISSECTION           */
 /*                                                                                          */
 /********************************************************************************************/
 cblk2dom = (dim_t *)malloc(sizeof(dim_t)*cblknbr);
 if(get_interior_grid(domsize, cblknbr, rangtab, treetab, ndom, cblk2dom) != 0)
   {
     fprintfd(stderr, "Error in get_interior_grid \n");
     exit(-1);
   }
 
 /*if(get_interior_grid2(domsize, n, ia, ja, perm, iperm, cblknbr, rangtab, treetab, ndom, cblk2dom) != 0)
   {
   fprintfv(5, stderr, "Error in get_interior_grid2 \n");
   exit(-1);
   }*/

 /***********************************/
 /*** Construct mapptr            ***/
 /***********************************/
 (*mapptr)=(dim_t *)malloc(sizeof(dim_t)*((*ndom)+1));
  bzero((*mapptr), sizeof(dim_t)*((*ndom)+1));
  ind = 0;
  for(i=0;i<cblknbr;i++)
    {
      domind = cblk2dom[i];
      if(domind >= 0)
	{
	  (*mapptr)[domind+1] += rangtab[i+1]-rangtab[i];
	  ind += rangtab[i+1]-rangtab[i];
	}
    }


  for(i=1;i<=(*ndom);i++)
    (*mapptr)[i]+=(*mapptr)[i-1];

  

  /*** Construct mapp ***/
  (*mapp)=(dim_t *)malloc(sizeof(dim_t)*ind);
  for(i=0;i<cblknbr;i++)
    {
      domind = cblk2dom[i];
      if(domind >= 0)
	{
	  memcpy((*mapp)+(*mapptr)[domind], iperm+rangtab[i], sizeof(int)*(rangtab[i+1]-rangtab[i]));
	  (*mapptr)[domind] += rangtab[i+1]-rangtab[i];
#ifdef DEBUG_M
	  assert((*mapptr)[domind] <= (*mapptr)[domind+1]);
#endif
	}
    }

  /*** Reset mapptr ***/
  for(i=(*ndom);i>=1;i--)
    (*mapptr)[i] = (*mapptr)[i-1];
  (*mapptr)[0] = 0;

  /** Eliminate NULL domain **/
  j = 0;
  for(i=0;i<(*ndom);i++)
    if((*mapptr)[i+1]>(*mapptr)[i])
      (*mapptr)[j++] = (*mapptr)[i];
  (*mapptr)[j] = (*mapptr)[*ndom];
  *ndom = j;



#ifdef DEBUG_M
  assert((*mapptr)[(*ndom)] == ind);
#endif

  /*** Desallocations ***/
  free(cblk2dom);
  free(rangtab);
  free(treetab);

#ifdef DEBUG_M
#ifndef SCOTCH_PART /** @@@ OIMBE voir pourquoi ca marche pas en scotch **/
  /*** Check if any interior node of a subdomain is related to another subdomain interior node ***/
  fprintfd(stderr, "Check interior domains \n");
  check_interior_node((*ndom), (*mapptr), (*mapp), ia, ja, n);
  fprintfd(stderr, "Check interior domains DONE \n");
#endif
#endif

  /**** Expand domain to get overlap ****/
  get_overlap(*ndom, n, mapptr, mapp, ia, ja);

  /** Sort the node inside each subdomain according to their order in 
      the initial ordering made by the graph reorder (SCOTCH or METIS)
      **/
  /**** IT IS DONE AGAIN AT THE END OF HierarchDecomp *******/
  /**** We do it here only because by security to compute the new
  supernode partition in the interior domain:
  indeed some node in the interface are susceptible to become 
  interior node (due to function get_overlap); they have been 
  added at the end of the interior node set of each subdomain **/
  /*for(i=0;i<*ndom;i++)
    quicksort_int( (*mapp), (LONG)(*mapptr)[i], (LONG)((*mapptr)[i+1]-1), perm);  */
}




void PHIDAL_Perm2SizedDomains_SAVE(dim_t domsize, dim_t n, INTL *ia,dim_t *ja, dim_t *ndom, dim_t **mapptr,  dim_t **mapp, dim_t *perm, dim_t *iperm)
{
  /******************************************************************************************/
  /* This function computes a decomposition of the matrix graph into overlapped subdomain   */
  /*                  using a permutation vector of the unknowns. The permutation vector    */
  /*       is produced by a reordering method based on a Nested Dissection technique        */
  /* by METIS or SCOTCH library                                                             */
  /* The function decomposes the graph in subgraph of at most domsize nodes.                */
  /* the resulting number of domain is returned in ndom                                     */
  /* This routine is very useful when ones want to produce a domain decomposition of the    */
  /* matrix graph such that the fill-in is minimized and the overlap  between  subdomains   */
  /* is thin (the overlapp will roughly consists of the separator used in the Nested        */
  /* Dissection.                                                                            */
  /* On entry:                                                                              */
  /*     domsize : the maximal domain size admissible                                       */
  /*     n, ia, ja : the matrix graph (symmetric graph !)                                   */
  /*                                                                                        */
  /* On return:                                                                             */
  /*     perm, iperm : the ordering vectors produced by METIS or SCOTCH                     */
  /*     ndom : number of domain obtained                                                   */
  /*   mapptr, mapp: pointer to the graph decomposition                                     */
  /*                                                                                        */
  /* NOTE: mapptr and mapp are allocated inside this function                               */
  /*       ndom is a pointer since the number of domain obtained can be different than      */
  /*       the one desired in some rare cases (a warning is displayed in this cased)        */
  /******************************************************************************************/        


  dim_t cblknbr;
  dim_t * rangtab; /* Size n+1 */
  dim_t *treetab;

  rangtab = (dim_t *)malloc(sizeof(dim_t)*(n+1));
  treetab = (dim_t *)malloc(sizeof(dim_t)*(n+1));

#ifndef SCOTCH_PART
{
  int option[10];
  flag_t numflag = 0;
  option[0] = 0;
  
  /**** Compute a matrix reordering that minimizes fill-in *******/
  /** NOTE: METIS inverse the usual definition of perm and iperm
      (compared to SCOTCH or PHIDAL) **/
  /* metis_NodeND(&n, ia, ja, &numflag, option, iperm, perm);*/
  METIS_NodeND_WRAPPER(n, ia, ja, numflag, option, iperm, perm);
  find_supernodes(n, n, ia, ja, perm, iperm, &cblknbr, rangtab, treetab);
}
#else
 {
   SCOTCH_Graph        grafdat;
   SCOTCH_Strat        stratdat;
   char STRAT[400];
   
   SCOTCH_stratInit (&stratdat);
   
   
   /** This strategy includes the ordering of the leaves i.e. the interior nodes **/
   if(domsize < 120)
     sprintf(STRAT, "n{sep=/(vert>%d)?m{vert=100,low=h{pass=10},asc=f{bal=0.2}};,ole=f{cmin=0,cmax=10000,frat=0.08},ose=g}", domsize);
   else
     sprintf(STRAT, "n{sep=/(vert>120)?m{vert=100,low=h{pass=10},asc=f{bal=0.2}};,ole=f{cmin=0,cmax=10000,frat=0.08},ose=g}"); 
    
    SCOTCH_stratGraphOrder (&stratdat, STRAT);
    
    SCOTCH_graphInit  (&grafdat);
    /*SCOTCH_graphBuild (&grafdat, 0, n, ia, NULL, NULL, NULL, ia[n], ja, NULL);*/
    SCOTCH_graphBuild_WRAPPER (&grafdat, 0, n, ia, NULL, ja, NULL);
    
    /*  fprintfd(stderr, "NDOM %ld \n", (long)ndom);*/
    /** Do not free verttab and edgetab before SCOTCH_graphOrder **/
    /*SCOTCH_graphOrder (&grafdat, &stratdat, perm, iperm, &cblknbr, rangtab, treetab);*/
    SCOTCH_graphOrder_WRAPPER (&grafdat, &stratdat, n, perm, iperm, &cblknbr, rangtab, treetab);

    
    SCOTCH_graphExit (&grafdat);
    SCOTCH_stratExit (&stratdat);
 }
 
#endif

/*for(i=0;i<=cblknbr;i++)
  fprintfd(stderr, "rangtab[%d]Â = %d \n", i, rangtab[i]);*/

 mysave(&cblknbr, 1, "cblknbr.dt");
 mysave(perm, n,     "perm.dt");
 mysave(iperm, n,    "iperm.dt");
 mysave(rangtab, n+1,"rangtab.dt");
 mysave(treetab, n+1,"treetab.dt");

}


void PHIDAL_Perm2SizedDomains_LOAD(dim_t domsize, dim_t n, INTL *ia,dim_t *ja, dim_t *ndom, dim_t **mapptr,  dim_t **mapp, dim_t *perm, dim_t *iperm)
{
  /******************************************************************************************/
  /* This function computes a decomposition of the matrix graph into overlapped subdomain   */
  /*                  using a permutation vector of the unknowns. The permutation vector    */
  /*       is produced by a reordering method based on a Nested Dissection technique        */
  /* by METIS or SCOTCH library                                                             */
  /* The function decomposes the graph in subgraph of at most domsize nodes.                */
  /* the resulting number of domain is returned in ndom                                     */
  /* This routine is very useful when ones want to produce a domain decomposition of the    */
  /* matrix graph such that the fill-in is minimized and the overlap  between  subdomains   */
  /* is thin (the overlapp will roughly consists of the separator used in the Nested        */
  /* Dissection.                                                                            */
  /* On entry:                                                                              */
  /*     domsize : the maximal domain size admissible                                       */
  /*     n, ia, ja : the matrix graph (symmetric graph !)                                   */
  /*                                                                                        */
  /* On return:                                                                             */
  /*     perm, iperm : the ordering vectors produced by METIS or SCOTCH                     */
  /*     ndom : number of domain obtained                                                   */
  /*   mapptr, mapp: pointer to the graph decomposition                                     */
  /*                                                                                        */
  /* NOTE: mapptr and mapp are allocated inside this function                               */
  /*       ndom is a pointer since the number of domain obtained can be different than      */
  /*       the one desired in some rare cases (a warning is displayed in this cased)        */
  /******************************************************************************************/        


  dim_t cblknbr;
  dim_t *rangtab; /* Size n+1 */
  dim_t *treetab;
  INTL ind, i, j;
  dim_t domind;
  dim_t *cblk2dom;
  



  rangtab = (dim_t *)malloc(sizeof(dim_t)*(n+1));
  treetab = (dim_t *)malloc(sizeof(dim_t)*(n+1));


/*for(i=0;i<=cblknbr;i++)
  fprintfd(stderr, "rangtab[%d]Â = %d \n", i, rangtab[i]);*/

 myload(&cblknbr, 1, "cblknbr.dt");
 myload(perm, n,     "perm.dt");
 myload(iperm, n,    "iperm.dt");
 myload(rangtab, n+1,"rangtab.dt");
 myload(treetab, n+1,"treetab.dt");

 fprintfd(stderr, "Find supernodes = %d \n", cblknbr);

 /********************************************************************************************/
 /*                                                                                          */
 /*                  FIND THE intERIOR SUBDOMAINS DEFINED BY THE NESTED DISSECTION           */
 /*                                                                                          */
 /********************************************************************************************/


 cblk2dom = (dim_t *)malloc(sizeof(dim_t)*cblknbr);
 if(get_interior_grid(domsize, cblknbr, rangtab, treetab, ndom, cblk2dom) != 0)
   {
     fprintfd(stderr, "Error in get_interior_grid \n");
     exit(-1);
   }
 
 /*if(get_interior_grid2(domsize, n, ia, ja, perm, iperm, cblknbr, rangtab, treetab, ndom, cblk2dom) != 0)
   {
     fprintfd(stderr, "Error in get_interior_grid2 \n");
     exit(-1);
     }*/
  

 /***********************************/
 /*** Construct mapptr            ***/
 /***********************************/
 (*mapptr)=(dim_t *)malloc(sizeof(dim_t)*((*ndom)+1));
  bzero((*mapptr), sizeof(dim_t)*((*ndom)+1));
  ind = 0;
  for(i=0;i<cblknbr;i++)
    {
      domind = cblk2dom[i];
      if(domind >= 0)
	{
	  (*mapptr)[domind+1] += rangtab[i+1]-rangtab[i];
	  ind += rangtab[i+1]-rangtab[i];
	}
    }


  for(i=1;i<=(*ndom);i++)
    (*mapptr)[i]+=(*mapptr)[i-1];

  

  /*** Construct mapp ***/
  (*mapp)=(dim_t *)malloc(sizeof(dim_t)*ind);
  for(i=0;i<cblknbr;i++)
    {
      domind = cblk2dom[i];
      if(domind >= 0)
	{
	  memcpy((*mapp)+(*mapptr)[domind], iperm+rangtab[i], sizeof(int)*(rangtab[i+1]-rangtab[i]));
	  (*mapptr)[domind] += rangtab[i+1]-rangtab[i];
#ifdef DEBUG_M
	  assert((*mapptr)[domind] <= (*mapptr)[domind+1]);
#endif
	}
    }

  /*** Reset mapptr ***/
  for(i=(*ndom);i>=1;i--)
    (*mapptr)[i] = (*mapptr)[i-1];
  (*mapptr)[0] = 0;

  /** Eliminate NULL domain **/
  j = 0;
  for(i=0;i<(*ndom);i++)
    if((*mapptr)[i+1]>(*mapptr)[i])
      (*mapptr)[j++] = (*mapptr)[i];
  (*mapptr)[j] = (*mapptr)[*ndom];
  *ndom = j;
#ifdef DEBUG_M
  assert((*mapptr)[(*ndom)] == ind);
#endif


  /*** Desallocations ***/
  free(cblk2dom);
  free(rangtab);
  free(treetab);


  /**** Expand domain to get overlap ****/
  get_overlap(*ndom, n, mapptr, mapp, ia, ja);

#ifdef DEBUG_M
#ifndef SCOTCH_PART
  /*** Check if any interior node of a subdomain is related to another subdomain interior node ***/
  fprintfd(stderr, "Check interior domains \n");
  check_interior_node((*ndom), (*mapptr), (*mapp), ia, ja, n);
  fprintfd(stderr, "Check interior domains DONE \n");
#endif
#endif


  /** Sort the node inside each subdomain according to their order in 
      the initial ordering made by the graph reorder (SCOTCH or METIS)
      **/
  /**** IT IS DONE AGAIN AT THE END OF HierarchDecomp *******/
  /**** We do it here only because by security to compute the new
  supernode partition in the interior domain:
  indeed some node in the interface are susceptible to become 
  interior node (due to function get_overlap); they have been 
  added at the end of the interior node set of each subdomain **/
  
  /*for(i=0;i<*ndom;i++)
    quicksort_int( (*mapp), (LONG)(*mapptr)[i], (LONG)((*mapptr)[i+1]-1), perm);  */
  
}


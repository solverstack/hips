/* @authors P. HENON */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include "phidal_ordering.h"

void build_connector_struct(int n, INTL *ia, dim_t *ja, int *rperm, PhidalHID *BL, CONNECTOR_str *Ctab)
{
/********************************************************************************************
* This function compute the quotient graph on the connector set: each connector             *
* is considered as a vertex in this graph, there is an edge between two connectors          *
* if there is at least an edge between two vertice in these connector                       *
*                                                                                           *
* On entry :                                                                                *
*      ia ,ja  : the CSR pattern of the matrix                                              *
*      n       : matrix range                                                               *
*      PhidalHID  : the connector description compute by setup_connectors                   *
*      rperm   : rperm    is the reverse permutation array corresponding to the connector   *
*                redordering of the unknow of the initial matrix                            *
*      Ctab    : a pointer to a vector of structure CONNECTOR_str allocated by user         *
* On return :                                                                               *
*      Ctab : where C(i) is a structure that contains the information about connector i     *
*          .vlist : list of vertice in connector i                                          *
*          .nvert : size of vlist                                                           *
*          .nkey  : the length of key                                                       *
*          .key   : the key of connector i                                                  *
*          .numn  : number of neighbor                                                      *
*          .listn : list of the neighbor                                                    *
*                                                                                           *
********************************************************************************************/

  int i, j, k, ind;
  int node, neigh;
  int nblock;
  int *block_index;
  int *block_key;
  int *block_keyindex;
  int *node2connector; 
  CONNECTOR_str *C;
  int *flag;
  int *tmpn;  /** Work arrays **/
  int *jrev;

  /*** Unpack the connector structure ***/
  nblock = BL->nblock;
  block_index = BL->block_index;
  block_keyindex = BL->block_keyindex;
  block_key = BL->block_key;
  

  /*** Set up node2connector[i] = connector id of node i ***/
  node2connector = (int *)malloc(sizeof(int)*n);
  for(i=0;i<nblock;i++)
    for(j=block_index[i];j<block_index[i+1];j++)
      node2connector[rperm[j]] = i;
  

  /**** Fill the connector structures ***/
  flag = (int *)malloc(sizeof(int)*nblock);
  tmpn = (int *)malloc(sizeof(int)*nblock);
  jrev = (int *)malloc(sizeof(int)*nblock);


  bzero(flag, sizeof(int)*nblock);

  for(i=0;i<nblock;i++)
    {
      C = Ctab+i;
      C->nvert = block_index[i+1]-block_index[i];
#ifdef DEBUG_M
      assert(C->nvert > 0);
#endif
      C->vlist = (int *)malloc(sizeof(int)*C->nvert);
      memcpy(C->vlist, rperm + block_index[i], sizeof(int)*C->nvert);

      
      C->nkey =  block_keyindex[i+1]-block_keyindex[i];
#ifdef DEBUG_M
      assert(C->nkey > 0);
#endif
      C->key = (int *)malloc(sizeof(int)*C->nkey);
      memcpy(C->key, block_key + block_keyindex[i], sizeof(int)*C->nkey);
  
      /** Fill neighborhood info **/
      ind = 0;
      C->numn = 0;
      for(j=0;j<C->nvert;j++)
	{
	  node = C->vlist[j];
	  for(k=ia[node];k<ia[node+1];k++)
	    {
	      neigh = node2connector[ ja[k] ];
	      if(neigh != i && flag[neigh] == 0)
		{
		  flag[neigh] = 1;
		  jrev[ind++] = neigh;
		  tmpn[C->numn] = neigh;
		  C->numn++;
		}
	    }
	}
      /** reset flag **/
      for(k=0;k<ind;k++)
	flag[jrev[k]] = 0;


      if(C->numn>0)
	{
	  C->listn = (int *)malloc(sizeof(int)*C->numn);
	  memcpy(C->listn, tmpn, sizeof(int)*C->numn);
	}
      else
	C->listn = NULL;
    }
  
  free(jrev);
  free(flag);
  free(tmpn);
  free(node2connector);
}


void connector_graph(int nc, CONNECTOR_str *Ctab, int **c_ia, int **c_ja, int *mask, int maskval)
{
  dim_t i;
  int index;
  
  if(nc == 0)
    {
      (*c_ia) = NULL;
      (*c_ja) = NULL;
      return;
    }

  (*c_ia) = (int *)malloc(sizeof(int)*(nc+1));
  index =0;
  for(i=0;i<nc;i++)
    {
      (*c_ia)[i] = index;
      if(mask[i] == maskval)
	index += Ctab[i].numn;
    }
  (*c_ia)[nc]=index;

  if(index > 0)
    (*c_ja) = (int *)malloc(sizeof(int)*index);
  else
    (*c_ja) = NULL;

  for(i=0;i<nc;i++)
    if(mask[i] == maskval)
      memcpy( (*c_ja) + (*c_ia)[i], Ctab[i].listn, sizeof(int)*(Ctab[i].numn) );
}

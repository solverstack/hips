/* @authors P. HENON */

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>

#include "phidal_ordering.h"


/*#define SPKIT_F*/
/* utilisation du code fortran en complexe ... c propre ;) */
#undef SPKIT_F /** On utilise faplb et csrcsc en C (int64) **/


#ifdef  SPKIT_F
#ifdef TYPE_COMPLEX
#define FFaplb Faplbc
#define FFcsrcsc Fcsrcscc
#else 
#define FFaplb Faplb
#define FFcsrcsc Fcsrcsc
#endif
#endif


void PHIDAL_SymmetrizeMatrix(flag_t job, flag_t numflag, dim_t n, INTL *ia, dim_t *ja, COEF *a, INTL **gia, dim_t **gja, COEF **ga)
{
  /****************************************************************************************/
  /* This function symmetrizes a triangular matrix                                        */
  /* On entry:                                                                            */
  /* flag_t job : =0 only the pattern is symmetrized (null entries are added)                */
  /*           =1 null entries are added in the matrix so that the pattern is symmetric   */
  /*           =2 the coefficient are also symmetrized  (B = A+At-Diag)                   */
  /* int numflag : =0 C numbering                                                         */
  /*               =1 Fortran numbering                                                   */                        
  /* n, ia, ja, a : the triangular matrix in CSR (or CSC) format                          */
  /*              : if a == NULL then only ia and ja are considered                       */
  /* On return:                                                                           */
  /* gia, gja, ga : the symmetrized matrix     TODO: nom                                  */
  /* NOTE : gia, gja, ga are allocated in this function                                   */
  /****************************************************************************************/
  
  INTL nnz;
#ifdef SPKIT_F 
  dim_t i;
#endif /* SPKIT_F */
  int j, ierr;
  INTL *ib;
  dim_t *jb, *iwk;
  flag_t job2;
  COEF *b = NULL;


 
#ifdef DEBUG_M
  assert(ia[0] == numflag);
#endif

#ifdef SPKIT_F 
  if(numflag == 0) {
    CSR_Cnum2Fnum(ja, ia, n);
  }
#else
  if(numflag == 1) {
    CSR_Fnum2Cnum(ja, ia, n);
  }
#endif

  if(job != 0)
    job2 = 1;
  else
    job2 = 0;

#ifdef SPKIT_F 
  nnz = ia[n]-1;
#else
  nnz = ia[n];
#endif


  if(nnz > 0)
    jb = (int *)malloc(nnz*sizeof(int));
  else
    jb = NULL;

  ib = (INTL *)malloc((n+1)*sizeof(INTL));
  if(job != 0)
    b = (COEF *)malloc(nnz*sizeof(COEF));
  
  iwk = (int *)malloc(n*sizeof(int));
  
  *gja = (dim_t *)malloc((2*nnz+1)*sizeof(dim_t));
  *gia = (INTL *)malloc((n+1)*sizeof(INTL));
  if(job != 0)
    *ga = (COEF *)malloc((2*nnz+1)*sizeof(COEF));


  /******** Symmetrize the pattern *****************/



#ifndef SPKIT_F
  csr2csc(n, job2, a, ja, ia, b, jb, ib);
#else
  i = 1;
  FFcsrcsc(&n, &job2, &i, a, ja, ia, b, jb, ib);

#ifdef DEBUG_M
  checkCSR(n, ib, jb, b, 1);
#endif

#endif

  if(job == 1 && nnz > 0)
    bzero(b, sizeof(COEF)*nnz);

  if(job == 2) /*** Annul the diagonal in At ****/
    {
      dim_t i;
      for(i=0;i<n;i++)
#ifdef SPKIT_F 
	for(j=ib[i]-1; j<ib[i+1]-1;j++)
	  if(jb[j] == i+1)
#else
	for(j=ib[i]; j<ib[i+1];j++)
	  if(jb[j] == i)
#endif
	    b[j] = 0.0;
    }

#ifdef SPKIT_F 
  i = 2*nnz-n;
#endif

#ifndef SPKIT_F
  if(job == 0) 
    ierr = aplb(n, n, job, NULL, ja, ia, NULL, jb, ib, NULL, *gja, *gia, 2*nnz+1/* -n? */, iwk);
  else {
    ierr = aplb(n, n, job, a, ja, ia, b, jb, ib, *ga, *gja, *gia, 2*nnz+1/* -n */, iwk);
  }
#else 
  if(job == 0)
    FFaplb(&n, &n, &job, NULL, ja, ia, NULL, jb, ib, NULL, *gja, *gia, &i, iwk, &ierr);
  else 
    FFaplb(&n, &n, &job, a, ja, ia, b, jb, ib, *ga, *gja, *gia, &i, iwk, &ierr);
#endif

  if(ierr != 0)
    {
      fprintf(stderr, "Error in aplb : ierr = %ld nnz = %ld \n", (long)ierr, (long)nnz);
      assert(ierr==0);
    }

#ifdef SPKIT_F
  nnz = (*gia)[n]-1;
#else
  nnz = (*gia)[n];
#endif

  (*gja) = (int *)realloc((*gja), nnz*sizeof(int));
  if(job != 0)
    (*ga) = (COEF *)realloc((*ga), nnz*sizeof(COEF));

  free(iwk);

  if(jb!=NULL)
    free(jb); 
  if(ib!=NULL)
    free(ib); 
  if(b!=NULL)
    free(b);
  
#ifdef SPKIT_F 
  if(numflag == 0)
    {
      CSR_Fnum2Cnum(ja, ia, n);
      CSR_Fnum2Cnum(*gja, *gia, n);
    }
#else
  if(numflag == 1)
    {
      CSR_Cnum2Fnum(ja, ia, n);
      CSR_Cnum2Fnum(*gja, *gia, n);
    }
#endif



}

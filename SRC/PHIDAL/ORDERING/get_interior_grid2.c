/* @authors P. HENON */

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include <math.h>

#include "queue.h"
#include "phidal_ordering.h"
#include "scotch_metis_wrapper.h"

void QuotientDomainGraph(int n, INTL *ia, dim_t *ja, int ndom, int *mapp, int *mapptr, 
			 INTL **Qi, int **Qj, INTL **Qvwgt, INTL **Qewgt);
int get_subtree_node(int *nodelist, int cblknum, int *cblktab, int *son_index, int *sontab, dim_t *iperm);


int get_interior_grid2(int domsize, int n, INTL *ia, dim_t *ja, dim_t *perm, dim_t *iperm, dim_t cblknbr, int *cblktab, dim_t *treetab, int *ndom, int *cblk2dom)
{
  /*********************************************************************************************/
  /* This function finds the interior domains such that they have at least a size of domsize   */
  /*********************************************************************************************/

  /**** NOTE : treetab must be ordered by a postfix traversal of the elimination tree ***/
  int i, k, cblknum, father;
  INTL ind;
  flag_t flag;
  int *stsize;  /** subtree size tab **/
  int *nstab; /** Number of son for cblks **/
  int nlayer, old_nlayer;
  int *layer, *old_layer;
  int *son_index;
  int *sontab;
  int *tmpptr;
  REAL avs;
  int *layer2dom;
  int *mapptr, *mapp;
  int domind;
  INTL *qia;
  int *qja;
  INTL *vwgt;
  INTL *ewgt;

 
#ifdef DEBUG_M
  assert(cblknbr>0);
  assert(domsize > 0);
#endif


  /** Scotch mark the root with -1 : we need a root to have its father equals to itself **/
  for(i=0;i<cblknbr;i++)
    if(treetab[i] < 0 || treetab[i] == i)
      treetab[i] = -1;


#ifdef DEBUG_M
  for(i=0;i<cblknbr;i++) /** Post-ordering verification **/
    assert(treetab[i] >= i || treetab[i] == -1);
#endif
  
  /*** Compute the subtree size of each cblk ***/
  stsize = (int *)malloc(sizeof(int)*cblknbr);
  for(i=0;i<cblknbr;i++)
    stsize[i] = cblktab[i+1]-cblktab[i];
  for(i=0;i<cblknbr-1;i++)
    if(treetab[i] >=0 ) /** IF THIS SNODE IS NOT A ROOT **/
      stsize[ treetab[i] ] += stsize[i];
  
  
  /*** Compute the number of son of each cblk ***/
  nstab = (int *)malloc(sizeof(int)*cblknbr);
  bzero(nstab, sizeof(int)*cblknbr);
  for(i=0;i<cblknbr-1;i++)
    if(treetab[i] >= 0) /** IF THIS SNODE IS NOT A ROOT **/
      nstab[treetab[i]]++;

  ind = 0;
  for(i=0;i<cblknbr;i++)
    ind += nstab[i];

  /**********************************************/
  /*** Compute the son list of each supernode ***/
  /**********************************************/
  son_index = (int *)malloc(sizeof(int)*(cblknbr+1));
  sontab = (int *)malloc(sizeof(int)*ind);
  ind = 0;
  for(i=0;i<cblknbr;i++)
    {
      son_index[i] = ind;
      ind += nstab[i];
    }
  son_index[cblknbr] = ind;

  bzero(nstab, sizeof(int)*cblknbr);
  for(i=0;i<cblknbr-1;i++)
    {
      cblknum = treetab[i];
      if(cblknum >= 0) /** IF THIS SNODE IS NOT A ROOT **/
	{
	  sontab[son_index[cblknum]+nstab[cblknum]] = i;
	  nstab[cblknum]++;
	}
    }

  /**********************************************************/
  /*  Compute the initial layer composed of all the roots   */
  /**********************************************************/
  layer = (int *)malloc(sizeof(int)*cblknbr);
  old_layer = (int *)malloc(sizeof(int)*cblknbr);

  nlayer = 0;
  for(i=0;i<cblknbr;i++)
    if(treetab[i] == -1)
      {
	layer[nlayer] = i;
	nlayer++;
      }

  /*fprintfv(5, stderr, "Nlayer intIAL %d \n", nlayer);*/


  /**************************************************************************************/
  /*                                 EXPAND PHASE                                       */
  /* Replace each cblk i of the layer by its sons if the subtree rooted in i is too big */
  /**************************************************************************************/
  flag = 1;
  while(flag == 1)
    {
      REAL maxsonsize;
      /*fprintfv(5, stderr, "Nlayer %d \n", nlayer);*/
      flag = 0;
      tmpptr = old_layer;
      old_nlayer = nlayer;
      old_layer = layer;
      layer = tmpptr;
      nlayer = 0;
      
      for(i=0;i<old_nlayer;i++)
	{
	  cblknum = old_layer[i];
	  

	  if(nstab[cblknum]>=1) /** This node can have only one son but one of its descendant can have many **/ 
	    {
	      /** Compute the sum of the sons subtree sizes **/
	      avs = 0.0;
	      for(k=son_index[cblknum];k<son_index[cblknum+1];k++)
		avs += (REAL) stsize[sontab[k]];
	      
	      /** Find the biggest son **/
	      maxsonsize = (REAL)stsize[sontab[son_index[cblknum]]];
	      for(k=son_index[cblknum];k<son_index[cblknum+1];k++)
		if(stsize[sontab[k]] > maxsonsize)
		  maxsonsize = (REAL)stsize[sontab[k]];

	      /** If all the sons of this cblk won't be merged in a single domain in the 
		  compress phase then split it **/
	      /** NOTE: be carefull that this test is the opposite than the one used in the compress test **/
	      /*#define ORIG*/
#ifdef ORIG	
	      if(fabs(avs - domsize) > (fabs( maxsonsize-domsize) + fabs( avs-maxsonsize - domsize))/2.0)
#else
	      if(stsize[cblknum] > domsize)
#endif
		{
		  /*fprintfv(5, stderr, "Split cblk %d \n" ,cblknum);*/
		  /*** This cblk is worth replacing by its sons ***/
		  for(k=son_index[cblknum];k<son_index[cblknum+1];k++)
		    layer[nlayer++] = sontab[k];
		  flag = 1;
		}
	      else
		layer[nlayer++] = cblknum;
	    }
	  else
	    layer[nlayer++] = cblknum;
	  
	      
	}
    }

  free(old_layer);

  /****** Make a pass to replace each node in the layer by its father if it is the only son **/
  for(i=0;i<nlayer;i++)
    {
      father = treetab[layer[i]];
      while(father >= 0 && nstab[father] == 1)
	{
	  layer[i] = father;
	  father = treetab[father];
	}
    }



  /**********************************************************************/
  /*            COMPRESS PHASE                                          */
  /* Regroup subtree in subdomain that have a size near domsize         */
  /**********************************************************************/
  layer2dom = (int *)malloc(sizeof(int)*nlayer);

  /** Count the total number of node in the subtrees of the layer **/
  ind = 0;
  for(i=0;i<nlayer;i++)
    ind += stsize[layer[i]];
  

  /*fprintfv(5, stderr, "Nlayer = %d , number of node in the layer = %d \n", nlayer, ind);*/

  mapp = (int *)malloc(sizeof(int)*ind);
  mapptr = (int *)malloc(sizeof(int)*(nlayer+1));
  
  /** Fill mapp **/
  ind = 0;
  for(i=0;i<nlayer;i++)
    {
      mapptr[i] = ind;
      /*fprintfv(5, stderr, "domain %d cblknum %d ind = %d stsize = %d \n", i, layer[i], ind, stsize[layer[i]]);*/
      k = get_subtree_node(mapp+ind, layer[i], cblktab, son_index, sontab, iperm);
#ifdef DEBUG_M
      /*if(k !=  stsize[layer[i]])
	fprintfv(5, stderr, "cblknum = %d subtreenode found = %d stsize = %d \n", layer[i], k, stsize[layer[i]]); */
      assert(k == stsize[layer[i]]);
#endif
      ind += k;
    }
  
  mapptr[nlayer] = ind;
    
  /** Get the overlapp **/
  get_overlap(nlayer, n, &mapptr, &mapp, ia, ja);
  
#ifdef DEBUG_M
  /*fprintfv(5, stderr, "After Overlapp \n");
  for(i=0;i<nlayer;i++)
  fprintfv(5, stderr, "layerdom %d = %d nodes \n", i, mapptr[i+1]-mapptr[i]);*/

  for(i=0;i<nlayer;i++)
    assert(mapptr[i+1]-mapptr[i] > 0);
#endif

  /*****************************************/
  /* Compute the quotient weighted graph   */
  /* of the domain defined by the subtrees */
  /* rooted in the layer                   */
  /*****************************************/
  QuotientDomainGraph(n, ia, ja, nlayer, mapp, mapptr, &qia, &qja, &vwgt, &ewgt);

  /** Compute the number of domain such that
      their size are close to domsize **/
  ind = 0;
  for(i=0;i<nlayer;i++)
    ind += vwgt[i];

  /* We check that there is at least two domains < 0.75 domsize */
  k = 0;
  for(i=0;i<nlayer;i++)
    if(stsize[layer[i]] <= 0.75*domsize)
      {
	k++;
	if(k==2)
	  break;
      }
  
  if(k<2)
    (*ndom) = nlayer;
  else
    {
      (*ndom) = floor((REAL)ind/(REAL)domsize+0.5);
      if((*ndom) >= nlayer)
	(*ndom) = nlayer;
      
      if((*ndom) == 0)
	(*ndom) = 1;
    }
  
  
  fprintfd(stderr, "Total node in layer = %ld nlayer = %ld ndom = %ld \n", (long)ind, (long)nlayer, (long)*ndom);

#ifdef DEBUG_M
  /*for(i=0;i<nlayer;i++)
    {
      fprintfv(5, stderr, "Dom %d (%d) :\n   ", i, vwgt[i]);
      for(k=qia[i];k<qia[i+1];k++)
	fprintfv(5, stderr, "%d (%d) - ", qja[k], ewgt[k]);
      fprintfv(5, stderr, "\n");
      }*/
#endif
  if(*ndom < nlayer)
    {
#ifdef SCOTCH_PART
      
      fprintfd(stderr, "TO DO Call to Scoth must be set here. \n");
      assert(0);

#else
      /** These are for METIS call **/
      int option[8], edgecut;
      flag_t numflag, wgtflag; 
      

      /*** Partition the weighted quotient graph in ndom domains with METIS **/
      numflag = 0;
      wgtflag = 3; /** Both vertice and edges are weighted **/
      option[0] = 0;

      METIS_PartGraphKway_WRAPPER(nlayer, qia, qja, vwgt, ewgt, wgtflag, numflag, *ndom, option, &edgecut, layer2dom);    
#endif
    }
  else
    {
#ifdef DEBUG_M
      assert(nlayer == *ndom);
#endif
      for(i=0;i<nlayer;i++)
	layer2dom[i] = i;
    }

  
#ifdef DEBUG_M
  for(i=0;i<nlayer;i++)
    {
      /*fprintfv(5, stderr, "layer %d ==> part %d \n", i, layer2dom[i]);*/
      assert(layer2dom[i] >= 0);
      assert(layer2dom[i] < *ndom);
    }
#endif
      
      
  if(qia[nlayer] > 0)
    {
      free(qja);
      free(ewgt);
    }
  free(qia);
  free(vwgt);

  free(mapp);
  free(mapptr);


  /*** Set the domain for all the subtrees rooted in a cblk in the last layer ***/
  for(i=0;i<cblknbr;i++)
    cblk2dom[i] = -1;
  for(i=0;i<nlayer;i++)
    {
#ifdef DEBUG_M
      assert(layer2dom[i] >= 0);
#endif
      cblk2dom[layer[i]] = layer2dom[i];
    }
  free(layer2dom);
  
  for(i=cblknbr-1;i>0;i--)
    {
      domind = cblk2dom[i];
      if(domind >= 0)
	for(k=son_index[i];k<son_index[i+1];k++)
	  cblk2dom[sontab[k]] = domind;
    }

 
  free(stsize);
  free(nstab);
  free(layer);
  free(son_index);
  free(sontab);

  return 0;
}






void QuotientDomainGraph(int n, INTL *ia, dim_t *ja, int ndom, int *mapp, int *mapptr, 
			 INTL **Qi, int **Qj, INTL **Qvwgt, INTL **Qewgt)
{
  /********************************************************************/
  /* this function computes the weighted quotient graph of a graph    */
  /* partitioned in overlapping domains                               */
  /* The function admits that some nodes of the graph are             */
  /* contained in any domains                                         */
  /********************************************************************/

  dim_t i, j, k;
  int jpos, node, ind, dom, edgenbr;
  int *keystart;
  int *keylen;
  int *key;
  INTL *qia;
  dim_t *qja;
  int *jrev;
  int *tmpj;
  REAL *tmpewgt;
  INTL *vwgt, *ewgt;
  

  /******************************************************/
  /* First we compute the key of each node              */
  /* that is the list of subdomain to which it belongs  */
  /******************************************************/
  
  /** NOTE DO NOT FORGET THAT SOME NODE IN THE GRAPH 
      CAN BE OUTSIDE ANY DOMAINS **/

  /** Count the number of subdomain of each node **/
  keylen = (int *)malloc(sizeof(int)*n);
  bzero(keylen, sizeof(int)*n);

  ind = 0;
  for(k=0;k<ndom;k++)
    for(i=mapptr[k];i<mapptr[k+1];i++)
      {
	keylen[mapp[i]]++;
	ind++;
      }
  key = (int *)malloc(sizeof(int)*ind);

  keystart = (int *)malloc(sizeof(int)*n);
  ind = 0;
  for(i=0;i<n;i++)
    if(keylen[i] > 0)
      {
	keystart[i] = ind;
	ind += keylen[i];
      }
  
  /** We use keylen as indice to fill the key vector **/
  bzero(keylen, sizeof(int)*n);
  for(k=0;k<ndom;k++)
    for(i=mapptr[k];i<mapptr[k+1];i++)
      {
	node = mapp[i];
	key[keystart[node] + keylen[node]] = k;
	keylen[node]++;
      }
 
  qia = (INTL *)malloc(sizeof(INTL)*(ndom+1));
  vwgt = (INTL *)malloc(sizeof(INTL)*ndom);
  tmpj = (int *)malloc(sizeof(int)*ndom);
  tmpewgt = (REAL *)malloc(sizeof(REAL)*ndom);
  jrev = (int *)malloc(sizeof(int)*ndom);

  /** Compute the number of edge in the quotient graph **/
  /** use jrev as temporary working buffer **/
  edgenbr = 0;
  for(k=0;k<ndom;k++)
    {
      ind = 0;

      qia[k] = edgenbr;

      bzero(jrev, sizeof(int)*ndom);
      for(i=mapptr[k];i<mapptr[k+1];i++)
	{
	  node = mapp[i];
	  for(j=keystart[node];j<keystart[node] + keylen[node];j++)
	    {
	      /** Do not admit self edges **/
	      if(key[j] == k)
		continue;

	      if(jrev[key[j]] == 0)
		{
		  ind++;
		  jrev[key[j]] = 1;
		}
	    }
	}
      edgenbr += ind;
    }
  qia[ndom] = edgenbr;

  qja = (int *)malloc(sizeof(int)*edgenbr);
  ewgt = (INTL *)malloc(sizeof(INTL)*edgenbr);

  /** Compute the  quotient graph **/
  /** Each vertex is wieghted by the number of interior node
      plus the number of interface node divided by the number of
      domain that share it (key lenght)  **/
  /** Each vertex is valuated by the number of node in the interface 
      between the two subdomain ; each node is poundered by the
      inverse of its key lenght)**/


  for(i=0;i<ndom;i++)
    jrev[i] = -1;
  bzero(vwgt, sizeof(INTL)*ndom);
  edgenbr = 0;

  for(k=0;k<ndom;k++)
    {
      REAL d;
      ind = 0;
      d = 0;
      for(i=mapptr[k];i<mapptr[k+1];i++)
	{
	  node = mapp[i];
	  d += 1.0/(REAL)keylen[node];
	  for(j=keystart[node];j<keystart[node]+keylen[node];j++)
	    {
	      dom = key[j];
	      /** Do not admit self edges **/
	      if(dom == k)
		continue;

	      jpos = jrev[dom];
	      if(jpos < 0)
		{
		  jrev[dom] = ind;
		  tmpj[ind] = dom;
		  tmpewgt[ind] = 1.0/(REAL)keylen[node];
		  ind++;
		}
	      else
		tmpewgt[jpos] +=  1.0/(REAL)keylen[node];
	    }
	}
      vwgt[k] = (INTL)d;
      memcpy(qja+edgenbr, tmpj, sizeof(int)*ind);

      for(i=0;i<ind;i++)
	ewgt[edgenbr+i] = (INTL)(tmpewgt[i]);

      edgenbr += ind;
      /** reinit jrev **/
      for(i=0;i<ind;i++)
	jrev[tmpj[i]] = -1;
      
    }
#ifdef DEBUG_M
  /** Check if we found the same number of edges than 
      what we count before **/
  assert(edgenbr == qia[ndom]);
#endif


  free(jrev);
  free(tmpj);  
  free(tmpewgt);
  free(keylen);
  free(keystart);
  free(key);

  *Qi = qia;
  *Qj = qja;
  *Qvwgt = vwgt;
  *Qewgt = ewgt;


}




int get_subtree_node(int *nodelist, int cblknum, int *cblktab, int *son_index, int *sontab, dim_t *iperm)
{
  dim_t i;
  int nodenbr;
  int offset;
  nodenbr = 0;
 
  /*fprintfv(5, stderr, "cblknum = %d, NBSON = %d nodenbr = %d , cblksize = %d \n", cblknum, 
    son_index[cblknum+1]-son_index[cblknum], nodenbr, cblktab[cblknum+1]-cblktab[cblknum]);*/


  for(i=son_index[cblknum];i<son_index[cblknum+1];i++)
    nodenbr += get_subtree_node(nodelist + nodenbr, sontab[i], cblktab, son_index, sontab, iperm);


  offset = cblktab[cblknum];
  for(i=0;i<cblktab[cblknum+1]-cblktab[cblknum];i++)
    nodelist[nodenbr+i] = iperm[i+offset];

  nodenbr += cblktab[cblknum+1]-cblktab[cblknum];

  return nodenbr;
      
}

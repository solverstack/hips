/* @authors P. HENON */

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>

#include "phidal_ordering.h"

/** Local function **/
void CSR_deldiag(int numflag, int n, INTL *ia, dim_t *ja);

void PHIDAL_BuildGraph(flag_t unsym, flag_t numflag, dim_t n, INTL *ia, dim_t *ja, INTL **gia, dim_t **gja)
{
  /***************************************************************************************/
  /* This function computes the symmetric adjacency graph of a matrix                    */
  /* => if the matrix has an unsymmetric pattern then it is symmetrized and the          */
  /*     diagonal entries are delete (no self edge allowed)                              */
  /* => if the matrix has a symmetric pattern then just the diagonal entries are delete  */
  /* On entry:                                                                           */
  /*   numflag     : = 0 C numbering, := 1 Fortran Numbering of the matrix graph         */
  /*   unsym       : =1 means the matrix has not a symmetric pattern                     */
  /*                 =0 means the matrix has a symmetric pattern                         */
  /*   n, *ia, *ja : the CSR matrix indices                                              */
  /* On return :                                                                         */
  /*   gia, gja : the symmetric adjacency graph. Numering is C or Fortran depending on   */
  /*              the numflag value                                                      */
  /*                                                                                     */
  /* NOTE: symmetric means the matrix has a symmetric pattern not that the matrix        */
  /*       is symmetric                                                                  */
  /*        BE CAREFUL: if the pattern is symmetric you must enter this function with    */
  /*       the complete matrix : not the lower triangulat part.                          */
  /*       This means you should have call PHIDAL_SymmetrizeMatrix before enter here     */
  /***************************************************************************************/

  flag_t job;
  INTL nnz;

#ifdef DEBUG_M
  assert(ia[0] == numflag);
#endif

  if(numflag == 0)
    CSR_Cnum2Fnum(ja, ia, n);
  
  nnz = ia[n]-1;


  if(unsym  == 1)
    {
      job = 0;
      PHIDAL_SymmetrizeMatrix(job, 1, n, ia, ja, NULL, gia, gja, NULL);
    }
  else
    {
      *gja = (dim_t *)malloc(nnz*sizeof(dim_t));
      *gia = (INTL *)malloc((n+1)*sizeof(INTL));
      memcpy(*gja, ja, sizeof(int)*nnz);
      memcpy(*gia, ia, sizeof(INTL)*(n+1));
    }

  
  /******* Delete the diagonal entries ************/
  /*CSR_deldiag(1, n, *gia, *gja);*/

  if(numflag == 0)
    {
      CSR_Fnum2Cnum(ja, ia, n);
      CSR_Fnum2Cnum(*gja, *gia, n);
    }

}



void CSR_deldiag(int numflag, int n, INTL *ia, dim_t *ja)
{
  /**********************************************************************/
  /** This function delete the diagonal entries in a CSR or CSC matrix **/
  /** In place algorithm                                               **/
  /**********************************************************************/

  int ind;
  dim_t i, j, s;
  
#ifdef DEBUG_M
  assert(numflag == 0 || numflag == 1);
  assert(ia[0] == numflag);
#endif
  
  ind = 0;
  for(i=0;i<n;i++)
    {
      s = ind;
      for(j=ia[i];j< ia[i+1];j++)
	if(ja[j-numflag] != i+numflag) 
	  ja[ind++] =ja[j-numflag];
      /*else
	fprintfv(5, stderr, "row %d %d %d  ", i, ja[j-numflag], j-numflag);*/

      ia[i] = s+numflag;
    }
  
  ia[ n ] = ind+numflag;
}

/* @authors P. HENON */

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include "queue.h"


int vec_balanced_partition(int ndom, int n, REAL *W, int *elt2dom)
{
  /******************************************************************/
  /* This function partition contiguously a 1 dimensionnal domain   */
  /* composed of n insecable component. The weighted of each        */
  /* element is W[i]. In return elt2dom[i] is the domain of the     */
  /* component i                                                    */
  /******************************************************************/
  int i, dnum;
  REAL Wmean, Wloc, Wideal;

  if(ndom>n)
    return -1; /** Not able to partition **/
#ifdef DEBUG_M
  assert(ndom > 0);
  for(i=0;i<n;i++)
    assert(W[i]>0);
#endif

  Wmean = 0.0;
  for(i=0;i<n;i++)
    Wmean += W[i];
  Wmean /= ndom;

  Wloc = 0.0;
  i = 0;
  for(dnum=0;dnum<ndom;dnum++)
    {
      /** We force at least one element in the domain i **/
      Wloc += W[i];
      elt2dom[i] = dnum;
      i++;

      Wideal = Wmean*(dnum+1);
      while(i < n-ndom+dnum+1 && Wideal > Wloc+W[i]/2.0 )
	{
	  elt2dom[i] = dnum;
	  Wloc += W[i]; 
	  i++;
	}
    }
#ifdef DEBUG_M
  assert(i == n);
#endif
    
  return EXIT_SUCCESS;
}

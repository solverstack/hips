/* @authors P. HENON */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include "phidal_ordering.h"


/** local function **/
void  post_order(int n, int *father, int *T,  dim_t *perm, dim_t *iperm);
void compute_subtree_size(int n, int *father, dim_t *perm, dim_t *iperm, int *T);
void compute_elimination_tree(int nfirst, int n, INTL *ia, dim_t *ja, dim_t *perm, dim_t *iperm, int *father);



void  find_supernodes(int nfirst, int n, INTL *ia, dim_t *ja, dim_t *perm, dim_t *iperm, int *snodenbr, int *snodetab, dim_t *treetab)
{
  /********************************************************************************************/
  /* This function computes the supernodal partition of a reordered
     matrix; only the nfirst node of the reordered matrix are considered
          */
  /* The permutation of the matrix given on entry is modified to obtain a                     */
  /* postorder of the elimination tree: this does not affect the fill-in                      */
  /* properties of the initial ordering.                                                      */
  /* The matrix pattern is assumed to be symmetric                                            */
  /* NOTE :                                                                                   */
  /* This function can take on entry the lower triangular part or the whole matrix A:         */
  /* this does not change the results                                                         */
  /*                                                                                          */
  /* On entry:                                                                                */
  /* n, ia, ja : the initial matrix A in CSC format                                           */
  /*             in C numbering                                                               */
  /*  perm : the permutation vector                                                           */
  /* iperm : the inverse permutation vector                                                   */
  /* snodetab : allocated to contain at most n integers                                       */
  /* treetab  : allocated to contain at most n integers                                       */
  /*            it can be set to NULL in which case the return value is treetab = NULL        */
  /* On return :                                                                              */
  /* perm, iperm : postorder of the node in the elimination tree deduced from the initial     */
  /*               ordering                                                                   */
  /* snodenbr  : number of supernodes found                                                   */
  /* snodetab  : snodetab[i] is the beginning in the new ordering of the ith supernodes       */
  /* treetab   : treetab[s] is the number of the father of supernode i on the supernodal      */
  /*             elimination tree                                                             */
  /********************************************************************************************/

  int *father; /** father[i] is the father of node i in he elimination tree of A **/
  int *T; /** T[j] is the number of node in the subtree rooted in node j in the elimination tree of A **/
  int *S; /** S[i] is the number of sons for node i in the elimination tree **/
  int *isleaf, *prev_rownz;
  dim_t i, j, k;
  int pi, pj;
  int dad;


  T = (int *)malloc(sizeof(int)*n);
  assert(T != NULL);
  S = (int *)malloc(sizeof(int)*n);
  assert(S != NULL);
  father = (int *)malloc(sizeof(int)*n);
  assert(father != NULL);
  isleaf = (int *)malloc(sizeof(int)*n);
  assert(isleaf != NULL);
  prev_rownz = (int *)malloc(sizeof(int)*n);
  assert(prev_rownz != NULL);
#ifdef DEBUG_M
  assert(ia[0] == 0);
#endif


#ifdef DEBUG_M
  /** Check the permutation vector **/
  for(i=0;i<n;i++)
    {
      assert(perm[i] >= 0);
      assert(perm[i] < n);
    }

  bzero(S, sizeof(int)*n);
  for(i=0;i<n;i++)
    S[perm[i]]++;

  k = 0;
  for(i=0;i<n;i++)
    if(S[i] != 1)
      k++;
  if(k>0)
    fprintfd(stderr, "perm array is not valid number of error =  %d \n", k);
  assert(k==0);
#endif


  /*** Compute the elimination tree of A ***/
  compute_elimination_tree(nfirst, n, ia, ja, perm, iperm, father);
  
  /*for(i=0;i<n;i++)
    fprintfv(5, stderr, "Father[%d] = %d \n", i, father[i]);*/
  
  /*** Compute the postorder of the elimination tree ***/
  /*** This operation modifies perm and iperm ***/
  post_order(n, father, T, perm, iperm);

  /*** Compute the number of descendant of each node i in the elimination tree ***/
  compute_subtree_size(n, father, perm, iperm, T);
  /*{
    int i ;
    for(i=0;i<n;i++)
    fprintfv(5, stderr, "f(%d)=%d, T=%d  \n", i, father[i], T[i]);
    }*/


  
  bzero(isleaf, sizeof(int)*n);

  /*@@ OIMBE:  COMPRENDS PLUS L'UTILITE DE CET ALGO 
    SUFFIT DE MARQUER LES SUPERNOEUDS QUI ONT PLUS DE 1 FILS ?? */
  bzero(prev_rownz, sizeof(int)*n);
  for(j=0;j<nfirst;j++)
    {
      pj = iperm[j];
      for(i=ia[pj];i<ia[pj+1];i++)
	{
	  pi = perm[ja[i]];
	  if(pi > j)
	    {
	      k = prev_rownz[pi];
	      if(k < j - T[pj]+1 )
		{
		  isleaf[j] = 1;
		}
	      prev_rownz[pi] = j;
	    }
	}
    }
  
  /*** Compute the number of sons of each node in the elimination tree ***/
  bzero(S, sizeof(int)*n);
  for(i=0;i<n;i++)
    if(father[i] != i)
      S[father[i]]++;
  
  for(i=0;i<n;i++)
    if(S[i] != 1)
      isleaf[perm[i]] = 1;
  
  (*snodenbr) = 0;
  for(i=0;i<n;i++)
    if(isleaf[i] == 1)
      {
	snodetab[(*snodenbr)] = i;
	(*snodenbr)++;
      }
  snodetab[(*snodenbr)] = n;


  if(treetab != NULL)
    {
      /*** Node to supernode conversion vector ***/
      for(i=0;i<(*snodenbr);i++)
	for(j=snodetab[i];j<snodetab[i+1];j++)
	  S[j] = i;

      /*** Fill the treetab info ***/
      for(i=0;i<(*snodenbr);i++)
	{
	  k=(*snodenbr);
	  for(j=snodetab[i];j<snodetab[i+1];j++)
	    {
	      dad = S[perm[father[iperm[j]]]];
	      if( dad < k && dad > i)
		k = dad;
	    }
	  treetab[i] = k;
	  if(k==(*snodenbr))
	    {
	      /*fprintfv(5, stdout, "THIS IS A ROOT %d \n", i);*/
	      treetab[i] = i; /** This is a root **/
	    }
#ifdef DEBUG_M
	  assert(treetab[i] >= i);
#endif
	}
      
      
    }
  
#ifdef DEBUG_M
  /*{
    int rootnbr;
    rootnbr = 0;
    for(i=0;i<(*snodenbr);i++)
      {
	if(treetab[i] == i)
	  {
	    fprintfv(5, stderr, "\n MY ROOT %d [%d %d] ", i, snodetab[i], snodetab[i+1]-1);
	    rootnbr++;
	  }
	for(j=snodetab[i];j<snodetab[i+1];j++)
	  if(father[iperm[j]] == iperm[j])
	    fprintfv(5, stderr, "root %d iperm=%d ", j, iperm[j]);
      }
    fprintfv(5, stderr, "\n Number of SROOT %d \n", rootnbr);

    rootnbr = 0;
    for(i=0;i<n;i++)
      if(father[i] == i)
	rootnbr++;

    fprintfv(5, stderr, "Number of ROOT %d \n", rootnbr);
    }*/
#endif


  free(prev_rownz);
  free(isleaf);
  free(father);
  free(S);
  free(T);
}


void  post_order(int n, int *father, int *T,  dim_t *perm, dim_t *iperm)
{
  /********************************************************************************/
  /* This function compute the post order of the elimination tree given on entry  */
  /* On entry:                                                                    */
  /* n : number of nodes                                                          */
  /* father : father[i] is the node number of the father of node i                */
  /*          if node i is a root then father[i] = i                              */
  /* T : a temporary vector of size n                                             */
  /* perm, iperm : ordering of the matrix (value optional: if set then the post   */
  /*                            ordering try to keep the initial ordering as much */
  /*                            as possible)                                      */
  /* On return :                                                                  */
  /* perm, iperm : permutation and inverse permutation vector of the postorder    */
  /********************************************************************************/
  dim_t i;
  dim_t j, k, t;


  /*** First compute the number of node in the subtree rooted in node i ***/
  compute_subtree_size(n, father, perm, iperm, T);

  /** When multiple roots are present we have to compute the start index of each root **/
  t=0;
  for(k=0;k<n;k++)
    {
      i = iperm[k];
      if(father[i] == i)
	{
	  /** This is a root **/
	  j = T[i]; 
	  T[i] += t;
	  t += j;
	}
    }

#ifdef DEBUG_M
  for(i=0;i<n;i++)
    assert(T[i] <= T[father[i]]);
#endif
  


 for(k=n-1;k>=0;k--)
   {
     i = iperm[k];
     perm[i] = T[father[i]]; /** We MUST HAVE father[i] == i for a root ! **/
     T[father[i]]-= T[i];
     T[i] = perm[i]-1;
#ifdef DEBUG_M
     assert(perm[father[i]] >= perm[i]);
#endif
   }



  /** We need to retrieve 1 for the C numbering compliance **/
  for(i=0;i<n;i++)
    perm[i]--;

#ifdef DEBUG_M
  /** Check the permutation vector **/
  for(i=0;i<n;i++)
    {
      /*fprintfv(5, stderr, "(%d = %d) ", i, perm[i]);*/
      assert(perm[i] >= 0);
      assert(perm[i] < n);
    }

  bzero(iperm, sizeof(int)*n);
  for(i=0;i<n;i++)
    iperm[perm[i]]++;

  k = 0;
  for(i=0;i<n;i++)
    if(iperm[i] != 1)
      k++;
  if(k>0)
    fprintfd(stderr, "Number of erreur in perm vector in postorder %d \n", k);
  assert(k==0);
#endif


  /** Compute the iperm vector **/
  for(i=0;i<n;i++)
    iperm[perm[i]] = i;
  
  
  
}



void compute_subtree_size(int n, int *father, dim_t *perm, dim_t *iperm, int *T)
{
  /********************************************/
  /*  Compute the size of each subtree given  */
  /*  the number of the father of each node   */
  /********************************************/
  dim_t k, i;

  /*** OIMBE pas la peine d'utiliser un tas; il suffit de parcourir iperm pour assurer
       de toujours traiter un fils avant son pere ***/

  bzero(T, sizeof(int)*n);

  for(k=0;k<n;k++)
    {
      i = iperm[k];
      T[i]++;
      if(i!=father[i])
	T[father[i]] += T[i];
    }

  

#ifdef DEBUG_M
 {
   int sum;
   sum = 0;
   for(i=0;i<n;i++)
     if(father[i] == i)
       sum += T[i];
   
   if(sum != n)
     fprintfd(stderr, "Error in compute_subtree_size: sum of the subtree = %d n = %d \n", sum, n);
   assert(n == sum);
 }
#endif

}




void compute_elimination_tree(int nfirst, int n, INTL *ia, dim_t *ja, dim_t *perm, dim_t *iperm, int *father)
{
  /******************************************************************************/
  /* Compute the elimination tree of a matrix A (without computing the symbolic */
  /* factorization) associated with a reordering of the matrix                  */
  /* On entry:                                                                  */
  /* Only the nfirst node of the reordered matrix are considered                */
  /* node greater than nfirst are considered as roots                           */
  /* n, ia, ja : the adjacency graph of the matrix (symmetrized)                */
  /* perm : a permutation vector of the matrix                                  */
  /* iperm : the inverse permutation vector                                     */
  /* On return:                                                                 */
  /* father : father[i] = father of node i on the eliminination tree            */
  /* If node i is a root then father[i] = i                                     */
  /* NOTE : father is allocated at a size of n integer on input                 */
  /******************************************************************************/
  dim_t i, j, k;
  int node;
  int vroot;
  int *jrev;
  int *jf;
  int ind;
  int flag;


  jrev = (int *)malloc(sizeof(int)*n);
  for(i=0;i<n;i++)
    jrev[i] = -1;
  jf = (int *)malloc(sizeof(int)*n);
  bzero(jf, sizeof(int)*n);

  for(i=0;i<n;i++)
    father[i] = -1;

  for(i=0;i<nfirst;i++)
    {
      ind = 0;
      node = iperm[i];
      for(j=ia[node];j<ia[node+1];j++)
	{
	  k = ja[j];

	  if(perm[k] < perm[node])
	    {
	      flag = 1;
	      vroot = k;
	      while(father[vroot] != -1 && father[vroot] != node)
		{
		  if(jrev[vroot] >= 0)
		    {
		      flag = 0;
		      break;
		    }
		  
		  jrev[vroot] = ind;
		  jf[ind] = vroot;
		  ind++;
		  
		  vroot = father[vroot];
		}
	      if(flag == 1)
		father[vroot] = node;
	    }

	}
      /** reinit jrev **/
      for(j=0;j<ind;j++)
	jrev[jf[j]]= -1;
    }

  for(i=0;i<n;i++)
    if(father[i] == -1)
      father[i]=i;
  
  free(jrev);
  free(jf);

#ifdef DEBUG_M
  /*** Check to see if a father has a lower rank in the permutation array than one of its sons ***/
  for(i=0;i<n;i++)
    {
      if(perm[i] > perm[father[i]])
	{
	  fprintfd(stderr, "Node %d perm=%d Father %d perm=%d \n", i, perm[i], father[i], perm[father[i]]);
	  assert(perm[i] <= perm[father[i]]);
	}
    }
#endif

}

/* @authors P. HENON */

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include <math.h>
#include "queue.h"
#include "phidal_ordering.h"


int get_interior_grid(int domsize, dim_t cblknbr, int *cblktab, dim_t *treetab, int *ndom, int *cblk2dom)
{
  /*********************************************************************************************/
  /* This function finds the interior domains such that they have at least a size of domsize   */
  /*********************************************************************************************/

  /**** NOTE : treetab must be ordered by a postfix traversal of the elimination tree ***/
  int i, k, cblknum, father;
  int ind, flag;
  int *stsize;  /** subtree size tab **/
  int *nstab; /** Number of son for cblks **/
  int nlayer, old_nlayer;
  int *layer, *old_layer;
  int *son_index;
  int *sontab;
  int *tmpptr;
  REAL avs;
  Queue heap;
  int *layer2dom;
  int *dom2root;
  int *domsizetab;
  int *dom2newdom;
  int *old_dom2root;
  int domind, domnum, domnbr;


#ifdef DEBUG_M
  assert(cblknbr>0);
  assert(domsize > 0);
#endif


  /** Scotch mark the root with -1 : we need a root to have its father equals to itself **/
  for(i=0;i<cblknbr;i++)
    if(treetab[i] < 0 || treetab[i] == i)
      treetab[i] = -1;


#ifdef DEBUG_M
  for(i=0;i<cblknbr;i++) /** Post-ordering verification **/
    assert(treetab[i] >= i || treetab[i] == -1);
#endif
  
  /*** Compute the subtree size of each cblk ***/
  stsize = (int *)malloc(sizeof(int)*cblknbr);
  for(i=0;i<cblknbr;i++)
    stsize[i] = cblktab[i+1]-cblktab[i];
  for(i=0;i<cblknbr-1;i++)
    if(treetab[i] >=0 ) /** IF THIS SNODE IS NOT A ROOT **/
      stsize[ treetab[i] ] += stsize[i];
  
  
  /*** Compute the number of son of each cblk ***/
  nstab = (int *)malloc(sizeof(int)*cblknbr);
  bzero(nstab, sizeof(int)*cblknbr);
  for(i=0;i<cblknbr-1;i++)
    if(treetab[i] >= 0) /** IF THIS SNODE IS NOT A ROOT **/
      nstab[treetab[i]]++;

  ind = 0;
  for(i=0;i<cblknbr;i++)
    ind += nstab[i];

  /**********************************************/
  /*** Compute the son list of each supernode ***/
  /**********************************************/
  son_index = (int *)malloc(sizeof(int)*(cblknbr+1));
  if (ind != 0)
    sontab = (int *)malloc(sizeof(int)*ind);
  else sontab = NULL;
  ind = 0;
  for(i=0;i<cblknbr;i++)
    {
      son_index[i] = ind;
      ind += nstab[i];
    }
  son_index[cblknbr] = ind;

  bzero(nstab, sizeof(int)*cblknbr);
  for(i=0;i<cblknbr-1;i++)
    {
      cblknum = treetab[i];
      if(cblknum >= 0) /** IF THIS SNODE IS NOT A ROOT **/
	{
	  sontab[son_index[cblknum]+nstab[cblknum]] = i;
	  nstab[cblknum]++;
	}
    }

  /**********************************************************/
  /*  Compute the initial layer composed of all the roots   */
  /**********************************************************/
  layer = (int *)malloc(sizeof(int)*cblknbr);
  old_layer = (int *)malloc(sizeof(int)*cblknbr);

  nlayer = 0;
  for(i=0;i<cblknbr;i++)
    if(treetab[i] == -1)
      {
	layer[nlayer] = i;
	nlayer++;
      }

  /*fprintfv(5, stderr, "Nlayer intIAL %d \n", nlayer);*/


  /**************************************************************************************/
  /*                                 EXPAND PHASE                                       */
  /* Replace each cblk i of the layer by its sons if the subtree rooted in i is too big */
  /**************************************************************************************/
  flag = 1;
  while(flag == 1)
    {
      REAL maxsonsize;
      /*fprintfv(5, stderr, "Nlayer %d \n", nlayer);*/
      flag = 0;
      tmpptr = old_layer;
      old_nlayer = nlayer;
      old_layer = layer;
      layer = tmpptr;
      nlayer = 0;
      
      for(i=0;i<old_nlayer;i++)
	{
	  cblknum = old_layer[i];
	  
	
	  if(nstab[cblknum]>=1) /** This node can have only one son but one of its descendant can have many **/ 
	    {
	      /** Compute the sum of the sons subtree sizes **/
	      avs = 0.0;
	      for(k=son_index[cblknum];k<son_index[cblknum+1];k++)
		avs += (REAL) stsize[sontab[k]];
	      
	      /** Find the biggest son **/
	      maxsonsize = (REAL)stsize[sontab[son_index[cblknum]]];
	      for(k=son_index[cblknum];k<son_index[cblknum+1];k++)
		if(stsize[sontab[k]] > maxsonsize)
		  maxsonsize = (REAL)stsize[sontab[k]];

	      /** If all the sons of this cblk won't be merged in a single domain in the 
		  compress phase then split it **/
	      /** NOTE: be carefull that this test is the opposite than the one used in the compress test **/
	      if(fabs(avs - domsize) > (fabs( maxsonsize-domsize) + fabs( avs-maxsonsize - domsize))/2.0)
		{
		  /*fprintfv(5, stderr, "Split cblk %d \n" ,cblknum);*/
		  /*** This cblk is worth replacing by its sons ***/
		  for(k=son_index[cblknum];k<son_index[cblknum+1];k++)
		    layer[nlayer++] = sontab[k];
		  flag = 1;
		}
	      else
		layer[nlayer++] = cblknum;
	    }
	  else
	    layer[nlayer++] = cblknum;
	}
    }

  free(old_layer);

  /****** Make a pass to replace each node in the layer by its father if it is the only son **/
  for(i=0;i<nlayer;i++)
    {
      father = treetab[layer[i]];
      while(father >= 0 && nstab[father] == 1)
	{
	  layer[i] = father;
	  father = treetab[father];
	}
    }



  /**********************************************************************/
  /*            COMPRESS PHASE                                          */
  /*  Merge the subdomains that are < domsize with another one          */
  /*  not "far" from it in the elimination tree                         */
  /**********************************************************************/
  layer2dom = (int *)malloc(sizeof(int)*nlayer);
  dom2root = (int *)malloc(sizeof(int)*nlayer);
  domsizetab = (int *)malloc(sizeof(int)*nlayer);
  dom2newdom = (int *)malloc(sizeof(int)*nlayer);
  old_dom2root = (int *)malloc(sizeof(int)*nlayer);

  domnbr = nlayer;
  for(i=0;i<nlayer;i++)
    {
      cblknum = layer[i];
      layer2dom[i] = i;
      dom2root[i] = treetab[cblknum];
      domsizetab[i] = stsize[cblknum];
    }

  queueInit(&heap, nlayer);
  flag = 1;
  while(flag == 1) /*** Compress domain loop ***/
    {
      queueClear(&heap);

      
      /*fprintfv(5, stderr, "COMPRESS DOMNBR = %d \n", domnbr);*/
      /*for(i=0;i<domnbr;i++)
      fprintfv(5, stderr, "dom %d = %d nodes FATHER = %d \n", i, domsizetab[i], dom2root[i]); */



      for(i=0;i<domnbr;i++)
	queueAdd2(&heap, i, dom2root[i], domsizetab[i]);
      
      domind = 0;
      while(queueSize(&heap)>0)
	{
	  domnum = queueGet(&heap);
	  dom2newdom[domnum] = domind;
	  father = dom2root[domnum];

	  avs = domsizetab[domnum];	  
	  
	  while(queueSize(&heap)>0)
	    {
	      domnum = queueRead(&heap);

	      if( dom2root[domnum] == father 
		  /** NOTE: be carefull that this test is the opposite than the one used in the expand test **/
		  && fabs(domsizetab[domnum]+avs-domsize) <= (fabs(domsizetab[domnum]-domsize)+fabs(avs-domsize))/2.0 )
		{
		  queueGet(&heap);
		  avs += domsizetab[domnum];
		  dom2newdom[domnum] = domind;
		}
	      else
		break;
	    }
	  domind++;
	}

      /*** Rebuilb the domain infotabs ***/
      bzero(domsizetab, sizeof(int)*domnbr);
      for(i=0;i<nlayer;i++)
	{
	  layer2dom[i] = dom2newdom[layer2dom[i]];
	  domsizetab[layer2dom[i]] += stsize[layer[i]];
	}

      tmpptr = old_dom2root;
      old_dom2root = dom2root;
      dom2root = tmpptr;
      for(i=0;i<domind;i++)
	dom2root[i] = -2;

      /** Push up the domains that have a size < domsize **/
      flag = 0;
      for(i=0;i<domnbr;i++)
	{
	  domnum = dom2newdom[i];
	  if(domsizetab[domnum] < domsize && dom2root[domnum] == -2)
	    {
	      father = old_dom2root[i];
	      if(father == -1)
		dom2root[domnum] = -1;
	      else
		{
		  flag = 1; /** At least one domain was pushed up we can iterate **/
		  dom2root[domnum] = treetab[father];
		}
	    }
	  else
	    dom2root[domnum] = old_dom2root[i];
	}
#ifdef DEBUG_M
      assert(domind <= domnbr);
#endif
      domnbr = domind;
    }


  /*fprintfv(5, stderr, "\n AFTER COMPRESS \n");
  for(i=0;i<domnbr;i++)
  fprintfv(5, stderr, "dom %d = %d nodes FATHER = %d \n", i, domsizetab[i], dom2root[i]); */

  queueExit(&heap);  
  free(dom2newdom);
  free(dom2root);
  free(old_dom2root);
  free(domsizetab);

  (*ndom)=domnbr;

  /*** Set the domain for all the subtrees rooted in a cblk in the last layer ***/
  for(i=0;i<cblknbr;i++)
    cblk2dom[i] = -1;
  for(i=0;i<nlayer;i++)
    {
#ifdef DEBUG_M
      assert(layer2dom[i] >= 0);
#endif
      cblk2dom[layer[i]] = layer2dom[i];
    }
  free(layer2dom);
  
  for(i=cblknbr-1;i>0;i--)
    {
      domind = cblk2dom[i];
      if(domind >= 0)
	for(k=son_index[i];k<son_index[i+1];k++)
	  cblk2dom[sontab[k]] = domind;
    }

#ifdef DEBUG_M
  /** Check that a subdomain have not a separator cblk not listed in its interior cblk ***/
  /** If not it means that the expand layer phase is wrong **/

  for(i=cblknbr-1;i>0;i--)
    if(nstab[i] == 1 && cblk2dom[i]<0)
      assert(cblk2dom[sontab[son_index[i]]] <0);
      
	

   for(i=cblknbr-1;i>0;i--)
    {
      domind = cblk2dom[i];
      if(domind < 0 && nstab[i]>1)
	{
	  flag = 1;
	  for(k=son_index[i]+1;k<son_index[i+1];k++)
	    if(cblk2dom[sontab[k]] != cblk2dom[sontab[k-1]] || cblk2dom[sontab[k]] < 0)
	      {
		flag = 0;
		break;
	      }
	  
	  if(flag == 1)
	    {
	      fprintfd(stderr, "ERROR cblk %d have all its %d sons in the same domain %d \n", i, nstab[i], cblk2dom[sontab[son_index[i]]]);
	      fprintfd(stderr, "cblk %d substreesize = %d \n", i, stsize[i]);
	      for(k=son_index[i]; k < son_index[i+1];k++)
		fprintfd(stderr, "Son %d stsize %d \n", sontab[k], stsize[sontab[k]]);
	      assert(0);
	    }
	}
    }
#endif




 
  free(stsize);
  free(nstab);
  free(layer);
  free(son_index);
  if (sontab != NULL)
    free(sontab);

  return 0;
}


/* @authors P. HENON */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <math.h>
#include "phidal_ordering.h"
#include "queue.h"


void find_MIS(flag_t job, dim_t n, INTL *ia, dim_t *ja, INTL *sizetab, flag_t *mask, flag_t maskval, dim_t *MISsize, dim_t *MIS)
{
  /***************************************************************************************/
  /* This function compute an Maximum Independent Set                                    */
  /* On entry :                                                                          */
  /*      job       : if job = 0 try to maximize cardinal of the MIS                     */
  /*                  if job != 0 try to maximize weight of the MIS according to sizetab */
  /*      n, ia, ja : the graph of vertex in CSR format                                  */
  /*                  NB: NO EDGE from vertex i to vertex i are allowed                  */
  /*      sizetab   : sizetab[i] is the weight of vertex i                               */
  /*                  if sizetab == NULL all the vertice have the smae weight            */
  /*      mask      : if mask[i] != maskval then vertex i cannot be selected in the MIS  */
  /*                  if mask == NULL then all the vertice can be selected in the MIS    */
  /* On return :                                                                         */
  /*      MISsize   : the size of the MIS found                                          */
  /*      MIS       : the list of vertex i selected in the MIS                           */
  /*                  NB: MIS is allocated by user ; it must have a sufficient size      */
  /***************************************************************************************/
  
  int i,j, index;
  int v, neigh, nneigh;
  flag_t *lmask;
  INTL *lsizetab;
  int_t *degr;
  Queue heap;


  if(n==0)
    {
      (*MISsize) = 0;
      return;
    }

  lmask = (flag_t *)malloc(sizeof(flag_t)*n);
  if(mask == NULL)
    {
      maskval = 0;
      bzero(lmask, sizeof(flag_t)*n);
    }
  else
    memcpy(lmask, mask, sizeof(flag_t)*n);

  
  lsizetab = (INTL *)malloc(sizeof(INTL)*n);
  if(sizetab == NULL)
    bzero(lsizetab, sizeof(INTL)*n);
  else
    {
      /** We want to take sizetab into account with a precision of 10 percents compared to the average node size **/
      REAL average;
      average = 0;
      j = 0;
      for(i=0;i<n;i++)
	if(lmask[i] == maskval)
	  {
	    average += sizetab[i];
	    j++;
	  }

      average = fabs(average/j);

      memcpy(lsizetab, sizetab, sizeof(INTL)*n);

      for(i=0;i<n;i++)
	if(lmask[i] == maskval)
	  {
	    lsizetab[i] = (INTL)(lsizetab[i]*10.0/average);
	    /*fprintfv(5, stderr, "(i=%d sizetab = %d)  ", i, lsizetab[i]);*/
	  }
      /*fprintfv(5, stderr, "\n");*/
      
    }


  /***** Compute the degree (=number of unselected neighbor) for each vertex *****/
  degr = (int_t *)malloc(sizeof(int_t)*n);
  for(i=0;i<n;i++)
    {
      degr[i] = 0;
      for(j=ia[i];j<ia[i+1];j++)
	if(lmask[ja[j]] == maskval)
	  degr[i]++;
    }

  /**** Sort the vertice according to their degree and size ******/
  queueInit(&heap, 2*n);
  for(i=0;i<n;i++)
    if(lmask[i] == maskval)
      {
	if(job == 0)
	  queueAdd2(&heap, i, degr[i], -lsizetab[i]); 
	else
	  queueAdd2(&heap, i, -lsizetab[i], degr[i]); 
      }
  
  index = 0;
  while(queueSize(&heap) > 0)
    {
      v = queueGet(&heap);
      if(lmask[v] == maskval)
	{
	  lmask[v] = maskval+1;
	  MIS[index] = v;
	  index++;
	  /*** Mark all the neighbor of v and update degree ***/
	  for(i=ia[v];i<ia[v+1];i++)
	    {
	      neigh = ja[i];
	      if(lmask[neigh] == maskval)
		{
		  /*** Update the degree of the neighbor of neigh ***/
		  for(j=ia[neigh];j<ia[neigh+1];j++)
		    {
		      nneigh = ja[j];
		      if(lmask[nneigh] == maskval)
			{
			  degr[nneigh]--;
#ifdef DEBUG_M
			  assert(degr[nneigh] >= 0);
#endif
			}
		    }
		}
	    }

	  /**** Resort the vertice ****/
	   for(i=ia[v];i<ia[v+1];i++)
	    {
	      neigh = ja[i];
	      if(lmask[neigh] == maskval)
		{
		  lmask[neigh] = maskval+1; /** This vertex can not be selected in the MIS anymore ***/
		  /*** Update the degree of the neighbor of neigh ***/
		  for(j=ia[neigh];j<ia[neigh+1];j++)
		    {
		      nneigh = ja[j];
		      if(lmask[nneigh] == maskval)
			{
			  if(job == 0)
			    queueAdd2(&heap, nneigh, degr[nneigh], -lsizetab[nneigh]); 
			  else
			    queueAdd2(&heap, nneigh, -lsizetab[nneigh], degr[nneigh]); 
			}
		    }
		}
	    }

	}
      
    }
  (*MISsize) = index;
  
  /** Free memory ***/
  queueExit(&heap);
  free(degr);
  free(lmask);
  free(lsizetab);

}

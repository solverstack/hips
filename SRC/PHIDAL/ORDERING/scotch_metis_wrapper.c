/* @authors P. HENON */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include "phidal_ordering.h"
#include "scotch_metis_wrapper.h"

#ifdef SCOTCH_PART

static SCOTCH_Num *ia_tmp = NULL;
static SCOTCH_Num *ja_tmp= NULL;
static SCOTCH_Num *vwgt_tmp=NULL;
static SCOTCH_Num *ewgt_tmp=NULL;

int SCOTCH_graphOrder_WRAPPER( SCOTCH_Graph *  graphdatptr,  SCOTCH_Strat *  stratdatptr, dim_t n, dim_t *perm, dim_t *rperm, dim_t *cblknbr, dim_t *rangtab, dim_t *treetab)
{
  int ret;

  srand(0);
  if( sizeof(SCOTCH_Num) != sizeof(INTL) || sizeof(SCOTCH_Num) != sizeof(dim_t) )
    {
      SCOTCH_Num *perm_tmp = NULL, *rperm_tmp= NULL, *rangtab_tmp =NULL, *treetab_tmp = NULL, cblknbr_tmp;
      dim_t i;
      perm_tmp = (SCOTCH_Num *)malloc(sizeof(SCOTCH_Num)*n);
      rperm_tmp = (SCOTCH_Num *)malloc(sizeof(SCOTCH_Num)*n);

      if(rangtab != NULL)
	rangtab_tmp = (SCOTCH_Num *)malloc(sizeof(SCOTCH_Num)*(n+1));
      if(treetab != NULL)
	treetab_tmp = (SCOTCH_Num *)malloc(sizeof(SCOTCH_Num)*(n+1));

      /*if(perm != NULL)
	for(i=0;i<n;i++)
	  perm_tmp[i] = (SCOTCH_Num)perm[i];
      if(rperm != NULL)
	for(i=0;i<n;i++)
	rperm_tmp[i] = (SCOTCH_Num)rperm[i];*/


      ret = SCOTCH_graphOrder (graphdatptr, stratdatptr, perm_tmp, rperm_tmp, &cblknbr_tmp, rangtab_tmp, treetab_tmp);


      free(ia_tmp);
      free(ja_tmp);
      if(vwgt_tmp != NULL)
	{
	  free(vwgt_tmp);
	  vwgt_tmp = NULL;
	}
      if(ewgt_tmp != NULL)
	{
	  free(ewgt_tmp);
	  ewgt_tmp = NULL;
	}


      for(i=0;i<n;i++)
	{
	  perm[i] = (dim_t)perm_tmp[i];
	  rperm[i] = (dim_t)rperm_tmp[i];
	}
      free(perm_tmp);
      free(rperm_tmp);

      if(cblknbr != NULL)
	{

	  *cblknbr = (dim_t)cblknbr_tmp;
	}

      if(rangtab != NULL)
	{
	  for(i=0;i<cblknbr_tmp+1;i++)
	    rangtab[i] = (dim_t)rangtab_tmp[i];
	  free(rangtab_tmp);
	}
      if(treetab != NULL)
	{
	  for(i=0;i<cblknbr_tmp+1;i++)
	    treetab[i] = (dim_t)treetab_tmp[i];
	  free(treetab_tmp);
	}

    

    }
  else
    ret = SCOTCH_graphOrder (graphdatptr, stratdatptr, (SCOTCH_Num *)perm, (SCOTCH_Num *)rperm, 
		       (SCOTCH_Num *)cblknbr,  (SCOTCH_Num *)rangtab, (SCOTCH_Num *)treetab);

  return ret;
}

int SCOTCH_graphBuild_WRAPPER(SCOTCH_Graph *  grafdat, flag_t numflag, dim_t n, INTL *ia,  INTL *vwgt, dim_t *ja, INTL *ewgt)
{
  int ret;
  if( sizeof(SCOTCH_Num) != sizeof(INTL) || sizeof(SCOTCH_Num) != sizeof(dim_t) )
  {
      dim_t i;
      ia_tmp = (SCOTCH_Num *)malloc(sizeof(SCOTCH_Num)*(n+1));
      ja_tmp = (SCOTCH_Num *)malloc(sizeof(SCOTCH_Num)*(ia[n]-numflag));

      if(vwgt != NULL)
	vwgt_tmp = (SCOTCH_Num *)malloc(sizeof(SCOTCH_Num)*n);
      else
	vwgt_tmp = NULL;
	  
      if(ewgt != NULL)
	ewgt_tmp = (SCOTCH_Num *)malloc(sizeof(SCOTCH_Num)*(ia[n]-numflag));
      else
	ewgt_tmp = NULL;

      for(i=0;i<n+1;i++)
	ia_tmp[i] = (SCOTCH_Num)ia[i];

      for(i=0;i<ia[n]-numflag;i++)
	ja_tmp[i] = (SCOTCH_Num)ja[i];

      if(vwgt != NULL)
	for(i=0;i<n;i++)
	  vwgt_tmp[i] = (SCOTCH_Num)vwgt[i];

      if(ewgt != NULL)
	for(i=0;i<ia[n]-numflag;i++)
	  ewgt_tmp[i] = (SCOTCH_Num)ewgt[i];
      
      ret = SCOTCH_graphBuild (grafdat, (SCOTCH_Num)numflag, (SCOTCH_Num)n, ia_tmp, NULL, vwgt_tmp, NULL, (SCOTCH_Num)(ia_tmp[n]-numflag), ja_tmp, ewgt_tmp);


    }
  else
    {
      ret = SCOTCH_graphBuild (grafdat, (SCOTCH_Num)numflag, (SCOTCH_Num)n, (SCOTCH_Num *)ia, NULL, (SCOTCH_Num *)vwgt, 
			       NULL, (SCOTCH_Num)(ia[n]-numflag), (SCOTCH_Num *)ja, (SCOTCH_Num *)ewgt);
    }
  return ret;
}

int SCOTCH_graphPart_WRAPPER( dim_t n, SCOTCH_Graph *  grafdat, dim_t ndom,  SCOTCH_Strat *  grafstrat, mpi_t *node2dom)
{
  int ret;
    
  srand(0);
  if( sizeof(SCOTCH_Num) != sizeof(INTL) || sizeof(SCOTCH_Num) != sizeof(dim_t) )
    {
      SCOTCH_Num *node2dom_tmp=NULL;
      dim_t i;
      node2dom_tmp = (SCOTCH_Num *)malloc(sizeof(SCOTCH_Num)*n);
      
      ret = SCOTCH_graphPart(grafdat, (SCOTCH_Num) ndom, grafstrat, node2dom_tmp);
    
      free(ia_tmp);
      free(ja_tmp);
      if(vwgt_tmp != NULL)
	{
	  free(vwgt_tmp);
	  vwgt_tmp = NULL;
	}
      if(ewgt_tmp != NULL)
	{
	  free(ewgt_tmp);
	  ewgt_tmp = NULL;
	}


      for(i=0;i<n;i++)
	node2dom[i] = (mpi_t)node2dom_tmp[i];

      free(node2dom_tmp);
    }
  else
    {
      ret = SCOTCH_graphPart(grafdat, (SCOTCH_Num) ndom, grafstrat, (SCOTCH_Num *) node2dom);
    }
  return ret;
}

#ifdef SCOTCH_OVERLAP
int SCOTCH_graphPartOvl_WRAPPER( dim_t n, SCOTCH_Graph *  grafdat, dim_t ndom,  SCOTCH_Strat *  grafstrat, mpi_t *node2dom)
{
  int ret;
    
  srand(0);
  if( sizeof(SCOTCH_Num) != sizeof(INTL) || sizeof(SCOTCH_Num) != sizeof(dim_t) )
    {
      SCOTCH_Num *node2dom_tmp=NULL;
      dim_t i;
      node2dom_tmp = (SCOTCH_Num *)malloc(sizeof(SCOTCH_Num)*n);
      
      ret = SCOTCH_graphPartOvl(grafdat, (SCOTCH_Num) ndom, grafstrat, node2dom_tmp);
    
      free(ia_tmp);
      free(ja_tmp);
      if(vwgt_tmp != NULL)
	{
	  free(vwgt_tmp);
	  vwgt_tmp = NULL;
	}
      if(ewgt_tmp != NULL)
	{
	  free(ewgt_tmp);
	  ewgt_tmp = NULL;
	}


      for(i=0;i<n;i++)
	node2dom[i] = (mpi_t)node2dom_tmp[i];

      free(node2dom_tmp);
    }
  else
    {
      ret = SCOTCH_graphPartOvl(grafdat, (SCOTCH_Num) ndom, grafstrat, (SCOTCH_Num *) node2dom);
    }
  return ret;
}
#endif
#else

void METIS_NodeWND_WRAPPER(dim_t n, INTL *ia, dim_t *ja, INTL *vwgt, flag_t numflag, int *options, dim_t *rperm, dim_t *perm)
{
  int ntmp, numflagtmp;
  dim_t i;
  ntmp = (int)n;
  numflagtmp = (int)numflag; 

  srand(0);
  if( sizeof(idxtype) != sizeof(INTL) || sizeof(idxtype) != sizeof(dim_t) )
  {
      idxtype *ia_tmp = NULL, *ja_tmp= NULL, *vwgt_tmp=NULL, *perm_tmp=NULL, *rperm_tmp=NULL;;
      ia_tmp = (idxtype *)malloc(sizeof(idxtype)*(n+1));
      ja_tmp = (idxtype *)malloc(sizeof(idxtype)*(ia[n]-numflag));
      perm_tmp = (idxtype *)malloc(sizeof(idxtype)*n);
      rperm_tmp = (idxtype *)malloc(sizeof(idxtype)*n);

      if(vwgt != NULL)
	vwgt_tmp = (idxtype *)malloc(sizeof(idxtype)*n);
      
      for(i=0;i<n+1;i++)
	  ia_tmp[i] = (idxtype)ia[i];

      for(i=0;i<ia[n]-numflag;i++)
	{
	  ja_tmp[i] = (idxtype)ja[i];
	  /*perm_tmp[i] = (idxtype)perm[i];
	    rperm_tmp[i] = (idxtype)rperm[i];*/
	}
      
      if(vwgt != NULL)
	for(i=0;i<n;i++)
	  vwgt_tmp[i] = (idxtype)vwgt[i];

      /** Be careful perm and iperm are switched (compare to METIS notations that are wrong ) **/
      METIS_NodeWND(&ntmp, (idxtype *)ia_tmp, (idxtype *)ja_tmp, (idxtype *)vwgt_tmp, &numflagtmp, options, (idxtype *)rperm_tmp, (idxtype *)perm_tmp);

    

      for(i=0;i<n;i++)
	{
	  perm[i] = (dim_t)perm_tmp[i];
	  rperm[i] = (dim_t)rperm_tmp[i];
	}

      free(ia_tmp);
      free(ja_tmp);
      free(perm_tmp);
      free(rperm_tmp);
      if(vwgt != NULL)
	free(vwgt_tmp);
    }
  else
    {
      /** Becareful perm and iperm are switch (compare to METIS notations that are wrong ) **/
      METIS_NodeWND(&ntmp, (idxtype *)ia, (idxtype *)ja, (idxtype *)vwgt, &numflagtmp, options, (idxtype *)rperm, (idxtype *)perm);
    }
}


void METIS_NodeND_WRAPPER(dim_t n,  INTL *ia, dim_t *ja, flag_t numflag, int *options, dim_t *rperm, dim_t *perm)
{
  int ntmp, numflagtmp;
  dim_t i;

  srand(0);
  ntmp = (int)n;
  numflagtmp = (int)numflag; 
  if( sizeof(idxtype) != sizeof(INTL) || sizeof(idxtype) != sizeof(dim_t) )
    {
      idxtype *ia_tmp = NULL, *ja_tmp= NULL, *perm_tmp=NULL, *rperm_tmp=NULL;
      ia_tmp = (idxtype *)malloc(sizeof(idxtype)*(n+1));
      ja_tmp = (idxtype *)malloc(sizeof(idxtype)*(ia[n]-numflag));
      perm_tmp = (idxtype *)malloc(sizeof(idxtype)*n);
      rperm_tmp = (idxtype *)malloc(sizeof(idxtype)*n);

      for(i=0;i<n+1;i++)
	ia_tmp[i] = (idxtype)ia[i];

      for(i=0;i<ia[n]-numflag;i++)
	ja_tmp[i] = (idxtype)ja[i];

  
      /** Becareful perm and iperm are switch (compare to METIS notations that are wrong ) **/
      METIS_NodeND(&ntmp, ia_tmp, ja_tmp, &numflagtmp, options, rperm_tmp, perm_tmp);

      for(i=0;i<n;i++)
	{
	  perm[i] = (dim_t)perm_tmp[i];
	  rperm[i] = (dim_t)rperm_tmp[i];
	}

      free(ia_tmp);
      free(ja_tmp);
      free(perm_tmp);
      free(rperm_tmp);
    }
  else
    {
      /** Becareful perm and iperm are switch (compare to METIS notations that are wrong ) **/
      METIS_NodeND(&ntmp, (idxtype *)ia, (idxtype *)ja, &numflagtmp, options, (idxtype *)rperm, (idxtype *)perm);
    }
  
}


void METIS_PartGraphKway_WRAPPER(dim_t n, INTL *ia, dim_t *ja, INTL *vwgt, INTL *ewgt, flag_t wgtflag, flag_t numflag, int ndom, int *options, int *edgecutptr, mpi_t *node2dom)
{
  /** ndomptr doit etre un int car peut etre un dim_t ou un mpi_t **/

  int ntmp, numflagtmp, wgtflagtmp;
  dim_t i;
  ntmp = (int)n;
  numflagtmp = (int)numflag; 
  wgtflagtmp = (int)wgtflag; 
  
  srand(0);
  if( sizeof(idxtype) != sizeof(INTL) || sizeof(idxtype) != sizeof(dim_t) )
    {
      idxtype *ia_tmp = NULL, *ja_tmp= NULL, *vwgt_tmp=NULL, *ewgt_tmp=NULL, *node2dom_tmp=NULL;;
      ia_tmp = (idxtype *)malloc(sizeof(idxtype)*(n+1));
      ja_tmp = (idxtype *)malloc(sizeof(idxtype)*(ia[n]-numflag));
      node2dom_tmp = (idxtype *)malloc(sizeof(idxtype)*n);

      if(vwgt != NULL)
	vwgt_tmp = (idxtype *)malloc(sizeof(idxtype)*n);
      if(ewgt != NULL)
	ewgt_tmp = (idxtype *)malloc(sizeof(idxtype)*(ia[n]-numflag));
      
      for(i=0;i<n+1;i++)
	ia_tmp[i] = (idxtype)ia[i];
      for(i=0;i<ia[n]-numflag;i++)
	ja_tmp[i] = (idxtype)ja[i];
      
      if(vwgt != NULL)
	for(i=0;i<n;i++)
	  vwgt_tmp[i] = (idxtype)vwgt[i];
      if(ewgt != NULL)
	for(i=0;i<ia[n]-numflag;i++)
	  ewgt_tmp[i] = (idxtype)ewgt[i];

      METIS_PartGraphKway(&ntmp, ia_tmp, ja_tmp, vwgt_tmp, ewgt_tmp, &wgtflagtmp, &numflagtmp, &ndom, options, edgecutptr, node2dom_tmp);
      
      for(i=0;i<n;i++)
	node2dom[i] = (mpi_t)node2dom_tmp[i];

      free(ia_tmp);
      free(ja_tmp);
      free(node2dom_tmp);
      if(vwgt != NULL)
	free(vwgt_tmp);
      if(ewgt != NULL)
	free(ewgt_tmp);
    }
  else
    {
      METIS_PartGraphKway(&ntmp, (idxtype *)ia, (idxtype *)ja, (idxtype *)vwgt, (idxtype *)ewgt,  &wgtflagtmp, &numflagtmp, &ndom, options,  edgecutptr, (idxtype *)node2dom);
    }
}

void METIS_PartGraphRecursive_WRAPPER(dim_t n, INTL *ia, dim_t *ja, INTL *vwgt, INTL *ewgt, flag_t wgtflag, flag_t numflag, int ndom, int *options, int *edgecutptr, mpi_t *node2dom)
{
  /** ndomptr doit etre un int car peut etre un dim_t ou un mpi_t **/

  int ntmp, numflagtmp, wgtflagtmp;
  dim_t i;
  ntmp = (int)n;
  numflagtmp = (int)numflag; 
  wgtflagtmp = (int)wgtflag; 

  srand(0);
  if( sizeof(idxtype) != sizeof(INTL) || sizeof(idxtype) != sizeof(dim_t) )
    {
      idxtype *ia_tmp = NULL, *ja_tmp= NULL, *vwgt_tmp=NULL, *ewgt_tmp=NULL, *node2dom_tmp=NULL;;
      ia_tmp = (idxtype *)malloc(sizeof(idxtype)*(n+1));
      ja_tmp = (idxtype *)malloc(sizeof(idxtype)*(ia[n]-numflag));
      node2dom_tmp = (idxtype *)malloc(sizeof(idxtype)*n);

      if(vwgt != NULL)
	vwgt_tmp = (idxtype *)malloc(sizeof(idxtype)*n);
      if(ewgt != NULL)
	ewgt_tmp = (idxtype *)malloc(sizeof(idxtype)*(ia[n]-numflag));
      
      for(i=0;i<n+1;i++)
	ia_tmp[i] = (idxtype)ia[i];

      for(i=0;i<ia[n]-numflag;i++)
	ja_tmp[i] = (idxtype)ja[i];

      
      if(vwgt != NULL)
	for(i=0;i<n;i++)
	  vwgt_tmp[i] = (idxtype)vwgt[i];
      if(ewgt != NULL)
	for(i=0;i<ia[n]-numflag;i++)
	  ewgt_tmp[i] = (idxtype)ewgt[i];

      METIS_PartGraphRecursive(&ntmp, ia_tmp, ja_tmp, vwgt_tmp, ewgt_tmp, &wgtflagtmp, &numflagtmp, &ndom, options, edgecutptr, node2dom_tmp);

      free(ia_tmp);
      free(ja_tmp);
      for(i=0;i<n;i++)
	node2dom[i] = (mpi_t)node2dom_tmp[i];
      
      free(node2dom_tmp);
      if(vwgt != NULL)
	free(vwgt_tmp);
      if(ewgt != NULL)
	free(ewgt_tmp);
    }
  else
    {
      METIS_PartGraphRecursive(&ntmp, (idxtype *)ia, (idxtype *)ja, (idxtype *)vwgt, (idxtype *)ewgt,  &wgtflagtmp, &numflagtmp, &ndom, options,  edgecutptr, (idxtype *)node2dom);
    }
}


void METIS_PartGraphVKway_WRAPPER(dim_t n, INTL *ia, dim_t *ja, INTL *vwgt, INTL *ewgt, flag_t wgtflag, flag_t numflag, int ndom, int *options, int *edgecutptr, mpi_t *node2dom)
{
  /** ndomptr doit etre un int car peut etre un dim_t ou un mpi_t **/

  int ntmp, numflagtmp, wgtflagtmp;
  dim_t i;
  ntmp = (int)n;
  numflagtmp = (int)numflag; 
  wgtflagtmp = (int)wgtflag; 
  
  srand(0);
  if( sizeof(idxtype) != sizeof(INTL) || sizeof(idxtype) != sizeof(dim_t) )
    {
      idxtype *ia_tmp = NULL, *ja_tmp= NULL, *vwgt_tmp=NULL, *ewgt_tmp=NULL, *node2dom_tmp=NULL;;
      ia_tmp = (idxtype *)malloc(sizeof(idxtype)*(n+1));
      ja_tmp = (idxtype *)malloc(sizeof(idxtype)*(ia[n]-numflag));
      node2dom_tmp = (idxtype *)malloc(sizeof(idxtype)*n);

      if(vwgt != NULL)
	vwgt_tmp = (idxtype *)malloc(sizeof(idxtype)*n);
      if(ewgt != NULL)
	ewgt_tmp = (idxtype *)malloc(sizeof(idxtype)*(ia[n]-numflag));
      
      for(i=0;i<n+1;i++)
	ia_tmp[i] = (idxtype)ia[i];

      for(i=0;i<ia[n]-numflag;i++)
	ja_tmp[i] = (idxtype)ja[i];
      
      if(vwgt != NULL)
	for(i=0;i<n;i++)
	  vwgt_tmp[i] = (idxtype)vwgt[i];
      if(ewgt != NULL)
	for(i=0;i<ia[n]-numflag;i++)
	  ewgt_tmp[i] = (idxtype)ewgt[i];

      METIS_PartGraphVKway(&ntmp, ia_tmp, ja_tmp, vwgt_tmp, ewgt_tmp, &wgtflagtmp, &numflagtmp, &ndom, options, edgecutptr, node2dom_tmp);

      free(ia_tmp);
      free(ja_tmp);
      
      for(i=0;i<n;i++)
	node2dom[i] = (mpi_t)node2dom_tmp[i];
      free(node2dom_tmp);
      if(vwgt != NULL)
	free(vwgt_tmp);
      if(ewgt != NULL)
	free(ewgt_tmp);
    }
  else
    {
      METIS_PartGraphVKway(&ntmp, (idxtype *)ia, (idxtype *)ja, (idxtype *)vwgt, (idxtype *)ewgt,  &wgtflagtmp, &numflagtmp, &ndom, options,  edgecutptr, (idxtype *)node2dom);
    }
}


#endif

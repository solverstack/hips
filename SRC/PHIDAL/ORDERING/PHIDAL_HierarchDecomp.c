/* @authors P. HENON */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "phidal_ordering.h"

/** Uncomment this "#define" to dump matrix and structure that can be used by mPHIDAL (MATLAB version of PHIDAL) **/
/*#define MATLAB_OUTPUT    */






void PHIDAL_HierarchDecomp(flag_t verbose, flag_t numflag, dim_t n,  INTL *ia,  dim_t *ja,
			   int *mapp, int *mapptr,  dim_t ndom,  PhidalHID *BL, dim_t *perm, dim_t *iperm)
{
  /************************************************************* ********/
  /* This function compute the hierarchical interface decomposition of  */
  /* a graph partitioned in overlapping domain                          */
  /* ENTRY :                                                            */
  /* verbose: set to 1 to verbose mode                                  */
  /* numflag : 0 the graph is in C numbering; 1 the graph is in Fortran */
  /*numbering                                                           */
  /* n, ia, ja: the graph                                               */
  /* mapp, mapptr : the partition infos                                 */
  /* ndom : the number of domain                                        */
  /* perm, iperm: the permutation and inverse                           */
  /* permutation that you would like to keep in                         */
  /* the interior of subdomain as much as possible                      */
  /*                                                                    */          
  /* RETURN:                                                            */
  /* BL : the hierarchical decomposition structure                      */
  /* perm, iperm : the new permutation and inverse permutation that     */
  /* corresponds to the hierarchical connector ordering                 */
  /**********************************************************************/						
  dim_t i;
  int overlap;
  chrono_t t1, t2;


  /** Convert the matrix in C numbering (start from 0 instead of 1) **/
  if(numflag == 1)
    CSR_Fnum2Cnum(ja, ia, n);
  
  if(verbose >= 2)
    fprintfd(stdout, "Compute the Hierarchical Interface decomposition for %d domains \n", ndom);



  t1 = dwalltime();



  /*---------------------------------------------------------------------*
    |  Compute level and block structure of the matrix                     |
    *---------------------------------------------------------------------*/
  overlap = 1;
  
  setuplevels(mapp, mapptr, overlap, ndom, n, ia, ja, perm, iperm, BL);

  /*** FROM NOW THE PERM and IPERM VECTOR ARE LOST: WE STILL HAVE PERM2 
       UNTOUCHED UNTIL WE REORDER THE NEW IPERM TO MATCH 
       THE INITIAL IPERM AS MUCH AS POSSIBLE    ***/


  if(verbose >= 5)
    {
      fprintfd(stdout, "\n Hierarchical Interface Decomposition PHASE 1:  \n");
  
      HID_Info(stdout, BL);
    }

  /*----------------------------------------------------------------------*
   |    recompute the class and connectors                                |
   *----------------------------------------------------------------------*/
  setup_class(n, ia, ja, iperm, BL);
  fprintfd(stderr, "done \n");


  /*---------------------------------------------------------------------*
   |  Compute for each levels the block reordering                       |
   *---------------------------------------------------------------------*/
  block_elimination_ordering(ndom, BL , iperm);


#ifdef DENSE /* When used with PHIDAL_Perm2Overlapped */
             /* The sort is too costly in this code I have to fix it */
  /*---------------------------------------------------------------------*
   |      Reorder the interior of connector to match as much as possible |
   |    the initial ordering                                             |
   *---------------------------------------------------------------------*/
  /** We treat the interior domain separatly because their ordering
      should differ very little from the original ordering 
  and the quicksort is costly on all the interior domain **/
  perm2 = malloc(sizeof(int)*n);
  memcpy(perm2, perm, sizeof(int)*n);

  for(i=BL->block_levelindex[0];i<BL->block_levelindex[1];i++)
    {
      dim_t j;
      int offset;
      j =  BL->block_index[i];
      offset = perm2[j] - perm[j]; /** The original is a post-order of
				       the elimination tree so domain
				       > 0 will have their numbering 
				       shifted with an offset **/
      while(j <  BL->block_index[i+1] && perm[j]+offset == perm2[j])
	j++;
      quicksort_int( iperm, (LONG)j, (LONG)(BL->block_index[i+1]-1), perm2);
    }

  for(i=BL->block_levelindex[1];i<BL->nblock;i++)
    quicksort_int( iperm, (LONG)(BL->block_index[i]), (LONG)(BL->block_index[i+1]-1), perm2);
  free(perm2);
#endif


  t2 = dwalltime();


  t2 -= t1;
  if(verbose >= 2)
    fprintfd(stdout, "Time to compute the permutation : %g s \n",  t2);

  /*****************************/
  /** Compute the perm vector **/
  /*****************************/
  for(i=0;i<n;i++)
    perm[iperm[i]] = i; 

  /** @@ OIMBE ne pas mettre perm et iperm dans l'interface ?? **/
  /** seulement dans la structrure HID pour etre cache de
      l'utilisateur **/
  /*BL->perm = (int *)malloc(sizeof(int)*n);
  BL->iperm = (int *)malloc(sizeof(int)*n);
  memcpy(BL->perm, perm, sizeof(int)*n);
  memcpy(BL->iperm, iperm, sizeof(int)*n);*/

  /* dans hips.c*/
#ifdef DEBUG_M
  if(verbose >= 5) 
    { 
      fprintfd(stdout, "\n Hierarchical Interface Decomposition INFO \n"); 
  
      HID_Info(stdout, BL);   
    } 
#endif

  /*************************************************************/
  /* Add a connector in the interface to unconnected domain    */
  /* This is a patch for the parallel preconditioner that does */
  /* not work when a processor has only one connector level    */
  /*************************************************************/
  HID_PatchLevel(BL);

  if(numflag == 1)
    /** Reconvert the matrix in Fortran numbering (start from 0 instead of 1) **/
    CSR_Cnum2Fnum(ja, ia, n);

}


REAL HID_quality(PhidalHID *BL)
{
  dim_t i;
  REAL quality;

  quality = 0;

  /*for(i=0;i<BL->nblock;i++)*/
  for(i=BL->block_levelindex[1];i<BL->nblock;i++)
    quality += (BL->block_index[i+1]-BL->block_index[i])*(BL->block_keyindex[i+1]-BL->block_keyindex[i]);
  
  return quality;
}

void HID_Info(FILE *file, PhidalHID *BL)
{
  int *dom2nodenbr;
  int i, k, ind;
  int maxdom, mindom, avg;

  fprintf(file, "Total number of connectors = %d \n", BL->nblock);
  fprintf(stdout, "Total number of levels = %d \n", BL->nlevel);
  for(i = 0;i<BL->nlevel;i++)
    fprintf(file, "Level %d = %d Nodes    %d  Connectors \n", i, BL->block_index[BL->block_levelindex[i+1]]-BL->block_index[BL->block_levelindex[i]], BL->block_levelindex[i+1]-BL->block_levelindex[i]);
  fprintf(file, "\n");

  dom2nodenbr = (int *)calloc(BL->ndom, sizeof(int));
  for(k=0;k<BL->nblock;k++)
    {
      ind = BL->block_index[k+1]-BL->block_index[k];
      for(i=BL->block_keyindex[k];i<BL->block_keyindex[k+1];i++)
	dom2nodenbr[BL->block_key[i]] += ind;
    }
  
  avg = maxdom = 0;
  mindom = dom2nodenbr[0];
  for(i=0;i<BL->ndom;i++)
    {
      /*fprintfd(file, "DOMAIN %d = %d nodes \n",i, dom2nodenbr[i]);*/
      avg += dom2nodenbr[i];
      if(dom2nodenbr[i] > maxdom)
	maxdom = dom2nodenbr[i];
      if(dom2nodenbr[i] < mindom)
	mindom = dom2nodenbr[i];
    }
  avg /= BL->ndom;

  fprintf(file, "Max domain size = %d nodes \n", maxdom);
  fprintf(file, "Min domain size = %d nodes \n", mindom);
  fprintf(file, "Avg domain size= %d nodes \n\n", (int)avg);

  fprintf(file, "Overlap measure = %g \n", HID_quality(BL));

  free(dom2nodenbr);

}


void PhidalHID_Expand(PhidalHID *BL)
{
  /**************************************************/
  /* Expand the HID struct in number of dof instead */
  /* of number of node                              */
  /**************************************************/
  dim_t i;
  dim_t w;
  BL->n *= BL->dof;
  for(i=0;i<=BL->nblock;i++)
    BL->block_index[i] *= BL->dof;

  if(BL->block_domwgt != NULL)
    {
      w = BL->dof * BL->dof;
      for(i=0;i<BL->ndom;i++)
	BL->block_domwgt[i] *= w;
    }
     
  BL->dof = 1;
}


void HID_PatchLevel(PhidalHID *BL)
{
  int *flag;
  dim_t i, j, k;
  if(BL->nlevel == 1)
    return; /** Only one level nothing to do **/


  flag = (int *)malloc(sizeof(int)*BL->ndom);
  bzero(flag, sizeof(int)*BL->ndom);
  for(i=BL->block_levelindex[1];i<BL->nblock;i++)
    for(j=BL->block_keyindex[i];j<BL->block_keyindex[i+1];j++)
      flag[BL->block_key[j]] = 1;
  
  j=0;
  for(i=0;i<BL->ndom;i++)
    if(flag[i]==0)
      j++;
  
  if(j>0)
    {
      fprintferr(stderr, "There are %d domains without interface \n", j);
      /*fprintf(stderr, "J == %d \n", j);
      for(i=BL->block_keyindex[BL->nblock-1];i<BL->block_keyindex[BL->nblock];i++)
      fprintf(stderr, "%d \n", BL->block_key[i]);*/

      for(i=BL->block_keyindex[BL->nblock-1];i<BL->block_keyindex[BL->nblock];i++)
	flag[BL->block_key[i]] = 0;
      
      /** Put any unconnector domain in the last connector key **/
      BL->block_key = (int*)realloc(BL->block_key, sizeof(int)*(BL->block_keyindex[BL->nblock]+j));
      BL->block_keyindex[BL->nblock] += j;
      k = BL->block_keyindex[BL->nblock-1];
      for(i=0;i<BL->ndom;i++)
	if(flag[i] == 0)
	  BL->block_key[k++] = i;
#ifdef DEBUG_M
      assert(BL->block_keyindex[BL->nblock] == k);
#endif

      /*fprintf(stderr, "\n \n");
      for(i=BL->block_keyindex[BL->nblock-1];i<BL->block_keyindex[BL->nblock];i++)
      fprintf(stderr, "%d \n", BL->block_key[i]);*/
      
    }
   
    
  free(flag);
}

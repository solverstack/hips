/* @authors P. HENON */

#ifndef STRUCT_H
#define STRUCT_H
/*struct CTree {

  int sizeC;                
  int* Clist;               
  struct CTree *Left;       
  struct CTree *Right;      
  int    Bsiz;              
} ;

typedef struct CTree *CTptr;*/ 


typedef struct Graph_t *GraphPtr; 
typedef struct Graph_t {
  /* simplied version of metis graph == will be updated later to match metis
     definition so as to use nested dissections */

  int nvtxs, nedges;	  /* The # of vertices and edges in the graph */
  idxtype *xadj;	  /* Pointers to the locally stored vertices */
  idxtype *adj;	          /* Array storing the adjacency lists */
  idxtype *label;
} GType; 



#endif

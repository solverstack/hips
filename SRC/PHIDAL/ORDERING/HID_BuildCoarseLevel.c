/* @authors J. GAIDAMOUR, P. HENON */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "phidal_ordering.h"

void  merge_last_level(PhidalHID *BL);
void one_node_per_connector(flag_t job, PhidalHID *BL, dim_t *perm, dim_t *iperm);

/**** @@ OIMBE : donne rien sur les grilles.... ****/

void HID_BuildCoarseLevel(flag_t job, PhidalHID *BL, dim_t *perm, dim_t *iperm)
{
  /******************************************************************/
  /* This function build a coarser level in the HID structure       */
  /******************************************************************/
  assert(0);
  
  switch(job)
    {
    case 0:
      one_node_per_connector(0, BL, perm, iperm);
      break;

      /*** @@OIMBE : attention cas 1 et 2 a finir (nettoyer les connecteurs vides ****/
    case 1:
      one_node_per_connector(1, BL, perm, iperm);
      break;
    case 2:
      one_node_per_connector(2, BL, perm, iperm);
      break;



    case 3:
      merge_last_level(BL);
      break;
    default:
      fprintferr(stderr, "ERROR in HID_BuildCoarseLevel : job = %d is invalid \n", job);
      exit(-1);
    }
}

void one_node_per_connector(flag_t job, PhidalHID *BL, dim_t *perm, dim_t *iperm)
{
  /**********************************************/
  /* This function compute a connector composed */
  /* of one node per connector                  */
  /* job == 0 : one node per domain             */
  /* job == 1 : one node per interface connector*/
  /* job == 2 : one node per connector          */
  /**********************************************/
  int *tmp;
  int i, ind, ind2, j, k;
  
  
  /*for(i=0;i<BL->n;i++)
    fprintf(stderr,"  DE iperm[%d] = %d \n", i, iperm[i]);*/


  if(BL->nblock == BL->block_levelindex[1])
    return;
  ind = 0;
  switch(job)
    {
    case 0:
      fprintfd(stderr, "COARSE GRID : one node per domain \n");
      tmp = (int *)malloc(sizeof(int)*(BL->block_levelindex[1]));
      for(i=0;i<BL->block_levelindex[1];i++)
	tmp[ind++] = iperm[BL->block_index[i]];

      ind2 = 0;
      for(i=0;i<BL->block_levelindex[1];i++)
	for(j=BL->block_index[i]+1;j<BL->block_index[i+1];j++)
	  iperm[ind2++] = iperm[j];

      for(k=1;k<BL->nlevel;k++)
	for(i=BL->block_levelindex[k];i<BL->block_levelindex[k+1];i++)
	  for(j=BL->block_index[i];j<BL->block_index[i+1];j++)
	    iperm[ind2++] = iperm[j];
      
      break;
    case 1:
      fprintfd(stderr, "COARSE GRID : one node per interface connector  \n");
      tmp = (int *)malloc(sizeof(int)*(BL->nblock-BL->block_levelindex[1]));
      for(i=BL->block_levelindex[1];i<BL->nblock;i++)
	tmp[ind++] = iperm[BL->block_index[i]];

      ind2 = BL->block_index[BL->block_levelindex[1]];
      for(k=1;k<BL->nlevel;k++)
	for(i=BL->block_levelindex[k];i<BL->block_levelindex[k+1];i++)
	  for(j=BL->block_index[i]+1;j<BL->block_index[i+1];j++)
	    iperm[ind2++] = iperm[j];
  
      break;
    case 2:
      fprintfd(stderr, "COARSE GRID : one node per connector  \n");
      tmp = (int *)malloc(sizeof(int)*(BL->nblock));
      for(i=0;i<BL->nblock;i++)
	tmp[ind++] = iperm[BL->block_index[i]];

      ind2 = 0;
      for(k=0;k<BL->nlevel;k++)
	for(i=BL->block_levelindex[k];i<BL->block_levelindex[k+1];i++)
	  for(j=BL->block_index[i]+1;j<BL->block_index[i+1];j++)
	    iperm[ind2++] = iperm[j];

      break;
    default:
      assert(0);
    }
  
 
#ifdef DEBUG_M
  assert(ind2 + ind == BL->n);
#endif
  /*for(i=0;i<ind;i++)
    fprintf(stderr,"tmp[%d] = %d \n", i, tmp[i]);*/


  memcpy(iperm + ind2, tmp, sizeof(int)*ind);
  free(tmp);

  /*for(i=0;i<BL->n;i++)
    fprintf(stderr,"  DE iperm[%d] = %d \n", i, iperm[i]);*/

  /*** Rebuild the block_index block_levelindex, block_key and block_keyindex ***/
  BL->block_index = (int *)realloc(BL->block_index, sizeof(int)*(BL->nblock+1));
  ind = 0;
  switch(job)
    {
    case 0:
      for(i=0;i<BL->block_levelindex[1];i++)
	BL->block_index[i] -= ind++;
      for(i=BL->block_levelindex[1];i<=BL->nblock;i++)
	BL->block_index[i] -= ind;

      break;
    case 1:
      for(i=BL->block_levelindex[1];i<=BL->nblock;i++)
	BL->block_index[i] -= ind++;
      break;
    case 2:
      for(i=0;i<=BL->nblock;i++)
	BL->block_index[i] -= ind++;
      break;
    }
  
  BL->block_index[BL->nblock+1] = BL->n;

  BL->block_levelindex = (int *)realloc(BL->block_levelindex, sizeof(int)*(BL->nlevel+1));
  BL->block_levelindex[BL->nlevel+1] = BL->nblock+1;


  BL->block_keyindex = (int *)realloc(BL->block_keyindex, sizeof(int)*(BL->nblock+2));
  ind = BL->block_keyindex[BL->nblock];
  BL->block_keyindex[BL->nblock+1] = ind + BL->ndom;
  BL->block_key = (int *)realloc(BL->block_key, sizeof(int)*(ind+BL->ndom));
  for(i=0;i<BL->ndom;i++)
    BL->block_key[ind+i] = i;

  BL->nlevel++;
  BL->nblock++;

  /*** Eliminate null block ! ***/
#ifdef DEBUG_M
  for(i=0;i<BL->nblock;i++)
    assert(BL->block_index[i+1]-BL->block_index[i]>0);
  for(i=0;i<BL->nlevel;i++)
    assert(BL->block_levelindex[i+1]-BL->block_levelindex[i]>0);
#endif

#ifdef DEBUG_M
  /** Check the inverse permutation vector **/
  for(i=0;i<BL->n;i++)
    {
      assert(iperm[i] >= 0);
      assert(iperm[i] < BL->n);
    }

  bzero(perm, sizeof(int)*BL->n);
  for(i=0;i<BL->n;i++)
    perm[iperm[i]]++;

  k = 0;
  for(i=0;i<BL->n;i++)
    if(perm[i] != 1)
      k++;
  if(k>0)
    fprintfd(stderr, "perm array is not valid number of error =  %d \n", k);
  assert(k==0);

  
#endif

  /** Reset perm **/
  for(i=0;i<BL->n;i++)
    perm[iperm[i]] = i;
}



void merge_last_level(PhidalHID *BL)
{
  int i, length, l1;
  int *key, *key1, *key2, *keytmp;
  

  /*** Compute the union of the key of the last level ***/
  key = (int *)malloc(sizeof(int)*BL->ndom);
  key2 = (int *)malloc(sizeof(int)*BL->ndom);
  length = 0;
  for(i=BL->block_levelindex[BL->nlevel-1];i<BL->block_levelindex[BL->nlevel];i++)
    {
      key1 = BL->block_key + BL->block_keyindex[i];
      l1 = BL->block_keyindex[i+1] - BL->block_keyindex[i];

      UnionSet(key, length, key1, l1, key2, &length);
      keytmp = key;
      key = key2;
      key2 = keytmp;
    }

#ifdef DEBUG_M
  assert(length <= BL->ndom);
  assert(BL->block_levelindex[BL->nlevel] > BL->block_levelindex[BL->nlevel-1]);
#endif

  i = BL->block_levelindex[BL->nlevel-1]; 
  BL->block_index[i+1] = BL->block_index[BL->block_levelindex[BL->nlevel]];
  
  BL->block_keyindex[i+1] = BL->block_keyindex[i] + length;
  memcpy(BL->block_key + BL->block_keyindex[i], key, length*sizeof(int));

  BL->nblock = BL->block_levelindex[BL->nlevel-1]+1;
  BL->block_levelindex[BL->nlevel] = BL->block_levelindex[BL->nlevel-1]+1;

  free(key);
  free(key2);
}

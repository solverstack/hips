/* @authors J. GAIDAMOUR, P. HENON */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "phidal_ordering.h"

void Improve_Partition(flag_t verbose, dim_t tagnbr, INTL n, INTL *ig, dim_t *jg, dim_t **mapp, dim_t **mapptr, dim_t *ndom, PhidalHID *BL, dim_t *perm, dim_t *iperm)
{
  /**********************************************************************/
  /* This function recompute a new HID from the previous one            */
  /* Principle is to take a point in each connector of the last level : */
  /* these points are the cross point of a new domain partition         */
  /**********************************************************************/
  dim_t *nodelist;
  dim_t i, nodenbr, level;
  int fillopt[10];

#ifdef DEBUG_M
  assert(tagnbr > 0 && tagnbr <= n);
#endif



  fprintfd(stderr, "IMPROVE START \n");
  /*HID_Info(stdout, BL);*/

  level = BL->nlevel-1;
  for(;level >= 0;level--)
    {
      fprintfd(stderr, "level = %ld points nbr = %ld \n", (long)level, (long)(BL->nblock - BL->block_levelindex[level]));
      if(BL->block_levelindex[BL->nlevel] - BL->block_levelindex[level] >= (*ndom)/tagnbr)
	break;
      if(level > 0 && (BL->block_levelindex[BL->nlevel] - BL->block_levelindex[level-1] >= 2*(*ndom)/tagnbr) )
	break;
      
    }
  
  fprintfd(stderr, "level = %ld in Improve_Partition : we use %ld points \n", (long)level, (long)(BL->block_levelindex[BL->nlevel] - BL->block_levelindex[level]));
  
  nodelist = (dim_t *)malloc(sizeof(dim_t)*(BL->nblock-BL->block_levelindex[level]));
  nodenbr = 0;
  for(i=BL->block_levelindex[level];i<BL->nblock;i++)
    nodelist[nodenbr++] = iperm[BL->block_index[i]];
  


  /** APPEL FONCTION CONSTRUCTION DOMAIN X ***/


  free(nodelist);

  /*** Recompute the HID ***/
  PHIDAL_HierarchDecomp(verbose, 0, n, ig, jg, *mapp, *mapptr, *ndom, BL, perm, iperm);
  

  /*** Reorder the interior domain ****/
  CSR_Perm(n, ig, jg, NULL, perm);
  fillopt[0] = 0; /** 0 ND, 1 MD, 2 MF **/

  PHIDAL_MinimizeFill(fillopt, n, ig, jg, BL, iperm);
  for(i=0;i<n;i++)
    perm[iperm[i]] = i;
}

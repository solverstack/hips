/* @authors P. HENON */

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include <math.h>
#include "queue.h"
#include "phidal_ordering.h"

/** local functions **/
int get_interior_domainsDOWN_TOP(int ndom, dim_t *perm, dim_t cblknbr, int *cblktab, dim_t *treetab, int *mapp, int *mapptr);
int get_interior_domainsTOP_DOWN(int ndom, dim_t *perm, dim_t cblknbr, int *cblktab, dim_t *treetab, int *mapp, int *mapptr);


int get_interior_domains(int ndom, dim_t *perm, dim_t cblknbr, int *cblktab, dim_t *treetab, int *mapp, int *mapptr)
{
  /*return get_interior_domainsDOWN_TOP(ndom, perm, cblknbr, cblktab, treetab, mapp, mapptr);*/
  return get_interior_domainsTOP_DOWN(ndom, perm, cblknbr, cblktab, treetab, mapp, mapptr);
}

int get_interior_domainsDOWN_TOP(int ndom, dim_t *perm, dim_t cblknbr, int *cblktab, dim_t *treetab, int *mapp, int *mapptr)
{
  /**** NOTE : treetab must be ordered by a postfix traversal of the elimination tree ***/
  int i, k, father;
  int domind, ind, flag, layerflag;
  int *stsize;  /** subtree size tab **/
  int *nstab; /** Number of son for cblks **/
  int nlayer, old_nlayer;
  int *layer, *old_layer;
  int *layer2dom;
  REAL *Wlayer; 
  int *son_index;
  int *sontab;
  int *cblk2dom;

#ifdef DEBUG_M
  assert(cblknbr>0);
#endif
  if(ndom == 1) /** Nothing to do **/
    {
      memcpy(mapp, perm, sizeof(int)*cblktab[cblknbr]);
      mapptr[0] = 0;
      mapptr[1] = cblktab[cblknbr];
      return 0;
    }

#ifdef DEBUG_M
  for(i=0;i<cblknbr-1;i++) /** Post-ordering verification **/
    assert(treetab[i] >= i);
#endif
    
  /*** Compute the subtree size of each cblk ***/
  stsize = (int *)malloc(sizeof(int)*cblknbr);
  for(i=0;i<cblknbr;i++)
    stsize[i] = cblktab[i+1]-cblktab[i];

  for(i=0;i<cblknbr-1;i++)
    if(treetab[i]!=i) /** IF THIS SNODE IS NOT A ROOT **/
      stsize[ treetab[i] ] += stsize[i];
  
  /*** Compute the number of son of each cblk ***/
  nstab = (int *)malloc(sizeof(int)*cblknbr);
  bzero(nstab, sizeof(int)*cblknbr);
  for(i=0;i<cblknbr-1;i++)
    if(treetab[i] != i) /** IF THIS SNODE IS NOT A ROOT **/
      nstab[treetab[i]]++;

  ind = 0;
  for(i=0;i<cblknbr;i++)
    ind += nstab[i];

  /**********************************************/
  /*** Compute the son list of each supernode ***/
  /**********************************************/
  son_index = (int *)malloc(sizeof(int)*(cblknbr+1));
  sontab = (int *)malloc(sizeof(int)*ind);
  ind = 0;
  for(i=0;i<cblknbr;i++)
    {
      son_index[i] = ind;
      ind += nstab[i];
    }
  son_index[cblknbr] = ind;

  bzero(nstab, sizeof(int)*cblknbr);
  for(i=0;i<cblknbr-1;i++)
    {
      father = treetab[i];
      if(father != i) /** IF THIS SNODE IS NOT A ROOT **/
	{
	  sontab[son_index[father]+nstab[father]] = i;
	  nstab[father]++;
	}
    }

  /**********************************************************/
  /*  Compute the initial layer composed of all the leaves  */
  /**********************************************************/
  nlayer = 0;
  for(i=0;i<cblknbr;i++)
    if(nstab[i] == 0)
      nlayer++;
  
  if(nlayer < ndom)
    {
      fprintfd(stderr, "ERROR: elimination tree not broad enough for %d domains: maximum number of domains: %d \n", ndom, nlayer);
      return(-1);
    }

  layer = (int *)malloc(sizeof(int)*nlayer);
  layer2dom = (int *)malloc(sizeof(int)*nlayer);
  Wlayer = (REAL *)malloc(sizeof(REAL)*nlayer);

  nlayer = 0;
  for(i=0;i<cblknbr;i++)
    if(nstab[i] == 0)
      {
	layer[nlayer] = i;
	Wlayer[nlayer] = (REAL)stsize[i];
	nlayer++;
      }

  cblk2dom = (int *)malloc(sizeof(int)*cblknbr);
  for(i=0;i<cblknbr;i++)
    cblk2dom[i] = -1;

  
  layerflag = 1;
  old_layer = (int *)malloc(sizeof(int)*nlayer);
  while(layerflag == 1)
    {
      fprintfd(stderr, "Layer = %d supernodes \n", nlayer);

      vec_balanced_partition(ndom, nlayer, Wlayer, layer2dom);
      for(i=0;i<nlayer;i++)
	cblk2dom[layer[i]] = layer2dom[i];
      
      /***********************************/
      /* Compute the new layer           */
      /***********************************/
      bzero(nstab, sizeof(int)*cblknbr);

      layerflag = 0;

      for(i=0;i<nlayer;i++)
	{
	  father = treetab[layer[i]];
	  if(father != layer[i]) /** Not a root **/
	    {
#ifdef DEBUG_M
	      assert(nstab[father]>= 0);
	      assert(nstab[father] < son_index[father+1]-son_index[father]);
#endif
	      nstab[father]++;
	      if(nstab[father] == son_index[father+1]-son_index[father])
		{
		  flag = 1;
		  /*** Determine if all the sons of this cblk are in the same domain ***/
		  for(k=son_index[father]+1;k<son_index[father+1];k++)
		    if(cblk2dom[sontab[k]] != cblk2dom[sontab[k-1]])
		      {
			flag = 0;
			break;
		      }
		  if(flag == 0)
		    nstab[father]= -1;
		  else
		    layerflag = 1; /** We found at least a new element for the next layer **/
		}
	    }
	  else
	    nstab[father]= -1; /** Mark the roots as no possible father **/
	}

      /*** Mark all the fathers that are not in the new layer ***/
      for(i=0;i<nlayer;i++)
	{
	  father = treetab[layer[i]];
	  if(father != layer[i]) /** Not a root **/
	    if(nstab[father] != son_index[father+1]-son_index[father])
	      nstab[father] = -1;
	}
      
      old_nlayer = nlayer;
      memcpy(old_layer, layer, sizeof(int)*nlayer);
      nlayer = 0;
      for(i=0;i<old_nlayer;i++)
	{
	  father = treetab[old_layer[i]];
	  if(nstab[father] < 0)
	    {
	      Wlayer[nlayer] = (REAL)stsize[old_layer[i]];
	      layer[nlayer] = old_layer[i];
	      nlayer++;
	    }
	  else
	    {
	      nstab[father]--;
	      if(nstab[father] == 0)
		{
		  nstab[father] = -1;
		  Wlayer[nlayer] = (REAL)stsize[father];
		  layer[nlayer] = father;
		  nlayer++;
		}
	    }
	}
#ifdef DEBUG_M
      assert(nlayer >= ndom);
      assert(nlayer <= old_nlayer);
      /** test the post_ordering of the layer **/
      for(i=0;i<nlayer-1;i++)
	assert(layer[i]<layer[i+1]);
#endif
    }


  /*** Set the domain for all the subtrees rooted in a cblk in the last layer ***/
  for(i=layer[nlayer-1];i>0;i--)
    {
      domind = cblk2dom[i];
      if(domind >= 0)
	for(k=son_index[i];k<son_index[i+1];k++)
	  cblk2dom[sontab[k]] = domind;
    }

  
  
  /**** Construct mapp and mapptr ****/
  ind = 0;
  bzero(mapptr, sizeof(int)*(ndom+1));
  for(i=0;i<cblknbr;i++)
    {
      domind = cblk2dom[i];
      if(domind >= 0)
	{
	  memcpy(mapp+ind, perm+cblktab[i], sizeof(int)*(cblktab[i+1]-cblktab[i]));
	  mapptr[domind+1] += cblktab[i+1]-cblktab[i];
	  ind += cblktab[i+1]-cblktab[i];
	}
    }
  for(i=1;i<=ndom;i++)
    mapptr[i]+=mapptr[i-1];

  /*for(i=0;i<ndom;i++)
    fprintfd(stderr, "Domain %d = %d nodes \n", i, mapptr[i+1]-mapptr[i]);*/

#ifdef DEBUG_M
  assert(mapptr[ndom] == ind);
#endif

  free(stsize);
  free(Wlayer);
  free(layer2dom);
  free(old_layer);
  free(nstab);
  free(layer);
  free(son_index);
  free(sontab);
  free(cblk2dom);

  return 0;
}



#define LAYER_SPLIT_PRECISION 10.0  /** This is a percentage **/
#define IMBALANCE_TOL  200.0        /** This is the ratio between the greater interior domain and the smaller interior domain **/
#define OVERLAP_EXPO   0.7 /** This is the ratio to stop the partitioning when  overlap > total^ratio **/

int get_interior_domainsTOP_DOWN(int ndom, dim_t *perm, dim_t cblknbr, int *cblktab, dim_t *treetab, int *mapp, int *mapptr)
{
  /**** NOTE : treetab must be ordered by a postfix traversal of the elimination tree ***/
  int i, k, father;
  int domind, ind, flag, layerflag;
  int *stsize;  /** subtree size tab **/
  int *cblksize; /** Size of a cblk **/
  int *nstab; /** Number of son for cblks **/
  int nlayer, old_nlayer;
  int *layer, *old_layer;
  int *layer2dom;
  REAL *Wlayer; 
  int *son_index;
  int *sontab;
  int *cblk2dom;
  Queue heap;
  int overlap_size, overlap_max;


#ifdef DEBUG_M
  assert(cblknbr>0);
#endif
  if(ndom == 1) /** Nothing to do **/
    {
      memcpy(mapp, perm, sizeof(int)*cblktab[cblknbr]);
      mapptr[0] = 0;
      mapptr[1] = cblktab[cblknbr];
      return 0;
    }

  /** Scotch mark the root with -1 : we need a root to have its father equals to itself **/
  for(i=0;i<cblknbr;i++)
    if(treetab[i] < 0)
      treetab[i] = i;


#ifdef DEBUG_M
  for(i=0;i<cblknbr;i++) /** Post-ordering verification **/
    assert(treetab[i] >= i);
#endif
    
  /*** Compute the subtree size of each cblk ***/
  stsize = (int *)malloc(sizeof(int)*cblknbr);
  cblksize = (int *)malloc(sizeof(int)*cblknbr);
  for(i=0;i<cblknbr;i++)
    {
      cblksize[i] = cblktab[i+1]-cblktab[i];
      stsize[i] = cblksize[i];
    }

  for(i=0;i<cblknbr-1;i++)
    if(treetab[i]!=i) /** IF THIS SNODE IS NOT A ROOT **/
      stsize[ treetab[i] ] += stsize[i];
  
  /*** Compute the number of son of each cblk ***/
  nstab = (int *)malloc(sizeof(int)*cblknbr);
  bzero(nstab, sizeof(int)*cblknbr);
  for(i=0;i<cblknbr-1;i++)
    if(treetab[i] != i) /** IF THIS SNODE IS NOT A ROOT **/
      nstab[treetab[i]]++;

  ind = 0;
  for(i=0;i<cblknbr;i++)
    ind += nstab[i];

  /**********************************************/
  /*** Compute the son list of each supernode ***/
  /**********************************************/
  son_index = (int *)malloc(sizeof(int)*(cblknbr+1));
  sontab = (int *)malloc(sizeof(int)*ind);
  ind = 0;
  for(i=0;i<cblknbr;i++)
    {
      son_index[i] = ind;
      ind += nstab[i];
    }
  son_index[cblknbr] = ind;

  bzero(nstab, sizeof(int)*cblknbr);
  for(i=0;i<cblknbr-1;i++)
    {
      father = treetab[i];
      if(father != i) /** IF THIS SNODE IS NOT A ROOT **/
	{
	  sontab[son_index[father]+nstab[father]] = i;
	  nstab[father]++;
	}
    }

  /**********************************************************/
  /*  Compute the initial layer composed of all the roots   */
  /**********************************************************/
  nlayer = 0;
  for(i=0;i<cblknbr;i++)
    if(treetab[i] == i || treetab[i] == -1)
      nlayer++;
  
  layer = (int *)malloc(sizeof(int)*cblknbr);
  old_layer = (int *)malloc(sizeof(int)*cblknbr);
  layer2dom = (int *)malloc(sizeof(int)*cblknbr);
  Wlayer = (REAL *)malloc(sizeof(REAL)*cblknbr);
  queueInit(&heap, ndom*8);


  nlayer = 0;
  for(i=0;i<cblknbr;i++)
    if(treetab[i] == i)
      {
	layer[nlayer] = i;
	Wlayer[nlayer] = (REAL)stsize[i];
	nlayer++;
      }

  cblk2dom = (int *)malloc(sizeof(int)*cblknbr);
  for(i=0;i<cblknbr;i++)
    cblk2dom[i] = -1;


  /** Expand the layer until there is at least ndom supernode in it **/

  bzero(nstab, sizeof(int)*cblknbr);
  for(i=0;i<nlayer;i++)
    {
      ind = layer[i];
      nstab[ind] = 1;
      /** Sort the size of the cblk in this layer with a 10 percents precision **/
      queueAdd2(&heap, ind, (int)(-cblksize[ind]/LAYER_SPLIT_PRECISION), ((float)cblksize[ind])/stsize[ind]);
    }
  
  
  overlap_size = 0;
  while(nlayer < ndom && queueSize(&heap)>0)
    {
      father = queueGet(&heap);
      if(son_index[father+1]-son_index[father] == 0)
	continue; /** This is a leaf: can not split it **/
      nstab[father] = 0;
      overlap_size += cblksize[father];
      for(k=son_index[father];k<son_index[father+1];k++)
	{
	  ind = sontab[k];
	  nstab[ind]=1;
	  /** Sort the size of the cblk in this layer with a 10 percents precision **/
	  queueAdd2(&heap, ind, (int)(-cblksize[ind]/LAYER_SPLIT_PRECISION), ((float)cblksize[ind])/stsize[ind]); 
	}
      nlayer += son_index[father+1]-son_index[father] - 1;
    }

  if(nlayer < ndom)
    {
      fprintfd(stderr, "NOT ENOUGH LEAVES %d for %d domains \n", nlayer, ndom);
      return -1;
    }
  /** Form the layer **/
  ind = 0;
  for(i=0;i<cblknbr;i++)
    if(nstab[i] == 1)
      {
	layer[ind] = i;
	Wlayer[ind] = stsize[i];
	ind++;
      }

#ifdef DEBUG_M
  assert(ind == nlayer);
#endif


  /** Expand the layer until a tolerable load balance is reached **/
  overlap_max = 8*(int)pow((REAL)cblktab[cblknbr], OVERLAP_EXPO);
  fprintfd(stderr, "n = %d , ratio = %g \n", cblktab[cblknbr], pow((REAL)cblktab[cblknbr], 0.66));

  layerflag = 1;
  while(layerflag == 1)
    {
      int maxdom, mindom;


      vec_balanced_partition(ndom, nlayer, Wlayer, layer2dom);



      /** We use mapptr as a working buffer HERE **/
      bzero(mapptr, sizeof(int)*ndom);
      for(i=0;i<nlayer;i++)
	mapptr[layer2dom[i]] += stsize[layer[i]];
      maxdom = 0;
      mindom = cblktab[cblknbr];
      for(i=0;i<ndom;i++)
	{
	  if(mapptr[i]>maxdom)
	    maxdom = mapptr[i];
	  if(mapptr[i]<mindom)
	    mindom = mapptr[i];
	}
      /*fprintfd(stderr, "Layer = %d supernodes IMBALANCE %g , OVERLAP_SIZE %d MAX %d ratio %g \n", nlayer, ((REAL)maxdom)/mindom,overlap_size, overlap_max, 
	((REAL)overlap_size)/overlap_max);*/
      

      /****************************************************/
      /* Condition to stop the refinement                 */
      /****************************************************/
      if( (REAL)overlap_size > overlap_max)
	break;
      if( ((REAL)maxdom)/mindom <= IMBALANCE_TOL)
	break;
      if(queueSize(&heap) == 0)
	break;


      flag = 1;
      while(flag == 1 && queueSize(&heap)>0)
	{
	  father = queueGet(&heap);
	  if(son_index[father+1]-son_index[father] == 0)
	    continue; /** This is a leaf: can not split it **/
	  nstab[father] = 0;
	  overlap_size += cblksize[father];
	  for(k=son_index[father];k<son_index[father+1];k++)
	    {
	      ind = sontab[k];
	      nstab[ind]=1;
	      /** Sort the size of the cblk in this layer with a 10 percents precision **/
	      queueAdd2(&heap, ind, (int)(-cblksize[ind]/LAYER_SPLIT_PRECISION), ((float)cblksize[ind])/stsize[ind]); 
	    }
	  flag = 0;
	}

      if(flag == 1) /** No new supernodes can be added to the layer **/
	break;
      

      /** Make the new layer **/
      old_nlayer = nlayer;
      memcpy(old_layer, layer, sizeof(int)*nlayer);
      nlayer = 0;
      for(i=0;i<old_nlayer;i++)
	{
	  father = old_layer[i];
	  if(nstab[father] == 1)
	    {
	      layer[nlayer] = father;
	      Wlayer[nlayer] = (REAL)stsize[father];
	      nlayer++;
	    }
	  else
	    {
	      /** Replace this supernode by its sons **/
	      for(k=son_index[father];k<son_index[father+1];k++)
		{
#ifdef DEBUG_M
		  assert(nstab[sontab[k]] == 1);
#endif
		  layer[nlayer] = sontab[k];
		  Wlayer[nlayer] = (REAL)stsize[sontab[k]];
		  nlayer++;
		}
	    }
	}
#ifdef DEBUG_M
      assert(nlayer >= ndom);
      assert(nlayer >= old_nlayer);
      /** test the post_ordering of the layer **/
      for(i=0;i<nlayer-1;i++)
	assert(layer[i]<layer[i+1]);
#endif
    }


  /** Reinit cblk2dom **/
  for(i=0;i<cblknbr;i++)
    cblk2dom[i] = -1;
  
  for(i=0;i<nlayer;i++)
    cblk2dom[layer[i]] = layer2dom[i];

  /*** Set the domain for all the subtrees rooted in a cblk in the last layer ***/
  /*for(i=layer[nlayer-1];i>0;i--)*/
  for(i=cblknbr-1;i>0;i--)
    {
      domind = cblk2dom[i];
      if(domind >= 0)
	for(k=son_index[i];k<son_index[i+1];k++)
	  cblk2dom[sontab[k]] = domind;
    }

  
  
  /**** Construct mapp and mapptr ****/
  ind = 0;
  bzero(mapptr, sizeof(int)*(ndom+1));
  for(i=0;i<cblknbr;i++)
    {
      domind = cblk2dom[i];
      if(domind >= 0)
	{
	  memcpy(mapp+ind, perm+cblktab[i], sizeof(int)*(cblktab[i+1]-cblktab[i]));
	  mapptr[domind+1] += cblktab[i+1]-cblktab[i];
	  ind += cblktab[i+1]-cblktab[i];
	}
    }
  for(i=1;i<=ndom;i++)
    mapptr[i]+=mapptr[i-1];

  /*for(i=0;i<ndom;i++)
    fprintfd(stderr, "Domain %d = %d nodes \n", i, mapptr[i+1]-mapptr[i]);*/

#ifdef DEBUG_M
  assert(mapptr[ndom] == ind);
#endif

  queueExit(&heap);
  free(cblksize);
  free(stsize);
  free(Wlayer);
  free(layer2dom);
  free(old_layer);
  free(nstab);
  free(layer);
  free(son_index);
  free(sontab);
  free(cblk2dom);

  return 0;
}

/* @authors P. HENON */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <assert.h>

#include "phidal_ordering.h" 
#include "queue.h"
#define SEPARATOR -3

void computeNodeCoverGain(int nnode, int *nodeset, int *part, int *cn, int *mask, int n, INTL *ia, dim_t *ja, int *gain);
void computeMinVertexSeparator(int *part, int *cn, int n, INTL *ia, dim_t *ja,  int *mask);

void PHIDAL_Partition2OverlappedPartition(int numflag, int ndom, int n, INTL *ia, dim_t *ja, int *node2dom, int **mapp, int **mapptr)
{
  /*------------------------------------------------------------------------------------/
  /   This function tranforms a vertex based partition into an                          /
  /   edge based partition (i.e overlap on interface);                                  /
  /   The resulting partition is given in C numbering                                   /
  /   On entry:                                                                         /
  /   numflag    : =0 the matrix is given in C numbering                               /
  /                 =1 the matrix is given in Fortran numbering                         /
  /   n, ia, ja   : matrix pattern                                                      /
  /   node2dom    : node2dom[i] is the subdomain number that contains node i            /
  /                                                                                     /
  /   On return:                                                                        /
  /   mapp, mapptr: the edge based partition (overlapped partition)                     /
  /                                                                                     /
  /  NOTE: input matrix must be in C numbering (indices start from 0)                   /
  /------------------------------------------------------------------------------------*/
  
  dim_t i, j, k;
  int node, ind;
  int *mask, *mask1;
  int *cn;
  int *mapptr1;
  int *mapp1; 
  int *domflag;
  

  if(numflag == 1)
    {
      CSR_Fnum2Cnum(ja, ia, n);
      for(i=0;i<n;i++)
	node2dom[i]--;
    }


  mapp1 = (int *)malloc(sizeof(int)*n);
  mapptr1 = (int*)malloc(sizeof(int)*(ndom+1));
  bzero(mapptr1, sizeof(int)*(ndom+1));
  ind = 0;
  for(k=0;k<ndom;k++)
    for(i=0;i<n;i++)
      if(node2dom[i] == k)
	{
	  mapp1[ind++] = i;
	  mapptr1[k+1]++;
	}

#ifdef DEBUG_M
  assert(ind == n);
#endif

  for(k=0;k<ndom;k++)
    mapptr1[k+1] += mapptr1[k];
  
#ifdef DEBUG_M  
  assert(mapptr1[ndom] == n);
#endif




  mask = (int *)malloc(sizeof(int)*n);
  mask1 = (int *)malloc(sizeof(int)*n);

  cn = (int *)malloc(sizeof(int)*n);
  domflag = (int *)malloc(sizeof(int)*ndom);
  

  /*#ifdef DEBUG_M
  for(k=0;k<mapptr1[ndom];k++)
  node2dom[k] = -1;
  #endif*/

  /** setup node2dom such that node2dom[i] = domain of node i and 
      cn s.t. cn[i] = number of domain adjacent to node i**/

  /*for(k=0;k<ndom;k++)
    for(i=mapptr1[k];i<mapptr1[k+1];i++)
    node2dom[mapp1[i]] = k;*/


  
  /** Compute the domain connectivity degree of each node **/
  bzero(cn, sizeof(int)*n);
  for(k=0;k<ndom;k++)
    for(i=mapptr1[k];i<mapptr1[k+1];i++)
      {
	node = mapp1[i];
	bzero(domflag, sizeof(int)*ndom);
	domflag[node2dom[node]] = 1;
	for(j=ia[node];j<ia[node+1];j++)
	  domflag[node2dom[ja[j]]] = 1;
	
	for(j=0;j<ndom;j++)
	  cn[node] += domflag[j];
      }


  
#ifdef DEBUG_M
  for(k=0;k<n;k++)
    {
      assert(node2dom[k] >= 0);
      assert(cn[k] > 0);
    }
#endif


  /*-------------------------------/
  / Compute the vertex separator   /
  /-------------------------------*/
  computeMinVertexSeparator(node2dom, cn, n, ia, ja, mask);

  /*--------------------------------------------------------------------------------------------/
  / Construct the new edge-based partition using the vertex separator that has been computed    /
  /--------------------------------------------------------------------------------------------*/


  /* Get the new mapptr  */
  (*mapptr) = (int *)malloc(sizeof(int)*(ndom+1));
  ind = 0;
  for(k=0;k<ndom;k++)
    {
      (*mapptr)[k] = ind;
      ind += mapptr1[k+1] - mapptr1[k];
      bzero(mask1, sizeof(int)*n); /* mask1[node] == 1 means the node has already been counted as a new node in this domain **/
      for(i=mapptr1[k];i<mapptr1[k+1];i++)
	{
	  node =mapp1[i];
	  
	  for(j=ia[node];j<ia[node+1];j++)
	    {
	      if(node2dom[ja[j]] != k && mask[ja[j]] == SEPARATOR && mask1[ja[j]] == 0)
		{
		  mask1[ja[j]] = 1; 
		  ind++;
		}
	    }
	}
    }
  (*mapptr)[ndom] = ind;
  
  
  
  /** Allocation of the new mapp **/
  (*mapp) = (int *)malloc(sizeof(int)*(*mapptr)[ndom]);
  ind = 0;
  for(k=0;k<ndom;k++)
    {
      assert((*mapptr)[k] == ind);
      /* copy the node from the vertex based partition */
      memcpy((*mapp)+ind, mapp1 + mapptr1[k], sizeof(int)*( mapptr1[k+1] - mapptr1[k]));
      ind += mapptr1[k+1] - mapptr1[k];
      bzero(mask1, sizeof(int)*n); /* mask1[node] == 1 means the node has already been counted as a new node in this domain **/
      
      for(i=mapptr1[k];i<mapptr1[k+1];i++)
	{
	  node =mapp1[i];
	  
	  for(j=ia[node];j<ia[node+1];j++)
	    {
		if(node2dom[ja[j]] != k && mask[ja[j]] == SEPARATOR && mask1[ja[j]] == 0)
		  {
		    mask1[ja[j]] = 1; 
		    (*mapp)[ind] = ja[j];
		    ind++;
		  }
	    }
	}
    }
  assert((*mapptr)[ndom] == ind);

  

#ifdef DEBUG_M
  i = fix_unassigned_node(ndom, n, mapptr, mapp, ia, ja);
  if(i>0)
    {
      fprintfd(stderr, "ERROR there were %d unassigned node \n", i);
      exit(-1);
    }
  i =fix_false_interior_node(ndom, n, mapptr, mapp, ia, ja);
  if(i>0)
    {
      fprintfd(stderr, "ERROR there were %d false interior node \n", i);
      exit(-1);
    } 
#endif

  if(numflag == 1)
    {
      CSR_Cnum2Fnum(ja, ia, n);
      for(i=0;i<n;i++)
	node2dom[i]++;
    }


  free(domflag);
  free(cn);
  free(mapp1);
  free(mapptr1);
  free(mask);
  free(mask1);
}

void computeMinVertexSeparator(int *part, int *cn, int n, INTL *ia, dim_t *ja,  int *mask)
{
  dim_t i, j;
  int nnode;
  int node;
  int *gain, *nodeset;
  int prev_gain;
  Queue heap;
  float key;
  
  bzero(mask, sizeof(int)*n);

  /** Construct the set of node connected to more than one domain **/
  nnode = 0;
  for(i=0;i<n;i++)
    if(cn[i] > 1)
      nnode++;
  if(nnode == 0) /** Nothing to do **/
    return;
  nodeset = (int *)malloc(sizeof(int)*nnode);
  nnode = 0;
  for(i=0;i<n;i++)
    if(cn[i] > 1)
      nodeset[nnode++] = i;
  
  
  /** Compute the initial gain for each node if it is put in the vertex separator **/
  gain = (int *)malloc(sizeof(int)*n);
  computeNodeCoverGain(nnode, nodeset, part, cn, mask, n, ia, ja, gain);
  
  /** Put all the nodes in a heap ordered by ascending gain **/
  queueInit(&heap, 2*nnode);
  for(i=0;i<nnode;i++)
    queueAdd2(&heap, nodeset[i], -gain[nodeset[i]], cn[nodeset[i]]);

  /** Construct the separator with taking at first the node that have the largest gain **/
  /** each time a node is put in the vertex separator its neighbors have their gain updated 
      and re-Add in the heap **/
  while(queueSize(&heap) > 0)
    {
      queueGet2(&heap, &node, &key);
      prev_gain = (int)key;
#ifdef DEBUG_M
      assert(mask[node] != SEPARATOR);
#endif
      prev_gain = -prev_gain;
      if(prev_gain != gain[node])
	{
	  /** The gain of this node has changed since it has been entered in the heap **/
	  if(gain[node] > 0)
	    queueAdd2(&heap, node, -gain[node], (float)cn[node]);
	}
      else
	{
	  /** Add this node to the separator **/
	  mask[node] = SEPARATOR;
	  /** Update the gain of its neighbors **/
	  for(j=ia[node];j<ia[node+1];j++)
	    if(part[node] != part[ja[j]])
	      gain[ja[j]]--;
	}
    }

#ifdef DEBUG_M
  /** Check if all the node that was connected to more than one domain in the initial partition 
      are connected only to interior node or node in the vertex separator **/
  for(i=0;i<nnode;i++)
    {
      node = nodeset[i];

      if(mask[node] != SEPARATOR)
	{
	  assert(gain[node] <= 0);
	  for(j=ia[node];j<ia[node+1];j++)
	    assert(part[ja[j]] == part[node] || mask[ja[j]] == SEPARATOR);
	}
    }
#endif

  queueExit(&heap);
  free(gain);
  free(nodeset);
}


void computeNodeCoverGain(int nnode, int *nodeset, int *part, int *cn, int *mask, int n, INTL *ia, dim_t *ja, int *gain)
{
  int i, j, node;
  bzero(gain, sizeof(int)*n);
  for(i=0;i<nnode;i++)
    {
      node = nodeset[i];
      if(mask[node] != SEPARATOR)
	for(j=ia[node];j<ia[node+1];j++)
	  /** Test if the edge {node,ja[j]} is in the edge separator and not yet match by the vertex separator **/ 
	  if(mask[ja[j]] != SEPARATOR && part[ja[j]] != part[node])
	    {
#ifdef DEBUG_M
	      assert(cn[ja[j]] > 1);
#endif
	      gain[ja[j]]++;   
	    }
    }
}

/* @authors P. HENON */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <assert.h>
#include "phidal_common.h"

int fix_unassigned_node(int ndom, int n, int **mapptr, int **mapp, INTL * const ia, int const *ja)
{
  int i, j, k, index;
  int *tmp;
  int found_nbr;


  tmp = (int *)malloc(sizeof(int)* n);

  /** We compute the number of domain of each node **/
  bzero(tmp, sizeof(int)*n);
  for(i=0;i<(*mapptr)[ndom];i++)
    {
#ifdef DEBUG_M
      assert((*mapp)[i] >= 0 && (*mapp)[i] < n);
#endif
      tmp[(*mapp)[i]]++;
    }

  /** We count the nodes that aren't in any subdomain **/
  index = 0;
  for(i=0;i<n;i++)
    if(tmp[i] == 0)
      index++;

  found_nbr = index;


  /*if(found_nbr == 1)
    for(i=0;i<n;i++)
      if(tmp[i] == 0)
	{
	  fprintfd(stderr, "Node %d is unassigned \n", i);
	  for(j=ia[i];j<ia[i+1];j++)
	    fprintfd(stderr, "Neighbor %d nbconnexion %d \n", ja[j], tmp[ja[j]]);
	    }*/
	    


  /** Put the unassigned nodes in their related subdomains **/
  if(index > 0)
    {
      int *old_mapp;
      int *old_mapptr;
      int node;
      int * tmp2;

      /*fprintfd(stderr, "\n There are %d nodes not related to interior nodes \n", index);*/
      old_mapptr = (*mapptr);

      /** Count the extra length of mapp and update mapptr **/
      (*mapptr) = (int *)malloc(sizeof(int)*(ndom+1));

      tmp2 = (int*)malloc(sizeof(int)*n);
      index = 0;
      for(i=0;i<ndom;i++)
	{
	  memcpy(tmp2, tmp, sizeof(int)*n);
		  
	  (*mapptr)[i] = index;
	  for(j = old_mapptr[i];j<old_mapptr[i+1];j++)
	    {
	      node = (*mapp)[j];
	      index++;
	      for(k=ia[node] ; k < ia[node+1];k++)
		if(tmp2[ja[k]] == 0) /* unassigned node */
		  {
		    tmp2[ja[k]]++;
		    /*fprintfd(stderr, "Assign node  %d to domain %d \n", ja[k], i);*/
		    index++;
		  }
	    }
	}
      (*mapptr)[ndom] = index;

#ifdef DEBUG_M
      /*assert((*mapptr)[ndom] > old_mapptr[ndom]);*/
      if((*mapptr)[ndom] <= old_mapptr[ndom])
	{
	  fprintfd(stderr, "ERROR mapptr[ndom] = %d oldmapptr[ndom] = %d \n", (*mapptr)[ndom], old_mapptr[ndom]);
	  exit(-1);
	}
#endif
      
      /** update mapp **/

      old_mapp = (*mapp);
      (*mapp) = (int *)malloc(sizeof(int)*( (*mapptr)[ndom]));
      index = 0;

      for(i=0;i<ndom;i++)
	{
	  memcpy(tmp2, tmp, sizeof(int)*n);
	  
	  for(j = old_mapptr[i];j<old_mapptr[i+1];j++)
	    {
	      node = old_mapp[j];
	      (*mapp)[index] = node;
	      index++;
	      for(k=ia[node] ; k < ia[node+1];k++)
		if(tmp2[ja[k]] == 0) /* unassigned node */
		  {
		    tmp2[ja[k]]++;
		    (*mapp)[index] = ja[k];
		    index++;
		  }
	    }
	}


      free(old_mapptr);
      free(old_mapp);
      
#ifdef DEBUG_M
  assert(index == (*mapptr)[ndom]);
#endif
      free(tmp2);
    }

  free(tmp);

  return found_nbr;
}


int fix_false_interior_node(int ndom, int n, int **mapptr, int **mapp, INTL * const ia, int const *ja)
{
  int *tmp, *tmp1;
  int i, j, k, index;
  int found_nbr;



  tmp = (int *)malloc(sizeof(int)*n);

  /** We RE-compute the number of domain of each node **/
  bzero(tmp, sizeof(int)*n);
  for(i=0;i<(*mapptr)[ndom];i++)
    {
#ifdef DEBUG_M
      assert((*mapp)[i] >= 0 && (*mapp)[i] < n);
#endif
      tmp[(*mapp)[i]]++;
    }


  index = 0;
  tmp1 = (int *)malloc(sizeof(int)*n);
  for(k=0;k<ndom;k++)
    {
      bzero(tmp1, sizeof(int)*n);

      /** We mark the node in this subdomain **/
      for(i=(*mapptr)[k];i<(*mapptr)[k+1];i++)
	tmp1[(*mapp)[i]] = 1;



      /** Now we search any interior node that is connected to a node that 
	is NOT in this subdomain **/
      for(i=(*mapptr)[k];i<(*mapptr)[k+1];i++)
	{
	  int node;
	  node = (*mapp)[i];
	  if(tmp[node]==1) /** This is an interior node **/
	    {
	      for(j=ia[node];j<ia[node+1];j++)
		{
#ifdef DEBUG_M
		  assert(ja[j] >= 0 && ja[j] < n);
#endif
		  if(tmp1[ja[j]] == 0)
		    {
		      tmp1[ja[j]] = 1;
		      index++;
		      /*fprintfd(stderr, "Domain %d node mapp(%d) = %d is false nbdom %d \n", k, i, (*mapp)[i], tmp[(*mapp)[i]]);*/
		      break;
		    }
		}
	    }

	}
      
    }

  found_nbr = index;
  if(index > 0) /** We find some ! **/
    {
      int *old_mapp;
      int *old_mapptr;
      int node;
      
      /*fprintfd(stderr, "\n THERE ARE %d interior nodes related to more than 1 subdomain ! \n", index);*/
      
      old_mapptr = (*mapptr);
      
      /** Count the extra length of mapp and update mapptr **/
      (*mapptr) = (int *)malloc(sizeof(int)*(ndom+1));
      
      index = 0;
      for(k=0;k<ndom;k++)
	{
	  (*mapptr)[k] = index; /** OIMBE can be done in the previous loop **/
	  
	  bzero(tmp1, sizeof(int)*n);
	  
	  /** We mark the node in this subdomain **/
	  for(i= old_mapptr[k];i<old_mapptr[k+1];i++)
	    tmp1[(*mapp)[i]] = 1;
	  
	  /** Now we search any interior node that is connected to a node that 
	    is NOT in this subdomain **/
	  for(i=old_mapptr[k];i<old_mapptr[k+1];i++)
	    {
	      node = (*mapp)[i];
	      index++;
	      if(tmp[node]==1) /** This is an interior node **/
		{
		  for(j=ia[node];j<ia[node+1];j++)
		    if(tmp1[ja[j]] == 0)
		    {
		      tmp1[ja[j]] = 1;
		      index++;
		    }
		}
	    }
	}
      (*mapptr)[ndom] = index;
      
#ifdef DEBUG_M
      assert((*mapptr)[ndom] > old_mapptr[ndom]);
#endif
      
      /** update mapp **/
      
      old_mapp = (*mapp);
      (*mapp) = (int *)malloc(sizeof(int)*( (*mapptr)[ndom]));
      index = 0;
      for(k=0;k<ndom;k++)
	{
	  bzero(tmp1, sizeof(int)*n);
	  
	  /** We mark the node in this subdomain **/
	  for(i= old_mapptr[k];i<old_mapptr[k+1];i++)
	    tmp1[old_mapp[i]] = 1;
	  
	  /** Now we search any interior node that is connected to a node that 
	    is NOT in this subdomain **/
	  for(i=old_mapptr[k];i<old_mapptr[k+1];i++)
	    {
	      node = old_mapp[i];
	      (*mapp)[index] = node;
	      index++;
	      if(tmp[node]==1) /** This is an interior node **/
		{
		  for(j=ia[node];j<ia[node+1];j++)
		    if(tmp1[ja[j]] == 0)
		      {
			(*mapp)[index] = ja[j];
			tmp1[ja[j]] = 1;
			index++;
		      }
		}
	    }
	}
      
      free(old_mapptr);
      free(old_mapp);

#ifdef DEBUG_M
      assert(index == (*mapptr)[ndom]);
#endif
    }
  free(tmp1);
  free(tmp);

  return found_nbr;
}

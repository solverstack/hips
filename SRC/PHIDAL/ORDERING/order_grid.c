/* @authors P. HENON */

/* Copyright INRIA 2004
**
** This file is part of the Scotch distribution.
**
** The Scotch distribution is libre/free software; you can
** redistribute it and/or modify it under the terms of the
** GNU Lesser General Public License as published by the
** Free Software Foundation; either version 2.1 of the
** License, or (at your option) any later version.
**
** The Scotch distribution is distributed in the hope that
** it will be useful, but WITHOUT ANY WARRANTY; without even
** the implied warranty of MERCHANTABILITY or FITNESS FOR A
** PARTICULAR PURPOSE. See the GNU Lesser General Public
** License for more details.
**
** You should have received a copy of the GNU Lesser General
** Public License along with the Scotch distribution; if not,
** write to the Free Software Foundation, Inc.,
** 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.

*/
/************************************************************/
/**                                                        **/
/**   NAME       : order_grid.c                            **/
/**                                                        **/
/**   AUTHORS    : Francois PELLEGRINI                     **/
/**                                                        **/
/**   FUNCTION   : Part of a parallel direct block solver. **/
/**                This module builds orderings of grids   **/
/**                using nested dissection.                **/
/**                                                        **/
/**   DATES      : # Version 0.0  : from : 02 sep 1998     **/
/**                                 to     17 oct 2000     **/
/**                # Version 2.0  : from : 28 feb 2004     **/
/**                                 to     28 feb 2004     **/
/**                                                        **/
/**   NOTES      : # The numbering of grids is the same as **/
/**                  the one defined in graph_grid.c .     **/
/**                                                        **/
/** Modified by Pascal Henon to be part of HIPS release;   **/
/**                              06-06-2007                **/                                                
/************************************************************/

/*
**  The defines and includes.
*/

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include "redefine_functions.h"
#include "order_grid.h"
/***********************************/
/*                                 */
/* The ordering handling routines. */
/*                                 */
/***********************************/

/* This routine initializes the given
** ordering structure.
** It returns:
** - 0  : in all cases.
*/

int
orderInit (
Order * const               ordeptr)
{
  ordeptr->cblknbr = 0;
  ordeptr->rangtab = NULL;
  ordeptr->permtab = NULL;
  ordeptr->peritab = NULL;
  return (0);
}

/* This routine frees the contents
** of the given ordering.
** It returns:
** - VOID  : in all cases.
*/

void
orderExit (
Order * const               ordeptr)
{
  if (ordeptr->rangtab != NULL)
    free (ordeptr->rangtab);
  if (ordeptr->permtab != NULL)
    free (ordeptr->permtab);
  if (ordeptr->peritab != NULL)
    free (ordeptr->peritab);

#ifdef DEBUG_M
  memset (ordeptr, ~0, sizeof (Order));
#endif /* DEBUG_M */
}


/***************************/
/*                         */
/* Grid ordering routines. */
/*                         */
/***************************/

/* This routine builds a straight
** nested dissection ordering of the
** 2D grid whose characteristics are
** given as parameters.
** It returns:
** - 0  : in all cases.
*/

int
orderGrid2 (
Order * const               ordeptr,              /*+ Ordering to compute           +*/
const int                   xnbr,                 /*+ X dimension                   +*/
const int                   ynbr,                 /*+ Y dimension                   +*/
const int                   baseval,              /*+ Base value                    +*/
const int                   xlim,                 /*+ Minimum X dimension per block +*/
const int                   ylim)                 /*+ Minimum Y dimension per block +*/
{
  int *               permtnd;                    /* End of permutation array            */
  int *               permptr;                    /* Pointer to current permutation cell */
  int *               peritax;                    /* Based access for peritab            */
  int *               rangptr;                    /* Pointer to current range array cell */
  int                 ordenum;                    /* Number to set for current vertex    */
  int                 vertnbr;                    /* Number of vertices                  */
  int                 vertnum;                    /* Current vertex number               */

  vertnbr = xnbr * ynbr;
  if (((ordeptr->permtab = (int *) malloc ( vertnbr      * sizeof (int))) == NULL) ||
      ((ordeptr->peritab = (int *) malloc ( vertnbr      * sizeof (int))) == NULL) ||
      ((ordeptr->rangtab = (int *) malloc ((vertnbr + 1) * sizeof (int))) == NULL)) {
    fprintf(stderr, "orderGrid2: out of memory");
    orderExit  (ordeptr);
    orderInit  (ordeptr);
    return     (1);
  }

  ordeptr->cblknbr = 0;                             /* Reset number of column blocks */
  rangptr          = ordeptr->rangtab;
  rangptr[0]       = baseval;                       /* Set initial range index          */
  ordenum          = baseval;                       /* Set final numbering              */
  graphOrderGrid22 (&ordeptr->cblknbr, &rangptr, ordeptr->permtab, /* Perform recursion */
                    &ordenum, xnbr, xlim, ylim, 0, 0, xnbr, ynbr, 0);

#ifdef DEBUG_M
  if (ordenum != (baseval + vertnbr)) {
    fprintf(stderr, "orderGrid2: internal error");
    return     (1);
  }
#endif /* DEBUG_M */

  for (permptr = ordeptr->permtab, peritax = ordeptr->peritab - baseval, /* Build inverse permutation */
       permtnd = permptr + vertnbr, vertnum = baseval;
       permptr < permtnd;
       permptr ++, vertnum ++)
    peritax[*permptr] = vertnum;

#ifdef DEBUG_M
  if ((ordeptr->rangtab[0]                != baseval)           ||
      (ordeptr->rangtab[ordeptr->cblknbr] != baseval + vertnbr) ||
      (orderCheck (ordeptr) != 0)) {
    fprintf(stderr, "orderGrid2: invalid ordering");
  }
#endif /* DEBUG_M */

  ordeptr->rangtab = (int *) realloc (ordeptr->rangtab, (ordeptr->cblknbr + 1) * sizeof (int));

  return (0);
}

void
graphOrderGrid22 (
int * const                 cblkptr,              /*+ Number of column blocks, to be set  +*/
int ** const                rangptr,              /*+ Pointer to current range array cell +*/
int * const                 permtab,              /*+ Permutation array [based]           +*/
int * const                 ordeptr,              /*+ Pointer to current ordering value   +*/
const int                   xnbr,                 /*+ X Dimension                         +*/
const int                   xlim,                 /*+ Minimum X dimension per block       +*/
const int                   ylim,                 /*+ Minimum Y dimension per block       +*/
const int                   xmin,                 /*+ First X index for block             +*/
const int                   ymin,                 /*+ First Y index for block             +*/
const int                   xext,                 /*+ X dimension of block                +*/
const int                   yext,                 /*+ Y dimension of block                +*/
int                         flag)                 /*+ Direction flag                      +*/
{
  int                 xmed;                       /* Median X coordinate  */
  int                 ymed;                       /* Median Y coordinate  */
  int                 xnum;                       /* Current X coordinate */
  int                 ynum;                       /* Current Y coordinate */

  if ((xext <= xlim) &&                           /* If minimum block size reached */
      (yext <= ylim)) {
    switch (flag & 3) {                           /* Fill according to direction */
      case 0 :                                    /* X+, Y+                      */
        for (ynum = 0; ynum < yext; ynum ++) {    /* Fill block                  */
          for (xnum = 0; xnum < xext; xnum ++)
            permtab[(ymin + ynum) * xnbr + xmin + xnum] = (*ordeptr) ++;
        }
        break;
      case 1 :                                    /* X-, Y+     */
        for (ynum = 0; ynum < yext; ynum ++) {    /* Fill block */
          for (xnum = xext - 1; xnum >= 0; xnum --)
            permtab[(ymin + ynum) * xnbr + xmin + xnum] = (*ordeptr) ++;
        }
        break;
      case 2 :                                    /* X+, Y-       */
        for (ynum = yext - 1; ynum >= 0; ynum --) { /* Fill block */
          for (xnum = 0; xnum < xext; xnum ++)
            permtab[(ymin + ynum) * xnbr + xmin + xnum] = (*ordeptr) ++;
        }
        break;
      case 3 :                                    /* X-, Y-       */
        for (ynum = yext - 1; ynum >= 0; ynum --) { /* Fill block */
          for (xnum = xext - 1; xnum >= 0; xnum --)
            permtab[(ymin + ynum) * xnbr + xmin + xnum] = (*ordeptr) ++;
        }
        break;
    }

    (*rangptr)[1] = (*rangptr)[0] + xext * yext;  /* Set column block size */
    (*rangptr) ++;                                /* One more column block */
    (*cblkptr) ++;
  }
  else {                                          /* Nested dissection is applied */
    if (xext > yext) {                            /* Cut X dimension              */
      xmed = xext / 2;                            /* Get median coordinate        */

      graphOrderGrid22 (cblkptr, rangptr, permtab, ordeptr, /* Perform recursion */
                        xnbr, xlim, ylim,
                        xmin, ymin, xmed, yext, flag & ~1);
      if ((xext - xmed - 1) > 0)
        graphOrderGrid22 (cblkptr, rangptr, permtab, ordeptr,
                          xnbr, xlim, ylim,
                          xmin + xmed + 1, ymin, (xext - xmed - 1), yext, flag | 1);
      graphOrderGrid22 (cblkptr, rangptr, permtab, ordeptr, /* Fill separator */
                        xnbr, xext, yext,
                        xmin + xmed, ymin, 1, yext, flag);
    }
    else {                                        /* Cut Y dimension       */
      ymed = yext / 2;                            /* Get median coordinate */

      graphOrderGrid22 (cblkptr, rangptr, permtab, ordeptr, /* Perform recursion */
                        xnbr, xlim, ylim,
                        xmin, ymin, xext, ymed, flag & ~2);
      if ((yext - ymed - 1) > 0)
        graphOrderGrid22 (cblkptr, rangptr, permtab, ordeptr,
                          xnbr, xlim, ylim,
                          xmin, ymin + ymed + 1, xext, (yext - ymed - 1), flag | 2);
      graphOrderGrid22 (cblkptr, rangptr, permtab, ordeptr, /* Fill separator */
                        xnbr, xext, yext,
                        xmin, ymin + ymed, xext, 1, flag);
    }
  }
}

/* This routine builds a straight
** nested dissection ordering of the mesh
** defined as a 2D triangular grid with
** complete uppermost Y layers. These two
** layers are ordered last.
** It returns:
** - 0  : in all cases.
*/

int
orderGrid2C (
Order * const               ordeptr,              /*+ Ordering to compute           +*/
const int                   xnbr,                 /*+ X dimension                   +*/
const int                   ynbr,                 /*+ Y dimension                   +*/
const int                   baseval,              /*+ Base value                    +*/
const int                   xlim,                 /*+ Minimum X dimension per block +*/
const int                   ylim)                 /*+ Minimum Y dimension per block +*/
{
  int *               permtnd;                    /* End of permutation array            */
  int *               permptr;                    /* Pointer to current permutation cell */
  int *               peritax;                    /* Based access for peritab            */
  int *               rangptr;                    /* Pointer to current range array cell */
  int                 ordenum;                    /* Number to set for current vertex    */
  int                 vertnbr;                    /* Number of vertices                  */
  int                 vertnum;                    /* Current vertex number               */

  vertnbr = xnbr * ynbr;
  if (((ordeptr->permtab = (int *) malloc ( vertnbr      * sizeof (int))) == NULL) ||
      ((ordeptr->peritab = (int *) malloc ( vertnbr      * sizeof (int))) == NULL) ||
      ((ordeptr->rangtab = (int *) malloc ((vertnbr + 1) * sizeof (int))) == NULL)) {
    fprintf(stderr, "orderGrid2C: out of memory");
    orderExit  (ordeptr);
    orderInit  (ordeptr);
    return     (1);
  }

  ordeptr->cblknbr = 0;                             /* Reset number of column blocks */
  rangptr          = ordeptr->rangtab;
  rangptr[0]       = baseval;                       /* Set initial range index          */
  ordenum          = baseval;                       /* Set final numbering              */
  graphOrderGrid22 (&ordeptr->cblknbr, &rangptr, ordeptr->permtab, /* Perform recursion */
                    &ordenum, xnbr,
                    xlim, ylim, 0, 0, xnbr, ynbr - 2, 0);
  graphOrderGrid22 (&ordeptr->cblknbr, &rangptr, ordeptr->permtab, /* Order upper layers in ascending order */
                    &ordenum, xnbr,
                    xnbr, ynbr, 0, ynbr - 2, xnbr, 2, 0);

#ifdef DEBUG_M
  if (ordenum != (baseval + vertnbr)) {
    fprintf(stderr, "orderGrid2C: internal error");
    return     (1);
  }
#endif /* DEBUG_M */

  for (permptr = ordeptr->permtab, peritax = ordeptr->peritab - baseval, /* Build inverse permutation */
       permtnd = permptr + vertnbr, vertnum = baseval;
       permptr < permtnd;
       permptr ++, vertnum ++)
    peritax[*permptr] = vertnum;

#ifdef DEBUG_M
  if ((ordeptr->rangtab[0]                != baseval)           ||
      (ordeptr->rangtab[ordeptr->cblknbr] != baseval + vertnbr) ||
      (orderCheck (ordeptr) != 0)) {
    fprintf(stderr, "orderGrid2C: invalid ordering");
  }
#endif /* DEBUG_M */

  ordeptr->rangtab = (int *) realloc (ordeptr->rangtab, (ordeptr->cblknbr + 1) * sizeof (int));

  return (0);
}

/* This routine builds a straight
** nested dissection ordering of the
** 3D grid whose characteristics are
** given as parameters.
** It returns:
** - 0  : in all cases.
*/

int
orderGrid3 (
Order * const               ordeptr,              /*+ Ordering to compute           +*/
const int                   xnbr,                 /*+ X dimension                   +*/
const int                   ynbr,                 /*+ Y dimension                   +*/
const int                   znbr,                 /*+ Z dimension                   +*/
const int                   baseval,              /*+ Base value                    +*/
const int                   xlim,                 /*+ Minimum X dimension per block +*/
const int                   ylim,                 /*+ Minimum Y dimension per block +*/
const int                   zlim)                 /*+ Minimum Z dimension per block +*/
{
  int *               permtnd;                    /* End of permutation array            */
  int *               permptr;                    /* Pointer to current permutation cell */
  int *               peritax;                    /* Based access for peritab            */
  int *               rangptr;                    /* Pointer to current range array cell */
  int                 ordenum;                    /* Number to set for current vertex    */
  int                 vertnbr;                    /* Number of vertices                  */
  int                 vertnum;                    /* Current vertex number               */

  vertnbr = xnbr * ynbr * znbr;
  if (((ordeptr->permtab = (int *) malloc ( vertnbr      * sizeof (int))) == NULL) ||
      ((ordeptr->peritab = (int *) malloc ( vertnbr      * sizeof (int))) == NULL) ||
      ((ordeptr->rangtab = (int *) malloc ((vertnbr + 1) * sizeof (int))) == NULL)) {
    fprintf(stderr, "orderGrid3: out of memory");
    orderExit  (ordeptr);
    orderInit  (ordeptr);
    return     (1);
  }

  ordeptr->cblknbr = 0;                             /* Reset number of column blocks */
  rangptr          = ordeptr->rangtab;
  rangptr[0]       = baseval;                       /* Set initial range index          */
  ordenum          = baseval;                       /* Set final numbering              */
  graphOrderGrid32 (&ordeptr->cblknbr, &rangptr, ordeptr->permtab, /* Perform recursion */
                    &ordenum, xnbr, ynbr,
                    xlim, ylim, zlim, 0, 0, 0, xnbr, ynbr, znbr, 0);

#ifdef DEBUG_M
  if (ordenum != (baseval + vertnbr)) {
    fprintf(stderr, "orderGrid3: internal error");
    return     (1);
  }
#endif /* DEBUG_M */

  for (permptr = ordeptr->permtab, peritax = ordeptr->peritab - baseval, /* Build inverse permutation */
       permtnd = permptr + vertnbr, vertnum = baseval;
       permptr < permtnd;
       permptr ++, vertnum ++)
    peritax[*permptr] = vertnum;

#ifdef DEBUG_M
  if ((ordeptr->rangtab[0]                != baseval)           ||
      (ordeptr->rangtab[ordeptr->cblknbr] != baseval + vertnbr) ||
      (orderCheck (ordeptr) != 0)) {
    fprintf(stderr, "orderGrid3: invalid ordering");
  }
#endif /* DEBUG_M */

  ordeptr->rangtab = (int *)realloc(ordeptr->rangtab, (ordeptr->cblknbr + 1) * sizeof (int));

  return (0);
}

void
graphOrderGrid32 (
int * const                 cblkptr,              /*+ Number of column blocks, to be set  +*/
int ** const                rangptr,              /*+ Pointer to current range array cell +*/
int * const                 permtab,              /*+ Permutation array [based]           +*/
int * const                 ordeptr,              /*+ Pointer to current ordering value   +*/
const int                   xnbr,                 /*+ X Dimension                         +*/
const int                   ynbr,                 /*+ Y Dimension                         +*/
const int                   xlim,                 /*+ Minimum X dimension per block       +*/
const int                   ylim,                 /*+ Minimum Y dimension per block       +*/
const int                   zlim,                 /*+ Minimum Z dimension per block       +*/
const int                   xmin,                 /*+ First X index for block             +*/
const int                   ymin,                 /*+ First Y index for block             +*/
const int                   zmin,                 /*+ First Z index for block             +*/
const int                   xext,                 /*+ X dimension of block                +*/
const int                   yext,                 /*+ Y dimension of block                +*/
const int                   zext,                 /*+ Z dimension of block                +*/
int                         flag)                 /*+ Direction flag                      +*/
{
  int                 xmed;                       /* Median X coordinate  */
  int                 ymed;                       /* Median Y coordinate  */
  int                 zmed;                       /* Median Z coordinate  */
  int                 xnum;                       /* Current X coordinate */
  int                 ynum;                       /* Current Y coordinate */
  int                 znum;                       /* Current Z coordinate */

  if ((xext <= xlim) &&                           /* If minimum block size reached */
      (yext <= ylim) &&
      (zext <= zlim)) {
    switch (flag & 7) {                           /* Fill according to direction */
      case 0 :                                    /* X+, Y+, Z+                  */
        for (znum = 0; znum < zext; znum ++) {    /* Fill block                  */
          for (ynum = 0; ynum < yext; ynum ++) {
            for (xnum = 0; xnum < xext; xnum ++)
              permtab[((zmin + znum) * ynbr + (ymin + ynum)) * xnbr + xmin + xnum] = (*ordeptr) ++;
          }
        }
        break;
      case 1 :                                    /* X-, Y+, Z+ */
        for (znum = 0; znum < zext; znum ++) {    /* Fill block */
          for (ynum = 0; ynum < yext; ynum ++) {
            for (xnum = xext - 1; xnum >= 0; xnum --)
              permtab[((zmin + znum) * ynbr + (ymin + ynum)) * xnbr + xmin + xnum] = (*ordeptr) ++;
          }
        }
        break;
      case 2 :                                    /* X+, Y-, Z+ */
        for (znum = 0; znum < zext; znum ++) {    /* Fill block */
          for (ynum = yext - 1; ynum >= 0; ynum --) {
            for (xnum = 0; xnum < xext; xnum ++)
              permtab[((zmin + znum) * ynbr + (ymin + ynum)) * xnbr + xmin + xnum] = (*ordeptr) ++;
          }
        }
        break;
      case 3 :                                    /* X-, Y-, Z+ */
        for (znum = 0; znum < zext; znum ++) {    /* Fill block */
          for (ynum = yext - 1; ynum >= 0; ynum --) {
            for (xnum = xext - 1; xnum >= 0; xnum --)
              permtab[((zmin + znum) * ynbr + (ymin + ynum)) * xnbr + xmin + xnum] = (*ordeptr) ++;
          }
        }
        break;
      case 4 :                                    /* X+, Y+, Z-   */
        for (znum = zext - 1; znum >= 0; znum --) { /* Fill block */
          for (ynum = 0; ynum < yext; ynum ++) {
            for (xnum = 0; xnum < xext; xnum ++)
              permtab[((zmin + znum) * ynbr + (ymin + ynum)) * xnbr + xmin + xnum] = (*ordeptr) ++;
          }
        }
        break;
      case 5 :                                    /* X-, Y+, Z-   */
        for (znum = zext - 1; znum >= 0; znum --) { /* Fill block */
          for (ynum = 0; ynum < yext; ynum ++) {
            for (xnum = xext - 1; xnum >= 0; xnum --)
              permtab[((zmin + znum) * ynbr + (ymin + ynum)) * xnbr + xmin + xnum] = (*ordeptr) ++;
          }
        }
        break;
      case 6 :                                    /* X+, Y-, Z-   */
        for (znum = zext - 1; znum >= 0; znum --) { /* Fill block */
          for (ynum = yext - 1; ynum >= 0; ynum --) {
            for (xnum = 0; xnum < xext; xnum ++)
              permtab[((zmin + znum) * ynbr + (ymin + ynum)) * xnbr + xmin + xnum] = (*ordeptr) ++;
          }
        }
        break;
      case 7 :                                    /* X-, Y-, Z-   */
        for (znum = zext - 1; znum >= 0; znum --) { /* Fill block */
          for (ynum = yext - 1; ynum >= 0; ynum --) {
            for (xnum = xext - 1; xnum >= 0; xnum --)
              permtab[((zmin + znum) * ynbr + (ymin + ynum)) * xnbr + xmin + xnum] = (*ordeptr) ++;
          }
        }
        break;
    }
    (*rangptr)[1] = (*rangptr)[0] + xext * yext * zext; /* Set column block size */
    (*rangptr) ++;                                /* One more column block       */
    (*cblkptr) ++;
  }
  else {                                          /* Nested dissection is applied */
    if ((xext > yext) &&                          /* Cut X dimension              */
        (xext > zext)) {
      xmed = xext / 2;                            /* Get median coordinate */

      graphOrderGrid32 (cblkptr, rangptr, permtab, ordeptr, /* Perform recursion */
                        xnbr, ynbr, xlim, ylim, zlim,
                        xmin, ymin, zmin, xmed, yext, zext, flag & ~1);
      if ((xext - xmed - 1) > 0)
        graphOrderGrid32 (cblkptr, rangptr, permtab, ordeptr,
                          xnbr, ynbr, xlim, ylim, zlim,
                          xmin + xmed + 1, ymin, zmin, (xext - xmed - 1), yext, zext, flag | 1);
      graphOrderGrid32 (cblkptr, rangptr, permtab, ordeptr, /* Fill separator */
                        xnbr, ynbr, xext, yext, zext,
                        xmin + xmed, ymin, zmin, 1, yext, zext, flag);
    }
    else if (yext > zext) {                       /* Cut Y dimension       */
      ymed = yext / 2;                            /* Get median coordinate */

      graphOrderGrid32 (cblkptr, rangptr, permtab, ordeptr, /* Perform recursion */
                        xnbr, ynbr, xlim, ylim, zlim,
                        xmin, ymin, zmin, xext, ymed, zext, flag & ~2);
      if ((yext - ymed - 1) > 0)
        graphOrderGrid32 (cblkptr, rangptr, permtab, ordeptr,
                          xnbr, ynbr, xlim, ylim, zlim,
                          xmin, ymin + ymed + 1, zmin, xext, (yext - ymed - 1), zext, flag | 2);
      graphOrderGrid32 (cblkptr, rangptr, permtab, ordeptr, /* Fill separator */
                        xnbr, ynbr, xext, yext, zext,
                        xmin, ymin + ymed, zmin, xext, 1, zext, flag);
    }
    else {                                        /* Cut Z dimension       */
      zmed = zext / 2;                            /* Get median coordinate */

      graphOrderGrid32 (cblkptr, rangptr, permtab, ordeptr, /* Perform recursion */
                        xnbr, ynbr, xlim, ylim, zlim,
                        xmin, ymin, zmin, xext, yext, zmed, flag & ~4);
      if ((zext - zmed - 1) > 0)
        graphOrderGrid32 (cblkptr, rangptr, permtab, ordeptr,
                          xnbr, ynbr, xlim, ylim, zlim,
                          xmin, ymin, zmin + zmed + 1, xext, yext, (zext - zmed - 1), flag | 4);
      graphOrderGrid32 (cblkptr, rangptr, permtab, ordeptr, /* Fill separator */
                        xnbr, ynbr, xext, yext, zext,
                        xmin, ymin, zmin + zmed, xext, yext, 1, flag);
    }
  }
}

/* This routine builds a straight
** nested dissection ordering of the mesh
** defined as a 3D tetrahedral grid with
** complete uppermost Z layers. These two
** layers are ordered last.
** It returns:
** - 0  : in all cases.
*/

int
orderGrid3C (
Order * const               ordeptr,              /*+ Ordering to compute           +*/
const int                   xnbr,                 /*+ X dimension                   +*/
const int                   ynbr,                 /*+ Y dimension                   +*/
const int                   znbr,                 /*+ Z dimension                   +*/
const int                   baseval,              /*+ Base value                    +*/
const int                   xlim,                 /*+ Minimum X dimension per block +*/
const int                   ylim,                 /*+ Minimum Y dimension per block +*/
const int                   zlim)                 /*+ Minimum Z dimension per block +*/
{
  int *               permtnd;                    /* End of permutation array            */
  int *               permptr;                    /* Pointer to current permutation cell */
  int *               peritax;                    /* Based access for peritab            */
  int *               rangptr;                    /* Pointer to current range array cell */
  int                 ordenum;                    /* Number to set for current vertex    */
  int                 vertnbr;                    /* Number of vertices                  */
  int                 vertnum;                    /* Current vertex number               */

  vertnbr = xnbr * ynbr * znbr;
  if (((ordeptr->permtab = (int *) malloc ( vertnbr      * sizeof (int))) == NULL) ||
      ((ordeptr->peritab = (int *) malloc ( vertnbr      * sizeof (int))) == NULL) ||
      ((ordeptr->rangtab = (int *) malloc ((vertnbr + 1) * sizeof (int))) == NULL)) {
    fprintf(stderr, "orderGrid3C: out of memory");
    orderExit  (ordeptr);
    orderInit  (ordeptr);
    return     (1);
  }

  ordeptr->cblknbr = 0;                             /* Reset number of column blocks */
  rangptr          = ordeptr->rangtab;
  rangptr[0]       = baseval;                       /* Set initial range index          */
  ordenum          = baseval;                       /* Set final numbering              */
  graphOrderGrid32 (&ordeptr->cblknbr, &rangptr, ordeptr->permtab, /* Perform recursion */
                    &ordenum, xnbr, ynbr,
                    xlim, ylim, zlim, 0, 0, 0, xnbr, ynbr, znbr - 2, 0);
  graphOrderGrid32 (&ordeptr->cblknbr, &rangptr, ordeptr->permtab, /* Order upper layer */
                    &ordenum, xnbr, ynbr,
                    xnbr, ynbr, znbr, 0, 0, znbr - 2, xnbr, ynbr, 2, 0);
#ifdef DEBUG_M
  if (ordenum != (baseval + vertnbr)) {
    fprintf(stderr, "orderGrid3C: internal error");
    return     (1);
  }
#endif /* DEBUG_M */

  for (permptr = ordeptr->permtab, peritax = ordeptr->peritab - baseval, /* Build inverse permutation */
       permtnd = permptr + vertnbr, vertnum = baseval;
       permptr < permtnd;
       permptr ++, vertnum ++)
    peritax[*permptr] = vertnum;

#ifdef DEBUG_M
  if ((ordeptr->rangtab[0]                != baseval)           ||
      (ordeptr->rangtab[ordeptr->cblknbr] != baseval + vertnbr) ||
      (orderCheck (ordeptr) != 0)) {
    fprintf(stderr, "orderGrid3C: invalid ordering");
  }
#endif /* DEBUG_M */

  ordeptr->rangtab = (int *) realloc (ordeptr->rangtab, (ordeptr->cblknbr + 1) * sizeof (int));

  return (0);
}




int
orderCheck (
const Order * const  ordeptr)
{
  int                   baseval;                  /* Node base value            */
  int                   vnodnbr;                  /* Number of nodes            */
  int                   vnodmax;                  /* Maximum node value         */
  int                   vnodnum;                  /* Number of current node     */
  int                   rangnum;                  /* Current column block index */
  const int *   peritax;                  /* Based access to peritab    */
  const int *   permtax;                  /* Based access to permtab    */

  if (ordeptr->cblknbr < 0) {
    fprintf(stderr, "orderCheck: invalid nunber of column blocks");
    return     (1);
  }

  baseval = ordeptr->rangtab[0];                  /* Get base value */
  if (baseval < 0) {
    fprintf(stderr, "orderCheck: invalid vertex node base number");
    return     (1);
  }

  peritax = ordeptr->peritab - baseval;           /* Set based accesses */
  vnodmax = ordeptr->rangtab[ordeptr->cblknbr] - 1;
  vnodnbr = ordeptr->rangtab[ordeptr->cblknbr] - baseval;

  for (rangnum = 0; rangnum < ordeptr->cblknbr; rangnum ++) {
    if ((ordeptr->rangtab[rangnum] <  baseval) ||
        (ordeptr->rangtab[rangnum] >  vnodmax) ||
        (ordeptr->rangtab[rangnum] >= ordeptr->rangtab[rangnum + 1])) {
      fprintf(stderr, "orderCheck: invalid range array");
      return     (1);
    }
  }

  permtax = ordeptr->permtab - baseval;

  for (vnodnum = baseval;
       vnodnum <= vnodmax; vnodnum ++) {
    int                   vnodold;

    vnodold = peritax[vnodnum];
    if ((vnodold < baseval) ||
        (vnodold > vnodmax) ||
        (permtax[vnodold] != vnodnum)) {
      fprintf(stderr, "orderCheck: invalid permutation arrays");
      return     (1);
    }
  }

  return (0);
}

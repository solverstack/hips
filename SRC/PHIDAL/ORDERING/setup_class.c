/* @authors P. HENON */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <math.h>

#include "phidal_ordering.h"

#define EMPTY -1
#define NOT_SET -10







void merge_connector(CONNECTOR_str *Ctab, int m1, int m2, int classnum, int *class, int *degr, int nblock);

void find_local_minimum(CONNECTOR_str *Ctab, int connector_nbr, int *c_ia, int *c_ja, int *mask, int maskval, int *nc, int *clist);
int is_mergeable(int c1, int c2, CONNECTOR_str *Ctab, int *class);


void setup_class(int n, INTL *ia, dim_t *ja, int *rperm, PhidalHID *BL)
{
/************************************************************************************
* Function that computes the classes of connectors                                  *
*                                                                                   *
* On entry :                                                                        *
*   BL     : structure that  contains the connector, level and key defined by the   *
*            HID algorithm                                                          *
*   n, ia, ja : the adjacency matrix in CSR of the matrix                           *
*                                                                                   *
* On return :                                                                       *
*   BL      : the new class structure                                               *
*                                                                                   *
************************************************************************************/

  dim_t i, j;
  dim_t c;
  int nc, nclass, neigh=-1, count, new_nblock;
  int oldnblock;
  int mergeable=-1;
  int *class;
  int *degr;
  int *clist;

  CONNECTOR_str *Ctab;
  CONNECTOR_str *C;
  int ind1, ind2, lkey, lev;
  int old_nblock;

  /***** Allocate the connector structure *****/
  Ctab = (CONNECTOR_str *)malloc(sizeof(CONNECTOR_str)*BL->nblock);
  for(i=0;i<BL->nblock;i++)
    initCONNECTOR(Ctab + i);
  oldnblock = BL->nblock;

  /********************************************/
  /* Compute the connector structure          */
  /********************************************/

  build_connector_struct(n, ia, ja, rperm, BL, Ctab);


  /***********************************************************************************************
   *** Compute the classes of connectors : a class is the set of all locally minimum connectors***
   *** among the unclassed connectors according to the relation of key inclusion               ***
   **********************************************************************************************/
  
  /* if class(i) >= 0 then class(i) is the class number of connector i
     if class(i) < 0 
                class(i) == EMPTY this means the connector do not exist anymore: it has been merged with another connectors  
		class(i) == NOT_SET this means the connector do not have a class yet */
  class = (int *)malloc(sizeof(int)*BL->nblock);

  /*** Only interior nodes are in class 0 ***/
  /*** We trat them apart because we don't want any other connector to be in the class 0 
       (that could happen on odd graphs ****/
  for(i=0;i<BL->block_levelindex[1];i++)
    class[i] = 0;
  for(i=BL->block_levelindex[1];i<BL->nblock;i++)
    class[i] = NOT_SET;


  degr = (int *)malloc(sizeof(int)*BL->nblock);
  clist = (int *)malloc(sizeof(int)*BL->nblock);

  nclass = 1;
  count = BL->block_levelindex[1];
  new_nblock = BL->block_levelindex[1]; 
  
  while(count < BL->nblock)
    {
      int *c_ia;
      int *c_ja;
      bzero(degr, sizeof(int)*BL->nblock); /* degr(i) is the number of edge from connector i to a connector in this level */
      
      /********************************************/
      /* Compute the connector graph              */
      /********************************************/
      connector_graph(BL->nblock, Ctab, &c_ia, &c_ja, class, NOT_SET);


      /*********************************************/
      /* Find the set of all locally minimum       */
      /* Connectors                                */
      /*********************************************/
      find_local_minimum(Ctab, BL->nblock, c_ia, c_ja, class, NOT_SET, &nc, clist);

      /*find_MIS(1, BL->nblock, c_ia, c_ja, sizetab, class, NOT_SET, &nc, clist);*/
      /*find_MIS(0, BL->nblock, c_ia, c_ja, sizetab, class, NOT_SET, &nc, clist);*/

      free(c_ia);
      free(c_ja);


      /*** Compute the number of neighbors in the current level for each unclassed connector ***/
      for(i=0;i<nc;i++)
	{
	  c = clist[i];
	  C = Ctab + c;
#ifdef DEBUG_M
	  assert(class[c] == NOT_SET && degr[c] == 0);
#endif
	  class[c] = nclass;
	  new_nblock++;
	  for(j=0;j< C->numn;j++)
	    {
	      neigh = C->listn[j];
	      degr[neigh]++;
	    }
	  count++;
	}
      
      /***************************************************************************************/
      /**** Merge the connector that are related to only one connector in the current class **/
      /**** and such that the new merged connector is still a locally minimum               **/
      /***************************************************************************************/
      if(nclass > 1) /** Merge with interior node is not allowed **/
	{
	  int flag;
	  flag = 1;
	  while(flag == 1)
	    {
	      flag = 0;
	      for(i=0;i<BL->nblock;i++)
		if(class[i] == NOT_SET && degr[i] == 1 )
		  {
		    C = Ctab + i;
		    
		    for(j=0;j<C->numn;j++)
		      {
			neigh = C->listn[j];
#ifdef DEBUG_M
			assert(class[neigh] != EMPTY);
#endif  
			if(class[neigh] == nclass )
			  {
#ifdef DEBUG_M
			    assert(key_compare(Ctab[neigh].nkey, Ctab[neigh].key, Ctab[i].nkey, Ctab[i].key) == -1);
#endif
			    mergeable = is_mergeable(i, neigh, Ctab, class);
			    

			    /*fprintfv(5, stderr, "MERGEABLE == 0 FOR DEBUG \n");
			      mergeable = 0;*/

			    break;
			  }
			
		      }
		    
		    
		    if(mergeable == 1)
		      {
			/**************************************/
			/* Connector C(neigh) = C(neigh)+C(i) */
			/**************************************/
			flag = 1;
			merge_connector(Ctab, i, neigh, nclass, class, degr, BL->nblock);
			

			/* connector C(m1) does not exists anymore : mark it */
			class[i] = EMPTY;
			count = count+1;
		      }
		  }
	    }
	}

      /*fprintfv(5, stderr, "LEVEL %d \n", nclass);*/
      nclass++;

    }
  free(degr);
  free(clist);


  /*** Free fields of BL that has to be recomputed according to the new class and connectors ***/
  free(BL->block_index);
  free(BL->block_keyindex);
  free(BL->block_key);
  free(BL->block_levelindex);
 
  /*****************************************************************/
  /* Rebuild a PhidalHID corresponding to the new class definition */
  /*****************************************************************/
  BL->nlevel = nclass;
  BL->block_levelindex = (int *)malloc(sizeof(int)*(nclass+1));
  old_nblock = BL->nblock;
  BL->nblock = new_nblock;
  BL->block_index = (int *)malloc(sizeof(int)*(new_nblock+1));
  BL->block_keyindex = (int *)malloc(sizeof(int)*(new_nblock+1));
  
  /*** Compute size of block_key ***/
  count = 0;
  for(i=0;i<old_nblock;i++)
    if(class[i]>=0)
      count += Ctab[i].nkey;
  BL->block_key = (int *)malloc(sizeof(int)*count);
  

  /***  Fill rperm, block_keyindex, block_levelindex , block_key ****/
  ind1=0;
  ind2=0;
  lkey=0;
  for(lev=0;lev<nclass;lev++)
    {
      BL->block_levelindex[lev] = ind1;
      /* @OIMBE can be optimized in one traversal */
      for(i=0;i<old_nblock;i++)
	if(class[i] == lev)
	  {
	    C = Ctab + i;
	    BL->block_index[ind1] = ind2;
	    BL->block_keyindex[ind1] = lkey;
#ifdef DEBUG_M
	    assert(C->nkey > 0);
#endif

	    memcpy(rperm + ind2, C->vlist, C->nvert*sizeof(int));
	    memcpy(BL->block_key + lkey, C->key, C->nkey*sizeof(int));
	    
	    ind2 += C->nvert;
	    lkey += C->nkey;
	    ind1++;
	  }
    }
#ifdef DEBUG_M
  /*fprintfv(5, stderr, "IND1 %d new_nblock %d \n", ind1, new_nblock);*/
  assert(ind1 == new_nblock);
  assert(ind2 == n);
  assert(lkey == count);
#endif


  BL->block_levelindex[nclass] = new_nblock;
  BL->block_index[new_nblock] = n;
  BL->block_keyindex[new_nblock] = lkey;


  /*** Free memory used to construct the new connector graph ***/
  free(class);
  for(i=0;i<oldnblock;i++)
    cleanCONNECTOR(Ctab + i);
  free(Ctab);

}




void merge_connector(CONNECTOR_str *Ctab, int m1, int m2, int classnum, int *class, int *degr, int nblock)
{
  /**********************************
   * Merge connector:               *
   * Connector C(m2) = C(m2)+C(m1)  *
   *                                *
   *  And update degree             *
   **********************************/
  dim_t i;
  int neigh;
  int *mlist;
  int msize;

  CONNECTOR_str *C1;
  CONNECTOR_str *C2;
#ifdef DEBUG_M
  assert(m1 >= 0 && m1 <nblock);
  assert(m2 >= 0 && m2 <nblock);
#endif


  C1 = Ctab + m1;
  C2 = Ctab + m2;
  
  /*****************************/
  /** Merge the lists of node **/
  /*****************************/
  /** Sort the lists of node **/
  quicksort(C1->vlist, 0, C1->nvert-1);
  quicksort(C2->vlist, 0, C2->nvert-1);
  /** Merge the lists **/

  union_set(C1->vlist, C1->nvert, C2->vlist, C2->nvert, &mlist, &msize);
  if(C2->vlist != NULL)
    free(C2->vlist);
  C2->vlist = mlist;
  C2->nvert = msize;
  

  /*****************************/
  /** Merge the keys          **/
  /*****************************/
  /** Keys are already sorted **/
  /** Merge the keys **/
  union_set(C1->key, C1->nkey, C2->key, C2->nkey, &mlist, &msize);
  if(C2->key != NULL)
    free(C2->key);
  C2->key = mlist;
  C2->nkey = msize;

  /*********************************/
  /** Merge the list of neighbors **/
  /*********************************/
  if(class[m2] == classnum)
    {
      /** The degree of the neighbors will be recomputed after the merge **/
      for(i=0;i<C2->numn;i++)
	{
	  neigh = C2->listn[i];
	  degr[neigh]--;
	}
    }
   /** Sort the lists **/
  quicksort(C1->listn, 0, C1->numn-1);
  quicksort(C2->listn, 0, C2->numn-1);
  /** Merge the lists **/
  union_set(C1->listn, C1->numn, C2->listn, C2->numn, &mlist, &msize);
  if(set_delete(&mlist, &msize, m1) != 1)
    {
      fprintfd(stderr, "ERROR in setup_class : connectors: m1 %d   m2 %d\n", m1, m2);
      /*MPI_Abort(MPI_COMM_WORLD, -1);*/
      exit(-1);
    }
  if(set_delete(&mlist, &msize, m2) != 1)
    {
      fprintfd(stderr, "ERROR in setup_class : connectors: m1 %d   m2 %d\n", m1, m2);
      /*MPI_Abort(MPI_COMM_WORLD, -1);*/
      exit(-1);
    }
  if(C2->listn != NULL)
    free(C2->listn);
  C2->listn = mlist;
  C2->numn = msize;


  /***************************************************
   * Update m1 neighbor lists of adjacent connectors *
   ***************************************************/
  for(i=0;i<C1->numn;i++)
    {
      neigh = C1->listn[i];
      if(neigh != m2)
	{
	  if(set_delete(&(Ctab[neigh].listn), &(Ctab[neigh].numn), m1)!= 1)
	    {
	      fprintfd(stderr, "ERROR in setup_class : connectors: m1 %d   neigh %d\n", m1, neigh);
	      /*MPI_Abort(MPI_COMM_WORLD, -1);*/
	      exit(-1);
	    }

	  set_add(&(Ctab[neigh].listn), &(Ctab[neigh].numn), m2);
	}
    }

  /*** Recompute the degree of m2 in this class ***/
  if(class[m2] == NOT_SET)
    {
      degr[m2] = 0;
      for(i=0;i<C2->numn;i++)
	{
	  neigh = C2->listn[i];
	  if(class[neigh] == classnum)
	    degr[m2]++;
	}
    }
  if(class[m2] == classnum)
    {
      for(i=0;i<C2->numn;i++)
	{
	  neigh = C2->listn[i];
	  degr[neigh]++;
	}
    }

  


}




void find_local_minimum(CONNECTOR_str *Ctab, int connector_nbr, int *c_ia, int *c_ja, int *mask, int maskval, int *nc, int *clist)
{
  /*********************************************************************/
  /* This function find the set of all locally minimum connectors      */
  /* among the connector i such that mask(i) == maskval                */
  /*                                                                   */
  /* On entry:                                                         */
  /*                                                                   */
  /* On return:                                                        */
  /*  nc : number of locally minimum connector                         */
  /*  clist : list of the locally minimum connector                    */
  /*********************************************************************/
  dim_t i, j;
  int index;
  int neigh;
  int is_min = 0;
  
  index = 0;
  for(i=0;i<connector_nbr;i++)
    if(mask[i] == maskval)
      {
	/** This is an unclassed connector: find out if it is a local minimum **/
	is_min = 1;
	for(j=c_ia[i];j<c_ia[i+1];j++)
	  {
	    neigh = c_ja[j];
	    if(mask[neigh] != maskval)
	      continue;
#ifdef DEBUG_M
	    assert(fabs(key_compare(Ctab[i].nkey, Ctab[i].key, Ctab[neigh].nkey, Ctab[neigh].key)) <= 1.0);
	    
	    if(Ctab[i].nkey > Ctab[neigh].nkey)
	      assert(key_compare(Ctab[i].nkey, Ctab[i].key, Ctab[neigh].nkey, Ctab[neigh].key) > 0);
	    else
	      assert(key_compare(Ctab[i].nkey, Ctab[i].key, Ctab[neigh].nkey, Ctab[neigh].key) <= 0);
#endif

	    if(Ctab[i].nkey > Ctab[neigh].nkey  ||  key_compare(Ctab[i].nkey, Ctab[i].key, Ctab[neigh].nkey, Ctab[neigh].key) > 0)
	      {
		is_min = 0;
		break;
	      }
	  }
	    
	if(is_min == 1)
	  clist[index++] = i;

      }

  (*nc) = index;
  
}



int is_mergeable(int c1, int c2, CONNECTOR_str *Ctab, int *class)
{
  /**********************************************************/
  /* This function check if two connectors are mergeable    */
  /* A connector c1 is mergeable with c2 if                 */
  /* c1+c2 is still a local minimum                         */
  /* Return:                                                */
  /*         0 = not mergeable                              */
  /*         1 = mergeable                                  */
  /**********************************************************/
  
  CONNECTOR_str *C1, *C2;
  int kl;
  int *key;
  
  int i, neigh;
  
  C1 = Ctab + c1;
  C2 = Ctab + c2;

  /** Merge the keys **/
  union_set(C1->key, C1->nkey, C2->key, C2->nkey, &key, &kl);

#ifdef DEBUG_M
  assert(kl > C1->nkey || kl > C2->nkey);
  assert(kl >= C1->nkey && kl >= C2->nkey);
  assert(key != NULL);
#endif  
  
  /** Compare the key with the neighbor keys of c1 **/
  for(i=0;i<C1->numn;i++)
    {
      neigh = C1->listn[i];
      if( class[neigh] == NOT_SET && neigh != c1 && neigh != c2)
	if(key_compare(kl, key, Ctab[neigh].nkey, Ctab[neigh].key) != -1)
	  {
	    if(key != NULL)
	      free(key);
	    return 0; /** is_meageable == FALSE **/
	  }
    }

  /** Compare the key with the neighbor keys of c2 **/
  for(i=0;i<C2->numn;i++)
    {
      neigh = C2->listn[i];
      if( class[neigh] == NOT_SET && neigh != c1 && neigh != c2)
	if(key_compare(kl, key, Ctab[neigh].nkey, Ctab[neigh].key) != -1)
	  {
	    if(key != NULL)
	      free(key);
	    return 0; /** is_meageable == FALSE **/
	  }
    }


  if(key != NULL)
    free(key);

  
  return 1; /** is_mergeable == TRUE **/
}

/* @authors P. HENON */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <assert.h>
#include "phidal_ordering.h"

int expand_domain(dim_t ndom, dim_t n, dim_t **mapptr, dim_t **mapp, INTL * const ia, int const *ja);
int fix_overlap(dim_t ndom, dim_t n, dim_t **mapptr, dim_t **mapp, INTL * const ia, int const *ja);

void get_overlap(dim_t ndom, dim_t n, dim_t **mapptr, dim_t **mapp, INTL * const ia, dim_t * const ja)
{
  /***********************************************************************************/
  /* This function compute the overlap given the partition of interior domain        */
  /* in a graph                                                                      */
  /* On entry:                                                                       */
  /* ndom : number of domains                                                        */
  /* mapptr, mapp : pointer to the interior domains partition                        */
  /* n, ia, ja : the graph adjacency matrix (in CSR format)                          */
  /* On return:                                                                      */
  /* mapptr, mapp : the graph partition into domains with overlap                    */
  /*                                                                                 */
  /* NOTE: This function do not destroy the initial ordering of the interior domain  */
  /*   nodes. The overlapping nodes are added at this end of each domain node list   */
  /***********************************************************************************/
  dim_t i;

  /*** Expand iteratively the domains until all the graph nodes are asigned to a subdomain ****/
  do
    {

      i = expand_domain(ndom, n, mapptr, mapp, ia, ja);

      if(i>0)
	{
	  fprintfd(stderr, "There were %d nodes in the interface \n", i);
	}
    }
  while(i>0);

  
  /*fprintfd(stderr, "Fix false interior nodes \n");*/
  i = fix_overlap(ndom, n, mapptr, mapp, ia, ja);
  /*fprintfd(stderr, "Fix false interior nodes done \n");*/
  if(i>0)
    {
      fprintfd(stderr, "There were %d false interior node \n", i);
    }
  
}


int expand_domain(dim_t ndom, int n, dim_t **mapptr, dim_t **mapp, INTL * const ia, int const *ja)
{
  dim_t i, j, k, g, index;
  int *tmp;
  int found_nbr;
  int add_index;

  tmp = (int *)malloc(sizeof(int)* n);

  /** We compute the number of domain of each node **/
  bzero(tmp, sizeof(int)*n);
  for(i=0;i<(*mapptr)[ndom];i++)
    {
#ifdef DEBUG_M
      assert((*mapp)[i] >= 0 && (*mapp)[i] < n);
#endif
      tmp[(*mapp)[i]]++;
    }

  /** We count the nodes that aren't in any subdomain **/
  index = 0;
  for(i=0;i<n;i++)
    if(tmp[i] == 0)
      index++;

  found_nbr = index;

  /** Put the unassigned nodes in their related subdomains **/
  if(index > 0)
    {
      dim_t *old_mapp;
      dim_t *old_mapptr;
      dim_t node;
      int *tmp2;
      int *tmp3;

      /*fprintfv(5, stderr, "\n There are %d nodes not related to interior nodes \n", index);*/
      old_mapptr = (*mapptr);

      /** Count the extra length of mapp and update mapptr **/
      (*mapptr) = (dim_t *)malloc(sizeof(dim_t)*(ndom+1));

      tmp2 = (int*)malloc(sizeof(int)*n);
      tmp3 = (int*)malloc(sizeof(int)*n);

      old_mapp = (*mapp);
      memcpy(tmp2, tmp, sizeof(int)*n);
      index = 0;
      for(i=0;i<ndom;i++)
	{
	  (*mapptr)[i] = index;
	  g = 0;
	  for(j = old_mapptr[i];j<old_mapptr[i+1];j++)
	    {
	      node = old_mapp[j];
	      index++;
	      for(k=ia[node] ; k < ia[node+1];k++)
		{
		  if(tmp2[ja[k]] == 0) /* unassigned node */
		    {
		      tmp2[ja[k]]++;
		      tmp3[g++] = ja[k];
		      index++;
		    }
		}
	    }
	  for(j=0;j<g;j++)
	    tmp2[tmp3[j]] = tmp[tmp3[j]];
	}
      (*mapptr)[ndom] = index;


#ifdef DEBUG_M
      if((*mapptr)[ndom] <= old_mapptr[ndom])
	{
	  fprintfd(stderr, "ERROR mapptr[ndom] = %d oldmapptr[ndom] = %d \n", (*mapptr)[ndom], old_mapptr[ndom]);
	  exit(-1);
	}
      /*assert((*mapptr)[ndom] > old_mapptr[ndom]);*/
#endif

      /** update mapp **/
      old_mapp = (*mapp);
      (*mapp) = (int *)malloc(sizeof(int)*( (*mapptr)[ndom]));

      memcpy(tmp2, tmp, sizeof(int)*n);
      for(i=0;i<ndom;i++)
	{
	  index = (*mapptr)[i];
	  add_index = (*mapptr)[i+1]-1;
	  g = 0;
	  for(j = old_mapptr[i];j<old_mapptr[i+1];j++)
	    {
	      node = old_mapp[j];
	      (*mapp)[index] = node;
	      index++;
	      for(k=ia[node] ; k < ia[node+1];k++)
		if(tmp2[ja[k]] == 0) /* unassigned node */
		  {
		    tmp2[ja[k]]++;
		    tmp3[g++] = ja[k];
		    (*mapp)[add_index] = ja[k];
		    add_index--;
		  }
	    }

	  for(j=0;j<g;j++)
	    tmp2[tmp3[j]] = tmp[tmp3[j]];
#ifdef DEBUG_M
	  assert(add_index == index-1);
#endif
	}

      free(old_mapptr);
      free(old_mapp);
      free(tmp2);
      free(tmp3);
    }

  free(tmp);

  return found_nbr;
}

/*** OIMBE A REVOIR J'AI LIMPRESSION QUE L'ON AJOUTE PLUSIEUR FOIS DES NOEUDS INUTILES DANS L'OVERLAP ***/
int fix_overlap(dim_t ndom, dim_t n, dim_t **mapptr, dim_t **mapp, INTL * const ia, int const *ja)
{
  int *tmp, *tmp1;
  int i, j, k, index, add_index;
  int found_nbr;

  tmp = (int *)malloc(sizeof(int)*n);

  /** We RE-compute the number of domain of each node **/
  bzero(tmp, sizeof(int)*n);
  for(i=0;i<(*mapptr)[ndom];i++)
    {
#ifdef DEBUG_M
      assert((*mapp)[i] >= 0 && (*mapp)[i] < n);
#endif
      tmp[(*mapp)[i]]++;
    }


  index = 0;
  tmp1 = (int *)malloc(sizeof(int)*n);
  bzero(tmp1, sizeof(int)*n);
  for(k=0;k<ndom;k++)
    {
      /** We mark the node in this subdomain **/
      for(i=(*mapptr)[k];i<(*mapptr)[k+1];i++)
	tmp1[(*mapp)[i]] = k+1;

      /** Now we search any interior node that is connected to a node that 
	is NOT in this subdomain **/
      for(i=(*mapptr)[k];i<(*mapptr)[k+1];i++)
	{
	  int node;
	  node = (*mapp)[i];
	  if(tmp[node]==1) /** This is an interior node **/
	    {
	      for(j=ia[node];j<ia[node+1];j++)
		{
#ifdef DEBUG_M
		  assert(ja[j] >= 0 && ja[j] < n);
#endif
		  if(tmp1[ja[j]] != k+1)
		    {
		      tmp1[ja[j]] = k+1;
		      index++;
		      break;
		    }
		}
	    }

	}
      
    }

  found_nbr = index;
  if(index > 0) /** We find some ! **/
    {
      int *old_mapp;
      int *old_mapptr;
      int node;
      
      /*fprintfv(5, stderr, "\n THERE ARE %d interior nodes related to more than 1 subdomain ! \n", index);*/
      
      old_mapptr = (*mapptr);
      
      /** Count the extra length of mapp and update mapptr **/
      (*mapptr) = (int *)malloc(sizeof(int)*(ndom+1));
      
      index = 0;
      bzero(tmp1, sizeof(int)*n);
      for(k=0;k<ndom;k++)
	{
	  (*mapptr)[k] = index; /** OIMBE can be done in the previous loop **/
	  

	  
	  /** We mark the node in this subdomain **/
	  for(i= old_mapptr[k];i<old_mapptr[k+1];i++)
	    tmp1[(*mapp)[i]] = k+1;
	  
	  /** Now we search any interior node that is connected to a node that 
	    is NOT in this subdomain : we mark such node **/
	  for(i=old_mapptr[k];i<old_mapptr[k+1];i++)
	    {
	      node = (*mapp)[i];
	      index++;
	      if(tmp[node]==1) /** This is an interior node **/
		{
		  for(j=ia[node];j<ia[node+1];j++)
		    if(tmp1[ja[j]] != k+1)
		    {
		      tmp1[ja[j]] = k+1;
		      index++;
		    }
		}
	    }
	}
      (*mapptr)[ndom] = index;
      
#ifdef DEBUG_M
      assert((*mapptr)[ndom] > old_mapptr[ndom]);
#endif
      
      /** update mapp **/
      
      old_mapp = (*mapp);
      (*mapp) = (int *)malloc(sizeof(int)*( (*mapptr)[ndom]));

      bzero(tmp1, sizeof(int)*n);
      for(k=0;k<ndom;k++)
	{
	  /** We mark the node in this subdomain **/
	  for(i= old_mapptr[k];i<old_mapptr[k+1];i++)
	    tmp1[old_mapp[i]] = k+1;
	  
	  /** Now we search any interior node that is connected to a node that 
	    is NOT in this subdomain **/
	  index = (*mapptr)[k];
	  add_index = (*mapptr)[k+1]-1;
	  for(i=old_mapptr[k];i<old_mapptr[k+1];i++)
	    {
	      node = old_mapp[i];
	      (*mapp)[index] = node;
	      index++;
	      if(tmp[node]==1) /** This is an interior node **/
		{
		  for(j=ia[node];j<ia[node+1];j++)
		    if(tmp1[ja[j]] != k+1)
		      {
			tmp1[ja[j]] = k+1;
			(*mapp)[add_index] = ja[j];
			add_index--;
		      }
		}

	    }
#ifdef DEBUG_M
	  /*fprintfv(5, stderr, "add_index %d index %d \n", add_index, index);*/
	  assert(add_index == index-1);
#endif
	}
      
      free(old_mapptr);
      free(old_mapp);

    }
  free(tmp1);
  free(tmp);

  return found_nbr;
}

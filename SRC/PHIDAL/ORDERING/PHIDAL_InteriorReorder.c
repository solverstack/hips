/* @authors P. HENON */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>


#include "phidal_ordering.h"
#include "scotch_metis_wrapper.h"

/** uncomment this line to reorder only the connector of level 0
    (interior of subdomains) **/
#define ONLY_INTERIOR

void mybfs(int periphnbr, dim_t *periphlist, dim_t n, INTL *ia, dim_t *ja, flag_t *mask, flag_t maskval, dim_t *iperm, dim_t *layernbr, dim_t *layerptr);
void RCM(int maxit, int periphnbr, dim_t *periphlist, dim_t n, INTL *ia, dim_t *ja, flag_t *mask, flag_t maskval, dim_t *perm, dim_t *iperm);

void PHIDAL_MinimizeFill(int *fillopt, int n, INTL *ig, int *jg, PhidalHID *BL, dim_t *iperm)
{
  /***************************************************************************/
  /* This function reorder the matrix with Nested Dissection like algorithm  */
  /* in order to reduce fill-in. Only the interior of subdomain and */
  /* connector is reodered: the global connector partition is not
     affect by this ordering */
  /* This fonction use METIS are reorder inside the subdomain         */
  /* The reordering is applied to the iperm vector given in
     input */
  /* iperm is supposed to contain already a permutation vector (that
     can be identity (most usually it contains the permutation that
     was used to form the matrix (i.e. the PHIDAL one) */

  INTL i, j;
  int d;
  int offset;
  INTL ind;
  /* To store the sub graph of the interior node */
  dim_t ln;
  INTL *lig;
  dim_t *ljg; 

  dim_t *newperm;
  dim_t *newiperm;
  

  newperm = (int *)malloc(sizeof(int)*n);
  newiperm = (int *)malloc(sizeof(int)*n);


#ifdef DEBUG_M
  assert(ig[0] == 0);
  assert(BL->n == n);
#endif


#ifndef ONLY_INTERIOR
  for(d=0;d<BL->nblock;d++)
#else
  for(d=BL->block_levelindex[0];d<BL->block_levelindex[1];d++)
#endif
    {
      /*-------------------------------------------------------/
      / Extract the adjacency graph of the interior node of the/
      /  local domain in CSR format                            /
      /-------------------------------------------------------*/
      /* Count an upper bound to nnz */
      ind = 0;
      for(i=BL->block_index[d];i<BL->block_index[d+1];i++)
	/*ind += mat->nnzrow[i];*/
	ind += ig[i+1]-ig[i];

      if(ind == 0) /* We never know that could happen ! */
	continue; 

      ln = BL->block_index[d+1] - BL->block_index[d];
      lig = (INTL *)malloc(sizeof(INTL)*(ln+1)); 
      ljg = (dim_t *)malloc(sizeof(dim_t)*ind);

      offset = BL->block_index[d];
      /* We do not store the diagonale (if stored,  METIS crashes) */
      ind = 0;
      for(i=BL->block_index[d];i<BL->block_index[d+1];i++)
	{
	  lig[i-offset] = ind;
	  
	  /*for(j=0;j<mat->nnzrow[i];j++)*/
	  for(j=ig[i];j<ig[i+1];j++)
	    if(jg[j] >= BL->block_index[d] && jg[j] < BL->block_index[d+1] &&  jg[j]!= i) 
	      ljg[ind++] = jg[j] - offset;
	}
      lig[ BL->block_index[d+1]-offset ] = ind;
      
      if(lig[ln] == 0)
	{
	  for(i=BL->block_index[d];i<BL->block_index[d+1];i++)
	    {
	      newiperm[i] = i;
	      newperm[i] = i;
	    }
	}
      else
	{
#ifdef DEBUG_M
	  checkGraph(ln, lig, ljg, NULL, NULL, 0);
#endif
      
	  /*fprintfv(5, stderr, "GRAPH %d ln = %d NNZ = %d nnz = %d \n", d, ln, ig[n], lig[ln]);*/
	  {
	    /** Symmetrize the CSR matrix **/
	   
	    INTL *ib;
	    dim_t *jb;
	    ib = lig;
	    jb = ljg;
	
	    PHIDAL_SymmetrizeMatrix(0, 0, ln, ib, jb, NULL, &lig, &ljg, NULL);
	    
	    free(ib);
	    if(jb!= NULL)
	      free(jb);
	    
#ifdef DEBUG_M
	    checkGraph(ln, lig, ljg, NULL, NULL, 0);
#endif
	  }
	  
#ifdef SCOTCH_PART
	  {
	    SCOTCH_Graph        grafdat;
	    SCOTCH_Strat        stratdat;
	    char STRAT[400];
	    
	    SCOTCH_stratInit (&stratdat);
	    
	    
	    /** This strategy includes the ordering of the leaves i.e. the interior nodes **/
	    switch(fillopt[0])
	      {
	      case 0:
		 sprintf(STRAT, "n{sep=/(vert>120)?m{vert=100,low=h{pass=10},asc=f{bal=0.2}}|m{vert=100,low=h{pass=10},asc=f{bal=0.1}};,ole=f{cmin=0,cmax=10000,frat=0.00},ose=g}");
		 /*sprintf(STRAT, "n{sep=/(vert>120)?m{vert=100,low=h{pass=10},asc=f{bal=0.2}};,ole=f{cmin=0,cmax=10000,frat=0.0},ose=g}"); */
		break;
	      case 1:
		sprintf(STRAT, "d"); 
		break;
	      case 2:
		sprintf(STRAT, "f"); 
		break;
	      default:
		sprintf(STRAT, "n{sep=/(vert>120)?m{vert=100,low=h{pass=10},asc=f{bal=0.2}}|m{vert=100,low=h{pass=10},asc=f{bal=0.1}};,ole=f{cmin=0,cmax=10000,frat=0.00},ose=g}");
		/*sprintf(STRAT, "n{sep=/(vert>120)?m{vert=100,low=h{pass=10},asc=f{bal=0.2}};,ole=f{cmin=0,cmax=10000,frat=0.0},ose=g}"); */
	      }
	    
	    SCOTCH_stratGraphOrder (&stratdat, STRAT);
	    
	    SCOTCH_graphInit  (&grafdat);
	    /*SCOTCH_graphBuild (&grafdat, 0, ln, lig, NULL, NULL, NULL, lig[ln], ljg, NULL);*/
	    
	    SCOTCH_graphBuild_WRAPPER(&grafdat, 0, ln, lig, NULL, ljg, NULL);

	    	  
	    if(SCOTCH_graphCheck(&grafdat) != 0)
	      {
		fprintfd(stderr," Error in scotch graph check \n");
		exit(-1);
	      }
	    /*  fprintfv(5, stderr, "NDOM %ld \n", (long)ndom);*/
	    /** Do not free verttab and edgetab before SCOTCH_graphOrder **/
	    /*SCOTCH_graphOrder (&grafdat, &stratdat, newperm+offset, newiperm+offset, NULL, NULL, NULL);*/
	    SCOTCH_graphOrder_WRAPPER(&grafdat, &stratdat, ln, newperm+offset, newiperm+offset, NULL, NULL, NULL);
	    
	    SCOTCH_graphExit (&grafdat);
	    SCOTCH_stratExit (&stratdat);
	  }
	  
#else
	  {
	    int options[10];
	    int numflag;
	      
	    /*** Compute the reordering to minimize fill-in using METIS ***/
	    options[0] = 0;
	    numflag = 0;
	    
	    /** Do not forget that METIS defines perm as iperm and vice
		versa **/
	    /*metis_NodeND(&ln, lig, ljg, &numflag, options, newiperm+offset , newperm+offset);*/
	    METIS_NodeND_WRAPPER(ln, lig, ljg, numflag, options, newiperm+offset, newperm+offset);
	  }
#endif
	 
	  
	  /** Decal of the offset **/
	  for(i=BL->block_index[d];i<BL->block_index[d+1];i++)
	    newiperm[i] += offset;
	}

      free(lig);
      free(ljg);
    }

#ifndef ONLY_INTERIOR  
  for(i=0;i<ln;i++)
    newperm[newiperm[i]] = i;
#else
  for(i=0;i<BL->block_index[BL->block_levelindex[1]];i++)
    newperm[newiperm[i]] = i;

  /* complete the permutation vector */
  for(i=BL->block_index[BL->block_levelindex[1]];i<BL->n;i++)
    {
      newiperm[i] = i;
      newperm[i] = i;
    }
#endif

  /** Apply the permutation to the inputs perm vector **/

  /* Use newperm as temporary vector to compute the iperm vector */
  for(i=0;i<n;i++)
    newperm[i] = iperm[newiperm[i]];
  memcpy(iperm, newperm, sizeof(int)*n);
    
  /*for(i=0;i<n;i++)
    perm[iperm[i]] = i;*/
  
  free(newperm);
  free(newiperm);
}

void PHIDAL_InteriorReorderND(int halo, csptr mat, PhidalHID *BL, dim_t *perm, dim_t *iperm)
{
  /***************************************************************************/
  /* This function reorder the matrix with Nested Dissection like algorithm  */
  /* in order to reduce fill-in. Only the interior of subdomain and */
  /* connector is reodered: the global connector partition is not
     affect by this ordering */
  /* This fonction use METIS are reorder inside the subdomain         */
  /* The reordering is applied to the perm and iperm vector given in
     input */
  /* perm is supposed to contain already a permutation vector (that
     can be identity (most usually it contains the permutation that
     was used to form the matrix (i.e. the PHIDAL one) */

  dim_t i, j, d;
  int offset;
  int ind;

  INTL *ig, *ib;
  dim_t *jg, *jb,  n;

  dim_t *newperm;
  dim_t *newiperm;
  

  newperm = (dim_t *)malloc(sizeof(dim_t)*mat->n);
  newiperm = (dim_t *)malloc(sizeof(dim_t)*mat->n);


#ifdef DEBUG_M
  assert(BL->n == mat->n);
#endif


#ifndef ONLY_INTERIOR
  for(d=0;d<BL->nblock;d++)
#else
  for(d=BL->block_levelindex[0];d<BL->block_levelindex[1];d++)
#endif
    {
      /*-------------------------------------------------------/
      / Extract the adjacency graph of the interior node of the/
      /  local domain in CSR format                            /
      /-------------------------------------------------------*/
      /* Count an upper bound to nnz */
      ind = 0;
      for(i=BL->block_index[d];i<BL->block_index[d+1];i++)
	ind += mat->nnzrow[i];

      if(ind == 0) /* We never know that could happen ! */
	continue; 

      n = BL->block_index[d+1] - BL->block_index[d];
      ig = (INTL *)malloc(sizeof(INTL)*(n+1)); 
      jg = (dim_t *)malloc(sizeof(dim_t)*ind);

      offset = BL->block_index[d];
      if(halo == 0)
	{
	  ind = 0;
	  for(i=BL->block_index[d];i<BL->block_index[d+1];i++)
	    {
	      ig[i-offset] = ind;
	      for(j=0;j<mat->nnzrow[i];j++)
		if(mat->ja[i][j] >= BL->block_index[d] && mat->ja[i][j] < BL->block_index[d+1] &&  mat->ja[i][j] != i) /* We do not store the diagonale (if stored METIS crash !) */
		  jg[ind++] = mat->ja[i][j] - offset;
	    }
	  ig[ BL->block_index[d+1]-offset ] = ind;
	}
      else
	{
	  assert(0); /** Not implemented **/
	}

      
      /** Symmetrize the CSR matrix **/
      ib = ig;
      jb = jg;

      
      PHIDAL_SymmetrizeMatrix(0, 0, n, ib, jb, NULL, &ig, &jg, NULL);
      
      free(ib);
      if(jb!= NULL)
	free(jb);

#ifdef SCOTCH_PART
      {
	SCOTCH_Graph        grafdat;
	SCOTCH_Strat        stratdat;
	char STRAT[400];
   
	SCOTCH_stratInit (&stratdat);
	
	
	/** This strategy includes the ordering of the leaves i.e. the interior nodes **/
	sprintf(STRAT, "n{sep=/(vert>120)?m{vert=100,low=h{pass=10},asc=f{bal=0.2}};,ole=f{cmin=0,cmax=10000,frat=0.0},ose=g}"); 
	
	SCOTCH_stratGraphOrder (&stratdat, STRAT);
	
	SCOTCH_graphInit  (&grafdat);
	/*SCOTCH_graphBuild (&grafdat, 0, n, ig, NULL, NULL, NULL, ig[n], jg, NULL);*/
	SCOTCH_graphBuild_WRAPPER(&grafdat, 0, n, ig,  NULL, jg, NULL);

	/*  fprintfv(5, stderr, "NDOM %ld \n", (long)ndom);*/
	/** Do not free verttab and edgetab before SCOTCH_graphOrder **/
	/*SCOTCH_graphOrder (&grafdat, &stratdat, newperm+offset, newiperm+offset, NULL, NULL, NULL);*/
	SCOTCH_graphOrder_WRAPPER(&grafdat, &stratdat, n, newperm+offset, newiperm+offset, NULL, NULL, NULL);
	
	SCOTCH_graphExit (&grafdat);
	SCOTCH_stratExit (&stratdat);
      }
      
#else
      {
	int options[10];
	int numflag;
	/*** Compute the reordering to minimize fill-in using METIS ***/
	options[0] = 0;
	numflag = 0;
	
	/** Do not forget that METIS defines perm as iperm and vice
	    versa **/
	/* metis_NodeND(&n, ig, jg, &numflag, options, newiperm+offset , newperm+offset);*/
	METIS_NodeND_WRAPPER(n, ig, jg, numflag, options, newiperm+offset , newperm+offset);
      }
#endif
      free(ig);
      free(jg);
      
      /** Decal of the offset **/
      for(i=BL->block_index[d];i<BL->block_index[d+1];i++)
	newiperm[i] += offset;
    }

#ifndef ONLY_INTERIOR  
  for(i=0;i<mat->n;i++)
    newperm[newiperm[i]] = i;
#else
  for(i=0;i<BL->block_index[BL->block_levelindex[1]];i++)
    newperm[newiperm[i]] = i;

  /* complete the permutation vector */
  for(i=BL->block_index[BL->block_levelindex[1]];i<BL->n;i++)
    {
      newiperm[i] = i;
      newperm[i] = i;
    }
#endif

  /*** newiperm[i] is the new rank of row i in the permuted matrix ***/
  /*** unknown i in the permuted matrix was perm[i] in the unpermuted matrix ***/
  CS_Perm(mat, newperm);

  
  /** Apply the permutation to the inputs perm vector **/

  /* Use perm as temporary vector to compute the iperm vector */
  for(i=0;i<mat->n;i++)
    perm[i] = iperm[newiperm[i]];
  memcpy(iperm, perm, sizeof(int)*mat->n);
    
  for(i=0;i<mat->n;i++)
    perm[iperm[i]] = i;
  
  free(newperm);
  free(newiperm);
}



void PHIDAL_InteriorReorderRCM(int halo, csptr mat, PhidalHID *BL, dim_t *perm, dim_t *iperm)
{
  /***************************************************************************/
  /* This function reorder the matrix with Reverse Cuthill-Mackee algorithm  */
  /* in order to reduce fill-in. Only the interior of subdomain and */
  /* connector is reodered: the global connector partition is not
     affect by this ordering */
  /* This fonction use an RCM algorithm to reorder the unknowns inside the subdomain */
  /* The reordering is applied to the perm and iperm vector given in
     input */
  /* perm is supposed to contain already a permutation vector (that
     can be identity (most usually it contains the permutation that
     was used to form the matrix (i.e. the PHIDAL one) */

  dim_t i, j, d;
  int offset, neigh;
  int ind;
  INTL *ig, *ib; /* To store the sub graph of the interior node */
  dim_t *jg, *jb, n;
  dim_t *newperm;
  dim_t *newiperm;
  flag_t *mask;
  int periphnbr=-1;
  dim_t *periphlist=NULL;
  flag_t maskval;

  newperm = (dim_t *)malloc(sizeof(dim_t)*mat->n);
  newiperm = (dim_t *)malloc(sizeof(dim_t)*mat->n);
  mask = (flag_t *)malloc(sizeof(flag_t)*mat->n);
  if(halo == 1)
    periphlist = (dim_t *)malloc(sizeof(dim_t)*mat->n);

#ifdef DEBUG_M
  assert(BL->n == mat->n);
#endif

#ifndef ONLY_INTERIOR
  for(d=0;d<BL->nblock;d++)
#else
  for(d=BL->block_levelindex[0];d<BL->block_levelindex[1];d++)
#endif
    {
      /*-------------------------------------------------------/
      / Extract the adjacency graph of the interior node of the/
      /  local domain in CSR format                            /
      /-------------------------------------------------------*/
      /* Count an upper bound to nnz */
      ind = 0;
      for(i=BL->block_index[d];i<BL->block_index[d+1];i++)
	ind += mat->nnzrow[i];

      if(ind == 0) /* We never know that could happen ! */
	continue; 

      n = BL->block_index[d+1] - BL->block_index[d];
      ig = (INTL *)malloc(sizeof(INTL)*(n+1)); 
      jg = (dim_t *)malloc(sizeof(dim_t)*ind);

      offset = BL->block_index[d];
      if(halo == 0)
	{
	  ind = 0;
	  for(i=BL->block_index[d];i<BL->block_index[d+1];i++)
	    {
	      ig[i-offset] = ind;
	      for(j=0;j<mat->nnzrow[i];j++)
		if(mat->ja[i][j] >= BL->block_index[d] && mat->ja[i][j] < BL->block_index[d+1]) 
		  jg[ind++] = mat->ja[i][j] - offset;
	    }
	  ig[ BL->block_index[d+1]-offset ] = ind;
	}
      else
	{
	  ind = 0;
	  periphnbr = 0;
	  /** mask is used to mark the node that have a neighbor 
	      in a higer connector **/
	  bzero(mask, sizeof(int)*(BL->block_index[d+1]-BL->block_index[d]));
	  for(i=BL->block_index[d];i<BL->block_index[d+1];i++)
	    {
	      ig[i-offset] = ind;
	      for(j=0;j<mat->nnzrow[i];j++)
		{
		  neigh = mat->ja[i][j];
		  if(neigh >= BL->block_index[d] && neigh < BL->block_index[d+1]) 
		    jg[ind++] = neigh - offset;
		  if(mask[i-offset] == 0 && neigh >= BL->block_index[d+1])
		    {
		      periphlist[periphnbr++] =  i-offset;
		      mask[i-offset] = 1;
		    }
		}
	    }
	  ig[ BL->block_index[d+1]-offset ] = ind;
	}
      
      
      /** Symmetrize the CSR matrix **/
      ib = ig;
      jb = jg;

      
      PHIDAL_SymmetrizeMatrix(0, 0, n, ib, jb, NULL, &ig, &jg, NULL);
      
      free(ib);
      if(jb!= NULL)
	free(jb);


      /*** Compute the reordering to minimize fill-in using RCM ***/
      if(halo == 0)
	{
	  int periphnode;
	  periphnbr = 1;
	  periphnode = 0;
	  bzero(mask, sizeof(int)*n);
	  maskval = 1;
	  /*fprintfv(5, stderr, "Domain %d = %d nodes \n", d, n);*/
	  RCM(20, periphnbr, &periphnode, n, ig, jg, mask, maskval, newperm+offset, newiperm+offset);
	}
      else
	{
	  bzero(mask, sizeof(int)*n);
	  maskval = 1;
	  /** We use 1 iteration of bfs to be sure that the neighbor
	      of nodes in the halo are ordered at the end **/

	  if(periphnbr > 0)
	    {  
	      /*fprintfv(5, stderr, "Nbr = %d Periphlist = \n", periphnbr); 
	      for(i=0;i<periphnbr;i++)
	      fprintfv(5, stderr, "%d ", periphlist[i]);*/
      
	      
	      RCM(1, periphnbr, periphlist, n, ig, jg, mask, maskval, newperm+offset, newiperm+offset);

	      
	      /*fprintfv(5, stderr, "\n Last of iperm are :\n"); 
	      for(i=n-1;i>=n-periphnbr;i--)
	      fprintfv(5, stderr, "%d ", newiperm[i+offset]); 
	      fprintfv(5, stderr, "\n");*/


#ifdef DEBUG_M
	      /** Check if the nodes in the interior halo are ordered
		  with the highest number **/
	      for(i=0;i<n;i++)
		assert(newiperm[offset + newperm[offset+i]] == i);

	      for(i=0;i<periphnbr;i++)
		{
		  /*fprintfv(5, stderr, "periphlist[%d] = %d perm = %d \n", i, periphlist[i], newperm[offset+periphlist[i]]); */
		  assert(newperm[offset+periphlist[i]] >= n-periphnbr);
		  assert(newperm[offset+periphlist[i]] < n);
		}
#endif
	    }
	  else
	    {
	      periphnbr = 1;
	      periphlist[0] = 0;
	      RCM(100, periphnbr, periphlist, n, ig, jg, mask, maskval, newperm+offset, newiperm+offset);
	    }
	}

    

      free(ig);
      free(jg);
      
      /** Decal of the offset **/
      for(i=BL->block_index[d];i<BL->block_index[d+1];i++)
	newiperm[i] += offset;
    }


#ifndef ONLY_INTERIOR
  for(i=0;i<mat->n;i++)
    newperm[newiperm[i]] = i;
#else

  for(i=0;i<BL->block_index[BL->block_levelindex[1]];i++)
    newperm[newiperm[i]] = i;

  /* complete the permutation vector */
  for(i=BL->block_index[BL->block_levelindex[1]];i<BL->n;i++)
    {
      newiperm[i] = i;
      newperm[i] = i;
    }
#endif
    

  
  /*** newiperm[i] is the new rank of row i in the permuted matrix ***/
  /*** unknown i in the permuted matrix was perm[i] in the unpermuted matrix ***/
  CS_Perm(mat, newperm);

  
  /** Apply the permutation to the inputs perm vector **/

  /* Use perm as temporary vector to compute the iperm vector */
  for(i=0;i<mat->n;i++)
    perm[i] = iperm[newiperm[i]];
  memcpy(iperm, perm, sizeof(int)*mat->n);
    
  for(i=0;i<mat->n;i++)
    perm[iperm[i]] = i;
    
  if(halo != 0)
    free(periphlist);
  free(newperm);
  free(newiperm);
  free(mask);
}




void RCM(int maxit, int periphnbr, int *periphlist, int n, INTL *ia, dim_t *ja, int *mask, int maskval, dim_t *perm, dim_t *iperm)
{
  /***********************************************************************/
  /* Compute a Reverse Cuthill-Mac Kee reordering of a matrix            */
  /* The ordering is performed only on nodes such that mask[i] != maskval*/
  /***********************************************************************/
  int max_layernbr;
  int layernbr;
  int *layerptr;
  dim_t *iperm2;
  int ind, i;
  int it;
  if(n == 0)
    return;
  layerptr = (int *)malloc(sizeof(int)*(n+1));
  iperm2 = (dim_t *)malloc(sizeof(dim_t)* n);
  
  max_layernbr = 0;
  layernbr = 1;

  it = 0;
  while(layernbr>max_layernbr && it < maxit)
    {
      max_layernbr = layernbr;

      mybfs(periphnbr, periphlist, n, ia, ja, mask, maskval, iperm2, &layernbr, layerptr);
      it++;
      
      if(layernbr>=max_layernbr)
	memcpy(iperm, iperm2, sizeof(int)*layerptr[layernbr]);

      /*** Select a new peripheric node ***/
      if(it < maxit)
	{
	  periphnbr = 1;
	  periphlist[0] = iperm2[ layerptr[layernbr]-1 ];
	}

    
    }
  
  /*fprintfv(5, stderr, "Layernbr = %d \n", layernbr);*/

  memcpy(iperm2, iperm, sizeof(int)* layerptr[layernbr]);
  ind = 0;
  
  /** Reverse the ordering **/
  for(i=layerptr[layernbr]-1;i>=0;i--)
    {
      iperm[ind++] = iperm2[i];
#ifdef DEBUG_M
      assert(iperm2[i] < layerptr[layernbr]);
#endif
    }

 
 
  free(layerptr);
  free(iperm2);

  for(i=0;i<n;i++)
    perm[iperm[i]] = i;
 
  
}


void mybfs(int periphnbr, dim_t *periphlist, dim_t n, INTL *ia, dim_t *ja, flag_t *mask, flag_t maskval, dim_t *iperm, dim_t *layernbr, dim_t *layerptr)
{
  /*******************************************************************************************************************/
  /* Compute a Breath First Search in a graph from a set of starting nodes. The nodes considered in the graph        */
  /* are node i such that mask[i] != maskval. The set of starting nodes can be masked in which case they are not     */
  /* list in the iperm vector.                                                                                        */
  /*                                                                                                                 */
  /* NOTE: iperm (size = n) and layerptr (sizemax = n+1 in worst case) are allocated by user with                     */
  /* space                                                                                                           */
  /*******************************************************************************************************************/
  int i, j, ind;
  int node, neigh;
  flag_t *mask2;
  int nn;
  int layer;
  
  mask2 = (flag_t *)malloc(sizeof(flag_t)*n);
  memcpy(mask2, mask, sizeof(flag_t)*n);
  
  /** Compute the number of unmasked node **/
  nn = 0;
  for(i=0;i<n;i++)
    if(mask[i] != maskval)
      nn++;
  
  /*** Compute the first layer from the peripheric node list ****/
  ind = 0;
  layerptr[0] = 0;
  for(i=0;i<periphnbr;i++)
    {
      node = periphlist[i];

      if(mask2[node] != maskval)
	{
	  iperm[ind] = node;
	  mask2[node] = maskval;
	  ind++;
	}
      else
	{
	  /** Put the unmasked neighbors of the masked node in the first layer **/
	  /*for(j=0;j<mat->nnzrow[node];j++)*/
	  for(j=ia[node];j<ia[node+1];j++)
	    {
	      /*neigh = mat->ja[node][j];*/
	      neigh = ja[j];
	      if(mask2[neigh] != maskval)
		{
		  iperm[ind] = neigh;
		  mask2[neigh] = maskval;
		  ind++;
		}
	    }
	}
    }
  
#ifdef DEBUG_M
  assert(ind > 0);
#endif

  layer = 1;

  while(ind<nn)
    {
     
      layerptr[layer] = ind;
      
#ifdef DEBUG_M
      if(layerptr[layer]-layerptr[layer-1] == 0)
	fprintfd(stderr, "layer %d is empty \n", layer-1);
#endif

      for(i=layerptr[layer-1]; i<layerptr[layer]; i++)
	{
	  node = iperm[i];
	  /** Put the unmasked neighbors of the node in the next layer **/
	  /*for(j=0;j<mat->nnzrow[node];j++)*/
	  for(j=ia[node];j<ia[node+1];j++)
	    {
	      /*neigh = mat->ja[node][j];*/
	      neigh = ja[j];
	      if(mask2[neigh] != maskval)
		{
		  iperm[ind] = neigh;
		  mask2[neigh] = maskval;
		  ind++;
		} 
	    }
	}
      
      if(ind == layerptr[layer] && ind <nn)
	{
	  /*** Find another connexe component ****/
	  i = 0;
	  while(mask2[i] == maskval)
	    i++;
	  iperm[ind] = i;
	  mask2[i] = maskval;
	  ind++;
	}
      layer++;
    }


#ifdef DEBUG_M
  assert(ind == nn);
  assert(layer <= nn+1);
#endif

  layerptr[layer] = nn;
  
  (*layernbr) = layer;
  
  free(mask2);
}



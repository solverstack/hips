/* @authors P. HENON */

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>

#include "queue.h"
#include "phidal_ordering.h"
#include "order_grid.h"

#define XLIM 2
#define YLIM 2
#define ZLIM 2

int
orderGrid2 (
Order * const               ordeptr,              /*+ Ordering to compute           +*/
const int                   xnbr,                 /*+ X dimension                   +*/
const int                   ynbr,                 /*+ Y dimension                   +*/
const int                   baseval,              /*+ Base value                    +*/
const int                   xlim,                 /*+ Minimum X dimension per block +*/
const int                   ylim);                 /*+ Minimum Y dimension per block +*/

int
orderGrid3 (
Order * const               ordeptr,              /*+ Ordering to compute           +*/
const int                   xnbr,                 /*+ X dimension                   +*/
const int                   ynbr,                 /*+ Y dimension                   +*/
const int                   znbr,                 /*+ Z dimension                   +*/
const int                   baseval,              /*+ Base value                    +*/
const int                   xlim,                 /*+ Minimum X dimension per block +*/
const int                   ylim,                 /*+ Minimum Y dimension per block +*/
const int                   zlim);                /*+ Minimum Z dimension per block +*/

void PHIDAL_GridRegularDomains(int ndom, int nx, int ny, int nz, int n, INTL *ia, dim_t *ja, int **mapptr, int **mapp, dim_t *perm, dim_t *iperm)
{
  dim_t cblknbr;
  int * rangtab; /* Size n+1 */
  dim_t *treetab;
  Order orde;

#ifdef DEBUG_M
  assert(n == nx*ny*nz);
#endif
  rangtab = (int *)malloc(sizeof(int)*(n+1));
  treetab = (int *)malloc(sizeof(int)*(n+1));
  
  if(nz > 1)
    orderGrid3(&orde, nx, ny, nz, 0, XLIM, YLIM, ZLIM);
  else
    orderGrid2(&orde, nx, ny, 0, XLIM, YLIM);

  /*cblknbr = orde.cblknbr;
    rangtab = orde.rangtab;*/
  memcpy(perm, orde.permtab, sizeof(int)* (nx*ny*nz));
  memcpy(iperm, orde.peritab, sizeof(int)* (nx*ny*nz));
  fprintfd(stderr, "Number of supernodes in orderGrid = %d \n", orde.cblknbr);
  /*{
    dim_t i, j, k;
    for(i=0;i<n;i++)
    fprintfv(5, stderr, "%d ", iperm[i]);
    fprintfv(5, stderr, "\n\n");
    for(i=0;i<orde.cblknbr;i++)
    fprintfv(5, stderr, "[%d %d] ", orde.rangtab[i], orde.rangtab[i+1]-1);
    fprintfv(5, stderr, "\n");
    }*/
  orderExit(&orde);

  find_supernodes(n, n, ia, ja, perm, iperm, &cblknbr, rangtab, treetab);
  fprintfd(stderr, "Number of supernodes found in find_supernode = %d \n", cblknbr);

  /*{
    dim_t i, j, k;
    for(i=0;i<n;i++)
    fprintfv(5, stderr, "%d ", iperm[i]);
    fprintfv(5, stderr, "\n\n");
    for(i=0;i<cblknbr;i++)
    fprintfv(5, stderr, "[%d %d] ", rangtab[i], rangtab[i+1]-1);
    fprintfv(5, stderr, "\n");
     }*/




  /********************************************************************************************/
  /*                                                                                          */
  /*                  FIND THE intERIOR SUBDOMAINS DEFINED BY THE NESTED DISSECTION           */
  /*                                                                                          */
  /********************************************************************************************/
  *mapptr = (int *)malloc(sizeof(int)*(ndom+1));
  *mapp = (int *)malloc(sizeof(int)*n);
  if(get_interior_domains(ndom, iperm, cblknbr, rangtab, treetab, *mapp, *mapptr) != 0)
    {
      fprintfd(stderr, "It seems that %d is a too large number of domains for this matrix \n", ndom);
      exit(-1);
    }
  
#ifdef DEBUG_M
  /*** Check if any interior node of a subdomain is related to another subdomain interior node ***/
  fprintfd(stderr, "Check interior domains \n");
  check_interior_node(ndom, (*mapptr), (*mapp), ia, ja, n);
  fprintfd(stderr, "Check interior domains DONE \n");
#endif

  /*** Desallocations ***/
  free(rangtab);
  free(treetab);
  
  /**** Expand domain to get overlap ****/
  get_overlap(ndom, n, mapptr, mapp, ia, ja);
}




void PHIDAL_GridRegularSizeDomains(int domsize, int nx, int ny, int nz, int n, INTL *ia, dim_t *ja, int *ndom, int **mapptr, int **mapp, dim_t *perm, dim_t *iperm)
{
  dim_t cblknbr;
  int * rangtab; /* Size n+1 */
  dim_t *treetab;
  int ind, domind, i, j;
  int *cblk2dom;
  Order orde;

#ifdef DEBUG_M
  assert(n == nx*ny*nz);
#endif
  rangtab = (int *)malloc(sizeof(int)*(n+1));
  treetab = (int *)malloc(sizeof(int)*(n+1));
  
  if(nz > 1)
    orderGrid3(&orde, nx, ny, nz, 0, XLIM, YLIM, ZLIM);
  else
    orderGrid2(&orde, nx, ny, 0, XLIM, YLIM);

  /*cblknbr = orde.cblknbr;
    rangtab = orde.rangtab;*/
  memcpy(perm, orde.permtab, sizeof(int)* (nx*ny*nz));
  memcpy(iperm, orde.peritab, sizeof(int)* (nx*ny*nz));
  fprintfd(stderr, "Number of supernodes in orderGrid = %d \n", orde.cblknbr);
  /*{
    dim_t i, j, k;
    for(i=0;i<n;i++)
    fprintfd(stderr, "%d ", iperm[i]);
    fprintfd(stderr, "\n\n");
    for(i=0;i<orde.cblknbr;i++)
    fprintfd(stderr, "[%d %d] ", orde.rangtab[i], orde.rangtab[i+1]-1);
    fprintfd(stderr, "\n");
    }*/
  orderExit(&orde);


  find_supernodes(n, n, ia, ja, perm, iperm, &cblknbr, rangtab, treetab);

  fprintfd(stderr, "Number of supernodes found in find_supernode = %d \n", cblknbr);
  /*{
    dim_t i, j, k;
    for(i=0;i<n;i++)
    fprintfd(stderr, "%d ", iperm[i]);
    fprintfd(stderr, "\n\n");
    for(i=0;i<cblknbr;i++)
    fprintfd(stderr, "[%d %d] ", rangtab[i], rangtab[i+1]-1);
    fprintfd(stderr, "\n");
     }*/




  /********************************************************************************************/
  /*                                                                                          */
  /*                  FIND THE intERIOR SUBDOMAINS DEFINED BY THE NESTED DISSECTION           */
  /*                                                                                          */
  /********************************************************************************************/
  cblk2dom = (int *)malloc(sizeof(int)*cblknbr);
  if(get_interior_grid(domsize, cblknbr, rangtab, treetab, ndom, cblk2dom) != 0)
    {
      fprintfd(stderr, "Error in get_interior_grid \n");
      exit(-1);
    }

  /*if(get_interior_grid2(domsize, n, ia, ja, perm, iperm, cblknbr, rangtab, treetab, ndom, cblk2dom) != 0)
    {
    fprintfd(stderr, "Error in get_interior_grid2 \n");
    exit(-1);
    }*/
  


  /***********************************/
  /*** Construct mapptr            ***/
  /***********************************/
 (*mapptr)=(int *)malloc(sizeof(int)*((*ndom)+1));
  bzero((*mapptr), sizeof(int)*((*ndom)+1));
  ind = 0;
  for(i=0;i<cblknbr;i++)
    {
      domind = cblk2dom[i];
      if(domind >= 0)
	{
	  (*mapptr)[domind+1] += rangtab[i+1]-rangtab[i];
	  ind += rangtab[i+1]-rangtab[i];
	}
    }


  for(i=1;i<=(*ndom);i++)
    (*mapptr)[i]+=(*mapptr)[i-1];

  

  /*** Construct mapp ***/
  (*mapp)=(int *)malloc(sizeof(int)*ind);
  for(i=0;i<cblknbr;i++)
    {
      domind = cblk2dom[i];
      if(domind >= 0)
	{
	  memcpy((*mapp)+(*mapptr)[domind], iperm+rangtab[i], sizeof(int)*(rangtab[i+1]-rangtab[i]));
	  (*mapptr)[domind] += rangtab[i+1]-rangtab[i];
#ifdef DEBUG_M
	  assert((*mapptr)[domind] <= (*mapptr)[domind+1]);
#endif
	}
    }

  /*** Reset mapptr ***/
  for(i=(*ndom);i>=1;i--)
    (*mapptr)[i] = (*mapptr)[i-1];
  (*mapptr)[0] = 0;

  /** Eliminate NULL domain **/
  j = 0;
  for(i=0;i<(*ndom);i++)
    if((*mapptr)[i+1]>(*mapptr)[i])
      (*mapptr)[j++] = (*mapptr)[i];
  (*mapptr)[j] = (*mapptr)[*ndom];
  *ndom = j;


#ifdef DEBUG_M
  assert((*mapptr)[(*ndom)] == ind);

  /*** Check if any interior node of a subdomain is related to another subdomain interior node ***/
  fprintfd(stderr, "Check interior domains \n");
  check_interior_node((*ndom), (*mapptr), (*mapp), ia, ja, n);
  fprintfd(stderr, "Check interior domains DONE \n");
#endif


  /*** Desallocations ***/
  free(cblk2dom);
  free(rangtab);
  free(treetab);

  /**** Expand domain to get overlap ****/
  get_overlap(*ndom, n, mapptr, mapp, ia, ja);
}


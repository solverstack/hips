/* @authors P. HENON */

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include "phidal_ordering.h"
#include "scotch_metis_wrapper.h"

void PHIDAL_Interior2OverlappedPartition(int numflag, int ndom, int n, INTL *ia, dim_t *ja, int *node2dom, int **mapp, int **mapptr)
{
  /*------------------------------------------------------------------------------------/
  /   This function tranforms an interior domain partition in an overlapped partition  /
  
  /   On entry:                                                                         /
  /   numflag    : =0 the matrix is given in C numbering                               /
  /                 =1 the matrix is given in Fortran numbering                         /
  /   n, ia, ja   : matrix pattern                                                      /
  /   node2dom    : node2dom[i] is the subdomain number that contains node i            /
  /                                                                                     /
  /   On return:                                                                        /
  /   mapp, mapptr: the edge based partition (overlapped partition)                     /
  /                                                                                     /
  /  NOTE: input matrix must be in C numbering (indices start from 0)                   /
  /------------------------------------------------------------------------------------*/
  dim_t i, k;
  int ind;
  int *mapptr1;
  int *mapp1; 
  

   if(numflag == 1)
     CSR_Fnum2Cnum(ja, ia, n);


  mapp1 = (int *)malloc(sizeof(int)*n);
  mapptr1 = (int*)malloc(sizeof(int)*(ndom+1));
  bzero(mapptr1, sizeof(int)*(ndom+1));
  
  ind = 0;
  for(k=0;k<ndom;k++)
    for(i=0;i<n;i++)
      if(node2dom[i] == k)
	{
	  mapp1[ind++] = i;
	  mapptr1[k+1]++;
	}

#ifdef DEBUG_M
  assert(ind <= n);
#endif

  for(k=0;k<ndom;k++)
    mapptr1[k+1] += mapptr1[k];
  
#ifdef DEBUG_M  
  assert(mapptr1[ndom] <= n);
#endif
  *mapptr = mapptr1;
  *mapp = mapp1;

#ifdef DEBUG_M
  /*** Check if any interior node of a subdomain is related to another subdomain interior node ***/
  fprintfd(stderr, "Check interior domains \n");
  check_interior_node(ndom, (*mapptr), (*mapp), ia, ja, n);
  fprintfd(stderr, "Check interior domains DONE \n");
#endif

  
  /**** Expand domain to get overlap ****/
 
  get_overlap(ndom, n, mapptr, mapp, ia, ja);
  
   if(numflag == 1)
     CSR_Cnum2Fnum(ja, ia, n);
}

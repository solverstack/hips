/* @authors J. GAIDAMOUR */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <assert.h>
#include <phidal_ordering.h>


void Draw_2Dmesh(FILE* fd, int job, int nc, int n, int* ia, int* ja, int *iperm, int ndom, int *mapp, int *mapptr, PhidalHID *BL) 
{
  /** job = 0 : domaines **/
  /** job = 1 : level **/
  /** job = 2 : connector **/

  /* Grid 2D */
  /* set color */ 
  int i, j, k=0;
  int* xx = (int*)malloc(sizeof(int)*n);
  int* yy = (int*)malloc(sizeof(int)*n);
  int* color = (int*)malloc(sizeof(int)*n);
  for(i=0; i<n; i++) color[i] = -1;
    
    /* set xx, yy */
    for(i=0; i<nc; i++) {
      for(j=0; j<nc; j++) {
	xx[k] = i;
	yy[k] = j;

	k++;
      }
    }

    switch(job)
      {
      case 0:
	for(i=0; i<ndom; i++) {
	  for(j=mapptr[i]; j<mapptr[i+1]; j++) {
	    
	    if (color[mapp[j]] == -1) {
	      color[mapp[j]] = i+1; /* +1 because label '0' is to mark the separation between domains */
	    } else { color[mapp[j]] = ndom+1; /* printf("j=%d dans interface\n", j); */ }
	  }
	}
	
	break;
      case 1:
	for(i = 0;i<BL->nlevel;i++) {
	  int j;
	  for(j=BL->block_levelindex[i]; j<BL->block_levelindex[i+1]; j++) {
	    int k;
	    for(k=BL->block_index[j]; k<BL->block_index[j+1]; k++) {
	      color[iperm[k]] = i+1 /* 0 is a reserved color */;
	    }
	  }
	}
	break;
      case 2:
	
	/** CONNECTOR **/
	for(i = 0;i<BL->nblock;i++) {
	  int j;

	  for(j=BL->block_index[i]; j<BL->block_index[i+1]; j++) {
	    /* int k; */
	    color[iperm[j]] = i+1; /* 0 is a reserved color */
	    
	  }

	}
	break;
	
	
      }
    
    export_2Dmesh(fd, n, ia, ja, xx, yy, color, 0 , NULL);
    
}



void export_2Dmesh(FILE* fd, int n, int* ia, int* ja, int* xx, int* yy, int* color, int nodenbr, int* nodelist) {
  int i, j;
  int c;

  fprintf(fd,"MeshVersionFormatted 1\nDimension\n2\n");
  
  fprintf(fd,"Vertices\n%d\n", n);
  for(i=0; i<n; i++) {
    fprintf(fd, "%d %d 0\n", xx[i], yy[i]);
  }

  fprintf(fd,"Edges\n%d\n", ia[n]);
  for(i=0; i<n; i++) {
    for(j=ia[i]; j<ia[i+1]; j++) {
      if (ja[j] > i) {
	int l;
	
	/* test 1 */
	/*if (color[i] != color[ja[j]]) { c=0; } else c = color[i];*/
	/*if (color[ja[j]] == 0) c = 0;*/
	/*if (color[i] != color[ja[j]]) { c=0; } else c = color[i];*/
	
	c = MAX(color[i], color[ja[j]]);

	/* test 2 */
	if ((nodenbr != 0) && (nodelist!=NULL))
	  for(l=0; l<nodenbr; l++) {
	    if ((nodelist[l] == i) || (nodelist[l] == ja[j])) { c=1; break; }
	  }
	
	fprintf(fd, "%d %d %d\n", i+1, ja[j]+1, c);
	
      }
    }
  }

  fprintf(fd, "End\n");
}



void export_graphviz(FILE* fd, int n, int* ia, int* ja) {
  int i, j;

  fprintf(fd, "graph G {\n  node [shape=point, style=invisible]; \n\n");

  for(i=0; i<n; i++) {
    for(j=ia[i]; j<ia[i+1]; j++) {
      if (ja[j] > i)
	fprintf(fd, "%d -- %d;\n", i, ja[j]);
    }
  }

  fprintf(fd, "\n}\n");
}

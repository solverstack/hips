/* @authors P. HENON */

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include "queue.h"
#include "phidal_ordering.h" 


void PHIDAL_Perm2OverlappedDomains(dim_t ndom, dim_t n, INTL *ia, dim_t *ja, dim_t **mapptr, dim_t **mapp, dim_t *perm, dim_t *iperm)
{
  /******************************************************************************************/
  /* This function computes a decomposition of the matrix graph into overlapped subdomain   */
  /* using a permutation vector of the unknowns. To work correctly the permutation vector   */
  /* should be produced by a reordering method based on a Nested Dissection technique       */
  /* such as those produced by software like METIS or SCOTCH.                               */
  /* This routine is very useful when ones want to produce a domain decomposition of the    */
  /* matrix graph such that the fill-in is minimized and the overlap  between  subdomains   */
  /* is thin (the overlapp will roughly consists of the separator used in the Nested        */
  /* Dissection.                                                                            */
  /* On entry:                                                                              */
  /*     ndom : number of domain desired;                                                   */
  /*     n, ia, ja : the matrix graph (symmetric graph !)                                   */
  /*     perm, iperm : the ordering (coming from METIS or alike software )                  */
  /*                                                                                        */
  /* On return:                                                                             */
  /*    mapptr, mapp: pointer to the graph decomposition                                    */
  /* NOTE: mapptr and mapp are allocated inside this function                               */
  /*       ndom is a pointer since the number of domain obtained can be different than      */
  /*       the one desired in some rare cases (a warning is displayed in this cased)        */
  /******************************************************************************************/        


  dim_t cblknbr;
  dim_t * rangtab; /* Size n+1 */
  dim_t *treetab;
  /*int * iperm;*/ /* Taille n   */
  /*int * perm; */ /* Taille n   */

  rangtab = (dim_t *)malloc(sizeof(dim_t)*(n+1));
  treetab = (dim_t *)malloc(sizeof(dim_t)*(n+1));
  
  /*
    @@@ OIMBE : comprends pas perm et iperm inverser ???? ==>
  find_supernodes(n, ia, ja, perm, iperm, &cblknbr, rangtab, treetab);*/
  find_supernodes(n, n, ia, ja, iperm, perm, &cblknbr, rangtab, treetab);
  fprintfd(stderr, "Find supernodes = %d \n", cblknbr);

  /********************************************************************************************/
  /*                                                                                          */
  /*                  FIND THE intERIOR SUBDOMAINS DEFINED BY THE NESTED DISSECTION           */
  /*                                                                                          */
  /********************************************************************************************/
  
  *mapptr = (dim_t *)malloc(sizeof(dim_t)*(ndom+1));
  *mapp = (dim_t *)malloc(sizeof(dim_t)*n);

  /* @@@@ OIMBE perm ou iperm ???? */
  if(get_interior_domains(ndom, perm, cblknbr, rangtab, treetab, *mapp, *mapptr) != 0)
    {
      fprintfd(stderr, "It seems that %d is a too large number of domains for this matrix \n", ndom);
      exit(-1);
    }
 

#ifdef DEBUG_M
  /*** Check if any interior node of a subdomain is related to another subdomain interior node ***/
  fprintfd(stderr, "Check interior domains \n");
  check_interior_node(ndom, (*mapptr), (*mapp), ia, ja, n);
  fprintfd(stderr, "Check interior domains DONE \n");
#endif


  /*** Desallocations ***/
  free(rangtab);
  free(treetab);
  /*free(iperm);
    free(perm);*/
  
  /**** Expand domain to get overlap ****/
  get_overlap(ndom, n, mapptr, mapp, ia, ja);
  
}


/* @authors P. HENON */

#ifdef SCOTCH_PART


#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include "queue.h"
/*#define restrict 
  #include "scotch.h"*/

#include "phidal_ordering.h"
#include "scotch_metis_wrapper.h"




void PHIDAL_ScotchOrder2OverlappedDomains(int ndom, int n, INTL *ia,dim_t *ja, int **mapptr,  int **mapp, dim_t *perm, dim_t *iperm)
{
  


  dim_t cblknbr;
  dim_t * rangtab; /* Size n+1 */
  dim_t *treetab;
  /*int * iperm;*/ /* Taille n   */
  /*int * perm; */ /* Taille n   */

  rangtab = (dim_t *)malloc(sizeof(dim_t)*(n+1));
  treetab = (dim_t *)malloc(sizeof(dim_t)*(n+1));
  
  {
    SCOTCH_Graph        grafdat;
    SCOTCH_Strat        stratdat;
    char STRAT[400];

    SCOTCH_stratInit (&stratdat);

    
    /** This strategy includes the ordering of the leaves i.e. the interior nodes **/
    sprintf(STRAT, "n{sep=/(vert>120)?m{vert=100,low=h{pass=10},asc=f{bal=0.2}}|m{vert=100,low=h{pass=10},asc=f{bal=0.1}};,ole=f{cmin=0,cmax=10000,frat=0.00},ose=g}");
    
    
    SCOTCH_stratGraphOrder (&stratdat, STRAT);
    
    SCOTCH_graphInit  (&grafdat);
    /*SCOTCH_graphBuild (&grafdat, 0, n, ia, NULL, NULL, NULL, ia[n], ja, NULL);*/
    SCOTCH_graphBuild_WRAPPER (&grafdat, 0, n, ia, NULL, ja, NULL);
    
    /*  fprintfv(5, stderr, "NDOM %ld \n", (long)ndom);*/
    /** Do not free verttab and edgetab before SCOTCH_graphOrder **/
    /*SCOTCH_graphOrder (&grafdat, &stratdat, iperm, perm, &cblknbr, rangtab, treetab);*/
    /** OIMBE iperm et perm inversés ?? **/
    SCOTCH_graphOrder_WRAPPER (&grafdat, &stratdat, n, perm, iperm, &cblknbr, rangtab, treetab);
    
    SCOTCH_graphExit (&grafdat);
    SCOTCH_stratExit (&stratdat);
  }
  
  /*find_supernodes(n, ia, ja, iperm, perm, &cblknbr, rangtab, treetab);*/
  fprintfd(stderr, "Find supernodes = %d \n", cblknbr);

  /********************************************************************************************/
  /*                                                                                          */
  /*                  FIND THE intERIOR SUBDOMAINS DEFINED BY THE NESTED DISSECTION           */
  /*                                                                                          */
  /********************************************************************************************/
  
  /*** Find the number of subdomain created by scotch ***/
  *mapptr = (dim_t *)malloc(sizeof(dim_t)*(ndom+1));
  *mapp = (dim_t *)malloc(sizeof(dim_t)*n);
  if(get_interior_domains(ndom, perm, cblknbr, rangtab, treetab, *mapp, *mapptr) != 0)
    {
      fprintfd(stderr, "It seems that %d is a too large number of domain for this matrix \n", ndom);
      exit(-1);
    }
 

#ifdef DEBUG_M
  /*** Check if any interior node of a subdomain is related to another subdomain interior node ***/
  fprintfd(stderr, "Check interior domains \n");
  check_interior_node(ndom, (*mapptr), (*mapp), ia, ja, n);
  fprintfd(stderr, "Check interior domains DONE \n");
#endif


  /*** Desallocations ***/
  free(rangtab);
  free(treetab);
  /*free(iperm);
    free(perm);*/
  
  /**** Expand domain to get overlap ****/
  get_overlap(ndom, n, mapptr, mapp, ia, ja);
  
}



#endif

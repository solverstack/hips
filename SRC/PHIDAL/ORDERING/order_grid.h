/* @noheader */

/* Copyright INRIA 2004
**
** This file is part of the Scotch distribution.
**
** The Scotch distribution is libre/free software; you can
** redistribute it and/or modify it under the terms of the
** GNU Lesser General Public License as published by the
** Free Software Foundation; either version 2.1 of the
** License, or (at your option) any later version.
**
** The Scotch distribution is distributed in the hope that
** it will be useful, but WITHOUT ANY WARRANTY; without even
** the implied warranty of MERCHANTABILITY or FITNESS FOR A
** PARTICULAR PURPOSE. See the GNU Lesser General Public
** License for more details.
**
** You should have received a copy of the GNU Lesser General
** Public License along with the Scotch distribution; if not,
** write to the Free Software Foundation, Inc.,
** 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
**
** $Id: order_grid.h 2 2004-06-02 14:05:03Z ramet $
*/
/************************************************************/
/**                                                        **/
/**   NAME       : order_grid.h                            **/
/**                                                        **/
/**   AUTHORS    : Francois PELLEGRINI                     **/
/**                                                        **/
/**   FUNCTION   : Part of a parallel direct block solver. **/
/**                These lines are the data declarations   **/
/**                for the grid ordering routine.          **/
/**                                                        **/
/**   DATES      : # Version 0.0  : from : 04 sep 1998     **/
/**                                 to     02 may 1999     **/
/**                # Version 2.0  : from : 28 feb 2004     **/
/**                                 to     28 feb 2004     **/
/**                                                        **/
/************************************************************/


/*
**  The function prototypes.
*/
typedef struct Order_ {
  int                       cblknbr;              /*+ Number of column blocks             +*/
  int *                     rangtab;              /*+ Column block range array [based,+1] +*/
  int *                     permtab;              /*+ Permutation array [based]           +*/
  int *                     peritab;              /*+ Inverse permutation array [based]   +*/
} Order;


int                         orderInit           (Order * const ordeptr);
void                        orderExit           (Order * const ordeptr);
int                         orderCheck          (const Order * const ordeptr);

void                 graphOrderGrid22    (int * const cblkptr, int ** const rangptr, int * const permtab, int * const ordeptr, const int xnbr, const int xlim, const int ylim, const int xmin, const int ymin, const int xext, const int yext, int flag);

void                 graphOrderGrid32    (int * const cblkptr, int ** const rangptr, int * const permtab, int * const ordeptr, const int xnbr, const int ynbr, const int xlim, const int ylim, const int zlim, const int xmin, const int ymin, const int zmin, const int xext, const int yext, const int zext, int flag);


/* @authors P. HENON */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include "phidal_ordering.h"

#define MIS_VERSION 
#ifndef SCOTCH_PART
/** OIMBE, In some cases Metis crashes (or make HIPS crashes :)) **/
#define MIS_VERSION 
#endif


#ifndef MIS_VERSION

#include "scotch_metis_wrapper.h"

#endif

void connector_level_adj(INTL **c_ia, dim_t **c_ja, dim_t start_level, dim_t end_level, PhidalHID *BL);


void block_elimination_ordering(const int ndom, PhidalHID *BL, int *rperm)
{
  dim_t i,j;
  
  int nlevel;
  int nblock;
  int *block_index;
  int *block_levelindex;
  int *block_keyindex;
  int *block_key;

  int index, block;
  int *block_rperm;

  INTL *sizetab;

#ifdef MIS_VERSION
  int *MIS;
  int MISsize; 
  int *mask; 
#endif 

  int *tmp1;
  int *tmp2;
  dim_t n;


  /** Unpack BL **/
  nlevel = BL->nlevel;
  nblock = BL->nblock;
  block_index = BL->block_index;
  block_levelindex = BL->block_levelindex;
  block_keyindex = BL->block_keyindex;
  block_key = BL->block_key;


#ifdef DEBUG_M
  assert(nblock > 0);
#endif

/*-----------------------------------------------------------------------------------------------------*/

  /*--------------------------------------------------
  / Order the block to suit the elimination 
  / process in the factorization:
  / in each level, we compute maximum independent sets 
  / of blocks (e.g. their keys are disjoints) 
  / until all the blocks of a levels are reordered
  / The level 0 (interior nodes) don't need to be reordered
  /-------------------------------------------------*/

  block_rperm = (int *)malloc(sizeof(int)*nblock);

  /* The level 0 (interior nodes) don't need to be reordered*/
  for(index = 0;index < block_levelindex[1];index++)
    block_rperm[index] = index;
  

  /** sizetab[i] is the size of connector i **/
  sizetab = (INTL *)malloc(sizeof(INTL)*nblock);
  for(i=0;i<nblock;i++)
    sizetab[i] = block_index[i+1]-block_index[i];


#ifdef MIS_VERSION /******************** BEGIN MIS VERSION ****************************************/
  MIS = (int *)malloc(sizeof(int)*nblock);
  /** We use mask to mark block already selected in an independant set **/
  mask = (int *)malloc(sizeof(int)*nblock);
  bzero(mask, sizeof(int)*nblock);
  for(i=1;i<nlevel;i++)
    {
      dim_t start_level;
      dim_t end_level;

      INTL *c_ia;
      dim_t *c_ja;

#ifdef DEBUG_M
      int_t MISnbr;
      MISnbr = 0;
#endif

      start_level = block_levelindex[i];
      end_level   = block_levelindex[i+1]-1;


      /*** Compute the adjacency graph of the connector in this level ***/
      connector_level_adj(&c_ia, &c_ja, start_level, end_level, BL);

#ifdef DEBUG_M
      assert(index == start_level);
#endif
      while(index <= end_level)
	{
	  /** Compute another independent set **/
	  /*fprintfv(5, stderr, "LEVEL %d MIS %d\n", i, MISnbr);*/
	  find_MIS(1, end_level-start_level+1, c_ia, c_ja, sizetab+start_level, mask+start_level, 0, &MISsize, MIS);
	  for(j=0;j<MISsize;j++)
	    {
	      block_rperm[index] = start_level + MIS[j];
	      mask[start_level+ MIS[j]]=1;
	      index++;
	    }
#ifdef DEBUG_M
	  MISnbr++;
#endif
	}
      free(c_ia);
      if(c_ja != NULL)
	free(c_ja);
      
      
    }

  free(mask);
  free(sizetab);
  free(MIS);
  /**************************************  END MIS VERSION ************************************/

#else
  {
    dim_t *block_perm;
    block_perm = (int *)malloc(sizeof(int)*nblock);
    for(i=1;i<nlevel;i++)
      {
	int start_level;
	int end_level;
	INTL *c_ia;
	dim_t *c_ja;
	dim_t nodenbr;
#ifdef SCOTCH_PART
	
#else /* SCOTCH_PART */
	int metis_options[10];
#endif /* SCOTCH_PART */
	flag_t numflag;
	
	start_level = block_levelindex[i];
	end_level   = block_levelindex[i+1]-1;
	

	/*** Compute the adjacency graph of the connector in this level ***/

	connector_level_adj(&c_ia, &c_ja, start_level, end_level, BL);

	numflag = 0; /** Indicate C indexing type **/
	nodenbr = end_level-start_level+1;
	
#ifdef SCOTCH_PART
	{
	  SCOTCH_Strat stratdat;
	  SCOTCH_Graph grafdat;
	  char STRAT[400];
	  
	  SCOTCH_stratInit (&stratdat);
	  
	  
	  
#if 1 /* Pure minimum fill if 1, mixed ND/MF if 0 */
	  
	  sprintf(STRAT, "f{cmin=0,cmax=10000,frat=0.001}" ); 
	  /*SCOTCH_stratGraphOrder (&stratdat, "f{cmin=0,cmax=10000,frat=0.001}");*/
#else
	  sprintf(STRAT, "n{sep=/(vert>120)?m{vert=100,low=h{pass=10},asc=f{bal=0.2}};,ole=f{cmin=1,cmax=10000,frat=0.08},ose=g}"); 
	  /*SCOTCH_stratGraphOrder (&stratdat, "n{sep=/(vert>120)?m{vert=100,low=h{pass=10},asc=f{bal=0.2}};,ole=f{cmin=0,cmax=10000,frat=0.08},ose=g}");*/
#endif
	  SCOTCH_stratGraphOrder (&stratdat, STRAT);
	  
	  SCOTCH_graphInit  (&grafdat);
	  /*SCOTCH_graphBuild (&grafdat, numflag, nodenbr, c_ia, NULL,
	    sizetab + start_level, NULL, c_ia[n], c_ja, NULL);*/
	  
	  /*SCOTCH_graphBuild (&grafdat, numflag, nodenbr, c_ia, NULL, sizetab + start_level, NULL, c_ia[nodenbr], c_ja, NULL);*/
	  SCOTCH_graphBuild_WRAPPER(&grafdat, numflag, nodenbr, c_ia,
				    sizetab + start_level, c_ja, NULL);
	  
	  /*fprintfv(5, stderr, "GraphCheck return = %d \n", SCOTCH_graphCheck(&grafdat));	
	    SCOTCH_graphSave (&grafdat, stderr);*/
	  
	  /*SCOTCH_graphOrder (&grafdat, &stratdat, block_perm+start_level, block_rperm+start_level, NULL, NULL, NULL);*/

	  SCOTCH_graphOrder_WRAPPER (&grafdat, &stratdat, nodenbr, block_perm+start_level, block_rperm+start_level,
				     NULL, NULL, NULL);
	  
	  SCOTCH_graphExit (&grafdat);
	  SCOTCH_stratExit (&stratdat);
	}
#else /* SCOTCH_PART */
	metis_options[0] = 0; /** Use default strategies in the METIS reordering routine **/
	
	/**** Compute a connector ordering using a reordering of the connector adjacency matrix
	      that minimize fill in ******/
#ifdef DEBUG_M
	assert(nodenbr>0);
	{
	  dim_t g;
	  for(g=start_level;g<start_level + nodenbr;g++)
	    assert(sizetab[g] > 0);
	}
#endif
	/*fprintfv(5, stderr, "Attention METIS_Node a la place de METIS_NodeNW \n");
	metis_NodeND(&nodenbr, c_ia, c_ja, &numflag, metis_options, 
	block_rperm+start_level, block_perm+start_level);*/
	METIS_NodeWND_WRAPPER(nodenbr, c_ia, c_ja, sizetab + start_level, numflag, metis_options, 
		      block_rperm+start_level, block_perm+start_level);

#endif /* SCOTCH_PART */
	
	for(j=start_level;j<=end_level;j++)
	  {
	    block_rperm[j] += start_level;
	    block_perm[j] += start_level;
	  }
	
	free(c_ia);
	if(c_ja != NULL)
	  free(c_ja);
      }
    free(block_perm);
    free(sizetab);
  }
   
/*---------------------------------------------------------------------------------------*/
#endif
  /**---------------------------------------------------------------------------
  /  Compute the new permutation of blocks and unknowns according to the ordering 
  / we have just computed
  -------------------------------------------------------------------------------**/
  n = block_index[nblock];

  tmp1 = malloc(sizeof(int)*(nblock+1));
  tmp2 = malloc(sizeof(int)*n);
  memcpy(tmp1, block_index, sizeof(int)*(nblock+1));
  memcpy(tmp2, rperm, sizeof(int)*n);

  /*-------------------------
  / Compute the new permutation of 
  / the unknowns and compute the new index of the blocks
  ---------------------------*/
  index = 0;
  for(i=0;i<nblock;i++)
    {
      block = block_rperm[i];
      block_index[i] = index;
      for(j=tmp1[block];j<tmp1[block+1];j++)
	{
	  rperm[index] = tmp2[j];
	  index++;
	}
    }
  block_index[nblock] = n;
#ifdef DEBUG_M
  assert(index == n);
#endif
  /*-------------------------
  / Permute the keys of the blocks
  / according to the new ordering 
  ---------------------------*/
  free(tmp1);
  tmp1 = malloc(sizeof(int)*(nblock+1));
  memcpy(tmp1, block_keyindex, sizeof(int)*(nblock+1));

  free(tmp2);
  tmp2 = malloc(sizeof(int)*(block_keyindex[nblock]));
  memcpy(tmp2, block_key, sizeof(int)*(block_keyindex[nblock]));

  index = 0;
  for(i=0;i<nblock;i++)
    {
      block = block_rperm[i];
      block_keyindex[i] = index;
      for(j=tmp1[block];j<tmp1[block+1];j++)
	{
	  block_key[index] = tmp2[j];
	  index++;
	}
    }
  block_keyindex[nblock] = index;

#ifdef DEBUG_M
  assert(index == tmp1[nblock]);
  for(i=0;i<nblock;i++)
    {
      block = block_rperm[i];
      assert(tmp1[block+1] - tmp1[block] == block_keyindex[i+1] - block_keyindex[i]);
    }

#endif


  /** Free allocated memory **/
  free(block_rperm);
  free(tmp2);
  free(tmp1);
}
     

int independent_block(int block, int *block_keyindex, int * block_key, int *hit_flag)
{
  dim_t i;
  for(i=block_keyindex[block];i<block_keyindex[block+1];i++)
    if(hit_flag[ block_key[i] ] != 0 )
      return 0; /** return FALSE **/
  
  return 1; /** return TRUE **/
}

void mark_adjacent_domains(int block, int *block_keyindex, int *block_key, int *hit_flag)
{
  dim_t i;
  for(i=block_keyindex[block];i<block_keyindex[block+1];i++)
    {
#ifdef DEBUG_M
      assert(hit_flag[ block_key[i] ] == 0);
#endif
      hit_flag[ block_key[i] ] = 1;
    }
}
 


int connector_degree(int blocknum, int *block_keyindex, int *block_key, int start_level, int end_level, int *mask)
{
  dim_t i;
  int degree;
  int *bkeyptr;
  int bkl;

  bkeyptr = block_key + block_keyindex[blocknum];
  bkl = block_keyindex[blocknum+1] - block_keyindex[blocknum];

  degree = 0;
  for(i=start_level; i<=end_level;i++)
    if(i!= blocknum && mask[i] == 0)
      if(is_intersect_key(bkl, bkeyptr, block_keyindex[i+1]-block_keyindex[i], block_key + block_keyindex[i]) == 1)
	degree++;
  
  return degree;
}




void connector_level_adj(INTL **c_ia, dim_t **c_ja, dim_t start_level, dim_t end_level, PhidalHID *BL)
{
  dim_t i, j, k, g;
  int eind;
  int b, ind, dom;

  int *tmpj;
  int *jrev;
  int **blist; 
  int *nblist;
  int n;
  INTL *ia;
  dim_t *ja;

  /** For each domain we construct the list of adjacent connectors **/
  nblist = (int *)malloc(sizeof(int)*BL->ndom);
  bzero(nblist, sizeof(int)*BL->ndom);
  for(k=start_level;k<=end_level;k++)
    for(i=BL->block_keyindex[k];i<BL->block_keyindex[k+1];i++)
      nblist[BL->block_key[i]]++;
  
  blist = (int **)malloc(sizeof(int *)*BL->ndom);
  for(i=0;i<BL->ndom;i++)
    {
      if(nblist[i] > 0)
	{
	  blist[i] = (int *)malloc(sizeof(int)*nblist[i]);
	  bzero(blist[i], sizeof(int)*nblist[i]);
	}
      else
	blist[i] = NULL;
    }

  bzero(nblist, sizeof(int)*BL->ndom);


  for(k=start_level;k<=end_level;k++)
    for(i=BL->block_keyindex[k];i<BL->block_keyindex[k+1];i++)
      {
	j = BL->block_key[i];
	blist[j][nblist[j]++] =  k;
      }  

 

  /*** We construct the quotient graph of the connector in this level ****/
  n = end_level-start_level+1; /* BL->ndom initialememnt */
  tmpj = (int *)malloc(sizeof(int)*n);
  jrev = (int *)malloc(sizeof(int)*n);
  for(i=0;i<n;i++)
    jrev[i] = -1;

  /*** Compute the number of edge ***/
  eind = 0;
  for(k=start_level;k<=end_level;k++)
    {
      ind = 0;
      for(g=BL->block_keyindex[k];g<BL->block_keyindex[k+1];g++)
	{
	  for(i=0;i<nblist[BL->block_key[g]];i++)
	    {
	      b = blist[BL->block_key[g]][i];
	      if(b != k) /** We do not store the diagonal **/
		if(jrev[b-start_level] == -1)
		  {
		    jrev[b-start_level] = ind;
		    tmpj[ind] = b-start_level;
		    eind++;
		    ind++;
		  }
	    }
	}
      /** Reinit jrev **/
      for(i=0;i<ind;i++)
	jrev[tmpj[i]] = -1;
    }

  
  ia = (INTL *)malloc(sizeof(INTL)*(n+1));
  ja = (int *)malloc(sizeof(int)*eind); /** This allocation is an upper
					    bound **/
  bzero(tmpj, sizeof(int)*n);
  eind = 0;
  for(k=start_level;k<=end_level;k++)
    {
      ind = 0;
      for(g=BL->block_keyindex[k];g<BL->block_keyindex[k+1];g++)
	{
	  dom = BL->block_key[g];
	  for(i=0;i<nblist[dom];i++)
	    {
	      b = blist[dom][i];
	      if(b != k) /** We do not store the diagonal **/
		if(jrev[b-start_level] == -1)
		  {
		    jrev[b-start_level] = ind;
		    tmpj[ind] = b-start_level;
		    ind++;
		  }
	    }
	}
      
      ia[k-start_level] = eind;
      for(i=0;i<ind;i++)
	{
	  ja[eind + i] = tmpj[i];
	}
      eind += ind;
      
      /** Reinit jrev **/
      for(i=0;i<ind;i++)
	jrev[tmpj[i]] = -1;
    }
  ia[k-start_level] = eind;

  free(jrev);
  free(tmpj);

  for(i=0;i<BL->ndom;i++)
    {
      if(blist[i] != NULL)
	free(blist[i]);
    }
  free(nblist);
  free(blist);
#ifdef DEBUG_M
  /** Check the graph **/
  checkGraph(n, ia, ja, NULL, NULL, 0);
#endif
  *c_ia = ia;
  *c_ja = ja;
}

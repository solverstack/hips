/* @authors J. GAIDAMOUR */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <assert.h>
#include <phidal_common.h> /* quicksort */

void next_stripe(int tag, int tagnbr,
		 int fin_nbr, int* fin, 
		 int* fout_nbr, int* fout, int fout_maxsize,
		 int n, int* ia, int* ja, 
		 int* tags, int* count,
		 int* mask, int maskval);

/* */
void HIPS_XPartition(int tagnbr,                        /* in  : */ 
		     int nodenbr, int* nodelist,          /* in  : centers */
		     int n, int* ia, int* ja,             /* in  : graph */
		     int* ndom_out,                       /* out */
		     int** mapptr_out, int** mapp_out,    /* out */
		     int* mask, int maskval) {            /* in  */
  int i;
  int count;

  /* To store tags of visited nodes */ 
  /* Tags of node i : tags[i*tagnbr]-> tags[(i+1)*tagnbr -1] */
  int* tags = (int*)malloc(sizeof(int)*n*tagnbr);
  assert(tags != NULL);
  for(i=0; i<n*tagnbr; i++) tags[i]= -1;

  /* One list of nodes per center node to store the last stripe of BFS */
  int*  fifo1 = (int*)malloc(sizeof(int)*n*tagnbr);  /* max(sum of list sizes) = tagnbr*n */
  int** fifo1_ptr = (int**)malloc(sizeof(int*)*(nodenbr+1)); /* fifo_ptr[i] : pointer to the begining of the list for node i in fifo */
  assert(fifo1 != NULL);
  assert(fifo1_ptr != NULL);

  /* For swapping */
  int*  fifo2 = (int*)malloc(sizeof(int)*n*tagnbr);
  int** fifo2_ptr = (int**)malloc(sizeof(int*)*(nodenbr+1));
  assert(fifo2 != NULL);
  assert(fifo2_ptr != NULL);

  count=0;

  /* Init */
  for(i=0; i<nodenbr; i++) {
    fifo1[i] = nodelist[i];
    fifo1_ptr[i] = fifo1+i;
  }
  fifo1_ptr[nodenbr] = fifo1_ptr[nodenbr-1]+1;
  fifo2_ptr[0] = fifo2;

  count=0;
  for(i=0; i<nodenbr; i++) {
    tags[nodelist[i]*tagnbr] = nodelist[i];
  }
  if(tagnbr==1) count+=nodenbr;

  int** fin_ptr  = fifo1_ptr;
  int** fout_ptr = fifo2_ptr;

  /* Tag graph nodes with the nearest nodes of nodelist */
/*   int loop=0; */
  while(count != n) {
    for(i=0; i<nodenbr; i++) {
      int fout_nbr;
     
      /* printf("nextstripe %d : %d %d\n", nodelist[i], fin_ptr[i+1]-fin_ptr[i], *fin_ptr[i]); */
      next_stripe(nodelist[i], tagnbr,
		  fin_ptr[i+1]-fin_ptr[i], fin_ptr[i],
		  &fout_nbr,               fout_ptr[i], n*tagnbr-(fout_ptr[i]-fout_ptr[0]),
		  n, ia, ja,
		  tags, &count, mask, maskval);
      
      fout_ptr[i+1] = fout_ptr[i]+fout_nbr;
    }
   
    { /* swap fifo */
      int** tmp = fin_ptr;
      fin_ptr = fout_ptr;
      fout_ptr = tmp;
    }

 /*    loop++; */
     /* printf("count=%d\n", count); */
/*     if (loop == 2) */
/*       exit(1); */
  }

  free(fifo1_ptr); free(fifo1);
  free(fifo2_ptr); free(fifo2);

#ifdef DEBUG_M3
  /************ debug ***/
  {
    int t[16];
    t[0]=0;
    t[3]=1;
    t[12]=2;
    t[15]=3;
    
    int j;
    printf("tags : \n");
    for(j=0; j<n; j++) {
      if (tags[j*tagnbr] == -1) {
	printf("- %d :\n", j);
      } else if (tags[j*tagnbr+1] == -1) {
	printf("- %d : %d\n", j, t[tags[j*tagnbr]]);
      } else if (tags[j*tagnbr+2] == -1) {
	printf("- %d : %d %d\n", j, t[tags[j*tagnbr]], t[tags[j*tagnbr+1]]);
      } else printf("- %d : %d %d %d\n", j, t[tags[j*tagnbr]], t[tags[j*tagnbr+1]], t[tags[j*tagnbr+2]]);
      
    }
  }
  /************ debug ***/
#endif

  /* Build mapp from tags */
  for(i=0; i<n; i++) {   /* Sort tags */
    quicksort(tags, i*tagnbr, (i+1)*tagnbr-1);	  
  }

  int* mapp = (int*)malloc(sizeof(int)*n); *mapp_out = mapp;
  for(i=0; i<n; i++) { mapp[i]=i; }
  quicksort_node_tags(mapp, 0, n-1, tags, tagnbr);

  /* Build mapptr */
  /* compute ndom -> todo : mieux ? */
  int ndom, c;
  
  ndom=1; c=0;
  for(i=0; i<n; i++) {
    if (compare_node_tags(mapp[c], mapp[i], tags, tagnbr) != 0) { 
      ndom++; c=i;
    }
  }
  *ndom_out = ndom;

  int* mapptr = (int*)malloc(sizeof(int)*(ndom+1)); *mapptr_out = mapptr;
  mapptr[0] = 0; mapptr[ndom] = n;
  
  ndom=1; c=0;
  for(i=0; i<n; i++) {
    if (compare_node_tags(mapp[c], mapp[i], tags, tagnbr) != 0) { 
      mapptr[ndom]= i;
      ndom++; c=i;  
    }
  }

  free(tags);

#ifdef DEBUG_M
  /* debug only  : local sort of node id inside a domain */
  for(i=0; i<ndom; i++) {
    quicksort(mapp, mapptr[i], mapptr[i+1]-1);
  }
#endif

}


void next_stripe(int tag, int tagnbr,
		 int fin_nbr, int* fin, 
		 int* fout_nbr, int* fout, int fout_maxsize,
		 int n, int* ia, int* ja, 
		 int* tags, int* count, 
		 int* mask, int maskval) {
  int i, j, s;
  int node;
  int fout_i = 0;

  for(i=0; i<fin_nbr; i++) { /* /!\ i is not a node number */
    node=fin[i];
    
    for(j=ia[node]; j<ia[node+1]; j++) {
      /* inspection of node ja[j]; */

      if (ja[j] == node) continue; /* loop (diagonal term) in the graph */

#ifdef DEBUG_M2
      {
	int si = ja[j]*tagnbr;
	printf("tag = %d from node %d voisin : %d tags3=%d %d %d\n", tag, fin[i], ja[j], tags[si], tags[si+1], tags[si+2]);
      }
#endif

      /* masked nodes -> continue; */
      if (mask != NULL && mask[ja[j]] == maskval) continue;

      int si = ja[j]*tagnbr;
      int sf = si+tagnbr;

      /* nodes that have already 'tagnbr' tags -> continue; */
      if (tags[sf-1] != -1) continue;

      /* node tagged with "tag" -> already explored by bfs -> continue; */
      {
	int ignore = 0;
	for(s = si; s < sf; s++) {
	  if (tags[s] == -1)  { break; } /* this line is optional */
	  if (tags[s] == tag) { ignore = 1; break; }
	}
	if (ignore) continue;
      }

      /** add in fifo (out) **/
      /* printf("%d %d\n", fout_i, fout_maxsize); */
      assert(fout_i<fout_maxsize);
      fout[fout_i] = ja[j]; fout_i++;
   
      /**/

      /* add a tag for this node */
      s = si; while(tags[s] != -1) s++;
      assert(s < sf); assert(tags[s] == -1);
      tags[s] = tag; 

#ifdef DEBUG_M2
      printf("-->tag = %d from node %d voisin : %d tags3=%d %d %d\n", tag, fin[i], ja[j], tags[si], tags[si+1], tags[si+2]);
#endif

      /* have we finish with this node ? */
      if(s == sf-1) { /* mask[ja[j]]= maskval; */ (*count)++; } 

    }
  }

  *fout_nbr = fout_i;

}

/* @authors P. HENON */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "phidal_ordering.h"


void PHIDAL_GetSupernodes(flag_t verbose, int numflag, int n,  INTL *ia,  dim_t *ja,  
			  PhidalHID *BL, dim_t *perm, dim_t *iperm,
			  int **dom2cblktab, int **cblktab, int **cblktreetab)
{


  /******************************************************************************************/
  /* On return:                                                                             */
  /*             perm, iperm : the ordering vectors (might be modified by a post-ordering)  */
  /*     ndom : number of domain obtained                                                   */
  /*   mapptr, mapp: pointer to the graph decomposition                                     */
  /* dom2rangtab : [dom2rangtab[d]:dom2rangtab[d+1]-1] is the list of  cblk in domain d     */
  /*rangtab[i] is the first column of cblk i (in the global matrix)                         */
  /*        (last column = rangtab[i+1]-1)                                                  */
  /* cblktreetab[i] is the number of the cblk father of cblk i                              */
  /* if cblktreetab[i] == -1 then it is a root                                              */
  /*                                                                                        */
  /******************************************************************************************/


  dim_t cblknbr;
  int * rangtab; /* Size n+1 */
  dim_t *treetab;
  int ind, i, j;
  dim_t *perm2, *iperm2;
  int *node2cblk;
  int *cblkperm, *cblkiperm;
  int newcblknbr;

  
  /** Convert the matrix in C numbering (start from 0 instead of 1) **/
  if(numflag == 1)
    CSR_Fnum2Cnum(ja, ia, n);
  
  if(verbose >= 1)
    fprintfd(stdout, "Compute the supernode partition of the HID ordering \n");

  
  rangtab = (int *)malloc(sizeof(int)*(n+1));
  treetab = (int *)malloc(sizeof(int)*(n+1));
  perm2 = (int *)malloc(sizeof(int)*n);
  iperm2 = (int *)malloc(sizeof(int)*n);
  node2cblk = (int *)malloc(sizeof(int)*n);

  memcpy(perm2, perm, sizeof(int)*n);
  memcpy(iperm2, iperm, sizeof(int)*n);

  /*** Find the supernode only in the interior domain node set ****/
  find_supernodes(BL->block_index[BL->block_levelindex[1]], 
    n, ia, ja, perm2, iperm2, &cblknbr, rangtab, treetab);
  /* find_supernodes(n, n, ia, ja, perm2, iperm2, &cblknbr, rangtab, treetab);*/
  fprintfd(stderr, "Number of supernodes found in interior domain = %d \n", cblknbr);

  for(i=0;i<cblknbr;i++)
    {
      for(j=rangtab[i];j<rangtab[i+1];j++)
	node2cblk[iperm2[j]] = i;
    }

  /** Update perm and iperm for the level 0 (interior domain) **/
  /** @@@@OIMBE This can be LONG for big domain !!! ***/
  if(BL->nblock > 1)
    for(i=BL->block_levelindex[0];i<BL->block_levelindex[1];i++)
      quicksort_int( iperm, BL->block_index[i], BL->block_index[i+1]-1, perm2); 
  else
    memcpy(iperm, iperm2, sizeof(dim_t)*n);


  for(i=0;i<n;i++)
    perm[iperm[i]] = i; 

#ifdef DEBUG_M
  /** Check the permutation **/
  bzero(perm2, sizeof(int)*n);
  bzero(iperm2, sizeof(int)*n);
  for(i=0;i<n;i++)
    {
      iperm2[iperm[i]]++;
      perm2[perm[i]]++;
    }
  for(i=0;i<n;i++)
    {
      assert(iperm2[i] == 1);
      assert(perm2[i] == 1);
    }
#endif

  free(perm2);
  free(iperm2);

  /** Count the total number of supernodes in level 0 **/
  *dom2cblktab = (int *)malloc(sizeof(int)*(BL->locndom+1));
  cblkperm = (int *)malloc(sizeof(int)*cblknbr);
  cblkiperm = (int *)malloc(sizeof(int)*cblknbr);

  for(i=0;i<cblknbr;i++)
    cblkperm[i] = -1;

  ind = 0;
  for(j=0;j<BL->block_levelindex[1];j++)
    {
      (*dom2cblktab)[j] = ind;

      i = BL->block_index[j];
      cblkperm[ node2cblk[iperm[i]] ] = ind;
      cblkiperm[ind] =  node2cblk[iperm[i]];
      ind++;
      for(i=BL->block_index[j]+1;i<BL->block_index[j+1];i++)
	if(node2cblk[iperm[i]] != node2cblk[iperm[i-1]])
	  {
#ifdef DEBUG_M
	    assert(node2cblk[iperm[i]] >= 0);
	    assert(node2cblk[iperm[i]] < cblknbr);
#endif
	    cblkperm[ node2cblk[iperm[i]] ] = ind;
	    cblkiperm[ind] =  node2cblk[iperm[i]];
	    ind++;
	  }
    }
  newcblknbr = ind;
#ifdef DEBUG_M
  assert(newcblknbr <= cblknbr);
#endif

  (*dom2cblktab)[BL->locndom] = ind;

#ifdef DEBUG_M
  /** Count the total number of supernodes in the interior sudomain
      **/
  j = 1;
  for(i=1;i<BL->block_index[BL->block_levelindex[1]];i++)
    if(node2cblk[iperm[i]] != node2cblk[iperm[i-1]])
      j++;
  /** If this equality is false it means that some supernodes are
      splitted between interior subdomain: that is not possible **/
  assert(ind == j);
#endif
  free(node2cblk);

  /*********************************************/
  /* Compute dom2cblktab, cblktab and treetab  */
  /*********************************************/
  *cblktab = (int *)malloc(sizeof(int)*(newcblknbr+1));
  *cblktreetab = (int *)malloc(sizeof(int)*(newcblknbr));
  ind = 0;
  for(i=0;i<newcblknbr;i++)
    {
      (*cblktab)[i] = ind;
      j = cblkiperm[i];

      ind += rangtab[j+1]-rangtab[j]; 

      if(treetab[j] >= 0)
	(*cblktreetab)[i] = cblkperm[treetab[j]];
      else
	(*cblktreetab)[i] = -1;
    }
  (*cblktab)[newcblknbr] = ind;

  free(cblkiperm);
  free(cblkperm);
  free(treetab);
  free(rangtab);

  if(verbose >= 1)
    fprintfd(stdout, "Number of supernodes found in the level 0 = %d \n", newcblknbr);


#ifdef DEBUG_M
  /** check that all the supernodes have their father in the same
	 subdomain **/
     for(i=0;i<BL->locndom;i++)
       {
	 for(j=(*dom2cblktab)[i];j<(*dom2cblktab)[i+1];j++)
	   assert((*cblktreetab)[j] < (*dom2cblktab)[i+1]);
       }
#endif

  /*exit(0);*/
  if(numflag == 1)
    /** Reconvert the matrix in Fortran numbering (start from 0 instead of 1) **/
    CSR_Cnum2Fnum(ja, ia, n);
}

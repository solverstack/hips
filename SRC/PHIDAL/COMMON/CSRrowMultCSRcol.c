/* @authors P. HENON */

#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <stdio.h>

#include "phidal_common.h"


void CSRrowMultCSRcol(REAL droptol, REAL *droptab, REAL fillrat, dim_t nnb, COEF alpha, csptr *X, csptr *Y, csptr m, int mncol, int *wki1, int *wki2, COEF *wkd)
{
  /*************************************************************/
  /* This function performs the "dot product" of two vectors   */
  /* containing CSR matrix : m = m + alpha.dot(X, Y)           */
  /*************************************************************/
  int *jrev;
  int *tmpj;
  COEF *tmpa;
  int *rowj;
  COEF *rowa;
  int *xrowj, *yrowj;
  COEF *xrowa, *yrowa;
  int xrown, yrown;
  int ynnzr;
  int i, j, k, ii, jj;
  int lenr;
  int jcol, jpos;
  csptr Xk, Yk;
  REAL d;
  long nnzM;
  REAL maxfill=-1;
  flag_t fillrow;
  Queue queue;
#ifdef ALLOC_COMPACT_ILUT
  int memsize;
#else
  int small = 0;
#endif
  dim_t *jatab=NULL;
  COEF *matab=NULL;


  if(nnb == 0)
    return;

#ifdef ALLOC_COMPACT_ILUT
  memsize = MAX((int)(CSnnz(m)*ALLOC_COMPACT_INIT), mncol);
  jatab = (int *)malloc(sizeof(int)*memsize);
  matab = (COEF *)malloc(sizeof(COEF)*memsize);
#else
  small = 0;
  if((REAL)m->n*(REAL)mncol <= (REAL)SMALLBLOCK)
    {
      small = 1;
      jatab = (int *)malloc(sizeof(int)*(m->n*mncol));
      matab = (COEF *)malloc(sizeof(COEF)*(m->n*mncol));
    }
#endif

  jrev = wki1;

#ifdef ALLOC_COMPACT_ILUT
   tmpj = jatab;
   tmpa = matab;
#else
  if(small==1)
    {
      tmpj = jatab;
      tmpa = matab;
    }
  else
    {
      tmpj = wki2;
      tmpa = wkd;
    }
#endif  
  
  if(fillrat > 0)
    {
      k = 0;
      for(i=0;i<m->n;i++)
	if(m->nnzrow[i] > k)
	  k = m->nnzrow[i];
      queueInit(&queue, (int)(k*fillrat));
      
      nnzM = CSnnz(m);
      maxfill = nnzM*fillrat;
    }



  /*** Init jrev ***/
  for(i=0;i<mncol;i++)
    jrev[i] = -1;


  nnzM = 0;
  for(i=0;i<m->n;i++)
    {
#ifdef ALLOC_COMPACT_ILUT
      if(nnzM + mncol > memsize)
	{
	  memsize = MAX(memsize*ALLOC_COMPACT_RATIO, (nnzM+mncol));
	  jatab = (int *)realloc(jatab, sizeof(int)*memsize);
	  matab = (COEF *)realloc(matab, sizeof(COEF)*memsize);
	  assert(jatab != NULL);
	  assert(matab != NULL);
	}
      tmpj = jatab + nnzM;
      tmpa = matab + nnzM;
#endif
      rowj = m->ja[i];
      rowa = m->ma[i];
      lenr = m->nnzrow[i];
      /*** Unpack row i of m in jrev, tmpj, tmpa ***/
      for(j=0;j<lenr;j++)
	jrev[rowj[j]] = j;
      if(lenr>0)
	{
	  memcpy(tmpj, rowj, sizeof(int)*lenr);
	  /*memcpy(tmpa, rowa, sizeof(COEF)*lenr);*/
	  bzero(tmpa, sizeof(COEF)*lenr);
	}

      for(k = 0; k < nnb; k++)
	{
	  Xk = X[k];
	  xrown = Xk->nnzrow[i];
	
	
	  if(xrown == 0)
	    continue;

	  Yk = Y[k];
	  ynnzr = Yk->nnzr;
	  xrowj = Xk->ja[i];
	  xrowa = Xk->ma[i];

#ifdef OPTIM_FEW_NZR
	  ynzrtab = Yk->nzrtab;
	  yinzrtab = Yk->inzrtab;

	  /*** Multiply row i of G by W and substract it from row i of m ***/
	  if(xrown <= ynnzr*(log((float)xrown)/log(2.0)+1)) /** OIMBE seems
								to be an expansive tests **/
	    {
	      /*fprintfv(5, stdout, "USE LINEAR xrown %d  ynnzr %d \n", xrown, ynnzr);*/
	      ii = 0;
	      j = 0;
	      /*** Traverse all the nonzero in row i of Xk ***/
	      while(ii < xrown &&  j < ynnzr)
		{
		  jcol = xrowj[ii];
		  j = Yk->inzrtab[jcol];
		  if(j>=ynnzr)
		    break;

		  icol = ynzrtab[j];
		  if(icol != jcol)
		    {
		      while(ii < xrown && xrowj[ii] < icol)
			ii++;
		      continue;
		    }
#ifdef DEBUG_M
		  assert(icol == jcol);
#endif
		  
		  d =  alpha*xrowa[ii];
		  yrowj = Yk->ja[jcol];
		  yrowa = Yk->ma[jcol];
		  yrown = Yk->nnzrow[jcol];

		  for(jj = 0;jj<yrown;jj++)
		    {
		      jcol = yrowj[jj];
#ifdef DEBUG_M
		      assert(jcol < mncol);
#endif
		      
		      jpos = jrev[jcol];
		      if(jpos >= 0)
			tmpa[jpos] += d*yrowa[jj];
		      else
			{
			  /** Fill-in element **/
			  jrev[jcol] = lenr;
			  tmpa[lenr] = d*yrowa[jj];
			  tmpj[lenr] = jcol;
			  lenr++;
			}
		    }

		  ii++;
		  j++;
		}  
	    }
	  else
	    {
	      /*** Traverse the nonzero row of Yk and search 
		   by dichotomy the next greater or equal col indices nonzero in row i of Xk ***/
	      /*fprintfv(5, stdout, "USE DICHOTOM  xrown %d  ynnzr %d \n", xrown, ynnzr);*/
	      ii = 0;
	      j = 0;
	      while(ii < xrown &&  j < ynnzr)
		{
		  icol = ynzrtab[j];

		  /** Find the nonzero x[i, j] in row x[i,:] such that j >= icol **/
		  ii = find_dichotom(icol, ii, xrown-1, xrowj);
		  if(ii == -1) /** None found **/
		    break;
		  jcol = xrowj[ii];
		  if(jcol != icol)
		    {
		      j = yinzrtab[jcol];
		      continue;
		    } 
#ifdef DEBUG_M
		  assert(icol == jcol);
#endif
		  d =  alpha*xrowa[ii];
		  yrowj = Yk->ja[jcol];
		  yrowa = Yk->ma[jcol];
		  yrown = Yk->nnzrow[jcol];
		  
		  for(jj = 0;jj<yrown;jj++)
		    {
		      jcol = yrowj[jj];
#ifdef DEBUG_M
		      assert(jcol < mncol);
#endif
		      jpos = jrev[jcol];
		    if(jpos >= 0)
		      tmpa[jpos] += d*yrowa[jj];
		    else
		      {
			/** Fill-in element **/
			jrev[jcol] = lenr;
			tmpa[lenr] = d*yrowa[jj];
			tmpj[lenr] = jcol;
			lenr++;
		      }
		    }
		  ii++;
		  j++;
		}
		
	      
	    }
#else
	  /*** Multiply row i of G by W and substract it from row i of m ***/
	  for(ii = 0; ii<xrown;ii++)
	    {
	      jcol = xrowj[ii];
	      yrown = Yk->nnzrow[jcol];
	      if(yrown == 0)
		continue;


	      d =  alpha*xrowa[ii];
	      yrowj = Yk->ja[jcol];
	      yrowa = Yk->ma[jcol];



	      for(jj = 0;jj<yrown;jj++)
		{
		  jcol = yrowj[jj];
#ifdef DEBUG_M
		  assert(jcol < mncol);
#endif
		  jpos = jrev[jcol];
		  if(jpos >= 0)
		    tmpa[jpos] += d*yrowa[jj];
		  else
		    {
		      /** Fill-in element **/
		      jrev[jcol] = lenr;
		      tmpa[lenr] = d*yrowa[jj];
		      tmpj[lenr] = jcol;
		      lenr++;
		    }
		}
	    }
#endif

	}

      /** We use a partial sum in tmpa to avoid rounding problem **/
      for(k=0;k< m->nnzrow[i];k++)
	tmpa[k]+=rowa[k];

      /** Do the steps in right order ! **/

      /*** 1 Reinit jrev ***/
      for(k=0;k<lenr;k++)
	jrev[tmpj[k]] = -1;

      /*** 2 Dropping in the row  ***/
      if(droptab != NULL)
	d = droptab[i]*droptol;
      else
	d = droptol;

      jpos = 0; 
      for(k=0;k<lenr;k++)
	if(fabs(tmpa[k])> d)
	  {
	    tmpj[jpos] = tmpj[k];
	    tmpa[jpos] = tmpa[k];
	    jpos++;
	  }
      lenr = jpos;


   
      /**** 3 Keep only the largest entries in the row according to fillrat ***/
      if(fillrat > 0)
	{
	  fillrow = (int) ( ((REAL)maxfill)/(m->n-i));
	  vec_filldrop(&lenr, tmpj, tmpa, fillrow, &queue);
	  maxfill -= lenr;
	}

      /*** 4 Create the new row i in m ***/
      if(m->inarow != 1 && m->nnzrow[i] > 0)
	{
	  free(m->ja[i]);
	  free(m->ma[i]);
	}
      m->nnzrow[i] = lenr;

#ifdef DEBUG_M
      assert(lenr <= mncol);
#endif

#ifdef ALLOC_COMPACT_ILUT
      nnzM += (long)lenr;
#else
      if(lenr > 0)
	{
	  if(small==1)
	    {
	      m->ja[i] = tmpj;
	      m->ma[i] = tmpa;
	      tmpj += lenr;
	      tmpa += lenr;
	      nnzM += (long)lenr;
	    }
	  else
	    {
	      m->ja[i] = (int *)malloc(sizeof(int)*lenr);
	      memcpy(m->ja[i], tmpj, sizeof(int)*lenr);
	      m->ma[i] = (COEF *)malloc(sizeof(COEF)*lenr);
	      memcpy(m->ma[i], tmpa, sizeof(COEF)*lenr);
	    }
	}
#endif 
      
    }
  
  if(fillrat > 0)
    queueExit(&queue);

  if(m->inarow == 1)
    {
      if(m->matab != NULL)
	free(m->matab);
      if(m->jatab != NULL)
	free(m->jatab);
      m->inarow = 0;
    }
  CS_SetNonZeroRow(m);

#ifndef ALLOC_COMPACT_ILUT
  if(small == 1)
#endif
    {
      matab = (COEF *)realloc(matab, sizeof(COEF)*nnzM);
      jatab = (int *)realloc(jatab, sizeof(int)*nnzM);
      m->matab = matab;
      m->jatab = jatab;
      nnzM = 0;
      for(k=0;k<m->nnzr;k++)
	{
	  i = m->nzrtab[k];
	  m->ja[i] = jatab+nnzM;
	  m->ma[i] = matab+nnzM;
	  nnzM += m->nnzrow[i];
	}
      m->inarow = 1;
    }

}





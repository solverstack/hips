/* @authors P. HENON */

#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <stdio.h>

#include "phidal_common.h"

int RowColcompact(dim_t nb, csptr *X, csptr *Y, csptr x, csptr y, int *wki1, int *wki2);

void CSCrowMultCSRcol(REAL droptol, REAL *droptab, REAL fillrat, dim_t nnb, COEF alpha, csptr *X, csptr *Y, csptr m, dim_t mnrow, int *wki1, int *wki2, COEF *wkd, cell_int *celltab, cell_int **BcolList)
{
  /*************************************************************/
  /* This function performs the "dot product" of two vectors   */
  /* X is a row of CSC matrices                                */
  /* Y is a column of CSR matrices                             */
  /* m = m - dot(X, Y)                                         */
  /* NOTE: each matrices of Y must be  SORTED by column indices*/
  /*************************************************************/
  struct SparRow x; 
  struct SparRow y;

  /*if(nnb == 0)
    return;*/

#ifdef DEBUG_M
  {
    dim_t k;
    for(k=0;k<nnb;k++)
      CS_Check(X[k], mnrow);
    for(k=0;k<nnb;k++)
      CS_Check(Y[k], m->n);

    CS_Check(m, mnrow);
  }
#endif
  x.n = 0;
  y.n = 0;

  /*x = (csptr)malloc(sizeof(struct SparRow));
    y = (csptr)malloc(sizeof(struct SparRow)); */

  /*CS_VirtualMerge(nnb, X, x);
    CS_VirtualMerge(nnb, Y, y);*/
  if(RowColcompact(nnb, X, Y, &x, &y, wki1, wki2) > 0)
    { 
      /** m = m + alpha.x.y **/
      CSCxCSR_CSC_GEMMT(alpha, &x, &y, m, mnrow, droptol, droptab, fillrat, wki1, wki2, wkd, celltab, BcolList);
      /** Reinit the pointer of x and y to no delete them in cleanCS **/
      /*bzero(x->nnzrow, sizeof(int)*x->n);
	bzero(x->ja, sizeof(int *)*x->n);
	bzero(x->ma, sizeof(COEF *)*x->n);
	
	bzero(y->nnzrow, sizeof(int)*y->n);
	bzero(y->ja, sizeof(int *)*y->n);
	bzero(y->ma, sizeof(COEF *)*y->n);
	cleanCS(x);
	cleanCS(y);*/

      /** Destroy x and y : DO NOT USE cleanCS (it uses a lot of time in this case) **/
      if(x.nnzrow) free(x.nnzrow);
      if(y.nnzrow) free(y.nnzrow);
      if(x.ja) free(x.ja);
      if(x.ma) free(x.ma);
      if(y.ja) free(y.ja);
      if(y.ma) free(y.ma);
    }
  /*free(x);
    free(y);*/

}


int RowColcompact(dim_t nb, csptr *X, csptr *Y, csptr x, csptr y, int *wki1, int *wki2)
{
  /*******************************************************/
  /* This function merges several CS  matrices           */  
  /* into a single one                                   */
  /* NOTE the returned matrix is virtual i.e.            */
  /* it is a pointer to rows in the mattab matrices      */
  /*******************************************************/

  csptr mx, my;
  dim_t *xnnzrow, *ynnzrow, *mxnnzrow, *mynnzrow;
  dim_t **xja, **yja, **mxja, **myja;
  COEF **xma, **yma, **mxma, **myma;

  INTL i, j, k, ind;
  dim_t n;
  
  n = 0;
  for(i=0;i<nb;i++)
    {
      mx = X[i];
      my = Y[i];

      GetCommonRows(mx, my, wki1+i, wki2+n);
      n += wki1[i];
    }

  if(n==0)
    return 0;
  

  /*initCS(x, n);
    initCS(y, n);*/
  x->n = n;
  y->n = n;
  x->nnzrow = (dim_t *)malloc(sizeof(dim_t)*n);
  y->nnzrow = (dim_t *)malloc(sizeof(dim_t)*n);
  x->ja = (dim_t **)malloc(sizeof(dim_t *)*n);
  x->ma = (COEF **)malloc(sizeof(COEF *)*n);
  y->ja = (dim_t **)malloc(sizeof(dim_t *)*n);
  y->ma = (COEF **)malloc(sizeof(COEF *)*n);

  xnnzrow = x->nnzrow;
  ynnzrow = y->nnzrow;
  xja = x->ja;
  yja = y->ja;
  xma = x->ma;
  yma = y->ma;

  ind = 0;
  for(i=0;i<nb;i++)
    {
      mx = X[i];
      my = Y[i];

      mxnnzrow = mx->nnzrow;
      mynnzrow = my->nnzrow;
      mxja = mx->ja;
      myja = my->ja;
      mxma = mx->ma;
      myma = my->ma;

      for(k=0;k<wki1[i];k++)
	{
	  j = wki2[ind];
	  xnnzrow[ind] = mxnnzrow[j];
	  xja[ind] = mxja[j];
	  xma[ind] = mxma[j];
	  ynnzrow[ind] = mynnzrow[j];
	  yja[ind] = myja[j];
	  yma[ind] = myma[j];

	  ind++;
	}
      
    }
#ifdef DEBUG_M
  assert(ind == n);
#endif

  return n;
}  





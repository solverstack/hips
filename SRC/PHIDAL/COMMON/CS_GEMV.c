/* @authors P. HENON */

#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <stdio.h>

#include "phidal_common.h"

void matvec(csptr mata, COEF *x, COEF *y)
{
/*---------------------------------------------------------------------
| This function does the matrix vector product y = A x.
|----------------------------------------------------------------------
| on entry:
| mata  = the matrix (in SparRow form)
| x     = a vector
|
| on return
| y     = the product A * x
|--------------------------------------------------------------------*/
/*   local variables    */
  INTL i,j, k;
  dim_t *ki;
  COEF *kr;

   bzero(y, sizeof(COEF)*mata->n);
   /*for (i=0; i<mata->n; i++) */

   for(j=0;j<mata->nnzr;j++)
     {
       i = mata->nzrtab[j];

       kr = mata->ma[i];
       ki = mata->ja[i];
       for (k=0; k<mata->nnzrow[i]; k++)
         y[i] += kr[k] * x[ki[k]];
     }
   return;
}

void matvec_add(csptr mata, COEF *x, COEF *y)
{
/*---------------------------------------------------------------------
| This function does the matrix vector product y = y + A x.
|----------------------------------------------------------------------
| on entry:
| mata  = the matrix (in SparRow form)
| x     = a vector
|
| on return
| y     = the product A * x
|--------------------------------------------------------------------*/
/*   local variables    */
  INTL i,j, k;
  dim_t *ki;
  COEF *kr;
  COEF t; /** Very important to use a temporary real t to store the partial sum 
		because of the rouding error **/
  /*for (i=0; i<mata->n; i++) */
  for(j=0;j<mata->nnzr;j++)
    {
      i = mata->nzrtab[j];
      kr = mata->ma[i];
      ki = mata->ja[i];
      t = 0.0;
      for (k=0; k<mata->nnzrow[i]; k++)
	t += kr[k] * x[ki[k]];
      y[i] += t;
    }
  return;
}

void NoDiag_matvec_add(csptr mata, COEF *x, COEF *y)
{
/*---------------------------------------------------------------------
| This function does the matrix vector product y = y + A x.
|----------------------------------------------------------------------
| on entry:
| mata  = the matrix (in SparRow form)
| x     = a vector
|
| on return
| y     = the product A * x
|--------------------------------------------------------------------*/
/*   local variables    */
  INTL i, j, k;
  dim_t *ki;
  COEF *kr;
  COEF t; /** Very important to use a temporary real t to store the partial sum 
		 because of the rouding error **/
   /*for (i=0; i<mata->n; i++) */
   for(j=0;j<mata->nnzr;j++)
     {
       i=mata->nzrtab[j];
       kr = mata->ma[i];
       ki = mata->ja[i];
       t = 0.0;
       for (k=0; k<mata->nnzrow[i]; k++)
	 if(ki[k] != i)
	   t += kr[k] * x[ki[k]];
       
       /*for(k=0;k<mata->nnzrow[i];k++)
	 if(ki[k]==i)
	 {
	 t -= kr[k] * x[i];
	 break;
	 }*/
       
       y[i] += t;
     }
   return;
}


/*---------------end of matvec------------------------------------------
----------------------------------------------------------------------*/
void matvecz(csptr mata, COEF *x, COEF *y, COEF *z) 
{
/*---------------------------------------------------------------------
| This function does the matrix vector  z = y - A x.
|----------------------------------------------------------------------
| on entry:
| mata  = the matrix (in SparRow form)
| x, y   = two input vector
|
| on return
| z    = the result:  y - A * x
|--------------------------------------------------------------------*/
/*   local variables    */
  INTL i, j, k;
  dim_t *ki;
   COEF *kr, t; /** Very important to use a temporary real t to store the partial sum 
		 because of the rouding error **/

   /*for (i=0; i<mata->n; i++) */
   for(j=0;j<mata->nnzr;j++)
     {
       i = mata->nzrtab[j];
       kr = mata->ma[i];
       ki = mata->ja[i];
       t = 0.0;
       for (k=0; k<mata->nnzrow[i]; k++)
         t += kr[k] * x[ki[k]];
       z[i] = y[i] - t; 
     }
   return;
}





void CSC_matvec_add(csptr mata, COEF *x, COEF *y)
{
/*---------------------------------------------------------------------
| This function does the matrix vector product y = y + A x.
|----------------------------------------------------------------------
| on entry:
| mata  = the matrix (in SparRow form)
| x     = a vector
|
| on return
| y     = the product A * x
|--------------------------------------------------------------------*/
/*   local variables    */
  INTL i, j, k;
  dim_t *ki;
  COEF *kr, t;
  /*for (i=0; i<mata->n; i++) */
  for(j=0;j<mata->nnzr;j++)
    {
      i = mata->nzrtab[j];
      kr = mata->ma[i];
      ki = mata->ja[i];
      t = x[i] ;
      for (k=0; k<mata->nnzrow[i]; k++)
	y[ki[k]] += t * kr[k];
    }

}


void CSC_NoDiag_matvec_add(csptr mata, COEF *x, COEF *y)
{
/*---------------------------------------------------------------------
| This function does the matrix vector product y = y + (A-Diag(A)) x.
| This is used when A is a lower triangular matrix and we do no want to take into account the diagonal 
|----------------------------------------------------------------------
| on entry:
| mata  = the matrix (in SparRow form)
| x     = a vector
|
| on return
| y     = the product A * x
|--------------------------------------------------------------------*/
/*   local variables    */
  INTL i, j, k;
  dim_t *ki;
  COEF *kr, t;
  /*for (i=0; i<mata->n; i++) */

   for(j=0;j<mata->nnzr;j++)
     {
       i = mata->nzrtab[j];
       kr = mata->ma[i];
       ki = mata->ja[i];
       t = x[i] ;
       for (k=0; k<mata->nnzrow[i]; k++)
	 if(ki[k] != i)
	   y[ki[k]] += t * kr[k];
       
       /*for(k=0;k<mata->nnzrow[i];k++)
	 if(ki[k]==i)
	 {
	 y[i] -= t * kr[k];
	 break;
	 }*/
       
     }
   
}


void CSC_matvecz(csptr mata, COEF *x, COEF *y, COEF *z) 
{
/*---------------------------------------------------------------------
| This function does the matrix vector  z = y - A x.
| with mata a CSC matrix
|----------------------------------------------------------------------
| on entry:
| mata  = the matrix (in SparRow form)
| x, y   = two input vector
|
| on return
| z    = the result:  y - A * x
|--------------------------------------------------------------------*/
/*   local variables    */
  INTL i, j, k;
  dim_t *ki;
   COEF *kr, t;
   /*for (i=0; i<mata->n; i++) 
     {*/
    
   for(j=0;j<mata->nnzr;j++)
     {
       i = mata->nzrtab[j];    
       kr = mata->ma[i];
       ki = mata->ja[i];
       t = x[i] ;
       for (k=0; k<mata->nnzrow[i]; k++)
         z[ki[k]] = y[ki[k]] - t * kr[k];
     }
   return;
}

/* @authors P. HENON */

#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <stdio.h>
#include <assert.h>

#include "phidal_common.h"

void GetCommonRows_Fast(csptr a1, csptr a2, int *nr, int *nrtab);
void GetCommonRows_Seq(csptr a1, csptr a2, int *nr, int *nrtab);

void GetCommonRows(csptr a1, csptr a2, int *nr, int *nrtab)
{
  if(a1->nnzr < a1->n/4 || a2->nnzr < a2->n/4)
    GetCommonRows_Fast(a1, a2, nr, nrtab);
  else
    GetCommonRows_Seq(a1, a2, nr, nrtab);
}


void GetCommonRows_Fast(csptr a1, csptr a2, int *nr, int *nrtab)
{
  int i1, i2, r, q;
  dim_t nnzr1, nnzr2;
  int *nzrtab1, *inzrtab1, *nzrtab2, *inzrtab2;
  int ind;

  nnzr1 = a1->nnzr;
  nnzr2 = a2->nnzr;
  nzrtab1 = a1->nzrtab;
  nzrtab2 = a2->nzrtab;
  inzrtab1 = a1->inzrtab;
  inzrtab2 = a2->inzrtab;
  
  i1 = 0;
  i2 = 0;
  ind = 0;
  while(i1 < nnzr1 && i2 < nnzr2)
    {
      r = nzrtab1[i1];
      i2 = inzrtab2[r];
      if(i2>=nnzr2)
	break;
      
      q = nzrtab2[i2];
      if(q != r)
	{
	  i1 = inzrtab1[q];
	  continue;
	}
      
#ifdef DEBUG_M
      assert(r == q);
      assert(r < a1->n && q < a2->n);
#endif
      nrtab[ind++] = r;

      i1++;
      i2++;
    }
  *nr = ind;
}


void GetCommonRows_Seq(csptr a1, csptr a2, int *nr, int *nrtab)
{
  dim_t i;
  int ind;
  int *nnz1, *nnz2;
  dim_t s, e;

  if(a1->nnzr == 0 || a2->nnzr == 0)
    {
      *nr = 0;
      return;
    }

  nnz1 = a1->nnzrow;
  nnz2 = a2->nnzrow;

  s = MAX(a1->nzrtab[0], a2->nzrtab[0]);
  e = MIN(a1->nzrtab[a1->nnzr-1], a2->nzrtab[a2->nnzr-1]);
  ind = 0;
  for(i=s;i<=e;i++)
    if(nnz1[i] > 0 && nnz2[i]>0)
      nrtab[ind++] = i;

  *nr = ind;
}


int  find_dichotom(int col, int start, int end, dim_t *ja)
{
  /**********************************************************/
  /* Return the indices start <= i <= end such that         */
  /* xrowj[i] >= icol and i minimal                         */
  /* If none is found then return -1                        */
  /**********************************************************/
  dim_t s, e, m;
  
  s = start;
  e = end;
#ifdef DEBUG_M
  assert(end >= start);
#endif
  do
    {
      if(ja[s] >= col)
	return s;
      if(ja[e] < col)
	return -1;
      
      m = (s+e)/2;
      if(ja[m] >= col)
	e = m;
      else
	s = m;
      /*fprintfv(5, stderr, "s (%d, %d) e (%d, %d) m (%d %d) col %d \n", s, ja[s], e, ja[e], m, ja[m], col);*/
    }
  while(e-s > 1);

#ifdef DEBUG_M
  assert(e >= start && e <= end);
#endif
  return e;
}



void IntersectRow(dim_t n1, dim_t *ja1, dim_t n2, dim_t *ja2, int *n, int *indtab)
{
  /************************************************************/
  /* This function find the intersection between two row      */
  /* and return the set of indice i of the first row          */
  /* where ja1[i] == ja2[i]                                   */
  /************************************************************/
  int i1, i2;
#ifdef LOG_SEARCH
  int j1, j2;
#endif
  int s1, s2, e1, e2;
  int ind;


  /**** Find the space search in row1 and row2 *****/
  if(n1 == 0 || n2 == 0)
    {
      *n = 0;
      return;
    }

  if(ja1[0] >= ja2[0])
    {
      s1 = 0;
      s2 =  find_dichotom(ja1[0], 0, n2-1, ja2);
      if(s2 < 0)
	{
	  *n = 0;
	  return;
	}
    }
  else
    {
      s2 = 0;
      s1 =  find_dichotom(ja2[0], 0, n1-1, ja1);
      if(s1 < 0)
	{
	  *n = 0;
	  return;
	}
    }

  if(ja1[n1-1] <= ja2[n2-1])
    {
      e1 = n1-1;
      e2 = find_dichotom(ja1[e1], s2, n2-1, ja2);
#ifdef DEBUG_M
      assert(e2 >= 0);
#endif
    }
  else
    {
      e2 = n2-1;
      e1 = find_dichotom(ja2[e2], s1, n1-1, ja1);
#ifdef DEBUG_M
      assert(e1 >= 0);
#endif
    }
  
  /*** Now we intersect ja1[s1:e1] and ja2[s2:e2] ***/
  
  i1 = s1;
  i2 = s2;
  ind = 0;

#ifdef LOG_SEARCH
  while(i1 <= e1 && i2 <= e2)
    {
      j1 = ja1[i1];
      i2 = find_dichotom(j1, i2, e2, ja2);
      if(i2 < 0)
	break;
      j2 = ja2[i2];
      if(j1 != j2)
	{
	  i1 = find_dichotom(j2, i1, e1, ja1);
	  if(i1 < 0)
	    break;
	  continue;
	}

      /** j1 == j2 **/
      indtab[ind++] = i1;
      i1++;
      i2++;
    }
#else /* Sequential Search */
  while(i1 <= e1 && i2 <= s2)
    {
      if(ja1[i1] < ja2[i2])
	{
	  i1++;
	  continue;
	}
      if(ja1[i1] > ja2[i2])
	{
	  i2++;
	  continue;
	}
      /** j1 == j2 **/
      indtab[ind++] = i1;
      i1++;
      i2++;
    }
#endif

  *n = ind;
  return;
			     
}

/*#define DICHOTOM*/

#ifdef  DICHOTOM 
void CS_IntersectRow(dim_t n1, dim_t *ja1, csptr *ma1, dim_t n2, dim_t *ja2, csptr *ma2, int *n, csptr *wkcs1, csptr *wkcs2)
{
  /**************************************************************/
  /* This function return the intersection of two csr row       */
  /* it intersect two row R1 and R2                             */
  /*       the resulting intersection is wkcs1 and wkcs2        */
  /* (of size nb) that                                          */
  /* contains the cs matrices that have the same j indice in    */
  /* R1 and R2                                                  */
  /* NOTE the array ja1 and ja2 must sorted !                   */
  /**************************************************************/
  /************************************************************/
  int i1, i2, j1, j2;
  int s1, s2, e1, e2;
  int ind;


  /**** Find the space search in row1 and row2 *****/
  if(n1 == 0 || n2 == 0)
    {
      *n = 0;
      return;
    }

  if(ja1[0] >= ja2[0])
    {
      s1 = 0;
      s2 =  find_dichotom(ja1[0], 0, n2-1, ja2);
      if(s2 < 0)
	{
	  *n = 0;
	  return;
	}
    }
  else
    {
      s2 = 0;
      s1 =  find_dichotom(ja2[0], 0, n1-1, ja1);
      if(s1 < 0)
	{
	  *n = 0;
	  return;
	}
    }
  
  if(ja1[n1-1] <= ja2[n2-1])
    {
      e1 = n1-1;
      e2 = find_dichotom(ja1[e1], s2, n2-1, ja2);
#ifdef DEBUG_M
      assert(e2 >= 0);
#endif
    }
  else
    {
      e2 = n2-1;
      e1 = find_dichotom(ja2[e2], s1, n1-1, ja1);
#ifdef DEBUG_M
      assert(e1 >= 0);
#endif
    }
  
  /*** Now we intersect ja1[s1:e1] and ja2[s2:e2] ***/
  
  i1 = s1;
  i2 = s2;
  ind = 0;
  while(i1 <= e1 && i2 <= e2)
    {
      j1 = ja1[i1];
      i2 = find_dichotom(j1, i2, e2, ja2);
      if(i2 < 0)
	break;
      j2 = ja2[i2];
      if(j1 != j2)
	{
	  i1 = find_dichotom(j2, i1, e1, ja1);
	  if(i1 < 0)
	    break;
	  continue;
	}
      
      /** j1 == j2 **/
      if(ma1[i1]->nnzr > 0 && ma2[i2]->nnzr > 0)
	{
	  wkcs1[ind] = ma1[i1];
	  wkcs2[ind] = ma2[i2];
	  ind++;
	}
      i1++;
      i2++;
    }
  
  *n = ind;
  return;
  
  
}


#else
void CS_IntersectRow(dim_t n1, dim_t *ja1, csptr *ma1, dim_t n2, dim_t *ja2, csptr *ma2, int *nb, csptr *wkcs1, csptr *wkcs2)
{
  /**************************************************************/
  /* This function return the intersection of two csr row       */
  /* it intersect two row R1 and R2                             */
  /*       the resulting intersection is wkcs1 and wkcs2        */
  /* (of size nb) that                                          */
  /* contains the cs matrices that have the same j indice in    */
  /* R1 and R2                                                  */
  /* NOTE the array ja1 and ja2 must sorted !                   */
  /**************************************************************/
  int i1, i2;
  int ind ;

  i2 = 0;
  ind = 0;
  for(i1=0;i1<n1;i1++)
    {
      while(i2<n2)
	{
	  if(ja2[i2]>= ja1[i1])
	    break;
	  i2++;
	}
      if(i2 >= n2)
	break;

      if(ja2[i2] == ja1[i1])
	{
	  if(ma1[i1]->nnzr == 0 || ma2[i2]->nnzr == 0)
	    {
	      i2++;
	      continue;
	    }

	  wkcs1[ind] = ma1[i1];
	  wkcs2[ind] = ma2[i2];
	  ind++;
	  i2++;
	}
    }
  
  *nb = ind;
  
}
#endif

void UnionSet(int *set1, dim_t n1, int *set2, dim_t n2, int *set, int *n)
{
  /********************************************************/
  /* Compute the union of two sorted set                  */
  /* set must have a big enough size to contain the union */
  /* i.e n1+n2                                            */
  /********************************************************/
  int ind, ind1, ind2;

  ind = 0;
  ind1 = 0;
  ind2 = 0;

#ifdef DEBUG_M
 {
   dim_t i;
   for(i=0;i<n1-1;i++)
     assert(set1[i] < set1[i+1]);
   for(i=0;i<n2-1;i++)
     assert(set2[i] < set2[i+1]);
 }
#endif

  while(ind1 < n1 && ind2 < n2)
    {
      if(set1[ind1] == set2[ind2])
	{
	  set[ind] = set1[ind1];
	  ind++;
	  ind1++;
	  ind2++;

	  continue;
	}
      
      if(set1[ind1] < set2[ind2])
	{
	  set[ind] = set1[ind1];
	  ind++;
	  ind1++;
	  continue;
	}
      
      if(set1[ind1] > set2[ind2])
	{
	  set[ind] = set2[ind2];
	  ind++;
	  ind2++;
	  continue;
	}
    }

  while(ind1 < n1)
    {
      set[ind] = set1[ind1];
      ind++;
      ind1++;
    }
  while(ind2 < n2)
    {
      set[ind] = set2[ind2];
      ind++;
      ind2++;
    }
#ifdef DEBUG_M
  assert(ind <= ind1 +ind2);
  assert(ind >= MAX(ind1, ind2));
  {
    dim_t i;
    for(i=0;i<ind-1;i++)
      assert(set[i] < set[i+1]);
  }
#endif


  *n = ind;
}



void union_set(int *set1, dim_t n1, int *set2, dim_t n2, int **set, int *n)
{
  /********************************************************/
  /* Compute the union of two sorted set                  */
  /********************************************************/
  int ind, ind1, ind2;
  int *tmp;

  ind = 0;
  ind1 = 0;
  ind2 = 0;

  tmp = (int *)malloc(sizeof(int)*(n1+n2));

  while(ind1 < n1 && ind2 < n2)
    {
      if(set1[ind1] == set2[ind2])
	{
	  tmp[ind] = set1[ind1];
	  ind++;
	  ind1++;
	  ind2++;
	  continue;
	}
      
      if(set1[ind1] < set2[ind2])
	{
	  tmp[ind] = set1[ind1];
	  ind++;
	  ind1++;
	  continue;
	}
      
      if(set1[ind1] > set2[ind2])
	{
	  tmp[ind] = set2[ind2];
	  ind++;
	  ind2++;
	  continue;
	}
    }

  while(ind1 < n1)
    {
      tmp[ind] = set1[ind1];
      ind++;
      ind1++;
    }
  while(ind2 < n2)
    {
      tmp[ind] = set2[ind2];
      ind++;
      ind2++;
    }
#ifdef DEBUG_M
  assert(ind <= ind1 +ind2);
#endif

  if(ind > 0)
    {
      (*set) = (int *)malloc(sizeof(int)*ind);
      memcpy((*set), tmp, sizeof(int)*ind);
    }
  else
    (*set) = NULL;
  *n = ind;

  free(tmp);
}

void IntersectSet(dim_t *ja1, dim_t n1, dim_t *ja2, dim_t n2, dim_t *ja, int *n)
{
  /************************************************************/
  /* This function find the intersection between two row      */
  /************************************************************/
  int i1, i2;
  int s1, s2, e1, e2;
  int ind;


  /**** Find the space search in row1 and row2 *****/
  if(n1 == 0 || n2 == 0)
    {
      *n = 0;
      return;
    }

  if(ja1[0] >= ja2[0])
    {
      s1 = 0;
      s2 =  find_dichotom(ja1[0], 0, n2-1, ja2);
      if(s2 < 0)
	{
	  *n = 0;
	  return;
	}
    }
  else
    {
      s2 = 0;
      s1 =  find_dichotom(ja2[0], 0, n1-1, ja1);
      if(s1 < 0)
	{
	  *n = 0;
	  return;
	}
    }

  if(ja1[n1-1] <= ja2[n2-1])
    {
      e1 = n1-1;
      e2 = find_dichotom(ja1[e1], s2, n2-1, ja2);
#ifdef DEBUG_M
      assert(e2 >= 0);
#endif
    }
  else
    {
      e2 = n2-1;
      e1 = find_dichotom(ja2[e2], s1, n1-1, ja1);
#ifdef DEBUG_M
      assert(e1 >= 0);
#endif
    }
  
  /*** Now we intersect ja1[s1:e1] and ja2[s2:e2] ***/
  
  i1 = s1;
  i2 = s2;
  ind = 0;

  while(i1 <= e1 && i2 <= e2)
    {
      if(ja1[i1] < ja2[i2])
	{
	  i1++;
	  continue;
	}
      if(ja1[i1] > ja2[i2])
	{
	  i2++;
	  continue;
	}
      /** j1 == j2 **/
      ja[ind++] = ja1[i1];
      i1++;
      i2++;
    }

#ifdef DEBUG_M
  assert(ind <= MIN(n1, n2));
#endif

  *n = ind;
  return;
			     
}

int set_delete(int **list, int *size, int elt)
{
  /****************************************************/
  /** Delete an element  from a set                   */
  /** Return TRUE if the element is in the set        */
  /** NB: the set  is not realloc                     */
  /****************************************************/
  dim_t i;
  int ind;
  int flag;

  flag = 0;
  ind = 0;
  for(i=0;i<(*size);i++)
    {
      if((*list)[i] == elt)
	{
	  flag = 1;
	  continue;
	}
      (*list)[ind] = (*list)[i];
      ind++;
    }
  (*size) = ind;
  return flag;
}


void set_add(int **list, int *size, int elt)
{
  /****************************************************/
  /** Add an element into a set                       */
  /** NB: the set MUST HAVE SUFFICIENT ALLOCATED SPACE*/
  /****************************************************/
  dim_t i;

  for(i=0;i<(*size);i++)
    if((*list)[i] == elt)
      return; /** elt is already in the set **/

  (*list)[(*size)] = elt;
  (*size)++;
}


/* @authors P. HENON */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "queue.h"
#include "phidal_common.h"

/*#undef  ALLOC_COMPACT_ILUT*/


void CS_ILUT(csptr A, csptr L, csptr U, REAL droptol, REAL *droptab, REAL fillrat, Heap *heap, int *wki1, int *wki2, COEF *wkd)
{
  /************************************************************************************************/
  /* This function compute an ILUT factorization of a sparse matrix                               */
  /* The dropping is contral via droptab: any term in column i of L lesser than                   */
  /* Uii*droptab[i]*droptol are dropped and any term in row i of U lesser than droptab[i]*droptol */
  /* are dropped. Usually droptab[i] contains Norm(A[i,i:n])                                      */
  /* If droptab is NULL then droptab is consider to be unitary                                    */
  /*                                                                                              */
  /* NOTE: the algorithm can work in place (i.e. L can be equals to A or U in inputs              */
  /*                                                                                              */
  /************************************************************************************************/

  /***********************************************************************************/
  /* NOTE: We assume that every row i of U contains the diagonal term in first place */
  /***********************************************************************************/
     

  dim_t i, j, k;
  int jcol, jpos;
  int *jrev, *Ltmpj=NULL, *Utmpj=NULL;  
  COEF *Ltmpa=NULL, *Utmpa=NULL;
  int *aja, *lja, *uja ,*rowjz;
  COEF *ama, *lma, *uma ,*rowaz;

  COEF d;
  REAL tt;
  COEF s;
  int lenl, lenu, nnzz;
  REAL maxfill=-1;
  flag_t fillrow;
  Queue queue;
  long nnzA;
  long nnzL, nnzU;
#ifdef ALLOC_COMPACT_ILUT
  int memsize;
#else
  int small = 0;
#endif
  int *Ljatab=NULL, *Ujatab=NULL;
  COEF *Lmatab=NULL, *Umatab=NULL;

#ifdef ALLOC_COMPACT_ILUT
  memsize = MAX((int)(CSnnz(A)*ALLOC_COMPACT_INIT), A->n);
  Ljatab = (int *)malloc(sizeof(int)*memsize);
  Lmatab = (COEF *)malloc(sizeof(COEF)*memsize);
  Ujatab = (int *)malloc(sizeof(int)*memsize);
  Umatab = (COEF *)malloc(sizeof(COEF)*memsize);
#else
  small = 0;
  if((REAL)A->n*(REAL)A->n <= (REAL)SMALLBLOCK)
    {
      small = 1;
      Ljatab = (int *)malloc(sizeof(int)*(((A->n+1)*A->n)/2)); /** avoid null size **/
      Ujatab = (int *)malloc(sizeof(int)*(((A->n+1)*A->n)/2));
      Lmatab = (COEF *)malloc(sizeof(COEF)*(((A->n+1)*A->n)/2)); /** avoid null size **/
      Umatab = (COEF *)malloc(sizeof(COEF)*(((A->n+1)*A->n)/2));
    }
#endif

  if(fillrat > 0)
    {
      k = 0;
      for(i=0;i<L->n;i++)
	if(A->nnzrow[i] > k)
	  k = A->nnzrow[i];
      queueInit(&queue, (int)(k*fillrat));
      nnzA = CSnnz(A);
      maxfill = nnzA*fillrat;
    }


  Heap_Clear(heap);
  jrev = wki1;

#ifdef ALLOC_COMPACT_ILUT
   Ltmpj = Ljatab;
   Ltmpa = Lmatab;
   Utmpj = Ujatab;
   Utmpa = Umatab;
#else
  if(small == 1)
    {
      Ltmpj = Ljatab;
      Ltmpa = Lmatab;
      Utmpj = Ujatab;
      Utmpa = Umatab;
    }
#endif

  for(j=0;j<L->n;j++) 
    jrev[j] = -1;

  nnzL = 0;
  nnzU = 0;
#ifdef ALLOC_COMPACT_ILUT
  /** We need to maintain the nzrtab during the computation 
      of U **/
  U->nnzr =0;
#endif
  for(i=0;i<A->n;i++)
    {

#ifdef ALLOC_COMPACT_ILUT
      if(nnzL + nnzU + A->n > memsize)
	{
	  memsize = MAX(memsize*ALLOC_COMPACT_RATIO, (nnzL+nnzU+A->n));
	  Ljatab = (int *)realloc(Ljatab, sizeof(int)*memsize);
	  Lmatab = (COEF *)realloc(Lmatab, sizeof(COEF)*memsize);
	  Ujatab = (int *)realloc(Ujatab, sizeof(int)*memsize);
	  Umatab = (COEF *)realloc(Umatab, sizeof(COEF)*memsize);
	  if(Ljatab == NULL || Lmatab == NULL || Ujatab == NULL || Umatab == NULL)
	    {
	      fprintferr(stderr, "Error in CS_ILUT : unable to extend the coefficient vector to memsize = %ld \n", (long)memsize);
	      fprintferr(stderr, "Try to decrease ALLOC_COMPACT_RATIO in hips_define.h");
	    }
	  
	  assert(Ljatab != NULL);
	  assert(Lmatab != NULL);
	  assert(Ujatab != NULL);
	  assert(Umatab != NULL);
	}
      Ltmpj = Ljatab + nnzL;
      Ltmpa = Lmatab + nnzL;
      Utmpj = Ujatab + nnzU;
      Utmpa = Umatab + nnzU;

      /** We need to reset U->ja and U->ma **/
      lenu = 0;
      for(j=0;j<U->nnzr;j++)
	{
	  k = U->nzrtab[j];
	  U->ja[k] = Ujatab+lenu;
	  U->ma[k] = Umatab+lenu;
	  lenu += U->nnzrow[k];
	}
#else
      if(small != 1)
	{
	  Ltmpj = wki2;
	  Ltmpa = wkd;
	  Utmpj = wki2+i;
	  Utmpa = wkd+i;
	}
#endif
      aja = A->ja[i];
      ama = A->ma[i];
      if(A->nnzrow[i] == 0)
	{
	  fprintfd(stderr, "WARNING row %ld of A is null \n", (long)i);
	}

      /*** Copy the l part and the u part of A ***/
      lenl = 0;
      lenu = 1; /** diagonal is always stored **/
      Utmpj[0] = i;
      Utmpa[0] = 0.0;

      for(k=0;k<A->nnzrow[i];k++)
	{
	  if(aja[k] < i)
	    {
	      Ltmpj[lenl] = aja[k];
	      Ltmpa[lenl] = ama[k];
	      lenl++;
	      continue;
	    }
	  if(aja[k] > i)
	    {
	      Utmpj[lenu] = aja[k];
	      Utmpa[lenu] = ama[k];
	      lenu++;
	      continue;
	    }
	  if(aja[k]==i)
	    {
	      Utmpa[0] = ama[k];
	      continue;
	    }
	}

      lja = Ltmpj;
      lma = Ltmpa;
      uja = Utmpj;
      uma = Utmpa;

      /** Init jrev **/
      for(j=0;j<lenl;j++)
	jrev[lja[j]] = j;

      for(j=0;j<lenu;j++)
	jrev[uja[j]] = j;



#ifdef DEBUG_M
      /** This is correct iff uja [0] = i **/
      assert(uja[0] == i);
#endif
   
      for(j=0;j<lenl;j++)
	Heap_Add(heap, lja[j]);
      
      while(Heap_Size(heap)>0)
	{
	  jcol = Heap_Get(heap);
	  j = jrev[jcol];
#ifdef DEBUG_M
	  assert(j>=0 && j<i);
	  assert(Ltmpj[j] == jcol);
#endif	  
	  nnzz = U->nnzrow[jcol];
	  rowjz = U->ja[jcol];
	  rowaz = U->ma[jcol];

	  d =  Ltmpa[j]*rowaz[0]; /** rowaz[0] is never null since it is the inverse of a previous diagonal term **/
	  if(droptab != NULL)
	    tt = droptab[jcol]*droptol;
	  else
	    tt = droptol;

	  if(coefabs(d) <= tt) /** Dropping in the L factor **/
	    {
	      jrev[jcol] = -1; /** Reinit jrev now for this position (can not do it later) **/
	      Ltmpj[j] = -1; /** Mark this entry to be delete **/
	      continue;
	    }

	  Ltmpa[j] = d;

	  for(k=1;k<nnzz;k++)
	    {
	      s = d*rowaz[k];
	      jcol = rowjz[k];
	      jpos = jrev[jcol];
	     
	      if(jcol < i) /** New fill-in entry in L **/
		{
		  if(jpos >= 0)
		    Ltmpa[jpos] -= s;
		  else
		    {
		      Heap_Add(heap, jcol);
		      jrev[jcol] = lenl;
		      Ltmpj[lenl] = jcol;
		      Ltmpa[lenl] = -s;
		      lenl++;
		    }
		}
	      else
		{  /** New fill-in entry in U **/
		  if(jpos >= 0)
		    Utmpa[jpos] -= s;
		  else
		    {
		      jrev[jcol] = lenu;
		      Utmpj[lenu] = jcol;
		      Utmpa[lenu] = -s;
		      lenu++;
		    }
		}

	    }
	}

      /*** Do this steps in the right order ! ****/

      /**** 1 Dropping in L row ******/
      jpos = 0;
      for(k=0;k<lenl;k++)
	if(Ltmpj[k]>= 0) /** entries to be deleted are marked with -1 **/
	  {
	    Ltmpj[jpos] = Ltmpj[k];
	    Ltmpa[jpos] = Ltmpa[k];
	    jpos++;
	  }
      lenl = jpos;
      

      /**** 2 Reinit jrev ****/ /** DO IT HERE !! **/
      for(k=0;k<lenl;k++) 
	jrev[Ltmpj[k]] = -1;
      for(k=0;k<lenu;k++)
	jrev[Utmpj[k]] = -1;



      /**** 4 Dropping in U row ******/
      if(droptab != NULL)
	tt = droptab[i]*droptol;
      else
	tt = droptol;

#ifdef SYMMETRIC_DROP
      tt*=coefabs(Utmpa[0]);
#endif

      jpos = 1; /* Begin at i+1 to no delete the diagonal entry ! */
      for(k=1;k<lenu;k++)
	{
	  if(coefabs(Utmpa[k])> tt)
	    {
	      Utmpj[jpos] = Utmpj[k];
	      Utmpa[jpos] = Utmpa[k];
	      jpos++;
	    }
	}
      lenu = jpos;

      /**** 5 Keep only the largest entries in U row according to fillrat ***/
      if(fillrat > 0)
	{

	  k = lenu-1;
	  fillrow = (int)( (REAL)(maxfill)/(A->n-i));
	  LU_filldrop(&lenl, Ltmpj, Ltmpa, &k, Utmpj+1, Utmpa+1, fillrow, &queue);
	  lenu = k+1;
	  maxfill -= lenl+lenu;
	  nnzA -= A->nnzrow[i];
	}


      /**** 6 Inverse the diagonal term in U *******/
      if(coefabs(Utmpa[0]) < EPSILON)
	{
	  fprintferr(stderr, "Pivot (%ld, %ld) is small "_coef_" replace it \n", (long)i, (long)i, pcoef(Utmpa[0]));
	  Utmpa[0] = EPSILON;
	}
      



      Utmpa[0] = 1.0/(Utmpa[0]);
    

      /**** 7 Create the new row in L ******/
      if(L->inarow != 1 && L->nnzrow[i] > 0)
	{
	  free(L->ja[i]);
	  free(L->ma[i]);
	}
      L->nnzrow[i] = lenl;
#ifdef ALLOC_COMPACT_ILUT
      if(lenl>0)
	{
	  L->ja[i] = Ltmpj;
	  L->ma[i] = Ltmpa;
	  nnzL += (long)lenl;
	}
      else
	{
	  L->ja[i] = NULL;
	  L->ma[i] = NULL;
	}
#else
      if(lenl>0)
	{
	  if(small == 1)
	    {
	      L->ja[i] = Ltmpj;
	      L->ma[i] = Ltmpa;
	      Ltmpj += lenl;
	      Ltmpa += lenl;
	      nnzL += (long)lenl;
	    }
	  else
	    {
	      L->ja[i] = (int *)malloc(sizeof(int)*lenl);
	      memcpy(L->ja[i], Ltmpj, sizeof(int)*lenl);
	      L->ma[i] = (COEF *)malloc(sizeof(COEF)*lenl);
	      memcpy(L->ma[i], Ltmpa, sizeof(COEF)*lenl);
	    }
	}
#endif



      /**** 6  Create the new row in U ******/
      if(U->inarow != 1 && U->nnzrow[i] > 0)
	{
	  free(U->ja[i]);
	  free(U->ma[i]);
	}
      
      U->nnzrow[i] = lenu;
#ifdef ALLOC_COMPACT_ILUT
      U->nzrtab[U->nnzr++] = i;
      U->ja[i] = Utmpj;
      U->ma[i] = Utmpa;
      nnzU += (long)lenu;
#else
      if(small == 1)
	{
	  U->ja[i] = Utmpj;
	  U->ma[i] = Utmpa;
	  Utmpj += lenu;
	  Utmpa += lenu;
	  nnzU += (long)lenu;
	}
      else
	{
	  U->ja[i] = (int *)malloc(sizeof(int)*lenu);
	  memcpy(U->ja[i], Utmpj, sizeof(int)*lenu);
	  U->ma[i] = (COEF *)malloc(sizeof(COEF)*lenu);
	  memcpy(U->ma[i], Utmpa, sizeof(COEF)*lenu);
	}
#endif
    }

  if(fillrat > 0)
    queueExit(&queue);

  if(L->inarow == 1)
    {
      if(L->matab != NULL)
	free(L->matab);
      if(L->jatab != NULL)
	free(L->jatab);
      L->inarow = 0;
    }
  if(U->inarow == 1)
    {
      if(U->matab != NULL)
	free(U->matab);
      if(U->jatab != NULL)
	free(U->jatab);
      U->inarow = 0;
    }

  CS_SetNonZeroRow(L);
  CS_SetNonZeroRow(U);

#ifndef ALLOC_COMPACT_ILUT
  if(small == 1)
#endif
    {
      Lmatab = (COEF *)realloc(Lmatab, sizeof(COEF)*nnzL);
      Ljatab = (int *)realloc(Ljatab, sizeof(int)*nnzL);
      L->matab = Lmatab;
      L->jatab = Ljatab;
      
      nnzL = 0;
      for(k=0;k<L->nnzr;k++)
	{
	  i = L->nzrtab[k];
	  L->ja[i] = Ljatab+nnzL;
	  L->ma[i] = Lmatab+nnzL;
	  nnzL += L->nnzrow[i];
	}
      L->inarow = 1;
      
      Umatab = (COEF *)realloc(Umatab, sizeof(COEF)*nnzU);
      Ujatab = (int *)realloc(Ujatab, sizeof(int)*nnzU);
      U->matab = Umatab;
      U->jatab = Ujatab;
      nnzU = 0;
      for(k=0;k<U->nnzr;k++)
	{
	  i = U->nzrtab[k];
	  U->ja[i] = Ujatab+nnzU;
	  U->ma[i] = Umatab+nnzU;
	  nnzU += U->nnzrow[i];
	}
      U->inarow = 1;
    }


#ifdef DEBUG_M
  CS_Check(L, L->n);
  CS_Check(U, U->n);
#endif
 
}



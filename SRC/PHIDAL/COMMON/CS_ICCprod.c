/* @authors P. HENON */

#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <math.h>
#include <stdio.h>

#include "queue.h"
#include "phidal_common.h"

void CS_ICCprod(csptr A, csptr L, COEF *D, REAL droptol, REAL *droptab, REAL fillrat, int *wki1, int *wki2, COEF *wkd, cell_int *celltab, cell_int **ArowList)
{
  /****************************************************************/
  /* This function does L = L - A.D.At, in addition               */
  /* A = A.D    on return                                         */
  /* On entry:                                                    */
  /* A a CSC sparse matrix in SparRow format                      */
  /* D a diagonal matrix (D is the inverse of the diagonal terms  */
  /*     i.e. it comes from a previous call to CS_ICCT )          */       
  /* L a CSC lower triangular matrix                              */
  /* On return:                                                   */
  /* A = A.D    (SORTED BY ROW INDICES ON RETURN)                 */
  /* L = L-A.D.At                                                 */
  /****************************************************************/
  dim_t i, j, k;
  int min, ind;

 
  /*cell_int *celltab;
    cell_int **ArowList;*/
  dim_t nnza;
  cell_int *c, *next;
  cell_int **l;

  int jcol, jpos;
  int *jrev, *tmpj;  
  COEF *tmpa;
  int *aja;
  COEF *ama;
  REAL d;
  COEF s;
  int lenl;
  REAL maxfill=-1;
  flag_t fillrow;
  Queue queue;
  long nnzL;
#ifdef ALLOC_COMPACT_ILUT
  int memsize;
#else
  int small = 0;
#endif
  int *jatab=NULL;
  COEF *matab=NULL;

#ifdef ALLOC_COMPACT_ILUT
  memsize = MAX((int)(CSnnz(L)*ALLOC_COMPACT_INIT), L->n);
  jatab = (int *)malloc(sizeof(int)*memsize);
  matab = (COEF *)malloc(sizeof(COEF)*memsize);
#else
  small = 0;
  if((REAL)L->n*(REAL)L->n <= (REAL)SMALLBLOCK)
    {
      small = 1;
      jatab = (int *)malloc(sizeof(int)*(L->n*L->n));
      matab = (COEF *)malloc(sizeof(COEF)*(L->n*L->n));
    }
#endif

  if(fillrat > 0)
    {
      k = 0;
      for(i=0;i<L->n;i++)
	if(L->nnzrow[i] > k)
	  k = L->nnzrow[i];
      queueInit(&queue, (int)(k*fillrat));
      nnzL = CSnnz(L);
      maxfill = nnzL*fillrat;
    }



  /*celltab = (cell_int *)malloc(sizeof(cell_int)*A->n);
    ArowList = (cell_int **)malloc(sizeof(cell_int *)*L->n);*/

  /** Init the cellules **/

  bzero(ArowList, sizeof(cell_int *)*L->n);
  
  /** Initialize the row list of A **/

  /* First we need to place the entry with the lower row indice in first position */
  /** OIMBE on peut s'en passer si dans CS_TRSM on met le plus petit indice en tete de colonne
      au moment du dropping **/
  for(k=0;k<A->nnzr;k++)
      {
	j = A->nzrtab[k];

	aja = A->ja[j];
	ama = A->ma[j];
	min = aja[0];
	ind = 0;
	for(i=1;i<A->nnzrow[j];i++)
	  if(aja[i] < min)
	    {
	      min = aja[i];
	      ind = i;
	    }
	/*** Swap the entry in col j of A such that the first entry in col j be the minimum row index ***/
	if(ind > 0)
	  {
	    aja[ind] = aja[0];
	    s = ama[ind];
	    ama[ind] = ama[0];
	    aja[0] = min;
	    ama[0] = s;
	  }
      }

  for(k=0;k<A->nnzr;k++)
    {
      j = A->nzrtab[k];
      celltab[j].elt = j;
      celltab[j].nnz = A->nnzrow[j];
      celltab[j].ja = A->ja[j];
      celltab[j].ma = A->ma[j];
	  
      jcol = A->ja[j][0];
      
      
      l = ArowList+jcol;
      c = celltab+j;
      c->next = *l;
      *l = c;

    }
  

  jrev = wki1;

#ifdef ALLOC_COMPACT_ILUT
   tmpj = jatab;
   tmpa = matab;
#else
  if(small == 1)
    {
      tmpj = jatab;
      tmpa = matab;
    }
  else
    {
      tmpj = wki2;
      tmpa = wkd;
    }
#endif

  /** Init jrev **/
  for(j=0;j<L->n;j++) 
    jrev[j] = -1;

  nnzL = 0;
#ifdef ALLOC_COMPACT_ILUT
  /** We need to maintain the nzrtab during the computation 
      of M **/
  L->nnzr =0;
#endif
  for(k=0;k<L->n;k++)
    {
      /******************************************/
      /**   Compute the column k of L          **/
      /******************************************/
#ifdef ALLOC_COMPACT_ILUT
      if(nnzL + L->n > memsize)
	{
	  memsize = MAX(memsize*ALLOC_COMPACT_RATIO, (nnzL+L->n));
	  jatab = (int *)realloc(jatab, sizeof(int)*memsize);
	  matab = (COEF *)realloc(matab, sizeof(COEF)*memsize);
	  assert(jatab != NULL);
	  assert(matab != NULL);
	  /** We need to reset M->ja and M->ma **/
	  lenl = 0;
	  for(j=0;j<L->nnzr;j++)
	    {
	      i = L->nzrtab[j];
	      L->ja[i] = jatab+lenl;
	      L->ma[i] = matab+lenl;
	      lenl += L->nnzrow[i];
	    }
	  
	}
      tmpj = jatab + nnzL;
      tmpa = matab + nnzL;
#endif

      /** Unpack the column k of L **/
      aja = L->ja[k];
      ama = L->ma[k];
      lenl = 1; /** start at 1 : the diagonal term is stored at index 0 **/

      tmpj[0] = k;
      tmpa[0] = 0.0;  /** If no diagonal is found assume it is zero **/

      for(i=0;i<L->nnzrow[k];i++)
	{
	  jcol = aja[i];
	  if(jcol>k)
	    {
	      tmpj[lenl] = jcol;
	      tmpa[lenl] = ama[i];
	      lenl++;
	    }
	  else
	    {
#ifdef DEBUG_M
	      assert(jcol == k);
#endif
	      tmpa[0] = ama[i]; /** Diagonal term (k,k) is stored in tmpa[k] **/
	    }
	}
      
      /* Init jrev */
      for(j=0;j<lenl;j++)
	jrev[tmpj[j]] = j;


      /** Multiply Arow by Afirst **/
      c = ArowList[k];
      while(c!=NULL)
	{
	  next = c->next; /** chainage modifie c->next so we must keep record of it **/

	  nnza = c->nnz;
	  aja = c->ja;
	  ama = c->ma;
	  j = c->elt;

#ifdef DEBUG_M
	  assert(nnza>0);
#endif
	  
	  s = ama[0]*D[j];
	
	  min = aja[nnza-1];
	  ind = nnza-1;
	  for(i=0;i<nnza;i++)
	    {
	      jcol = aja[i];
#ifdef DEBUG_M
	      assert(jcol >= k);
#endif
	      if(jcol < min && jcol > k)
		{
		  min = jcol;
		  ind = i;
		}
#ifdef DEBUG_M
	      assert(jcol >= k);
#endif
	      jpos = jrev[jcol];
	      if(jpos >= 0)
		tmpa[jpos] -= s*ama[i];
	      else
		{
		  jrev[jcol] = lenl;
		  tmpj[lenl] = jcol;
		  tmpa[lenl] = -s*ama[i];
		  lenl++;
		}
	    }
	  ama[0] = s;

	  if(ind > 1)
	    {
	      /*** Swap the entry in col j of L such that the next entry in col j be the minimum row index > k ***/
	      aja[ind] = aja[1];
	      s = ama[ind];
	      ama[ind] = ama[1];
	      aja[1] = min;
	      ama[1] = s;
	    }
	  
	 
	  /** Update the next columns lists **/
	  c->nnz--;
	  if(c->nnz > 0)
	    {
	      c->ja++;
	      c->ma++;
	      
	      jcol = *(c->ja);
#ifdef DEBUG_M
	      assert(jcol>k);
#endif
	      l = ArowList+jcol;
	      c->next = *l;
	      *l = c;
	    }
	  
	  
	  c = next;
	}

      /*** Do these steps in the right order ! ****/	   
	  
      /**** 2 Reinit jrev ****/ /** DO IT HERE !! **/
      for(i=0;i<lenl;i++) 
	jrev[tmpj[i]] = -1;


      /**** 3 Dropping in L col k ******/
      if(droptab != NULL)
	d = droptab[k]*droptol;
      else
	d = droptol;

      jpos = 1; /** Begin at 1 to keep the diagonal term in tmp[0] **/
      for(i=1;i<lenl;i++) /* Begin at 1 to prevent deletion of the diagonal entry ! */
	if(coefabs(tmpa[i])> d)
	  {
	    tmpj[jpos] = tmpj[i];
	    tmpa[jpos] = tmpa[i];
	    jpos++;
	  }
      lenl = jpos;
	  
      /**** 4 Keep only the largest entries in col k of L  according to fillrat ***/
      if(fillrat > 0)
	{
	  dim_t ll;
	  fillrow = (int)( (REAL)(maxfill)/(L->n-k));
	  ll = lenl-1;
	  vec_filldrop(&ll, tmpj+1, tmpa+1, fillrow, &queue);
	  ll = i+1;
	  maxfill -= ll;
	}
	  
      /**** 5 Create the new row in L ******/
      if(L->inarow != 1 && L->nnzrow[k] > 0)
	{
	  free(L->ja[k]);
	  free(L->ma[k]);
	}

      L->nnzrow[k] = lenl;
#ifdef ALLOC_COMPACT_ILUT
      if(lenl>0)
	{
	  L->nzrtab[L->nnzr++] = k;
	  L->ja[k] = tmpj;
	  L->ma[k] = tmpa;
	  nnzL += (long)lenl;
	}
      else
	{
	  L->ja[k] = NULL;
	  L->ma[k] = NULL;
	}
#else
      if(lenl>0)
	{
	  if(small == 1)
	    {
	      L->ja[k] = tmpj;
	      L->ma[k] = tmpa;
	      tmpj += lenl;
	      tmpa += lenl;
	      nnzL += (long)lenl;
	    }
	  else
	    {
	      L->ja[k] = (int *)malloc(sizeof(int)*lenl);
	      memcpy(L->ja[k], tmpj, sizeof(int)*lenl);
	      L->ma[k] = (COEF *)malloc(sizeof(COEF)*lenl);
	      memcpy(L->ma[k], tmpa, sizeof(COEF)*lenl);
	    }
	}
#endif
    }



  if(fillrat > 0)
    queueExit(&queue);	

#ifdef DEBUG_M
  /** Check if A is sorted by row indices **/
  for(i=0;i<A->n;i++)
    {
      for(k=1;k<A->nnzrow[i];k++)
	assert(A->ja[i][k] > A->ja[i][k-1]);
    }
#endif
  
  if(L->inarow == 1)
    {
      if(L->matab != NULL)
	free(L->matab);
      if(L->jatab != NULL)
	free(L->jatab);
      L->inarow = 0;
    }
  
  CS_SetNonZeroRow(L);

#ifndef ALLOC_COMPACT_ILUT
  if(small == 1)
#endif
    {
      matab = (COEF *)realloc(matab, sizeof(COEF)*nnzL);
      jatab = (int *)realloc(jatab, sizeof(int)*nnzL);
      L->matab = matab;
      L->jatab = jatab;
      nnzL = 0;
      for(k=0;k<L->nnzr;k++)
	{
	  i = L->nzrtab[k];
	  L->ja[i] = jatab+nnzL;
	  L->ma[i] = matab+nnzL;
	  nnzL += L->nnzrow[i];
	}
      L->inarow = 1;
    }

#ifdef DEBUG_M
  CS_Check(L, L->n);
#endif 
}

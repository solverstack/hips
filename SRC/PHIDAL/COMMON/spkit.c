/* @authors J. GAIDAMOUR */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <assert.h>

/*#include "type.h"*/
#include "phidal_common.h"



void csr2csc2(dim_t n, dim_t n2, flag_t job, COEF* a, dim_t *ja, INTL *ia, COEF* ao, dim_t *jao, INTL *iao);

/* Attention, par rapport a SPKIT, numérotation C ! */
/* TODO : renommer ! */
/* TODO : aplb : ii->i, ka,kb,k, len dans le bon ordre et ne pas commencer a -1*/

/*  aplb   :   computes     C = A+B                                        */
/* ----------------------------------------------------------------------- */
/*  performs the matrix sum  C = A+B.  */
/* ----------------------------------------------------------------------- */
/*  on entry: */
/*  --------- */
/*  nrow  = integer. The row dimension of A and B */
/*  ncol  = integer. The column dimension of A and B. */
/*  job   = integer. Job indicator. When job = 0, only the structure */
/*                   (i.e. the arrays jc, ic) is computed and the */
/*                   real values are ignored. */

/*  a, */
/*  ja, */
/*  ia    = Matrix A in compressed sparse row format. */
 
/*  b,  */
/*  jb,  */
/*  ib    =  Matrix B in compressed sparse row format. */

/*  nzmax = integer. The  length of the arrays c and jc. */
/*          amub will stop if the result matrix C  has a number  */
/*          of elements that exceeds exceeds nzmax. See ierr. */
 
/*  on return: */
/* ---------- */
/*  c,  */
/*  jc,  */
/*  ic	= resulting matrix C in compressed sparse row sparse format. */
	    
/*  return  integer. serving as error message.  */
/*           = 0 means normal return, */
/*           > 0 means that amub stopped while computing the */
/*             i-th row  of C with i=ierr-1 (in C numbering), because the number  */
/*             of elements in C exceeds nzmax. */

/*  work arrays: */
/* ------------ */
/*  iw	= integer work array of length equal to the number of */
/*          columns in A. */

/* ----------------------------------------------------------------------- */

/* attention, par rapport à Fortran, le tableau iw !! */
/* incrementation de len apres, remettre avant pour etre identique (modif quand recherche bug -1<->0 sur iw) */

int aplb(dim_t nrow, dim_t ncol, flag_t job, 
	 COEF* a, dim_t *ja, INTL *ia, 
	 COEF* b, dim_t *jb, INTL *ib, 
	 COEF* c, dim_t *jc, INTL *ic, 
	 INTL nzmax, dim_t *iw) {

  int ii, k, ka, kb, jcol, jpos;

  int values = (job != 0);
  int len = 0;


  assert(ia != NULL);
  if(ia[nrow]>0)
    assert(ja != NULL);
  assert((job == 0) || ((job != 0) && (a != NULL)) || (ia[nrow] == 0 && a == NULL));


  assert(ib != NULL);
  if(ib[nrow]>0)
    assert(jb != NULL);

  assert((job == 0) || ((job != 0) && (b != NULL)));


  assert(ic != NULL);

  
  assert((job == 0) || ((job != 0) && (c != NULL)));
 
  ic[0] = 0; /*TODO : incluable ds la boucle*/

  memset(iw, -1, ncol*sizeof(int));

  for(ii=0; ii<nrow; ii++) {
    /* row i */
    for(ka=ia[ii];ka<ia[ii+1];ka++) {
      jcol = ja[ka];
      if (len >= nzmax) { assert(0); return ii; }
      jc[len] = jcol ;
      if (values) c[len] = a[ka];
      iw[jcol] = len;
      len++;
    }

    for(kb=ib[ii];kb<ib[ii+1];kb++) {
      jcol = jb[kb];
      jpos = iw[jcol];
      if (jpos == -1) {
	if (len >= nzmax) { assert(0); return ii+1; }
	jc[len] = jcol;
	if (values) c[len] = b[kb];
	iw[jcol]= len;
	len++;
      } else {
	if (values) c[jpos] += b[kb];
      }
    }

    for(k=ic[ii]; k<len; k++)
      iw[jc[k]] = -1;
    ic[ii+1] = len; /*TODO : incluable ds la boucle*/
  }
  return 0;
}


/* ----------------------------------------------------------------------- */
/*  csrcsc  : converts compressed sparse row format to compressed sparse c */
/*       subroutine csrcsc (n,job, a,ja,ia,ao,jao,iao) */
/*       integer ia(n+1),iao(n+1),ja(*),jao(*) */
/*       real*8  a(*),ao(*) */
/* ----------------------------------------------------------------------- */
/*  Compressed Sparse Row     to      Compressed Sparse Column */

/*  (transposition operation)   Not in place.  */
/* -----------------------------------------------------------------------  */
/*  -- not in place -- */
/*  this subroutine transposes a matrix stored in a, ja, ia format. */
/*  --------------- */
/*  on entry: */
/* ---------- */
/*  n	= dimension of A. */
/*  job	= integer to indicate whether to fill the values (job.eq.1) of the */
/*          matrix ao or only the pattern., i.e.,ia, and ja (job .ne.1) */

/*  a	= real array of length nnz (nnz=number of nonzero elements in input  */
/*          matrix) containing the nonzero elements. */
/*  ja	= integer array of length nnz containing the column positions */
/*  	  of the corresponding elements in a. */
/*  ia	= integer of size n+1. ia(k) contains the position in a, ja of */
/* 	  the beginning of the k-th row. */

/*  on return: */
/*  ----------  */
/*  output arguments: */
/*  ao	= real array of size nzz containing the "a" part of the transpose */
/*  jao	= integer array of size nnz containing the column indices. */
/*  iao	= integer array of size n+1 containing the "ia" index array of */
/* 	  the transpose.  */

/* ----------------------------------------------------------------------- */

void csr2csc(dim_t n, flag_t job, COEF* a, dim_t *ja, INTL *ia, COEF* ao, dim_t *jao, INTL *iao) {
  csr2csc2(n, n, job, a, ja, ia, ao, jao, iao);
}

/* ----------------------------------------------------------------------- */
/*       subroutine csrcsc2 (n,n2,job, a,ja,ia,b,jb,ib) */
/*       integer ia(n+1),ib(n2+1),ja(*),jb(*) */
/*       real*8  a(*),b(*) */
/* ----------------------------------------------------------------------- */
/*  Compressed Sparse Row     to      Compressed Sparse Column */

/*  (transposition operation)   Not in place.  */
/* -----------------------------------------------------------------------  */
/*  Rectangular version.  n is number of rows of CSR matrix, */
/*                        n2 (input) is number of columns of CSC matrix. */
/* -----------------------------------------------------------------------  */
/*  -- not in place -- */
/*  this subroutine transposes a matrix stored in a, ja, ia format. */
/*  --------------- */
/*  on entry: */
/* ---------- */
/*  n	= number of rows of CSR matrix. */
/*  n2    = number of columns of CSC matrix. */
/*  job	= integer to indicate whether to fill the values (job.eq.1) of the */
/*          matrix b or only the pattern., i.e.,ia, and ja (job .ne.1) */

/*  a	= real array of length nnz (nnz=number of nonzero elements in input  */
/*          matrix) containing the nonzero elements. */
/*  ja	= integer array of length nnz containing the column positions */
/*  	  of the corresponding elements in a. */
/*  ia	= integer of size n+1. ia(k) contains the position in a, ja of */
/* 	  the beginning of the k-th row. */

/*  on return: */
/*  ----------  */
/*  output arguments: */
/*  b	= real array of size nzz containing the "a" part of the transpose */
/*  jb	= integer array of size nnz containing the column indices. */
/*  ib	= integer array of size n+1 containing the "ia" index array of */
/* 	  the transpose.  */

/* -----------------------------------------------------------------------  */

void csr2csc2(dim_t n, dim_t n2, flag_t job, COEF* a, dim_t *ja, INTL *ia, COEF* ao, dim_t *jao, INTL *iao) {
  dim_t i, j, k;
  dim_t next;

  assert(ia[0] == 0); /** C numbering **/


  assert(ia != NULL);
  if(ia[n]>0)
    assert(ja != NULL);

  assert((job == 0) || ((job == 1) && (a != NULL)) || (ia[n] == 0 && a == NULL));


  assert(iao != NULL);

  if(ia[n2]>0)
    {
      assert(jao != NULL);
      assert((job == 0) || ((job == 1) && (ao != NULL)));
    }
  
#ifdef DEBUG_M
  checkCSR(n, ia, ja, a, 0);
#endif

  /* compute lengths of rows of transp(A) */
  bzero(iao, (n2+1)*sizeof(INTL));
  for(i=0;i<n;i++)
    for(k=ia[i];k<ia[i+1];k++)
      iao[ja[k]+1]++;

  /* compute pointers from lengths */
  iao[0] = 0;
  for(i=0;i<n2;i++)
    iao[i+1] += iao[i];

#ifdef DEBUG_M
  /**vrai en symetrique **/
  /*for(i=0;i<n2;i++) 
    assert(iao[i+1]-iao[i] == ia[i+1]-ia[i]);*/

  assert(iao[n2] == ia[n]);
#endif

  /* now do the actual copying */
  for(i=0;i<n;i++)
    for(k=ia[i];k<ia[i+1];k++) {
      j = ja[k];
      next = iao[j];
      if (job == 1) ao[next] = a[k];
      jao[next] = i;
      iao[j]++;
    }

  /* reshift iao and leave */
  for(i=n2-1;i>=0;i--)
    iao[i+1] = iao[i];
  iao[0] = 0;
  assert(iao[n2] == ia[n]);



#ifdef DEBUG_M
  checkCSR(n2, iao, jao, a, 0);
#endif
}


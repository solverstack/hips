/* @authors P. HENON */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include "queue.h"
#include "phidal_common.h"

void PhidalMatrix_Init(PhidalMatrix *a)
{
#ifdef DEBUG_M
  assert(a!=NULL);
#endif
  a->csc = 0;
  a->symmetric = 0;
  a->dim1 = 0;
  a->dim2 = 0;
  a->tli = 0;
  a->tlj = 0;
  a->bri = 0;
  a->brj = 0;
  a->cia = NULL;
  a->cja = NULL;
  a->ca  = NULL;
  a->ria = NULL;
  a->rja = NULL;
  a->ra  = NULL;
  a->virtual = 0;
  a->bloknbr = 0;
  a->bloktab = NULL;


}

void PhidalMatrix_Clean(PhidalMatrix *a)
{
  dim_t i;
#ifdef DEBUG_M
  assert(a!=NULL);
#endif
  if(a->cia != NULL)
    free(a->cia);
  if(a->cja != NULL)
    free(a->cja);
  if(a->ca != NULL)
    free(a->ca);
  if(a->ria != NULL)
    free(a->ria);
  if(a->rja != NULL)
    free(a->rja);
  if(a->ra != NULL)
    free(a->ra);

  if(a->virtual == 0 && a->bloktab != NULL)
    {
      for(i=0;i<a->bloknbr;i++)
	{
	  if(a->bloktab+i != NULL)
	    cleanCS(a->bloktab+i);
	}
      free(a->bloktab);
    }
  a->symmetric = 0;
  a->csc = 0;
  a->dim1 = 0;
  a->dim2 = 0;
  a->tli = 0;
  a->tlj = 0;
  a->bri = 0;
  a->brj = 0;
  a->virtual = 0;
  a->bloknbr = 0;

}


void PhidalPrec_Init(PhidalPrec *p)
{
  p->symmetric = 0;
  p->dim = 0;
  p->levelnum = 0;
  p->forwardlev = 0;

  p->D = NULL;
    
  p->LU = NULL; 
  p->EF = NULL; 
  p->S = NULL; 
  p->B = NULL; 

/* TODO : harmoniser avec DB*/
  /*M_MALLOC(p->LU, SCAL);
  M_MALLOC(p->EF, SCAL);
  M_MALLOC(p->S,  SCAL);
  M_MALLOC(p->B,  SCAL);*/

#if defined (ILUT_WHOLE_SCHUR) || defined(ILUT_SOLVE)
  p->csL = NULL;
  p->csU = NULL;
#endif
  /*p->C = NULL;  OPTIMREC forward */
  p->prevprec = NULL;
  p->nextprec = NULL;

  p->schur_method = 0;
  p->pivoting = 0;
  p->permtab = NULL;

/*   p->tol_schur = 1; */
  /*p->info = NULL;*/

}


void PhidalPrec_Clean(PhidalPrec *p)
{
#ifdef DEBUG_M
  if(p->forwardlev > 0)
    assert(p->nextprec != NULL);
  else
    assert(p->nextprec == NULL);
#endif
  if(p->forwardlev  > 0)
    {
      if(p->nextprec != NULL)
	{
	  PhidalPrec_Clean(p->nextprec);
	  free(p->nextprec);
	}
      
      if (p->EF != NULL) {
	if(PREC_E(p) != NULL)
	  {
	    PhidalMatrix_Clean(PREC_E(p));
	    free(PREC_E(p));
	  }
	
	if(PREC_F(p) != NULL)
	  {
	    PhidalMatrix_Clean(PREC_F(p));
	    free(PREC_F(p));
	  }
	

      }
  
      if (p->S != NULL) {
	if(PREC_S_SCAL(p) != NULL)
	  {
	    if(p->schur_method == 1)
	      PhidalMatrix_Clean(PREC_S_SCAL(p));
	    
	    free(PREC_S_SCAL(p));
	  }
	M_CLEAN(p->S);
      }

      if (p->B != NULL) {
	if(PREC_B(p) != NULL)
	  {
	    PhidalMatrix_Clean(PREC_B(p));
	    free(PREC_B(p));
	  }
	M_CLEAN(p->B);
      }
      /*if(p->C != NULL) OPTIMREC Forward 
	{
	PhidalMatrix_Clean(p->C);
	free(p->C);
	}*/
    }
  
  if (p->LU != NULL) {
    if(PREC_L_SCAL(p) != NULL)
      {
	PhidalMatrix_Clean(PREC_L_SCAL(p));
	free(PREC_L_SCAL(p));
      }
    
    if(PREC_U_SCAL(p) != NULL)
      {
	PhidalMatrix_Clean(PREC_U_SCAL(p));
	free(PREC_U_SCAL(p));
      }
    M_CLEAN(p->LU);
  }
  if(p->EF != NULL)
    M_CLEAN(p->EF);
  if(p->S != NULL)
    M_CLEAN(p->S);
  if(p->B != NULL)
    M_CLEAN(p->B);


#if defined(ILUT_WHOLE_SCHUR) || defined(ILUT_SOLVE)
  if(p->csL != NULL)
    {
      cleanCS(p->csL);
      free(p->csL);
    }
  
  if(p->csU != NULL)
    {
      cleanCS(p->csU);
      free(p->csU);
    }
#endif

  if(p->symmetric == 1 && p->D != NULL)
    free(p->D);

  if(p->pivoting == 1)
    free(p->permtab);


  p->symmetric = 0;
  p->dim = 0;
  p->levelnum = 0;
  p->forwardlev = 0;
  p->schur_method = 0;


  p->pivoting = 0;

/*   p->tol_schur = 1; */

  /*if (p->info != NULL) {
    PrecInfo_Clean(p->info);
    free(p->info);
    }*/
}


void initCONNECTOR(CONNECTOR_str *C)
{
  C->vlist = NULL;
  C->nvert = 0;
  C->key = NULL;
  C->nkey = 0;
  C->listn = NULL;
  C->numn = 0;
}


void cleanCONNECTOR(CONNECTOR_str *C)
{
  if(C->vlist != NULL)
    free(C->vlist);
  if(C->key != NULL)
    free(C->key);
  if(C->listn != NULL)
    free(C->listn);
  initCONNECTOR(C);
}



void PhidalHID_Init(PhidalHID *BS)
{
  BS->ndom = 0;
  BS->locndom = 0;
  BS->n = 0;
  BS->nblock = 0;
  BS->nlevel = 0;
  BS->block_index = NULL;
  BS->block_levelindex = NULL;
  BS->block_keyindex = NULL;
  BS->block_key = NULL;

  BS->block_domwgt = NULL;

  /*BS->dof = 1; */

  /*BS->perm = NULL;
    BS->iperm = NULL;*/
}
 
void PhidalHID_Clean(PhidalHID *BS) 
{
  if(BS == NULL)
    return;

  if(BS->block_index != NULL)
    free(BS->block_index);
  if(BS->block_levelindex != NULL)
    free(BS->block_levelindex);
  if(BS->block_keyindex != NULL)
    free(BS->block_keyindex);
  if(BS->block_key != NULL)
    free(BS->block_key);

  if(BS->block_domwgt != NULL)
    free(BS->block_domwgt);
  /*if(BS->perm != NULL)
    free(BS->perm);
    if(BS->iperm != NULL)
  free(BS->iperm);*/
}

void PrecInfo_Init(PrecInfo* info) {
  if (info == NULL) return; 
  info->nnzA = 0; 
  info->nnzP = 0; 
  info->peak = 0; 
}

void PrecInfo_Clean(PrecInfo* info) {
  if (info == NULL) return; 
}

void PrecInfo_AddNNZ(PrecInfo* info, INTL nnz) {
  if (info == NULL) return; 
  info->nnzP += nnz; 
  info->peak = MAX(info->peak, info->nnzP); 
}

void PrecInfo_SetNNZA(PrecInfo* info, INTL nnz) {
  if (info == NULL) return; 
  info->nnzA = nnz; 
}


void PrecInfo_SubNNZ(PrecInfo* info, INTL nnz) {
  if (info == NULL) return; 
  info->nnzP -= nnz; 
}

void PrecInfo_Print(PrecInfo* info, int proc_id) {
  if (info == NULL) return;

  fprintf(stdout, 
	  "Info (%d) nnzA = %ld ; nnzP = %ld ; ratio = %g ; peak = %ld ; peak ratio = %g \n", 
	  proc_id,
	  (long)info->nnzA, (long)info->nnzP, 
	  (REAL)info->nnzP/(REAL)info->nnzA, 
	  (long)info->peak, (REAL)info->peak/(REAL)info->nnzA); 
}

void PrecInfo_PrintMax(REAL ratio_max, REAL peakratio_max, int proc_id) {
  if (ratio_max == 0) return;

  if(proc_id == 0)
    fprintf(stdout, 
	    "Info (total) ratio = %g ; peak ratio = %g\n", 
	    ratio_max, peakratio_max);
}

void PrecInfo_PrintMaxThese(REAL ratio_max_ref1, REAL peakratio_max_ref1, int proc_id) {
  if (ratio_max_ref1 == 0) return;

  if(proc_id == 0)
    fprintf(stdout, 
	    "Info (balance) ratio = %g ; peak ratio = %g\n", 
	    ratio_max_ref1, peakratio_max_ref1);
}

int reinitCS(csptr amat)
{
  /*---------------------------------------------------------------------------------------
    | Reinit a sparse Matrix in SparRow format into a sparse void matrix of same dimension
    |--------------------------------------------------------------------------------------
    | on entry:
    |==========
    | ( amat )  =  Pointer to a SparRow struct.
    |     len   =  size of matrix
    |--------------------------------------------------------------------*/
  
  dim_t i, k; 
  if (amat == NULL) return 0;
  if (amat->n < 1) return 0;

  if(amat->inarow != 1)
    {
      /*for (i=0; i<amat->n; i++) 
	{
	if (amat->nnzrow[i] > 0) 
	{
	if (amat->ma[i]) 
	free(amat->ma[i]);
	if (amat->ja[i]) 
	free(amat->ja[i]);
	}
	}*/
      for(k = 0;k<amat->nnzr;k++)
	{
	  i = amat->nzrtab[k];
	  if(amat->ma[i] != NULL)
	    free(amat->ma[i]);
	  amat->ma[i] = NULL;
	  free(amat->ja[i]);
	  amat->ja[i] = NULL;
	  amat->nnzrow[i] = 0;
	}
    }
  else
    {
      
      if(amat->jatab != NULL)
	free(amat->jatab);
      if(amat->matab != NULL)
	free(amat->matab);
	  
      for(k = 0;k<amat->nnzr;k++)
	{
	  i = amat->nzrtab[k];
	  amat->ma[i] = NULL;
	  amat->ja[i] = NULL;
	  amat->nnzrow[i] = 0;
	}
      amat->inarow = 0;
    }	


  bzero(amat->inzrtab, sizeof(dim_t) * amat->nnzr);
  amat->nnzr = 0;
  amat->nzrtab[0] = amat->n;

  
  return 0;
}

void PhidalOptions_Init(PhidalOptions *option)
{
  option->verbose = 0;
  option->fd = stdout;
  option->symmetric = 0;
  option->krylov_method = 0;

  option->count_nnz = 0;
  option->scale = 1;
  option->scalenbr = 1;
  option->pivoting = 0;
  option->pivot_ratio = 1000.0;
  option->locally_nbr = 100;
  option->schur_method = 2;

  option->fillrat = -1.0;
  option->Schur_fillrat = -1.0;

  option->droptol0    = 0.000;
  option->droptol1    = 0.001;
  option->droptolrat  = 1.0;
  option->dropSchur   = 0.000;  
  option->droptolE    = 0.001;

  option->tol = 10e-7;
  option->itmax = 100;
  option->krylov_im = 100;
  option->itmaxforw = 10;
  option->itforwrat = 0.5;
  option->forwardlev = 1;
  option->amalg_rat = 0.05;
  option->shiftdiag = 0;

  option->use_pastix = 0;

  /*-----------------------------------------/
  /  ADVANCED PARAMETERS                     /
  /  In most of the cases  user do not have  /
  /  to change them                          /
  /-----------------------------------------*/
}

  
void PhidalOptions_Clean(PhidalOptions *option)
{

}



int initCS(csptr amat, int len)
{
/*---------------------------------------------------------------------- 
| Initialize SparRow structs.
|----------------------------------------------------------------------
| on entry: 
|========== 
| ( amat )  =  Pointer to a SparRow struct.
|     len   =  size of matrix
|
| On return:
|===========
|
|  amat->n
|      ->*nnzrow
|      ->**ja
|      ->**ma
|
| integer value returned:
|             0   --> successful return.
|             1   --> memory allocation error.
|--------------------------------------------------------------------*/

   amat->n = len;
   amat->inarow = 0;
   if(len > 0)
     {
       amat->nnzrow = (dim_t *) malloc(len*sizeof(dim_t));
       amat->ja = (dim_t **) malloc(len*sizeof(dim_t *));
       amat->ma = (COEF **) malloc(len*sizeof(COEF *));
       if(amat->nnzrow == NULL || amat->ja == NULL || amat->ma == NULL)
	 return 1;
       
       if ( (amat->nnzrow == NULL) || (amat->ja == NULL) || 
	   (amat->ma == NULL) ) return 1;

       bzero( amat->nnzrow, sizeof(dim_t)*len);
       bzero( amat->ja, sizeof(dim_t *)*len);
       bzero( amat->ma, sizeof(COEF *)*len);
       amat->jatab = NULL;
       amat->matab = NULL;
     }
   else
     {
       amat->nnzrow = NULL;
       amat->ja = NULL;
       amat->ma = NULL;
       amat->jatab = NULL;
       amat->matab = NULL;
     }
   
   amat->nnzr = 0;
   if(len>0)
     {
       amat->nzrtab = (dim_t *)malloc(sizeof(dim_t)*len);
       amat->inzrtab = (dim_t *)malloc(sizeof(dim_t)*len);
       amat->nzrtab[0] = amat->n;
       bzero(amat->inzrtab, sizeof(dim_t) * amat->n);
     }
   else
     {
       amat->nzrtab = NULL;
       amat->inzrtab = NULL;
     }
   return 0;
}


int cleanCS(csptr amat)
{
/*----------------------------------------------------------------------
| Free up memory allocated for SparRow structs.
|----------------------------------------------------------------------
| on entry:
|==========
| ( amat )  =  Pointer to a SparRow struct.
|     len   =  size of matrix
|--------------------------------------------------------------------*/
   /*	*/
  dim_t i, k; 
  if (amat == NULL) return 0;

  if(amat->inarow == 0)
    {

      if(amat->n == amat->nnzr)
	for (i=0; i<amat->n; i++) 
	  {
	    if (amat->ma[i]!= NULL) 
	      free(amat->ma[i]);
	    if (amat->ja[i]!=NULL) 
	      free(amat->ja[i]);
	  }	
      else
	for(k = 0;k<amat->nnzr;k++)
	  {
	    i = amat->nzrtab[k];
	    if(amat->ma[i]!= NULL)
	      free(amat->ma[i]);
	    free(amat->ja[i]);
	  }
    }
  else
    {
      if( amat->jatab != NULL)
	free(amat->jatab);
      if( amat->matab != NULL)
	free(amat->matab);

      for(k = 0;k<amat->nnzr;k++)
	{
	  i = amat->nzrtab[k];
	  amat->nnzrow[i] = 0;
	}
      amat->inarow = 0;
      
    }

  if (amat->ma) free(amat->ma);
  if (amat->ja) free(amat->ja);
  if (amat->nnzrow) free(amat->nnzrow); 
     

  if(amat->nzrtab) free(amat->nzrtab);
  if(amat->inzrtab) free(amat->inzrtab);
  amat->nnzr = 0;
  amat->n = 0;


  return 0;
}




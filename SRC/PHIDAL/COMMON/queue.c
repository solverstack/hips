/* @authors P. HENON */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "phidal_common.h"
#include "queue.h"



int queueInit(Queue *q, int size)
{
    q->size = size;
    q->used = 0;
    if(size > 0)
      {
	q->elttab = (int *)malloc(size * sizeof(int));
	q->keytab = (FLOAT *)malloc(size * sizeof(FLOAT));
	q->keytab2 = (int *)malloc(size * sizeof(int));
      }
    else
      {
	q->elttab = NULL;
    	q->keytab = NULL;
	q->keytab2 = NULL;
      }
    return 1;
}

Queue * queueCopy(Queue *dst, Queue *src)
{
  if(src == NULL || dst == NULL)
    return NULL;
  memcpy(dst, src, sizeof(Queue));
  dst->elttab  = (int *)  malloc(src->size * sizeof(int));
  memcpy(dst->elttab, src->elttab, src->size * sizeof(int));
  dst->keytab  = (FLOAT *)malloc(src->size * sizeof(FLOAT));
  memcpy(dst->keytab, src->keytab, src->size * sizeof(FLOAT));
  dst->keytab2 = (int *)  malloc(src->size * sizeof(int));
  memcpy(dst->keytab2, src->keytab2, src->size * sizeof(int));
  return dst;
}

void queueExit(Queue *q)
{
  if(q->size != 0)
    {
      free(q->elttab);
      free(q->keytab);
      free(q->keytab2);
    }
}

void queueAdd(Queue *q, int elt, FLOAT key)
{
  queueAdd2(q, elt, key, 0);
}

void queueAdd2(Queue *q, int elt, FLOAT key, int key2)
{
    dim_t i;
    int   swap_elt;
    FLOAT swap_key;
    int   swap_key2;
    int *tmp;
    FLOAT * tmp2;
    int   * tmp3;

    /** Allocate more space if necessary **/
    if(q->size == q->used)
	{
	    tmp  = q->elttab;
	    tmp2 = q->keytab;
	    tmp3 = q->keytab2;
	    /* OIMBE Realloc ?? */
	    q->elttab = (int *)malloc((q->size*2+1)*sizeof(int));
	    memcpy(q->elttab, tmp, q->size * sizeof(int));

	    q->keytab = (FLOAT *)malloc((q->size*2+1)*sizeof(FLOAT));
	    memcpy(q->keytab, tmp2, q->size * sizeof(FLOAT));

	    q->keytab2 = (int *)malloc((q->size*2+1)*sizeof(int));
	    memcpy(q->keytab2, tmp3, q->size * sizeof(int));

	    q->size = q->size*2 +1;
	    free(tmp);
	    free(tmp2);
	    free(tmp3);
	}

    q->elttab[q->used] = elt;
    q->keytab[q->used] = key;
    q->keytab2[q->used] = key2;
    q->used++;
    
    i = q->used;
    while( (i>1) 
	   &&  compWith2keys(q, i-1, i/2-1))
	{
	    swap_elt = q->elttab[i-1];
	    swap_key = q->keytab[i-1];
	    swap_key2 = q->keytab2[i-1];
	    q->elttab[i-1] = q->elttab[i/2-1];
	    q->keytab[i-1] = q->keytab[i/2-1];
	    q->keytab2[i-1] = q->keytab2[i/2-1];

	    q->elttab[i/2-1] = swap_elt;
	    q->keytab[i/2-1] = swap_key;
	    q->keytab2[i/2-1] = swap_key2;
	    i=i/2;
	}
}


int queueGet(Queue *q)
{
    dim_t i, j;
    int return_elt;
    int swap_elt;
    FLOAT swap_key;
    int   swap_key2;


    return_elt = q->elttab[0];
    
    q->elttab[0]  = q->elttab[q->used-1];
    q->keytab[0]  = q->keytab[q->used-1];
    q->keytab2[0] = q->keytab2[q->used-1];
    q->used--;
    
    i=1;
    
    while(i <= (q->used/2))
	{
	    if( (2*i == q->used)
		|| compWith2keys(q, 2*i-1, 2*i))     /*(q->keytab[2*i-1] < q->keytab[2*i]))*/
		{
		    j = 2*i;
		}
	    else
		{
		    j = 2*i+1;
		}
	    if (!compWith2keys(q, i-1, j-1))         /*(q->keytab[i-1] >= q->keytab[j-1])*/
		{
		    swap_elt = q->elttab[i-1];
		    swap_key = q->keytab[i-1];
		    swap_key2 = q->keytab2[i-1];

		    q->elttab[i-1]  = q->elttab[j-1];
		    q->keytab[i-1]  = q->keytab[j-1];
		    q->keytab2[i-1] = q->keytab2[j-1];

		    q->elttab[j-1] = swap_elt;
		    q->keytab[j-1] = swap_key;
		    q->keytab2[j-1] = swap_key2;
		    
		    i=j;
		}
	    else
		break;
	}
    return return_elt;
}



int queueSize(Queue *q)
{
    return q->used;
}


void queueClear(Queue *q)
{
    q->used = 0;
}


/** Read the next element that 'll be given by queueGet 
    but not retrieve it from the queue **/
int queueRead(Queue *q)
{
  return q->elttab[0];
}

int queueReadKey(Queue *q)
{
  return q->keytab[0];
}

int compWith2keys(Queue *q, int elt1, int elt2)
{
  /* if elt1 < elt2 return 1  */
  /* if elt1 = elt2 return 0  */
  /* if elt1 > elt2 return 0 */
  
  if(q->keytab[elt1] < q->keytab[elt2])
    return 1;
  if(q->keytab[elt1] > q->keytab[elt2]) 
    return 0;
  if(q->keytab2[elt1] < q->keytab2[elt2])
    return 1;
  return 0;
}



void queueGet2(Queue *q, int *elt, FLOAT *key)
{
    dim_t i, j;
    int swap_elt;
    FLOAT swap_key;
    int   swap_key2;


    (*elt) = q->elttab[0];
    (*key) = q->keytab[0];
    
    q->elttab[0]  = q->elttab[q->used-1];
    q->keytab[0]  = q->keytab[q->used-1];
    q->keytab2[0] = q->keytab2[q->used-1];
    q->used--;
    
    i=1;
    
    while(i <= (q->used/2))
	{
	    if( (2*i == q->used)
		|| compWith2keys(q, 2*i-1, 2*i))     /*(q->keytab[2*i-1] < q->keytab[2*i]))*/
		{
		    j = 2*i;
		}
	    else
		{
		    j = 2*i+1;
		}
	    if (!compWith2keys(q, i-1, j-1))         /*(q->keytab[i-1] >= q->keytab[j-1])*/
		{
		    swap_elt = q->elttab[i-1];
		    swap_key = q->keytab[i-1];
		    swap_key2 = q->keytab2[i-1];

		    q->elttab[i-1]  = q->elttab[j-1];
		    q->keytab[i-1]  = q->keytab[j-1];
		    q->keytab2[i-1] = q->keytab2[j-1];

		    q->elttab[j-1] = swap_elt;
		    q->keytab[j-1] = swap_key;
		    q->keytab2[j-1] = swap_key2;
		    
		    i=j;
		}
	    else
		break;
	}
}

void queueGet3(Queue *q, int *elt, int *key)
{
    dim_t i, j;
    int swap_elt;
    FLOAT swap_key;
    int   swap_key2;


    (*elt) = q->elttab[0];
    (*key) = q->keytab2[0];
    
    q->elttab[0]  = q->elttab[q->used-1];
    q->keytab[0]  = q->keytab[q->used-1];
    q->keytab2[0] = q->keytab2[q->used-1];
    q->used--;
    
    i=1;
    
    while(i <= (q->used/2))
	{
	    if( (2*i == q->used)
		|| compWith2keys(q, 2*i-1, 2*i))     /*(q->keytab[2*i-1] < q->keytab[2*i]))*/
		{
		    j = 2*i;
		}
	    else
		{
		    j = 2*i+1;
		}
	    if (!compWith2keys(q, i-1, j-1))         /*(q->keytab[i-1] >= q->keytab[j-1])*/
		{
		    swap_elt = q->elttab[i-1];
		    swap_key = q->keytab[i-1];
		    swap_key2 = q->keytab2[i-1];

		    q->elttab[i-1]  = q->elttab[j-1];
		    q->keytab[i-1]  = q->keytab[j-1];
		    q->keytab2[i-1] = q->keytab2[j-1];

		    q->elttab[j-1] = swap_elt;
		    q->keytab[j-1] = swap_key;
		    q->keytab2[j-1] = swap_key2;
		    
		    i=j;
		}
	    else
		break;
	}
}


/*****************************************************************************/
/*******                             HEAP                         ************/
/*****************************************************************************/
int Heap_Init(Heap *q, int size)
{
    q->size = size;
    q->used = 0;
    if(size > 0)
      q->elttab = (int *)malloc(size * sizeof(int));
    else
      q->elttab = NULL;
    return 1;
}

Heap * Heap_Copy(Heap *dst, Heap *src)
{
  if(src == NULL || dst == NULL)
    return NULL;
  memcpy(dst, src, sizeof(Heap));
  dst->elttab  = (int *)  malloc(src->size * sizeof(int));
  memcpy(dst->elttab, src->elttab, src->size * sizeof(int));
  return dst;
}

void Heap_Exit(Heap *q)
{
    if(q->size != 0)
      free(q->elttab);
}

void Heap_Add(Heap *q, int elt)
{
    dim_t i;
    int   swap_elt;
    int *tmp;

    /** Allocate more space if necessary **/
    if(q->size == q->used)
	{
	    tmp  = q->elttab;

	    /* OIMBE Realloc ?? */
	    q->elttab = (int *)malloc((q->size*2+1)*sizeof(int));
	    memcpy(q->elttab, tmp, q->size * sizeof(int));

	    q->size = q->size*2 +1;
	    free(tmp);
	}

    q->elttab[q->used] = elt;
    q->used++;
    
    i = q->used;
    while( (i>1) 
	   && q->elttab[i-1] < q->elttab[i/2-1])
	{
	    swap_elt = q->elttab[i-1];
	    q->elttab[i-1] = q->elttab[i/2-1];
	    q->elttab[i/2-1] = swap_elt;
	    i=i/2;
	}
}


int Heap_Get(Heap *q)
{
    dim_t i, j;
    int return_elt;
    int swap_elt;
    
    /** I add that to try optimize the code **/
    int *elttab;
    int used;
    elttab = q->elttab;

    return_elt = elttab[0];
    
    elttab[0]  = elttab[q->used-1];
    q->used--;
    
    i=1;
    
    used = q->used;

    while(i <= (used/2))
	{

	    if( (2*i == used)
		|| (elttab[2*i-1] < elttab[2*i]))
		{
		    j = 2*i;
		}
	    else
		{
		    j = 2*i+1;
		}
	    if (elttab[i-1] >= elttab[j-1])
		{
		    swap_elt = elttab[i-1];
		    elttab[i-1]  = elttab[j-1];
		    elttab[j-1] = swap_elt;
		    i=j;
		}
	    else
		break;
	}
    return return_elt;
}



int Heap_Size(Heap *q)
{
    return q->used;
}


void Heap_Clear(Heap *q)
{
    q->used = 0;
}


/** Read the next element that 'll be given by Heap_Get 
    but not retrieve it from the Heap_ **/
int Heap_Read(Heap *q)
{
  return q->elttab[0];
}



/* @authors P. HENON */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "phidal_common.h"

void phidal_block_pattern(dim_t tli, dim_t tlj, dim_t bri, dim_t brj, char *UPLO, char *DIAG, csptr P, int_t locally_nbr, PhidalHID *BL)
{
  /*************************************************************************/
  /* This function computes the phidal sparse block pattern                */
  /* for the area of the matrix between the block (tli, tlj) and the       */
  /* (bri, brj).                                                           */
  /* The cs matrix P has only non-empty rows between rows tli and bri      */
  /* i.e. P is indexed with absolute indices                               */
  /*                                                                       */
  /*************************************************************************/

  dim_t i, j, k, l;
  int e, s, llb;
  int *block_keyindex;
  int *block_key;
  int lcblknbr;
  dim_t *ja1, *ja2, n1, n2;
  int *tmpptr;
  int uplo;
  int udiag;
  flag_t job; 
  int ind; 
  int **domco; /** domconlist[i] is the list of connector in
		       domain i **/
  int *domconbr;
  int domnum;
  int transposed;
  int *tpl;
  int tpln;


#ifdef DEBUG_M
  assert(P->n == BL->nblock);
#endif

  if(bri < 0)
    bri = BL->nblock-1;
  if(brj < 0)
    brj = BL->nblock-1;
  if(tli<0)
    tli = 0;
  if(tlj<0)
    tlj = 0;

  

  transposed = 0;
  /*fprintfv(5, stdout, "Avant transpose tli = %d tlj = %d bri = %d brj = %d \n", tli, tlj, bri, brj);*/

  if(strcmp(UPLO, "N") == 0)
    {
      if( !( (tlj > bri) || (tli > brj) ) && !(tli == tlj && bri == brj))
	{
	  fprintfd(stderr, "Error in phidal_block_pattern: Matrix must be in the lower or upper triangular part ! \n");
	  exit(-1);
	}
      uplo = 0;

      if(tli > brj)
	{
	  /*** We need to compute in the upper triangular part ****/
	  /*** so we compute the pattern for the transposed matrice 
	       and at the end we re-transposed the patter      ****/
	  n1 = tli;
	  n2 = bri;
	  tli = tlj;
	  bri = brj;
	  tlj = n1;
	  brj = n2;
	  transposed = 1;

	}

    }
  else
    {
      if(tli != tlj || bri != brj)
	{
	  fprintfd(stderr, "Error in phidal_block_pattern:  Matrix must be symmetric \n");
	  exit(-1);
	}
      
      if(strcmp(UPLO, "L") == 0)
	uplo = 1;
      else
	{
	  if(strcmp(UPLO, "U") == 0)
	    uplo = 2;
	  else
	    {
	      fprintfd(stderr, "BAD PARAMETERS in phidal_block_pattern \n");
	      exit(-1);
	    }
	}
    }
  
  if(strcmp(DIAG, "N") == 0)
    udiag = 0;
  else
    {
      if(strcmp(DIAG, "U") == 0)
	udiag = 1;
      else
	{
	  fprintfd(stderr, "BAD PARAMETERS in phidal_block_pattern \n");
	  exit(-1);
	}
    }	
  
  
 
  if(locally_nbr >= BL->nlevel)
    locally_nbr = BL->nlevel-1;
  if(locally_nbr < 0)
    locally_nbr = 0;

  lcblknbr =  BL->block_levelindex[locally_nbr+1];
  
  block_keyindex = BL->block_keyindex;
  block_key = BL->block_key;
  
  ja1 = (int *)malloc(sizeof(int)* (brj-tlj+1));
  ja2 = (int *)malloc(sizeof(int)* (brj-tlj+1));

  domco = (int **)malloc(sizeof(int *)*BL->ndom);
  domconbr   = (int *)malloc(sizeof(int)*BL->ndom);
  tpl   = (int *)malloc(sizeof(int)*BL->ndom);
 

  /*** WE WANT THE UPPER PART OF P in CSR FORMAT ***/
  /** build domco list for all the domain in the connector key of
      connector [tlj:brj] **/
  tpln = 0;
  bzero(domconbr, sizeof(int)*BL->ndom);
  for(j=tlj;j<=brj;j++)
    for(k=block_keyindex[j];k<block_keyindex[j+1];k++)
      {
	domnum =block_key[k]; 
	if(domconbr[block_key[k]] == 0)
	  tpl[tpln++] = domnum; /** Mark the domain that have at least
				   one connector **/
	domconbr[block_key[k]]++;
      }
  for(j=0;j<tpln;j++)
    {
      domnum = tpl[j];
      domco[domnum] = (int *)malloc(sizeof(int)*domconbr[domnum]);
    }

  bzero(domconbr, sizeof(int)*BL->ndom);
  for(j=tlj;j<=brj;j++)
    for(k=block_keyindex[j];k<block_keyindex[j+1];k++)
      {
	domnum =block_key[k]; 

	if(domnum >= BL->ndom)
	  fprintfd(stderr, "domnum %ld ndom %ld \n", (long)domnum, (long)BL->ndom);
	assert(domnum < BL->ndom);
	domco[domnum][domconbr[domnum]] = j;
	domconbr[block_key[k]]++;
      }
  

  /** Build the pattern for the upper block triangular pattern  of the matrix **/
  /** Locally consistent part **/
  llb = MIN(bri, lcblknbr-1);
  for(i=tli;i<=llb;i++)
    {
      domnum = block_key[block_keyindex[i]];
      memcpy(ja1, domco[domnum], sizeof(int)*domconbr[domnum]);
      n1 = domconbr[domnum];
      for(k=block_keyindex[i]+1;k<block_keyindex[i+1];k++)
	{
	  domnum = block_key[k];
	  UnionSet(ja1, n1, domco[domnum], domconbr[domnum], ja2, &n2);
	  /** Swap ja1 and ja2 **/
	  tmpptr = ja1;
	  ja1 = ja2;
	  n1 = n2;
	  ja2 = tmpptr;

	  /** Keep only the term in the upper triangular matrix **/
	  if(udiag == 0)
	    {
	      ind = 0;
	      for(j=0;j<n1;j++)
		if(ja1[j] >= i)
		  ja1[ind++] = ja1[j];
	      n1 = ind;
	    }
	  else
	    {
	      ind = 0;
	      for(j=0;j<n1;j++)
		if(ja1[j] > i)
		  ja1[ind++] = ja1[j];
	      n1 = ind;
	    }

	}

      P->nnzrow[i] = n1;
      if(n1>0)
	{
	  P->ja[i] = (int *)malloc(sizeof(int)*n1);
	  memcpy(P->ja[i], ja1, sizeof(int)*n1);
	}
    }

  /**********************************/
  /**** Strictly consistent part ****/
  /**********************************/
  /*** There was error in the previous
       phidal_block_pattern
       some fill could occure inside a strictly
       level: connector A key = 1,2,3 could 
       be connected with connector B key = 1, 2
       (hapen in OILPAN domsize = 300 )
       even if they were in the same level;
  To prevent this we force the diagonal block structure 
  inside strictly levels **/

  for(l=locally_nbr+1;l<BL->nlevel;l++)
    {
      s = MAX(BL->block_levelindex[l],tli);
      if(s>bri)
	break;
      e = MIN(bri, BL->block_levelindex[l+1]-1);
      for(i=s;i<=e;i++)
	{
#ifdef DEBUG_M
	  assert(i >= tli);
	  assert(i <= bri);
#endif
	  domnum = block_key[block_keyindex[i]];
	  memcpy(ja1, domco[domnum], sizeof(int)*domconbr[domnum]);
	  n1 = domconbr[domnum];

	  /** Keep only the term in the upper triangular matrix
	      and delete any fill-in block in the level outside the
	      diagonal **/
	  if(udiag == 0)
	    {
	      ind = 0;
	      for(j=0;j<n1;j++)
		if(ja1[j] > e || ja1[j] == i)
		  ja1[ind++] = ja1[j];
	      n1 = ind;
		}
	  else
	    {
	      ind = 0;
	      for(j=0;j<n1;j++)
		if(ja1[j] > e )
		  ja1[ind++] = ja1[j];
	      n1 = ind;
	    }
	  
	  for(k=block_keyindex[i]+1;k<block_keyindex[i+1];k++)
	    {
	      if(n1 == 0)
		break;
	      domnum = block_key[k];
	      IntersectSet(ja1, n1, domco[domnum], domconbr[domnum], ja2, &n2);
	      /** Swap ja1 and ja2 **/
	      tmpptr = ja1;
	      ja1 = ja2;
	      n1 = n2;
	      ja2 = tmpptr;
	      
	    }
	  
	  P->nnzrow[i] = n1;
	  if(n1>0)
	    {
	      P->ja[i] = (int *)malloc(sizeof(int)*n1);
	      memcpy(P->ja[i], ja1, sizeof(int)*n1);
	    }
	  
	}
    }
      
  job = 0; /** job = 0 only pattern is concerned **/
  /*fprintfd(stdout, "OO tli = %d tlj = %d bri = %d brj = %d \n", tli, tlj, bri, brj);
  for(i=0;i<P->n;i++)
  if(P->nnzrow[i] > 0)
  for(j=0;j<P->nnzrow[i];j++)
  fprintfd(stdout, "%d %d \n", i, P->ja[i][j]);
  fprintfd(stdout, "\n\n");*/

  CS_SetNonZeroRow(P);

  /*** Take the part of the pattern that has been asked for ***/
  if(uplo == 0)
    {
      if(tli == tlj && bri == brj)
	CS_SymmetrizeTriang(job, "U", P);
      else
	{
#ifdef DEBUG_M
	  assert(tlj > bri); 
#endif
	  /** The matrix is entirely in the upper
	      part : retranspose to get the orginal
	      if needed**/
	  if(transposed == 1)
	    {
	      n1 = tli;
	      n2 = bri;
	      tli = tlj;
	      bri = brj;
	      tlj = n1;
	      brj = n2;
	      CS_Transpose(job, P, P->n);
	      /*for(i=tli;i<=bri;i++)
		if(P->nnzrow[i] > 0)
		for(j=0;j<P->nnzrow[i];j++)
		fprintfd(stdout, "%d %d \n", i, P->ja[i][j]);*/
  
	    }
	}
    }

  if(uplo == 1) /** P is only for square matrix: a SYMMETRIC pattern **/
    {
#ifdef DEBUG_M
      assert(tli == tlj && bri == brj);
#endif
      CS_Transpose(job, P, P->n); 
    }
#ifdef DEBUG_M
  if(uplo == 2)
    assert(tli == tlj && bri == brj);
#endif
  
  /*** Fill the ma tab (index of the cs matrix in the PhidalMatrix ***/
  ind = 0;
  for(i=tli;i<=bri;i++)
    if(P->nnzrow[i] > 0)
      {
	P->ma[i] = (COEF *)malloc(sizeof(COEF)*P->nnzrow[i]);
	for(j=0;j<P->nnzrow[i];j++)
	  P->ma[i][j] =(COEF) ind++;
      }
  
  /*fprintfd(stdout, "tli = %d tlj = %d bri = %d brj = %d \n", tli, tlj, bri, brj);
    dumpCS(0, stdout, P);	*/

  
  for(j=0;j<tpln;j++)
    free(domco[tpl[j]]);
  free(domco);
  free(domconbr);
  free(tpl);
  free(ja1);
  free(ja2);
  
  CS_SetNonZeroRow(P);
}

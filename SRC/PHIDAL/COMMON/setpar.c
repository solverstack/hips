/* @authors P. HENON */

#include <stdlib.h>
#include <stdio.h>

#include "phidal_common.h"

#define BUFLEN 200

void GENERAL_setpar(char *filename,  char *matrix, char *sfile_path, int *unsym, int *rsa,  PhidalOptions *option) 
{
  /* setup parameters for preconditioning and iteration */
  FILE *fp;
  char buf[BUFLEN];
  dim_t num;

  /* read parameters for preconditioner and iteration from file
     'input' */
    
  if(filename == NULL)
    {
      if((fp = fopen("inputs","r")) == NULL) 
	{
	  fprintfv(5, stderr, "Cannot open file inputs\n");
	  exit(1);
	}
    }
  else
    if( (fp = fopen(filename, "r")) == NULL )
      {
	fprintfv(5, stderr, "Cannot open file ARGV. Trying inputs file ... \n"); 
	if((fp = fopen("inputs","r")) == NULL) 
	  {
	    fprintfv(5, stderr, "Cannot open file inputs\n");
	    exit(1);
	  }
      }
  num = 0;

  while(fgets(buf, BUFLEN, fp) != NULL) {
    switch(num) {
    case 0:                             /* matrix */
      sscanf(buf, "%s", matrix); 
      break;
    case 1:                             /* unsym */
      sscanf(buf, "%d", rsa);
      switch(*rsa){
      case 0:
	(*rsa) = 0;
	(*unsym) = 1;
	option->symmetric = 0;
	break;
      case 1:
	(*rsa) = 0;
	(*unsym) = 0;
	option->symmetric = 0;
	break;
      case 2:
	(*rsa) = 1;
	(*unsym) = 1;
	option->symmetric = 1;
	/* option->krylov_method = 1; */

      break;
      default:
	break;
      }
      break;
    case 2:
      sscanf(buf, "%d", &(option->verbose));
      break;
    case 3:                             /* scale */
      sscanf(buf, "%d", &(option->scale));
      if(option->scale > 0)
	{
	  if( (*rsa) == 1 )
	    option->scale = 2;
	  else
	    option->scale = 1;
	}
      break;
    case 4:                            /* Numer of level in locally droping strategy **/
      sscanf(buf, "%d", &(option->locally_nbr)); 
      break;
    case 5:                             /* outer tol */
      sscanf(buf,_scan_f_, &(option->tol));
      break;
    case 6:                             /* im (Outer Krylov subs dim) */
      sscanf(buf,"%d",&(option->krylov_im));
      break;
    case 7:                             /* max outer gmres steps */
      sscanf(buf,"%d", &(option->itmax));
      break;
    case 8:
      sscanf(buf,"%d",&(option->forwardlev));
      break;
    case 9:                             
      sscanf(buf,"%d", &(option->schur_method));
      break;
    case 10:                             
      sscanf(buf,"%d", &(option->itmaxforw));
      break;
    case 11:                             
      sscanf(buf,_scan_f_, &(option->itforwrat));
      break;
    case 12:                             
      sscanf(buf, _scan_f_, &(option->droptol0));
      break;
    case 13:                             
      sscanf(buf, _scan_f_, &(option->droptol1));
      break; 
    case 14:                             /*droptol for Schur complement  */
      sscanf(buf, _scan_f_, &(option->droptolE));
      option->dropSchur = 0;
      break;
    case 15:
      if(sfile_path != NULL)
	sscanf(buf, "%s", sfile_path); /** path_file is the file name prefix of the local data files of all processors **/  

    default:
      break;
    }
    num++;
  }
  fclose(fp);
  
}

void GRID_setpar(char *filename,  int *nc, int *cube, char *sfile_path, int *unsym, int *rsa,  PhidalOptions *option) 
{
  /* setup parameters for preconditioning and iteration */
  FILE *fp;
  char buf[BUFLEN];
  dim_t num;

  /* read parameters for preconditioner and iteration from file
     'input' */
    
  if(filename == NULL)
    {
      if((fp = fopen("inputs","r")) == NULL) 
	{
	  fprintfv(5, stderr, "Cannot open file inputs\n");
	  exit(1);
	}
    }
  else
    if( (fp = fopen(filename, "r")) == NULL )
      {
	fprintfv(5, stderr, "Cannot open file ARGV. Trying inputs file ... \n"); 
	if((fp = fopen("inputs","r")) == NULL) 
	  {
	    fprintfv(5, stderr, "Cannot open file inputs\n");
	    exit(1);
	  }
      }
  num = 0;

  while(fgets(buf, BUFLEN, fp) != NULL) {
    switch(num) {
    case 0:                             /* matrix */
      sscanf(buf, "%d", nc); 
      break;
    case 1:                             /* matrix */
      sscanf(buf, "%d", cube); 
      break;  
    case 2:                             /* unsym */
      sscanf(buf, "%d", rsa);
      switch(*rsa){
      case 0:
	(*rsa) = 0;
	(*unsym) = 1;
	option->symmetric = 0;
	break;
      case 1:
	(*rsa) = 0;
	(*unsym) = 0;
	option->symmetric = 0;
	break;
      case 2:
	(*rsa) = 1;
	(*unsym) = 1;
	option->symmetric = 1;
	/* option->krylov_method = 1; */

      break;
      default:
	break;
      }
      break;
    case 3:
      sscanf(buf, "%d", &(option->verbose));
      break;
    case 4:                             /* scale */
      sscanf(buf, "%d", &(option->scale));
      if(option->scale > 0)
	{
	  if( (*rsa) == 1 )
	    option->scale = 2;
	  else
	    option->scale = 1;
	}
      break;
    case 5:                            /* Numer of level in locally droping strategy **/
      sscanf(buf, "%d", &(option->locally_nbr)); 
      break;
    case 6:                             /* outer tol */
      sscanf(buf,_scan_f_, &(option->tol));
      break;
    case 7:                             /* im (Outer Krylov subs dim) */
      sscanf(buf,"%d",&(option->krylov_im));
      break;
    case 8:                             /* max outer gmres steps */
      sscanf(buf,"%d", &(option->itmax));
      break;
    case 9:
      sscanf(buf,"%d",&(option->forwardlev));
      break;
    case 10:                             
      sscanf(buf,"%d", &(option->schur_method));
      break;
    case 11:                             
      sscanf(buf,"%d", &(option->itmaxforw));
      break;
    case 12:                             
      sscanf(buf,_scan_f_, &(option->itforwrat));
      break;
    case 13:                             
      sscanf(buf, _scan_f_, &(option->droptol0));
      break;
    case 14:                             
      sscanf(buf, _scan_f_, &(option->droptol1));
      break; 
    case 15:                             /*droptol for Schur complement  */
      sscanf(buf, _scan_f_, &(option->droptolE));
      option->dropSchur = 0;
      break;
    case 16:
      if(sfile_path != NULL)
	sscanf(buf, "%s", sfile_path); /** path_file is the file name prefix of the local data files of all processors **/  

    default:
      break;
    }
    num++;
  }
  fclose(fp);
  
}




void PhidalOptions_Print(FILE *file, PhidalOptions *option)
{

  fprintfv(5, file, "\n################################################################################## \n");
  fprintfv(5, file, "#  verbose = %d \n", option->verbose);
  switch(option->scale)
    {
    case 0:
      fprintfv(5, file, "# No normalisation used for dropping \n");
      break;
      /*case 1:
	fprintfv(5, file, "# Unsymmetric normalisation used for dropping \n");
	break;
	case 2:
	fprintfv(5, file, "# Symmetric normalisation used for dropping \n");
	break;*/
    case 1:
    case 2:
      fprintfv(5, file, "# Normalisation used for dropping in ILUT \n");
      break;
    default:
      fprintfv(5, file, "ERROR wrong normalisation argument = %d (should be 0,1 or 2)\n",option->scale);
    }
  
  /*fprintfv(5, file, "#  pivoting = %d \n", option->pivoting);
    fprintfv(5, file, "#  pivot_ratio = %g \n", option->pivot_ratio);*/
  fprintfv(5, file, "#  Number of connector levels using consistent dropping                     = %d \n", option->locally_nbr);
  fprintfv(5, file, "#  Relative residual tolerance for solution                                 = %g \n", option->tol);
  fprintfv(5, file, "#  Krylov subspace dimension                                                = %d \n", option->krylov_im);
  fprintfv(5, file, "#  Maximum number of iteration for outer accelerator                        = %d \n", option->itmax);
  fprintfv(5, file, "#  Number of FORWARD Schur recursion step                                   = %d \n", option->forwardlev);
  fprintfv(5, file, "#  SCHUR Method                                                             = %d \n", option->schur_method);
  if(option->schur_method == 0) {
    fprintfv(5, file, "#  No iteration in the Schur complement recursion \n");
      }
  else
    {
      fprintfv(5, file, "#  Maximum number of iterations in the forward recursion                    = %d \n", option->itmaxforw);
      fprintfv(5, file, "#  Ratio for max. it. number from a forward recursion level to another      = %g \n", option->itforwrat);
    }

  fprintfv(5, file, "#  Numerical threshold to build at the outer recursion level                = %g \n", option->droptol0);
  if(option->forwardlev > 0)
    {
      fprintfv(5, file, "#  Num. Thresh. in first level of recursion:                                = %g \n", option->droptol1);
      fprintfv(5, file, "#  Num. Thresh. to drop the coupling matrix E                               = %g \n", option->droptolE);
    }
  /*fprintfv(5, file, "#  Fill ratio control parameter = %g \n", option->fillrat);*/
  fprintfv(5, file, "################################################################################## \n");
}




void PhidalOptions_Fix(PhidalOptions *options, PhidalHID *BL)
{
  options->locally_nbr = MIN(BL->nlevel, options->locally_nbr);
  
  if(options->forwardlev == -1)
    options->forwardlev = BL->nlevel-1;
  
  options->forwardlev = MIN(BL->nlevel-1, options->forwardlev);
  
}

/* @authors P. HENON */

#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <stdio.h>
#include <assert.h>

#include "phidal_common.h"

int is_in_key(const int p, const int block, const int *block_keyindex, const int *block_key)
{
  dim_t i;
  for(i=block_keyindex[block];i<block_keyindex[block+1];i++)
    if(p==block_key[i])
      return TRUE;
  return FALSE;
}

int is_in_key2(const int p, const INTL block, const INTL *block_keyindex, const mpi_t *block_key)
{
  dim_t i;
  for(i=block_keyindex[block];i<block_keyindex[block+1];i++)
    if(p==block_key[i])
      return TRUE;
  return FALSE;
}



int is_intersect_key(int kl1, int *key1, int  kl2, int *key2)
{
  int i1, i2;
  i1 = 0;
  i2 = 0;
  while(i1 < kl1 && i2 < kl2)
    {
      if(key1[i1] < key2[i2])
	{
	  i1++;
	  continue;
	}
      if(key1[i1] > key2[i2])
	{
	  i2++;
	  continue;
	}
      if(key1[i1] == key2[i2])
	return 1;
    }
  
  return 0;

}

int is_equal_key(int *key1, int kl1, int *key2, int kl2)
{
  /*-------------------------------------------------
  / Check if two keys are equal 
  /
  / ON ENTRY:
  /      key1, key2 : pointer to the keys
  /      kl1, kl2   : length of the keys 
  / ON RETURN:
  / an int       : 1 : keys are equal
  /                0 : keys are not equal
  /--------------------------------------------------*/
  dim_t k;
  if(kl1 != kl2)
    return 0; /* keys are not of the same length => not equal */
  
  for(k=0;k<kl1;k++)
    if(key1[k] != key2[k])
      return 0;

  return 1;
}


int key_compare(int kl1, int *key1, int kl2, int *key2)
{
  /********************************************/
  /* Compare two key                          */
  /* On entry:                                */
  /* kl1, kl2 : key lengths                   */
  /* key1, key2 : keys                        */
  /* On return:                               */
  /* -1 : key1 is included in key2            */
  /*  0 : key1 == key2                        */
  /*  1 : key2 is included in key1            */
  /*  other : key1 and key2 are not comparable*/
  /********************************************/
  int k1, k2;
  int elt, flag;

  if(kl1 > kl2)
    return -key_compare(kl2, key2, kl1, key1);


  k1 = 0;
  k2 = 0;
  while(k1 < kl1)
    {
      elt = key1[k1];
      flag = 0;
      while(k2 < kl2)
	if(elt == key2[k2])
	  {
	    flag = 1;
	    break;
	  }
	else
	  k2++;

      if(flag == 0)
	return 2; /*** Key are not comparable ***/
      k1++;
    }

  if(kl1 < kl2)
    return -1;

#ifdef DEBUG_M
  assert(kl1 == kl2);
  assert(key_compare(kl2, key2, kl1, key1) == 0);
#endif

  return 0;


}

void intersect_key(dim_t nk1, dim_t nk2, int *block_key1, int *block_key2, int *ret_key, int *nk)
{
  /*-----------------------------------------------------------/
  / Get the intersection of 2 keys                             /
  / MUST ALLOCATE THE RETURNED KEY BEFORE CALL                 /
  /-----------------------------------------------------------*/
  int i1, i2;
#ifdef DEBUG_M
  assert(ret_key != NULL);
#endif

  i1 = 0;
  i2 = 0;
  (*nk) = 0;
  while(i1 < nk1 && i2 < nk2)
    {
      if(block_key1[i1] < block_key2[i2])
	{
	  i1++;
	  continue;
	}
      if(block_key1[i1] > block_key2[i2])
	{
	  i2++;
	  continue;
	}
      
      /** We have block_key[i1] == block_key[i2] **/
      ret_key[(*nk)] = block_key1[i1];
      (*nk)++;
      i1++;
      i2++;

    }
#ifdef DEBUG_M
  for(i1=1;i1<(*nk);i1++)
    assert(ret_key[i1]>ret_key[i1-1]);
#endif

}

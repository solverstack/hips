/* @authors P. HENON */

#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <stdio.h>
#include <assert.h>

#include "phidal_common.h"
#include "scotch_metis_wrapper.h"

REAL CSC_LDLt_countops_rec(csptr mat, int fcol, int lcol);


void csr_matvec_add(dim_t n, INTL *ia, dim_t *ja, COEF *a, COEF *x, COEF *y)
{
  dim_t i, j;

  for(i=0;i<n;i++)
    for(j=ia[i];j<ia[i+1];j++)
      y[i] += x[ja[j]]*a[j];
}



void ijv2csr(dim_t n, LONG nnz, dim_t *ii, dim_t *jj, COEF *val, INTL **ir, dim_t **jr, COEF **ar)
{
  /*********************************************************************/
  /* This function covert a matrix in coordinate format into a matrix  */
  /* in CSR format                                                     */
  /*********************************************************************/
  dim_t i, k;
  INTL *ia;
  dim_t *ja;
  COEF *a;
  
  ia = (INTL *)malloc(sizeof(INTL)*(n+1));
  
  /** Count the number of zero in each row **/
  bzero(ia, sizeof(INTL)*(n+1));
  for(i=0;i<nnz;i++)
    ia[ii[i]+1]++;

  /** Set up ia */
  for(i=1;i<=n;i++)
    ia[i] += ia[i-1];

#ifdef DEBUG_M
  assert(ia[n] == nnz);
#endif

  /** Set up ja and a **/
  ja = (dim_t *)malloc(sizeof(dim_t)*(nnz));
  if(val != NULL)
    a = (COEF *)malloc(sizeof(COEF)*(nnz));
  else
    a = NULL;

  if(val != NULL)
    for(i=0;i<nnz;i++)
      {
	k = ia[ii[i]];
#ifdef DEBUG_M
	assert(k<nnz);
#endif
	ja[k] = jj[i];
	a[k] = val[i];
	ia[ii[i]]++;
      }
  else
    for(i=0;i<nnz;i++)
      {
#ifdef DEBUG_M
	assert(ia[ii[i]] < nnz);
#endif
	ja[ia[ii[i]]++] = jj[i];
      }
  
  /** re-switch ia **/
  for(i=n;i>0;i--)
    ia[i] = ia[i-1];
  ia[0] = 0;

  /*for(i=0;i<n;i++)
    {
      assert(ia[i] >= 0);
      assert(ia[i] < nnz);
      ja[ia[i]]++;
      ja[ia[i]]--;
      }*/



  /** Suppress REAL **/
  CSR_SuppressDouble(1, n, ia, ja, a);

  if(ia[n] != nnz)
    {
      ja = (dim_t *)realloc(ja, sizeof(dim_t)*(ia[n])); 
      if(val !=NULL)
	a = (COEF *)realloc(a, sizeof(COEF)*(ia[n])); 
    }

  *ir = ia;
  *jr = ja;
  if(val != NULL)
    *ar = a;
}



int CSR_SuppressZeros(flag_t numflag, dim_t n, INTL *ia, dim_t *ja, COEF *a)
{
  /*************************************************************/
  /* This function suppress all the zeros term in a CSR matrix */
  /*************************************************************/
  dim_t i, j;
  INTL ind, s;

#ifdef DEBUG_M
  checkCSR(n, ia, ja, a, numflag);
#endif

  /** Convert the matrix in C numbering (start from 0 instead of 1) **/
  if(numflag == 1)
    CSR_Fnum2Cnum(ja, ia, n);

  ind = 0;
  for(i=0;i<n;i++)
    {
      s = ind;
      for(j=ia[i];j<ia[i+1];j++)
	if(ja[j] >= 0)
	  if(a[j] != 0 || ja[j] == i)
	    {
	      ja[ind]=ja[j];
	      a[ind++]=a[j];
	    }
      ia[i] = s;
	  
    }

  ia[n] = ind;


  if(numflag == 1)
    /** Reconvert the matrix in Fortran numbering (start from 0 instead of 1) **/
    CSR_Cnum2Fnum(ja, ia, n);

#ifdef DEBUG_M
  checkCSR(n, ia, ja, a, numflag);
#endif

  return ind;
}




void set_coeff(csptr A, csptr P)
{
  /*********************************************************************/
  /* This function copy the coefficients of a matrix A into            */
  /* a matrix P that has a sparse pattern that contains the sparse     */
  /* pattern of the matrix A                                           */
  /* NOTE: the matrix A and P must have their rows sorted by ascending */
  /*  indices                                                          */
  /*********************************************************************/

  dim_t i, j;
  int ind;
  int *Aja, *Pja;
  COEF *Ama, *Pma;
  dim_t nnzrowi;
  for(i=0;i<A->n;i++)
    {
      nnzrowi = A->nnzrow[i];
      Aja = A->ja[i];
      Ama = A->ma[i];
      Pja = P->ja[i];
      Pma = P->ma[i];
      ind = 0;
      
      /** Do not consider the upper triangular part of A **/
      /** NOTE: The matrices are in CSC format **/
      for(j=0;j<nnzrowi;j++)
	if(Aja[j]>=i)
	  break;
      
      for(;j<nnzrowi;j++)
	{
	  while(Pja[ind] < Aja[j])
	    ind++;
#ifdef DEBUG_M
	  assert(Pja[ind] == Aja[j]);
#endif
	  Pma[ind] = Ama[j];
	}
    }


  /* CS_SetNonZeroRow(P); PAS UTILE **/
}


void sort_row(csptr P)
{
  /***********************************************************************************************/
  /* This function sort all the row of a symbolic matrix (just the pattern) by ascending indices */
  /* This is done in place                                                                       */
  /***********************************************************************************************/
  dim_t i, k;
  
  /*for(i=0;i<P->n;i++)*/
  for(k=0;k<P->nnzr;k++)
    {
      i = P->nzrtab[k];
      if(P->nnzrow[i] > 1)
	quicksort(P->ja[i], 0, P->nnzrow[i]-1);
    }
}


REAL CSnorm1(csptr mat)
{
  /*-----------------------------------/
  / Return sum of fabs of nnz          /
  / in a matrix in SparRow format      /
  /-----------------------------------*/
  REAL sum;
  dim_t i, j, k;

  sum = 0.0;


  /*for(i=0;i<mat->n;i++)*/
  for(k=0;k<mat->nnzr;k++)
    {
      i = mat->nzrtab[k];
      for(j=0;j<mat->nnzrow[i];j++)
	sum += coefabs(mat->ma[i][j]);
    }
  
  return sum;
}

REAL CSnormFrob(csptr mat)
{
  /*-----------------------------------/
  / Return Frobenius norm              /
  / of a matrix in SparRow format      /
  /-----------------------------------*/
  REAL sum;
  REAL rsum;
  dim_t i, j, k;

  sum = 0.0;

  /*for(i=0;i<mat->n;i++)*/
  for(k=0;k<mat->nnzr;k++)
    {
      i = mat->nzrtab[k];
      rsum = 0.0;
      for(j=0;j<mat->nnzrow[i];j++)
	rsum += mat->ma[i][j]*mat->ma[i][j];
      sum += rsum;
    }
  
  sum = sqrt(sum);
  return sum;
}


REAL CSnormSquare(csptr mat)
{
  /*-----------------------------------/
  / Return Frobenius norm              /
  / of a matrix in SparRow format      /
  /-----------------------------------*/
  REAL sum;
  REAL rsum;
  dim_t i, j, k;

  sum = 0.0;

  /*for(i=0;i<mat->n;i++)*/
  for(k=0;k<mat->nnzr;k++)
    {
      i = mat->nzrtab[k];
      rsum = 0.0;
      for(j=0;j<mat->nnzrow[i];j++)
	rsum += mat->ma[i][j]*mat->ma[i][j];
      sum += rsum;
    }
  
  return sum;
}
      

long CSnnz(csptr mat)
{
  /*-----------------------------------/
  / Return number of non zero entries  /
  / in a matrix in SparRow format      /
  /-----------------------------------*/
  long nnz;
  dim_t i, k;

  nnz = 0;
  /*for(i=0;i<mat->n;i++)*/
  for(k=0;k<mat->nnzr;k++)
    {
      i = mat->nzrtab[k];
      nnz += (long)mat->nnzrow[i];
    }
  
  return nnz;
}
    


void CS_GetLower(flag_t job, char *DIAG, csptr a, csptr l)
{
  /***********************************************/
  /* This function copy the lower part of a      */
  /* in matrix l                                 */
  /* if DIAG = "U" then the diagonal is ignored  */
  /* if DIAG = "N" then there is always a        */
  /* diagonal entry in a row (set to 0.0 is nece */
  /* rary) and place at the end of the row       */
  /* NOTE: can be done in place (a == l)         */
  /***********************************************/
  int udiag ;
  int ind;
  int i, j, ii;
  int *tmpi;
  REAL *tmpd=NULL;


  tmpi = (int *)malloc(sizeof(int)*a->n);
  if(job == 1)
    tmpd = (REAL *)malloc(sizeof(REAL)*a->n);
  
  if(strcmp(DIAG, "U") == 0)
    udiag = 1;
  else
    udiag = 0;

 
  
  if(udiag == 0)
    for(i=0;i<a->n;i++)
      {
	ind = 0;
	ii = -1;
	for(j=0;j<a->nnzrow[i];j++)
	  if(a->ja[i][j] < i)
	    {
	      
	      tmpi[ind] = a->ja[i][j];
	      if(job == 1)
		tmpd[ind] = a->ma[i][j];
	      ind++;
	    }
	  else
	    if(a->ja[i][j] == i)
	      ii = j;


	if(ii == -1) /** No diagonal element found **/
	  {
	    tmpi[ind] = i;
	    if(job == 1)
	      tmpd[ind] = 0.0;
	    ind++;
	  }
	else
	  {
	    tmpi[ind] = i;
	    if(job == 1)
	      tmpd[ind] = a->ma[i][ii];
	    ind++;
	  }
	

	if(ind >0)
	  {
	    if(l->inarow != 1)
	      l->ja[i] = (int *)realloc(l->ja[i], sizeof(int)*ind);
	    else
	      l->ja[i] = (int *)malloc(sizeof(int)*ind);
	    memcpy(l->ja[i], tmpi, sizeof(int)*ind);

	    if(job == 1)
	      {
		if(l->inarow != 1)
		  l->ma[i] = (COEF *)realloc(l->ma[i], sizeof(COEF)*ind);
		else
		  l->ma[i] = (COEF *)malloc(sizeof(COEF)*ind);
		
		memcpy(l->ma[i], tmpd, sizeof(COEF)*ind);
	      }
	  }
	else
	  {
	    if(l->nnzrow[i]> 0 &&  l->inarow != 1)
	      {
		free(l->ja[i]);
		if(job == 1)
		  free(l->ma[i]);
	      }
	    l->ja[i] = NULL;
	    if(job == 1)
	      l->ma[i] = NULL;
	  }
	l->nnzrow[i] = ind;
      }
  else
    for(i=0;i<a->n;i++)
      {
	ind = 0;
	for(j=0;j<a->nnzrow[i];j++)
	  if(a->ja[i][j] < i)
	    {
	      tmpi[ind] = a->ja[i][j];
	      if(job == 1)
		tmpd[ind] = a->ma[i][j];
	      ind++;
	    }
	

	if(ind>0)
	  {
	    if(l->inarow != 1)
	      l->ja[i] = (int *)realloc(l->ja[i], sizeof(int)*ind);
	    else
	      l->ja[i] = (int *)malloc(sizeof(int)*ind);

	    memcpy(l->ja[i], tmpi, sizeof(int)*ind);
	    if(job == 1)
	      {
		if(l->inarow != 1)
		  l->ma[i] = (COEF *)realloc(l->ma[i], sizeof(COEF)*ind);
		else
		  l->ma[i] = (COEF *)malloc(sizeof(COEF)*ind);

		memcpy(l->ma[i], tmpd, sizeof(COEF)*ind);
	      }
	  }
	else
	  {
	    if(l->inarow != 1 && l->nnzrow[i]> 0 )
	      {
		free(l->ja[i]);
		if(job == 1)
		  free(l->ma[i]);
	      }
	    l->ja[i] = NULL;
	    if(job == 1)
	      l->ma[i] = NULL;
	  }
	l->nnzrow[i] = ind;
	
      }

  free(tmpi);
  if(job == 1)
    free(tmpd);

  if(l->inarow == 1)
    {
      if(l->matab != NULL)
	free(l->matab);
      if(l->jatab != NULL)
	free(l->jatab);
      l->inarow = 0;
    }
  

  CS_SetNonZeroRow(l);

#ifdef REALLOC_BLOCK
  if(job == 1)
    {
      if((REAL)a->n* (REAL)a->n <= (REAL)SMALLBLOCK)
	CSrealloc(l);

    }
#endif


} 


void CS_GetUpper(flag_t job, char *DIAG, csptr a, csptr u)
{
  /***********************************************/
  /* This function copy the upper part of a      */
  /* in matrix u                                 */
  /* job = 0 : ignore coeff                      */
  /* if DIAG = "U" then the diagonal is ignored  */
  /* if DIAG = "N" then there is always a        */
  /* diagonal entry in a row (set to 0.0 is nece */
  /* rary) and place at the begining of the row  */  /* NOTE: can be done in place (a == u)         */        
  /***********************************************/
  int udiag ;
  int ind;
  int i, j, ii;
  int *tmpi;
  COEF *tmpd=NULL;


  tmpi = (int *)malloc(sizeof(int)*a->n);
  if(job == 1)
    tmpd = (COEF *)malloc(sizeof(COEF)*a->n);
  
  if(strcmp(DIAG, "U") == 0)
    udiag = 1;
  else
    udiag = 0;
  
  if(udiag == 0)
    for(i=0;i<a->n;i++)
      {
	tmpi[0] = i;
	ii = -1;
	ind = 1;
	for(j=0;j<a->nnzrow[i];j++)
	  if(a->ja[i][j] > i)
	    {
	      
	      tmpi[ind] = a->ja[i][j];
	      if(job == 1)
		tmpd[ind] = a->ma[i][j];
	      ind++;
	    }
	  else
	    if(a->ja[i][j] == i)
	      ii = j;

	if(job == 1)
	  {
	    if(ii == -1) /** No diagonal element found **/
	      tmpd[0] = 0.0;
	    else
	      tmpd[0] = a->ma[i][ii];
	  }

	u->nnzrow[i] = ind;
	if(u->inarow != 1)
	  u->ja[i] = (int *)realloc(u->ja[i], sizeof(int)*ind);
	else
	  u->ja[i] = (int *)malloc(sizeof(int)*ind);
	memcpy(u->ja[i], tmpi, sizeof(int)*ind);
	if(job == 1)
	  {
	    if(u->inarow != 1)
	      u->ma[i] = (COEF *)realloc(u->ma[i], sizeof(COEF)*ind);
	    else
	      u->ma[i] = (COEF *)malloc(sizeof(COEF)*ind);
	    memcpy(u->ma[i], tmpd, sizeof(COEF)*ind);
	  }
      }
  else
    for(i=0;i<a->n;i++)
      {
	ind = 0;
	for(j=0;j<a->nnzrow[i];j++)
	  if(a->ja[i][j] > i)
	    {
	      tmpi[ind] = a->ja[i][j];
	      if(job == 1)
		tmpd[ind] = a->ma[i][j];
	      ind++;
	    }
	

	if(ind>0)
	  {
	    if(u->inarow != 1)
	      u->ja[i] = (int *)realloc(u->ja[i], sizeof(int)*ind);
	    else
	      u->ja[i] = (int *)malloc(sizeof(int)*ind);

	    memcpy(u->ja[i], tmpi, sizeof(int)*ind);
	    if(job == 1)
	      {
		if(u->inarow != 1)
		  u->ma[i] = (COEF *)realloc(u->ma[i], sizeof(COEF)*ind);
		else
		  u->ma[i] = (COEF *)malloc(sizeof(COEF)*ind);
		memcpy(u->ma[i], tmpd, sizeof(COEF)*ind);
	      }
	  }
	u->nnzrow[i] = ind;
      }

  free(tmpi);
  if(job == 1)
    free(tmpd);

  if(u->inarow == 1)
    {
      if(u->matab != NULL)
	free(u->matab);
      if(u->jatab != NULL)
	free(u->jatab);
      u->inarow = 0;
    }

  CS_SetNonZeroRow(u);
#ifdef REALLOC_BLOCK
  if(job == 1)
    {
      if((REAL)a->n*(REAL)a->n <= (REAL)SMALLBLOCK)
	CSrealloc(u);
    }
#endif
}


void ascend_column_reorder(csptr mat)
{
/*--------------------------------------/
/  This function reorder a matrix in    /
/  SparRow format such that the column  /
/ indices are in increasing order       /
/ within a row                          /
/--------------------------------------*/
  dim_t i;
  for(i=0;i<mat->n;i++)
    if(mat->nnzrow[i] > 1)
      {
	if(mat->ma[i] != NULL)
	  quicksort_row(mat->ja[i], mat->ma[i], 0, mat->nnzrow[i]-1);
	else
	  quicksort(mat->ja[i], 0, mat->nnzrow[i]-1);
      }
  
}

void ascend_column_reorder2(dim_t n, INTL *ia, dim_t *ja, COEF* a)
{
  /*--------------------------------------/
    /  This function reorder a matrix in    /
    /  CSR format such that the column      /
    / indices are in increasing order       /
    / within a row                          /
    /--------------------------------------*/
  dim_t i;
  dim_t nnzrow;
  
  for(i=0;i<n;i++) {
    nnzrow = ia[i+1] - ia[i];
    
    if(nnzrow > 1)
      {
	if(a != NULL)
	  quicksort_row(&ja[ia[i]], &a[ia[i]], 0, nnzrow-1);
	else
	  quicksort(&ja[ia[i]], 0, nnzrow-1);
      }
  } 
  
}


void CSR_SuppressDouble(flag_t op, dim_t n, INTL *ia, dim_t *ja, COEF  *a)
{
  /*************************************************************************/
  /* This function supress the coefficient having the same                 */
  /* column index                                                          */
  /* op = 0 Coefficient with same indexes : max modulus is retained        */
  /* op = 1 Coefficient with same indexes are summed                       */
  /* Coefficient with column index == -1 are ignored                       */
  /*************************************************************************/
  INTL i, j, ind, iai;
  dim_t pos;
  INTL *jrev;

#ifdef DEBUG_M
  assert(ia[0] == 0);
#endif
  jrev = (INTL *)malloc(sizeof(INTL)*n);

  for(i=0;i<n;i++)
    jrev[i] = -1;

  ind = 0;
  for(i=0;i<n;i++)
    {
      iai = ind;
      for(j=ia[i];j<ia[i+1];j++)
	{
	  pos = ja[j];
	  if(pos < 0) /** a non local term (see SetMatrixCoef_DG in hips.c) **/
	    continue;

	  if( jrev[pos] == -1)
	    {
	      ja[ind] = pos;
	      if(a != NULL)
		a[ind] = a[j];
	      jrev[pos] = ind;
	      ind++;
	    }
	  else
	    {
	      if(a != NULL)
		{
		  switch(op)
		    {
		    case 0:
		      if(coefabs( a[jrev[pos]]) <  coefabs(a[j]) )
			a[jrev[pos]] = a[j];
		      break;
		    case 1: 
		      a[jrev[pos]] += a[j];
		      break;
		    default :
		      assert(0);
		    }
		}
	    }
	}
      ia[i] = iai;
      /** reset jrev **/
      for(j=ia[i];j<ind;j++)
	jrev[ja[j]] = -1;
    }

  if(ind != ia[n])
    fprintfd(stderr, "CSR_SuppressDouble : nnz supress = %ld \n", (long)(ia[n] - ind));

  free(jrev);
  ia[n] = ind;
}



void extract_mat(csptr amat, csptr bmat, dim_t nrow, dim_t ncol, int *rowtab, int *coltab)
{
/*-------------------------------------------------------------------------------/
/ Extract the submatrix composed of rows and columns given in  rowtab and coltab /
/ The submatrix extracted is indexed with local indices (start from 0)           /
/ rowtab[i] becomes i                                                            /
/ amat is supposed to be a square matrix                                         /
/ OUPUT: bmat                                                                    /
/-------------------------------------------------------------------------------*/
  
  dim_t i, j;
  int jj;
  int *jrev;
  int *tmpj;
  COEF *tmp;
  
  int *jrow;
  COEF *marow;
  int rowind;
  int jpos;


  jrev = (int *)malloc(sizeof(int)*amat->n);
  tmpj = (int *)malloc(sizeof(int)*ncol);
  tmp  = (COEF *)malloc(sizeof(COEF)*ncol);

  
  /** Initialize the bmat structure **/
  initCS(bmat, nrow);
  
  for(i=0;i<amat->n;i++)
    jrev[i] = -1;

  for(i=0;i<ncol;i++)
    jrev[coltab[i]] = i;
  
  for(i=0;i<nrow;i++)
    {
      jj = 0;
      rowind = rowtab[i];
      jrow = amat->ja[rowind];
      marow = amat->ma[rowind];
      for(j=0;j<amat->nnzrow[rowind];j++)
	{
	  jpos  = jrev[jrow[j]];
	  if( jpos >= 0)
	  {
	    tmpj[jj] = jpos;
	    tmp[jj] = marow[j];
	    jj++;
	  }
	}
      bmat->nnzrow[i] = jj;
      if(jj>0)
	{
	  bmat->ja[i] = (int *)malloc(sizeof(int)*jj);
	  bmat->ma[i] = (COEF *)malloc(sizeof(COEF)*jj);
	  memcpy(bmat->ja[i], tmpj, sizeof(int)*jj);
	  memcpy(bmat->ma[i], tmp, sizeof(COEF)*jj);
	}
      else
	{
	  bmat->ja[i] = NULL;
	  bmat->ma[i] = NULL;
	}
	
    }

  free(jrev);
  free(tmp);
  free(tmpj);


  CS_SetNonZeroRow(bmat);
#ifdef REALLOC_BLOCK
  if((REAL)nrow*(REAL)ncol <= (REAL)SMALLBLOCK)
    CSrealloc(bmat);
#endif
}


void cs2csr(csptr mat, INTL *ia, dim_t *ja ,COEF *a)
{
  /***********************************************/
  /* Convert a SparRow matrix into a CSR matrix **/
  /* ia, ja and a MUST HAVE BEEN ALLOCATED      **/
  /***********************************************/
  dim_t i;
  INTL nnz;

  nnz = 0;
  
  for(i=0;i<mat->n;i++)
    {
      ia[i] = nnz;
      memcpy(ja + nnz, mat->ja[i], sizeof(int)* mat->nnzrow[i]);
      if(a!=NULL)
	memcpy(a + nnz, mat->ma[i], sizeof(COEF)* mat->nnzrow[i]);
      nnz += mat->nnzrow[i];
    }
  ia[mat->n] = nnz;

  

}


int CSRcs(dim_t n, COEF *a, dim_t *ja, INTL *ia, csptr bmat)
{
/*----------------------------------------------------------------------
| Convert CSR matrix to SparRow struct
|----------------------------------------------------------------------
| on entry:
|==========
| a, ja, ia  = Matrix stored in CSR format (with FORTRAN indexing).
|
| On return:
|===========
|
| ( bmat )  =  Matrix stored as SparRow struct.
|
|       integer value returned:
|             0   --> successful return.
|             1   --> memory allocation error.
|  NOTE: bmat must have been INITIALISED WITH A SUFFICIENT DIMENSION
|--------------------------------------------------------------------*/
  int i, j, j1, len, st; 
  COEF *bra=NULL;
  int *bja;
  /*	setup data structure for bmat (csptr) struct */
  /*if (setupCS(bmat, n)) {*/
 
  /* JE RETIRE CA CAR ON A BESOIN D'INITIALISER DES CS avec UNE TAILLE 
     SUPERIEUR A LA CSR DANS CERTAINS CAS 
     if(initCS(bmat, n)) 
     {
     printfv(5, " ERROR SETTING UP bmat IN initCS \n") ;
     exit(0);
     }*/
#ifdef DEBUG_M
  assert(bmat->n >= n);
#endif

  st = ia[0];
  for (j=0; j<n; j++) {
    len = ia[j+1] - ia[j];
    bmat->nnzrow[j] = len;
    if (len > 0) 
      {
	bja = (int *) malloc(len*sizeof(int));
	if(a != NULL)
	  bra = (COEF *) malloc(len*sizeof(COEF));
	
	
	if ( (bja == NULL) )
	  return(1) ; 
	i = 0;
	for (j1=ia[j] - st; j1<ia[j+1] - st; j1++) {
	  bja[i] = ja[j1] - st;
	  i++;
	}
	if(a != NULL)
	  {
	    i = 0;
	    for (j1=ia[j] - st; j1<ia[j+1] - st; j1++) {
	      bra[i] = a[j1] ;  
	      i++;
	    }	
	  }
	bmat->ja[j] = bja;
	bmat->ma[j] = bra;
      }
    else
      {
	bmat->ja[j] = NULL;
	bmat->ma[j] = NULL;
      }
  }

  for(j=n;j<bmat->n;j++)
    {
      bmat->nnzrow[j] = 0;
      bmat->ja[j] = NULL;
      bmat->ma[j] = NULL;
    }

  CS_SetNonZeroRow(bmat);	

#ifdef REALLOC_BLOCK
  /*CSrealloc(bmat); pas bon si a == NULL */
#endif

  return 0;
}

int CSrealloc(csptr mat)
{
  /********************************************************************/
  /* This function realloc a SparRow matrix in order to allocate the  */
  /* indices and the coefficients in a single vector instead of one   */
  /* allocation for each row or column                                */
  /* The resulting SparRow matrix is transparent for any function     */
  /* on SparRow matrix                                                */
  /********************************************************************/
  dim_t i, k;
  INTL nnz;


  if(mat->inarow == 1)
    return 1;

  nnz = 0;

  /*for(i=0;i<mat->n;i++)  */
  for(k=0;k<mat->nnzr;k++)
    {
      i = mat->nzrtab[k];
      nnz += mat->nnzrow[i];
    }

  if(nnz == 0)
    return 0;

  mat->jatab = (int *)malloc(sizeof(int)*nnz);
  assert(mat->jatab != NULL);
  mat->matab = (COEF *)malloc(sizeof(COEF)*nnz);
  assert(mat->matab != NULL);

  nnz = 0;
  for(k=0;k<mat->nnzr;k++)
    {
      i = mat->nzrtab[k];
      memcpy(mat->jatab+nnz, mat->ja[i], sizeof(int)*mat->nnzrow[i]);
      memcpy(mat->matab+nnz, mat->ma[i], sizeof(COEF)*mat->nnzrow[i]);
      free(mat->ja[i]);
      free(mat->ma[i]);
      mat->ja[i] = mat->jatab+nnz;
      mat->ma[i] = mat->matab+nnz;
      nnz += mat->nnzrow[i];
    }
  mat->inarow = 1;
  
  return 0;
}


int CSunrealloc(csptr mat)
{
  /********************************************************************/
  /* This function transformsa SparRow matrix that was realloc in     */
  /* a contiguous manner into a SparRow matrix where each row is      */
  /* allocated separatly: i.e. it undo "CSRealloc"                    */
  /********************************************************************/
  dim_t i, k;
  dim_t nnzrow;
  int *tmpj;
  COEF *tmpa;

  if(mat->inarow != 1)
    return 0;

  if(mat->nnzr == 0)
    {
      mat->inarow = 0;
      return 0;
    }

  nnzrow = 0;
  
  for(k=0;k<mat->nnzr;k++)
    {
      i = mat->nzrtab[k];
      nnzrow = mat->nnzrow[i];

      tmpj = (int *)malloc(sizeof(int)*nnzrow);
      memcpy(tmpj, mat->ja[i], sizeof(int)*nnzrow);
      mat->ja[i] = tmpj;

      tmpa = (COEF *)malloc(sizeof(COEF)*nnzrow);
      memcpy(tmpa, mat->ma[i], sizeof(COEF)*nnzrow);
      mat->ma[i] = tmpa;
    }
  if(mat->jatab != NULL)
    free(mat->jatab);
  mat->jatab = NULL;
  if(mat->matab != NULL)
    free(mat->matab);
  mat->matab = NULL;

  mat->inarow = 0;
  
  return 0;
}

int CSRcs_InaRow(dim_t n, COEF *a, dim_t *ja, INTL *ia, csptr bmat)
{
/*----------------------------------------------------------------------
| Convert CSR matrix to SparRow struct
| allocation of the SparRow entry is made with a single block a memory
| to avoid memory fragmentation
|----------------------------------------------------------------------
| on entry:
|==========
| a, ja, ia  = Matrix stored in CSR format (with FORTRAN indexing).
|
| On return:
|===========
|
| ( bmat )  =  Matrix stored as SparRow struct.
|
|       integer value returned:
|             0   --> successful return.
|             1   --> memory allocation error.
|--------------------------------------------------------------------*/
  int i, j, j1, len, st; 
  COEF *bra;
  int *bja;
  INTL nnz;

  /*				*/
  /* JE RETIRE CA CAR ON A BESOIN D'INITIALISER DES CS avec UNE TAILLE 
     SUPERIEUR A LA CSR DANS CERTAINS CAS 
     if(initCS(bmat, n)) 
     {
     printfv(5, " ERROR SETTING UP bmat IN initCS \n") ;
     exit(0);
     }*/
#ifdef DEBUG_M
  assert(bmat->n >= n);
#endif

  if(ia[n] == 0)
    {
      CS_SetNonZeroRow(bmat);
      return 0;
    }

  nnz = 0;
  bmat->inarow = 1;
  bmat->jatab = (dim_t *)malloc(sizeof(dim_t)*ia[n]);
  bmat->matab = (COEF *)malloc(sizeof(COEF)*ia[n]);
  if(bmat->jatab == NULL || bmat->matab == NULL)
    return 1;

  st = ia[0];
  for (j=0; j<n; j++) {
    len = ia[j+1] - ia[j];
    bmat->nnzrow[j] = len;
    if (len > 0) {
      /*bja = (int *) malloc(len*sizeof(int));
	bra = (COEF *) malloc(len*sizeof(COEF));*/
      bja = bmat->jatab+nnz;
      bra = bmat->matab+nnz;
      i = 0;
      for (j1=ia[j] - st; j1<ia[j+1] - st; j1++) {
	bja[i] = ja[j1] - st;
	bra[i] = a[j1] ;  
	i++;
      }
      bmat->ja[j] = bja;
      bmat->ma[j] = bra;
      nnz += len;
    }
  }
  
  for(j=n;j<bmat->n;j++)
    {
      bmat->nnzrow[j] = 0;
      bmat->ja[j] = NULL;
      bmat->ma[j] = NULL;
    }

  

  CS_SetNonZeroRow(bmat);
  return 0;
}



void CS_Copy(csptr amat, csptr bmat)
{
/*----------------------------------------------------------------------
| copy amat in SparRow struct to bmat in SparRow struct
|----------------------------------------------------------------------
| on entry:
|==========
| ( amat )   = Matrix stored in SparRow format
|
|
| On return:
|===========
|
| ( bmat )  =  Matrix stored as SparRow struct containing a copy
|              of amat 
|
|--------------------------------------------------------------------*/
   int i, j, k, len;
   COEF *bma;
   dim_t *bja;
#ifdef DEBUG_M
   assert(bmat->n == amat->n);
#endif
   

   if(amat->n == 0 || amat->nnzr == 0)
     return;

#ifdef DEBUG_M
   assert(amat->nzrtab != NULL);
   assert(amat->inzrtab != NULL);
#endif
   
   /** Copy the non zero row structure **/
   bmat->nnzr = amat->nnzr;
   memcpy(bmat->nzrtab, amat->nzrtab, amat->nnzr * sizeof(dim_t));
   memcpy(bmat->inzrtab, amat->inzrtab, amat->n * sizeof(dim_t));
   
   memcpy(bmat->nnzrow, amat->nnzrow,  amat->n*sizeof(dim_t));
   

   if(amat->inarow == 1)
     {
       len = CSnnz(amat);
       if(len>0)
	 {
	   bmat->jatab = (dim_t *)malloc(sizeof(dim_t)*len);
	   bmat->matab = (COEF *)malloc(sizeof(COEF)*len);
	   
	   len = 0;
	   for(k=0;k<amat->nnzr;k++)
	     {
	       i = amat->nzrtab[k];
	       memcpy(bmat->jatab+len, amat->ja[i], sizeof(dim_t)*amat->nnzrow[i]);
	       memcpy(bmat->matab+len, amat->ma[i], sizeof(COEF)*amat->nnzrow[i]);
	       bmat->ja[i] = bmat->jatab+len;
	       bmat->ma[i] = bmat->matab+len;
	       len += amat->nnzrow[i];
	     }
	   bmat->inarow = 1;   
	 }
       else
	 bmat->inarow = 0;
     }
   else
     for (k=0; k<amat->nnzr; k++) 
       {
	 j = amat->nzrtab[k];
	 len = amat->nnzrow[j];
	 bja = (dim_t *) malloc(len*sizeof(dim_t));
	 bma = (COEF *) malloc(len*sizeof(COEF));
	 memcpy(bja,amat->ja[j],len*sizeof(dim_t));
	 memcpy(bma,amat->ma[j],len*sizeof(COEF));
	 bmat->ja[j] = bja;
	 bmat->ma[j] = bma;
       }
}



void CS_Move(csptr amat, csptr bmat)
{
/*----------------------------------------------------------------------
| Move matrix amat into matrix bmax (pointer copy in bmat, reinit amat)
|----------------------------------------------------------------------
| on entry:
|==========
| ( amat )   = Matrix stored in SparRow format
|
|
| On return:
|===========
|
| ( bmat )  =  Matrix stored as SparRow struct containing a copy
|              of amat 
|--------------------------------------------------------------------*/

#ifdef DEBUG_M
   assert(bmat->n == amat->n);
   assert(bmat->nnzrow != NULL);
   assert(bmat->ja != NULL);
   assert(bmat->ma != NULL);
#endif

   if(bmat->n != amat->n)
     {
       fprintfd(stderr, "Error in CS_Move: amat->n = %ld bmat->n = %ld \n", (long)amat->n, (long)bmat->n);
       exit(-1);
     }
   
   if(amat->inarow == 1)
     {
       bmat->inarow = 1;
       bmat->jatab = amat->jatab;
       bmat->matab = amat->matab;
     }
   else
     bmat->inarow = 0;

   memcpy(bmat->nnzrow, amat->nnzrow, sizeof(dim_t)*amat->n);
   memcpy(bmat->ja, amat->ja, sizeof(dim_t *)*amat->n);
   memcpy(bmat->ma, amat->ma, sizeof(COEF *)*amat->n);
   
   
   amat->inarow = 0;
   amat->jatab = NULL;
   amat->matab = NULL;
   bzero(amat->nnzrow, sizeof(dim_t)*bmat->n);
   bzero(amat->ja, sizeof(dim_t *)* bmat->n);
   bzero(amat->ma, sizeof(COEF *)* bmat->n);
   
   /** Copy the non zero row structure **/
   bmat->nnzr = amat->nnzr;
   memcpy(bmat->nzrtab, amat->nzrtab, sizeof(dim_t) * amat->nnzr);
   memcpy(bmat->inzrtab, amat->inzrtab, sizeof(dim_t) * amat->n);
   amat->nnzr = 0;
   amat->inzrtab[0] = amat->n;

}


void CS_SymmetrizeTriang(flag_t job, char *UPLO, csptr mat)
{
  /************************************************/
  /* This function symmetrize a triangular matrix */
  /* in CSR format                                */
  /* If job = 0 : only the pattern is touched     */
  /*    job = 1 : A = A+At                        */
  /************************************************/
  dim_t i;
  csptr At;
  dim_t *tmpja;
  COEF *tmpma=NULL;
  int uplo;

  At = (csptr) malloc(sizeof(struct SparRow));
  initCS(At, mat->n);

  /** Transpose the matrix without the diagonal **/
  uplo = 0;
  
  if(strcmp(UPLO, "U") == 0)
    {
      uplo = 2;
      CS_GetUpper(job, "U", mat, At);
    }
  if(strcmp(UPLO, "L") == 0)
    {
      uplo = 1;
      CS_GetLower(job, "U", mat, At);
    }
#ifdef DEBUG_M
  assert(uplo != 0);
#endif
  CS_Transpose(job, At, mat->n);
  
  /** mat = mat + At ***/
  tmpja = (dim_t *)malloc(sizeof(dim_t)*mat->n);
  if(job == 1)
    tmpma = (COEF *)malloc(sizeof(COEF)*mat->n);
  for(i=0;i<mat->n;i++)
    {
      if(uplo == 1) /** mat is a lower triangular matrix **/
	{
	  if(mat->nnzrow[i] > 0)
	    {
	      memcpy(tmpja, mat->ja[i], sizeof(dim_t)*mat->nnzrow[i]);
	      if(job == 1)
		memcpy(tmpma, mat->ma[i], sizeof(COEF)*mat->nnzrow[i]);
	    }
	  if(At->nnzrow[i]>0)
	    {
	      memcpy(tmpja+mat->nnzrow[i], At->ja[i], sizeof(dim_t)*At->nnzrow[i]);
	      if(job == 1)
		memcpy(tmpma+mat->nnzrow[i], At->ma[i], sizeof(COEF)*At->nnzrow[i]);
	    }
	}
      else
	{ /** mat is a upper triangular matrix **/
	  if(At->nnzrow[i]>0)
	    {
	      memcpy(tmpja, At->ja[i], sizeof(dim_t)*At->nnzrow[i]);
	      if(job == 1)
		memcpy(tmpma, At->ma[i], sizeof(COEF)*At->nnzrow[i]);
	    }
	  if(mat->nnzrow[i] > 0)
	    {
	      memcpy(tmpja+At->nnzrow[i], mat->ja[i], sizeof(dim_t)*mat->nnzrow[i]);
	      if(job == 1)
		memcpy(tmpma+At->nnzrow[i], mat->ma[i], sizeof(COEF)*mat->nnzrow[i]);
	    }
	}
      
      mat->nnzrow[i] += At->nnzrow[i];
      if(mat->nnzrow[i]>0)
	{
	  mat->ja[i] = (dim_t *)realloc(mat->ja[i], sizeof(dim_t)*mat->nnzrow[i]);
	  memcpy(mat->ja[i], tmpja, sizeof(dim_t)*mat->nnzrow[i]);
	  if(job == 1)
	    {
	      mat->ma[i] = (COEF *)realloc(mat->ma[i], sizeof(COEF)*mat->nnzrow[i]);
	      memcpy(mat->ma[i], tmpma, sizeof(COEF)*mat->nnzrow[i]);
	    }
	}
    }
  free(tmpja);
  if(job == 1)
    free(tmpma);
  
  cleanCS(At);
  free(At);

  CS_SetNonZeroRow(mat);
#ifdef REALLOC_BLOCK
  if(job == 1)
    {
      if((REAL)mat->n*(REAL)mat->n <= (REAL)SMALLBLOCK)

	CSrealloc(mat);
    }
#endif
}


void CSR_GetUpper(flag_t numflag, dim_t n, INTL *ia, dim_t *ja, COEF *a)
{
  /** In place **/
  INTL i, j, ind;

  if(numflag != 0)
    CSR_Fnum2Cnum(ja, ia, n);
  
  ind = 0;
  for(i=0;i<n;i++)
    {
      ia[i] = ind;
      for(j=ia[i];j<ia[i+1];j++) {
	if(ja[j] >= i) {
	  assert(ind<=j);
	  ja[ind] = ja[j];
	  if(a != NULL)
	    a[ind]  = a[j];
	  ind++;
	}
      }
    }
  ia[n]=ind;
  
  if(numflag != 0)
    CSR_Cnum2Fnum(ja, ia, n);
      
}



void CS_Transpose(flag_t job, csptr mat, int coldim)
{
  /********************************************************/
  /* This function transpose a CSR matrix                 */
  /* ie: it converts a CSR into a CSC                     */
  /* mat : the matrix to transpose                        */
  /* coldim : the dimension of the matrix (column number) */
  /* job = 0 : only the pattern is transposed             */
  /*     = 1 : transpose terms, CSrealloc                 */
  /*     = 2 : transpose terms, no CSrealloc              */
  /********************************************************/
  dim_t i, j, k;
  dim_t *new_nnzrow;
  dim_t **new_ja;
  COEF **new_ma=NULL;
  int ind, colind;
  
  /*fprintfv(5, stderr, "TRANSPOSE mat %g \n", CSnormFrob(mat));*/

  if(coldim == 0)
    return;

  new_nnzrow = (dim_t *)malloc(sizeof(dim_t)*coldim);
  new_ja     = (dim_t **)malloc(sizeof(dim_t *)*coldim);
  if(job >= 1)
    new_ma     = (COEF **)malloc(sizeof(COEF *)*coldim);
  bzero(new_nnzrow, sizeof(dim_t)*coldim);
  
  /** Count the number of entries in each column **/
  /*for(i=0;i<mat->n;i++)*/
  for(k=0;k<mat->nnzr;k++)
    {
      i = mat->nzrtab[k];
      for(j=0;j<mat->nnzrow[i];j++)
	{
#ifdef DEBUG_M
	  assert(mat->ja[i][j] < coldim);
#endif
	  new_nnzrow[mat->ja[i][j]]++;
	}
    }
    
  /** Alloc the column **/
  for(i=0;i<coldim;i++)
    if(new_nnzrow[i] > 0)
      {
	new_ja[i] = (dim_t *)malloc(sizeof(dim_t)*new_nnzrow[i]);
	if(job >= 1)
	  new_ma[i] = (COEF *)malloc(sizeof(COEF)*new_nnzrow[i]);
      }
    else
      {
	new_ja[i] = NULL;
	if(job >= 1)
	  new_ma[i] = NULL;
      }
  
  /** Fill the transpose matrix **/
  bzero(new_nnzrow, sizeof(dim_t)*coldim);
  /*for(i=0;i<mat->n;i++)*/
  for(k=0;k<mat->nnzr;k++)
    {
      i = mat->nzrtab[k];
      for(j=0;j<mat->nnzrow[i];j++)
	{
	  colind = mat->ja[i][j];
	  ind = new_nnzrow[colind];
	  new_ja[colind][ind] =  i;
	  if(job >= 1)
	    new_ma[colind][ind] =  mat->ma[i][j];
	  new_nnzrow[colind]++;
	}
    }
  
  reinitCS(mat);

  /*FIX*/
  if (coldim > mat->n) { /* ou pas */
    free(mat->nzrtab);
    free(mat->inzrtab);
    mat->nzrtab  = (dim_t*)malloc(sizeof(dim_t)*coldim);
    mat->inzrtab = (dim_t*)malloc(sizeof(dim_t)*coldim);

    bzero(mat->inzrtab, sizeof(dim_t) * mat->nnzr);
    mat->nnzr = 0;
    mat->nzrtab[0] = mat->n;
  }
  /*FIX*/
  
  mat->n = coldim;
  if(mat->nnzrow != NULL)
    free(mat->nnzrow);
  mat->nnzrow = new_nnzrow;

  if(mat->ja != NULL)
    {
      free(mat->ja);
      if(job >= 1)
	free(mat->ma);
    }

  mat->ja = new_ja;
  if(job >= 1)
    mat->ma = new_ma;




  CS_SetNonZeroRow(mat);


#ifdef REALLOC_BLOCK  
  if(job == 1)
    {

      if((REAL)mat->n*(REAL)coldim <= (REAL)SMALLBLOCK)

	CSrealloc(mat);
    }
#endif

}

REAL CSC_LDLt_countops_rec(csptr mat, int fcol, int lcol)
{
  REAL opc;
  opc = 0.0;

#ifdef DEBUG_M
  assert(lcol < mat->n);
  assert(fcol <= lcol);
#endif
  if(lcol - fcol < 50)
    {
      int i, nnz;
      for(i=fcol; i<= lcol;i++)
	{
	  nnz = mat->nnzrow[i];
	  opc += (nnz *(nnz+1))/2.0;
	}
    }
  else
    {
      int mcol;
      mcol = (fcol+lcol)/2;
      opc += CSC_LDLt_countops_rec(mat, fcol, mcol);
      opc += CSC_LDLt_countops_rec(mat, mcol+1, lcol);
    }
  
  return opc;
} 

REAL CSC_LDLt_OPC(csptr mat)
{
  return CSC_LDLt_countops_rec(mat, 0, mat->n - 1);
}


REAL norm2(COEF *x, dim_t n)
{
  REAL nm;
  blas_t incx = 1;
  nm = (REAL) BLAS_NRM2(n, x, incx);
  return nm;
}


void CSR_RowPerm(dim_t n, INTL *ia, dim_t *ja, COEF *a, dim_t *perm)
{
  /*----------------------------------------------------------------------
    |
    | This subroutine permutes the rows of a matrix in CSR format. 
    | rperm  computes B = P A  where P is a permutation matrix.  
    | The permutation P is defined through the array perm: for each j, 
    | perm[j] represents the destination row number of row number j. 
    | NOTE : matrix must be in C numbering 
    |        if a is NULL then it is ignored
    |-----------------------------------------------------------------------*/
  INTL i, k;
  INTL *oldia;
  dim_t *oldja;
  COEF *olda=NULL; 
  if(n==0 || ia[n] == 0)
    return;

#ifdef DEBUG_M
  assert(ia[0] == 0);
#endif
  oldia = (INTL *)malloc(sizeof(INTL)*(n+1));
  memcpy(oldia, ia, sizeof(INTL)*(n+1));
  oldja = (dim_t *)malloc(sizeof(dim_t)* ia[n]);
  memcpy(oldja, ja, sizeof(dim_t)*ia[n]);
  if(a != NULL)
    {
      olda = (COEF *)malloc(sizeof(COEF)* ia[n]);
      memcpy(olda, a, sizeof(COEF)*ia[n]);
    }

  ia[0] = 0;
  for(i=0;i<n;i++)
    ia[perm[i]+1] = oldia[i+1]-oldia[i];
  for(i=1;i<=n;i++)
    ia[i] += ia[i-1];
#ifdef DEBUG_M
  assert(ia[n] == oldia[n]);
#endif

  for(i=0;i<n;i++)
    {
      k = perm[i];
#ifdef DEBUG_M
      assert(oldia[i+1]-oldia[i]>= 0);
#endif
      if(oldia[i+1]-oldia[i]>0)
	{
	  memcpy(ja+ia[k], oldja + oldia[i], sizeof(dim_t)*(oldia[i+1]-oldia[i]));
	  if(a!=NULL)
	    memcpy(a+ia[k], olda + oldia[i], sizeof(COEF)*(oldia[i+1]-oldia[i]));
	}
    }
  
  free(oldia);
  free(oldja);
  if(a!=NULL)
    free(olda);
}

void CSR_ColPerm(dim_t n, INTL *ia, dim_t *ja, COEF *a, dim_t *perm)
{
  INTL i;
  if(n==0 || ia[n] == 0)
    return;
  
  for(i=ia[0];i<ia[n];i++)
    ja[i] = perm[ja[i]];
}


void CSR_Perm(dim_t n, INTL *ia, dim_t *ja, COEF *a, dim_t *perm)
{
  CSR_RowPerm(n, ia, ja, a, perm);
  CSR_ColPerm(n, ia, ja, a, perm);
}



int CS_RowPerm(csptr mat, dim_t *perm)
{
/*----------------------------------------------------------------------
|
| This subroutine permutes the rows of a matrix in SparRow format. 
| rperm  computes B = P A  where P is a permutation matrix.  
| The permutation P is defined through the array perm: for each j, 
| perm[j] represents the destination row number of row number j. 
|
|-----------------------------------------------------------------------
| on entry:
|----------
| (amat) = a matrix stored in SparRow format.
|
|
| on return:
| ----------
| (amat) = P A stored in SparRow format.
|
| integer value returned:
|             0   --> successful return.
|             1   --> memory allocation error.
|---------------------------------------------------------------------*/
  dim_t **addj, *nnz, i, size;
  COEF **addm;

   size = mat->n;
   if(size == 0)
     return 0;

   addj = (dim_t **) malloc(size*sizeof(dim_t *));
   addm = (COEF **) malloc(size*sizeof(COEF *));
   nnz = (dim_t *) malloc(size*sizeof(dim_t));
   if ( (addj == NULL) || (addm == NULL) || (nnz == NULL) ) 
     return 1;

   /** Need this !! */
   bzero(nnz, sizeof(dim_t)*size);

   for (i=0; i<size; i++) 
     {
       addj[perm[i]] = mat->ja[i];
       addm[perm[i]] = mat->ma[i];
       nnz[perm[i]] = mat->nnzrow[i];
     }
   for (i=0; i<size; i++) 
     {
       mat->ja[i] = addj[i];
       mat->ma[i] = addm[i];
       mat->nnzrow[i] = nnz[i];
     }
   free(addj);
   free(addm);
   free(nnz);


   CS_SetNonZeroRow(mat);

#ifdef DEBUG_M
   CS_Check(mat, mat->n);
#endif

   return 0;
}
/*------- end of rperm ------------------------------------------------- 
|---------------------------------------------------------------------*/

int CS_ColPerm(csptr mat, dim_t *perm) 
{
/*----------------------------------------------------------------------
|
| This subroutine permutes the columns of a matrix in SparRow format.
| cperm computes B = A P, where P is a permutation matrix.
| that maps column j into column perm(j), i.e., on return 
| The permutation P is defined through the array perm: for each j, 
| perm[j] represents the destination column number of column number j. 
|
|-----------------------------------------------------------------------
| on entry:
|----------
| (mat) = a matrix stored in SparRow format.
|
|
| on return:
| ----------
| (mat) = A P stored in SparRow format.
|
| integer value returned:
|             0   --> successful return.
|             1   --> memory allocation error.
|---------------------------------------------------------------------*/
   int i, j, k, *aja;
   if(mat->n == 0)
     return 0;
   
   for(k=0;k<mat->nnzr;k++)
     {
       i = mat->nzrtab[k];
       aja = mat->ja[i];
       for (j=0; j<mat->nnzrow[i]; j++)
	 aja[j] = perm[aja[j]];
     }
   /** NO NEED TO RECOMPUTE NON ZERO ROW STRUCTURE */

#ifdef DEBUG_M
   CS_Check(mat, mat->n);
#endif

   return 0;
}



/*------- end of cperm ------------------------------------------------- 
|---------------------------------------------------------------------*/
int CS_Perm(csptr mat, dim_t *perm) 
{
/*----------------------------------------------------------------------
|
| This subroutine permutes the rows and columns of a matrix in 
| SparRow format.  dperm computes B = P^T A P, where P is a permutation 
| matrix.
|
|-----------------------------------------------------------------------
| on entry:
|----------
| (amat) = a matrix stored in SparRow format.
|
|
| on return:
| ----------
| (amat) = P^T A P stored in SparRow format.
|
| integer value returned:
|             0   --> successful return.
|             1   --> memory allocation error.
|---------------------------------------------------------------------*/
  if (CS_RowPerm(mat, perm))/** NON ZERO ROW STRUCTURE IS UPDATED HERE **/
    return 1; 
  if (CS_ColPerm(mat, perm)) 
    return 1;
  return 0;
}



void CS_Reorder(csptr m, dim_t *permtab, dim_t *ipermtab)
{
  dim_t i, j;
  INTL nnz;
  INTL *ia;
  dim_t *ja, n; /* To store the sub matrix of the interior node */
 
  nnz = CSnnz(m);

  if(nnz == 0) /* We never know that could happen ! */
    {
      for(i=0;i<m->n;i++)
	{
	  permtab[i] = i;
	  ipermtab[i] = i;
	}
      return; 
    }

  /** Alocate the csr matrix **/
  n = m->n;
  ia = (INTL *)malloc(sizeof(INTL)*(n+1)); 
  ja = (dim_t *)malloc(sizeof(dim_t)*nnz);
  
  nnz = 0;
  for(i=0;i<m->n;i++)
    {
      ia[i] = nnz;
      for(j=0;j< m->nnzrow[i];j++)
	if(m->ja[i][j] != i) /* We do not store the diagonale (if stored METIS crash !) */
	  ja[nnz++] = m->ja[i][j];
    }
  ia[n] = nnz;
  

#ifdef SCOTCH_PART
  {
    dim_t cblknbr;
    int * rangtab; /* Size n+1 */
    dim_t *treetab;
    SCOTCH_Graph        grafdat;
    SCOTCH_Strat        stratdat;
    char STRAT[400];
    
    SCOTCH_stratInit (&stratdat);
    
    
    /** This strategy includes the ordering of the leaves i.e. the interior nodes **/
    sprintf(STRAT, "n{sep=/(vert>120)?m{vert=100,low=h{pass=10},asc=f{bal=0.2}}|m{vert=100,low=h{pass=10},asc=f{bal=0.1}};,ole=f{cmin=0,cmax=10000,frat=0.00},ose=g}");
    
    
    SCOTCH_stratGraphOrder (&stratdat, STRAT);
    
    SCOTCH_graphInit  (&grafdat);
    /*SCOTCH_graphBuild (&grafdat, 0, n, ia, NULL, NULL, NULL, ia[n], ja, NULL);*/
    SCOTCH_graphBuild_WRAPPER (&grafdat, 0, n, ia, NULL, ja, NULL);
    
    /*  fprintfv(5, stderr, "NDOM %ld \n", (long)ndom);*/
    /** Do not free verttab and edgetab before SCOTCH_graphOrder **/
    rangtab = (int *)malloc(sizeof(int)*(n+1));
    treetab = (int *)malloc(sizeof(int)*(n+1));
    /*SCOTCH_graphOrder (&grafdat, &stratdat, permtab, ipermtab, &cblknbr, NULL, NULL);*/
    SCOTCH_graphOrder_WRAPPER (&grafdat, &stratdat, n, permtab, ipermtab, &cblknbr, NULL, NULL);
    free(rangtab);
    free(treetab);

    SCOTCH_graphExit (&grafdat);
    SCOTCH_stratExit (&stratdat);
  }
#else
  {
    /*** Compute the reordering to minimize fill-in using METIS-4.0 ***/
    int options[10];
    dim_t numflag;
    
    options[0] = 0;
    numflag = 0;
    
    /*METIS_NodeND(&n, ia, ja, &numflag, options, ipermtab, permtab);*/
    METIS_NodeND_WRAPPER(n, ia, ja, numflag, options, ipermtab, permtab);
  }
#endif  

  free(ia);
  free(ja);
  /*** permtab[i] is the new rank of row i in the permuted matrix ***/
  /*** unknown i in the permuted matrix was perm[i] in the unpermuted matrix ***/
  /*dpermC(m, permtab);*/
  CS_Perm(m, permtab); /** NON ZERO ROW STRUCTURE IS UPDATED HERE **/

}




void CS_VirtualMerge(dim_t nb, csptr *mattab, csptr amat)
{
  /*******************************************************/
  /* This function merges several CS  matrices           */  
  /* into a single one                                   */
  /* NOTE the returned matrix is virtual i.e.            */
  /* it is a pointer to rows in the mattab matrices      */
  /*******************************************************/

  csptr m;
  int i, ind;
  dim_t n;
  
  n = 0;
  for(i=0;i<nb;i++)
    n += mattab[i]->n;
  
  initCS(amat, n);
  
  ind = 0;
  for(i=0;i<nb;i++)
    {
      m = mattab[i];
      /*for(k = 0;k < m->n;k++)
	{
	  amat->nnzrow[ind] = m->nnzrow[k];
	  amat->ja[ind] = m->ja[k];
	  amat->ma[ind] = m->ma[k];
	  ind++;
	  }*/
      memcpy(amat->nnzrow+ind, m->nnzrow, sizeof(dim_t)*m->n);
      memcpy(amat->ja+ind, m->ja, sizeof(dim_t *)*m->n);
      memcpy(amat->ma+ind, m->ma, sizeof(COEF *)*m->n);
      ind += m->n;
    }
  
  CS_SetNonZeroRow(amat);
}  


void CS_VirtualSplit(csptr amat, dim_t nb, csptr *mattab)
{
  /*******************************************************/
  /* This function is the inverse of CS_VirtualMerge     */
  /*******************************************************/
  csptr m;
  int i, ind;
  
  ind = 0;
  for(i=0;i<nb;i++)
    {
      m = mattab[i];
      memcpy(m->nnzrow, amat->nnzrow+ind, sizeof(dim_t)*m->n);
      memcpy(m->ja, amat->ja+ind, sizeof(dim_t *)*m->n);
      memcpy(m->ma, amat->ma+ind, sizeof(COEF *)*m->n);
      ind += m->n;
      CS_SetNonZeroRow(m);
    }
}




void CS_DropT(csptr amat, REAL droptol, REAL *droptab)
{
  dim_t i, j, k;
  int len;
  dim_t nnzj;
  INTL nnz;
  dim_t *ja, *newja;
  COEF *ma, *newma;
  REAL t;


  if(amat->inarow == 1)
    {
      nnz = 0;
      for(k=0;k<amat->nnzr;k++)
	{
	  i = amat->nzrtab[k];
	  nnzj = amat->nnzrow[i];
	  ja = amat->ja[i];
	  ma = amat->ma[i];
      

	  if(droptab != NULL)
	    t = droptol * droptab[i];
	  else
	    t = droptol;
      
	  newja = amat->jatab + nnz;
	  newma = amat->matab + nnz;
	  len = 0;
	  for(j=0;j<nnzj;j++)
	    if(coefabs(ma[j])>t)
	      {
		newma[len] = ma[j];
		newja[len] = ja[j];
		len++;
	      }
      
	  amat->nnzrow[i] = len;
	  nnz += len;
	}


      if(amat->jatab != NULL)
	amat->jatab = (dim_t *)realloc(amat->jatab, sizeof(dim_t)*nnz);
      else
	if(nnz>0)
	  amat->jatab = (dim_t *)malloc(sizeof(dim_t)*nnz);
      
      if(amat->matab != NULL)
	amat->matab = (COEF *)realloc(amat->matab, sizeof(COEF)*nnz);
      else
	if(nnz>0)
	  amat->matab = (COEF *)malloc(sizeof(COEF)*nnz);
    
      nnz = 0;
      for(k=0;k<amat->nnzr;k++)
	{
	  i = amat->nzrtab[k];
	  amat->ja[i] = amat->jatab + nnz;
	  amat->ma[i] = amat->matab + nnz;
	  nnz += amat->nnzrow[i];
	}

    }
  else
    {

      for(k=0;k<amat->nnzr;k++)
	{
	  i = amat->nzrtab[k];
	  nnzj = amat->nnzrow[i];
	  ja = amat->ja[i];
	  ma = amat->ma[i];
      
	  len = 0;
	  if(droptab != NULL)
	    t = droptol * droptab[i];
	  else
	    t = droptol;
      
	  for(j=0;j<nnzj;j++)
	    if(coefabs(ma[j])>t)
	      {
		ma[len] = ma[j];
		ja[len] = ja[j];
		len++;
	      }

	  if(len > 0)
	    {
	      amat->ja[i] = (dim_t *)realloc(amat->ja[i], sizeof(dim_t)*len);
	      amat->ma[i] = (COEF *)realloc(amat->ma[i], sizeof(COEF)*len);
	    }
	  else
	    {
	      if(amat->ja[i] != NULL)
		free(amat->ja[i]);
	      if(amat->ma[i] != NULL)
		free(amat->ma[i]);
	    }
	  amat->nnzrow[i] = len;
	}


    }
  CS_SetNonZeroRow(amat);

}


void CS_DropT_SaveDiag(csptr amat, REAL droptol, REAL *droptab)
{
  dim_t i, j, k;
  int len;
  dim_t nnzj;
  INTL nnz;
  dim_t *ja, *newja;
  COEF *ma, *newma;
  REAL t;

  if(amat->inarow == 1)
    {
      nnz = 0;
      for(k=0;k<amat->nnzr;k++)
	{
	  i = amat->nzrtab[k];
	  nnzj = amat->nnzrow[i];
	  ja = amat->ja[i];
	  ma = amat->ma[i];
      
      
	  if(droptab != NULL)
	    t = droptol * droptab[i];
	  else
	    t = droptol;
      
	  newja = amat->jatab + nnz;
	  newma = amat->matab + nnz;
	  len = 0;
	  for(j=0;j<nnzj;j++)
	    if(coefabs(ma[j])>t  || ja[j] == i)
	      {
		newma[len] = ma[j];
		newja[len] = ja[j];
		len++;
	      }
      
	  amat->nnzrow[i] = len;
	  nnz += len;
	}


      amat->jatab = (dim_t *)realloc(amat->jatab, sizeof(dim_t)*nnz);
      amat->matab = (COEF *)realloc(amat->matab, sizeof(COEF)*nnz);
  
      nnz = 0;
      for(k=0;k<amat->nnzr;k++)
	{
	  i = amat->nzrtab[k];
	  amat->ja[i] = amat->jatab + nnz;
	  amat->ma[i] = amat->matab + nnz;
	  nnz += amat->nnzrow[i];
	}
    }
  else
    for(k=0;k<amat->nnzr;k++)
      {
	i = amat->nzrtab[k];
	nnzj = amat->nnzrow[i];
	ja = amat->ja[i];
	ma = amat->ma[i];
      
    
	len = 0;
	if(droptab != NULL)
	  t = droptol * droptab[i];
	else
	  t = droptol;

	for(j=0;j<nnzj;j++)
	  {
	    if(coefabs(ma[j])>t || ja[j] == i)
	      {
		ma[len] = ma[j];
		ja[len] = ja[j];
		len++;
	      }
	  }
	if(len > 0)
	  {
	    amat->ja[i] = (dim_t *)realloc(amat->ja[i], sizeof(dim_t)*len);
	    amat->ma[i] = (COEF *)realloc(amat->ma[i], sizeof(COEF)*len);
	  }
	else
	  {
	    if(amat->ja[i] != NULL)
	      free(amat->ja[i]);
	    if(amat->ma[i] != NULL)
	      free(amat->ma[i]);
	  }
	amat->nnzrow[i] = len;
      }


  CS_SetNonZeroRow(amat);



}

void CS_RowMult(COEF *vec, csptr m)
{
  dim_t i, j, k;
  COEF d;
  COEF *maptr;

  /*for(i=0;i<m->n;i++)*/
  for(j=0;j<m->nnzr;j++)
    {
      i = m->nzrtab[j];

#ifdef DEBUG_M
#ifdef TYPE_REAL
      assert(coefabs(vec[i])>0);
#endif /*TODO*/
#endif
      d = vec[i];
      maptr = m->ma[i];
      for(k=0;k<m->nnzrow[i];k++)
	maptr[k] *= d;
    }
}

void CS_RowMult2(REAL *vec, csptr m)
{
  dim_t i, j, k;
  REAL d;
  COEF *maptr;

  /*for(i=0;i<m->n;i++)*/
  for(j=0;j<m->nnzr;j++)
    {
      i = m->nzrtab[j];

#ifdef DEBUG_M
#ifdef TYPE_REAL
      assert(coefabs(vec[i])>0);
#endif /*TODO*/
#endif
      d = vec[i];
      maptr = m->ma[i];
      for(k=0;k<m->nnzrow[i];k++)
	maptr[k] *= d;
    }
}

void CS_ColMult(COEF *vec, csptr m)
{
  dim_t i, j, k;
  dim_t *japtr;
  COEF *maptr;

  /*for(i=0;i<m->n;i++)*/
  for(j=0;j<m->nnzr;j++)
    {
      i = m->nzrtab[j];
      japtr = m->ja[i];
      maptr = m->ma[i];
      for(k=0;k<m->nnzrow[i];k++)
	{

#ifdef DEBUG_M
#ifdef TYPE_REAL
        assert(vec[japtr[k]]>0);
#endif /*TODO*/
#endif
	  maptr[k] *= vec[japtr[k]];
	}
    }		     
}


void CS_ColMult2(REAL *vec, csptr m)
{
  dim_t i, j, k;
  dim_t *japtr;
  COEF *maptr;

  /*for(i=0;i<m->n;i++)*/
  for(j=0;j<m->nnzr;j++)
    {
      i = m->nzrtab[j];
      japtr = m->ja[i];
      maptr = m->ma[i];
      for(k=0;k<m->nnzrow[i];k++)
	{

#ifdef DEBUG_M
#ifdef TYPE_REAL
        assert(vec[japtr[k]]>0);
#endif /*TODO*/
#endif
	  maptr[k] *= vec[japtr[k]];
	}
    }		     
}




void CS_SetNonZeroRow(csptr m)
{
  dim_t i, j;
  int last;

  if(m->n == 0)
    return;

#ifdef DEBUG_M
  assert(m->nzrtab != NULL);
  assert(m->inzrtab != NULL);
#endif

  m->nnzr = 0;
  last = 0;
  for(i=0;i<m->n;i++)
    if(m->nnzrow[i] > 0)
      {
	for(j=last;j<=i;j++)
	  m->inzrtab[j] = m->nnzr;
	m->nzrtab[m->nnzr] = i;
	m->nnzr++;
	last = i+1;
      }

  for(j=last;j<m->n;j++)
    m->inzrtab[j] = m->n;
    
}


void CS_MatricesAdd(csptr a, int matnbr, csptr mattab, dim_t *tmpj, COEF *tmpa, dim_t *jrev)
{
  /*****************************************************************/
  /* Add several matrices in a SparRow matrix                      */
  /* On entry:                                                     */
  /*       a: the matrix that receives contributions               */
  /*    matnbr: number of matrices to add in matrix a              */
  /*  mattab : an array of pointer to the matrices to add          */
  /* tmpj : temporary vector that is at least of dim = nbcol of a  */
  /* tmpa: idem                                                    */
  /* jrev :idem                                                    */
  /* NOTE : jrev HAS TO BE INITIALIZED TO -1 BY THE USER (since    */
  /* the maximum size of a row is not available at this stage      */
  /*****************************************************************/

  int i, j, k, jpos;
  dim_t *ja;
  COEF *ma;
  dim_t nrow;
  csptr t;

  for(i=0;i<a->n;i++)
    {
      /** Unpack row i of matrix a **/
      ja = a->ja[i];
      ma = a->ma[i];
      nrow = 0;
      for(j=0;j<a->nnzrow[i];j++)
	jrev[ja[j]] = nrow++;
      memcpy(tmpj, ja, sizeof(dim_t)*nrow);
      memcpy(tmpa, ma, sizeof(COEF)*nrow);
	  
      /** Add row i of each contribution matrices **/
      for(k=0;k<matnbr;k++)
	{
	  t = mattab+k;
	  if(t->nnzrow[i] == 0)
	    continue;

	  ja = t->ja[i];
	  ma = t->ma[i];
	  for(j=0;j<t->nnzrow[i];j++)
	    {
	      jpos = jrev[ja[j]];
	      if(jpos < 0)
		{
		  tmpj[nrow] = ja[j];
		  tmpa[nrow] = ma[j];
		  jrev[ja[j]] = nrow++;
		}
	      else
		tmpa[jpos] += ma[j];
	    }
	}
      if(nrow == 0)
	continue;

      /** Reset jrev **/
      for(j=0;j<nrow;j++)
	jrev[tmpj[j]] = -1;

      /** realloc row i of matrix a **/
      if(nrow != a->nnzrow[i])
	{
	  if(a->nnzrow[i] > 0)
	    {
	      free(a->ja[i]);
	      free(a->ma[i]);
	    }
	  a->ja[i] = (dim_t *)malloc(sizeof(dim_t)*nrow);
	  a->ma[i] = (COEF *)malloc(sizeof(COEF)*nrow);
	  a->nnzrow[i] = nrow;
	}
      
      memcpy(a->ja[i], tmpj, sizeof(dim_t)*nrow);
      memcpy(a->ma[i], tmpa, sizeof(COEF)*nrow);

    }
  
  /** Recompute the set of non zero rows in a **/ 
  CS_SetNonZeroRow(a);
}



void CS_MatricesGather(csptr a, int matnbr, csptr mattab, dim_t *tmpj, COEF *tmpa, dim_t *jrev)
{
  /*****************************************************************/
  /* Merge several matrices in a single one: if several entryies   */
  /* a(i,j) in the contribution have not the same value then the   */
  /* one with the maximum modulus is retained                      */
  /* On entry:                                                     */
  /*       a: the matrix that receives contributions               */
  /*    matnbr: number of matrices to add in matrix a              */
  /*  mattab : an array of pointer to the matrices to add          */
  /* tmpj : temporary vector that is at least of dim = nbcol of a  */
  /* tmpa: idem                                                    */
  /* jrev :idem                                                    */
  /* NOTE : jrev HAS TO BE INITIALIZED TO -1 BY THE USER (since    */
  /* the maximum size of a row is not available at this stage      */
  /*****************************************************************/

  int i, j, k, jpos;
  dim_t *ja;
  COEF *ma;
  dim_t nrow;
  csptr t;

  for(i=0;i<a->n;i++)
    {
      /** Unpack row i of matrix a **/
      ja = a->ja[i];
      ma = a->ma[i];
      nrow = 0;
      for(j=0;j<a->nnzrow[i];j++)
	jrev[ja[j]] = nrow++;
      memcpy(tmpj, ja, sizeof(dim_t)*nrow);
      memcpy(tmpa, ma, sizeof(COEF)*nrow);
	  
      /** Add row i of each contribution matrices **/
      for(k=0;k<matnbr;k++)
	{
	  t = mattab+k;
	  if(t->nnzrow[i] == 0)
	    continue;

	  ja = t->ja[i];
	  ma = t->ma[i];
	  for(j=0;j<t->nnzrow[i];j++)
	    {
	      jpos = jrev[ja[j]];
	      if(jpos < 0)
		{
		  tmpj[nrow] = ja[j];
		  tmpa[nrow] = ma[j];
		  jrev[ja[j]] = nrow++;
		}
	      else
		{
		  if(coefabs(tmpa[jpos]) < coefabs(ma[j]))
		    tmpa[jpos] = ma[j];
		  
		  /*		  if(ma[j] != 0)
		    {
		      if(coefabs(tmpa[jpos]-ma[j])/ coefabs(ma[j]) > 1e-9)
			{
			  fprintfd(stderr, "tmpj[%ld] = %ld, ja[%ld] = %ld \n", (long)jpos, (long)tmpj[jpos], (long)j, (long)ja[j]);
			  fprintfd(stderr, "tmpa[%ld] = "_coef_", ma[%ld] = "_coef_" \n", (long)jpos, pcoef(tmpa[jpos]), (long)j, pcoef(ma[j]));
			  assert(tmpa[jpos] == ma[j]);
			}
		    }
		  else
		  assert(tmpa[jpos] == ma[j]);*/
		  
		}
	    }
	}
      if(nrow == 0)
	continue;

      /** Reset jrev **/
      for(j=0;j<nrow;j++)
	jrev[tmpj[j]] = -1;

      /** realloc row i of matrix a **/
      if(nrow != a->nnzrow[i])
	{
	  if(a->nnzrow[i] > 0)
	    {
	      free(a->ja[i]);
	      free(a->ma[i]);
	    }
	  a->ja[i] = (dim_t *)malloc(sizeof(dim_t)*nrow);
	  a->ma[i] = (COEF *)malloc(sizeof(COEF)*nrow);
	  a->nnzrow[i] = nrow;
	}
      
      memcpy(a->ja[i], tmpj, sizeof(dim_t)*nrow);
      memcpy(a->ma[i], tmpa, sizeof(COEF)*nrow);

    }
  
  /** Recompute the set of non zero rows in a **/ 
  CS_SetNonZeroRow(a);
}


void CSR_ComplementSubmatrix(dim_t nn, dim_t *nodelist, dim_t n, INTL *ia, dim_t *ja, COEF *ma,
		   INTL **sia, dim_t **sja, COEF **sma)
{
  /*********************************************************************/
  /* This function the submatrix from a CSR/CSC matrix  wich entries   */
  /* are not in                                                        */
  /* M([nodelisr],[nodelist])                                          */
  /* Note the CSR matrix must be in C numbering                        */
  /*********************************************************************/
  INTL i, j, k, ind;
  flag_t *isnodelist;

  isnodelist = (flag_t *)malloc(sizeof(flag_t)*n);

  bzero(isnodelist, sizeof(flag_t)*n);
  for(i=0;i<nn;i++)
    isnodelist[nodelist[i]] = 1;

#ifdef DEBUG_M
  assert(ia[0] == 0);
#endif

  /** Compute the number of non zeros in the submatrix **/
  ind = 0;
  for(k=0;k<nn;k++)
    {
      i = nodelist[k];
      for(j=ia[i];j<ia[i+1];j++)
	if(isnodelist[ja[j]] == 1)
	  ind++;
    }
  ind = ia[n]-ind;

  if(ind > 0)
    {
      *sja = (dim_t *)malloc(sizeof(dim_t)*ind);
      if(ma != NULL)
	*sma = (COEF *)malloc(sizeof(COEF)*ind);
    }
  else
    {
      *sja = NULL;
      if(ma != NULL)
	*sma = NULL;
    }

  *sia = (INTL *)malloc(sizeof(INTL)*(n+1));

  ind = 0;
  for(i=0;i<n;i++)
    {
      (*sia)[i] = ind;
      if(isnodelist[i] == 1)
	{
	  for(j=ia[i];j<ia[i+1];j++)
	    {
	      if(isnodelist[ja[j]] == 0)
		{
		  (*sja)[ind] = ja[j];
		  if(ma != NULL)
		    (*sma)[ind] = ma[j];
		  ind++;
		}
	    }
	}
      else
	{
	  memcpy(*sja + ind, ja + ia[i], sizeof(dim_t)*(ia[i+1]-ia[i]));
	  if(ma != NULL)
	    memcpy(*sma + ind, ma + ia[i], sizeof(COEF)*(ia[i+1]-ia[i]));
	  ind += ia[i+1]-ia[i];
	}
    }
  (*sia)[n] = ind;
  free(isnodelist);


#ifdef DEBUG_M
  if(ma == NULL)
    checkCSR(n, *sia, *sja, NULL, 0);
  else
    checkCSR(n, *sia, *sja, *sma, 0);
#endif

}

void CSR_GetSquareSubmatrix(dim_t nn, int *nodelist, dim_t n, INTL *ia, dim_t *ja, COEF *ma,
		   INTL **sia, dim_t **sja, COEF **sma)
{
  /*********************************************************************/
  /* This function extracts a square submatrix from a CSR/CSC matrix   */
  /* the submatrix is of dimension nn and consist of the submatrix     */
  /* M([nodelisr],[nodelist])                                          */
  /* Note the CSR matrix must be in C numbering                        */
  /*********************************************************************/
  INTL i, j, k, ind, num;
  dim_t *newnum;

#ifdef DEBUG_M
  assert(nn>0);
  assert(nn<=n);

  for(i=0;i<nn;i++)
    assert(nodelist[i] >= 0 && nodelist[i] < n);

  checkCSR(n, ia, ja, ma, 0);
#endif

  /** Compute the new numbering of unknown in the 
      submatrix (-1 if not in the submatrix) **/
  newnum = (dim_t *)malloc(sizeof(dim_t)*n);
  for(i=0;i<n;i++)
    newnum[i] = -1;
  for(i=0;i<nn;i++)
    newnum[nodelist[i]] = i;


  /** Compute the maximum number of non zeros in the submatrix **/
  ind = 0;
  for(k=0;k<nn;k++)
    {
      i = nodelist[k];
      ind += ia[i+1]-ia[i];
    }
  if(ind>0)
    *sja = (dim_t *)malloc(sizeof(dim_t)*ind);
  else
    *sja = NULL;

  if(ma != NULL) {
    if(ind>0)
      *sma = (COEF *)malloc(sizeof(COEF)*ind);
    else
      *sma = NULL;
  }

  *sia = (INTL *)malloc(sizeof(INTL)*(nn+1));
  ind = 0;
  for(k=0;k<nn;k++)
    {
      i = nodelist[k];
      (*sia)[k] = ind;
      for(j=ia[i];j<ia[i+1];j++)
	{
	  num = newnum[ja[j]];
	  if(num >= 0)
	    {
	      (*sja)[ind] = num;
	      if(ma != NULL)
		(*sma)[ind] = ma[j];
	      ind++;
	    }
	}
    }
  (*sia)[nn] = ind;

  /** Realloc the vector to fit the size **/
  
  (*sja) = (dim_t *)realloc(*sja, sizeof(dim_t)*ind);
  if(ma != NULL)
    (*sma) = (COEF *)realloc(*sma, sizeof(COEF)*ind);

  free(newnum);

#ifdef DEBUG_M
  if(ma == NULL)
    checkCSR(nn, *sia, *sja, NULL, 0);
  else
    checkCSR(nn, *sia, *sja, *sma, 0);
#endif
}





void CSRcolnormINF(csptr m, REAL *normtab)
{
  dim_t i, j, k;
  REAL d;
  dim_t *ja;
  COEF *ma;
  for(i=0;i<m->n;i++)
    {
      ja = m->ja[i];
      ma = m->ma[i];
      for(k=0;k<m->nnzrow[i];k++)
	{
	  d = coefabs(ma[k]);
	  j = ja[k];
	  if(d > normtab[j])
	    normtab[j] = d;
	}
    }
}


void CSRcolnorm1(csptr m, REAL *normtab)
{
  dim_t i, k;
  dim_t *ja;
  COEF *ma;
  for(i=0;i<m->n;i++)
    {
      ja = m->ja[i];
      ma = m->ma[i];
      for(k=0;k<m->nnzrow[i];k++)
	normtab[ja[k]] += coefabs(ma[k]);
    }
}


void CSR_NoDiag_colnorm1(csptr m, REAL *normtab)
{
  dim_t i, k;
  dim_t *ja;
  COEF *ma;
  for(i=0;i<m->n;i++)
    {
      ja = m->ja[i];
      ma = m->ma[i];
      for(k=0;k<m->nnzrow[i];k++)
	normtab[ja[k]] += coefabs(ma[k]);

      for(k=0;k<m->nnzrow[i];k++)
	if(ja[k] == i)
	  {
	    normtab[ja[k]] -= coefabs(ma[k]);
	    break;
	  }
    }
}


void CSRcolnorm2(csptr m, REAL *normtab)
{
  dim_t i, k;
  dim_t *ja;
  COEF *ma;
  for(i=0;i<m->n;i++)
    {
      ja = m->ja[i];
      ma = m->ma[i];
      for(k=0;k<m->nnzrow[i];k++)
	normtab[ja[k]] += ma[k] * CONJ(ma[k]);
    }
}

void CSR_NoDiag_colnorm2(csptr m, REAL *normtab)
{
  dim_t i, k;
  dim_t *ja;
  COEF *ma;
  for(i=0;i<m->n;i++)
    {
      ja = m->ja[i];
      ma = m->ma[i];
      for(k=0;k<m->nnzrow[i];k++)
	normtab[ja[k]] += ma[k] * CONJ(ma[k]);

      for(k=0;k<m->nnzrow[i];k++)
	if(ja[k] == i)
	  {
	    normtab[ja[k]] -= ma[k] * CONJ(ma[k]);
	    break;
	  }
    }
}

void CSRrownormINF(csptr m, REAL *normtab)
{
  dim_t i, k;
  REAL d;
  COEF *ma;

  for(i=0;i<m->n;i++)
    {
      ma = m->ma[i];
      for(k=0;k<m->nnzrow[i];k++)
	{
	  d = coefabs(ma[k]);
	  if(d > normtab[i])
	    normtab[i] = d;
	}
    }
}


void CSRrownorm1(csptr m, REAL *normtab)
{
  dim_t i, k;
  REAL d;
  COEF *ma;

  for(i=0;i<m->n;i++)
    {
      ma = m->ma[i];
      d = 0.0;
      for(k=0;k<m->nnzrow[i];k++)
	d += coefabs(ma[k]);
      normtab[i] += d;
    }
}

void CSR_NoDiag_rownorm1(csptr m, REAL *normtab)
{
  dim_t i, k;
  REAL d;
  dim_t *ja;
  COEF *ma;

  for(i=0;i<m->n;i++)
    {
      ja = m->ja[i];
      ma = m->ma[i];
      d = 0.0;
      for(k=0;k<m->nnzrow[i];k++)
	d += coefabs(ma[k]);

      for(k=0;k<m->nnzrow[i];k++)
	if(ja[k] == i)
	  {
	    d -= coefabs(ma[k]);
	    break;
	  }

      normtab[i] += d;
    }
}


void CSRrownorm2(csptr m, REAL *normtab)
{
  dim_t i, k;
  REAL d;
  COEF *ma;
 
  for(i=0;i<m->n;i++)
    {
      ma = m->ma[i];
      d = 0.0;
      for(k=0;k<m->nnzrow[i];k++)
	d += ma[k] * CONJ(ma[k]);
      normtab[i] += d;
    }
}

void CSR_NoDiag_rownorm2(csptr m, REAL *normtab)
{
  dim_t i, k;
  REAL d;
  dim_t *ja;
  COEF *ma;

  for(i=0;i<m->n;i++)
    {
      ja = m->ja[i];
      ma = m->ma[i];
      d = 0.0;
      for(k=0;k<m->nnzrow[i];k++)
	d += ma[k] * CONJ(ma[k]);

      for(k=0;k<m->nnzrow[i];k++)
	if(ja[k] == i)
	  {
	    d -= ma[k] * CONJ(ma[k]);
	    break;
	  }
      normtab[i] += d;
    }
}

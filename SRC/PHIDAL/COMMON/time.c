/* @authors P. HENON */

/*----------------------------------------------------------------------
 * wallclock timer function
 *
 * dwaltime : time in seconds since 00:00:00 UTC, Jan. 1st, 1970
 *--------------------------------------------------------------------*/
#include <stdlib.h>
#include <sys/time.h>
#include <stdio.h>
#include "phidal_common.h"

double dwalltime()
{
  double t;
  struct timeval tval;
  gettimeofday(&tval,NULL);
  t = (double)tval.tv_sec + (double)tval.tv_usec / 1.0e+6;
  return t;
}

/* @authors P. HENON */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "queue.h"
#include "phidal_common.h"

void vec_filldrop(dim_t *vlen, dim_t *ja, COEF *ma, int_t p, Queue *heap)
{
  /********************************************************************/
  /* This fonction keeps the p largest components of a sparse vector  */
  /* On entry:                                                        */
  /*   vlen : pointer to the number of non zero in the vector          */
  /*   ja, ma : sparse vector in morse format                         */
  /*   p : the number of component to keep                            */
  /* On return :                                                      */
  /*   vlen : the new number of component in the vector = min(m, vlen)  */
  /*    ja, ma : the truncated sparse vector                          */
  /********************************************************************/
  dim_t i,j;
  dim_t n;
  REAL minval;
  REAL d;
  n = *vlen;
  if(n<=p) 
    return; /** Nothing to do the vector has less than p components **/
  if(p==0)
    {
      *vlen = 0;
      return;
    }
	

  queueClear(heap);
  
  for(i=0;i<p;i++)
    {
      queueAdd(heap, i, fabs(ma[i]));
    }
  
  minval = fabs(ma[queueRead(heap)]);
  
  for(i=p;i<n;i++)
    {
      d = fabs(ma[i]);
      if(d <= minval)
	{
	  ja[i] = -1; /** Mark for deletion **/
	  continue;
	}

      /** This component is greater than the minval : add it in the heap and delete the former minval component **/
      j = queueGet(heap);
#ifdef DEBUG_M
      assert(fabs(ma[j]) == minval);
#endif
      ja[j] = -1;
      queueAdd(heap, i, d);
      minval = fabs(ma[queueRead(heap)]);
    }

  /** Compact the vector **/
  j = 0;
  for(i=0;i<n;i++)
    if(ja[i]>= 0) /** entries to be deleted are marked with -1 **/
      {
	ja[j] = ja[i];
	ma[j] = ma[i];
	j++;
      }
#ifdef DEBUG_M
  assert(j == p);
#endif
  
  
  *vlen = p;

}

void LU_filldrop(dim_t *vlen1, dim_t *ja1, COEF *ma1, dim_t *vlen2, dim_t *ja2, COEF *ma2, dim_t p, Queue *heap)
{
  /***********************************************************************/
  /* This fonction keeps the p largest components of two sparse vectors  */
  /*                                                                     */
  /***********************************************************************/
  dim_t i,j;
  dim_t n1, n2;
  REAL minval;
  REAL d;
  n1 = *vlen1;
  n2 = *vlen2;
  if(n1+n2<=p) 
    return; /** Nothing to do the vector has less than p components **/
  if(p==0)
    {
      *vlen1 = 0;
      *vlen2 = 0;
      return;
    }
	

  queueClear(heap);
  
  if(p<=n1)
    for(i=0;i<p;i++)
      queueAdd(heap, i, fabs(ma1[i]));
  else
    {
      for(i=0;i<n1;i++)
	queueAdd(heap, i, fabs(ma1[i]));
      for(i=0;i<p-n1;i++)
	queueAdd(heap, -(i+1), fabs(ma2[i])); /** -i-1 is to differentiate entries in vec2 from those in vec1 **/
    }

  j = queueRead(heap);
  if(j>=0)
    minval = fabs(ma1[j]);
  else
    minval = fabs(ma2[-j-1]);
  
  for(i=p;i<n1;i++)
    {
      d = fabs(ma1[i]);
      if(d <= minval)
	{
	  ja1[i] = -1; /** Mark for deletion **/
	  continue;
	}
      
      j = queueGet(heap);

#ifdef DEBUG_M
      if(j>=0)
	assert(fabs(ma1[j]) == minval);
      else
	assert(fabs(ma2[-j-1]) == minval);
#endif

      if(j>=0)
	ja1[j] = -1;
      else
	ja2[-j-1] = -1;

      queueAdd(heap, i, d);
      j = queueRead(heap);
      if(j>=0)
	minval = fabs(ma1[j]);
      else
	minval = fabs(ma2[-j-1]);
    }

  for(i=MAX(p,n1)-n1;i<n2;i++)
    {
      d = fabs(ma2[i]);
      if(d <= minval)
	{
	  ja2[i] = -1; /** Mark for deletion **/
	  continue;
	}

      /** This component is greater than the minval : add it in the heap and delete the former minval component **/
      j = queueGet(heap);
#ifdef DEBUG_M
      if(j>=0)
	assert(fabs(ma1[j]) == minval);
      else
	assert(fabs(ma2[-j-1]) == minval);
#endif
      if(j>=0)
	ja1[j] = -1;
      else
	ja2[-j-1] = -1;


      queueAdd(heap, -(i+1), d);
      j = queueRead(heap);
      if(j>=0)
	minval = fabs(ma1[j]);
      else
	minval = fabs(ma2[-j-1]);
    }

  /** Compact the vector **/
  j = 0;
  for(i=0;i<n1;i++)
    if(ja1[i]>= 0) /** entries to be deleted are marked with -1 **/
      {
	ja1[j] = ja1[i];
	ma1[j] = ma1[i];
	j++;
      }
  *vlen1 = j;
  
  j = 0;
  for(i=0;i<n2;i++)
    if(ja2[i]>= 0) /** entries to be deleted are marked with -1 **/
      {
	ja2[j] = ja2[i];
	ma2[j] = ma2[i];
	j++;
      }
  *vlen2 = j;
  
#ifdef DEBUG_M
  assert(*vlen1 + *vlen2 == p);
#endif

}

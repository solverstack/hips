/* @authors P. HENON */

#include <stdio.h>
#include <stdlib.h>

#include "phidal_common.h"



void CSR_Fnum2Cnum(dim_t *ja, INTL *ia, dim_t n)
{
  dim_t i, j;
  for(i=0;i<=n;i++)
    ia[i]--;

  for(i=0;i<n;i++)
    for(j=ia[i];j<ia[i+1];j++)
      ja[j]--;
  
}

void CSR_Cnum2Fnum(dim_t *ja, INTL *ia, dim_t n)
{
  dim_t i, j;
  
  for(i=0;i<n;i++)
    for(j=ia[i];j<ia[i+1];j++)
      ja[j]++;
  
  for(i=0;i<=n;i++)
    ia[i]++;
}

void CSR_Write(FILE *fp, flag_t symmetric, dim_t n, INTL *ia, dim_t *ja, COEF *a)
{
 /*-------------------------------
 / Write a sparse matrix in csr format 
 / in a file
 / NOTE: if a is NULL (no coefficient values) 
 /       then all only the non-zero pattern is saved
 / ON ENTRY:
 /     fp   :file descriptor
 /     a, ja ,ia, n : fields of csr matrix
 /------------------------------ */

  dim_t i;  
  INTL nnz;

  nnz = ia[n];
  
  fprintf(fp, _ints_" "_int_"\n", n, nnz);

  for(i=0;i<=n;i++)
    fprintf(fp, _int_" ", ia[i]);
  fprintf(fp, "\n");
  
  for(i=0;i<nnz;i++)
    fprintf(fp, _ints_" ", ja[i]);
  fprintf(fp, "\n");

  if(a != NULL)
    for(i=0;i<nnz;i++)
      {
#if defined(TYPE_REAL)
	fprintf(fp, "%lf ", a[i]);
#elif defined(TYPE_COMPLEX)
	fprintf(fp, "%lf %lf ", pcoef(a[i]));
#endif
      }
  fprintf(fp, "\n");

  /** indicate if the matrix is symetric and only the lower
      triangular part is saved **/
  fprintf(fp, _ints_" \n", symmetric);
}

void CSR_Load(FILE *fp, flag_t *symmetric, dim_t *n, INTL **ia, dim_t **ja, COEF **a)
{
 /*-------------------------------
 / Load a sparse matrix in coordinate format 
 / into a CSR format 
 / in a file
 / ON ENTRY:
 /     fp   : file descriptor
 / ON RETURN : 
 /     a, ja ,ia, n : fields of csr matrix
 / NOTE: if a is NULL (no coefficient values) 
 /       then all only the non-zero pattern is loaded
 /------------------------------ */

  dim_t i;
  INTL nnz;
  
#ifdef TYPE_COMPLEX
  REAL ax, ay;
#endif
  
  /*#if defined(INTSIZE64) || defined(INTSSIZE64)
  FSCANF(fp, "%d %d \n", n, &nnz);
#else
  FSCANF(fp, "%ld %ld \n", n, &nnz);
  #endif*/

  FSCANF(fp, _ints_" "_int_"\n", n, &nnz);


#ifdef DEBUG_M
  assert(*n>=0);
  assert(nnz>=0);
#endif

  if(*n>0)
    *ia = (INTL *)malloc(sizeof(INTL)* (*n+1));
  else
    *ia = NULL;
  

  for(i=0;i<= *n;i++)
    FSCANF(fp, _int_" ", *ia + i);

  if(nnz>0)
    *ja = (dim_t *)malloc(sizeof(dim_t)*nnz);
  else 
    *ja = NULL;


  for(i=0;i<nnz;i++)
    FSCANF(fp, _ints_" ", *ja + i);

  fprintfd(stderr, "NNZ = %ld\n", (long)nnz);

  if(a != NULL && nnz > 0)
    {
      *a  = (COEF *)malloc(sizeof(COEF)*nnz);
      if(*a == NULL)
	{
	  fprintfd(stderr, "nnz = "_int_"malloc failled \n", nnz);
	  exit(-1);
	}
      for(i=0;i<nnz;i++)
#if defined(TYPE_REAL)
#if defined(PREC_SIMPLE)
	FSCANF(fp, "%f ", *a + i);
#else
	FSCANF(fp, "%lf ", *a + i);
#endif
#elif defined(TYPE_COMPLEX)
      {
#if defined(PREC_SIMPLE)
        FSCANF(fp, "%f %f\n", &ax, &ay);
#else
	FSCANF(fp, "%lf %lf\n", &ax, &ay);
#endif
        *(*a + i) = ax + I*ay;
      }
#endif

    }

  FSCANF(fp, _ints_" ", symmetric);
}





void dumpcsr(FILE *fp, COEF *a, dim_t *ja, INTL *ia, dim_t n)
{
 /*-------------------------------
 / Dump a sparse matrix in csr format 
 / into coordinate format (i j values)
 / in a file
 / NOTE: if a is NULL (no coefficient values) 
 /       then all the coefficients values are set to 1
 / ON ENTRY:
 /     fp   :file descriptor
 /     a, ja ,ia, n : fields of csr matrix
 /------------------------------ */

  dim_t i, j;
  
  if(a != NULL)
    {
      if(ia[0] == 0)
	for(i=0;i<n;i++)
	  for(j=ia[i];j<ia[i+1];j++)
	    fprintf(fp, "%ld %ld "_coef_"\n", (long)i, (long)ja[j], pcoef(a[j]));
      else
	for(i=0;i<n;i++)
	  for(j=ia[i];j<ia[i+1];j++)
	    fprintf(fp, "%ld %ld "_coef_"\n", (long)(i+1), (long)(ja[j-1]), pcoef(a[j-1]));
    }
  else
    {
      if(ia[0] == 0)
	for(i=0;i<n;i++)
	  for(j=ia[i];j<ia[i+1];j++)
	    fprintf(fp, "%ld %ld 1 \n", (long)i, (long)(ja[j]));
      else
	for(i=0;i<n;i++)
	  for(j=ia[i];j<ia[i+1];j++)
	    fprintf(fp, "%ld %ld 1\n", (long)(i+1), (long)(ja[j-1]));
    }
}

#ifdef TYPE_REAL
void dumpMTX(FILE *fp, REAL *a, dim_t *ja, INTL *ia, dim_t n)
{
 /*-------------------------------
 / Dump a sparse matrix in csr format 
 / into coordinate format ( in FORTRAN numerotation ) 
 / in a file
 / NOTE: if a is NULL (no coefficient values) 
 /       then all the coefficients values are set to 1
 / ON ENTRY:
 /     fp   :file descriptor
 /     a, ja ,ia, n : fields of csr matrix
 /------------------------------ */

  dim_t i, j;
  INTL nnz;

  nnz = ia[n];
  
  fprintf(fp, "%ld %ld %ld \n", (long)n, (long)n, (long)nnz);

  if(a != NULL)
    {
      for(i=0;i<n;i++)
	for(j=ia[i];j<ia[i+1];j++)
	  fprintf(fp, "%ld %ld %.14e \n", (long)(i+1), (long)(ja[j]+1), a[j]);
    }
  else
    {
      for(i=0;i<n;i++)
	for(j=ia[i];j<ia[i+1];j++)
	  fprintf(fp, "%ld %ld 1 \n", (long)(i+1), (long)(ja[j]+1));
    }
}

#endif

void dumpCS(dim_t num, FILE *fp, csptr A)
{
/*-------------------------------------------------------------+
| to dump a SparRow matrix into file 
| if num == 1 then fortran numbering is used  
|--------------------------------------------------------------*/
  int i, k, nzi;
  int *row;
  COEF *rowm;
  int baseval;

  if(num == 1)
    baseval = 1;
  else
    baseval = 0;

  for (i=0; i<A->n; i++) {
    nzi = A->nnzrow[i];
    row = A->ja[i];
    rowm = A->ma[i];
    for (k=0; k< nzi; k++){
#if defined(TYPE_REAL)
      fprintf(fp,"%ld %ld %e \n",(long)(i+baseval), (long)(row[k]+baseval), rowm[k]);
#elif defined(TYPE_COMPLEX)
      fprintf(fp,"%ld %ld %e %e\n",(long)(i+baseval), (long)(row[k]+baseval), pcoef(rowm[k]));
#endif
    }
  }
}

/* affichage i et j inversé */
void dumpCS2(dim_t num, FILE *fp, csptr A)
{
/*-------------------------------------------------------------+
| to dump a SparRow matrix into file 
| if num == 1 then fortran numbering is used  
|--------------------------------------------------------------*/
  int i, k, nzi;
  int *row;
  COEF *rowm;
  int baseval;

  if(num == 1)
    baseval = 1;
  else
    baseval = 0;

  for (i=0; i<A->n; i++) {
    nzi = A->nnzrow[i];
    row = A->ja[i];
    rowm = A->ma[i];
    for (k=0; k< nzi; k++){
      /* if (row[k]+baseval != i+baseval) { */
#if defined(TYPE_REAL)
      fprintf(fp,"%ld %ld %.13e \n", (long)(row[k]+baseval), (long)(i+baseval), rowm[k]);
#elif defined(TYPE_COMPLEX)
      fprintf(fp,"%ld %ld %.13e  %.13e\n", (long)(row[k]+baseval), (long)(i+baseval), pcoef(rowm[k]));
#endif
      /* } */
    }
  }
}


#ifdef TYPE_REAL
void dumpLLt(FILE *fp, csptr L, csptr U)
{
/*-------------------------------------------------------------+
| to dump a SparRow matrix into file 
|--------------------------------------------------------------*/
  int i, k, nzi;
  int *row;
  COEF *rowm;
  long nnz = 0;;
  
  nnz = (long)CSnnz(L) + L->n;

  fprintf(fp, "%ld %ld %ld \n", (long)L->n, (long)L->n, nnz);
  
  
  for (i=0; i<L->n; i++) {
    nzi = L->nnzrow[i];
    row = L->ja[i];
    rowm = L->ma[i];
    for (k=0; k< nzi; k++){
      fprintf(fp,"%ld %ld %e \n", (long)(i+1), (long)(row[k]+1), rowm[k]);
    }
    fprintf(fp,"%ld %ld %e \n", (long)(i+1), (long)(i+1), 1.0/U->ma[i][0]); /** Dump the diagonal element **/
  }
  
  /*for (i=0; i<U->n; i++) {
    fprintf(fp,"%d %d %e \n", i+1, i+1, 1.0/U->ma[i][0]);
    }*/
}



void writeVecInt(FILE *fp, dim_t *vec, dim_t n)
{
  dim_t i;
  for(i=0;i<n;i++)
    fprintf(fp, "%ld ", (long)(vec[i]));
  fprintf(fp, "\n");
}

void readVecInt(FILE *fp, dim_t *vec, dim_t n)
{
  dim_t i;
  for(i=0;i<n;i++)
    /*#ifndef INTSIZE64*/
    FSCANF(fp, _ints_ , vec+i);
  /*#else
    FSCANF(fp, "%ld ", vec+i);
    #endif*/
}

void writeVecCoef(FILE *fp, COEF *vec, dim_t n)
{
  dim_t i;
  for(i=0;i<n;i++){

#ifdef TYPE_REAL
#ifdef PREC_SIMPLE
    fprintf(fp, "%f ", vec[i]);
#else

    fprintf(fp, "%f ", vec[i]);
#endif
#else
#ifdef PREC_SIMPLE
    fprintf(fp, "%f %f", CREAL(vec[i]), CIMAG(vec[i]));
#else
    fprintf(fp, "%lf %lf", CREAL(vec[i]), CIMAG(vec[i]));
#endif

#endif

  }

  fprintf(fp, "\n");
}

void readVecCoef(FILE *fp, COEF *vec, dim_t n)
{
  dim_t i;
  for(i=0;i<n;i++){
#ifdef TYPE_REAL
#ifdef PREC_SIMPLE
    FSCANF(fp, "%f ", vec+i);
#else
    FSCANF(fp, "%lf ", vec+i);
#endif
#else
    assert(0);
#endif
  }
}

void writeCS(FILE *fp, csptr A)
{
/*-------------------------------------------------------------+
| to dump a SparRow matrix into file 
| if num == 1 then fortran numbering is used  
|--------------------------------------------------------------*/
  dim_t i;
  
  fprintf(fp, "%ld \n", (long)A->n);
  writeVecInt(fp, A->nnzrow, A->n);
  
  for(i=0; i<A->n; i++)
    writeVecInt(fp, A->ja[i], A->nnzrow[i]);

  for(i=0; i<A->n; i++)
     writeVecCoef(fp, A->ma[i], A->nnzrow[i]);
}

int loadCS(FILE *fp, csptr A)
{
  dim_t i;


  FSCANF(fp, _ints_, &A->n);

  initCS(A, A->n);
  readVecInt(fp, A->nnzrow, A->n);
  
  for(i=0; i<A->n; i++)
    {
      if(A->nnzrow[i]>0)
	{
	  A->ja[i] = (dim_t *)malloc(sizeof(dim_t)*A->nnzrow[i]);
	  readVecInt(fp, A->ja[i], A->nnzrow[i]);
	}
      else
	A->ja[i] = NULL;
    }
  
  for(i=0; i<A->n; i++)
    {
      if(A->nnzrow[i]>0)
	{
	  A->ma[i] = (COEF *)malloc(sizeof(COEF)*A->nnzrow[i]);
	  readVecCoef(fp, A->ma[i], A->nnzrow[i]);
	}
      else
	A->ja[i] = NULL;
    }
  return 0;
}



void print_mat(csptr A)
{
  dim_t i, j;

  for(i=0;i<A->n;i++)
    {
      fprintf(stderr, "nnz[%ld] = %ld \n" ,(long)i, (long)(A->nnzrow[i]));
      for(j=0;j<A->nnzrow[i];j++)
	fprintf(stderr, "(%ld %ld) = %g ", (long)i, (long)(A->ja[i][j]), A->ma[i][j]); 
      if(A->nnzrow[i]>0)
	fprintf(stderr, "\n");
    }
}

#endif /*ifdef TYPE_REAL*/

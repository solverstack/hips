/* @authors P. HENON */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "queue.h"
#include "phidal_common.h"


void CS_ILUTP(csptr A, csptr L, csptr U, REAL pivot_ratio, REAL droptol, REAL *droptab, REAL fillrat, dim_t *permtab, Heap *heap, int *wki1, int *wki2, COEF *wkd)
{
  /************************************************************************************************/
  /* This function compute an ILUTP factorization of a sparse matrix                              */
  /* The pivoting is allowed along the column of the matrix                                       */
  /* the permutation is returned in permtab                                                       */
  /* The dropping is contral via droptab: any term in column i of L lesser than                   */
  /* Uii*droptab[i]*droptol are dropped and any term in row i of U lesser than droptab[i]*droptol */
  /* are dropped. Usually droptab[i] contains Norm(A[i,i:n])                                      */
  /* If droptab is NULL then droptab is consider to be unitary                                    */
  /*                                                                                              */
  /* NOTE: the algorithm can work in place (i.e. L can be equals to A or U in inputs              */
  /* IMPORTANT : the matrix L and U are not reordered according to permtab in this function       */
  /* they are left in the original numbering. It is up to the user to use permtab or permute the  */
  /* matrix L and U accordingly to permtab                                                        */
  /************************************************************************************************/

  /***********************************************************************************/
  /* NOTE: We assume that every row i of U contains the diagonal term in first place */
  /***********************************************************************************/
     
  /*** OIMBE LE REALLOC_BLOCK EST A FAIRE COMME DANS CS_ILUT (small block
       aussi a faire ***/


  dim_t i, j, k;
  int jcol, jpos;
  int *jrev, *tmpj;  
  COEF *tmpa;
  int *aja, *lja, *uja ,*rowjz;
  COEF *ama, *lma, *uma ,*rowaz;


  REAL tt;
  COEF s, d;
  int lenl, lenu, nnzz;
  REAL maxfill=-1;
  flag_t fillrow;
  Queue queue;
  long nnzA;
  dim_t *ipermtab;

  if(fillrat > 0)
    {
      k = 0;
      for(i=0;i<L->n;i++)
	if(A->nnzrow[i] > k)
	  k = A->nnzrow[i];
      queueInit(&queue, (int)(k*fillrat));
      nnzA = CSnnz(A);
      maxfill = nnzA*fillrat;
    }

  Heap_Clear(heap);
  jrev = wki1;
  tmpj = wki2;
  tmpa = wkd;

  /** Init the permutation array **/
  ipermtab = (int *)malloc(sizeof(int)*A->n);
  for(i=0;i<A->n;i++)
    {
      permtab[i] = i;
      ipermtab[i] = i;
    }


  for(j=0;j<L->n;j++) 
    jrev[j] = -1;

  for(i=0;i<A->n;i++)
    {
      aja = A->ja[i];
      ama = A->ma[i];
      if(A->nnzrow[i] == 0)
	{
	  fprintfd(stderr, "Problem row %ld of A is null \n", (long)i);
	  exit(-1);
	}
      /*** Copy the l part and the u part of A ***/
      lenl = 0;
      lenu = 1; /** diagonal is always stored **/
      tmpj[i] = ipermtab[i];
      tmpa[i] = 0.0;
      for(k=0;k<A->nnzrow[i];k++)
	{
	  jcol = permtab[aja[k]];
	  if(jcol < i)
	    {
	      tmpj[lenl] = aja[k];
	      tmpa[lenl] = ama[k];
	      lenl++;
	      continue;
	    }
	  if(jcol > i)
	    {
	      tmpj[i+lenu] = aja[k];
	      tmpa[i+lenu] = ama[k];
	      lenu++;
	      continue;
	    }
	  if(jcol==i)
	    {
#ifdef DEBUG_M
	      assert(aja[k] == tmpj[i]);
#endif
	      tmpa[i] = ama[k];
	      continue;
	    }
	}

      lja = tmpj;
      lma = tmpa;
      uja = tmpj+i;
      uma = tmpa+i;

      /** Init jrev **/
      for(j=0;j<lenl;j++)
	jrev[permtab[lja[j]]] = j;

      for(j=0;j<lenu;j++)
	jrev[permtab[uja[j]]] = j+i;

   

      for(j=0;j<lenl;j++)
	Heap_Add(heap, permtab[lja[j]]);
      
      while(Heap_Size(heap)>0)
	{
	  jcol = Heap_Get(heap);
	  j = jrev[jcol];
#ifdef DEBUG_M
	  assert(permtab[tmpj[j]] == jcol);
#endif	  
	  nnzz = U->nnzrow[jcol];
	  rowjz = U->ja[jcol];
	  rowaz = U->ma[jcol];

#ifdef DEBUG_M
	  assert(permtab[rowjz[0]] == jcol);
#endif
	  d =  tmpa[j]*rowaz[0]; /** rowaz[0] is never null since it is the inverse of a previous diagonal term **/
	  if(droptab != NULL)
	    tt = droptab[jcol]*droptol;
	  else
	    tt = droptol;

	  if(coefabs(d) <= tt) /** Dropping in the L factor **/
	    {
	      jrev[jcol] = -1; /** Reinit jrev now for this position (can not do it later) **/
	      tmpj[j] = -1; /** Mark this entry to be delete **/
	      continue;
	    }

	  tmpa[j] = d;

	  for(k=1;k<nnzz;k++)
	    {
	      s = d*rowaz[k];
	      jcol = permtab[rowjz[k]];
	      jpos = jrev[jcol];
	      if(jpos >= 0)
		tmpa[jpos] -= s;
	      else
		{
		  if(jcol < i) /** New fill-in entry in L **/
		    {
		      Heap_Add(heap, jcol);
		      jrev[jcol] = lenl;
		      tmpj[lenl] = rowjz[k];
		      tmpa[lenl] = -s;
		      lenl++;
		    }
		  else
		    {  /** New fill-in entry in U **/
		      jrev[jcol] = lenu+i;
		      tmpj[lenu+i] = rowjz[k];
		      tmpa[lenu+i] = -s;
		      lenu++;
		    }
		}

	    }
	}

  
     
      /*** Do these steps in the right order ! ****/

      /**** 1 Dropping in L row ******/
      jpos = 0;
      for(k=0;k<lenl;k++)
	if(tmpj[k]>= 0) /** entries to be deleted are marked with -1 **/
	  {
	    tmpj[jpos] = tmpj[k];
	    tmpa[jpos] = tmpa[k];
	    jpos++;
	  }
      lenl = jpos;
      
      /**** 2 Reinit jrev ****/
      for(k=0;k<lenl;k++) 
	jrev[permtab[tmpj[k]]] = -1;
      for(k=i;k<i+lenu;k++)
	jrev[permtab[tmpj[k]]] = -1;



      
    /****3  Find the largest entry in the U row part and swap it to the diagonal ****/
      tt = 0.0;
      jpos = -1;
      for(k=i;k<lenu+i;k++)
	if(coefabs(tmpa[k])>tt)
	  {
	    tt = coefabs(tmpa[k]);
	    jpos = k;
	  }
      /*if(coefabs(tmpa[i]) > EPSILON) *//** Always pivot when there is a small diagonal term **/
	/*** perform the pivoting only if tt > pivot_ratio*uii ***/
	if(tt < coefabs(tmpa[i])*pivot_ratio)
	  jpos = -1; 

	if(coefabs(tmpa[i]) < EPSILON && jpos == -1)
	  fprintf(stderr, "CS_ILUTP %ld no pivot available \n", (long)i );
	
	if(jpos != -1)
	  {
#ifdef DEBUG_M
	  assert(permtab[tmpj[i]] == i);
#endif

	  jcol = permtab[tmpj[jpos]];

	  if(jcol != i)
	    fprintferr(stderr, "PIVOT %ld : ratio = %g \n", (long)i, coefabs(tmpa[jpos])/coefabs(tmpa[i]));
	  
	  permtab[tmpj[jpos]] = i;
	  ipermtab[i] = tmpj[jpos];

	  permtab[tmpj[i]] = jcol;
	  ipermtab[jcol] = tmpj[i];

	  /** Now we put the diagonal term in position i in tmp (needed for the dropping algorithms) **/
	  d =  tmpa[jpos];
	  k =  tmpj[jpos];
	  tmpa[jpos] = tmpa[i];
	  tmpj[jpos] = tmpj[i];
	  tmpa[i] = d;
	  tmpj[i] = k;
	}
      /*else
	fprintfd(stderr, "Row %ld of U is null ! \n", i);*/



      /**** 4 Dropping in U row ******/
      if(droptab != NULL)
	tt = droptab[i]*droptol;
      else
	tt = droptol;

#ifdef SYMMETRIC_DROP
      tt*=coefabs(tmpa[i]);
#endif

      jpos = i+1; 
      for(k=i+1;k<i+lenu;k++)
	{
	  if(coefabs(tmpa[k])> tt)
	    {
	      tmpj[jpos] = tmpj[k];
	      tmpa[jpos] = tmpa[k];
	      jpos++;
	    }
	}
      lenu = jpos-i;

      /**** 5 Keep only the largest entries in U row according to fillrat ***/
      if(fillrat > 0)
	{

	  k = lenu-1;
	  fillrow = (int)( (REAL)(maxfill)/(A->n-i));
	  LU_filldrop(&lenl, tmpj, tmpa, &k, tmpj+i+1, tmpa+i+1, fillrow, &queue);
	  lenu = k+1;
	  maxfill -= lenl+lenu;
	  nnzA -= A->nnzrow[i];
	}


      /**** 6 Inverse the diagonal term in U *******/
      if(coefabs(tmpa[i]) < EPSILON)
	{
	  fprintferr(stderr, "Pivot (%ld, %ld) is small "_coef_": replace it \n", (long)i, (long)i, pcoef(tmpa[i]));
	  tmpa[i] = EPSILON;
	}
      tmpa[i] = 1.0/tmpa[i];
    

      /**** 7 Create the new row in L ******/
      if(L->inarow != 1 && L->nnzrow[i] > 0)
	{
	  free(L->ja[i]);
	  free(L->ma[i]);
	}
      L->nnzrow[i] = lenl;
      if(lenl>0)
	{
	  L->ja[i] = (int *)malloc(sizeof(int)*lenl);
	  memcpy(L->ja[i], tmpj, sizeof(int)*lenl);
	  L->ma[i] = (COEF *)malloc(sizeof(COEF)*lenl);
	  memcpy(L->ma[i], tmpa, sizeof(COEF)*lenl);
	}




      /**** 6  Create the new row in U ******/
      if(U->inarow != 1 && U->nnzrow[i] > 0)
	{
	  free(U->ja[i]);
	  free(U->ma[i]);
	}
      U->nnzrow[i] = lenu;
      U->ja[i] = (int *)malloc(sizeof(int)*lenu);
      memcpy(U->ja[i], tmpj+i, sizeof(int)*lenu);
      U->ma[i] = (COEF *)malloc(sizeof(COEF)*lenu);
      memcpy(U->ma[i], tmpa+i, sizeof(COEF)*lenu);
     

      /*** 7 Sort the rows : PAS RENTABLE essayer sur BMWCRA1 droptol = 0.01 ***/
      /*quicksort_row(L->ja[i], L->ma[i], 0, L->nnzrow[i]-1);
	quicksort_row(U->ja[i], U->ma[i], 0, U->nnzrow[i]-1);*/

    }


#ifdef DEBUG_M
  {
    int *flag;
    flag = (int *)malloc(sizeof(int)*A->n);
    bzero(flag, sizeof(int)*A->n);
    for(i=0;i<A->n;i++)
      assert(permtab[ipermtab[i]] == i);

    for(i=0;i<A->n;i++)
      flag[permtab[i]]++;

    for(i=0;i<A->n;i++)
      assert(flag[i] == 1);
    
    free(flag);
  }
#endif


  free(ipermtab);
  if(fillrat > 0)
    queueExit(&queue);
  
  if(L->inarow == 1)
    {
      if(L->matab != NULL)
	free(L->matab);
      if(L->jatab != NULL)
	free(L->jatab);
      L->inarow = 0;
    }
  if(U->inarow == 1)
    {
      if(U->matab != NULL)
	free(U->matab);
      if(U->jatab != NULL)
	free(U->jatab);
      U->inarow = 0;
    }
  
  CS_SetNonZeroRow(L);
  CS_SetNonZeroRow(U);

 
#ifdef REALLOC_BLOCK
  CSrealloc(L);
  CSrealloc(U);
#endif
}



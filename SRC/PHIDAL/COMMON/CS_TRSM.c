/* @authors P. HENON */

#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <math.h>
#include <stdio.h>

#include "queue.h"
#include "phidal_common.h"

/*** OIMBE a faire CSR_TRSM interface propre ***/


void CSC_CSR_InvLT(csptr  L, csptr M,  int Mncol, REAL droptol, REAL *droptab, REAL fillrat, int *wki1, int *wki2, COEF *wkd, cell_int *celltab, cell_int **LrowList)
{
  /*********************************************************************/
  /* this function does M = L^-1.M  in place                           */
  /* with a dropping criterion droptol*droptab[i] in each row          */
  /* if droptab == NULL then it is considered as unitary               */
  /* L is a unitary CSC diagonal matrix  (diag is not stored)          */
  /* M is a CSR matrix                                                 */ 
  /* NOTE: L IS SORTED BY row indices                                  */
  /*      This is done if L comes from the CS_ICC routine              */
  /*********************************************************************/
  dim_t i, j, k;
  dim_t n;
  cell_int *c, *next;
  cell_int **l;
  int jcol, jpos;
  int *jrev;
  dim_t *tmpj;  
  COEF *tmpa;
  int *rowja;
  COEF *rowma;
  REAL d;
  COEF s;
  int len;
  REAL maxfill=-1;
  flag_t fillrow;
  Queue queue;
  long nnzM, nnz;
  int frowind;

#ifdef ALLOC_COMPACT_ILUT
  int memsize;
#else
  int small = 0;
#endif

  dim_t *jatab=NULL;
  COEF *matab=NULL;

#ifdef DEBUG_M
  CS_Check(L, L->n);
  CS_Check(M, Mncol);
#endif


  if(M->nnzr == 0)
    return;

#ifdef ALLOC_COMPACT_ILUT
  memsize = MAX((int)(CSnnz(M)*ALLOC_COMPACT_INIT), Mncol);
  jatab = (int *)malloc(sizeof(int)*memsize);
  matab = (COEF *)malloc(sizeof(COEF)*memsize);
#else
  small = 0;
  if((REAL)M->n*(REAL)Mncol <= (REAL)SMALLBLOCK)
    {
      small = 1;
      jatab = (dim_t *)malloc(sizeof(dim_t)*(M->n*Mncol));
      matab = (COEF *)malloc(sizeof(COEF)*(M->n*Mncol));
    }
#endif


#ifdef DEBUG_M
  assert(L->n == M->n);
#endif

  n = L->n;


  if(fillrat > 0)
    {
      k = 0;
      for(j=0;j<M->nnzr;j++)
	{
	  i = M->nzrtab[j];
	  k = M->nnzrow[i];
	}
      queueInit(&queue, (int)((k+1)*fillrat));
      nnzM = CSnnz(M);
      maxfill = nnzM*fillrat;
    }

  jrev = wki1;

#ifdef ALLOC_COMPACT_ILUT
   tmpj = jatab;
   tmpa = matab;
#else
  if(small==1)
    {
      tmpj = jatab;
      tmpa = matab;
    }
  else
    {
      tmpj = wki2;
      tmpa = wkd;
    }
#endif

  for(j=0;j<Mncol;j++) 
    jrev[j] = -1;


  /*** We not need to treat the first null row of M ***/
  frowind = M->nzrtab[0];

  /** Init the cellules **/
  bzero(LrowList, sizeof(cell_int *)*n);

  /** Initialize the row list of L **/
  for(k=0;k<L->nnzr;k++)
    {
      i = L->nzrtab[k];
      celltab[i].nnz = L->nnzrow[i];
      /*if(L->nnzrow[i]>0)*/
	{
#ifdef DEBUG_M
	  assert(L->ja[i][0] > i); /** L is unitary on the diagonal (not stored) **/
#endif
	  celltab[i].elt = i;
	  celltab[i].ja = L->ja[i];
	  celltab[i].ma = L->ma[i];
	  jcol = L->ja[i][0];

	  l = LrowList+jcol;
	  c = celltab+i;
	  c->next = *l;
	  *l = c;
	}
    }

  nnzM = 0;
#ifdef ALLOC_COMPACT_ILUT
  /** We need to maintain the nzrtab during the computation 
      of M **/
  M->nnzr =0;
#endif
  for(k=frowind;k<n;k++) 
    {
      /***************************************/
      /* Treat row k of M                    */
      /***************************************/
#ifdef ALLOC_COMPACT_ILUT
      if(nnzM + Mncol > memsize)
	{
	  memsize = MAX(memsize*ALLOC_COMPACT_RATIO, (nnzM+Mncol));
	  jatab = (int *)realloc(jatab, sizeof(int)*memsize);
	  matab = (COEF *)realloc(matab, sizeof(COEF)*memsize);
	  assert(jatab != NULL);
	  assert(matab != NULL);
	  /** We need to reset M->ja and M->ma **/
	  len = 0;
	  for(j=0;j<M->nnzr;j++)
	    {
	      i = M->nzrtab[j];
	      M->ja[i] = jatab+len;
	      M->ma[i] = matab+len;
	      len += M->nnzrow[i];
	    }
	  
	}
      tmpj = jatab + nnzM;
      tmpa = matab + nnzM;
#endif

      /** Unpack row k of M **/
      len = M->nnzrow[k];
#ifdef DEBUG_M
      assert(len <= Mncol);
#endif

      if(len>0)
	{
	  memcpy(tmpj, M->ja[k], sizeof(int)*len);
	  memcpy(tmpa, M->ma[k], sizeof(COEF)*len);
	}
      
      /* Init jrev */
      for(j=0;j<len;j++)
	jrev[tmpj[j]] = j;

      c = LrowList[k];
      while(c!=NULL)
	{
	  next = c->next; /** chainage modifie c->next so we must keep record of it **/
	  j = c->elt;
	  s = (*c->ma);
	  nnz = M->nnzrow[j];

	  /** Update the next columns lists **/
	  c->nnz--;
	  if(c->nnz > 0 && nnz > 0) /* if nnzrow[j] = 0 then no need to chain */
	    {                        /* the rest of the column j of L          */         
	      c->ja++;
	      c->ma++;
	      jcol = *(c->ja);
#ifdef DEBUG_M
	      assert(jcol>k);
#endif
	      l = LrowList+jcol;
	      c->next = *l;
	      *l = c;
	    }
	  c = next;
	  
	  

	  rowja = M->ja[j];
	  rowma = M->ma[j];
	  
	  
	  for(i=0;i<nnz;i++)
	    {
	      jcol = rowja[i];
	      jpos = jrev[jcol];
	      
	      if(jpos >= 0)
		tmpa[jpos] -= s*rowma[i];
	      else
		{
		  /** New entry in row k of M **/
		  jrev[jcol] = len;
		  tmpj[len] = jcol;
		  tmpa[len] = -s*rowma[i];
		  len++;
		}
	    }
	}

      /*** Do these steps in the right order ! ****/	   

      /**** 1 Reinit jrev ****/ /** DO IT HERE !! **/
      for(i=0;i<len;i++) 
	jrev[tmpj[i]] = -1;


      /**** 3 Dropping in row k of M ******/
      if(droptab != NULL)
	d = droptab[k]*droptol;
      else
	d = droptol;
      
      jpos = 0;
      for(i=0;i<len;i++) 
	if(coefabs(tmpa[i])> d)
	  {
	    tmpj[jpos] = tmpj[i];
	    tmpa[jpos] = tmpa[i];
	    jpos++;
	  }
      len = jpos;
	  
      /**** 4 Keep only the largest entries in row k of M  according to fillrat ***/
      if(fillrat > 0)
	{
	  fillrow = (int)( (REAL)(maxfill)/(n-k));
	  vec_filldrop(&len, tmpj, tmpa, fillrow, &queue);
	  maxfill -= len;
	}
	  
	 
	  
      /**** 5 Create the new row in M ******/
      /** Do not use a realloc it is worst (tested) **/

      if(M->inarow != 1 && M->nnzrow[k] > 0)
	{
	  free(M->ja[k]);
	  free(M->ma[k]);
	}

#ifdef ALLOC_COMPACT_ILUT
      if(len>0)
	{
	  M->nzrtab[M->nnzr++] = k;
	  M->ja[k] = tmpj;
	  M->ma[k] = tmpa;
	  nnzM += (long)len;
	}
      else
	{
	  M->ja[k] = NULL;
	  M->ma[k] = NULL;
	}
#else
      if(len > 0)
	{
	   if(small==1)
	    {
	      M->ja[k] = tmpj;
	      M->ma[k] = tmpa;
	      tmpj += len;
	      tmpa += len;
	      nnzM += (long)len;
	    }
	  else
	    {
	      M->ja[k] = (int *)malloc(sizeof(int)*len);
	      memcpy(M->ja[k], tmpj, sizeof(int)*len);
	      M->ma[k] = (COEF *)malloc(sizeof(COEF)*len);
	      memcpy(M->ma[k], tmpa, sizeof(COEF)*len);
	    }
	}
#endif
      M->nnzrow[k] = len;
    }
  
  if(fillrat > 0)
    queueExit(&queue);	

  if(M->inarow == 1)
    {
      if(M->matab != NULL)
	free(M->matab);
      if(M->jatab != NULL)
	free(M->jatab);
      M->inarow = 0;
    }

  CS_SetNonZeroRow(M);
#ifndef ALLOC_COMPACT_ILUT
  if(small == 1)
#endif
    {
      matab = (COEF *)realloc(matab, sizeof(COEF)*nnzM);
      jatab = (int *)realloc(jatab, sizeof(int)*nnzM);
      
      M->matab = matab;
      M->jatab = jatab;
      nnzM = 0;
      for(k=0;k<M->nnzr;k++)
	{
	  i = M->nzrtab[k];
	  M->ja[i] = jatab+nnzM;
	  M->ma[i] = matab+nnzM;
	  nnzM += M->nnzrow[i];
	}
      M->inarow = 1;
    }

#ifdef DEBUG_M
  CS_Check(M, Mncol);
#endif
}



void CSR_CSR_InvLT(csptr L, csptr M, int Mncol, REAL droptol, REAL *droptab, REAL fillrat, int *wki1, int *wki2, COEF *wkd)
{
  /*********************************************************************/
  /* this function does M = L^-1.M  in place                           */
  /* with a dropping criterion droptol*droptab[i] in each row          */
  /* if droptab == NULL then it is considered as unitary               */
  /* L is a unitary CSR diagonal matrix  (diag is not stored)          */
  /* M is a CSR matrix                                                 */ 
  /*********************************************************************/
  int *jrev;
  int *tmpj;
  COEF *tmpa;
  int *rowjz, *Lrowj, *Mrowj;
  COEF *rowaz, *Lrowa, *Mrowa;
  int lenr, Lnnz, rownnz;
  dim_t i, j, k;
  int jcol, jpos;
  REAL d;
  long nnzM;
  REAL maxfill=-1;
  flag_t fillrow;
  Queue queue;

#ifdef ALLOC_COMPACT_ILUT
  int memsize;
#else
  int small = 0;
#endif

  dim_t *jatab=NULL;
  COEF *matab=NULL;

  if(M->nnzr == 0)
    return;

#ifdef ALLOC_COMPACT_ILUT
  memsize = MAX((int)(CSnnz(M)*ALLOC_COMPACT_INIT), Mncol);
  jatab = (int *)malloc(sizeof(int)*memsize);
  matab = (COEF *)malloc(sizeof(COEF)*memsize);
#else
  small = 0;
  if((REAL)M->n*(REAL)Mncol <= (REAL)SMALLBLOCK)
    {
      small = 1;
      jatab = (int *)malloc(sizeof(int)*(M->n*Mncol));
      matab = (COEF *)malloc(sizeof(COEF)*(M->n*Mncol));
    }
#endif




#ifdef DEBUG_M
  assert(L->n == M->n);
#endif

  jrev = wki1;
#ifdef ALLOC_COMPACT_ILUT
  tmpj = jatab;
  tmpa = matab;
#else
  if(small==1)
   {
     tmpj = jatab;
     tmpa = matab;
   }
  else
    {
      tmpj = wki2;
      tmpa = wkd;
    }
#endif


  if(fillrat > 0)
    {
      k = 0;
      /*for(i=0;i<M->n;i++)
	if(M->nnzrow[i] > k)*/
       for(j=0;j<M->nnzr;j++)
	{
	  i = M->nzrtab[j];
	  k = M->nnzrow[i];
	}
      queueInit(&queue, (int)(k*fillrat));
      
      nnzM = CSnnz(M);
      maxfill = nnzM*fillrat;
    }
  
  /*** Init jrev ***/
  for(i=0;i<Mncol;i++)
    jrev[i] = -1;

  i = M->nzrtab[0];
  
  nnzM = 0;
#ifdef ALLOC_COMPACT_ILUT
  /** We need to maintain the nzrtab during the computation 
      of M **/
  M->nnzr =0;
#endif
  for(;i<M->n;i++)
    {
#ifdef ALLOC_COMPACT_ILUT
      if(nnzM + Mncol > memsize)
	{
	  memsize = MAX(memsize*ALLOC_COMPACT_RATIO, (nnzM+Mncol));
	  jatab = (int *)realloc(jatab, sizeof(int)*memsize);
	  matab = (COEF *)realloc(matab, sizeof(COEF)*memsize);
	  assert(jatab != NULL);
	  assert(matab != NULL);
	  /** We need to reset M->ja and M->ma **/
	  lenr = 0;
	  for(j=0;j<M->nnzr;j++)
	    {
	      k = M->nzrtab[j];
	      M->ja[k] = jatab+lenr;
	      M->ma[k] = matab+lenr;
	      lenr += M->nnzrow[k];
	    }
	}
      tmpj = jatab + nnzM;
      tmpa = matab + nnzM;
#endif

      rowjz = M->ja[i];
      rowaz = M->ma[i];
      lenr = M->nnzrow[i];

      Lrowj = L->ja[i];
      Lrowa = L->ma[i];
      Lnnz = L->nnzrow[i];

      /*** Unpack row i of m in jrev, tmpj, tmpa ***/
      for(j=0;j<lenr;j++)
	jrev[rowjz[j]] = j;
      if(lenr>0)
	{
	  memcpy(tmpj, rowjz, sizeof(int)*lenr);
	  memcpy(tmpa, rowaz, sizeof(COEF)*lenr);
	}
      
      for(j=0;j<Lnnz;j++)
	{
	  jcol = Lrowj[j];
#ifdef DEBUG_M
	  assert(jcol <i);
#endif
	  rownnz = M->nnzrow[jcol];
	  if(rownnz == 0)
	    continue;

	  d = Lrowa[j];
	  Mrowj = M->ja[jcol];
	  Mrowa = M->ma[jcol];

	  for(k=0;k<rownnz;k++)
	    {
	      jcol = Mrowj[k];
	      jpos = jrev[jcol];
	      if(jpos >= 0)
		tmpa[jpos] -= d*Mrowa[k];
	      else
		{
		  /** Fill-in element **/
		  jrev[jcol] = lenr;
		  tmpa[lenr] = -d*Mrowa[k];
		  tmpj[lenr] = jcol;
		  lenr++;
		}
	    }
	}
      /*** Reinit jrev ***/
      for(k=0;k<lenr;k++)
	jrev[tmpj[k]] = -1;
      

      /**** Dropping in M row ******/
      if(droptab != NULL)
	d = droptab[i]*droptol;
      else
	d = droptol;

      jpos = 0; 
      for(k=0;k<lenr;k++)
	if(coefabs(tmpa[k])> d)
	  {
	    tmpj[jpos] = tmpj[k];
	    tmpa[jpos] = tmpa[k];
	    jpos++;
	  }
      lenr = jpos;
      
      /**** 3 Keep only the largest entries in the row according to fillrat ***/
      if(fillrat > 0)
	{
	  fillrow = (int) ( ((REAL)maxfill)/(M->n-i));
	  vec_filldrop(&lenr, tmpj, tmpa, fillrow, &queue);
	  maxfill -= lenr;
	}


      /*** Create the new row i in m ***/
      if(M->inarow != 1 && M->nnzrow[i] > 0)
	{
	  free(M->ja[i]);
	  free(M->ma[i]);
	}
      M->nnzrow[i] = lenr;

#ifdef ALLOC_COMPACT_ILUT
      if(lenr>0)
	{
	  M->nzrtab[M->nnzr++] = i;
	  M->ja[i] = tmpj;
	  M->ma[i] = tmpa;
	  nnzM += (long)lenr;
	}
      else
	{
	  M->ja[i] = NULL;
	  M->ma[i] = NULL;
	}
#else
      if(lenr > 0)
	{
	  if(small==1)
	    {
	      M->ja[i] = tmpj;
	      M->ma[i] = tmpa;
	      tmpj += lenr;
	      tmpa += lenr;
	      nnzM += (long)lenr;
	    }
	  else
	    {
	      M->ja[i] = (int *)malloc(sizeof(int)*lenr);
	      memcpy(M->ja[i], tmpj, sizeof(int)*lenr);
	      M->ma[i] = (COEF *)malloc(sizeof(COEF)*lenr);
	      memcpy(M->ma[i], tmpa, sizeof(COEF)*lenr);
	    }
	}
#endif
    }
  if(fillrat > 0)
    queueExit(&queue);
  
  if(M->inarow == 1)
    {
      if(M->matab != NULL)
	free(M->matab);
      if(M->jatab != NULL)
	free(M->jatab);
      M->inarow = 0;
    }

  CS_SetNonZeroRow(M);
#ifndef ALLOC_COMPACT_ILUT
  if(small == 1)
#endif
    {
      matab = (COEF *)realloc(matab, sizeof(COEF)*nnzM);
      jatab = (int *)realloc(jatab, sizeof(int)*nnzM);
      
      M->matab = matab;
      M->jatab = jatab;
      nnzM = 0;
      for(k=0;k<M->nnzr;k++)
	{
	  i = M->nzrtab[k];
	  M->ja[i] = jatab+nnzM;
	  M->ma[i] = matab+nnzM;
	  nnzM += M->nnzrow[i];
	}
      M->inarow = 1;
    }

#ifdef DEBUG_M
  CS_Check(M, Mncol);
#endif
}



void CSR_CSR_InvUT(csptr U, csptr M, REAL droptol, REAL *droptab,  REAL fillrat, Heap *heap, int *wki1, int *wki2, COEF *wkd)
{
  /************************************************************************/
  /* this function does M = M.U^-1 done in place                          */
  /* with a dropping criterion droptol*droptab[i] in each column          */
  /* if droptab = NULL then it is considered as unitary                   */
  /* U is a CSR diagonal matrix (diag is stored at begining of each row)  */
  /* M is a CSR matrix                                                    */ 
  /************************************************************************/
  dim_t i, j, k, r;
  int jcol, jpos;
  int *jrev, *tmpj;  
  COEF *tmpa;
  int *Mja, *rowjz;
  COEF *Mma,*rowaz;
  
  REAL d, s, t;
  int lenr, nnzz;
  REAL maxfill=-1;
  flag_t fillrow;
  Queue queue;
  long nnzM;

#ifdef ALLOC_COMPACT_ILUT
  int memsize;
#else
  int small = 0;
#endif
  dim_t *jatab=NULL;
  COEF *matab=NULL;

  if(M->nnzr == 0)
    return;

#ifdef ALLOC_COMPACT_ILUT
  memsize = MAX((int)(CSnnz(M)*ALLOC_COMPACT_INIT), U->n);
  jatab = (int *)malloc(sizeof(int)*memsize);
  matab = (COEF *)malloc(sizeof(COEF)*memsize);
#else
  small = 0;
  if((REAL)M->n*(REAL)U->n <= (REAL)SMALLBLOCK)
    {
      small = 1;
      jatab = (int *)malloc(sizeof(int)*(M->n * U->n));
      matab = (COEF *)malloc(sizeof(COEF)*(M->n * U->n));
    }
#endif

  Heap_Clear(heap);
  jrev = wki1;

#ifdef ALLOC_COMPACT_ILUT
  tmpj = jatab;
  tmpa = matab;
#else
  if(small==1)
    {
      tmpj = jatab;
      tmpa = matab;
    }
  else
    {
      tmpj = wki2;
      tmpa = wkd;
    }
#endif

  if(fillrat > 0)
    {
      k = 0;

      for(j=0;j<M->nnzr;j++)
	{
	  i = M->nzrtab[j];
	  k = M->nnzrow[i];
	}
      queueInit(&queue, (int)(k*fillrat));
      
      nnzM = CSnnz(M);
      maxfill = nnzM*fillrat;
    }


  for(j=0;j<U->n;j++) 
    jrev[j] = -1;

  nnzM = 0;
  for(r=0;r<M->nnzr;r++)
    {
#ifdef ALLOC_COMPACT_ILUT
      if(nnzM + U->n > memsize)
	{
	  memsize = MAX(memsize*ALLOC_COMPACT_RATIO, (nnzM+U->n));
	  jatab = (int *)realloc(jatab, sizeof(int)*memsize);
	  matab = (COEF *)realloc(matab, sizeof(COEF)*memsize);
	  assert(jatab != NULL);
	  assert(matab != NULL);
	}
      tmpj = jatab + nnzM;
      tmpa = matab + nnzM;
#endif
      i = M->nzrtab[r];
      Mja = M->ja[i];
      Mma = M->ma[i];

      lenr = M->nnzrow[i];
      for(j=0;j<lenr;j++)
	jrev[Mja[j]] = j;

      memcpy(tmpj, Mja, sizeof(int)*lenr);
      memcpy(tmpa, Mma, sizeof(COEF)*lenr);

      for(j=0;j<lenr;j++)
	Heap_Add(heap, Mja[j]);
      
      while(Heap_Size(heap)>0)
	{
	  jcol = Heap_Get(heap);
	  j = jrev[jcol];
	  
	  nnzz = U->nnzrow[jcol];
	  rowjz = U->ja[jcol];
	  rowaz = U->ma[jcol];

	  
	  d =  tmpa[j]*rowaz[0]; /** rowaz[0] is never null since it is the inverse of a previous diagonal term **/
	  if(droptab != NULL)
	    t =  droptab[tmpj[j]]*droptol;
	  else
	    t = droptol;
	   
	  if(fabs(d) <=t) /** Dropping in the L factor **/
	    {
	      jrev[ tmpj[j] ] = -1; /** Reinit jrev now for this position (can not do it later) **/
	      tmpj[j] = -1; /** Mark this entry to be delete **/

	      continue;
	    }

	  tmpa[j] = d;

	  for(k=1;k<nnzz;k++)
	    {
	      s = d*rowaz[k];
	      jcol = rowjz[k];
	      jpos = jrev[jcol];
	      if(jpos >= 0)
		tmpa[jpos] -= s;
	      else
		{
		  /** New fill-in entry in M row i **/
		  Heap_Add(heap, jcol);
		  jrev[jcol] = lenr;
		  tmpj[lenr] = jcol;
		  tmpa[lenr] = -s;
		  lenr++;
		}

	    }
	}

      /*** Do this steps in the right order ! ****/

      /****  1 Dropping in M row i ******/
      jpos = 0;
      for(k=0;k<lenr;k++)
	if(tmpj[k]>= 0) /** entries to be deleted are marked with -1 **/
	  {
	    tmpj[jpos] = tmpj[k];
	    tmpa[jpos] = tmpa[k];
	    jpos++;
	  }
      lenr = jpos;
      

      /**** 2 Reinit jrev ****/ /** DO IT HERE !! **/
      for(k=0;k<lenr;k++) 
	jrev[tmpj[k]] = -1;
      
      /**** 3 Keep only the largest entries in the row according to fillrat ***/
      if(fillrat > 0)
	{
	  fillrow = (int) ( ((REAL)maxfill)/(M->n-i));
	  vec_filldrop(&lenr, tmpj, tmpa, fillrow, &queue);
	  maxfill -= lenr;
	}


      /**** 4 Create the new row in M ******/
      if(M->inarow != 1 && M->nnzrow[i] > 0)
	{
	  free(M->ja[i]);
	  free(M->ma[i]);
	}
      M->nnzrow[i] = lenr;

#ifdef ALLOC_COMPACT_ILUT
      if(lenr>0)
	{
	  M->ja[i] = tmpj;
	  M->ma[i] = tmpa;
	  nnzM += (long)lenr;
	}
      else
	{
	  M->ja[i] = NULL;
	  M->ma[i] = NULL;
	}
#else
      if(lenr>0)
	{
	  if(small==1)
	    {
	      M->ja[i] = tmpj;
	      M->ma[i] = tmpa;
	      tmpj += lenr;
	      tmpa += lenr;
	      nnzM += (long)lenr;
	    }
	  else
	    {
	      M->ja[i] = (int *)malloc(sizeof(int)*lenr);
	      memcpy(M->ja[i], tmpj, sizeof(int)*lenr);
	      M->ma[i] = (COEF *)malloc(sizeof(COEF)*lenr);
	      memcpy(M->ma[i], tmpa, sizeof(COEF)*lenr);
	    }
	}
#endif
    }

  if(fillrat > 0)
    queueExit(&queue);

  if(M->inarow == 1)
    {
      if(M->matab != NULL)
	free(M->matab);
      if(M->jatab != NULL)
	free(M->jatab);
      M->inarow = 0;
    }

  CS_SetNonZeroRow(M);
#ifndef ALLOC_COMPACT_ILUT
  if(small == 1)
#endif
    {
      matab = (COEF *)realloc(matab, sizeof(COEF)*nnzM);
      jatab = (int *)realloc(jatab, sizeof(int)*nnzM);
      M->matab = matab;
      M->jatab = jatab;
      nnzM = 0;
      for(k=0;k<M->nnzr;k++)
	{
	  i = M->nzrtab[k];
	  M->ja[i] = jatab+nnzM;
	  M->ma[i] = matab+nnzM;
	  nnzM += M->nnzrow[i];
	}
      M->inarow = 1;
    }

#ifdef DEBUG_M
  CS_Check(M, U->n);
#endif
}

/* @authors P. HENON */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <assert.h>
#include "phidal_common.h"
#include "queue.h"

int LDLk(csptr L, REAL normA, REAL epsilon)
{
  /******************************************************************************************/
  /* This function computes the LDLt(k) factorization of a sparse matrix                    */
  /* We assume that the sparse pattern of the matrix has been symbolically                  */
  /* factorized (the non zero pattern of the final factor has already been allocated        */
  /* == call to symb_fact)                                                                  */
  /* On entry :                                                                             */
  /*  L : a csc matrix that contains the lower triangular part of the matrix                */
  /* normA : the norm of matrix (used for static numerical pivoting)                        */
  /* epsilon : the tolerance that will be used in the accelerator (also used for pivoting)  */
  /* On return :                                                                            */
  /*  L : the factorized matrix                                                             */
  /*  the diagonal stores the inverse of the factorized matrix diagonal                     */
  /* NOTE:                                                                                  */
  /* This function can use any sparse pattern it is not optimized for a direct factorization*/
  /******************************************************************************************/


  /*** @@@@@@@@@@ OIMBE DOIT POUVOIR MIEUX CODER l'ajout des contribution (voir sparseTRSM)
       puisque on sait que l'on peut toujours ajouter dans la colonne distante EN DIRECT 
       l'algo actuelle peut traiter incomplete et complete factorization sparse pattern ****/

  dim_t j, k;
  int indi, indj;
  dim_t *jaColi, *jaColj;
  COEF *maColi, *maColj;
  int *Ljaptr;
  COEF *Lmaptr;
  dim_t nnzrow;
  int fnnzrow;
  REAL d;
  REAL thresh;

  /** Threshold for pivoting **/
  thresh = sqrt(epsilon)*normA;

  for(k=0;k<L->n;k++)
    {
      Ljaptr = L->ja[k];
      Lmaptr = L->ma[k];
      nnzrow = L->nnzrow[k];

      /** Static pivoting **/
      if(fabs(Lmaptr[0]) < thresh)
	{
	  fprintfd(stderr, "Pivot \n");
	  Lmaptr[0] = thresh;
	}
	
      Lmaptr[0] = 1.0/Lmaptr[0];
      

      /** Update lower triangular part L **/
      for(j=1;j<nnzrow;j++)
	{
	  jaColi = L->ja[ Ljaptr[j] ];
	  maColi = L->ma[ Ljaptr[j] ];
	  jaColj = Ljaptr + j;
	  maColj = Lmaptr + j;

	  fnnzrow = L->nnzrow[ (*jaColj) ];

	  d = -(*maColj)*Lmaptr[0];

	  indj = j;
	  indi = 0;
	  while(indi < fnnzrow && indj < nnzrow)
	    {
	      
	      while(indi < fnnzrow && (*jaColi) < (*jaColj) )
		{
		  jaColi++;
		  maColi++;
		  indi++;
		}

	      if(indi >= fnnzrow)
		break;

	      while(indj < nnzrow && (*jaColi) > (*jaColj) )
		{
		  jaColj++;
		  maColj++;
		  indj++;
		}
	      
	      if(indj >= nnzrow)
		break;
	      
	      if( (*jaColi) !=  (*jaColj))
		continue;
	      
#ifdef DEBUG_M
	      assert((*jaColi) ==  (*jaColj));
#endif


#ifndef WORSE
	      /** Much Better solution **/
	      while(indi < fnnzrow && indj < nnzrow && (*jaColi) ==  (*jaColj))
		{
		  (*maColi) += d* (*maColj);
		  jaColj++;
		  maColj++;
		  jaColi++;
		  maColi++;
		  indi++;
		  indj++;
		}
#else /** Worse **/
	      cur = 0;
	      while( (indi+cur < fnnzrow && indj+cur < nnzrow) && (jaColi[cur] ==  jaColj[cur]) )
		cur++;

	      DAXPY(cur, d, maColj, incx, maColi, incx);
	      
	      if(indi+cur < fnnzrow && indj+cur < nnzrow)
		{
		  indi+=cur;
		  indj+=cur;
		  jaColj += cur;
		  maColj += cur;
		  jaColi += cur;
		  maColi += cur;
		}
	      else
		break;
#endif
	    }

	  Lmaptr[j] = -d;
	}
      
    }

  CS_SetNonZeroRow(L);
  return 1;
}

/* @authors P. HENON */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include "phidal_common.h"
#include "type.h"

/** Local subfunctions **/
int partition(int *inttab, int start, int end, int pivot);
INTL partition64(INTL *inttab, INTL start, INTL end, INTL pivot);
int partition_node(int *nodelist, int start, int end, int pivot, int keysize, int *keyindex, int *key);
int partition_node_tags(int *nodelist, int start, int end, int pivot, int *tags, int tagnbr);
int compare_node_tags(dim_t node1, dim_t node2, int* tags, int tagnbr);

int partition_row(dim_t *ja, COEF *a, int start, int end, int pivot);
int partition_int(int *blocklist, int start, int end, int pivot, int *blocksize);

/**************************** QUICKSORT USED TO SORT AN ARRAY OF intEGER  ************/

void quicksort(int *inttab, int start, int end)
{
  /*----------------------------------------------------------------------
  / sort the node in inttab according to their size:
  / 
  / The sort is performed using a quicksort algorithm (see litterature)
  ------------------------------- ---------------------------------------*/
#ifdef ESSL_SORT
  dim_t n;
  n = end-start+1;
#ifdef DEBUG_M
  assert(n>0);
#endif
  isort(&(inttab[start]), 1, n);
#else
  if(start < end)
    {
      int boundary;
      
      boundary = partition(inttab, start, end, start);
      if(boundary == -1)
	return;
      quicksort(inttab, start, boundary-1);
      quicksort(inttab, boundary+1, end);
    }
#endif
  
}
      
int partition(int *inttab, int start, int end, int pivot)
{
  int up;
  int down;
  int tmp;
  int cursor;
  up = start;
  down = end;
  
  while(up<down)
    {
      
      /** Move UP to  the first node > PIVOT **/
      cursor = up;
      while( cursor <= end && inttab[cursor] <= inttab[pivot])
	cursor++;

      if(cursor <=end)
	up = cursor;
      else
	{
	  if(inttab[end] == inttab[pivot])
	    up = end;
	}

      /** Move DOWN to  the first node <= PIVOT **/
      cursor = down;
      while(cursor >= start  && inttab[cursor] > inttab[pivot] )
	cursor--;
      
      if(cursor >= start)
	down = cursor;


      if(up<down)
	{
	  /* Exchange up and down */
	  tmp = inttab[down];
	  inttab[down] = inttab[up];
	  inttab[up] = tmp;
	}
    }
      
  /*exchange value in down and pivindex */
  tmp = inttab[down];
  inttab[down] = inttab[pivot];
  inttab[pivot] = tmp;
  
  
  /** Test to eliminate array composed of identical element **/
  /** DO NOT NEED IF ARRAY ARE COMPOSED OF 2 by 2 DIFFERENT ELEMENT **/
  /*if(up == down && down == end)
    { 
      int flag = 1;
      for(cursor = start; cursor <= end; cursor++)
	if(inttab[cursor] != inttab[pivot])
	  {
	    flag = 0;
	    break;
	  }
      if(flag)
	return -1;
    }*/

  return down;
}




void quicksort64(INTL *inttab, INTL start, INTL end)
{
  /*----------------------------------------------------------------------
  / sort the node in inttab according to their size:
  / 
  / The sort is performed using a quicksort algorithm (see litterature)
  ------------------------------- ---------------------------------------*/
#ifdef ESSL_SORT
  INTL n;
  n = end-start+1;
#ifdef DEBUG_M
  assert(n>0);
#endif
  isort(&(inttab[start]), 1, n);
#else
  if(start < end)
    {
      int boundary;
      
      boundary = partition64(inttab, start, end, start);
      if(boundary == -1)
	return;
      quicksort64(inttab, start, boundary-1);
      quicksort64(inttab, boundary+1, end);
    }
#endif
  
}
      
INTL partition64(INTL *inttab, INTL start, INTL end, INTL pivot)
{
  INTL up;
  INTL down;
  INTL tmp;
  INTL cursor;
  up = start;
  down = end;
  
  while(up<down)
    {
      
      /** Move UP to  the first node > PIVOT **/
      cursor = up;
      while( cursor <= end && inttab[cursor] <= inttab[pivot])
	cursor++;

      if(cursor <=end)
	up = cursor;
      else
	{
	  if(inttab[end] == inttab[pivot])
	    up = end;
	}

      /** Move DOWN to  the first node <= PIVOT **/
      cursor = down;
      while(cursor >= start  && inttab[cursor] > inttab[pivot] )
	cursor--;
      
      if(cursor >= start)
	down = cursor;


      if(up<down)
	{
	  /* Exchange up and down */
	  tmp = inttab[down];
	  inttab[down] = inttab[up];
	  inttab[up] = tmp;
	}
    }
      
  /*exchange value in down and pivindex */
  tmp = inttab[down];
  inttab[down] = inttab[pivot];
  inttab[pivot] = tmp;
  
  
  /** Test to eliminate array composed of identical element **/
  /** DO NOT NEED IF ARRAY ARE COMPOSED OF 2 by 2 DIFFERENT ELEMENT **/
  /*if(up == down && down == end)
    { 
      int flag = 1;
      for(cursor = start; cursor <= end; cursor++)
	if(inttab[cursor] != inttab[pivot])
	  {
	    flag = 0;
	    break;
	  }
      if(flag)
	return -1;
    }*/

  return down;
}

/**************************** QUICKSORT USED TO SORT THE NODE ACCORDING TO THEIR KEYs ************/
void quicksort_node(int *nodelist, int start, int end, int keysize, int *keyindex, int *key)
{
  /*-------------------------------------
  / sort the node in nodelist according to their key:
  / If we denote by f the first label such that
  /   key1[f] != key2[f] then
  /   key1 <= key2  iff  key1[f] <= key2[f]
  / 
  / The sort is performed using a quicksort algorithm (see litterature)
  ---------------------------------------*/

  if(start < end)
    {
      int boundary;
      
      boundary = partition_node(nodelist, start, end, start, keysize, keyindex, key);
      if(boundary == -1)
	return;
      quicksort_node(nodelist, start, boundary-1, keysize, keyindex, key);
      quicksort_node(nodelist, boundary+1, end, keysize, keyindex, key);
    }
  return;
}
      
int partition_node(int *nodelist, int start, int end, int pivot, int keysize, int *keyindex, int *key)
{
  int up;
  int down;
  int tmp;
  int cursor;
  up = start;
  down = end;
  
  
  while(up<down)
    {
      
      /** Move UP to  the first node > UP **/
      cursor = up;
      while( cursor <= end
	    && compare_node(nodelist[cursor], nodelist[pivot], keysize, keyindex, key) <= 0 )
	cursor++;
      if(cursor <=end)
	up = cursor;
      else
	{
	  if(compare_node(nodelist[end], nodelist[pivot], keysize, keyindex, key) == 0)
	    up = end;
	}

      /** Move DOWN to  the first node <= DOWN **/
      cursor = down;
      while(cursor >= start  
	     && compare_node(nodelist[cursor], nodelist[pivot], keysize, keyindex, key) > 0)
	cursor--;
      if(cursor >= start)
	down = cursor;


      if(up<down)
	{
	  /* Exchange up and down */
	  tmp = nodelist[down];
	  nodelist[down] = nodelist[up];
	  nodelist[up] = tmp;
	}
    }
      
  /*exchange value in down and pivindex */
  tmp = nodelist[down];
  nodelist[down] = nodelist[pivot];
  nodelist[pivot] = tmp;
  
  /** Test to eliminate array composed of identical element **/
  /*if(up == down && down == end)
    { 
      int flag = 1;
      for(cursor = start; cursor <= end; cursor++)
	if(compare_node(nodelist[cursor], nodelist[pivot], keysize, keyindex, key) != 0)
	  {
	    flag = 0;
	    break;
	  }
      if(flag)
	return -1;
    }*/

  return down;
}


int compare_node(dim_t node1, dim_t node2, int keysize, int *keyindex, int *key)
{
  /*-------------------------------------------------
  / Compare two node according to their keys (composed
  /  by the list of their adjacent subdomains sorted in
  / ascending order:
  / node1 <= node2 iff (for the first f such that
  / key1[f] != key2[f], we have key1[f] <= key2[f]
  / Note: two node to be compared have the same 
  / number of adjacent subdomains (== same key length) 
  / ON ENTRY:
  / node1, node2 : the labels of the two nodes to compared
  / keysize      : size of the key (number of adjacent
  / keyindex     : keyindex[i] is the index of the node i key 
  /                in the key vector
  / key          : vector of the keys 
  / ON RETURN:
  / an int       : if <0 (node1 < node2
  /                if == 0 ( node1 == node2 )
  /                if > 0  ( node1 > node2  )             
  /--------------------------------------------------*/
  
  int f1;
  int f2;
  dim_t i;
  f1 = keyindex[node1];
  f2 = keyindex[node2];


#ifdef DEBUG_M
  assert(keyindex[node1+1]-keyindex[node1] == keysize);
  assert(keyindex[node2+1]-keyindex[node2] == keysize);
#endif
  
  for (i=0;i<keysize;i++)
    {
      if ( key[f1+i] != key[f2+i] )
	{
	  if(key[f1+i]>key[f2+i])
	    return 1;
	  else
	    return -1;
	}
    }
  return 0;

}
/******************** END OF QUICKSORT FOR NODE *****************************************/

/**************************** QUICKSORT USED TO SORT THE NODE ACCORDING TO THEIR TAGS ************/
void quicksort_node_tags(int *nodelist, int start, int end, int* tags, int tagnbr)
{
  if(start < end)
    {
      int boundary;
      
      boundary = partition_node_tags(nodelist, start, end, start, tags, tagnbr);
      if(boundary == -1)
	return;
      quicksort_node_tags(nodelist, start, boundary-1, tags, tagnbr);
      quicksort_node_tags(nodelist, boundary+1, end, tags, tagnbr);
    }
  return;
}
      
int partition_node_tags(int *nodelist, int start, int end, int pivot, int *tags, int tagnbr)
{
  int up;
  int down;
  int tmp;
  int cursor;
  up = start;
  down = end;
  
  
  while(up<down)
    {
      
      /** Move UP to  the first node > UP **/
      cursor = up;
      while( cursor <= end
	    && compare_node_tags(nodelist[cursor], nodelist[pivot], tags, tagnbr) <= 0 )
	cursor++;
      if(cursor <=end)
	up = cursor;
      else
	{
	  if(compare_node_tags(nodelist[end], nodelist[pivot], tags, tagnbr) == 0)
	    up = end;
	}

      /** Move DOWN to  the first node <= DOWN **/
      cursor = down;
      while(cursor >= start  
	     && compare_node_tags(nodelist[cursor], nodelist[pivot], tags, tagnbr) > 0)
	cursor--;
      if(cursor >= start)
	down = cursor;


      if(up<down)
	{
	  /* Exchange up and down */
	  tmp = nodelist[down];
	  nodelist[down] = nodelist[up];
	  nodelist[up] = tmp;
	}
    }
      
  /*exchange value in down and pivindex */
  tmp = nodelist[down];
  nodelist[down] = nodelist[pivot];
  nodelist[pivot] = tmp;
  
  /** Test to eliminate array composed of identical element **/
  /*if(up == down && down == end)
    { 
      int flag = 1;
      for(cursor = start; cursor <= end; cursor++)
	if(compare_node(nodelist[cursor], nodelist[pivot], keysize, keyindex, key) != 0)
	  {
	    flag = 0;
	    break;
	  }
      if(flag)
	return -1;
    }*/

  return down;
}


int compare_node_tags(dim_t node1, dim_t node2, int* tags, int tagnbr)
{
  /*-------------------------------------------------
  / Compare two node according to their tags             
  /--------------------------------------------------*/
  
  int f1;
  int f2;
  dim_t i;

  f1 = node1*tagnbr;
  f2 = node2*tagnbr;
 
  for (i=0;i<tagnbr;i++)
    {
      if ( tags[f1+i] != tags[f2+i] )
	{
	  if(tags[f1+i]>tags[f2+i])
	    return 1;
	  else
	    return -1;
	}
    }
  return 0;
}


/******************** END OF QUICKSORT FOR NODE/TAGS ***************************************/


/**************************** QUICKSORT FOR AN ARRAY OF int WITH intERGER KEYS  ************/
 
void quicksort_int(int *nodelist, int start, int end, int *noderank)
{
  /*-------------------------------------
  / sort the node in nodelist according to their rank noderank[nodelist[i]]
  / 
  / The sort is performed using a quicksort algorithm (see litterature)
  ---------------------------------------*/

  if(start < end)
    {
      int boundary;
      
      boundary = partition_int(nodelist, start, end, start, noderank);
      if(boundary == -1)
	return;
      quicksort_int(nodelist, start, boundary-1, noderank);
      quicksort_int(nodelist, boundary+1, end, noderank);
    }
}
      
int partition_int(int *nodelist, int start, int end, int pivot, int *noderank)
{
  /** @@OIMBE: finalement le long sert pas ... **/


  int up;
  int down;
  int tmp;
  int cursor;
  up = start;
  down = end;
  
  
  while(up<down)
    {
      
      /** Move UP to  the first node > UP **/
      cursor = up;
      while( cursor <= end
	     && noderank[nodelist[cursor]] <= noderank[nodelist[pivot]])
	cursor++;
      if(cursor <=end)
	up = cursor;
      else
	{
	  if(noderank[nodelist[end]] == noderank[nodelist[pivot]])
	    up = end;
	}

      /** Move DOWN to  the first node <= DOWN **/
      cursor = down;
      while(cursor >= start  
	     && noderank[nodelist[cursor]] > noderank[nodelist[pivot]] )
	cursor--;
      if(cursor >= start)
	down = cursor;


      if(up<down)
	{
	  /* Exchange up and down */
	  tmp = nodelist[down];
	  nodelist[down] = nodelist[up];
	  nodelist[up] = tmp;
	}
    }
      
  /*exchange value in down and pivindex */
  tmp = nodelist[down];
  nodelist[down] = nodelist[pivot];
  nodelist[pivot] = tmp;
  
  /** Test to eliminate array composed of identical element **/
  /*if(up == down && down == end)
    { 
      int flag = 1;
      for(cursor = start; cursor <= end; cursor++)
	if(noderank[nodelist[cursor]] !=  noderank[nodelist[pivot]])
	  {
	    flag = 0;
	    break;
	  }
      if(flag)
	return -1;
    }*/
  
  return down;
}
/******************** END OF QUICKSORT FOR ARRAY OF int  *****************************************/

/**************************** QUICKSORT FOR AN ROW IN CSR MATRIX  ************/

void quicksort_row(dim_t *ja, COEF *a, int start, int end)
{
  /*-------------------------------------
  / sort the node in ja according to their size:
  / 
  / The sort is performed using a quicksort algorithm (see litterature)
  ---------------------------------------*/
  if(start < end)
    {
      int boundary;
      
      boundary = partition_row(ja, a, start, end, start);
      /*if(boundary == -1)
	return iter;*/
      quicksort_row(ja, a, start, boundary-1);
      quicksort_row(ja, a, boundary+1, end);
    }
}
      
int partition_row(dim_t *ja, COEF *a, int start, int end, int pivot)
{
  int up;
  int down;
  int tmp;
  COEF tmpd;
  int cursor;
  up = start;
  down = end;
  
  
  while(up<down)
    {
      
      /** Move UP to  the first node > UP **/
      cursor = up;
      while( cursor <= end
	    && ja[cursor] <= ja[pivot])
	cursor++;
      if(cursor <=end)
	up = cursor;
      else
	{
	  if(ja[end] == ja[pivot])
	    up = end;
	}

      /** Move DOWN to  the first node <= DOWN **/
      cursor = down;
      while(cursor >= start  
	     && ja[cursor] > ja[pivot] )
	cursor--;
      if(cursor >= start)
	down = cursor;


      if(up<down)
	{
	  /* Exchange up and down */ /*TODO : SWAP sur place*/
	  tmp = ja[down];
	  ja[down] = ja[up];
	  ja[up] = tmp;

	  tmpd = a[down];
	  a[down] = a[up];
	  a[up] = tmpd;

	}
    }
      
  /*exchange value in down and pivindex */
  tmp = ja[down];
  ja[down] = ja[pivot];
  ja[pivot] = tmp;
  
  tmpd = a[down];
  a[down] = a[pivot];
  a[pivot] = tmpd;


  return down;
}
/******************** END OF QUICKSORT FOR A ROW  *****************************************/

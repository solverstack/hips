/* @authors P. HENON */

#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <stdio.h>

#include "phidal_common.h"


void CSCrowICCprod(REAL droptol, REAL *droptab, REAL fillrat, dim_t nnb, csptr *X, csptr l, COEF *D, int *wki1, int *wki2, COEF *wkd, cell_int *celltab, cell_int **cellptrtab)
{
  /*************************************************************/
  /* This function performs l = l - X.D.Xt                     */
  /* X is a row of CSC matrices                                */
  /* l is a CSC lower triangular matrix                        */
  /*                                                           */
  /* On return:                                                */
  /* l the result                                              */
  /* X = X.D and X is sorted by row indices                    */
  /*************************************************************/
  csptr x;

  if(nnb == 0)
    return;
  
  /**** We compact X in a single CSC matrix ******/
  x = (csptr)malloc(sizeof(struct SparRow));
  CS_VirtualMerge(nnb, X, x);

  CS_ICCprod(x, l, D, droptol, droptab, fillrat, wki1, wki2, wkd, celltab, cellptrtab); /** UPDATED ROW STRUCT IS HERE **/

  /** Reinit the pointer of x and y to no delete them in cleanCS **/
  /*bzero(x->nnzrow, sizeof(int)*x->n);
    bzero(x->ja, sizeof(int *)*x->n);
    bzero(x->ma, sizeof(COEF *)*x->n);
    cleanCS(x);*/

    /** Destroy x : DO NOT USE cleanCS (it uses a lot of time in this case) **/
  free(x->nnzrow);
  free(x->ja);
  free(x->ma);
  free(x->nzrtab);
  free(x->inzrtab);
  free(x);
}

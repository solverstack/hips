/* @authors P. HENON */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <assert.h>

#include "phidal_common.h"


long SF_GSurow(csptr A, flag_t level, csptr P)
{
  /***********************************************************************************************/
  /* This function computes the non zero pattern of the levelized incomplete factor              */
  /* for a sparse lower triangular                                                               */
  /* matrix in CSC format.  This pattern is exact iff the matrix has a SYMMETRIC non zero        */
  /* structure.                                                                                  */
  /* On entry:                                                                                   */
  /* A : pointer to the matrix                                                                   */
  /* level : level desired for the ilu(k) factorization                                          */
  /* P   : an empty csr matrix (initialized with dimension n)                                    */
  /* On return:                                                                                  */
  /*     P : a csr matrix containing the non zero pattern of the factorized matrix               */
  /*         the memory for the numerical values is allocated and initialized to zero            */ 
  /*     The total number of nnz in the lower part is returned                                   */
  /*                                                                                             */
  /* NOTE:                                                                                       */
  /*   1) This algorithm has been implemented according to the paper of David Hysom and          */
  /*     Alex Pothen : Level-based Incomplete LU factorization: Graph Model and Algorithm        */ 
  /***********************************************************************************************/
  int *visited;
  int *length;
  int *stack;
  int *adj;
  dim_t *ja;
  int used;
  dim_t h, i,j,k, t;
  long nnz;
  int lengthmax;

  lengthmax = 0;

  if(A->n == 0)
    return 0;
  /** Allocated the working array **/
  visited =  (int *)malloc(sizeof(int)*A->n);
  length  =  (int *)malloc(sizeof(int)*A->n);
  stack   =  (int *)malloc(sizeof(int)*A->n);
  ja = (int *)malloc(sizeof(int)* A->n);
  nnz = 0;

  /** Initialized visited ***/
  for(j=0;j<A->n;j++)
    {
      visited[j] = -1;
      length[j] = 0;
    }
 

  /** Apply GS_Urow for each row **/
  for(i=0;i<A->n;i++)
    {
      

      used = 0; /** Reset the stack number of elements **/
      stack[0] = i;
      used++;
      length[i] = 0;
      visited[i] = i;

      ja[0] = i;
      k=1;
      /** BFS phase **/
      while(used > 0)
	{
	  
	  used--;
	  h = stack[used];
	  adj = A->ja[h];
	  for(j=0;j<A->nnzrow[h];j++)
	    {
	      t = adj[j];
	      if(visited[t] != i)
		{
		  visited[t] = i;
		  if(t<i && length[h] < level)
		    {

		      stack[used] = t;
		      used++;
		      length[t] = length[h]+1;
		      if(length[t] > lengthmax)
			lengthmax = length[t];
		    }
		  if(t>i)
		    {
		      ja[k++] = t;
		    }
		}
	    
	    }
	}
      
      

      /*** allocate the new row and fill it ***/
      P->nnzrow[i] = k;
      nnz += k;
      if(k>0)
	{
	  P->ja[i] = (int *)malloc(sizeof(int)*k);
	  memcpy(P->ja[i], ja, sizeof(int)*k);
	  P->ma[i] = (COEF *)calloc(sizeof(COEF), sizeof(COEF)*k);
	}
    }


  free(ja);
  free(visited);
  free(length);
  free(stack);

  


  return nnz;
}



long SF_level(flag_t job, csptr A, flag_t level, csptr P)
{
  /***********************************************************************************************/
  /* This function computes the non zero pattern of the levelized incomplete factor              */
  /* for a sparse lower triangular                                                               */
  /* matrix in CSC format.  This pattern is exact iff the matrix has a SYMMETRIC non zero        */
  /* structure.                                                                                  */
  /* On entry:                                                                                   */
  /* job : = 0 alloc the matrix ; =1 fill the indices of the sparse pattern                      */
  /* A : pointer to the matrix                                                                   */
  /* level : level desired for the ilu(k) factorization                                          */
  /* P   : an empty csr matrix (initialized with dimension n)                                    */
  /* On return:                                                                                  */
  /*     P : a csr matrix containing the non zero pattern of the factorized matrix               */
  /*         the memory for the numerical values is allocated and initialized to zero            */ 
  /*     The total number of nnz in the lower part is returned                                   */
  /*                                                                                             */
  /* NOTE:                                                                                       */
  /*   1) This algorithm has been implemented according to the paper of David Hysom and          */
  /*     Alex Pothen : Level-based Incomplete LU factorization: Graph Model and Algorithm        */ 
  /***********************************************************************************************/
  int *visited;
  int *length;
  int *stack;
  int *adj;
  dim_t *ja;
  int used;
  dim_t h, i,j,k, t;
  long nnz;
  int lengthmax;

  lengthmax =0;
  if(A->n == 0)
    return 0;
  /** Allocate the working array **/
  visited =  (int *)malloc(sizeof(int)*A->n);
  length  =  (int *)malloc(sizeof(int)*A->n);
  stack   =  (int *)malloc(sizeof(int)*A->n);
  ja = (int *)malloc(sizeof(int)* A->n);
  nnz = 0;

  /** Initialize visited ***/
  for(j=0;j<A->n;j++)
    {
      visited[j] = -1;
      length[j] = 0;
    }
 

  /** Apply GS_Urow for each row **/
  for(i=0;i<A->n;i++)
    {
      used = 0; /** Reset the stack number of elements **/
      stack[0] = i;
      used++;
      length[i] = 0;
      visited[i] = i;

      ja[0] = i;
      k=1;
      /** BFS phase **/
      while(used > 0)
	{
	  
	  used--;
	  h = stack[used];
	  adj = A->ja[h];
	  for(j=0;j<A->nnzrow[h];j++)
	    {
	      t = adj[j];
	      if(visited[t] != i)
		{
		  visited[t] = i;
		  if(t<i && length[h] < level)
		    {

		      stack[used] = t;
		      used++;
		      length[t] = length[h]+1;
		      /*if(length[t] > lengthmax)
			lengthmax = length[t];*/

		    }
		  if(t>i)
		    {
		      ja[k++] = t;
		    }
		}
	    
	    }
	}

      /*** allocate the new row and fill it ***/
      if(job == 0)
	P->nnzrow[i] = k;
      else
	{
	  P->ja[i] = P->ja[0] + nnz;
	  memcpy(P->ja[i], ja, sizeof(int)*k);
	  P->ma[i] = P->ma[0] + nnz;
	}
      nnz += k;
    }
  free(ja);
  free(visited);
  free(length);
  free(stack);

  /*fprintfv(5, stdout, "LEVEL FILL MAXIMUM %ld \n", (long) lengthmax);*/

  if(job == 0)
    {
      fprintfd(stderr, "NNZ in the triangular factor L = %ld \n", nnz);
      P->inarow = 1;
      P->jatab = (int *)malloc(sizeof(int)*nnz);
      P->ja[0] = P->jatab;
      P->matab = (COEF *)malloc(sizeof(COEF)*nnz);
      P->ma[0] = P->matab;
      assert(P->ja[0] != NULL);
      assert(P->ma[0] != NULL);

      bzero(P->ma[0], sizeof(COEF)*nnz);

   
      
    }

  CS_SetNonZeroRow(P);
 

  return nnz;
}

/* @authors P. HENON */

#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <math.h>
#include <stdio.h>

#include "phidal_common.h"
#include "queue.h"

void CSCxCSR_CSC_GEMMT(COEF alpha, csptr A, csptr B, csptr C, int Cnrow, REAL droptol, REAL *droptab, REAL fillrat, int *wki1, int *wki2, COEF *wkd, cell_int *celltab, cell_int **BcolList)
{
  /*********************************************************************/
  /* this function does C = C + alpha.AxB                              */
  /* with a dropping criterion droptol*droptab[i] in each column       */
  /* if droptab == NULL then it is considered as unitary               */
  /* A is a CSC matrix                                                 */
  /* B is a CSR matrix SORTED BY COLUMN INDEX                          */
  /* M is a CSC matrix                                                 */ 
  /* NOTE: B IS SORTED BY column indices                               */
  /*********************************************************************/

  dim_t i, j, k;
  cell_int *c, *next;
  cell_int **l;
  int jcol, jpos;
  int *jrev, *tmpj;  
  COEF *tmpa;
  int *colja;
  COEF *colma;
  REAL d;
  COEF s;
  int len, len2;
  REAL maxfill=-1;
  flag_t fillrow;
  Queue queue;
  long nnzC;
#ifdef ALLOC_COMPACT_ILUT
  int memsize;
#else
  int small = 0;
#endif
  dim_t *jatab = NULL;
  COEF *matab = NULL;


  /** DO NOT TEST B->nnzr or C->nnzr they came from a virtual merge !! */
  
#ifdef DEBUG_M
  assert(A->n == B->n);
#endif
  
#ifdef ALLOC_COMPACT_ILUT
  memsize = MAX((int)(CSnnz(C)*ALLOC_COMPACT_INIT), Cnrow);
  jatab = (int *)malloc(sizeof(int)*memsize);
  matab = (COEF *)malloc(sizeof(COEF)*memsize);
#else
  small = 0;
  if((REAL)C->n*(REAL)Cnrow <= (REAL)SMALLBLOCK)
    {
      small = 1;
      jatab = (int *)malloc(sizeof(int)*(C->n*Cnrow));
      matab = (COEF *)malloc(sizeof(COEF)*(C->n*Cnrow));
    }
#endif

#ifdef DEBUG_M
  /*NE PAS faire de CS_Check sur A et B CA A CAUSE du RowCompact 
    (Pas de structre non zero rows)
  CS_Check(A, Cnrow);  
  CS_Check(B, C->n);*/

  CS_Check(C, Cnrow);

  /** Check if B is sorted by column indices **/
  for(i=0;i<B->n;i++)
    {
      for(k=1;k<B->nnzrow[i];k++)
	assert(B->ja[i][k] > B->ja[i][k-1]);
    }
#endif

  if(fillrat > 0)
    {
      k = 0;
      for(i=0;i<C->n;i++)
	if(C->nnzrow[i] > k)
	  k = C->nnzrow[i];
      queueInit(&queue, (int)((k+1)*fillrat));
      nnzC = CSnnz(C);
      maxfill = nnzC*fillrat;
    }

  bzero(BcolList, sizeof(cell_int *)*C->n);
  
  /** Init the cells **/
  for(j=0;j<B->n;j++)
    {
      celltab[j].elt = j;
      celltab[j].nnz = B->nnzrow[j];
#ifdef DEBUG_M
      assert(A->nnzrow[j] > 0 && B->nnzrow[j] > 0);
#endif
      celltab[j].ja = B->ja[j];
      celltab[j].ma = B->ma[j];

      jcol = B->ja[j][0];
      
      l = BcolList+jcol;
      c = celltab+j;
      c->next = *l;
      *l = c;
    }

			 
  jrev = wki1;
			 
#ifdef ALLOC_COMPACT_ILUT
   tmpj = jatab;
   tmpa = matab;
#else
  if(small==1)
    {
      tmpj = jatab;
      tmpa = matab;
    }
  else
    {
      tmpj = wki2;
      tmpa = wkd;
    }
#endif

  for(j=0;j<Cnrow;j++) 
    jrev[j] = -1;
  /*memset(jrev, -1, sizeof(int)*Cnrow);*/

  
  nnzC = 0;
  for(k=0;k<C->n;k++) 
    {
      /***************************************/
      /* Treat col k of C                    */
      /***************************************/
#ifdef ALLOC_COMPACT_ILUT
      if(nnzC + Cnrow > memsize)
	{
	  memsize = MAX(memsize*ALLOC_COMPACT_RATIO, (nnzC+Cnrow));
	  jatab = (int *)realloc(jatab, sizeof(int)*memsize);
	  matab = (COEF *)realloc(matab, sizeof(COEF)*memsize);
	  assert(jatab != NULL);
	  assert(matab != NULL);
	}
      tmpj = jatab + nnzC;
      tmpa = matab + nnzC;
#endif

      /** Unpack col k of C **/
#ifdef DEBUG_M
      assert(C->nnzrow[k] <= Cnrow);
#endif

      len = 0;
      l = BcolList+k;
      c = *l;
      while(c!=NULL)
	{
	  next = c->next; /** chainage modifie c->next so we must keep record of it **/
	  j = c->elt;
	  s = alpha* (*c->ma);
	  
	  /** Update the next columns lists **/
	  c->nnz--;
	  if(c->nnz > 0)
	    {
	      c->ja++;
	      c->ma++;
	      
	      jcol = *(c->ja);
#ifdef DEBUG_M
	      assert(jcol>k);
#endif
	      l = BcolList+jcol;
	      c->next = *l;
	      *l = c;
	    }
	  c = next;


	  colja = A->ja[j];
	  colma = A->ma[j];
	  
	  for(i=0;i<A->nnzrow[j];i++)
	    {
	      jcol = colja[i];
	      jpos = jrev[jcol];
	      
	      if(jpos >= 0)
		tmpa[jpos] += s*colma[i];
	      else
		{
		  /** New entry in col k of C **/
		  jrev[jcol] = len;
		  tmpj[len] = jcol;
		  tmpa[len] = s*colma[i];
		  len++;
		}
	    }
	}


      /*** Do these steps in the right order ! ****/	   
      /*** Add row k of C in tmp row **/
      len2 = len;
      colja = C->ja[k];
      colma = C->ma[k];
      for(j=0;j<C->nnzrow[k];j++)
	{
	  jcol = colja[j];
	  jpos = jrev[jcol];
	  if(jpos >=0)
	    tmpa[jpos] += colma[j];
	  else
	    {
	      tmpj[len2] = jcol;
	      tmpa[len2] = colma[j];
	      len2++;
	    }
	}


      /**** 1 Reinit jrev ****/ /** DO IT HERE !! **/
      for(i=0;i<len;i++) 
	jrev[tmpj[i]] = -1;

      len = len2;

      /**** 3 Dropping in C col k ******/
      if(droptab != NULL)
	d = droptab[k]*droptol;
      else
	d = droptol;
      
      jpos = 0;
      for(i=0;i<len;i++) 
	if(coefabs(tmpa[i])> d)
	  {
	    tmpj[jpos] = tmpj[i];
	    tmpa[jpos] = tmpa[i];
	    jpos++;
	  }
      len = jpos;
	  
      /**** 4 Keep only the largest entries in col k of C  according to fillrat ***/
      if(fillrat > 0)
	{
	  fillrow = (int)( (REAL)(maxfill)/(C->n-k));
	  vec_filldrop(&len, tmpj, tmpa, fillrow, &queue);
	  maxfill -= len;
	}
	  
	 
	  
      /******* 5 Create the new row in C       ******/
      /* Do not use a realloc it is worst (tested) */
      if(C->inarow != 1 && C->nnzrow[k] > 0)
	{
	  free(C->ja[k]);
	  free(C->ma[k]);
	}
#ifdef ALLOC_COMPACT_ILUT
      nnzC += (long)len;
#else
      if(len > 0)
	{
	  if(small==1)
	    {
	      C->ja[k] = tmpj;
	      C->ma[k] = tmpa;
	      tmpj += len;
	      tmpa += len;
	      nnzC += (long)len;
	    }
	  else
	    {
	      C->ja[k] = (int *)malloc(sizeof(int)*len);
	      memcpy(C->ja[k], tmpj, sizeof(int)*len);
	      C->ma[k] = (COEF *)malloc(sizeof(COEF)*len);
	      memcpy(C->ma[k], tmpa, sizeof(COEF)*len);
	    }
	}
#endif

      C->nnzrow[k] = len;
    }



  if(fillrat > 0)
    queueExit(&queue);	

  if(C->inarow == 1)
    {
      if(C->matab != NULL)
	free(C->matab);
      if(C->jatab != NULL)
	free(C->jatab);
      C->inarow = 0;
    }

  CS_SetNonZeroRow(C);


#ifndef ALLOC_COMPACT_ILUT
  if(small == 1)
#endif
    {
      jatab = (int *)realloc(jatab, sizeof(int)*nnzC);
      matab = (COEF *)realloc(matab, sizeof(COEF)*nnzC);
      
      C->matab = matab;
      C->jatab = jatab;
      /** On some system (AIX), realloc can move the memory even if the realloc
	  shrink the allocated ptr size **/
      nnzC = 0;
      for(k=0;k<C->nnzr;k++)
	{
	  i = C->nzrtab[k];
	  C->ja[i] = jatab+nnzC;
	  C->ma[i] = matab+nnzC;
	  nnzC += C->nnzrow[i];
	}
      C->inarow = 1;
    }


#ifdef DEBUG_M
  CS_Check(C, Cnrow);
#endif


}


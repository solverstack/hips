/* @authors P. HENON */

#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <math.h>
#include <stdio.h>

#include "queue.h"
#include "phidal_common.h"



void CSR_Lsol(flag_t unitdiag, csptr mata, COEF *b, COEF *x)
{
/*---------------------------------------------------------------------
| This function does the forward solve L x = b.
| if unitdiag == 1 then the diagonal is considered to be unitary (stored or not stored)
| Can be done in place.
|----------------------------------------------------------------------
| on entry:
| mata  = the matrix (in SparRow form)
| b     = a vector
| unitdiag == 1 then the diagonal is considered to be unitary (stored or not stored)
| on return
| x     = the solution of L x = b 
|--------------------------------------------------------------------*/
/*   local variables    */
   dim_t i, j, k;
   COEF *kr;
   int *ki=NULL;

   if( x != b)
     memcpy(x, b, sizeof(COEF)*mata->n);

   if(unitdiag == 1)
     {
       for(j=0;j<mata->nnzr;j++)
	 {
	   i = mata->nzrtab[j];
	   kr = mata->ma[i];
	   ki = mata->ja[i];
	   for (k=0; k<mata->nnzrow[i]-1; k++)
	     x[i] -= kr[k]*x[ki[k]];
	   if(ki[k] != i)
	     x[i] -= kr[k]*x[ki[k]];
	 }
     }
   else
     for (i=0; i<mata->n; i++) 
       {
#ifdef DEBUG_M
	 assert(mata->nnzrow[i]>=1);
#endif
	 kr = mata->ma[i];
	 ki = mata->ja[i];
	 for (k=0; k<mata->nnzrow[i]-1; k++)
	   x[i] -= kr[k]*x[ki[k]];
#ifdef DEBUG_M
	 assert(ki[k] == i);
#endif
	 x[i] *= kr[k];
       }
   
   return;
}



void CSC_Lsol(flag_t unitdiag, csptr mata, COEF *b, COEF *x)
{
/*---------------------------------------------------------------------
| This function does the forward solve L x = b.
|
| L is a lower triangular matrix in CSC format that  
| 
| Can be done in place.
|----------------------------------------------------------------------
| on entry:
| mata  = the matrix (in SparRow form)
| b     = a vector
| unitdiag == 1 then the diagonal is considered to be unitary (stored or not stored)
| on return
| x     = the solution of L x = b 
|--------------------------------------------------------------------*/
  dim_t i, j, k;
  int *ki=NULL;
  COEF *kr=NULL;
  COEF d;

  if(x != b)
    memcpy(x, b, sizeof(COEF)*mata->n);
  if(unitdiag == 1)
    for(j=0;j<mata->nnzr;j++)
      {
	i = mata->nzrtab[j];
	d = x[i];
	ki = mata->ja[i];
	kr = mata->ma[i];
	if(ki[0] != i)
	  x[ki[0]] -= d*kr[0];
	for(k=1;k<mata->nnzrow[i];k++)
	  x[ki[k]] -= d*kr[k];
      }
  else
    for(i=0;i<mata->n;i++)
      {
#ifdef DEBUG_M
	assert(mata->nnzrow[i]>=1);
	assert(ki[0] == i);
#endif
	x[i] *= kr[0];
	
	d = x[i];
	ki = mata->ja[i];
	kr = mata->ma[i];
	for(k=1;k<mata->nnzrow[i];k++)
	  x[ki[k]] -= d*kr[k];
	
      }
}


void CSR_Usol(flag_t unitdiag, csptr mata, COEF *b, COEF *x)
{
/*---------------------------------------------------------------------
| This function does the backward solve U x = b.
| Can be done in place.
|----------------------------------------------------------------------
| on entry:
| mata  = the matrix (in SparRow form)
| b    = a vector
|
| on return
| x     = the solution of U * x = b 
|
|---------------------------------------------------------------------*/
/*   local variables    */
   int i, j, k, *ki;
   COEF *kr;

   if(x != b)
     memcpy(x, b, sizeof(COEF)*mata->n);

   if(unitdiag == 1)
     /*for (i=mata->n-1; i>=0; i--) */
     for(j=mata->nnzr-1;j>=0;j--)
       {
	 i = mata->nzrtab[j];
	 kr = mata->ma[i];
	 ki = mata->ja[i];
	 
	 for (k=1; k<mata->nnzrow[i]; k++)
	   x[i] -=kr[k] * x[ki[k]];
	     
	 if(ki[0]!=i)
	   x[i] -=kr[0] * x[ki[0]];
       }
   else
     for (i=mata->n-1; i>=0; i--) 
       {
	 kr = mata->ma[i];
	 ki = mata->ja[i];

	 for (k=1; k<mata->nnzrow[i]; k++)
	   x[i] -=kr[k] * x[ki[k]];

#ifdef DEBUG_M
	 assert(mata->nnzrow[i]>0);
	 assert(ki[0] == i);
#endif
	 x[i] *= kr[0];
     }

}

void CSC_Usol(flag_t unitdiag, csptr mata, COEF *b, COEF *x)
{
/*---------------------------------------------------------------------
| This function does the backward solve U x = b.
| Can be done in place.
|----------------------------------------------------------------------
| on entry:
| mata  = the matrix (in SparRow form)
| b    = a vector
|
| on return
| x     = the solution of U * x = b 
|
|---------------------------------------------------------------------*/
  dim_t i,j,k;
  int *ki;
  COEF *kr;
  COEF d;
  
  if(x != b)
    memcpy(x, b, sizeof(COEF)*mata->n);

  if(unitdiag == 1)
    /*for(i=mata->n-1;i>=0;i--)*/
    for(j=mata->nnzr-1;j>=0;j--)
      {
	i = mata->nzrtab[j];
	d = x[i];
	ki = mata->ja[i];
	kr = mata->ma[i];
	
	for(k=0;k<mata->nnzrow[i]-1;k++)
	  x[ki[k]] -= d*kr[k];
	
	if(ki[k] != i)
	  x[ki[k]] -= d*kr[k];
      }
  else
    for(i=mata->n-1;i>=0;i--)
      {

	d = x[i];
	ki = mata->ja[i];
	kr = mata->ma[i];
	

	for(k=0;k<mata->nnzrow[i]-1;k++)
	  x[ki[k]] -= d*kr[k];

#ifdef DEBUG_M
	assert(mata->nnzrow[i]>0);
	assert(ki[k] == i);
#endif
	x[i] *= kr[k];

       
      }
}

/*---------------end of Lsol-----------------------------------------


----------------------------------------------------------------------*/
void CSC_Ltsol(flag_t unitdiag, csptr mata, COEF *b, COEF *x)
{
  CSR_Usol(unitdiag, mata, b, x);
}




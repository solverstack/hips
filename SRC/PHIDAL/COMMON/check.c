/* @authors P. HENON */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>

#include "phidal_common.h"

int checkBlock(csptr mata, int r, int s);
int checkL(csptr mata);
int checkU(csptr mata);
int PhidalHID_Check(PhidalHID *BL, mpi_t proc_id);
int matrix_equality(csptr A, csptr B);

#undef free
#undef malloc
#undef realloc

void *MonMalloc(size_t size)
{
  void *ptr;
  ptr = malloc(size);
  if(ptr == NULL)
    {
      fprintfd(stderr, "ERROR: Unable to allocate %ld bytes \n", (long)size);
      assert(0);
    }
  return ptr;
}


void *MonRealloc(void *ptr, size_t size)
{
  
  ptr = realloc(ptr, size);
 
  if(ptr == NULL && size > 0)
    {
      fprintfd(stderr, "ERROR: Unable to reallocate %ld bytes \n", (long)size);
      assert(0);
    }
  return ptr;
}

void MonFree(void *ptr)
{

  if(ptr == NULL)
    fprintfd(stderr, "Warning try to free a null pointer \n");

  assert(ptr != NULL);

  free(ptr);
}




void CS_Check(csptr m, int dim2)
{
  dim_t i, j;
  dim_t nnzr;
  nnzr = 0;
  for(i=0;i<m->n;i++)
    {
      assert(m->nnzrow[i] <= dim2);
      if(m->nnzrow[i]>0)
	{
	  nnzr++;
	  assert(m->nzrtab[m->inzrtab[i]] == i);
	}

      for(j=1;j<m->nnzrow[i];j++)
	assert(m->ja[i][j] != m->ja[i][j-1]);

      for(j=0;j<m->nnzrow[i];j++)
	{
	  assert(m->ja[i][j] >= 0);
	  assert(m->ja[i][j] < dim2);
	}
    }

  assert(nnzr == m->nnzr);
  for(i=0;i<m->nnzr;i++)
    {
      assert(m->nnzrow[m->nzrtab[i]]>0);
      assert(m->ja[m->nzrtab[i]] != NULL);
    }
  for(i=0;i<m->n;i++)
    if(m->inzrtab[i] < m->nnzr)
      assert(m->nzrtab[m->inzrtab[i]]>=i);
}


void set_diagonaly_dominance(csptr mat)
{
  dim_t i, j;
  int flag;
  
  fprintfd(stdout, "SET DIAGONAL DOMINANCE IN MATRIX \n");

  for(i=0;i<mat->n;i++)
    {
      flag = 0;
      for(j=0;j<mat->nnzrow[i];j++)
	{
	  if(mat->ja[i][j] == i)
	    {
	      mat->ma[i][j] += (COEF)mat->n;
	      flag =1;
	      break;
	    }
	  
	}
      if(flag == 0)
	fprintfd(stderr, "No diagonal term on row %ld \n", (long)i);
    }
}

 


int checkBlock(csptr mata, int r, int s)
{
  dim_t i, g;
  int flag = 0;

  if(mata == NULL)
    return 0;

  
  if(mata->n != r)
    {
      fprintfd(stderr, "ERROR IN BLOCK\n");
      fprintfd(stderr, "Matrix should have %ld rows and have %ld rows \n", (long)r, (long)mata->n);
      return -1;
    }
  
  for(i=0;i<mata->n;i++)
    {
      if(mata->nnzrow[i] > s || mata->nnzrow[i]<0 )
	{
	  fprintfd(stderr, "ERROR IN BLOCK \n");
	  fprintfd(stderr, "Row[%ld] has %ld elts \n", (long)i, (long)mata->nnzrow[i]);
	  flag = 1;
	}
      for(g=0;g<mata->nnzrow[i];g++)
	{
	  if(mata->ja[i][g] >= s || mata->ja[i][g] < 0)
	    {
	      fprintfd(stderr, "ERROR IN BLOCK \n");
	      fprintfd(stderr, "Row[%ld] ja[%ld] = %ld where dim2 = %ld\n",(long) i, (long)g, (long) mata->ja[i][g], (long)s);
	      flag = 1;
	    }
	  if(mata->ma[i][g]*30 != (mata->ma[i][g]*60)/2.0)
	    {
	      fprintfd(stderr, "ERROR IN BLOCK (NaN) \n");
	      fprintfd(stderr, "Row[%ld] ma[%ld] = "_coef_"\n", (long)i, (long)g, pcoef(mata->ma[i][g]));
	      flag = 1;
	    }
	}
      /*for(g=1;g<mata->nnzrow[i];g++)
	{
	  if(mata->ja[i][g-1] >= mata->ja[i][g])
	    {
	      fprintfd(stderr, "ERROR IN BLOCK \n");
	      fprintfd(stderr, "Row %ld ja[%ld] = %ld >= ja[%ld] = %ld \n", i, g-1, mata->ja[i][g-1], g, mata->ja[i][g]);
	      flag = 1;
	    }
	    }*/
	  

      if(flag)
	return 1;
    }
  return 0;
}
 
  
int checkL(csptr mata)
{
  dim_t i, g;
  int flag = 0;
  for(i=0;i<mata->n;i++)
    {
      if(mata->nnzrow[i] > i )
	{
	  fprintfd(stderr, "ERROR IN L \n");
	  fprintfd(stderr, "Row[%ld] has %ld elts \n", (long)i, (long)mata->nnzrow[i]);
	  flag = 1;
	}
      for(g=0;g<mata->nnzrow[i];g++)
	{
	  if(mata->ja[i][g] >= i)
	    {
	      fprintfd(stderr, "ERROR IN L \n");
	      fprintfd(stderr, "Row[%ld] ja[%ld] = %ld \n", (long)i, (long)g, (long)mata->ja[i][g]);
	      flag = 1;
	    }
	  if(mata->ma[i][g]*30 != (mata->ma[i][g]*60)/2.0)
	    {
	      fprintfd(stderr, "ERROR IN L (NaN) \n");
	      fprintfd(stderr, "Row[%ld] ma[%ld] = "_coef_"\n", (long)i, (long)g, pcoef(mata->ma[i][g]));
	      flag = 1;
	    }
	}


      if(flag)
	return 1;
    }
  return 0;
}



int checkU(csptr mata)
{
  dim_t i, g;
  int flag = 0;
  for(i=0;i<mata->n;i++)
    {
      if(mata->nnzrow[i] > mata->n - i || mata->nnzrow[i] <= 0)
	{
	  fprintfd(stderr, "ERROR IN U \n");
	  fprintfd(stderr, "Row[%ld] has %ld elts \n", (long)i, (long)mata->nnzrow[i]);
	  flag = 1;
	}
      for(g=0;g<mata->nnzrow[i];g++)
	{
	  if(mata->ja[i][g] < i)
	    {
	      fprintfd(stderr, "ERROR IN U \n");
	      fprintfd(stderr, "Row[%ld](nnz = %ld) ja[%ld] = %ld\n", (long)i, (long)mata->nnzrow[i], (long)g, (long)mata->ja[i][g]);
	      flag =1;
	    }
	  if(mata->ma[i][g]*30 != (mata->ma[i][g]*60)/2.0)
	    {
	      fprintfd(stderr, "ERROR IN U (NaN) \n");
	      fprintfd(stderr, "Row[%ld] ja[%ld] = %ld ma[%ld] = "_coef_"\n", (long)i, (long)g, (long)mata->ja[i][g], (long)g, pcoef(mata->ma[i][g]));
	      flag = 1;
	    }
	}


       if(flag)
	 return 1;
    }
  return 0;
}



int matrix_equality(csptr A, csptr B)
{
  /*----------------------------------/
  / Check if A == B                   /
  / On return:                        /
  / 0 --> equality is true            /
  / 1 --> different number of rows    /
  / 2 -->different number of element  /
  / 3 -->different index              /
  / 4 --> different coeff value       /
  /----------------------------------*/  
  dim_t i, j;

  if(A->n != B->n)
    {
      fprintfd(stderr, "MATRIX EQUALITY: A->n = %ld B->n = %ld \n", (long)A->n, (long)B->n);
      return 1;
    }
  for(i=0;i<A->n;i++)
    {
      if(A->nnzrow[i] != B->nnzrow[i])
	{
	  fprintfd(stderr, "MATRIX EQUALITY: row %ld A->nnzrow[%ld] = %ld B->nnzrow[%ld] = %ld \n",
		   (long)i, (long)i, (long)A->nnzrow[i], (long)i, (long)B->nnzrow[i]);
	  return 2;
	}
      for(j=0;j<A->nnzrow[i];j++)
	{
	  if(A->ja[i][j] != B->ja[i][j])
	    {
	      fprintfd(stderr, "MATRIX EQUALITY: A->ja[%ld][%ld] = %ld B->ja[%ld][%ld] = %ld \n",
		       (long)i, (long)j, (long)A->ja[i][j], (long)i, (long)j, (long)B->ja[i][j]);
	      return 3;
	    }
	  if(coefabs(A->ma[i][j]-B->ma[i][j]) > 10e-9)
	    {
	      fprintfd(stderr, "MATRIX EQUALITY: A->ma[%ld][%ld] = "_coef_" B->ma[%ld][%ld] = "_coef_"\n",
		       (long)i, (long)j, pcoef(A->ma[i][j]), (long)i, (long)j, pcoef(B->ma[i][j]));
	      return 4;
	    }
	}
    }
  return 0;
}


int PhidalHID_Check(PhidalHID *BL, mpi_t proc_id)
{
  
  dim_t i;
  for(i=0;i<BL->nlevel;i++)
    {
      assert(BL->block_levelindex[i] >= 0);
      assert(BL->block_levelindex[i+1] <= BL->nblock);
      assert(BL->block_levelindex[i+1]-BL->block_levelindex[i]>=0);
      assert(BL->block_levelindex[BL->nlevel] == BL->nblock); 
    }

  for(i=0;i<BL->nblock;i++)
    {
      assert(BL->block_index[i] <  BL->block_index[i+1]);
      assert(BL->block_index[i] >= 0 && BL->block_index[i+1] <= BL->n);
      assert(BL->block_keyindex[i+1]-BL->block_keyindex[i] > 0);
      assert(BL->block_keyindex[i+1]-BL->block_keyindex[i] <= BL->ndom);
    }
  return 0;
}

void checkGraph(dim_t n, INTL *ia, dim_t *ja, INTL *vwgt, INTL *ewgt, flag_t num)
{
  /***********************************/
  /* Check a graph                   */
  /* On entry:                       */
  /*    n, ia, ja : CSR matrix       */
  /*    num       : 0 = C            */
  /*                1 = Fortran      */
  /*                                 */
  /***********************************/
  dim_t i, j;
  long nnz;
  assert(num == 0 || num == 1);
  
  assert(n>num);
  assert(ia != NULL);
  assert(ja != NULL);

  nnz = 0;
  for(i=0;i<n;i++)
    {
      assert(ia[i] >= num);
      assert(ia[i] <= ia[i+1]);
      assert(ia[i+1]-ia[i]<=n);

      if(vwgt != NULL)
	assert(vwgt[i] > 0);
      
      
      nnz += ia[i+1]-ia[i];
      for(j = ia[i]-num;j<ia[i+1]-num;j++)
	{
	  assert(ja[j] >= num);
	  assert(ja[j] < n+num);
	  assert(ja[j] != i); /** No self-edge allowed **/
	  if(ewgt != NULL)
	    assert(ewgt[j] > 0);

	  if(j<ia[i+1]-num-1)
	    {
	      if( ja[j] == ja[j+1])
		fprintfd(stderr, "ja[%ld] = ja[%ld] = %ld in row %ld \n", (long)j, (long)(j+1), (long)ja[j], (long)i);
	      assert(ja[j] != ja[j+1]);
	    }
	}
    }
  /*fprintfd(stderr, "NNZ %ld n %ld Moyenne %g \n", nnz, n, ((float)nnz)/n);*/
  /*MPI_Abort(MPI_COMM_WORLD, -1);*/
}



void checkCSR(dim_t n, INTL *ia, dim_t *ja, COEF *a, flag_t numflag)
{
  /***********************************/
  /* Check a CSR matrix              */
  /* On entry:                       */
  /*    n, ia, ja : CSR matrix       */
  /*    numflag       : 0 = C            */
  /*                1 = Fortran      */
  /*                                 */
  /***********************************/
  dim_t i, j;
  long nnz;
  assert(numflag == 0 || numflag == 1);
  assert(ia[0] == numflag);
  assert(n>numflag);
  if(ia[n] == 0)
    return;

  assert(ia != NULL);
  assert(ja != NULL);

  nnz = 0;

  for(i=0;i<n;i++)
    {
      assert(ia[i] >= numflag);
      assert(ia[i] <= ia[i+1]);
      assert(ia[i+1]-ia[i]<=n);
    
      /*if(ia[i+1] == ia[i])
	fprintferr(stderr, "Warning in check CSR : Row %ld is empty \n", i);*/
      /** DEBUG IFP **/
      /*if(i == 72815)
	{
	  for(j=ia[i];j<ia[i+1];j++)
	    fprintfd(stderr, "A(%ld %ld)  = "_coef_"\n", i, ja[j], pcoef(a[j]));

	  fprintfd(stderr, "END ROW \n");
	  }*/
      /*if(a != NULL)
	{
	  for(j=ia[i];j<ia[i+1];j++)
	    {
	      if(((ja[j]==i && j == ia[i]+1) || (ia[i+1] <= ia[i]+2)))
		{
		  fprintfd(stderr, "ROW %ld nnz = %ld \n", i, ia[i+1]-ia[i]);
		  fprintfd(stderr, "A(%ld %ld)  = "_coef_"\n", i, ja[j-1], pcoef(a[j-1]));
		  fprintfd(stderr, "A(%ld %ld)  = "_coef_"\n", i, ja[j], pcoef(a[j]));
		}
	    }
	    }*/
      

      nnz += ia[i+1]-ia[i];
      for(j = ia[i]-numflag;j<ia[i+1]-numflag;j++)
	{
	  assert(ja[j] >= numflag);
	  assert(ja[j] < n+numflag);

	  /*if(coefabs(a[ja[j]]) < EPSILON)
	    {
	      fprintfd(stderr, "DIAG "_coef_"\n",  pcoef(a[ja[j]]));
	      fprintfd(stderr, "Replaced by "_coef_"\n",  pcoef(a[ja[j]])+1.0);
	      }*/


	  
	  if(j<ia[i+1]-numflag-1)
	    {
	      /*if(ja[j] >=  ja[j+1])
		fprintfd(stderr, " ja[%ld]= %ld  >= ja [%ld] = %ld \n", j, ja[j], j+1, ja[j+1]);
		assert(ja[j] < ja[j+1]);*/
	      
	      if( ja[j] == ja[j+1])
		{
		  if(a != NULL) {
		    fprintfd(stderr, "ja[%ld] ("_coef_") = ja[%ld] ("_coef_") = %ld in row %ld \n", (long)j, pcoef(a[j]), (long)(j+1), pcoef(a[j+1]), (long)ja[j], (long)i);
		  }
		  else {
		    fprintfd(stderr, "ja[%ld] = ja[%ld] = %ld in row %ld \n", (long)j, (long)(j+1), (long)ja[j], (long)i);
		  }
		}
		  
	      assert(ja[j] != ja[j+1]);
	    }
	  
	}
    }
}


int checkHID(dim_t nlevel, int *levelindex, int *rperm, int *keyindex, int *key, dim_t n, INTL *ia, dim_t *ja)
{
  /***********************************************/
  /* This function check the HID decomposition   */
  /*                                             */
  /***********************************************/
  int i, j, k, r,  lev;
  dim_t node, neigh;
  int *key1, *key2;
  int kl1, kl2;
  int error;
  int flag;

  error = 0;
  for(lev = 0; lev < nlevel; lev++)
    for(i=levelindex[lev];i<levelindex[lev+1];i++)
      {
	node = rperm[i];
	key1 = key + keyindex[node];
	kl1 = keyindex[node+1]-keyindex[node];
#ifdef DEBUG_M
	assert(kl1 == lev+1);
#endif
	for(j=ia[node];j<ia[node+1];j++)
	  {
	    neigh = ja[j];
	    key2 = key + keyindex[neigh];
	    kl2 = keyindex[neigh+1]-keyindex[neigh];
	    
	    if(kl2 == kl1)
	      for(k=0;k<kl1;k++)
		if(key1[k] != key2[k])
		  {
		    fprintfd(stderr, "ERROR in checkHID :Level %ld node %ld is adjacent with neighbor %ld \n", (long)lev, (long)node, (long)neigh);
		    error = -1;
		    break;
		  }

	    if(kl1 < kl2) /*** Check consistency ***/
	      for(k=0;k<kl1;k++)
		{
		  flag = 0;
		  for(r=0;r<kl2;r++)
		    if(key2[r] == key1[k])
		      {
			flag = 1;
			break;
		      }
		  if(flag == 0)
		    {
		      fprintfd(stderr, "ERROR in checkHID :Level %ld node %ld is not consistent with  neighbor %ld \n", (long)lev, (long)node, (long)neigh);
		      error = -2;
		      break;
		    }
		}

	  }
 
      }
  
  return error;

} 



int check_interior_node(dim_t ndom, int *mapptr, int *mapp, INTL *ia, dim_t *ja, dim_t n)
{
  dim_t i,j,k;
  int *tmp;
  dim_t node;
  dim_t n1, n2;


  tmp = (int *)malloc(sizeof(int)*n);
  for(i=0;i<n;i++)
    tmp[i] = -1;

  for(k=0;k<ndom;k++)
    for(i=mapptr[k];i<mapptr[k+1];i++)
      {
	node = mapp[i];
	assert(tmp[node] == -1);
	tmp[node] = k;
      }
  
  for(k=0;k<ndom;k++)
    for(i=mapptr[k];i<mapptr[k+1];i++)
      {
	node = mapp[i];
	for(j=ia[node];j<ia[node+1];j++)
	  {
	    if(tmp[ja[j]] != -1 && tmp[ja[j]] != k)
	      fprintfd(stderr, "node %ld (domain %ld) is adjacent to node %ld (domain %ld) \n", (long)node, (long)k, (long)ja[j], (long)tmp[ja[j]]); 
	    assert(tmp[ja[j]] == -1 || tmp[ja[j]] == k);

	  }
      }

  /**** Test if the nodes in the interface are connected to more than one subdomain ****/
  n1 = 0;
  n2 = 0;
  for(i=0;i<n;i++)
    if(tmp[i]== -1)
      {
	int flag, dom;
	flag=0;
	dom = -1;
	for(j=ia[i];j<ia[i+1];j++)
	  {
	    node = ja[j];
	    if(tmp[node]>=0)
	      {
		if(dom >= 0 && tmp[node] != dom )
		  {
		    dom = tmp[node];
		    flag = 1;
		    break;
		  }
		dom = tmp[node];
	      }
	  }
	if(dom == -1)
	  {
	    n1++;
	    /*fprintfd(stderr, "Node %ld of the interface is not related to any subdomain \n", i);*/
	  }
	if(flag == 0 && dom >= 0)
	  {
	    n2++;
	    /*fprintfd(stderr, "Node %ld of the interface is only related to ONE subdomain \n", i);*/
	  }
      }
  fprintfd(stderr, "\n\n There were %ld nodes in the interface not related to ANY subdomain \n", (long)n1);
  fprintfd(stderr, "There were %ld nodes in the interface related to ONE subdomain \n\n", (long)n2);

  free(tmp);
  return 0;
}



int PhidalMatrix_Check(PhidalMatrix *M, PhidalHID *BL)
{
  dim_t i, j, k;
  csptr Mij;
  assert(M->tli >= 0 && M->tli < BL->nblock);
  assert(M->tlj >= 0 && M->tlj < BL->nblock);
  assert(M->bri >= 0 && M->bri < BL->nblock);
  assert(M->brj >= 0 && M->brj < BL->nblock);
  assert(M->tli <= M->bri);
  assert(M->tlj <= M->brj);
  
  assert(M->bloknbr == M->ria[ M->bri+1]);
  assert(M->bloknbr == M->cia[ M->brj+1]);

  if(M->csc == 0)
    {
      for(i=M->tli; i<=M->bri;i++)
	for(k=M->ria[i];k<M->ria[i+1];k++)
	  {
	    j = M->rja[k];
	    Mij = M->ra[k];
	    if(Mij->n != 0)
	      {
		assert(Mij->n == BL->block_index[i+1]-BL->block_index[i]);
		CS_Check(Mij, BL->block_index[j+1]-BL->block_index[j]);
	      }
	  }
      
      for(j=M->tlj; j<=M->brj;j++)
	for(k=M->cia[j];k<M->cia[j+1];k++)
	  {
	    i = M->cja[k];
	    Mij = M->ca[k];
	    if(Mij->n != 0)
	      {
		assert(Mij->n == BL->block_index[i+1]-BL->block_index[i]);
		CS_Check(Mij, BL->block_index[j+1]-BL->block_index[j]);
	      }
	  }
    }
  else
    {
      assert(M->csc == 1);
      for(i=M->tli; i<=M->bri;i++)
	for(k=M->ria[i];k<M->ria[i+1];k++)
	  {
	    j = M->rja[k];
	    Mij = M->ra[k];
	    if(Mij->n != 0)
	      {
		assert(Mij->n == BL->block_index[j+1]-BL->block_index[j]);
		CS_Check(Mij, BL->block_index[i+1]-BL->block_index[i]);
	      }
	  }
      
      for(j=M->tlj; j<=M->brj;j++)
	for(k=M->cia[j];k<M->cia[j+1];k++)
	  {
	    i = M->cja[k];
	    Mij = M->ca[k];
	    if(Mij->n != 0)
	      {
		assert(Mij->n == BL->block_index[j+1]-BL->block_index[j]);
		CS_Check(Mij, BL->block_index[i+1]-BL->block_index[i]);
	      }
	  }
    }


  return 0;
}




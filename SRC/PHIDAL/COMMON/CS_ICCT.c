/* @authors P. HENON */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "queue.h"
#include "phidal_common.h"

#if defined(TYPE_REAL)
#define STATIC_PIV(arg) if(arg >= 0) arg = EPSILON; else arg = -EPSILON
#elif defined (TYPE_COMPLEX)
#define STATIC_PIV(arg) arg = EPSILON
#else
#error TYPE_REAL / TYPE_COMPLEX undefined
#endif

/**** OIMBE: Il n'y a pas de ALLOC_COMPACT_ILUT car trop complique et couteux a gerer
      a cause des LrowLIst et LrowFirst **/

void CS_ICCT(csptr L, COEF *D, REAL droptol, REAL *droptab, REAL fillrat, int *wki1, int *wki2, COEF *wkd, cell_int *firsttab, cell_int **LrowList, flag_t shiftdiag)
{
  /********************************************************************************************/
  /* Compute an incomplete Crout factorization of a sparse matrix in SparRow structure        */
  /* On input :                                                                               */
  /* L is the lower triangular part of A                                                      */
  /* On return:                                                                               */
  /* L is the strictly lower triangular part of the factor                                    */
  /* D is the inverse of the diagonal term of the factor                                      */
  /* If shiftdiag == 1 then the diagonal term is augmented by what was dropped in the column  */
  /* NOTE: on return L is SORTED by row indices                                               */
  /********************************************************************************************/

  dim_t i, j, k;
  dim_t n, min, ind;
  dim_t nnzl;
  cell_int *c, *next;
  cell_int **l;

  int jcol, jpos;
  int *jrev, *tmpj;  
  COEF *tmpa;
  REAL shift;
  dim_t counta;

  int *lja;
  COEF *lma;
  REAL d;
  COEF s;
  int lenl;
  REAL maxfill=-1;
  flag_t fillrow;
  Queue queue;
  long nnzL;
  int small = 0;
  dim_t *jatab=NULL;
  COEF *matab=NULL;
  n = L->n;

  small = 0;
  if(((REAL)(n+1)*(REAL)n)/2.0 <= (REAL)SMALLBLOCK)
    {
      small = 1;
      jatab = (int *)malloc(sizeof(int)*(((n+1)*n)/2));
      matab = (COEF *)malloc(sizeof(COEF)*(((n+1)*n)/2));
    }

  if(fillrat > 0)
    {
      k = 0;
      for(j=0;j<L->nnzr;j++)
	{
	  i = L->nzrtab[j];
	  if(L->nnzrow[i] > k)
	    k = L->nnzrow[i];
	}
      queueInit(&queue, (int)(k*fillrat));
      nnzL = CSnnz(L);
      maxfill = nnzL*fillrat;
    }

  /** Init the cellules **/
  for(i=0;i<n;i++)
    firsttab[i].elt = i;
  bzero(LrowList, sizeof(cell_int *)*n);


  jrev = wki1;

  if(small == 1)
    {
      tmpj = jatab;
      tmpa = matab;
    }
  else
    {
      tmpj = wki2;
      tmpa = wkd;
    }

  for(j=0;j<L->n;j++) 
    jrev[j] = -1;


  nnzL = 0;
  for(k=0;k<n;k++)
    {
      /******************************************/
      /**   Compute the column k of L          **/
      /******************************************/
      shift = 0.0;
      counta = 0;
      /** Unpack the column k of L **/
      lja = L->ja[k];
      lma = L->ma[k];
      lenl = 1; /** start at 1 : the diagonal term is stored at index 0 **/

      tmpj[0] = k;
      tmpa[0] = 0.0;  /** If no diagonal is found assume it is zero **/

      for(i=0;i<L->nnzrow[k];i++)
	{
	  jcol = lja[i];
	  if(jcol>k)
	    {
	      tmpj[lenl] = jcol;
	      tmpa[lenl] = lma[i];
	      lenl++;
	    }
	  else
	    {
#ifdef DEBUG_M
	      assert(jcol == k);
#endif
	      tmpa[0] = lma[i]; /** Diagonal term (k,k) is stored in tmpa[k] **/
	    }
	}
      
      /* Init jrev */
      for(j=0;j<lenl;j++)
	jrev[tmpj[j]] = j;


      
      /** Multiply Lrow by Lfirst **/
      c = LrowList[k];
      while(c!=NULL)
	{
	  next = c->next;
	  nnzl = c->nnz;
	  lja = c->ja;
	  lma = c->ma;
	  j = c->elt;

#ifdef DEBUG_M
	  assert(nnzl > 0);
	  assert(lja[0] == k);
#endif
	  
	  s = lma[0]*D[j];
	
	  min = n;
	  ind = -1;
	  for(i=0;i<nnzl;i++)
	    {
	      jcol = lja[i];
#ifdef DEBUG_M
	      assert(jcol >= k);
#endif
	      if(jcol < min && jcol > k)
		{
		  min = jcol;
		  ind = i;
		}
#ifdef DEBUG_M
	      assert(jcol >= k);
#endif
	      shift += coefabs(lma[i]);
	      counta++;

	      jpos = jrev[jcol];
	      if(jpos >= 0)
		tmpa[jpos] -= s*lma[i];
	      else
		{
		  jrev[jcol] = lenl;
		  tmpj[lenl] = jcol;
		  tmpa[lenl] = -s*lma[i];
		  lenl++;
		}
	    }
	  lma[0] = s;

	  if(ind > 1)
	    {
	      /*** Swap the entry in col j of L such that the next entry in col j be the minimum row index > k ***/
	      lja[ind] = lja[1];
	      s = lma[ind];
	      lma[ind] = lma[1];
	      lja[1] = min;
	      lma[1] = s;
	    }
	  

	  /** Update the next columns lists **/
	  c->nnz--;
	  if(c->nnz > 0)
	    {
	      c->ja++;
	      c->ma++;
	      
	      jcol = *(c->ja);
#ifdef DEBUG_M
	      assert(jcol>k);
#endif
	      l = LrowList+jcol;
	      c->next = *l;
	      *l = c;
	    }
	  
	  c = next;
	}


      /*** Do these steps in the right order ! ****/	   
      /**** 1 Inverse the diagonal term  *******/
      if(coefabs(tmpa[0]) < EPSILON)
	{
	  fprintferr(stderr, "Pivot (%ld, %ld) is small "_coef_": replace it \n", (long)k, (long)k, pcoef(tmpa[0]));

	  STATIC_PIV(tmpa[0]);
	}

      
      D[k] = 1.0/tmpa[0];
	  

      
      /**** 2 Reinit jrev ****/ /** DO IT HERE !! **/
      for(i=0;i<lenl;i++) 
	jrev[tmpj[i]] = -1;


      /**** 3 Dropping in L col k ******/
      if(droptab != NULL)
	d = droptab[k]*droptol;
      else
	d = droptol;

      d *= coefabs(tmpa[0]);


      jpos = 0;
      for(i=1;i<lenl;i++) /* Begin at 1 to delete the diagonal entry (not store in L) ! */
	{
	  if(coefabs(tmpa[i])> d)
	    {
	      tmpj[jpos] = tmpj[i];
	      tmpa[jpos] = tmpa[i];
	      shift += coefabs(tmpa[i]);
	      counta++;
	      jpos++;
	    }

	}
      lenl = jpos;

      if(shiftdiag == 1)
	{
	  D[k] = 1.0/D[k];
#ifdef TYPE_COMPLEX
	  if(counta > 0)
	    {
	      /*shift /= (REAL)counta;*/
	      shift = (shift - 2.0*coefabs(D[k]))/(REAL)counta;
	    }
	  else
	    shift = 0.0;

	  /*shift = cimag(D[k])<=0 ? -shift : shift; */

	  /*fprintfd(stderr, "piv =" _coef_ " shift =" _coef_ "\n",  pcoef(D[k]), pcoef(I*shift));*/
	  D[k] += I*shift;
#else
	  shift = D[k]<=0 ? -shift : shift; 
	  D[k] += shift;

#endif
	  assert(coefabs(D[k])>0);
	  D[k] = 1.0/D[k];

	}
    
    
      /**** 4 Keep only the largest entries in col k of L  according to fillrat ***/
      if(fillrat > 0)
	{
	  fillrow = (int)( (REAL)(maxfill)/(L->n-k));
	  vec_filldrop(&lenl, tmpj, tmpa, fillrow, &queue);
	  maxfill -= lenl;
	}
	  
	 
	  
      /**** 5 Create the new row in L ******/
      if(L->inarow != 1 && L->nnzrow[k] > 0 )
	{
	  free(L->ja[k]);
	  free(L->ma[k]);
	}
      L->nnzrow[k] = lenl;

      if(lenl>0)
	{
	  if(small == 1)
	    {
	      L->ja[k] = tmpj;
	      L->ma[k] = tmpa;
	      tmpj += lenl;
	      tmpa += lenl;
	      nnzL += (long)lenl;
	    }
	  else
	    {
	      L->ja[k] = (int *)malloc(sizeof(int)*lenl);
	      memcpy(L->ja[k], tmpj, sizeof(int)*lenl);
	      L->ma[k] = (COEF *)malloc(sizeof(COEF)*lenl);
	      memcpy(L->ma[k], tmpa, sizeof(COEF)*lenl);
	    }
	}

      /**** 6 Compute Lfirst for this column ******/
      if(L->nnzrow[k] > 0) /** You must have at least one extra-diagonal **/
	{
	  lja = L->ja[k];
	  lma = L->ma[k];

	  firsttab[k].nnz = L->nnzrow[k];
	  firsttab[k].ja = lja;
	  firsttab[k].ma = lma;


	  min = n;
	  ind = -1;
	  for(i=0;i<L->nnzrow[k];i++)
	    {
	      jcol = lja[i];
#ifdef DEBUG_M
	      assert(jcol >k);
#endif
	      if(jcol<min)
		{
		  min = jcol;
		  ind = i;
		}
	    }
	  
	  if(ind > 0)
	    {
	      /*** Swap the entry in col j of L such that the next entry in col j be the minimum row index > k **/
	      lja[ind] = lja[0];
	      s = lma[ind];
	      lma[ind] = lma[0];
	      lja[0] = min;
	      lma[0] = s;
	    }
	  
	  if(ind >= 0)
	    {
	      l = LrowList+min;
	      c = firsttab+k;
	      c->next = *l;
	      *l = c;
	    }

	}
    }

  /*free(firsttab);
    free(LrowList);*/

  if(fillrat > 0)
    queueExit(&queue);	


  if(L->inarow == 1)
    {
      if(L->matab != NULL)
	free(L->matab);
      if(L->jatab != NULL)
	free(L->jatab);
      L->inarow = 0;
    }

  CS_SetNonZeroRow(L);

  if(small == 1)
    {
      matab = (COEF *)realloc(matab, sizeof(COEF)*nnzL);
      jatab = (int *)realloc(jatab, sizeof(int)*nnzL);
      nnzL = 0;
      L->matab = matab;
      L->jatab = jatab;
      for(k=0;k<L->nnzr;k++)
	{
	  i = L->nzrtab[k];
	  L->ja[i] = jatab+nnzL;
	  L->ma[i] = matab+nnzL;
	  nnzL += L->nnzrow[i];
	}
      L->inarow = 1;
    }
#ifdef REALLOC_BLOCK
  else
    CSrealloc(L);
#endif

#ifdef DEBUG_M
  CS_Check(L, L->n);
#endif

}

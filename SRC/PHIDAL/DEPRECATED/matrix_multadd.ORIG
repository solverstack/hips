#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <assert.h>
#include <math.h>
#include "mlbd_struct.h"

extern void quicksort(int *inttab, int start, int end);
extern void quicksort_row(int *ja, double *a, int start, int end);
extern int checkBlock(csptr mata, int r, int s);

void block_mult_add(csptr A, csptr B, csptr C, int cdim, double droptol, double *normvec)
{
  /*--------------------------------------------------------/
  / Perform C = C + A*B                                     /
  / Dropping in row defined by droptol and normvec          /
  /--------------------------------------------------------*/


  double *tmp;
  int *tmpj;
  int *jrev; /** reverse index vector **/
  int *Ajptr, *Bjptr, *Cjptr;
  double *Amaptr, *Bmaptr, *Cmaptr;
  int i, j, k;
  int ja; 
  int jpos;
  int nnzrow; /* store the number of entries in a row of A*B */ 
  int nne; /* number of new entries in a row */
  double scal;

#ifdef DEBUG_M
  assert(cdim > 0);
#endif

  tmp = (double *)malloc(sizeof(double)*cdim);
  tmpj = (int *)malloc(sizeof(int)*cdim);

  jrev = (int *)malloc(sizeof(int)*cdim); 
  /** Initialize jrev **/
  for(i=0;i<cdim;i++)
    jrev[i] = -1;
      

  for(i=0; i<C->n; i++)
    {

#ifdef DEBUG_M
      assert(A->nnzrow[i] <= B->n);
      assert(C->nnzrow[i] <= cdim);
#endif

      if(A->nnzrow[i] == 0)
	continue;
      
      /*bzero(jrev, sizeof(int)*cdim);*/
      nnzrow = 0;
      
      Ajptr = A->ja[i];
      Amaptr = A->ma[i];
      Cjptr = C->ja[i];
      Cmaptr = C->ma[i];

      
      /*----------------------------/
      /  Compute tmp=C[i] + A[i]*B  /
      /----------------------------*/
      
      /** Set tmp = C[i] **/
      for(j = 0;j< C->nnzrow[i];j++)
	{
	  jrev[Cjptr[j]] = j;
	}
      
      if(C->nnzrow[i]>0)
	{
	  memcpy(tmpj, Cjptr, sizeof(int)*C->nnzrow[i]);
	  memcpy(tmp, Cmaptr, sizeof(double)*C->nnzrow[i]);
	}
      nnzrow = C->nnzrow[i];

      nne = 0;
      /*thresh = droptol*normvec[i]; ACTUALLY NO DROPPING NEEDED  **/
      for(j=0;j<A->nnzrow[i];j++)
	{
	  ja = Ajptr[j];
	  Bjptr = B->ja[ ja ];
	  Bmaptr = B->ma[ ja ];
	  scal = Amaptr[j];


	  for(k=0;k<B->nnzrow[ja];k++)
	    {
	      nne++;
	      jpos =  jrev[Bjptr[k]];
	      if(jpos >= 0)
		{
		  /** This is not a fill-in element **/
		  tmp[ jpos ] += scal*Bmaptr[k];
		}
	      else
		{
		  /** This is a fill-in element **/
		  tmp[ nnzrow ] = scal*Bmaptr[k];
		  tmpj[nnzrow] = Bjptr[k];
		  jrev[ Bjptr[k] ] = nnzrow;
		  nnzrow++;
		}
	    }
	  
	}
      /** Reinitialize jrev  **/
      for(j=0;j<nnzrow;j++)
	jrev[tmpj[j]] = -1;


      /** (A*B)[i][*] == 0 **/
      if(nne == 0) /* Nothing to do: leave row C[i, *] intact */
	continue;
      /*---------------------------------------/
      / Realloc row C[i, *] and fill it        /
      /---------------------------------------*/
#ifdef DEBUG_M
      assert(nnzrow > 0);      
#endif
      if(C->nnzrow[i] > 0)
	{
	  C->ja[i] = (int *)realloc(C->ja[i], sizeof(int)*nnzrow);
	  C->ma[i] = (double *)realloc(C->ma[i], sizeof(double)*nnzrow);
	}
      else
	{
	  C->ja[i] = (int *)malloc(sizeof(int)*nnzrow);
	  C->ma[i] = (double *)malloc(sizeof(double)*nnzrow);
	}
	    
      memcpy(C->ja[i], tmpj, sizeof(int)*nnzrow);
      memcpy(C->ma[i], tmp, sizeof(double)*nnzrow);
      C->nnzrow[i] = nnzrow;

#ifdef DEBUG_M
      assert(nne > 0);
      assert(nnzrow <= cdim);
#endif

    }
  
  free(tmpj);
  free(tmp);
  free(jrev);
}



void block_add(csptr A, csptr C, int cdim)
{
  /*--------------------------------------------/
  / Perform in place C  = C + A (no dropping)   /
  /--------------------------------------------*/

  double *tmp;
  int *tmpj;
  int *jrev;
  int *Ajptr, *Cjptr;
  double *Amaptr, *Cmaptr;
  int i, j;
  int jpos;
  int nnzrow; /* store the number of entries in new row */ 

#ifdef DEBUG_M
  assert(cdim > 0);
  assert(checkBlock(C, C->n, cdim) == 0);
  assert(checkBlock(A, C->n, cdim) == 0);
#endif

  tmp = (double *)malloc(sizeof(double)*cdim);
  tmpj = (int *)malloc(sizeof(int)*cdim);
  jrev = (int *)malloc(sizeof(int)*cdim); 
  /** Initialize jrev **/
  for(i=0;i<cdim;i++)
    jrev[i] = -1;
  
  for(i=0; i<C->n; i++)
    {

#ifdef DEBUG_M
      assert(A->nnzrow[i] <= cdim);
      assert(C->nnzrow[i] <= cdim);
#endif

      if(A->nnzrow[i] == 0) /** Nothing to add in this row **/
	continue;
      
      nnzrow = 0;
      
      Ajptr = A->ja[i];
      Amaptr = A->ma[i];

      /*-------------------------------/
      / Add row A[i,*] to C[i,*]       /
      /-------------------------------*/
      Cjptr = C->ja[i];
      Cmaptr = C->ma[i];
      
      /** If row C[i,*] empty then Copy row A[i, *] into C[i, *] **/
      if(C->nnzrow[i] == 0) 
	{
	  C->ja[i] = (int *)malloc(sizeof(int)*A->nnzrow[i]);
	  C->ma[i] = (double *)malloc(sizeof(double)*A->nnzrow[i]);
	  memcpy(C->ja[i], Ajptr, sizeof(int)*A->nnzrow[i]);
	  memcpy(C->ma[i], Amaptr, sizeof(double)*A->nnzrow[i]);
	  C->nnzrow[i] = A->nnzrow[i];
	  continue;
	}

      
      /** Set tmp = C[i] **/
      for(j = 0;j< C->nnzrow[i];j++)
	{
	  jrev[Cjptr[j]] = j;
	}
      memcpy(tmpj, Cjptr, sizeof(int)*C->nnzrow[i]);
      memcpy(tmp, Cmaptr, sizeof(double)*C->nnzrow[i]);
      nnzrow = C->nnzrow[i];
#ifdef DEBUG_M
      assert(nnzrow >= 0);
#endif
      
      /** tmp, tmpj are use to store temporaly the new row C(i, *) **/
      /* Put A[i, *] + C[i,*] in tmp */
      for(j=0;j<A->nnzrow[i];j++)
	{
#ifdef DEBUG_M
	  assert(Ajptr[j] >=0);
	  assert(Ajptr[j] < cdim);
#endif
	  jpos = jrev[Ajptr[j]];
	  if(jpos >= 0)
	    {
	      /** This is not a fill-in element **/
	      tmp[ jpos ] += Amaptr[j];
	    }
	  else
	    {
	      /** This is a fill-in element **/
	      tmp[ nnzrow ] = Amaptr[j];
	      tmpj[nnzrow] = Ajptr[j];
	      jrev[ Ajptr[j] ] = nnzrow;
	      nnzrow++;
	    }
	}
      /** Reinitialize jrev  **/
      for(j=0;j<nnzrow;j++)
	jrev[tmpj[j]] = -1;


#ifdef DEBUG_M
      assert(nnzrow <= cdim);
      assert(nnzrow >= C->nnzrow[i]);
      if(nnzrow < A->nnzrow[i])
	{
	  fprintfv(5, stderr, "i %d nnzrow %d A->nnzrow[i] %d C->nnzrow[i] = %d \n",  i, nnzrow,  A->nnzrow[i] , C->nnzrow[i] );
	}
      assert(nnzrow >= A->nnzrow[i]);
#endif

      /* Realloc & fill C[i] */
      if(C->nnzrow[i] > 0)
	{
	  C->ja[i] = (int *)realloc(C->ja[i], sizeof(int)*nnzrow);
	  C->ma[i] = (double *)realloc(C->ma[i], sizeof(double)*nnzrow);
	}
      else
	{
	  C->ja[i] = (int *)malloc(sizeof(int)*nnzrow);
	  C->ma[i] = (double *)malloc(sizeof(double)*nnzrow);
	}
      memcpy(C->ja[i], tmpj, sizeof(int)*nnzrow);
      memcpy(C->ma[i], tmp, sizeof(double)*nnzrow);
      C->nnzrow[i] = nnzrow;

#ifdef DEBUG_M
      assert(C->ja[i] != NULL);
      assert(C->ma[i] != NULL);
#endif

      C->nnzrow[i] = nnzrow;
      memcpy(C->ja[i], tmpj, sizeof(int)*nnzrow);
      memcpy(C->ma[i], tmp, sizeof(double)*nnzrow);


   

      /** Sorted the column indices in row[i] of C NEW **/
      /*quicksort_row(C->ja[i], C->ma[i], 0, nnzrow-1);*/
    }

  free(tmp);
  free(tmpj);
  free(jrev);
}


void block_substrT(csptr A, csptr C, int cdim, double droptol, double *normvec, double *diagU)
{
  /*-------------------------------------------------------------------------------/
  / Perform in place C  = C - A (drop small entries*diagU < normvec(row)*droptol)  /
  / If diagU == NULL then considered as = 1 everywhere                             /
  /-------------------------------------------------------------------------------*/
  double *tmp;
  int *tmpj;
  int *jrev;
  int *Ajptr, *Cjptr;
  double *Amaptr, *Cmaptr;
  int i, j;
  int jpos;
  double thresh;
  int nnzrow; /* store the number of entries in new row */ 
  

#ifdef DEBUG_M
  assert(cdim > 0);
#endif

  tmp = (double *)malloc(sizeof(double)*cdim);
  tmpj = (int *)malloc(sizeof(int)*cdim);
  jrev = (int *)malloc(sizeof(int)*cdim); 
  /** Initialize jrev **/
  for(i=0;i<cdim;i++)
    jrev[i] = -1;
  
  for(i=0; i<C->n; i++)
    {

#ifdef DEBUG_M
      assert(A->nnzrow[i] <= cdim);
      assert(C->nnzrow[i] <= cdim);
#endif

      if(A->nnzrow[i] == 0) /** Nothing to add in this row **/
	continue;
      
      nnzrow = 0;
      
      Ajptr = A->ja[i];
      Amaptr = A->ma[i];

      /*-------------------------------/
      / Add row A[i,*] to C[i,*]       /
      /-------------------------------*/
      Cjptr = C->ja[i];
      Cmaptr = C->ma[i];
            
      /** Set tmp = C[i] **/
      for(j = 0;j< C->nnzrow[i];j++)
	{
	  /*tmp[j] = Cmaptr[j];*/
	  jrev[Cjptr[j]] = j;
	}

      if(C->nnzrow[i]>0)
	{
	  memcpy(tmpj, Cjptr, sizeof(int)*C->nnzrow[i]);
	  memcpy(tmp, Cmaptr, sizeof(double)*C->nnzrow[i]);
	}
      nnzrow = C->nnzrow[i];
      
      
      /** tmp, tmpj are use to store temporaly the new row C(i, *) **/
      /* Put A[i, *] + C[i,*] in tmp */
      for(j=0;j<A->nnzrow[i];j++)
	{
	  jpos = jrev[Ajptr[j]];
	  if(jpos >= 0)
	    {
	      /** This is not a fill-in element **/
	      tmp[ jpos ] -= Amaptr[j];
	    }
	  else
	    {
	      /** This is a fill-in element **/
	      tmp[ nnzrow ] = -Amaptr[j];
	      tmpj[nnzrow] = Ajptr[j];
	      jrev[ Ajptr[j] ] = nnzrow;
	      nnzrow++;
	    }
	}

      /** Reinitialize jrev  **/
      for(j=0;j<nnzrow;j++)
	jrev[tmpj[j]] = -1;


#ifdef DEBUG_M
      assert(nnzrow <= cdim);
      assert(nnzrow >= C->nnzrow[i]);
      assert(nnzrow >= A->nnzrow[i]);
#endif      
  
      
      /*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
      /*------------------------/
      / Dropp small entries     /
      /------------------------*/
      if(droptol > 0)
	{
	  thresh = normvec[i]*droptol; 
	  jpos = 0;
	  if(diagU != NULL)
	    {
	      for(j=0;j<nnzrow;j++)
		if(fabs(tmp[j]*diagU[tmpj[j]]) > thresh)
		  {
		    tmpj[jpos] = tmpj[j];
		    tmp[jpos] = tmp[j];
		    jpos++;
		  }
	    }
	  else
	    {
	      for(j=0;j<nnzrow;j++)
		if(fabs(tmp[j]) > thresh)
		  {
		    tmpj[jpos] = tmpj[j];
		    tmp[jpos] = tmp[j];
		    jpos++;
		  }
	     }
	  nnzrow = jpos;
	}
      /*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/



      /* Realloc & fill C[i] */
      if(nnzrow == 0)
	{
	  if(C->nnzrow[i] > 0)
	    {
	      free(C->ja[i]);
	      free(C->ma[i]);
	      C->nnzrow[i] = 0;
	    }
	  continue;
	}

      if(C->nnzrow[i] > 0)
	{
	  C->ja[i] = (int *)realloc(C->ja[i], sizeof(int)*nnzrow);
	  C->ma[i] = (double *)realloc(C->ma[i], sizeof(double)*nnzrow);
	}
      else
	{
	  C->ja[i] = (int *)malloc(sizeof(int)*nnzrow);
	  C->ma[i] = (double *)malloc(sizeof(double)*nnzrow);
	}
      memcpy(C->ja[i], tmpj, sizeof(int)*nnzrow);
      memcpy(C->ma[i], tmp, sizeof(double)*nnzrow);
      C->nnzrow[i] = nnzrow;

#ifdef DEBUG_M
      assert(C->ja[i] != NULL);
      assert(C->ma[i] != NULL);
#endif
  

      /** Sorted the column indices in row[i] of C NEW **/
      /*quicksort_row(C->ja[i], C->ma[i], 0, nnzrow-1);*/
    }

  free(tmp);
  free(tmpj);
  free(jrev);
}


void matrix_dropping(csptr A, int adim, int sp, double droptol, double *normvec)
{
  int i,j;
  double *tmp;
  int *tmpj;
  int *Ajptr;
  double *Amaptr;
  int nne;
  double thresh;

  if(droptol == 0) /** NOTHING TO DO **/
    return;

  tmp = (double *)malloc(sizeof(double)*adim);
  tmpj = (int *)malloc(sizeof(int)*adim);
  for(i=0;i<A->n;i++)
    {
      if(A->nnzrow[i] == 0)
	continue;

      Ajptr = A->ja[i];
      Amaptr = A->ma[i];
      nne = 0;
      thresh = normvec[i]*droptol / (float)sp;
      for(j=0;j<A->nnzrow[i];j++)
	if(fabs(Amaptr[j]) > thresh)
	  {
	    tmpj[nne] = Ajptr[j];
	    tmp[nne] = Amaptr[j];
	    nne++;
	  }

#ifdef DEBUG_M
      assert(nne <= A->nnzrow[i]);
#endif
      A->nnzrow[i] = nne;
      if(nne > 0)
	{
	  A->ja[i] = (int *)realloc(A->ja[i], sizeof(int)*nne);
	  A->ma[i] = (double *)realloc(A->ma[i], sizeof(double)*nne);
	  memcpy(A->ja[i], tmpj, sizeof(int)*nne);
	  memcpy(A->ma[i], tmp, sizeof(double)*nne);
	}
      else
	{
	  free(A->ja[i]);
	  free(A->ma[i]);
	}
    }
  
  free(tmp);
  free(tmpj);
}

#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <stdio.h>

#include "phidal_common.h"

int RowColcompact(int nb, csptr *X, csptr *Y, csptr x, csptr y);

void CSCrowMultCSRcol(double droptol, double *droptab, double fillrat, int nnb, double alpha, csptr *X, csptr *Y, csptr m, int mnrow, int *wki1, int *wki2, double *wkd)
{
  /*************************************************************/
  /* This function performs the "dot product" of two vectors   */
  /* X is a row of CSC matrices                                */
  /* Y is a column of CSR matrices                             */
  /* m = m - dot(X, Y)                                         */
  /* NOTE: each matrices of Y must be  SORTED by column indices*/
  /*************************************************************/
  csptr x, y;

#ifdef DEBUG_M
  {
    int k;
    for(k=0;k<nnb;k++)
      CS_Check(X[k], mnrow);
    for(k=0;k<nnb;k++)
      CS_Check(Y[k], m->n);

    CS_Check(m, mnrow);
  }
#endif
  

  x = (csptr)malloc(sizeof(struct SparRow));
  y = (csptr)malloc(sizeof(struct SparRow)); 

  /*CS_VirtualMerge(nnb, X, x);
    CS_VirtualMerge(nnb, Y, y);*/
  if(RowColcompact(nnb, X, Y, x, y) > 0)
    { 
      /** m = m + alpha.x.y **/
      CSCxCSR_CSC_GEMMT(alpha, x, y, m, mnrow, droptol, droptab, fillrat, wki1, wki2, wkd);
      

      /** Reinit the pointer of x and y to no delete them in cleanCS **/
      bzero(x->nnzrow, sizeof(int)*x->n);
      bzero(x->ja, sizeof(int *)*x->n);
      bzero(x->ma, sizeof(double *)*x->n);
      
      bzero(y->nnzrow, sizeof(int)*y->n);
      bzero(y->ja, sizeof(int *)*y->n);
      bzero(y->ma, sizeof(double *)*y->n);
      
      cleanCS(x);
      cleanCS(y);
    }
  free(x);
  free(y);
}


int RowColcompact(int nb, csptr *X, csptr *Y, csptr x, csptr y)
{
  /*******************************************************/
  /* This function merges several CS  matrices           */  
  /* into a single one                                   */
  /* NOTE the returned matrix is virtual i.e.            */
  /* it is a pointer to rows in the mattab matrices      */
  /*******************************************************/

  csptr mx, my;
  int *xnnzrow, *ynnzrow, *mxnnzrow, *mynnzrow;
  int **xja, **yja, **mxja, **myja;
  double **xma, **yma, **mxma, **myma;
  int i, k, ind;
  int n;
  
  n = 0;
  for(i=0;i<nb;i++)
    {
      mx = X[i];
      my = Y[i];
      mxnnzrow = mx->nnzrow;
      mynnzrow = my->nnzrow;
      for(k=0;k<mx->n;k++)
	if(mxnnzrow[k]>0 && mynnzrow[k]>0)
	  n++;
    }

  if(n==0)
    return 0;
  

  initCS(x, n);
  initCS(y, n);
  xnnzrow = x->nnzrow;
  ynnzrow = y->nnzrow;
  xja = x->ja;
  yja = y->ja;
  xma = x->ma;
  yma = y->ma;

  ind = 0;
  for(i=0;i<nb;i++)
    {
      mx = X[i];
      my = Y[i];
      mxnnzrow = mx->nnzrow;
      mynnzrow = my->nnzrow;
      mxja = mx->ja;
      myja = my->ja;
      mxma = mx->ma;
      myma = my->ma;

      for(k=0;k<mx->n;k++)
	if(mxnnzrow[k]>0 && mynnzrow[k]>0)
	  {
	    xnnzrow[ind] = mxnnzrow[k];
	    xja[ind] = mxja[k];
	    xma[ind] = mxma[k];
	    ynnzrow[ind] = mynnzrow[k];
	    yja[ind] = myja[k];
	    yma[ind] = myma[k];
	    ind++;
	  }
    }
#ifdef DEBUG_M
  assert(ind == n);
#endif

  return n;
}  

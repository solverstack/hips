/* @release_exclude */
/* @authors P. HENON */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>
/*#include "psparslib.h"
#include "heads.h"
#include "mlbd_struct.h"*/

#include "phidal_sequential.h"

/*** @@ OIMBE : write a good interface for a PHIDAL_TRSM ***/

void PHIDAL_InvLT(PhidalMatrix *L, PhidalMatrix *M, double droptol, double *droptab, double fillrat, PhidalHID *BL, int *wki1, int *wki2, double *wkd)
{
  /*********************************************************************/
  /* this function does M = L^-1.M                                     */
  /* with a dropping criterion drotol                                  */
  /* L is a lower triangular PhidalMatrix                              */
  /* M is a PhidalMatrix                                               */ 
  /* The block pattern of M must be the same as C                      */
  /*********************************************************************/
  int i, j, k;
  int mj;
  int nnb;
  int rind, cind;
  csptr *csrtab1;
  csptr *csrtab2;
  csrtab1 = (csptr *)malloc(sizeof(csptr)* (L->brj-L->tlj+1));
  csrtab2 = (csptr *)malloc(sizeof(csptr)* (L->brj-L->tlj+1));


#ifdef DEBUG_M
  assert(L->tli == L->tlj);
  assert(L->tli == M->tli);
  assert(L->bri == M->bri);
#endif


  /** Inversion of each block column of M **/

  for(j=M->tlj;j<=M->brj;j++)
    {
      mj = BL->block_index[j+1]-BL->block_index[j];
      for(k=M->cia[j];k<M->cia[j+1];k++)
	{
	  i = M->cja[k];
	  if(i > M->tli)
	    {
	      rind = L->ria[i];
	      cind = M->cia[j];
	      CS_IntersectRow(L->ria[i+1]-rind-1, L->rja + rind, L->ra + rind, 
			      M->cia[j+1]-cind, M->cja + cind, M->ca + cind,
			      &nnb, csrtab1, csrtab2);
	      if(nnb >0)
		CSRrowMultCSRcol(0.0, NULL, -1.0,  nnb, -1.0, csrtab1, csrtab2, 
				 M->ca[k], BL->block_index[j+1]-BL->block_index[j], wki1, wki2, wkd); /*** NO DROPPING ***/
	      

	    }

	  /** Mij = Lii^-1.Mij **/
#ifdef DEBUG_M
	  assert(L->rja[ L->ria[i+1]-1 ] == i);
#endif
	  if(droptab != NULL)
	    CSR_CSR_InvLT( L->ra[ L->ria[i+1]-1], M->ca[k], mj,  droptol, droptab+(BL->block_index[i]-BL->block_index[L->tli]), fillrat, wki1, wki2, wkd);
	  else
	    CSR_CSR_InvLT( L->ra[ L->ria[i+1]-1], M->ca[k], mj,  droptol, NULL, fillrat, wki1, wki2, wkd);
	  
	  
	  
	}
      
    }


  if(csrtab1 != NULL)
    free(csrtab1);
  if(csrtab2 != NULL)
    free(csrtab2);

}



void PHIDAL_InvUT(PhidalMatrix *U, PhidalMatrix *M, double droptol, double *droptab, double fillrat, PhidalHID *BL, Heap *heap, int *wki1, int *wki2, double *wkd)
{
  /*********************************************************************/
  /* this function does M = M.U^-1                                     */
  /* with a dropping criterion drotol                                  */
  /* L is a lower triangular PhidalMatrix                              */
  /* M is a PhidalMatrix                                               */ 
  /*********************************************************************/
 

  int i, j, k;
  int nnb;
  int rind, cind;
  csptr *csrtab1;
  csptr *csrtab2;
  csrtab1 = (csptr *)malloc(sizeof(csptr)* (U->bri-U->tli+1));
  csrtab2 = (csptr *)malloc(sizeof(csptr)* (U->bri-U->tli+1));

#ifdef DEBUG_M
  assert(U->tli == U->tlj);
  assert(U->tlj == M->tlj);
  assert(U->brj == M->brj);
#endif

  /** NOTE: it is very important to do the algorithm in this way 
      for optimize the number of row reallocations in M ***/

  /** Inversion of each block row of M **/

  for(i=M->tli;i<=M->bri;i++)
    {
      for(k=M->ria[i];k<M->ria[i+1];k++)
	{
	  j = M->rja[k];
	  if(j > M->tlj)
	    {

	      cind = U->cia[j];
	      rind = M->ria[i];
	      CS_IntersectRow(M->ria[i+1]-rind, M->rja + rind, M->ra + rind,
			      U->cia[j+1]-cind-1, U->cja + cind, U->ca + cind, 
			      &nnb, csrtab1, csrtab2);
	      if(nnb >0)
		CSRrowMultCSRcol(0.0, NULL, -1.0, nnb, -1.0, csrtab1, csrtab2, 
				 M->ra[k], BL->block_index[j+1]-BL->block_index[j], wki1, wki2, wkd); /*** NO DROPPING ***/
	      
	      
	    }

	  /** Mij = Mij.Ujj^-1**/
	  if(droptab != NULL)
	    CSR_CSR_InvUT( U->ca[ U->cia[j+1]-1], M->ra[k], droptol, droptab+(BL->block_index[j]-BL->block_index[U->tlj]), fillrat, heap, wki1, wki2, wkd);
	  else
	    CSR_CSR_InvUT( U->ca[ U->cia[j+1]-1], M->ra[k], droptol, NULL, fillrat, heap, wki1, wki2, wkd);
	  
	  
	}
      
    }

  if(csrtab1 != NULL)
    free(csrtab1);
  if(csrtab2 != NULL)
    free(csrtab2);

}


void PHIDAL_ICCT_InvLT(PhidalMatrix *L, PhidalMatrix *M, double droptol, double *droptab, double fillrat, PhidalHID *BL, int *wki1, int *wki2, double *wkd)
{
  /*********************************************************************/
  /* this function does Mt = L^-1.Mt                                   */
  /* with a dropping criterion drotol                                  */
  /* L is a lower triangular PhidalMatrix (block matrices are in CSC   */
  /* M is a PhidalMatrix  (blockmatrices are in CSC)                   */ 
  /* The block pattern of M must be the same as C                      */
  /*********************************************************************/
  int i, j, ii;
  int nnb;
  int rind, cind;
  csptr csL;
  csptr *csrtab1;
  csptr *csrtab2;
  

  csrtab1 = (csptr *)malloc(sizeof(csptr)* (L->brj-L->tlj+1));
  csrtab2 = (csptr *)malloc(sizeof(csptr)* (L->brj-L->tlj+1));


#ifdef DEBUG_M
  assert(L->tli == L->tlj);
  assert(L->tlj == M->tlj);
  assert(L->brj == M->brj);
#endif


  /** Inversion of each block column of M **/
  for(j=M->tlj;j<=M->brj;j++)
    {
      if(j>M->tlj)
	for(ii=M->cia[j];ii<M->cia[j+1];ii++)
	  {
	    i = M->cja[ii];
	    
	    /*** Compute M(i,j) = M(i,j) - M(i, 0:j-1).L(j, 0:j-1)t **/	      
	    CS_IntersectRow(M->ria[i+1]-M->ria[i], M->rja + M->ria[i], M->ra + M->ria[i], 
			    L->ria[j+1]-L->ria[j]-1, L->rja + L->ria[j], L->ra + L->ria[j], 
			    &nnb, csrtab1, csrtab2);
	    if(nnb>0)
	      CSCrowMultCSRcol(0.0, NULL, -1.0, nnb, -1.0, csrtab1, csrtab2, M->ca[ii], BL->block_index[i+1]-BL->block_index[i], wki1, wki2, wkd); /*** NO DROPPING HERE ***/
	    
	  }
      
      
      /*** Divide the column block matrices by the diagonal factor ***/
      csL = L->ca[L->cia[j]];
      for(ii=M->cia[j];ii<M->cia[j+1];ii++)
	{
	  i = M->cja[ii];
	  if(droptab != NULL)
	    CSC_CSR_InvLT(csL, M->ca[ii],  BL->block_index[i+1]-BL->block_index[i], droptol, droptab, fillrat, wki1, wki2, wkd);
	  else
	    CSC_CSR_InvLT(csL, M->ca[ii],  BL->block_index[i+1]-BL->block_index[i], droptol, NULL, fillrat, wki1, wki2, wkd);
	}

    }


  if(csrtab1 != NULL)
    free(csrtab1);
  if(csrtab2 != NULL)
    free(csrtab2);

}

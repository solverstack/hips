/* @authors P. HENON */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>

#include "phidal_sequential.h"

/** Local function **/
void invL_thresh(PhidalMatrix *L, int colnum, int ncol,  int *ja, csptr *csrtab, REAL droptol, REAL *droptab, REAL fillrat, PhidalHID *BL, csptr *wkcs1, csptr *wkcs2, int *wki1, int *wki2, REAL *wkd);
void invU_thresh(PhidalMatrix *U, int rownum, int nrow,  int *ja, csptr *csrtab, REAL droptol, REAL *droptab,  REAL fillrat, PhidalHID *BL, Heap *heap, csptr *wkcs1, csptr *wkcs2, int *wki1, int *wki2, REAL *wkd);


#define VERSION_ILUC
#ifdef VERSION_ILUC
void  PhidalMatrix_ILUTP(int pivoting, PhidalMatrix *L, PhidalMatrix *U, REAL droptol, REAL *droptab, REAL fillrat, int *permtab, PhidalHID *BL, PhidalOptions *option)
{
  PhidalMatrix_ILUCT(L, U, droptol, droptab, fillrat, BL, option);
}
#else

void PhidalMatrix_ILUTP(int pivoting, PhidalMatrix *L, PhidalMatrix *U, REAL droptol, REAL *droptab, REAL fillrat, int *permtab, PhidalHID *BL, PhidalOptions *option)
{
  /*******************************************************************/
  /* This function computes an ILUTP factorization of the matrix A.  */
  /* Partial pivoting is allowed in the diagonal block (to allow     */
  /* the pivoting pivoting == 1)                                     */
  /* Fill-in in the factor is only permit in the phidal pattern      */
  /*******************************************************************/
  dim_t i, j, k;
  int ii, jj;
  int *wki1, *wki2;
  REAL *wkd;
  Heap heap;
  csptr csU, csL;
  csptr *csrtab1;
  csptr *csrtab2;
  int nnb;
  REAL *droptabtmp;


#ifdef DEBUG_M
  if(pivoting == 1)
    assert(permtab != NULL);
#endif

#ifdef SYMMETRIC_DROP
  droptabtmp = (REAL *)malloc(sizeof(REAL)*L->dim1);
  if(droptab != NULL)
    memcpy(droptabtmp, droptab, sizeof(REAL)*L->dim1);
  else
    for(i=0;i<L->dim1;i++)
      droptabtmp[i] = 1.0;
#else
  droptabtmp = droptab;
#endif


  /*** Find the largest diagonal block in this matrix ****/
  ii = 0;
  for(i=L->tli;i<=L->bri;i++)
    if(BL->block_index[i+1]-BL->block_index[i] > ii)
      ii = BL->block_index[i+1]-BL->block_index[i];

  Heap_Init(&heap, ii);
  wki1= (int *)malloc(sizeof(int)*ii);
  wki2= (int *)malloc(sizeof(int)*ii);
  wkd = (REAL *)malloc(sizeof(REAL)*ii);
  csrtab1 = (csptr *)malloc(sizeof(csptr)* (L->bri-L->tli+1));
  csrtab2 = (csptr *)malloc(sizeof(csptr)* (L->bri-L->tli+1));

  for(i=L->tli;i<=L->bri;i++)
    {
      csL = L->ra[ L->ria[i+1]-1 ];
      csU = U->ra[ U->ria[i] ];
      
      if(i>L->tli)
	{
	  /*** Compute L( (0,0), ,(i-1, i-1))^1.U((0,i),(i-1,i)) ****/
	  jj = U->cia[i];

	  invL_thresh(L, i-1, U->cia[i+1]-jj-1, U->cja+jj, U->ca+jj, droptol, droptabtmp, fillrat, BL, csrtab1, csrtab2, wki1, wki2, wkd);

	  for(k=U->cia[i];k<U->cia[i+1];k++)
	    {
	      if(U->cja[k] >= i)
		break;
#ifdef REALLOC_BLOCK
	      CSrealloc(U->ca[k]);
#endif
	    }

	  /*** Compute L((i,0),(i,i-1)).U((0,0) ,(i-1, i-1))^1 ****/
	  ii = L->ria[i];
#ifdef DEBUG_M
	  for(k=L->ria[i];k< L->ria[i+1];k++)
	    {
	      if(checkBlock( L->ra[k], BL->block_index[i+1]-BL->block_index[i], BL->block_index[ L->rja[k]+1]- BL->block_index[ L->rja[k]]) != 0)
		{
		  fprintfv(5, stderr, "Error block %d %d \n", i, L->rja[k]);
		  assert(0);
		}
	    }
#endif
	  invU_thresh(U, i-1, L->ria[i+1]-ii-1, L->rja+ii, L->ra+ii, droptol, droptab, fillrat, BL, &heap, csrtab1, csrtab2,  wki1, wki2, wkd);

	  for(k=L->ria[i];k<L->ria[i+1];k++)
	    {
	      if(L->rja[k] >= i)
		break;
#ifdef REALLOC_BLOCK
	      CSrealloc(L->ra[k]);
#endif
	    }

	  /*** Compute U(i,i) = L((i,0),(i,i-1))*U((i,0),(i,i-1)) ***/
	  CS_IntersectRow(L->tli, i-1, 
			  L->ria[i+1]-ii-1, L->rja+ii, L->ra+ii,
			  U->cia[i+1]-jj-1, U->cja+jj, U->ca+jj,
			  &nnb, csrtab1, csrtab2);

	  if(nnb >0)
	    CSRrowMultCSRcol(0.0, NULL, -1.0, nnb, -1.0, csrtab1, csrtab2, 
			     csU, csU->n, wki1, wki2, wkd); /*** NO DROPPING HERE ***/
	      
	      
 
	}



      /**********************************/
      /** Factorize the diagonal block **/
      /**********************************/
      /** csU contains the block diagonal matrix ***/
      ii =  BL->block_index[i] - BL->block_index[L->tli];
      if(pivoting == 1)
	{

	  if(droptab != NULL)
	    CS_ILUTP(csU, csL, csU, option->pivot_ratio, droptol, droptab+ii, option->fillrat, permtab+ii, &heap, wki1, wki2, wkd);
	  else
	    CS_ILUTP(csU, csL, csU, option->pivot_ratio, droptol, NULL, option->fillrat, permtab+ii, &heap, wki1, wki2, wkd);
	  /** NB: The column permutation is NOT done on csU and csL inside CS_ILUTP : the matrices are left in their original numbering ***/ 

	  /*** Permute the column of all the block matrices in L(*,i) and U(*,i) ***/
	  for(k=U->cia[i];k<U->cia[i+1];k++)
	    {
	      csU = U->ca[k];
	      CS_ColPerm(csU, permtab+ii);
	    } 
	  
	  for(k=L->cia[i];k<L->cia[i+1];k++)
	    {
	      csL = L->ca[k];
	      CS_ColPerm(csL, permtab+ii);
	    } 
	  
	}
      else
	{
	  if(droptab != NULL)
	    CS_ILUT(csU, csL, csU, droptol, droptab+ii, option->fillrat, &heap, wki1, wki2, wkd);
	  else
	    CS_ILUT(csU, csL, csU, droptol, NULL, option->fillrat, &heap, wki1, wki2, wkd);


	}

#ifdef REALLOC_BLOCK      
      CSrealloc(csL);
      CSrealloc(csU);
#endif

#ifdef SYMMETRIC_DROP
#ifdef DEBUG_M
      assert(droptabtmp != NULL);
#endif
      dropptr = droptabtmp+ii;
      for(i=0;i<csU->n;i++)
	{
#ifdef DEBUG_M
	  assert(csU->ja[i][0] == i);
#endif
	  dropptr[i] *= fabs(1.0 / csU->ma[i][0]);
	}
#endif


      
    }
#ifdef SYMMETRIC_DROP
  free(droptabtmp);
#endif
  Heap_Exit(&heap);
  free(wki1);
  free(wki2);
  free(wkd);
  free(csrtab1);
  free(csrtab2);
  
  
}

#endif


void invL_thresh(PhidalMatrix *L, int colnum, int ncol,  int *ja, csptr *csrtab, REAL droptol, REAL *droptab, REAL fillrat,  PhidalHID *BL, csptr *wkcs1, csptr *wkcs2, int *wki1, int *wki2, REAL *wkd)
{
  dim_t i, k;
  int nnb;
  int rind;
  int jmin, jmax;

  jmin = L->tli;
  for(k=0;k<ncol;k++)
    {
      i = ja[k];
      if(i>colnum)
	break;

      if(i > L->tli)
	{
	  jmax = i-1;
	  rind = L->ria[i];
	  CS_IntersectRow(jmin, jmax, L->ria[i+1]-rind, L->rja + rind, L->ra + rind, 
			  ncol, ja, csrtab, &nnb, wkcs1, wkcs2);
	  if(nnb >0)
	    CSRrowMultCSRcol(0.0, NULL, -1.0, nnb, -1.0, wkcs1, wkcs2, 
			     csrtab[k], BL->block_index[colnum+2]-BL->block_index[colnum+1], wki1, wki2, wkd); /** NO DROPPING HERE **/
	  
	}
#ifdef SYMMETRIC_DROP
      assert(droptab != NULL);
#endif
      /** Mij = Lii^-1.Mij **/
      if(droptab != NULL)
	CSR_CSR_InvLT( L->ra[ L->ria[i+1]-1], csrtab[k], BL->block_index[i+1] - BL->block_index[i],  droptol, droptab+(BL->block_index[i]- BL->block_index[L->tli]), fillrat, wki1, wki2, wkd);
      else
	CSR_CSR_InvLT( L->ra[ L->ria[i+1]-1], csrtab[k], BL->block_index[i+1] - BL->block_index[i],  droptol, NULL, fillrat, wki1, wki2, wkd);
	  
    }
}



void invU_thresh(PhidalMatrix *U, int rownum, int nrow,  int *ja, csptr *csrtab, REAL droptol, REAL *droptab, REAL fillrat, PhidalHID *BL, Heap *heap, csptr *wkcs1, csptr *wkcs2, int *wki1, int *wki2, REAL *wkd)
{
  dim_t j, k;
  int nnb;
  int cind;
  int jmin, jmax;


  jmin = U->tlj;
  for(k=0;k<nrow;k++)
    {
      j = ja[k];
      if(j>rownum)
	break;

      if(j > U->tlj)
	{
	  jmax = j-1;
	  cind = U->cia[j];
	  CS_IntersectRow(jmin, jmax,  nrow, ja, csrtab, 			  U->cia[j+1]-cind, U->cja + cind, U->ca + cind, 
			  &nnb, wkcs1, wkcs2);
	  if(nnb >0)
	    CSRrowMultCSRcol(0.0, NULL, -1.0, nnb, -1.0, wkcs1, wkcs2, 
			     csrtab[k], BL->block_index[j+1]-BL->block_index[j], wki1, wki2, wkd); /*** NO DROPPING HERE ***/
	  
	}
#ifdef DEBUG_M
      assert(U->rja[ U->ria[j] ] == j);
      if(checkBlock(csrtab[k], BL->block_index[rownum+2]-BL->block_index[rownum+1], BL->block_index[j+1]-BL->block_index[j]) != 0)
	{
	  fprintferr(stderr, "ILU Error block %d %d \n", rownum+1, ja[k]);
	  assert(0);
	}
#endif
      /** Mij = Mij.Ujj^-1**/
      if(droptab != NULL)
	CSR_CSR_InvUT( U->ra[ U->ria[j]], csrtab[k] ,droptol, droptab + (BL->block_index[j] - BL->block_index[U->tlj]), fillrat, heap, wki1, wki2, wkd);  
      else
	CSR_CSR_InvUT( U->ra[ U->ria[j]], csrtab[k] ,droptol, NULL, fillrat, heap, wki1, wki2, wkd);  
    }
      

}

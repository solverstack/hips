/* @authors P. HENON */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>

#include "phidal_sequential.h"



void PhidalMatrix_ILUTSchur(int job, PhidalMatrix *G, PhidalMatrix *W, PhidalMatrix *S, PhidalHID *BL, PhidalOptions *option)
{
  /*********************************************************************/
  /* Compute the schur complement S = S-G*W                            */
  /* Fill-in in S respects the block pattern of S                      */
  /*                                                                   */
  /* PHIDAL block pattern                                              */
  /* If job == 1 then G and W are destroy along the Schur complement   */
  /* computing                                                         */
  /*********************************************************************/
  int  *wki1, *wki2;
  REAL *wkd;
  int i, ii, k, kk;
  int maxB, nnb;
  csptr *csrtab1, *csrtab2;

#ifdef DEBUG_M
  assert(S->tli == S->tlj);
  assert(S->bri == S->brj);
  assert(G->tli == S->tli);
  assert(G->bri == S->bri);
  assert(W->tlj == S->tlj);
  assert(W->brj == S->brj);
#endif


  maxB = 0;
  for(i=S->tli;i<=S->bri;i++)
    if(BL->block_index[i+1]-BL->block_index[i] > maxB)
      maxB = BL->block_index[i+1]-BL->block_index[i];

  wkd = (REAL *)malloc(sizeof(REAL)*maxB);
  wki1 = (int *)malloc(sizeof(int)*maxB);
  wki2 = (int *)malloc(sizeof(int)*maxB);

  csrtab1 = (csptr *)malloc(sizeof(csptr)* (G->brj-G->tlj+1));
  csrtab2 = (csptr *)malloc(sizeof(csptr)* (G->brj-G->tlj+1));


  for(k=S->tlj;k<=S->brj;k++)
    {
      for(kk = S->cia[k]; kk < S->cia[k+1];kk++)
	if(S->cja[kk] == k)
	  break;
#ifdef DEBUG_M
      assert(S->cja[kk] == k);
      assert(S->rja[kk] == k);
#endif

      /*** Compute S(k,k) = S(k,k) - G(k, :).W(:, k) **/
      CSRrowMultCSRcol(0.0, NULL, -1.0, G->ria[k+1]-G->ria[k], 
		       -1.0, G->ra+G->ria[k], W->ca+W->cia[k], 
		       S->ca[kk], BL->block_index[k+1]-BL->block_index[k], wki1, wki2, wkd);


      for(ii=kk+1;ii<S->cia[k+1];ii++)
	{
	  i = S->cja[ii];
	  
	  /*** Compute S(i,k) = S(i,k) - G(i,:).W(:,k) **/	      
	  CS_IntersectRow(G->ria[i+1]-G->ria[i], G->rja + G->ria[i], G->ra + G->ria[i], 
			  W->cia[k+1]-W->cia[k], W->cja + W->cia[k], W->ca + W->cia[k], 
			  &nnb, csrtab1, csrtab2);
#ifdef DEBUG_M
	  assert(nnb <= G->brj-G->tlj+1);
#endif
	  if(nnb>0)
	    CSRrowMultCSRcol(0.0, NULL, -1.0, nnb, -1.0, csrtab1, csrtab2, S->ca[ii], BL->block_index[k+1]-BL->block_index[k], wki1, wki2, wkd); /*** NO DROPPING HERE ***/
	}
      
      for(ii=kk+1;ii<S->ria[k+1];ii++)
	{
	  i = S->rja[ii];
	  
	  /*** Compute S(k,i) = S(k,i) - G(k,:).W(:,i) **/	      
	  CS_IntersectRow(G->ria[k+1]-G->ria[k], G->rja + G->ria[k], G->ra + G->ria[k], 
			  W->cia[i+1]-W->cia[i], W->cja + W->cia[i], W->ca + W->cia[i], 
			  &nnb, csrtab1, csrtab2);
#ifdef DEBUG_M
	  assert(nnb <= G->brj-G->tlj+1);
#endif
	  if(nnb>0)
	    CSRrowMultCSRcol(0.0, NULL, -1.0, nnb, -1.0, csrtab1, csrtab2, S->ra[ii], BL->block_index[i+1]-BL->block_index[i], wki1, wki2, wkd); /*** NO DROPPING HERE ***/
	}
      
      if(job == 1)
	{ 
	  /*** Destroy row block k of G and column block k of W ***/
	  for(i=G->ria[k];i<G->ria[k+1];i++)
	    reinitCS(G->ra[i]);
	  for(i=W->cia[k];i<W->cia[k+1];i++)
	    reinitCS(W->ca[i]);
	}
      
    }
  free(csrtab1);
  free(csrtab2);
  free(wki1);
  free(wki2);
  free(wkd);
}



void PhidalMatrix_ICCTSchur(int job, PhidalMatrix *G, REAL *D, PhidalMatrix *S, PhidalHID *BL, PhidalOptions *option)
{
  /*********************************************************************/
  /* Compute the schur complement S = S-G.D.Gt                         */
  /* Fill-in in S respects the block pattern of S                      */
  /* PHIDAL block pattern                                              */
  /* If job == 1 then G is  destroy along the Schur complement         */
  /* computing                                                         */
  /*********************************************************************/
  int  *wki1, *wki2;
  REAL *wkd;
  REAL *D2;
  csptr *csrtab1, *csrtab2;
  int i, ii, j,jj, k;
  int nnb;
  int maxB;
  PhidalMatrix Grow;


#ifdef DEBUG_M
  assert(S->symmetric == 1);
  assert(S->csc == 1);
  assert(S->tli == S->tlj);
  assert(S->bri == S->brj);
  assert(G->tli == S->tli);
  assert(G->bri == S->bri);
  PhidalMatrix_Check(G, BL);
#endif


  maxB = 0;
  for(i=S->tli;i<=S->bri;i++)
    if(BL->block_index[i+1]-BL->block_index[i] > maxB)
      maxB = BL->block_index[i+1]-BL->block_index[i];


  wkd = (REAL *)malloc(sizeof(REAL)*maxB);
  /*wki1 = (int *)malloc(sizeof(int)*maxB);
    wki2 = (int *)malloc(sizeof(int)*maxB);*/
    /** Need that for CSCrowMultCSRcol (see RowCompact) **/
  wki1= (int *)malloc(sizeof(int)*MAX(BL->nblock, maxB)); 
  wki2= (int *)malloc(sizeof(int)*G->dim2);



  csrtab1 = (csptr *)malloc(sizeof(csptr)* (G->brj-G->tlj+1));
  csrtab2 = (csptr *)malloc(sizeof(csptr)* (G->brj-G->tlj+1));

  D2 = (REAL *)malloc(sizeof(REAL)* G->dim2);
  for(k=S->tlj;k<=S->brj;k++)
    {
      
      /*** Compute L(k,k) = L(k,k) - L(k, 0:k-1).D-1.L(k, 0:k-1)t **/
      nnb = 0;
      ii = 0;
      for(jj=G->ria[k];jj<G->ria[k+1];jj++)
	{
	  j = G->rja[jj];
	  csrtab1[nnb++] = G->ra[jj];
	  memcpy(D2+ii, D+BL->block_index[j]-BL->block_index[G->tlj], sizeof(REAL)* (BL->block_index[j+1]-BL->block_index[j]));
	  ii += BL->block_index[j+1]-BL->block_index[j];
	}
#ifdef DEBUG_M
      assert(nnb >0);
      assert(nnb <= G->brj-G->tlj+1);
      assert(S->cja[S->cia[k]] == k);
#endif

      CSCrowICCprod(0.0, NULL, -1.0, nnb, csrtab1, S->ca[S->cia[k]], D2, wki1, wki2, wkd);/*** NO DROPPING HERE ***/

      for(ii=S->cia[k]+1;ii<S->cia[k+1];ii++)
	{
	  i = S->cja[ii];
	  
#ifdef DEBUG_M
	  assert(S->ca[ii]->n == BL->block_index[k+1]-BL->block_index[k]);
	  CS_Check(S->ca[ii], BL->block_index[i+1]-BL->block_index[i]);
	  assert(i>k);
#endif
	  /*** Compute L(i,k) = L(i,k) - G(i,:).G(k,:)t **/	      
	  CS_IntersectRow(G->ria[i+1]-G->ria[i], G->rja + G->ria[i], G->ra + G->ria[i], 
			  G->ria[k+1]-G->ria[k], G->rja + G->ria[k], G->ra + G->ria[k], 
			  &nnb, csrtab1, csrtab2);
#ifdef DEBUG_M
	  assert(nnb <= G->brj-G->tlj+1);
#endif
	  if(nnb>0)
	    CSCrowMultCSRcol(0.0, NULL, -1.0, nnb, -1.0, csrtab1, csrtab2, S->ca[ii], BL->block_index[i+1]-BL->block_index[i], wki1, wki2, wkd); /*** NO DROPPING HERE ***/

	}


      if(job == 1)
	{ 
	  /*** Destroy the row k of G ***/
	  /*PhidalMatrix_Init(&Grow);
	  PhidalMatrix_Setup(k, G->tlj, k, G->brj, "N", "N" , option->locally_nbr, &Grow, BL);
 	  PhidalMatrix_Cut(G, &Grow, BL);
	  PhidalMatrix_Clean(&Grow);*/

	  for(i=G->ria[k];i<G->ria[k+1];i++)
	    reinitCS(G->ra[i]);
	}
  
    }

  free(csrtab1);
  free(csrtab2);
  free(D2);
  free(wki1);
  free(wki2);
  free(wkd);
}



#ifdef OLD
REAL PhidalMatrix_SchurError(PhidalMatrix *G, PhidalMatrix *W, PhidalMatrix *S, PhidalHID *BL)
{
  dim_t i,j,k;
  int jcol;
  int *isInS;
  REAL schurerr;
  REAL *Gnorm;
  REAL *Wnorm;

  if(G->bloknbr == 0 || W->bloknbr == 0)
    return 0.0; /** No schur complement error **/

  /** Compute the norm of the block of G and W **/
  Gnorm = (REAL *)malloc(sizeof(REAL)*G->bloknbr);
  Wnorm = (REAL *)malloc(sizeof(REAL)*W->bloknbr);
  for(k=0;k<G->bloknbr;k++)
    Gnorm[k] = CSnorm1(G->ra[k]);
  for(k=0;k<G->bloknbr;k++)
    Wnorm[k] = CSnorm1(W->ra[k]);

  isInS = (int *)malloc(sizeof(int)*(BL->nblock));
  bzero(isInS, sizeof(int)*BL->nblock);

  schurerr = 0.0;
  for(i=G->tli;i<G->bri;i++)
    {
      /** Unpack row i of S in isInS **/
      for(k=S->ria[i];k<S->ria[i+1];k++)
	isInS[S->rja[k]] = 1; 
      
      for(k=G->ria[i];k<G->ria[i+1];k++)
	{
	  jcol = G->rja[k];
	  for(j=W->ria[jcol];j<W->ria[jcol+1];j++)
	    if(isInS[W->rja[j]] != 1)
	      {
		/*fprintfv(5, stderr, "BLOCK %d %d is not in pattern \n", i, W->rja[j]);*/
		schurerr += Gnorm[k]*Wnorm[j];
	      }
	}

      /** Reinit isInS **/
      for(k=S->ria[i];k<S->ria[i+1];k++)
	isInS[S->rja[k]] = 0; 
    }
  
  free(Gnorm);
  free(Wnorm);
  free(isInS);
  return schurerr;
}
#endif

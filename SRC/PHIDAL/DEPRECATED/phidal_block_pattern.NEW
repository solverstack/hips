#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "phidal_common.h"



void phidal_block_pattern(int tli, int tlj, int bri, int brj, char *UPLO, char *DIAG, csptr P, int locally_nbr, PhidalHID *BL)
{
  /*************************************************************************/
  /* This function computes the phidal sparse block pattern                */
  /* for the area of the matrix between the block (tli, tlj) and the       */
  /* (bri, brj).                                                           */
  /*                                                                       */
  /*************************************************************************/

  int i, j, k;
  int *block_keyindex;
  int *block_key;
  int lcblknbr;
  int *ja1, *ja2, n1, n2;
  int *tmpptr;
  int uplo;
  int udiag;
  int job, ind; 
  int **domco; /** domconlist[i] is the list of connector in
		       domain i **/
  int *domconbr;
  int domnum, transposed;
  int *tpl;
  int tpln;


#ifdef DEBUG_M
  assert(P->n == BL->nblock);
#endif

  if(bri < 0)
    bri = BL->nblock-1;
  if(brj < 0)
    brj = BL->nblock-1;
  if(tli<0)
    tli = 0;
  if(tlj<0)
    tlj = 0;

  

  transposed = 0;

  if(strcmp(UPLO, "N") == 0)
    {
      if( ! ( (tlj > bri) || (tli > brj) ) )
	{
	  fprintfv(5, stderr, "Error in phidal_block_pattern: Matrix must be in the lower or upper triangular part ! \n");
	  exit(-1);
	}
      uplo = 0;

      if(tli > brj)
	{
	  /*** We need to compute in the upper triangular part ****/
	  /*** so we compute the pattern for the transposed matrice 
	       and at the end we re-transposed the patter      ****/
	  n1 = tli;
	  n2 = bri;
	  tli = tlj;
	  bri = brj;
	  tlj = n1;
	  brj = n2;
	  transposed = 1;

	}

    }
  else
    {
      if(tli != tlj || bri != brj)
	{
	  fprintfv(5, stderr, "Error in phidal_block_pattern:  Matrix must be symmetric \n");
	  exit(-1);
	}
      
      if(strcmp(UPLO, "L") == 0)
	uplo = 1;
      else
	{
	  if(strcmp(UPLO, "U") == 0)
	    uplo = 2;
	  else
	    {
	      fprintfv(5, stderr, "BAD PARAMETERS in phidal_block_pattern \n");
	      exit(-1);
	    }
	}
    }
  
  if(strcmp(DIAG, "N") == 0)
    udiag = 0;
  else
    {
      if(strcmp(DIAG, "U") == 0)
	udiag = 1;
      else
	{
	  fprintfv(5, stderr, "BAD PARAMETERS in phidal_block_pattern \n");
	  exit(-1);
	}
    }	
  
  
 
  if(locally_nbr >= BL->nlevel)
    locally_nbr = BL->nlevel-1;
  
  lcblknbr =  BL->block_levelindex[locally_nbr+1];
  
  block_keyindex = BL->block_keyindex;
  block_key = BL->block_key;
  
  ja1 = (int *)malloc(sizeof(int)* (brj-tlj+1));
  ja2 = (int *)malloc(sizeof(int)* (brj-tlj+1));

  domco = (int **)malloc(sizeof(int *)*BL->ndom);
  domconbr   = (int *)malloc(sizeof(int)*BL->ndom);
  tpl   = (int *)malloc(sizeof(int)*BL->ndom);
 

  /*** WE WANT THE UPER PART OF P in CSR FORMAT ***/
  /** build domco list for all the domain in the connector key of
      connector [tlj:brj] **/
  tpln = 0;
  bzero(domconbr, sizeof(int)*BL->ndom);
  for(j=tlj;j<=brj;j++)
    for(k=block_keyindex[j];k<block_keyindex[j+1];k++)
      {
	domnum =block_key[k]; 
	if(domconbr[block_key[k]] == 0)
	  tpl[tpln++] = domnum; /** Mark the domain that have at least
				   on connector **/
	domconbr[block_key[k]]++;
      }
  for(j=0;j<tpln;j++)
    {
      domnum = tpl[j];
      domco[domnum] = (int *)malloc(sizeof(int)*domconbr[domnum]);
    }
 
  bzero(domconbr, sizeof(int)*BL->ndom);
  for(j=tlj;j<=brj;j++)
    for(k=block_keyindex[j];k<block_keyindex[j+1];k++)
      {
	domnum =block_key[k]; 
	domco[domnum][domconbr[domnum]] = j;
	domconbr[block_key[k]]++;
      }
  

  /** Build the pattern for the upper block triangular pattern  of the matrix **/
  /** Locally consistent part **/
  for(i=tli;i<lcblknbr;i++)
    {
      domnum = block_key[block_keyindex[i]];
      memcpy(ja1, domco[domnum], sizeof(int)*domconbr[domnum]);
      n1 = domconbr[domnum];
      for(k=block_keyindex[i]+1;k<block_keyindex[i+1];k++)
	{
	  domnum = block_key[k];
	  UnionSet(ja1, n1, domco[domnum], domconbr[domnum], ja2, &n2);

	  /** Swap ja1 and ja2 **/
	  tmpptr = ja1;
	  ja1 = ja2;
	  n1 = n2;
	  ja2 = tmpptr;
	}

      P->nnzrow[i] = n1;
      if(n1>0)
	{
	  P->ja[i] = (int *)malloc(sizeof(int)*n1);
	  memcpy(P->ja[i], ja1, sizeof(int)*n1);
	}
    }

  /** Strictly consistent part **/
  for(i=lcblknbr;i<=bri;i++)
    {
      domnum = block_key[block_keyindex[i]];
      memcpy(ja1, domco[domnum], sizeof(int)*domconbr[domnum]);
      n1 = domconbr[domnum];
      for(k=block_keyindex[i]+1;k<block_keyindex[i+1];k++)
	{
	  domnum = block_key[k];
	  IntersectSet(ja1, n1, domco[domnum], domconbr[domnum], ja2, &n2);

	  /** Swap ja1 and ja2 **/
	  tmpptr = ja1;
	  ja1 = ja2;
	  n1 = n2;
	  ja2 = tmpptr;
	  if(n1 == 0)
	    break;
	}

      P->nnzrow[i] = n1;
      if(n1>0)
	{
	  P->ja[i] = (int *)malloc(sizeof(int)*n1);
	  memcpy(P->ja[i], ja1, sizeof(int)*n1);
	}
    }

  job = 0; /** job = 0 only pattern is concerned **/

  /*** Take the part of the pattern that has been asked for ***/
  if(uplo == 0)
    {
      if(tli == tlj && bri == brj)
	{
	  CS_GetUpper(job, DIAG, P, P); /** Only the upper part of P is right
					    due to the locally level **/
	  CS_SymmetrizeTriang(job, "U" , P);
	}
      else
	{
#ifdef DEBUG_M
	  assert(tlj > bri); 
#endif
	  /** The matrix is entirely in the upper
	      part : retranspose to get the orginal
	      if needed**/
	  if(transposed == 1)
	    CS_Transpose(job, P, P->n);
	}
    }


  if(uplo == 1) /** P is only for square matrix: a SYMMETRIC pattern **/
    {
#ifdef DEBUG_M
      assert(tli == tlj && bri == brj);
#endif
      CS_GetUpper(job, DIAG, P, P);
      CS_Transpose(job, P, P->n); 
    }
  if(uplo == 2)
    {
#ifdef DEBUG_M
      assert(tli == tlj && bri == brj);
#endif
      CS_GetUpper(job, DIAG, P, P); 
    }
 


  /*** Fill the ma tab (index of the cs matrix in the PhidalMatrix ***/
  ind = 0;
  for(i=tli;i<=bri;i++)
    if(P->nnzrow[i] > 0)
      {
	P->ma[i] = (double *)malloc(sizeof(double)*P->nnzrow[i]);
	for(j=0;j<P->nnzrow[i];j++)
	  P->ma[i][j] =(double) ind++;
      }

  dumpCS(0, stdout, P);

  for(j=0;j<tpln;j++)
    free(domco[tpl[j]]);
  free(domco);
  free(domconbr);
  free(tpl);
  free(ja1);
  free(ja2);
  CS_SetNonZeroRow(P);
}





int block_in_pattern(int i, int j, int locally_nbr, PhidalHID *BL, int *tmpkey)
{
  /********************************************************/
  /* Return 1 is some fill-in is allowed in the block(i,j)*/
  /*  and 0 otherwise                                     */
  /* Work is a working vector of length equals to the     */
  /* lengthest key possible in te matrix                  */
  /********************************************************/
  int lcblknbr, kl;
#ifdef DEBUG_M
  assert(locally_nbr < BL->nlevel);
#endif

  lcblknbr =  BL->block_levelindex[locally_nbr+1];
  
  if(i>= lcblknbr && j >= lcblknbr)
    {
      /********* The block is in the strictly consistent part of the matrix *****************************/
      
      /**** NOTE: there is a particular case were two connectors i, j in the same level can
	    verify key(i) included in key(j) : we do not allow fill-in in this case 
	    in the strictly level ****/
      
      if(is_intersect_key(BL->block_keyindex[i+1]-BL->block_keyindex[i], BL->block_key+BL->block_keyindex[i],
			  BL->block_keyindex[j+1]-BL->block_keyindex[j], BL->block_key+BL->block_keyindex[j]) == 1)
	{
	  intersect_key(BL->block_keyindex[i+1]-BL->block_keyindex[i], BL->block_keyindex[j+1]-BL->block_keyindex[j],  
			BL->block_key+BL->block_keyindex[i], BL->block_key+BL->block_keyindex[j], tmpkey, &kl);
	  if(kl == BL->block_keyindex[i+1]-BL->block_keyindex[i] || kl == BL->block_keyindex[j+1]-BL->block_keyindex[j])
	    return 1;
	}
      return 0;
    }



  /********* The block is in the locally consistent part of the matrix *****************************/
  if(is_intersect_key(BL->block_keyindex[i+1]-BL->block_keyindex[i], BL->block_key+BL->block_keyindex[i],
		      BL->block_keyindex[j+1]-BL->block_keyindex[j], BL->block_key+BL->block_keyindex[j]) == 1)
    return 1;
  

  return 0;
}

/* @authors P. HENON */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>

#define PARALLEL

#include "phidal_parallel.h"


/** Local functions **/
INTS PhidalDistrPrec_SolveForward(REAL tol, int_t itmax, PhidalDistrPrec *P, COEF *x, COEF *b, PhidalDistrHID *BL, PhidalOptions *option, dim_t *itertab, REAL *resnormtab);

INTS PHIDAL_DistrSolve( PhidalDistrMatrix *A, PhidalDistrPrec *P, PhidalDistrHID *DBL, PhidalOptions *option, COEF *rhs, COEF * x, dim_t *itertab, REAL *resnormtab)
{
  /** There is no flexible PCG so the first level is always
      fgmres **/
  if(option->itmax <= 0)
    {
      /** Solve using a forward backward sweep **/
      PHIDAL_DistrPrecSolve(option->tol, P, x, rhs, DBL, option, itertab, resnormtab);
      itertab[0] = 1;
      resnormtab[0] = -1;
    }
  else
    {
      if(option->krylov_method != 1 || (option->forwardlev > 0 && option->schur_method != 0))
	{
	  CHECK_RETURN(HIPS_DistrFgmresd_PH_PH(option->verbose, option->tol, option->itmax, A, P, DBL, option, rhs, x, option->fd, itertab, resnormtab));
	}
      else
	{
	  CHECK_RETURN(HIPS_DistrPCG_PH_PH(option->verbose, option->tol, option->itmax, A, P, DBL, option, rhs, x, option->fd, itertab, resnormtab));
	}
    }

  /*if(option->verbose > 0 && DBL->proc_id == 0)
    fprintfv(5, stdout, "Number of outer iterations %d \n", itertab[0]);*/
  return HIPS_SUCCESS;
}


INTS PHIDAL_DistrPrecSolve(REAL tol, PhidalDistrPrec *P, COEF *x, COEF *b, PhidalDistrHID *DBL, PhidalOptions *option, dim_t *itertab, REAL *resnormtab)
{
  PhidalDistrMatrix *L, *U;
  PhidalMatrix *LL, *UU;
  PhidalHID *BL;
  int_t itmax;
  dim_t i;
  COEF *D;

  BL = &DBL->LHID;
  L = PREC_L_SCAL(P);
  U = PREC_U_SCAL(P);
  D = P->D;
  LL = &L->M;
  UU = &U->M;


  if(P->forwardlev == 0)
    {
      
      /*if(P->levelnum > 0)*/
	{
	  PhidalDistrMatrix_Lsolve(1, L, x, b, DBL);
	  if(P->symmetric == 1)
	    for(i=0;i<LL->dim1;i++)
	      x[i] *= D[i];
	  
	  if(P->symmetric == 1)
	    PhidalDistrMatrix_Usolve(1, U, x, x, DBL); /** Unitary diagonale **/
	  else
	    PhidalDistrMatrix_Usolve(0, U, x, x, DBL);
	}
	/*else
	{
	  PhidalMatrix_Lsolve(1, LL, x, b, BL);
	  if(P->symmetric == 1)
	    for(i=0;i<LL->dim1;i++)
	      x[i] *= D[i];
	  
	  if(P->symmetric == 1)
	    PhidalMatrix_Usolve(1, UU, x, x, BL);
	  else
	    PhidalMatrix_Usolve(0, UU, x, x, BL); 
	    }*/

      if(P->pivoting == 1)
	/** permute back x **/
	VecInvPermute(LL->tli, LL->bri, x, P->permtab, BL);
    } 
  else 
    {

      if(option->itmax <= 0)
	itmax = -1;
      else
	{
	  itmax = option->itmaxforw;
	  for(i=P->forwardlev; i<option->forwardlev;i++)
	    itmax = (int)(itmax*option->itforwrat);
	}

      CHECK_RETURN(PhidalDistrPrec_SolveForward(tol, itmax, P, x, b, DBL, option, itertab, resnormtab));
      if(P->pivoting == 1)
	/** permute back x **/
	VecInvPermute(LL->tli, LL->bri, x, P->permtab, BL);
    }

  return HIPS_SUCCESS;
}


INTS PhidalDistrPrec_SolveForward(REAL tol, int_t itmax, PhidalDistrPrec *P, COEF *x, COEF *b, PhidalDistrHID *DBL, PhidalOptions *option, dim_t *itertab, REAL *resnormtab)
{
  PhidalDistrMatrix *L, *U;
  PhidalDistrMatrix *S, *E, *F;
  PhidalMatrix *LL, *SS;
  PhidalHID *BL;
  REAL bn, rn;
  COEF *y;
  COEF *D;

  REAL tolrat;

  dim_t i;
  dim_t g;

  L = PREC_L_SCAL(P);
  E = PREC_E(P);
  U = PREC_U_SCAL(P);
  F = PREC_F(P);
  D = P->D;
  S = PREC_S_SCAL(P);

  LL = &L->M;
  SS = &S->M;
  BL = &DBL->LHID;

#ifdef DEBUG_M
  assert(P->dim>0);
#endif
  y = (COEF *)malloc(sizeof(COEF)*P->dim);
  memcpy(y, b, sizeof(COEF)*P->dim);

  /*** Restriction operation ***/
  PhidalDistrMatrix_Lsolve(1, L, x, b, DBL);  
  if(P->symmetric == 1)
    for(i=0;i<L->M.dim1;i++)
      x[i] *= D[i];
  
  if(P->symmetric == 1)
    PhidalDistrMatrix_Usolve(1, U, x, x, DBL); /** unitary diagonale **/
  else
    PhidalDistrMatrix_Usolve(0, U, x, x, DBL);
  
  
  PhidalDistrMatrix_MatVecSub(0, E, DBL, x, y+L->M.dim1);
 
#ifdef DEBUG_M
  if(P->schur_method == 1)
    assert(S->M.dim1 == BL->n - BL->block_index[BL->block_levelindex[P->levelnum]]-LL->dim1);
#endif


  bn = dist_ddot(DBL->proc_id, b, b, LL->tli, LL->bri, DBL);
  rn = dist_ddot(DBL->proc_id, y+LL->dim1, y+LL->dim1, LL->bri+1, BL->nblock-1, DBL);
  tolrat = sqrt(bn)/sqrt(rn);

  if(option->schur_method == 0 || itmax <= 1)
    {
      CHECK_RETURN(PHIDAL_DistrPrecSolve(tol*tolrat, P->nextprec, x+LL->dim1, y+LL->dim1, DBL, option, itertab, resnormtab));
    }
  else
    {
      flag_t verbose = (option->verbose >= 4)?2:0;

      if(option->krylov_method != 1)
	{
	  if(P->schur_method == 2)
	    {
	      CHECK_RETURN(HIPS_DistrFgmresd_PH(verbose, tol*tolrat, itmax, P->nextprec, DBL, option, y+LL->dim1, x+LL->dim1, stdout, itertab, resnormtab));
	    }
	  else
	    {
	      CHECK_RETURN(HIPS_DistrFgmresd_PH_PH(verbose, tol*tolrat, itmax, S, P->nextprec, DBL, option, y+LL->dim1, x+LL->dim1, stdout, itertab, resnormtab));
	    }
	}
      else
	{
	  if(P->schur_method == 2)
	    {
	      CHECK_RETURN(HIPS_DistrPCG_PH(verbose, tol*tolrat, itmax, P->nextprec, DBL, option, y+LL->dim1, x+LL->dim1, stdout, itertab, resnormtab));
	    }
	  else
	    {
	      CHECK_RETURN(HIPS_DistrPCG_PH_PH(verbose, tol*tolrat, itmax, S, P->nextprec, DBL, option, y+LL->dim1, x+LL->dim1, stdout, itertab, resnormtab));
	    }
	}
      
      if(DBL->proc_id == 0)
	{
	  for(g=0;g<P->levelnum+1;g++)
	    fprintfv(1, stdout, "    ");
	  fprintfv(1, stdout, "Level %ld : inner iterations = %ld \n", (long)(P->levelnum+1),(long)itertab[0]);
	}

    }

  /*** Prolongation operation ***/
  PhidalDistrMatrix_MatVecSub(0, F, DBL, x+LL->dim1, y);
  PhidalDistrMatrix_Lsolve(1, L, x, y, DBL);
  if(P->symmetric == 1)
    for(i=0;i<LL->dim1;i++)
      x[i] *= D[i];
  
  if(P->symmetric == 1)
    PhidalDistrMatrix_Usolve(1, U, x, x, DBL); /** unitary diagonale **/
  else
    PhidalDistrMatrix_Usolve(0, U, x, x, DBL);
      
  free(y);
  return HIPS_SUCCESS;
}


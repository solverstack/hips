/* @authors P. HENON */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>
#define PARALLEL
#include "phidal_parallel.h"


void PHIDAL_DistrMLICCT(PhidalDistrMatrix *DA, PhidalDistrPrec *P, PhidalDistrHID *DBL, PhidalOptions *option)
{
  REAL *droptab;
  PhidalMatrix *A;
  PhidalHID *BL;
  dim_t i;

  A = &DA->M;
  BL = &DBL->LHID;
#ifdef DEBUG_M
  assert(A->symmetric == 1);
#endif
  PhidalDistrPrec_Init(P);

  if(option->forwardlev == -1)
    option->forwardlev = BL->nlevel-1;

  if(option->forwardlev >= BL->nlevel || option->forwardlev < 0 )
    {
      option->forwardlev = BL->nlevel-1;
      /*fprintfv(5, stderr, "Invalid parameter : number of forward levels %d total number of level %d \n", option->forwardlev, BL->nlevel);*/
      fprintfv(5, stderr, "WARNING: the number of forwardlev has been reset to %d \n",option->forwardlev);  
    }

#ifdef DEBUG_M
  if(option->forwardlev > 0)
    assert(BL->block_levelindex[BL->nlevel]-BL->block_levelindex[1]  > 0);
#endif
  

  /**** On some processors the last levels could be void we have to set forwardlev 
	to avoid these void levels *****/
  for(i=BL->nlevel-1;i>=0;i--)
    if(BL->block_levelindex[i+1]-BL->block_levelindex[i] == 0)
      {
	option->forwardlev = MIN(i-1, option->forwardlev);
	/*fprintfv(5, stderr, "\n Proc %d : forwardlev = %d \n\n",	DBL->proc_id, option->forwardlev );*/
      }
    else
      break;

  if(option->forwardlev<0)
    {
      fprintfv(5, stderr, "ALL LEVELS ARE VOID !!! %d\n", DBL->proc_id);
      MPI_Abort(DBL->mpicom, -1);
    }


#ifdef DEBUG_M
  assert(option->forwardlev == 0 || option->forwardlev == 1);
  /** Le reste n'est pas implemente **/
  /*assert(option->schur_method == 0 || option->schur_method == 2);*/
#endif




  /*** If droptab != NULL then the dropping will be made according to the norm of the column ***/
  /*if(option->scale > 0)
    {
    droptab = (REAL *)malloc(sizeof(REAL)*A->dim1);
    bzero(droptab, sizeof(REAL)*A->dim1);
    }
    else*/
  droptab = NULL;
  PhidalDistrPrec_MLICCT(0, option->forwardlev, droptab,  DA, P, DBL, option);


  /******************************************************/
  /*  Initialize the communication structure for solve  */
  /******************************************************/
  PhidalCommVec_PrecSetup(P, DBL);

  if(droptab != NULL)
    free(droptab);
}

void  PhidalDistrPrec_MLICCT(int_t levelnum, int forwardlev, REAL *droptab, PhidalDistrMatrix *DA, PhidalDistrPrec *P, PhidalDistrHID *DBL, PhidalOptions *option)
{
  REAL droptol;
  REAL *scaletab=NULL;
  REAL *iscaletab=NULL;
  dim_t i;
  chrono_t t1, t2, ttotal;
  char c;
  PhidalMatrix *A;
  PhidalHID *BL;

  ttotal = 0;

  A = &DA->M;
  BL = &DBL->LHID;

#ifdef DEBUG_M
  assert(A->dim1>0);
#endif

  PhidalDistrPrec_Init(P);


  P->dim = A->dim1;
  P->schur_method = option->schur_method;
  P->forwardlev = forwardlev;


#ifdef SCALE_ALL
  if(option->scale > 0  && levelnum >= 1)/** TO SCALE RECURSIVELY ALL THE LEVEL **/
#else
    if(option->scale > 0 && levelnum == 0)  /** TO SCALE ONLY THE MATRIX A **/
#endif
      {
	scaletab = (REAL *)malloc(sizeof(REAL)*A->dim1);
	iscaletab = (REAL *)malloc(sizeof(REAL)*A->dim1);
	/*** Symmetric scale ***/
	/*PhidalDistrMatrix_ColNorm2(DA, DBL, iscaletab);*/
	/* OR BETTER */
	PhidalDistrMatrix_RowNorm2(DA, DBL, iscaletab);

	/** Take the square root of the norm such that a term 
	    aij = aij / (sqrt(rowi)*sqrt(colj)) **/
	for(i=0;i<A->dim1;i++)
	  iscaletab[i] = sqrt(iscaletab[i]); 
	for(i=0;i<A->dim1;i++)
	  scaletab[i] = 1.0/iscaletab[i];

	PhidalDistrMatrix_RowMult2(scaletab, DA, DBL);
	PhidalDistrMatrix_ColMult2(scaletab, DA, DBL);

	/**** Compute the droptab ******/
	/**** MUST BE THERE : AFTER THE SCALING ****/
	/**** BE CAREFUL USE A DROPTAB ONLY IF A SCALING IS DONE !! ***/
	/*if(droptab != NULL)
	  PhidalDistrMatrix_ColNorm2(DA, DBL, droptab);*/

	/** Temporaire **/
	if(droptab != NULL)
	  PhidalDistrMatrix_RowNorm2(DA, DBL, droptab);
      }

  /********************************************/
  /** Dropping in the Schur complement matrix **/
  /********************************************/
  /** A AJOUTER **/
  /*if(levelnum>0 && option->dropSchur > 0)
    PhidalMatrix_DropT(A, option->dropSchur, droptab, BL);*/

  /*** Compute the numerical threshold to use at this level in the factorization **/
  if(levelnum == 0)
    droptol = option->droptol0;
  else
    {
#ifdef DEBUG_M
      assert(levelnum>=1);
#endif
      droptol = option->droptol1 * pow(option->droptolrat, (REAL)(levelnum-1.0));
    }


  if(forwardlev == 0)
    {
      /**** just factorize the level *****/

      if (levelnum == 0) c = 'M'; else c = 'S';
      fprintfv(5, stdout, "  Numeric Factorisation (%c)\n",c); 
      t1 = dwalltime();
       
      P->symmetric = 1;

      P->levelnum =  levelnum;

      M_MALLOC(P->LU, D(SCAL));
      PREC_L_SCAL(P) = (PhidalDistrMatrix *)malloc(sizeof(PhidalDistrMatrix));
      PREC_U_SCAL(P) = (PhidalDistrMatrix *)malloc(sizeof(PhidalDistrMatrix));
      
      PrecInfo_AddNNZ(P->info, A->dim1);
      P->D = (COEF *)malloc(sizeof(COEF)*A->dim1);

      if(option->pivoting == 1)
	{
	  P->pivoting = 1;
	  P->permtab = (int *)malloc(sizeof(int)*A->dim1);
	  assert(0);
	}

      /*if(option->verbose >= 1)
	fprintfv(5, stdout, "Numerical threshold to factorize ALL levels >= %d = %g \n", levelnum, droptol);	*/

      if(option->schur_method != 1 && levelnum > 0)
	{
	  /** The factorization is done in place (A is a void matrix in return **/
	  PHIDAL_DistrICCT(1, DA, PREC_L_SCAL(P), P->D, droptol, droptab, option->fillrat, DBL, option, P->info);

	  if(P->info != NULL)
	    PrecInfo_SubNNZ(P->info, PhidalDistrMatrix_NNZ(DA));
	  PhidalDistrMatrix_Clean(DA);
	}
      else
	PHIDAL_DistrICCT(0, DA, PREC_L_SCAL(P), P->D, droptol, droptab, option->fillrat, DBL, option, P->info);
       
    

      /*** Construct U as a virtual matrix equal to the transpose of L ***/
      PhidalDistrMatrix_Init(PREC_U_SCAL(P));
      PhidalDistrMatrix_BuildVirtualMatrix(PREC_L_SCAL(P)->M.tli, PREC_L_SCAL(P)->M.tlj, PREC_L_SCAL(P)->M.bri, PREC_L_SCAL(P)->M.brj, PREC_L_SCAL(P), PREC_U_SCAL(P), DBL); 
      PhidalDistrMatrix_Transpose(PREC_U_SCAL(P));


      t2 = dwalltime(); ttotal += t2-t1;
      if(DBL->proc_id == 0)
	fprintfv(5, stdout, "   %c : Numeric Factorisation in %g seconds\n", c, t2-t1);
      goto unscale;
    }

  if(forwardlev == 1 && option->schur_method != 1)
    {
      /** In this case the GEMM and ICCT are done in one step to 
	  save the storage of an intermediate schur complement **/
      PhidalDistrPrec_MLICCTForward(levelnum, droptab, DA, P, DBL, option);
      goto unscale;
    }

  if(forwardlev > 0)
    {
      PhidalDistrPrec_MLICCTForward(levelnum, droptab, DA, P, DBL, option);
      /*** Go to next level ***/
      P->nextprec = (PhidalDistrPrec *)malloc(sizeof(PhidalDistrPrec));
      PhidalDistrPrec_Init(P->nextprec);
      P->nextprec->prevprec = P;

      P->nextprec->info = P->info;

      /** Important to call again PhidalPrec_MLICCT because of scaling **/
      if(droptab != NULL)
	PhidalDistrPrec_MLICCT(levelnum+1, forwardlev-1, droptab+(BL->block_index[PREC_S_SCAL(P)->M.tli]-BL->block_index[A->tli])
			       , PREC_S_SCAL(P), P->nextprec, DBL, option);
      else
	PhidalDistrPrec_MLICCT(levelnum+1, forwardlev-1, NULL, PREC_S_SCAL(P), P->nextprec, DBL, option);

      goto unscale;
    }


 unscale:
   

#ifdef SCALE_ALL
  if(option->scale >  0 && levelnum == 1) /** TO SCALE RECURSIVELY ALL THE LEVEL **/
#else
    if(option->scale >  0 && levelnum == 0) /** TO SCALE ONLY THE MATRIX A **/
#endif
      {
	/** Unscale the matrix and the preconditioner **/
	if(levelnum == 0 || option->schur_method == 1)
	  {
	    PhidalDistrMatrix_ColMult2(iscaletab, DA, DBL);
	    PhidalDistrMatrix_RowMult2(iscaletab, DA, DBL);
	  }

	PhidalDistrPrec_SymmetricUnscale(scaletab, iscaletab, P, DBL);
      
	free(scaletab);
	free(iscaletab);
      }

#ifdef DEBUG_M
  /*if(levelnum == 0)
    PhidalPrec_PrintInfo(P, BL);*/
#endif



  


}

void  PhidalDistrPrec_MLICCTForward(int_t levelnum, REAL *droptab, PhidalDistrMatrix *DA, PhidalDistrPrec *P, PhidalDistrHID *DBL, PhidalOptions *option)
{
  /**** A revoir: liberer le complement de Schur si schur_method == 0 ****/
  int Mstart, Mend, Sstart, Send;
  PhidalDistrMatrix M, G;
  int  *wki1, *wki2;
  COEF *wkd;
  int maxB;
  dim_t i;
  REAL droptol; 
  COEF *D; 
  REAL *droptabtmp;
  chrono_t t1, t2, ttotal;
  char c;
  PhidalMatrix *A;
  PhidalHID *BL;
  INTL nnztmp=-1;

  ttotal = 0;

  A = &DA->M;
  BL = &DBL->LHID;




#ifdef DEBUG_M
  assert(levelnum < BL->nlevel);
#endif

  P->dim = A->dim1;
  P->symmetric = 1;
  P->levelnum =  levelnum;
  Mstart   =  BL->block_levelindex[levelnum];
  Mend     =  BL->block_levelindex[levelnum+1]-1;
  Sstart   =  BL->block_levelindex[levelnum+1];
  Send     =  BL->nblock-1;  

  /** Treat the case where the levelnum is void ! **/
  if(Mstart > Mend)
    {
      fprintfv(5, stderr, "Proc %d: the level %d is void \n", DBL->proc_id, levelnum);
      MPI_Abort(DBL->mpicom, -1);
      exit(-1);
    }

  M_MALLOC(P->LU, D(SCAL));
  PREC_L_SCAL(P) = (PhidalDistrMatrix *)malloc(sizeof(PhidalDistrMatrix));
  PREC_U_SCAL(P) = (PhidalDistrMatrix *)malloc(sizeof(PhidalDistrMatrix));
  PrecInfo_AddNNZ(P->info, A->dim1);
  P->D = (COEF *)malloc(sizeof(COEF)*A->dim1);


  /*** Compute the numerical threshold to use at this level in the factorization **/
  if(levelnum == 0)
    droptol = option->droptol0;
  else
    {
#ifdef DEBUG_M
      assert(levelnum>=1);
#endif
      droptol = option->droptol1 * pow(option->droptolrat, (REAL)(levelnum-1.0));
    }
  /*if(option->verbose >= 1)
    fprintfv(5, stdout, "Numerical threshold to factorize the level %d = %g \n", levelnum, droptol);	*/

  /*** Factorize the Level ***/
  PhidalDistrMatrix_Init(&M);
  PhidalDistrMatrix_BuildVirtualMatrix(Mstart, Mstart, Mend, Mend, DA, &M, DBL); 
  if(option->pivoting == 1)
    {
      P->pivoting = 1;
      P->permtab = (int *)malloc(sizeof(int)*M.M.dim1);
      assert(0);
    }

  M_MALLOC(P->EF, D(SCAL));
  PREC_E_SCAL(P) = (PhidalDistrMatrix *)malloc(sizeof(PhidalDistrMatrix));
  PREC_F_SCAL(P) = (PhidalDistrMatrix *)malloc(sizeof(PhidalDistrMatrix));

  if (levelnum == 0) c = 'M'; else c = 'S';
  fprintfv(5, stdout, "  Numeric Factorisation (%c)\n",c); 
  
  t1  = dwalltime();
  if(option->schur_method != 1 && levelnum > 0)
    {

      PHIDAL_DistrICCT(1, &M, PREC_L_SCAL(P), P->D, droptol, droptab, option->fillrat, DBL, option, P->info);
      PhidalDistrMatrix_Setup(Sstart, Mstart, Send, Mend, "N", "N" , option->locally_nbr, PREC_E(P), DBL);



      PhidalDistrMatrix_Cut(DA, PREC_E(P), DBL);
    }
  else
    {

      PHIDAL_DistrICCT(0, &M, PREC_L_SCAL(P), P->D, droptol, droptab, option->fillrat, DBL, option, P->info);

      PhidalDistrMatrix_BuildVirtualMatrix(Sstart, Mstart, Send, Mend, DA, PREC_E(P), DBL);


    }
  PhidalDistrMatrix_Clean(&M);

  /*** Construct F as a virtual matrix equal to the transpose of E ***/
  PhidalDistrMatrix_Init(PREC_F(P));
  PhidalDistrMatrix_BuildVirtualMatrix(PREC_E(P)->M.tli, PREC_E(P)->M.tlj, PREC_E(P)->M.bri, PREC_E(P)->M.brj, PREC_E(P), PREC_F(P), DBL); 
  PhidalDistrMatrix_Transpose(PREC_F(P));



  t2 = dwalltime(); ttotal += t2-t1;
  fprintfv(5, stdout, "   %c : Numeric Factorisation in %g seconds\n", c, t2-t1);

  /************************************************/
  /* COMPUTE G= Lt^-1.E                           */
  /************************************************/
#ifdef DROP_TRSM
  droptol = option->dropSchur;
#endif

  fprintfv(5, stdout, " TRSM M / E\n");       
  t1  = dwalltime(); 

  maxB = 0;
  for(i=A->tli;i<=A->bri;i++)
    if(BL->block_index[i+1]-BL->block_index[i] > maxB)
      maxB = BL->block_index[i+1]-BL->block_index[i];

  /** Need that for CSCrowMultCSRcol (see RowCompact) **/
  wki1 = (int *)malloc(sizeof(int)*MAX(maxB, BL->nblock));
  wki2 = (int *)malloc(sizeof(int)*A->dim1);
  wkd = (COEF *)malloc(sizeof(COEF)*maxB);

  PhidalDistrMatrix_Init(&G);
  /** IMPORTANT :G is always computed using a consistent block pattern **/
  /*PhidalMatrix_Setup(PREC_E(P)->tli, PREC_E(P)->tlj, PREC_E(P)->bri, PREC_E(P)->brj, "N", "N", BL->nlevel, &G, BL);*/
  PhidalDistrMatrix_Setup(PREC_E(P)->M.tli, PREC_E(P)->M.tlj, PREC_E(P)->M.bri, PREC_E(P)->M.brj, "N", "N", option->locally_nbr, &G, DBL);
  PhidalDistrMatrix_Copy(PREC_E(P), &G, DBL);

  /*
    if(P->info != NULL)
    PrecInfo_AddNNZ(P->info, PhidalDistrMatrix_NNZ(&G));
    if(P->info != NULL)
    nnztmp = - PhidalDistrMatrix_NNZ(&G);*/


  /*if(option->pivoting == 1)*/
  /** Important to pivot G and not E because E is a virtual matrix pointing in the last upper schur complement **/
  /*PhidalDistrMatrix_ColPerm(&G, P->permtab, DBL); */
   
  D = P->D;
  droptabtmp = (REAL *)malloc(sizeof(REAL)*PREC_L_SCAL(P)->M.dim1);
  if(droptab != NULL)
    for(i=0;i<PREC_L_SCAL(P)->M.dim1;i++)
      droptabtmp[i] = droptab[i]*coefabs(1.0/D[i]);
  else
    for(i=0;i<PREC_L_SCAL(P)->M.dim1;i++)
      droptabtmp[i] = coefabs(1.0/D[i]);
  
  PHIDAL_ICCT_InvLT(&(PREC_L_SCAL(P)->M), &G.M, option->droptolE, droptabtmp, option->fillrat, BL, wki1, wki2, wkd);
  if(P->info != NULL)
    {
      /*nnztmp += PhidalDistrMatrix_NNZ(&G);*/
      PrecInfo_AddNNZ(P->info, PhidalDistrMatrix_NNZ(&G));
    }


  free(droptabtmp);
   
  /*** Construct U as a virtual matrix equal to the transpose of L ***/
  PhidalDistrMatrix_Init(PREC_U_SCAL(P));
  PhidalDistrMatrix_BuildVirtualMatrix(PREC_L_SCAL(P)->M.tli, PREC_L_SCAL(P)->M.tlj, PREC_L_SCAL(P)->M.bri, PREC_L_SCAL(P)->M.brj, PREC_L_SCAL(P), PREC_U_SCAL(P), DBL); 
  PhidalDistrMatrix_Transpose(PREC_U_SCAL(P));

  free(wki1);
  free(wki2);
  free(wkd);

  t2  = dwalltime(); ttotal += t2-t1;
  fprintfv(5, stdout, "  TRSM in %g seconds\n\n", t2-t1);
   
  /*** Compute the schur complement on the remaining levels ****/
  fprintfv(5, stdout, " GEMM E/E -> S\n"); 
  t1  = dwalltime(); 

  M_MALLOC(P->S,  D(SCAL));
  PREC_S_SCAL(P) = (PhidalDistrMatrix *)malloc(sizeof(PhidalDistrMatrix));

  if(option->schur_method != 1)
    PhidalDistrMatrix_Setup(Sstart, Sstart, Send, Send, "L", "N", option->locally_nbr, PREC_S_SCAL(P), DBL);
  else
    PhidalDistrMatrix_Setup(Sstart, Sstart, Send, Send, "L", "N", BL->nlevel, PREC_S_SCAL(P), DBL);
  
  if(option->schur_method == 2)
    {
      M_MALLOC(P->B,  D(SCAL));
      PREC_B_SCAL(P) = (PhidalDistrMatrix *)malloc(sizeof(PhidalDistrMatrix));
      PhidalDistrMatrix_Init(PREC_B_SCAL(P));
      if(levelnum == 0)
	PhidalDistrMatrix_BuildVirtualMatrix(Sstart, Sstart, Send, Send, DA, PREC_B_SCAL(P), DBL);
      else
	{
	  PhidalDistrMatrix_Setup(Sstart, Sstart, Send, Send, "L", "N",
			     option->locally_nbr, PREC_B_SCAL(P), DBL);
	  /*PhidalMatrix_Setup(Sstart, Sstart, Send, Send, "L", "N", BL->nlevel, PREC_B_SCAL(P), BL);*/
	  PhidalDistrMatrix_Copy(DA, PREC_B_SCAL(P), DBL);
	  if(P->info != NULL)
	    PrecInfo_AddNNZ(P->info, PhidalDistrMatrix_NNZ(PREC_B_SCAL(P)));

	}
    }

  if(option->schur_method != 1 && levelnum > 0)
    PhidalDistrMatrix_Cut(DA, PREC_S_SCAL(P), DBL);
  else
    {
      PhidalDistrMatrix_Copy(DA, PREC_S_SCAL(P), DBL);
      if(P->info != NULL)
	PrecInfo_AddNNZ(P->info, PhidalDistrMatrix_NNZ(PREC_S_SCAL(P)));
    }


  if(option->schur_method != 1 && levelnum > 0)
    {
      if(P->info != NULL)
	PrecInfo_SubNNZ(P->info, PhidalDistrMatrix_NNZ(DA));
      PhidalDistrMatrix_Clean(DA);
    }

  if(option->forwardlev == levelnum+1 && option->schur_method != 1)
    {
      P->nextprec = (PhidalDistrPrec *)malloc(sizeof(PhidalDistrPrec));
      PhidalDistrPrec_Init(P->nextprec);
      P->nextprec->prevprec = P;
      P->nextprec->info = P->info;

      
      /** OIMBE Passer Precinfo dans cette fonction ***/
      PhidalDistrPrec_GEMM_ICCT(levelnum+1, droptab, &G, PREC_S_SCAL(P), P->D, P->nextprec, DBL, option);

      if(P->info != NULL)
	PrecInfo_SubNNZ(P->info, PhidalDistrMatrix_NNZ(PREC_S_SCAL(P)));
      PhidalDistrMatrix_Clean(PREC_S_SCAL(P));
      free(PREC_S_SCAL(P));
      PREC_S_SCAL(P) = NULL;
    }
  else
    {

      /** G is destroyed inside this function **/
      /** Local schur product !! (no need to implement a parallel
	  version **/
      
      if(P->info != NULL)
	nnztmp =  - PhidalDistrMatrix_NNZ(PREC_S_SCAL(P));
      PhidalMatrix_ICCTSchur(1, &G.M, P->D, &PREC_S_SCAL(P)->M, BL, option);
      if(P->info != NULL)
	{
	  nnztmp += PhidalDistrMatrix_NNZ(PREC_S_SCAL(P));
	  PrecInfo_AddNNZ(P->info, nnztmp);
	}

      if(P->info != NULL)
	nnztmp =  - PhidalDistrMatrix_NNZ(PREC_S_SCAL(P));
      PhidalDistrMatrix_GatherCoef(0, 1, PREC_S_SCAL(P), DBL);
      if(P->info != NULL)
	{
	  nnztmp +=  PhidalDistrMatrix_NNZ(PREC_S_SCAL(P));
	  PrecInfo_AddNNZ(P->info, nnztmp);
	}
    }
  if(P->info != NULL)
    PrecInfo_SubNNZ(P->info, PhidalDistrMatrix_NNZ(&G));
  PhidalDistrMatrix_Clean(&G);
  
  t2  = dwalltime(); ttotal += t2-t1;
  fprintfv(5, stdout, "  GEMM in %g seconds\n\n", t2-t1);
}




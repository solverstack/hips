/* @authors P. HENON */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>

#define PARALLEL

#include "phidal_parallel.h"




void PhidalDistrPrec_Info(PhidalDistrPrec *p)
{
/*** A DEBUGGER **/
  fprintfd(stdout, "-------Level %d NNZ ----------------\n", p->levelnum);
  
  {
    fprintfd(stdout, "L(%d) : %ld \n", p->levelnum, PhidalDistrMatrix_NNZ(PREC_L_SCAL(p)));
    if(p->symmetric == 0) {
      fprintfd(stdout, "U(%d) : %ld \n", p->levelnum, PhidalDistrMatrix_NNZ(PREC_U_SCAL(p)));
    }
    else {
      fprintfd(stdout, "D(%d) : %ld \n", p->levelnum, (long)PREC_L_SCAL(p)->M.dim1);
    }
  }
  
  if(p->forwardlev > 0)
    {
      fprintfd(stdout, "E(%d) : %ld \n", p->levelnum, PhidalDistrMatrix_NNZ(PREC_E(p)));
      if(p->symmetric == 0)
	fprintfd(stdout, "F(%d) : %ld \n", p->levelnum, PhidalDistrMatrix_NNZ(PREC_F(p)));


      if(p->schur_method == 1) 
	fprintfd(stdout, "S(%d) : %ld \n",  p->levelnum, PhidalDistrMatrix_NNZ(PREC_S_SCAL(p)));

      if(p->schur_method == 2) 
	fprintfd(stdout, "C(%d) : %ld \n",  p->levelnum, PhidalDistrMatrix_NNZ(PREC_B(p)));

      PhidalDistrPrec_Info(p->nextprec);
    }
  fprintfd(stdout, "------------------------------------\n");
}



void PhidalDistrPrec_SymmetricUnscale(REAL *scaletab, REAL *iscaletab, PhidalDistrPrec *P, PhidalDistrHID *DBL)
{
  dim_t i;
  COEF *D;

  /*****************************************************/
  /*  A = Dr^-1.A'.Dr^-1                               */
  /*  A = Dr^-1.L'.D'.L't.Dr^-1                        */
  /*  A = Dr^-1.L'.Dr . Dr^-1.D'.Dr^-1  . Dr.L't.Dr^-1 */
  /*  A =     L       .      D          . Lt           */
  /*****************************************************/

#ifdef DEBUG_M
  assert(P->symmetric == 1);
  assert(P->D != NULL);
#endif

  if(P->forwardlev == 0)
    {

      PhidalDistrMatrix_RowMult2(iscaletab, PREC_L_SCAL(P), DBL);
      PhidalDistrMatrix_ColMult2(scaletab, PREC_L_SCAL(P), DBL);
      D = P->D;
      for(i=0;i<PREC_L_SCAL(P)->M.dim1;i++)
	D[i] *= scaletab[i]*scaletab[i]; /** This is the inverse of the diagonal **/
      return;
    }
  
  if(P->forwardlev > 0)
    {
      PhidalDistrPrec_SymmetricUnscale(scaletab + PREC_L_SCAL(P)->M.dim1, iscaletab + PREC_L_SCAL(P)->M.dim1, P->nextprec, DBL);
      PhidalDistrMatrix_RowMult2(iscaletab, PREC_L_SCAL(P), DBL);
      PhidalDistrMatrix_ColMult2(scaletab, PREC_L_SCAL(P), DBL);

      D = P->D;
      for(i=0;i<PREC_L_SCAL(P)->M.dim1;i++)
	D[i] *= scaletab[i]*scaletab[i]; /** This is the inverse of the diagonal **/
	  
      if(PREC_E(P)->M.virtual == 0)
	{
	  PhidalDistrMatrix_RowMult2(iscaletab + PREC_L_SCAL(P)->M.dim1, PREC_E(P), DBL);
	  PhidalDistrMatrix_ColMult2(iscaletab, PREC_E(P), DBL);
	} 
 
      if(P->schur_method == 1)
	{
#ifdef DEBUG_M
	  assert(PREC_S_SCAL(P)->M.virtual == 0);
#endif
	  PhidalDistrMatrix_RowMult2(iscaletab + PREC_L_SCAL(P)->M.dim1, PREC_S_SCAL(P), DBL);
	  PhidalDistrMatrix_ColMult2(iscaletab + PREC_L_SCAL(P)->M.dim1, PREC_S_SCAL(P), DBL);
	}

      if(P->schur_method == 2 && P->levelnum > 0)
	{
#ifdef DEBUG_M
	  assert(PREC_B(P)->M.virtual == 0);
#endif
	  PhidalDistrMatrix_RowMult2(iscaletab + PREC_L_SCAL(P)->M.dim1, PREC_B(P), DBL);
	  PhidalDistrMatrix_ColMult2(iscaletab + PREC_L_SCAL(P)->M.dim1, PREC_B(P), DBL);
	}
      

      return;
    }

  return;
 
}

void PhidalDistrPrec_UnsymmetricUnscale(REAL *scalerowtab, REAL *iscalerowtab, REAL *scalecoltab, REAL *iscalecoltab, PhidalDistrPrec *P, PhidalDistrHID *DBL)
{

  /*****************************************************/
  /*  A = Dr^-1.A'.Dc^-1                               */
  /*  A = Dr^-1.L'.U'.Dc^-1                            */
  /*  A = Dr^-1.L'.Dr   .   Dr^-1.U'.Dc^-1             */
  /*  A =     L       .      U                         */
  /*****************************************************/

  dim_t i;
  COEF *D;
 
#ifdef DEBUG_M
  assert(P->symmetric == 0);
  assert(P->D == NULL);
#endif

  if(P->forwardlev == 0)
    {
      PhidalDistrMatrix_RowMult2(iscalerowtab, PREC_L_SCAL(P), DBL);
      PhidalDistrMatrix_ColMult2(scalerowtab, PREC_L_SCAL(P), DBL);
      D = (COEF *)malloc(sizeof(COEF)*PREC_L_SCAL(P)->M.dim1);

      PhidalDistrMatrix_GetUdiag(PREC_U_SCAL(P), D);
      PhidalDistrMatrix_RowMult2(iscalerowtab, PREC_U_SCAL(P), DBL);
      PhidalDistrMatrix_ColMult2(iscalecoltab, PREC_U_SCAL(P), DBL);

      for(i=0;i<PREC_U_SCAL(P)->M.dim1;i++)
	D[i] *= scalerowtab[i]*scalecoltab[i]; /** D is the inverse of the diagonal **/
      PhidalDistrMatrix_SetUdiag(PREC_U_SCAL(P), D);
      
      free(D);
      return;
    }
  


  if(P->forwardlev > 0)
    {
      PhidalDistrPrec_UnsymmetricUnscale(scalerowtab + PREC_L_SCAL(P)->M.dim1, iscalerowtab + PREC_L_SCAL(P)->M.dim1, scalecoltab+PREC_U_SCAL(P)->M.dim1, iscalecoltab+PREC_U_SCAL(P)->M.dim1, P->nextprec, DBL);
      
      PhidalDistrMatrix_RowMult2(iscalerowtab, PREC_L_SCAL(P), DBL);
      PhidalDistrMatrix_ColMult2(scalerowtab, PREC_L_SCAL(P), DBL);

      D = (COEF *)malloc(sizeof(COEF)*PREC_L_SCAL(P)->M.dim1);
      PhidalDistrMatrix_GetUdiag(PREC_U_SCAL(P), D);
      
      PhidalDistrMatrix_RowMult2(iscalerowtab, PREC_U_SCAL(P), DBL);
      PhidalDistrMatrix_ColMult2(iscalecoltab, PREC_U_SCAL(P), DBL);

      for(i=0;i<PREC_U_SCAL(P)->M.dim1;i++)
	D[i] *= scalerowtab[i]*scalecoltab[i]; /** D is the inverse of the diagonal **/
      PhidalDistrMatrix_SetUdiag(PREC_U_SCAL(P), D);
      free(D);

      if(PREC_E(P)->M.virtual == 0)
	{
	  PhidalDistrMatrix_RowMult2(iscalerowtab + PREC_L_SCAL(P)->M.dim1, PREC_E(P), DBL);
	  PhidalDistrMatrix_ColMult2(iscalecoltab, PREC_E(P), DBL);
	} 
      
      if(PREC_F(P)->M.virtual == 0)
	{
	  PhidalDistrMatrix_RowMult2(iscalerowtab, PREC_F(P), DBL);
	  PhidalDistrMatrix_ColMult2(iscalecoltab + PREC_L_SCAL(P)->M.dim1, PREC_F(P), DBL);
	} 
 

      if(P->schur_method == 1)
	{
#ifdef DEBUG_M
	  assert(PREC_S_SCAL(P)->M.virtual == 0);
#endif
	  PhidalDistrMatrix_RowMult2(iscalerowtab + PREC_L_SCAL(P)->M.dim1, PREC_S_SCAL(P), DBL);
	  PhidalDistrMatrix_ColMult2(iscalecoltab + PREC_L_SCAL(P)->M.dim1, PREC_S_SCAL(P), DBL);
	}

      if(P->schur_method == 2 && P->levelnum > 0)
	{
#ifdef DEBUG_M
	  assert(PREC_B(P)->M.virtual == 0);
#endif
	  PhidalDistrMatrix_RowMult2(iscalerowtab + PREC_L_SCAL(P)->M.dim1, PREC_B(P), DBL);
	  PhidalDistrMatrix_ColMult2(iscalecoltab + PREC_L_SCAL(P)->M.dim1, PREC_B(P), DBL);
	}


      return;
    }

  return;
 
}







/* @authors P. HENON */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>

#include "phidal_parallel.h"

#ifndef DUMP
#define DEBUG_DUMP(k, comment)
#else
#define DEBPROC 0
#define DEBUG_DUMP(k, comment) { if ((START != 0) && ((k)== 0) && (strcmp("FIN", #comment) == 0)) { printf("DEBUG_DUMP : it %d %s\n", k, #comment); dump(L); } }



/* #define DEBUG_DUMP(k, comment) { diagdump2(D, DBL, k, #comment); } */
void diagdump2(COEF* D, PhidalDistrHID* DBL, int k, char* comment) {
  int ln = DBL->LHID.n;
  dim_t i;
  mpi_t proc_id;
  
  MPI_Comm_rank(DBL->mpicom, &proc_id);
   
  char filename[500];
  sprintf(filename, "ph/diag-proc%d-k%d-%s.txt", proc_id, k, comment);

  FILE* stream = fopen(filename, "w"); 
 
  /*   if (DBL.proc_id == 0) { */
 
  for(i=0; i<ln; i++) {
    fprintf(stream, "%.14e\n", 1/D[i]);
  }
  fclose(stream);
  /*   } */
  
}

void dump(PhidalDistrMatrix* L) {
  mpi_t proc_id;
  static int count = 0;

  MPI_Comm_rank(DBL->mpicom, &proc_id);
  if (proc_id != DEBPROC) return;
 
  printf("Dump ... %d\n", proc_id);
  {
    char filename[100];
    sprintf(filename, "ph-%d-n%d.txt", proc_id, count);
    
    FILE* stream = fopen(filename, "w");
    /* dumpPhidalMatrix(stream, &L->M); */
    fclose(stream);
  }
  printf("end - - Dump ... %d\n", proc_id);

  exit(1);
}

#endif

void Poste_DiagRcv(int start, int end, COEF *D, MPI_Request *diag_rqtab,  PhidalDistrMatrix *L, PhidalDistrHID *DBL);

void PhidalDistrMatrix_ICCT(flag_t job, PhidalDistrMatrix *L, COEF *D, REAL droptol, REAL *droptab, REAL fillrat, PhidalDistrHID *DBL, PhidalOptions *option, PrecInfo *info)
{
  /******************************************************************************************/
  /* This function computes the incomplete factorization A = L.D.Lt of a symmetric matrix   */
  /* On entry:                                                                              */
  /*   L is the lower triangular part of the matrix to factorize in CSC format              */
  /*   job == 0 : the shared block are freed on the non-leader                              */
  /*   processors otherwise they stay in memory (need this when to use                      */
  /* L^-1.M in multilevel recursion)                                                        */
  /* on return:                                                                             */
  /* L the strictly lower triangular factor (unitary on the diagonal not stored) in CSC     */
  /* D is the INVERSE of the diagonal factor                                                */
  /* NOTE: L is sorted by row index on return                                               */
  /******************************************************************************************/
#ifdef DEBUG_M
  assert(L->M.symmetric == 1);
#endif
  PhidalDistrMatrix_ICCT_Restrict(L->M.tli, job, L, D, droptol, droptab, fillrat,DBL, option, info);
}

void PhidalDistrMatrix_ICCT_Restrict(int START, flag_t job, PhidalDistrMatrix *L, COEF *D, REAL droptol, REAL *droptab, REAL fillrat, PhidalDistrHID *DBL, PhidalOptions *option, PrecInfo *info)
{
  /************************************************/
  /* THE BLOCK ARE FREED IN THE COUPLING MATRIX   */
  /* ie in ALL ROW BEFORE START                   */
  /************************************************/

  dim_t i, j, k;
  int nk, *jak, *rindk;
  csptr *rak;
  int ii, jj, kk;
  int *wki1, *wki2;
  COEF *wkd;
  csptr csL;
  csptr *csrtab1, *list1;
  csptr *csrtab2, *list2;
  int *listindex;
  REAL *droptabtmp;
  COEF *D2;
  int nnb;
  CellCSDistr *firstcol;
  cell_int *celltab;
  cell_int **cellptrtab;
  PhidalFactComm FC;
  PhidalHID *BL;
  PhidalMatrix *LL;
  MPI_Request *diag_rqtab;

#ifdef DEBUG_M
  int toto = 0;
  int titi = 0;
#endif

  LL = &(L->M);
  BL = &(DBL->LHID);

  DEBUG_DUMP(0, INIT);

#ifdef DEBUG_M
  assert(COMM_AHEAD >= 1);
  assert(START >= LL->tli);
  assert(START <= LL->bri);
  PhidalDistrMatrix_Check(L, DBL);
#endif

  /*** Allocate the communicator of the matrix ***/
  PhidalFactComm_Setup(&FC, L, DBL);

#ifdef TRACE_COM
  for(i=LL->tli;i<=LL->bri;i++)
    for(j=LL->cia[i];j<LL->cia[i+1];j++)
      {
	if(L->cind[j] < 0)
	  {
	  fprintfd(stderr, "Block L (%d %d) lead %d \n",  DBL->loc2glob_blocknum[LL->cja[j]], DBL->loc2glob_blocknum[i], L->clead[j]);
	  }
	else
	  {
	    fprintfd(stderr, "Block L (%d %d) lead %d CIND %d nbr %d \n",  DBL->loc2glob_blocknum[LL->cja[j]], DBL->loc2glob_blocknum[i], L->clead[j], L->cind[j], L->pset_index[L->cind[j]+1]-L->pset_index[L->cind[j]]);
	  }
      }
#endif


#ifdef GROS_DEBUG
  /********** DEBUG ******/
  for(i=LL->tli;i<=LL->bri;i++)
    for(j=LL->cia[i];j<LL->cia[i+1];j++)
      /*if(L->clead[j]  == DBL->proc_id)*/
      fprintfv(5, stderr, "Proc %d Block [%d %d] = %e \n", DBL->proc_id,   DBL->loc2glob_blocknum[LL->cja[j]], DBL->loc2glob_blocknum[i], CSnormFrob(LL->ca[j]));
  MPI_Barrier(DBL->mpicom);
  /*exit(0);*/
#endif

#ifdef COMM_ONE
  /*** Poste receive for contribution blocks **/ 
  PhidalFactComm_PosteCtrbReceive(&FC, L, START, START, LL->bri, LL->brj);
#endif

  /*** Create request for receive of diagonal term from leader
       processors ***/
  diag_rqtab = (MPI_Request *)malloc(sizeof(MPI_Request)*(LL->bri-START+1));
  Poste_DiagRcv(START, LL->bri, D, diag_rqtab, L, DBL);

  /*** Find the largest diagonal block in this matrix ****/
  ii = 0;
  for(i=START;i<=LL->bri;i++)
    if(BL->block_index[i+1]-BL->block_index[i] > ii)
      ii = BL->block_index[i+1]-BL->block_index[i];
  wkd = (COEF *)malloc(sizeof(COEF)*ii);
  /*wki1= (int *)malloc(sizeof(int)*ii);
    wki2= (int *)malloc(sizeof(int)*ii);*/

  /** Need that for CSCrowMultCSRcol (see RowCompact) **/
  wki1= (int *)malloc(sizeof(int)*MAX(BL->nblock, ii)); 
  wki2= (int *)malloc(sizeof(int)*LL->dim1);

  /*** Find the maximum row cumulate block size ****/
  nk = 0;
  for(i=START;i<=LL->bri;i++)
    {
      ii = 0;
      for(j=LL->ria[i];j<LL->ria[i+1];j++)
	ii+= LL->ra[j]->n;
      if(ii > nk)
	nk = ii;
    }
  
  if(nk>0)
    celltab = (cell_int *)malloc(sizeof(cell_int)*nk);
  else
    celltab = NULL;

  /** Find the maximum row dimension of a block **/
  nk = 0;
  for(i=START;i<=LL->bri;i++)
    {
      ii = BL->block_index[i+1]-BL->block_index[i];
      if(ii>nk)
	nk = ii;
    }
  cellptrtab = (cell_int **)malloc(sizeof(cell_int *)*nk);

  ii = 0;

  for(k=START;k<=LL->bri;k++)
    if(LL->ria[k+1] - LL->ria[k]-1 > ii)
      ii = LL->ria[k+1] - LL->ria[k]-1;

  if(ii>0)
    {
      jak = (int *)malloc(sizeof(int)*ii);
      rindk = (int *)malloc(sizeof(int)*ii);
      rak = (csptr *)malloc(sizeof(csptr)*ii);
    }
  else
    {
      jak = NULL;
      rindk = NULL;
      rak = NULL;
    }

  ii=0;

  for(k=START;k<=LL->brj;k++)
    {
      nnb = (LL->ria[k+1]-LL->ria[k] - 1) * (LL->cia[k+1]-LL->cia[k]);
      if( nnb > ii)
	ii = nnb;
    }

  list1 = (csptr *)malloc(sizeof(csptr)* (ii+1));
  list2 = (csptr *)malloc(sizeof(csptr)* (ii+1));
  listindex = (int *)malloc(sizeof(int)*(LL->bri-START+1));


  firstcol = (CellCSDistr *)malloc(sizeof(CellCSDistr)*(LL->brj+1));
  for(k=LL->tlj;k<=LL->brj;k++)
    {
      firstcol[k].nnz = LL->cia[k+1]-LL->cia[k];
      firstcol[k].ja  = LL->cja + LL->cia[k];
      firstcol[k].pind = L->cind + LL->cia[k];
      firstcol[k].ma  = LL->ca + LL->cia[k];
    }


  D2 = (COEF *)malloc(sizeof(COEF)*LL->dim1);

  droptabtmp = (REAL *)malloc(sizeof(REAL)*LL->dim1);
  if(droptab != NULL)
    memcpy(droptabtmp, droptab, sizeof(REAL)*LL->dim1);
  else
    for(i=0;i<LL->dim1;i++)
      droptabtmp[i] = 1.0;

#ifndef OLD_COMM_AHEAD
  PhidalFactComm_PosteCtrbReceive(&FC, L, START, MIN(START+COMM_AHEAD-1, LL->brj));
#endif
  

  for(k=START;k<=LL->brj;k++)
    {
      /*** Poste receive for contribution blocks in this column **/ 
      /*fprintfv(5, stderr, "Poste ctrb for column block %d \n", k);*/
#ifdef OLD_COMM_AHEAD
      if ((k-START) % COMM_AHEAD == 0)
	PhidalFactComm_PosteCtrbReceive(&FC, L, k, MIN(k+COMM_AHEAD-1, LL->bri));
#else
      if(k+COMM_AHEAD <= LL->bri)
	PhidalFactComm_PosteCtrbReceive(&FC, L, k+COMM_AHEAD, MIN(k+COMM_AHEAD, LL->bri));
#endif
      /*fprintfv(5, stderr, "Poste diag for column block %d \n", k);
	Poste_DiagRcv(k, k, D, diag_rqtab, L, DBL);*/



      /*** Factorize the column block k ***/
      csL =  LL->ca[ LL->cia[k] ];

      /*** OIMBE: il faut enlever ca pour que ca marche 
	   en multiniveau; sinon ca deadlock
	   if(k>LL->tlj) ***/
      {
	/**************************************************/
	/* L(k,k) = L(k,k) - L(k, 0:k-1).D-1.L(k,0:k-1)t **/
	/**************************************************/

	/** Find the local block in row L(k, :) **/
	nk = 0;
	for(jj=LL->ria[k];jj<LL->ria[k+1]-1;jj++) 
	  if(L->rlead[jj] == L->proc_id && LL->ra[jj]->nnzr > 0)
	    {
	      jak[nk] = LL->rja[jj];
	      rak[nk] = LL->ra[jj];

	      nk++;
	    }
	if(nk>0)
	  {
	    /*** Compute the dropping tab ***/
	    ii = 0;
	    for(jj=0;jj<nk;jj++)
	      {
		j = jak[jj];
		memcpy(D2+ii, D+BL->block_index[j]-BL->block_index[LL->tlj], 
		       sizeof(COEF)*(BL->block_index[j+1]-BL->block_index[j]));
		ii += BL->block_index[j+1]-BL->block_index[j];
	      } 
	      
	    /** Compute the product  
		L(k,k) = L(k,k) - L(k,0:k-1).D-1.L(k, 0:k-1)t  ONLY
		for LOCAL block **/
#ifdef DEBUG_M
	    titi += nk;
	    toto += nk;
#endif
	    if(info != NULL)
	      PrecInfo_SubNNZ(info, CSnnz(csL));
	    CSCrowICCprod(0.0, NULL, -1.0, nk, rak, csL, D2, wki1, wki2, wkd, celltab, cellptrtab);  /*** NO DROPPING HERE ***/
	    if(info != NULL)
	      PrecInfo_AddNNZ(info, CSnnz(csL));

	  }

	/*** The CSCrowICCprod function also divides the block by the
	     diagonal; so we have to do it for non local block that
	     have not been processed by ICCprod;
	     CSCrowICCprod also sorts the matrix ***/
	for(jj=LL->ria[k];jj<LL->ria[k+1]-1;jj++) 
	  if(L->rlead[jj] != L->proc_id && LL->ra[jj]->nnzr > 0)
	    {
	      j = LL->rja[jj];
	      kk = BL->block_index[j] - BL->block_index[LL->tli];
	      ascend_column_reorder(LL->ra[jj]);
	      /** CSC matrix so CS_RowMult means that the column are
		  divided **/
	      CS_RowMult(D+kk, LL->ra[jj]);
	    }



	/** Send the diagonal block contribution to the leader  **/
	if(L->clead[LL->cia[k]] != L->proc_id)
	  {
	    send_matrix(csL, L->cind[ LL->cia[k]], CTRB_TAG, &FC);
	    if(info != NULL)
	      PrecInfo_SubNNZ(info, CSnnz(csL));
	    reinitCS(csL);
	   
	    /** Poste the receive for the factorized matrix that
		the leader processor will send **/
	    poste_block_receive(L->cind[LL->cia[k]], LEAD_TAG,  &FC);
	  }

	/************************************************/
	/** L(i,k) = L(i,k) - L(i, 0:k-1).L(k, 0:k-1)t **/
	/************************************************/
	/** This time, we need to put in jak all the non null blocks
	    in row k (local and non local) **/
	      
	nk = 0;
	for(jj=LL->ria[k];jj<LL->ria[k+1]-1;jj++) 
	  if(LL->ra[jj]->nnzr > 0)
	    {
	      jak[nk] = LL->rja[jj];
	      rak[nk] = LL->ra[jj];
	      rindk[nk] = L->rind[jj];
	      nk++;
	    }
	if(nk>0)
	  {
	    CellCSDistr_ListUpdate(firstcol, k+1,  nk,  jak);
	      
	    /** Search all the "leader" products to do **/
	    CellCSDistr_IntersectList(L->proc_id, firstcol, 
				      nk,  jak, rindk, rak, L->pset_index, L->pset,
				      LL->cia[k+1]-LL->cia[k]-1, LL->cja+LL->cia[k]+1, L->pset_index, L->pset, 
				      listindex, list1, list2);
#ifdef DEBUG_M
	    for(jj=0;jj<(LL->cia[k+1]-LL->cia[k]-1);jj++)
	      toto += listindex[jj];
#endif
	  }
	else
	  {
	    bzero(listindex, sizeof(int)*(LL->cia[k+1]-LL->cia[k]-1));
	  }
	  
	/** Treat the contribution to non local block in priority **/
	for(ii=0;ii<LL->cia[k+1]-LL->cia[k]-1;ii++)
	  {
	    jj = ii+LL->cia[k]+1;
	    if(L->clead[jj] != L->proc_id)
	      {
		i = LL->cja[jj];
		/*** Compute L(i,k) = L(i,k) - L(i, 0:k-1).L(k, 0:k-1)t **/	      
		/*** NO DROPPING HERE ***/
		csrtab1 = list1 + ii*nk;
		csrtab2 = list2 + ii*nk;
		nnb     = listindex[ii];
		  
#ifdef DEBUG_M
		/*toto += nnb;*/
#endif
		if(nnb>0)
		  {
		    if(info != NULL)
		      PrecInfo_SubNNZ(info, CSnnz(LL->ca[jj]));
		      
		    CSCrowMultCSRcol(0.0, NULL, -1.0, nnb, -1.0, csrtab1, csrtab2, 
				     LL->ca[jj], BL->block_index[i+1]-BL->block_index[i], 
				     wki1, wki2, wkd, celltab, cellptrtab); 
		    if(info != NULL)
		      PrecInfo_AddNNZ(info, CSnnz(LL->ca[jj]));
		  }
		/** Send the contrib to the leader processor of this
		    matrix **/
		send_matrix(LL->ca[jj], L->cind[jj], CTRB_TAG, &FC); 
		if(info != NULL)
		  PrecInfo_SubNNZ(info, CSnnz(LL->ca[jj]));
		reinitCS(LL->ca[jj]);

	
		/** Poste the receive for the factorized matrix that
		    the leader processor will send **/
		poste_block_receive(L->cind[jj], LEAD_TAG, &FC);
	      }
	  }
	/** Now deals with the local blocks **/
	for(ii=0;ii<LL->cia[k+1]-LL->cia[k]-1;ii++)
	  {
	    jj = ii+LL->cia[k]+1;
	    if(L->clead[jj] == L->proc_id)
	      {
		i = LL->cja[jj];
		/*** Compute L(i,k) = L(i,k) - L(i, 0:k-1).L(k, 0:k-1)t **/	      
		/*** NO DROPPING HERE ***/
		csrtab1 = list1 + ii*nk;
		csrtab2 = list2 + ii*nk;
		nnb     = listindex[ii];
#ifdef DEBUG_M
		/*toto += nnb;*/
#endif
		if(nnb>0)
		  {
		    if(info != NULL)
		      PrecInfo_SubNNZ(info, CSnnz( LL->ca[jj]));
		    CSCrowMultCSRcol(0.0, NULL, -1.0, nnb, -1.0, csrtab1, csrtab2, 
				     LL->ca[jj], BL->block_index[i+1]-BL->block_index[i], 
				     wki1, wki2, wkd, celltab, cellptrtab);
		    if(info != NULL)
		      PrecInfo_AddNNZ(info, CSnnz( LL->ca[jj])); 
		  }
		  
	      }
	  }
	  
      }

   
      /** Deallocate blocks which column indices are < START **/
      for(ii=LL->ria[k];ii<LL->ria[k+1];ii++)
	{
	  if(LL->rja[ii] >= START)
	    break;
	    
	  if(L->rlead[ii] == DBL->proc_id)
	    { 
#ifndef PIC_INITIAL
	      assert(info != NULL);
	      if(info != NULL)
		PrecInfo_SubNNZ(info, CSnnz(LL->ra[ii]));
#endif
	      reinitCS(LL->ra[ii]);
	     
	    }
	}	
	
	
      /*** Factorize the diagonal block matrix of the column k ***/
      kk = BL->block_index[k]-BL->block_index[LL->tlj];

      if(L->clead[ LL->cia[k]] == L->proc_id)
	{
	  if(info != NULL)
	    PrecInfo_SubNNZ(info, CSnnz(csL));


	  /*** Receive contribution for the diagonal block ***/
	  if(L->cind[LL->cia[k]] >= 0)
	    receive_contrib(SYNCHRONOUS, csL, L->cind[LL->cia[k]], &FC);

	 
	  if(droptab == NULL)
	    CS_ICCT(csL, D+kk, droptol, NULL, fillrat, wki1, wki2, wkd, celltab, cellptrtab, option->shiftdiag);
	  else
	    CS_ICCT(csL, D+kk, droptol, droptab+kk, fillrat, wki1, wki2, wkd, celltab, cellptrtab, option->shiftdiag);

	  if(info != NULL)
	    PrecInfo_AddNNZ(info, CSnnz(csL));

	  /** Send L to the other (non leader) processors **/
	  if(L->cind[LL->cia[k]] >= 0)
	    {
	      /** Send the diagonal D to the other processor **/
#ifdef TRACE_COM
	      fprintfv(5, stderr, "Proc %d send diagonal %d \n", L->proc_id, DBL->loc2glob_blocknum[k]);
#endif
	      send_diagonal(D+kk,  BL->block_index[k+1]-BL->block_index[k], L->cind[LL->cia[k]], &FC);
#ifdef DEBUG_M
	      for(ii = 0 ; ii < BL->block_index[k+1]-BL->block_index[k]; ii++)
		assert(coefabs(D[kk+ii])>0);
#endif

#ifdef TRACE_COM
	      fprintfv(5, stderr, "PROC %d SEND CSL[%d] = %g \n", DBL->proc_id,  DBL->loc2glob_blocknum[k], CSnormFrob(csL));
#endif
	      send_matrix(csL, L->cind[LL->cia[k]], LEAD_TAG, &FC);
	    }
 
	}
      else
	{
	  /** Receive the diagonal **/
#ifdef TRACE_COM
	  fprintfv(5, stderr, "Proc %d receive diag %d \n", L->proc_id, DBL->loc2glob_blocknum[k]);
#endif
	  /*if(MPI_Wait(diag_rqtab+k-LL->tli, FC.status))*/
	  if(MPI_Wait(diag_rqtab+k-START, FC.status))
	    {
	      fprintfv(5, stderr, "Proc %d Error in PhidalDistrMatrix_ICCT for vector %d \n" ,DBL->proc_id, k);
	      MPI_Abort(DBL->mpicom, -1);
	    }

#ifdef DEBUG_M
	  for(ii = 0 ; ii < BL->block_index[k+1]-BL->block_index[k]; ii++)
	    assert(coefabs(D[kk+ii])>0);
#endif
	  /** Receive the factorized diagonal block **/
	  receive_matrix(SYNCHRONOUS, csL, L->cind[LL->cia[k]], &FC); 
	  
#ifdef TRACE_COM
	  fprintfv(5, stderr, "PROC %d receive CSL[%d] = %g \n", DBL->proc_id, DBL->loc2glob_blocknum[k], CSnormFrob(csL));
#endif
	}

      /**  Multiply the droptab by the diagonal elements **/
      ii = BL->block_index[k+1]-BL->block_index[LL->tlj];
      for(i=kk;i<ii;i++)
	droptabtmp[i] /= coefabs(D[i]);


      /*** Divide the column block matrices by the lower triangular
	   block diagonal factor ***/
      /*** Each processor only treat the block where it is the leader **/
      for(ii=LL->cia[k]+1;ii<LL->cia[k+1];ii++)
	if(L->clead[ii] == L->proc_id)
	  {
	    i = LL->cja[ii];

	    if(info != NULL)
	      PrecInfo_SubNNZ(info, CSnnz(LL->ca[ii]));
	    
	    /*** Receive the contributions for this block ****/
	    if(L->cind[ii] >= 0)
	      receive_contrib(SYNCHRONOUS, LL->ca[ii], L->cind[ii], &FC);

	    /*if(DBL->loc2glob_blocknum[i] == 6 && DBL->loc2glob_blocknum[k] == 4) 
	      fprintfv(5, stderr, "INVLT csL = %g M(%d, %d) = %g \n",  CSnormFrob(csL), DBL->loc2glob_blocknum[i], 
	      DBL->loc2glob_blocknum[k], CSnormFrob(LL->ca[ii]));*/
	    CSC_CSR_InvLT(csL, LL->ca[ii],  BL->block_index[i+1]-BL->block_index[i], droptol, droptabtmp+kk, 
			  fillrat, wki1, wki2, wkd, celltab, cellptrtab);
	    /*if(DBL->loc2glob_blocknum[i] == 6 && DBL->loc2glob_blocknum[k] == 4) 
	      fprintfv(5, stderr, "AFTER INVLT csL = %g M(%d, %d) = %g \n",  CSnormFrob(csL), DBL->loc2glob_blocknum[i], 
	      DBL->loc2glob_blocknum[k], CSnormFrob(LL->ca[ii]));*/
	    
	    if(info != NULL)
	      PrecInfo_AddNNZ(info, CSnnz(LL->ca[ii]));


	    /** Send the factorized matrix to the other (non leader) processors  **/
	    if(L->cind[ii] >= 0)
	      send_matrix(LL->ca[ii], L->cind[ii], LEAD_TAG, &FC);
	  }
      
      
      /**** Deallocate the non local block in L(k,:) *****/
      if(job == 0)
	for(ii=LL->ria[k];ii<LL->ria[k+1];ii++)
	  if(L->rlead[ii] != DBL->proc_id)
	    {
	      if(info != NULL)
		PrecInfo_SubNNZ(info, CSnnz(LL->ra[ii]));
	      reinitCS(LL->ra[ii]);
	    }
      
   



      /** Receive the non-local block in column L(k+1:n , k) **/
      /** @@ OIMBE on peut faire qqchose de + compliquer pour retarder
	  la reception de ces blocs ***/
      for(ii=LL->cia[k]+1;ii<LL->cia[k+1];ii++)
	if(L->clead[ii] != L->proc_id)
	  {
	    i = LL->cja[ii];
#ifdef DEBUG_M
	    assert(L->cind[ii] >= 0);
#endif

	    if(info != NULL)
	      PrecInfo_SubNNZ(info, CSnnz(LL->ca[ii]));
	    
	    /*** Receive the matrix from the leader processor ****/
	    receive_matrix(SYNCHRONOUS, LL->ca[ii], L->cind[ii], &FC);

	    if(info != NULL)
	      PrecInfo_AddNNZ(info, CSnnz(LL->ca[ii]));
	  }

    }



  /** Deallocate the communicators **/
  PhidalFactComm_Clean(&FC);
  if(diag_rqtab != NULL)
    free(diag_rqtab);

#ifdef DEBUG_M
  /*{
    int global;
    int LUstyle;
    MPI_Barrier(MPI_COMM_WORLD);
    MPI_Allreduce(&titi, &global, 1, MPI_int,MPI_SUM, MPI_COMM_WORLD);
    if(L->proc_id == 0)
    fprintfv(5, stderr, "TOTAL number of ICCprod = %d \n", global);
    LUstyle = global;

    global = 0;
    MPI_Allreduce(&toto, &global, 1, MPI_int,MPI_SUM, MPI_COMM_WORLD);
    if(L->proc_id == 0)
    fprintfv(5, stderr, "TOTAL number of contributions = %d \n", global);
   
    LUstyle = 2*global - LUstyle;
    if(L->proc_id == 0)
    fprintfv(5, stderr, "Number of contribution for LU = %d \n", LUstyle);
    }*/
#endif

  if(celltab != NULL)
    free(celltab);
  if(cellptrtab != NULL)
    free(cellptrtab);


  if(jak != NULL)
    free(jak);
  if(rak != NULL)
    free(rak);
  if(rindk != NULL)
    free(rindk);
  
  free(firstcol);
  free(listindex);
  free(list1);
  free(list2);

  free(D2);
  free(droptabtmp);
  free(wki1);
  free(wki2);
  free(wkd);

  DEBUG_DUMP(0, FIN);

#ifdef DEBUG_M
  PhidalDistrMatrix_Check(L, DBL);
#endif

}


void Poste_DiagRcv(int start, int end, COEF *D, MPI_Request *diag_rqtab,  PhidalDistrMatrix *L, PhidalDistrHID *DBL)
{
  /*****************************************/
  /* Poste Receive Rqtab for diagonal  for */
  /* Connector [start--end]                */
  /*****************************************/

  PhidalMatrix *LL;
  PhidalHID *BL;
  int i, kk;
  BL = &(DBL->LHID);
  LL = &(L->M);
  /*for(i=START;i<= LL->bri;i++)*/
  for(i=start;i<= end;i++)
    if(DBL->row_leader[i] != DBL->proc_id)
      {
#ifdef DEBUG_M
	assert(DBL->row_leader[i] == L->clead[ LL->cia[i] ]);
#endif
	kk = BL->block_index[i] - BL->block_index[LL->tli];
	if(MPI_Irecv( D + kk, CC(BL->block_index[i+1]-BL->block_index[i]), MPI_COEF_TYPE, DBL->row_leader[i], 
		      DIAG_TAG, DBL->mpicom, diag_rqtab + i-start) != MPI_SUCCESS)
	  {
	    fprintfd(stderr, "Proc %d Error in posting receive for diagonal %d \n", DBL->proc_id, i);
	    MPI_Abort(DBL->mpicom, -1);
	  }
      }
}

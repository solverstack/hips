/* @authors P. HENON */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>
#include <mpi.h>

#include "phidal_parallel.h"
#include "scotch_metis_wrapper.h"

int hazardous(int n, int seed)
{
  return seed%n;
}


void PhidalDistrHID_Setup(mpi_t proc_id, mpi_t nproc, mpi_t master, PhidalHID *BL, PhidalDistrHID *DBL, dim_t *iperm, MPI_Comm mpicom)
{
  /****************************************************************************/
  /* This function creates the local Hierarchical Interface Decomposition     */
  /* structure from the global one                                            */
  /****************************************************************************/
  PhidalDistrHID_MapDomains(proc_id, nproc, master, BL, DBL, mpicom); /** init DBL is made inside this function **/
  PhidalDistrHID_GenereLocalHID(proc_id, BL, DBL, iperm);
}

void PhidalDistrHID_MapDomains(mpi_t proc_id, mpi_t nproc, mpi_t master, PhidalHID *BL, PhidalDistrHID *DBL, MPI_Comm mpicom)
{
  mpi_t *dom2proc;

  dom2proc = (mpi_t *)malloc(sizeof(mpi_t)*BL->ndom);

  PhidalDistrHID_Init(DBL);

  DBL->proc_id = proc_id;
  DBL->globn  = BL->n;
  DBL->gnblock = BL->nblock;
  DBL->nproc = nproc;
  DBL->dom2proc = dom2proc;
  DBL->mpicom = mpicom;
  
  if(proc_id == master || master < 0)
    PhidalHID_MapDomains(nproc, BL, dom2proc);

  if(master >= 0 && nproc > 1)
    MPI_Bcast(dom2proc, EE(BL->ndom), COMM_INT, master, mpicom);
  
  DBL->dom2proc = dom2proc;
} 


void PhidalHID_MapDomains(mpi_t nproc, PhidalHID *BL, mpi_t *dom2proc)
{
  /********************************************************************/
  /* Using the HID structure this function mapp the domains on a      */
  /* set of processors. The mapping of the domain is made to          */
  /* equilibrate the number of node on eahc processor and minimize the*/
  /* overlapp between processor                                       */
  /********************************************************************/
  dim_t i, j, k;
  int eind;
  int b, ind, dom, nkey;
  REAL bsize;
  INTL *vwgt;
  INTL *ewgt;
  REAL domwgt;

  int numflag ;
  dim_t *tmpj;
  REAL *tmpa;
  dim_t *jrev;
  int **blist; 
  int *nblist;
  dim_t n, *ja;
  INTL *ia;
  int option[8];

  INTL *wgttmp;

  if(nproc == BL->ndom)
    {
      /** Exactly one subdomain per processor : nothing to do ***/
      for(i=0;i<BL->ndom;i++)
	dom2proc[i] = i;
      return;
    }

  if(nproc == 1)
    {
      /** Nothing to do **/
      for(i=0;i<BL->ndom;i++)
	dom2proc[i] = 0;
      return;
    }

  if(nproc > BL->ndom)
    {
      fprintferr(stderr, "Not enough subdomain (%d) to parallelize on %d processors \n", BL->ndom, nproc);
      fprintferr(stderr, "Try a smaller number of processors or set a smaller domain size parameter \n");
      MPI_Abort(MPI_COMM_WORLD, -1); 
      /*MPI_Finalize();*/
      exit(-1);
    }


  /***** First we construct the valued adjacency graph given by the domain partition      ****/
  /***** A vertex represents a domain and its weight is the number of node of this domain  ****/
  /***** A edge represents the presence of an overlapped between two subdomain: its weight ****/
  /****     is equal to the number of nodes in this overlap                                ****/
  

  /** For each domain we construct the list of adjacent connectors **/
  nblist = (int *)malloc(sizeof(int)*BL->ndom);
  bzero(nblist, sizeof(int)*BL->ndom);
  for(k=0;k<BL->nblock;k++)
    for(i=BL->block_keyindex[k];i<BL->block_keyindex[k+1];i++)
      nblist[BL->block_key[i]]++;
  
  blist = (int **)malloc(sizeof(int *)*BL->ndom);
  for(i=0;i<BL->ndom;i++)
    {
      if(nblist[i] > 0)
	{
	  blist[i] = (int *)malloc(sizeof(int)*nblist[i]);
	  bzero(blist[i], sizeof(int)*nblist[i]);
	}
      else
	blist[i] = NULL;
    }

  bzero(nblist, sizeof(int)*BL->ndom);


  for(k=0;k<BL->nblock;k++)
    for(i=BL->block_keyindex[k];i<BL->block_keyindex[k+1];i++)
      {
	j = BL->block_key[i];
	blist[j][nblist[j]++] =  k;
      }  

 

  /*** We construct the quotient graph of the domain ****/
  n = BL->ndom;
  tmpj = (dim_t *)malloc(sizeof(dim_t)*n);
  jrev = (dim_t *)malloc(sizeof(dim_t)*n);
  for(i=0;i<n;i++)
    jrev[i] = -1;

  /*** Compute the number of edge ***/
  eind = 0;
  for(k=0;k<n;k++)
    {
      ind = 0;
      for(i=0;i<nblist[k];i++)
	{
	  b = blist[k][i];
	  nkey = BL->block_keyindex[b+1]- BL->block_keyindex[b];
	  for(j=BL->block_keyindex[b];j<BL->block_keyindex[b+1];j++)
	    {
	      dom = BL->block_key[j];
	      if(dom != k) /** We do not store the diagonal **/
		if(jrev[dom] == -1)
		  {
		    jrev[dom] = ind;
		    tmpj[ind] = dom;
		    eind++;
		    ind++;
		  }
	    }
	}
      /** Reinit jrev **/
      for(i=0;i<ind;i++)
	jrev[tmpj[i]] = -1;
    }

  
  ia = (INTL *)malloc(sizeof(INTL)*(n+1));
  ja = (dim_t *)malloc(sizeof(dim_t)*eind); /** This allocation is an upper
						bound **/
  tmpa = (REAL *)malloc(sizeof(REAL)*n);
  bzero(tmpj, sizeof(dim_t)*n);
  bzero(tmpa, sizeof(REAL)*n);
  
  ewgt = (INTL *)malloc(sizeof(INTL)*eind);
  vwgt = (INTL *)malloc(sizeof(INTL)*n);
  bzero(vwgt, sizeof(INTL)*n);
 
  eind = 0;
  for(k=0;k<n;k++)
    {
      ind = 0;
      domwgt = 0.0;
#ifdef DEBUG_M
      for(i=0;i<n;i++)
	assert(jrev[i] == -1);
#endif
      for(i=0;i<nblist[k];i++)
	{
	  b = blist[k][i];
	  nkey = BL->block_keyindex[b+1]- BL->block_keyindex[b];
	  bsize = (BL->block_index[b+1]-BL->block_index[b])/(REAL)nkey;
	  for(j=BL->block_keyindex[b];j<BL->block_keyindex[b+1];j++)
	    {
	      dom = BL->block_key[j];
	      if(dom != k) /** We do not store the diagonal **/
		{
		  if(jrev[dom] == -1)
		    {
		      jrev[dom] = ind;
		      tmpj[ind] = dom;
		      ind++;
		    }
		  tmpa[jrev[dom]] += bsize;
		}
	      else
		domwgt += bsize;
	    }
	}
#ifdef DEBUG_M
      assert(ind <= n);
#endif

      vwgt[k] = (INTL)domwgt;
      ia[k] = eind;
      for(i=0;i<ind;i++)
	{
	  ja[eind + i] = tmpj[i];
#ifdef DEBUG_M
	  assert(tmpa[i] > 0);
#endif
	  ewgt[eind + i] = (INTL)ceil(tmpa[i]);
	}
      eind += ind;
      
      /** Reinit jrev **/
      for(i=0;i<ind;i++)
	jrev[tmpj[i]] = -1;

      bzero(tmpa, sizeof(REAL)*ind);
    }
  ia[k] = eind;
  
  free(jrev);
  free(tmpa);
  free(tmpj);

  for(i=0;i<n;i++)
    {
      if(blist[i] != NULL)
	free(blist[i]);
    }
  free(nblist);
  free(blist);

  numflag = 0;

  option[0] = 0;

  wgttmp = NULL;

#ifdef BALANCE_NNZ
  /*** The weight of the domain corresponds to the number of nonzero in it ***/
  if(BL->block_domwgt != NULL)
    {
      wgttmp = vwgt;
      vwgt = BL->block_domwgt;
    }
  
#endif
  
#ifdef DEBUG_M
  /** Check the graph **/
  checkGraph(n, ia, ja, vwgt, ewgt, 0);
  
  /*fprintfd(stderr,"n = %ld \n", (long)n);
    for(i=0;i<n;i++)
    {
    fprintfd(stderr, "Sommet %ld Load = %ld \n", (long)i, (long)vwgt[i]);
    for(j=ia[i];j<ia[i+1];j++)
    {
    fprintfd(stderr, "(edge %ld load %ld) ", (long)ja[j], (long)ewgt[j]);
    }
    fprintfd(stderr, "\n");
  }
  */

#endif
  
#ifdef SCOTCH_PART
  {
    char STRAT[1000];
    SCOTCH_Graph        grafdat;
    SCOTCH_Strat        grafstrat;
    INTL *procdom = (INTL *)malloc(sizeof(INTL)*nproc);


    SCOTCH_graphInit  (&grafdat);

    /*SCOTCH_graphBuild (&grafdat, 0, n, ia, NULL, vwgt, NULL, ia[n], ja, ewgt);*/
    SCOTCH_graphBuild_WRAPPER (&grafdat, 0, n, ia, vwgt, ja, ewgt);

    
    if(SCOTCH_graphCheck(&grafdat) != 0)
      {
	fprintfd(stderr," Error in scotch graph check \n");
	exit(-1);
      }
    /*SCOTCH_graphSave (&grafdat, stdout);*/
    /*sprintf(STRAT,"b{job=t,map=t,poli=S,sep=m{type=h,vert=80,low=h{pass=10}f{bal=0.0005,move=80}x,asc=b{bnd=d{dif=1,rem=1,pass=40}f{bal=0.005,move=80}x,org=f{bal=0.005,move=80}x}}|m{type=h,vert=80,low=h{pass=10}f{bal=0.0005,move=80}x,asc=b{bnd=d{dif=1,rem=1,pass=40}f{bal=0.005,move=80}x,org=f{bal=0.005,move=80}x}}}");*/
      SCOTCH_stratInit (&grafstrat);
      /*SCOTCH_stratGraphMap (&grafstrat, STRAT);*/
    
    SCOTCH_graphPart_WRAPPER(n, &grafdat, nproc, &grafstrat, dom2proc);
    SCOTCH_graphExit (&grafdat);
    SCOTCH_stratExit (&grafstrat);

    bzero(procdom, sizeof(INTL)*nproc);
    for(i=0;i<n;i++)
      procdom[dom2proc[i]] += vwgt[i];
    
    for(i=0;i<nproc;i++)
      if(procdom[i] == 0)
	{
	  fprintferr(stderr, "Warning in PHIDAL_SetupLocalData : SCOTCH compute a wrong partition of the domain quotient graph \n");
	  SCOTCH_graphInit  (&grafdat);
	  SCOTCH_graphBuild_WRAPPER (&grafdat, 0, n, ia, vwgt, ja, NULL);
	  if(SCOTCH_graphCheck(&grafdat) != 0)
	    {
	      fprintfd(stderr," Error in scotch graph check \n");
	      exit(-1);
	    }
	  sprintf(STRAT,"b{job=t,map=t,poli=S,sep=m{type=h,vert=80,low=h{pass=10}f{bal=0.0005,move=80}x,asc=b{bnd=d{dif=1,rem=1,pass=40}f{bal=0.005,move=80}x,org=f{bal=0.005,move=80}x}}|m{type=h,vert=80,low=h{pass=10}f{bal=0.0005,move=80}x,asc=b{bnd=d{dif=1,rem=1,pass=40}f{bal=0.005,move=80}x,org=f{bal=0.005,move=80}x}}}");
	  SCOTCH_stratInit (&grafstrat);
	  SCOTCH_stratGraphMap (&grafstrat, STRAT);
	  SCOTCH_graphPart_WRAPPER(n, &grafdat, nproc, &grafstrat, dom2proc);
	  SCOTCH_graphExit (&grafdat);
	  SCOTCH_stratExit (&grafstrat);
	  
	  bzero(procdom, sizeof(INTL)*nproc);
	  for(i=0;i<n;i++)
	    procdom[dom2proc[i]] += vwgt[i];
	  for(i=0;i<nproc;i++)
	    if(procdom[i] == 0)
	      {
		fprintferr(stderr, "Warning in PHIDAL_SetupLocalData : SCOTCH compute a wrong partition of the domain quotient graph (no edge weights) \n");
		
		SCOTCH_graphInit  (&grafdat);
		SCOTCH_graphBuild_WRAPPER (&grafdat, 0, n, ia, NULL, ja, NULL);
		if(SCOTCH_graphCheck(&grafdat) != 0)
		  {
		    fprintfd(stderr," Error in scotch graph check \n");
		    exit(-1);
		  }
		SCOTCH_stratInit (&grafstrat);
		SCOTCH_stratGraphMap (&grafstrat, STRAT);
		SCOTCH_graphPart_WRAPPER(n, &grafdat, nproc, &grafstrat, dom2proc);
		SCOTCH_graphExit (&grafdat);
		SCOTCH_stratExit (&grafstrat);
		
		bzero(procdom, sizeof(INTL)*nproc);
		for(i=0;i<n;i++)
		  procdom[dom2proc[i]] += vwgt[i];
		for(i=0;i<nproc;i++)
		  if(procdom[i] == 0)
		    {
		      fprintferr(stderr, "Warning in PHIDAL_SetupLocalData : SCOTCH compute a wrong partition of the domain quotient graph (no vertex and edge weights) \n");
		      exit(-1);
		    }
		break;
	      }
	}
  
  
    free(procdom);
    
  }
#else
  {
    int wgtflag ;
    int edgecut;
    INTL *procdom = (INTL *)malloc(sizeof(INTL)*nproc);
    wgtflag = 3; /** Both vertex and edge are weighted **/


    if(nproc <= 8 && nproc%2 == 0)
      METIS_PartGraphRecursive_WRAPPER(n, ia, ja, vwgt, ewgt, wgtflag, numflag, nproc, option, &edgecut, dom2proc);
    else
      METIS_PartGraphKway_WRAPPER     (n, ia, ja, vwgt, ewgt, wgtflag, numflag, nproc, option, &edgecut, dom2proc);
    
    bzero(procdom, sizeof(INTL)*nproc);
    for(i=0;i<n;i++)
      procdom[dom2proc[i]] += vwgt[i];

    for(i=0;i<nproc;i++)
      if(procdom[i] == 0)
	{
	  fprintferr(stderr, "Warning in PHIDAL_SetupLocalData : Metis compute a wrong partition of the domain quotient graph, domain  \n");
	  wgtflag = 2;
	  if(nproc <= 8 && nproc%2 == 0)
	    METIS_PartGraphRecursive_WRAPPER(n, ia, ja, vwgt, ewgt, wgtflag, numflag, nproc, option, &edgecut, dom2proc);
	  else
	    METIS_PartGraphKway_WRAPPER(n, ia, ja, vwgt, ewgt, wgtflag, numflag, nproc, option, &edgecut, dom2proc);

	  bzero(procdom, sizeof(INTL)*nproc);
	  for(i=0;i<n;i++)
	    procdom[dom2proc[i]] += vwgt[i];
	  
	  for(i=0;i<nproc;i++)
	    if(procdom[i] == 0)
	      {
		fprintferr(stderr, "Warning in PHIDAL_SetupLocalData : Metis compute a wrong partition of the domain quotient graph without edge weights \n");
		wgtflag = 0;
		if(nproc <= 8 && nproc%2 == 0)
		  METIS_PartGraphRecursive_WRAPPER(n, ia, ja, vwgt, ewgt, wgtflag, numflag, nproc, option, &edgecut, dom2proc);
		else
		  METIS_PartGraphKway_WRAPPER(n, ia, ja, vwgt, ewgt, wgtflag, numflag, nproc, option, &edgecut, dom2proc);
		
		bzero(procdom, sizeof(INTL)*nproc);
		for(i=0;i<n;i++)
		  procdom[dom2proc[i]] += vwgt[i];

		for(i=0;i<nproc;i++)
		  if(procdom[i] == 0)
		    {
		      fprintferr(stderr, "ERROR in PHIDAL_SetupLocalData : Metis compute a wrong partition of the domain quotient graph without weights \n");
		      exit(-1);
		    }
		      break;
	      }


	  break;
	}
		 

    free(procdom);
    fprintfd(stderr, "Edgecut  = %ld \n", (long)edgecut);   
  }


#endif

  
#ifdef BALANCE_NNZ
  /**** Print some info on the load balance *****/
  fprintfd(stdout, "LOAD BALANCE ON NNZ \n");
  bzero(ia, sizeof(INTL)*nproc);
  for(i=0;i<n;i++)
    ia[dom2proc[i]]+=vwgt[i];
  
  /*for(i=0;i<nproc;i++)
    fprintfd(stdout, "Proc %d weight(nnz) = %d \n", i, ia[i]);*/
  
  if(wgttmp != NULL)
    {
      bzero(ia, sizeof(INTL)*nproc);
      for(i=0;i<n;i++)
	ia[dom2proc[i]]+=wgttmp[i];
      /*for(i=0;i<nproc;i++)
	fprintfd(stdout, "NODE Proc %d = %d \n", i, ia[i]);*/
    }
  
  bzero(ia, sizeof(INTL)*nproc);
  for(i=0;i<n;i++)
    ia[dom2proc[i]]+=BL->block_index[i+1]-BL->block_index[i];
  /*for(i=0;i<nproc;i++)
    fprintfd(stdout, "Proc %d: interior nodes = %d \n", i, ia[i]);*/
  
#else
      fprintfd(stdout, "LOAD BALANCE ON NODE \n");
      bzero(ia, sizeof(INTL)*nproc);
      for(i=0;i<n;i++)
	ia[dom2proc[i]]+=vwgt[i];
      /*for(i=0;i<nproc;i++)
	fprintfd(stdout, "Proc %d weight(node) = %d \n", i, ia[i]);*/
      
      bzero(ia, sizeof(INTL)*nproc);
      for(i=0;i<n;i++)
	ia[dom2proc[i]]+=BL->block_index[i+1]-BL->block_index[i];
      
      /*for(i=0;i<nproc;i++)
	fprintfd(stdout, "Proc %d interior node = %d \n", i, ia[i]);*/
#endif

      
  if(wgttmp == NULL)
    {
      free(vwgt);
    }
  else
    free(wgttmp);
  
  free(ia);
  free(ja);
  
  free(ewgt);
} 


void GetProcUnknowns(mpi_t procnum, mpi_t *dom2proc, PhidalHID *BL, dim_t *block_psetindex, mpi_t *block_pset, dim_t *iperm, dim_t *unknownnbr,  dim_t **unknownlist)
{
 
  /*--------------------------------------------*/
  /*  Compute the sizes of local unknown list   */
  /*--------------------------------------------*/
  dim_t ln, i, j;
  dim_t *loc2orig;
  ln       = 0;

  /*** Count the local unknowns ****/
  for(i=0;i<BL->nlevel;i++)
    for(j = BL->block_levelindex[i];j<BL->block_levelindex[i+1];j++)
      if( is_in_key(procnum, j, block_psetindex, block_pset) )
	ln +=  BL->block_index[j+1] - BL->block_index[j];
  
  *unknownnbr = ln;
  *unknownlist = (dim_t *)malloc(sizeof(dim_t)*ln);
  loc2orig = *unknownlist;

  ln = 0;
  for(i=0;i<BL->nlevel;i++)
    {
      for(j = BL->block_levelindex[i];j<BL->block_levelindex[i+1];j++)
	if( is_in_key(procnum, j, block_psetindex, block_pset) )
	  {
	    memcpy(loc2orig+ln, iperm+BL->block_index[j], sizeof(dim_t)*(BL->block_index[j+1]-BL->block_index[j]));
	    ln +=  BL->block_index[j+1] - BL->block_index[j];
	  }
    }

}

void GetGlobalConnectorMapping(dim_t nproc, mpi_t *dom2proc, PhidalHID *BL, dim_t *block_psetindex, mpi_t *block_pset)
{
  dim_t i, j, k;
  dim_t *mask;
  /***** Compute the processor set of each connector ******/
  mask = (flag_t *)malloc(sizeof(flag_t)*nproc);
  j = 0;
  for(k=0;k<BL->nblock;k++)
    {
      block_psetindex[k] = j;
      bzero(mask, sizeof(flag_t)*nproc);
      for(i=BL->block_keyindex[k];i<BL->block_keyindex[k+1];i++)
	mask[dom2proc[BL->block_key[i]]] = 1;
      for(i=0;i<nproc;i++)
	if(mask[i] == 1)
	  block_pset[j++] = i;
    }
  block_psetindex[BL->nblock] = j;
  
  free(mask);
}


void PhidalDistrHID_GenereLocalHID(mpi_t procnum, PhidalHID *BL, PhidalDistrHID *DBL, dim_t *iperm)
{
  /****************************************************************************/
  /* This function creates the local Hierarchical Interface Decomposition     */
  /* for the processor "procnum"                                              */
  /* structure from the global one                                            */
  /****************************************************************************/
  dim_t i, j, k;
  dim_t *block_psetindex;
  mpi_t *block_pset;
  dim_t *mask;
  dim_t ln, lnblock, lnkey, lnpset; /** for local counting**/  
  dim_t nproc;
  PhidalHID *LHID;
  /*dim_t *iperm;*/
  dim_t *loc2orig, *orig2loc;
  mpi_t *dom2proc;
  /* use this to compute loc2orig and orig2loc vectors */
  /*iperm = BL->iperm;*/

  /*for(i=0;i<BL->ndom;i++)
    fprintfv(5, stderr, "DOM %d --> Proc %d \n", i, dom2proc[i]);*/


  nproc = DBL->nproc;
  dom2proc = DBL->dom2proc;

  LHID = &(DBL->LHID);


  /***** Compute the processor set of each connector ******/
  block_psetindex = (dim_t *)malloc(sizeof(dim_t)* (BL->nblock+1));
  block_pset = (mpi_t *)malloc(sizeof(mpi_t)*(BL->block_keyindex[BL->nblock]));
  mask = (flag_t *)malloc(sizeof(flag_t)*nproc);

  /*j = 0;
  for(k=0;k<BL->nblock;k++)
    {
      block_psetindex[k] = j;
      bzero(mask, sizeof(flag_t)*nproc);
      for(i=BL->block_keyindex[k];i<BL->block_keyindex[k+1];i++)
	mask[dom2proc[BL->block_key[i]]] = 1;
      for(i=0;i<nproc;i++)
	if(mask[i] == 1)
	  block_pset[j++] = i;
    }
    block_psetindex[BL->nblock] = j;*/
  GetGlobalConnectorMapping(nproc, dom2proc, BL, block_psetindex, block_pset);

  
  /*--------------------------------------*/
  /*  Compute sizes of local data         */
  /*--------------------------------------*/
  ln       = 0;
  lnblock  = 0;
  lnkey    = 0;
  lnpset   = 0;


  DBL->glob2loc_blocknum = (int_t *)malloc(sizeof(int_t)*BL->nblock);
  for(i=0;i<BL->nlevel;i++)
    {
      for(j = BL->block_levelindex[i];j<BL->block_levelindex[i+1];j++)
	if( is_in_key(procnum, j, block_psetindex, block_pset) )
	  {
	    DBL->glob2loc_blocknum[j] = lnblock;
	    lnblock++;
	    ln +=  BL->block_index[j+1] - BL->block_index[j];
	    lnkey += BL->block_keyindex[j+1] - BL->block_keyindex[j];
	    lnpset += block_psetindex[j+1] - block_psetindex[j]; 
	  }
	else
	  DBL->glob2loc_blocknum[j] = -1;
    }
  
  DBL->loc2glob_blocknum = (int_t *)malloc(sizeof(int_t)*lnblock);
  for(i=0;i<BL->nblock;i++)
    if(DBL->glob2loc_blocknum[i] >= 0)
      DBL->loc2glob_blocknum[DBL->glob2loc_blocknum[i]] = i;

  
 /*--------------------------------------/
 /  Alloc memory for local data          /
 /---------------------------------------*/
  DBL->block_psetindex = (dim_t *)malloc(sizeof(dim_t)*(lnblock+1));
  DBL->block_pset = (mpi_t *)malloc(sizeof(mpi_t)*lnpset);
  
  LHID->dof    = BL->dof;
  LHID->n      = ln;
  LHID->nlevel = BL->nlevel; /** I prefer to set the number level
				 at the global nlevel even if there is 
				 some empty local level **/
  LHID->nblock = lnblock;
  LHID->ndom = BL->ndom;
  LHID->locndom = 0; /** local number of domains **/
  for(i=0;i<BL->ndom;i++)
    if(dom2proc[i] == procnum)
      LHID->locndom++;
  

  LHID->block_index = (dim_t *)malloc(sizeof(dim_t)* (lnblock+1));
  LHID->block_levelindex = (dim_t *)malloc(sizeof(dim_t)* (BL->nlevel+1));
  LHID->block_keyindex = (dim_t *)malloc(sizeof(dim_t)* (lnblock+1));
  LHID->block_key      = (dim_t *)malloc(sizeof(dim_t)*lnkey);


  /*--------------------------------------/
  /  Fill the local data vectors          /
  /---------------------------------------*/
  ln       = 0;
  lnblock  = 0;
  lnkey    = 0;
  lnpset   = 0;


  DBL->loc2orig = (dim_t *)malloc(sizeof(dim_t)*LHID->n);
  loc2orig = DBL->loc2orig;

  for(i=0;i<BL->nlevel;i++)
    {
      LHID->block_levelindex[i] = lnblock;
      for(j = BL->block_levelindex[i];j<BL->block_levelindex[i+1];j++)
	if( is_in_key(procnum, j, block_psetindex, block_pset) )
	  {
	    memcpy(loc2orig+ln, iperm+BL->block_index[j], sizeof(dim_t)*(BL->block_index[j+1]-BL->block_index[j]));
	    LHID->block_index[lnblock] = ln;
	    LHID->block_keyindex[lnblock] = lnkey;
	    DBL->block_psetindex[lnblock] = lnpset;
	    memcpy( LHID->block_key + lnkey,  BL->block_key + BL->block_keyindex[j], 
		    sizeof(dim_t)*(BL->block_keyindex[j+1]-BL->block_keyindex[j]));
	    memcpy( DBL->block_pset + lnpset,  block_pset + block_psetindex[j], 
		    sizeof(mpi_t)*(block_psetindex[j+1]-block_psetindex[j]));
	    lnblock++;
	    ln +=  BL->block_index[j+1] - BL->block_index[j];
	    lnkey += BL->block_keyindex[j+1] - BL->block_keyindex[j];
	    lnpset += block_psetindex[j+1] - block_psetindex[j]; 
	  }
    }

  LHID->block_index[lnblock] = ln;
  LHID->block_levelindex[BL->nlevel] = lnblock; 
  LHID->block_keyindex[lnblock] = lnkey; 
  /** We do not copy the global perm and iperm vector;
      these are useless in the parallel version **/
  /*LHID->perm = NULL;
    LHID->iperm = NULL;*/

  DBL->block_psetindex[lnblock] = lnpset;

  /*******************************/
  /**  Compute orig2loc vector  **/
  /*******************************/
  DBL->orig2loc = (dim_t *)malloc(sizeof(dim_t)*DBL->globn);
  orig2loc = DBL->orig2loc;
  for(i=0;i<DBL->globn;i++)
    orig2loc[i] = -1;
  for(i=0;i<LHID->n;i++)
    orig2loc[loc2orig[i]] = i;

  DBL->row_leader = (dim_t *)malloc(sizeof(dim_t)*LHID->nblock);
#ifdef HAZARDOUS_BALANCE
  for(i=0;i<LHID->nblock;i++)
    {
      j = hazardous(DBL->block_psetindex[i+1]-DBL->block_psetindex[i], DBL->loc2glob_blocknum[i]); 
      DBL->row_leader[i] = DBL->block_pset[DBL->block_psetindex[i]+j];
      /*DBL->row_leader[i] = DBL->block_pset[DBL->block_psetindex[i]];*/
    }
#else
#define HOPE_BALANCE /** Pas vraiement de difference sur petits exemples **/
#ifdef HOPE_BALANCE
  {
    int *hope;
    flag_t *done;
    int bsize;
    int mind;
    dim_t p;
    done = mask;
    hope = (int *)malloc(sizeof(int)*nproc);
    bzero(hope, sizeof(int)*nproc);
    bzero(done, sizeof(flag_t)*nproc);

    for(i=0;i<BL->nblock;i++)
      {
	bsize = BL->block_index[i+1]-BL->block_index[i];
	for(j=block_psetindex[i];j<block_psetindex[i+1];j++)
	  hope[block_pset[j]] += bsize;
      }
    
    for(i=0;i<BL->nblock;i++)
      {

	bsize = BL->block_index[i+1]-BL->block_index[i];
	k = block_pset[block_psetindex[i]];
	mind = done[k];
	for(j=block_psetindex[i];j<block_psetindex[i+1];j++)
	  {
	    p = block_pset[j];
	    if(done[p] + hope[p] < mind)
	      k = p;
	    hope[p] -= bsize;
	  }
	/*fprintfv(5, stderr, "CONNECTOR %d ---> Proc %d \n", i, k);*/
#ifdef DEBUG_M
	assert(k>=0);
	assert(k<nproc);
#endif
	done[k] +=  bsize;
	
	if(DBL->glob2loc_blocknum[i] >= 0)
	  DBL->row_leader[DBL->glob2loc_blocknum[i]] = k;
	/*DBL->row_leader[i] = DBL->block_pset[DBL->block_psetindex[i]];*/
      }
    
    /*for(i=0;i<nproc;i++)
      fprintfv(5, stderr, "PROC %d TOTAL NODE = %d \n", i, done[i]);*/

    free(hope);
  }

#else /** NO HOPE **/

 {
    
    bzero(mask, sizeof(flag_t)*nproc);
    for(i=0;i<BL->nblock;i++)
      {
	int mind;
	mind = BL->block_index[BL->nblock];
	for(j=block_psetindex[i];j<block_psetindex[i+1];j++)
	  if(mask[block_pset[j]] < mind)
	    k = block_pset[j];
	
	/*fprintfv(5, stderr, "CONNECTOR %d ---> Proc %d \n", i, k);*/
	mask[k] +=  BL->block_index[i+1]-BL->block_index[i];
	if(DBL->glob2loc_blocknum[i] >= 0)
	  DBL->row_leader[DBL->glob2loc_blocknum[i]] = k;
	/*DBL->row_leader[i] = DBL->block_pset[DBL->block_psetindex[i]];*/
      }
    for(i=0;i<nproc;i++)
      fprintfv(5, stderr, "PROC %d UNKNOWN = %d \n", i, mask[i]);
  }
#endif

#endif



  free(block_pset);
  free(block_psetindex);

  /********************************************/
  /** Compute the list of adjacent processor **/
  /********************************************/
  bzero(mask, sizeof(flag_t)*nproc);
  DBL->adjpnbr = 0;
  for(k=0;k<LHID->nblock;k++)
    for(i=DBL->block_psetindex[k];i<DBL->block_psetindex[k+1];i++)
      {
	j = DBL->block_pset[i];
	if(j != procnum && mask[j] == 0)
	  {
	    DBL->adjpnbr++;
	    mask[DBL->block_pset[i]] = 1;
	  }
      }
  if(DBL->adjpnbr > 0)
    DBL->adjptab = (mpi_t *)malloc(sizeof(mpi_t)*DBL->adjpnbr);
  else
    DBL->adjptab = NULL;

  k = 0;
  for(i=0;i<nproc;i++)
    if(mask[i] == 1)
      DBL->adjptab[k++] = i;

  free(mask);

  
 

#ifdef DEBUG_M
  /*PhidalDistrHID_Check(DBL, procnum);*/
#endif
}



void PhidalDistrHID_Expand(PhidalDistrHID *DBL)
{
  dim_t dof;
/*   int *tmp; */
  int i, j, ng, ln;
  dim_t *orig2loc, *loc2orig/* , *node2proc */;
  dof = DBL->LHID.dof;

  ln = DBL->LHID.n;
  ng = DBL->globn;

  assert(dof >= 1);
  
  PhidalHID_Expand(&DBL->LHID);
  
  orig2loc = (dim_t *)malloc(sizeof(dim_t)*ng*dof);
  for(i=0;i<ng;i++)
    for(j=0;j<dof;j++)
      orig2loc[dof*i + j] = DBL->orig2loc[i]*dof+j;
  free(DBL->orig2loc);
  DBL->orig2loc = orig2loc;

  loc2orig = (dim_t *)malloc(sizeof(dim_t)*ln*dof);
  for(i=0;i<ln;i++)
    for(j=0;j<dof;j++)
      loc2orig[dof*i + j] = DBL->loc2orig[i]*dof+j;
  free(DBL->loc2orig);
  DBL->loc2orig = loc2orig;
  DBL->globn *= dof;
      
/*  node2proc = (dim_t *)malloc(sizeof(dim_t)*ln*dof); */
/*   for(i=0;i<ln;i++) */
/*     for(j=0;j<dof;j++) */
/*       node2proc[dof*i + j] = DBL->node2proc[i]*dof+j; */
/*   free(DBL->node2proc); */
/*   DBL->node2proc = node2proc; */
  /**** The structure has been expanded ***/
  
  

}

/* @authors P. HENON */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>
#include "phidal_parallel.h"


void PHIDAL_DistrILUCT(flag_t job, PhidalDistrMatrix *A, PhidalDistrMatrix *L, PhidalDistrMatrix *U, REAL droptol, REAL *droptab, REAL fillrat, PhidalDistrHID *DBL, PhidalOptions *option, PrecInfo *info)
{
  /**********************************************************************/
  /* This function computes an ILUTP factorization of the matrix A.     */
  /* Fill-in in the factor is only permit in the phidal pattern         */
  /* If job == 0 the matrix A is left unchanged                         */
  /* If job == 1 the factorization is done in place (A is a void matrix */
  /* when returned                                                      */
  /**********************************************************************/
  dim_t i;


  if(A->M.tli != A->M.tlj || A->M.bri != A->M.brj)
    {
      fprintfv(5, stderr, "Error in PHIDAL_DistrICCT : Matrix A is not a square matrix \n");
      exit(-1);
    }

  /** 1- Copy the upper triangular part of A into U (**/
  PhidalDistrMatrix_Setup(A->M.tli, A->M.tlj, A->M.bri, A->M.brj, "U", "N", option->locally_nbr, U, DBL);

  if(job == 0)
    {
      PhidalDistrMatrix_Copy(A, U, DBL);
      if(info != NULL)
	PrecInfo_AddNNZ(info, PhidalDistrMatrix_NNZ(U));
    }
  else
    PhidalDistrMatrix_Cut(A, U, DBL);


  /** 2- Copy the strictly lower triangular part of A into L **/
  PhidalDistrMatrix_Setup(A->M.tli, A->M.tlj, A->M.bri, A->M.brj, "L", "N", option->locally_nbr, L, DBL);
  if(job == 0)
    {
      PhidalDistrMatrix_Copy(A, L, DBL);
      if(info != NULL)
        PrecInfo_AddNNZ(info, PhidalDistrMatrix_NNZ(L));
    }
  else
    PhidalDistrMatrix_Cut(A, L, DBL);

  /*** Reset the block diagonal to null matrices **/
  if(info != NULL)
    for(i=L->M.tli;i<=L->M.brj;i++)
      PrecInfo_SubNNZ(info, CSnnz(L->M.ca[ L->M.cia[i]]));
  
  for(i=L->M.tli;i<=L->M.brj;i++)
    reinitCS(L->M.ca[ L->M.cia[i]]);
  
#ifdef DEBUG_M
  {
    /*int nb;*/
    /** Count the total number of blocs **/
    /*nb = PhidalDistrMatrix_NB(L);
    if(L->proc_id == 0)
    fprintfv(5, stderr, "TOTAL number of blocks in L is = %d \n", nb);*/
    
    /** Count number of theorical contribution **/
    /*nb = PhidalDistrMatrix_NumberContrib(L, DBL);
    if(L->proc_id == 0)
    fprintfv(5, stderr, "TOTAL number of contribution to factorize L is = %d \n", nb);*/
  }
#endif
  /*** @@@ OIMBE Attention si on utilise le multiniveaux il faudra passer le
  parametre job dans l'interface de PHIDAL_DistrICCT pour pouvoir
    garder les blocs partages sur tous les processeur (invLt en a
    besoins ***/
  PhidalDistrMatrix_ILUCT(0, L, U, droptol, droptab, fillrat, DBL, option, info);
}


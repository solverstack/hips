/* @authors P. HENON */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>

#define PARALLEL

#include "phidal_parallel.h"


void PhidalDistrMatrix_Transpose(PhidalDistrMatrix *DM)
{
  /********************************************************************/
  /* This function transpose a phidal matrix                          */
  /* exemple: if M is an upper phidal matrix in CSR block matrices    */
  /*  then it becomes a lower phidal matrix in CSC block matrices     */
  /********************************************************************/
  INTL *tmp;
  int *tmp2;
  
  tmp2 = DM->clead;
  DM->clead = DM->rlead;
  DM->rlead = tmp2;

  tmp = DM->cind;
  DM->cind = DM->rind;
  DM->rind = tmp;

  /** The principle is really simple: the block rows of the phidal matrix 
      becomes the column of the transpose phidal matrix **/
  PhidalMatrix_Transpose(&DM->M);
  
}



void PhidalDistrMatrix_CleanNonLocalBLock(PhidalDistrMatrix *DM)
{
  /*************************************************/
  /* This function deallocates all the non local   */
  /* csr submatrices that are not local to the     */
  /* processor owner of the PhidalDistrMatrix      */
  /*************************************************/
  dim_t i, j;
  PhidalMatrix *M;
  M = &(DM->M);

  for(i=M->tli;i<M->bri;i++)
    for(j=M->ria[i];j<M->ria[i+1];j++)
      if(DM->rlead[j] != DM->proc_id)
	reinitCS(M->ra[j]);
}



void PhidalDistrMatrix_Copy(PhidalDistrMatrix *A, PhidalDistrMatrix *B, PhidalDistrHID *DBL)
{
  /** Copy the PhidalMatrix **/
  PhidalMatrix_Copy(&(A->M), &(B->M), &(DBL->LHID));

}

void PhidalDistrMatrix_Cut(PhidalDistrMatrix *A, PhidalDistrMatrix *B, PhidalDistrHID *DBL)
{
  /** Copy the PhidalMatrix **/
  PhidalMatrix_Cut(&(A->M), &(B->M), &(DBL->LHID));
}


int PhidalDistrMatrix_NB(PhidalDistrMatrix *A, MPI_Comm mpicom)
{
  /** This function count the number of block of a distributed phidal
      matrix **/
  /** A block is count once even if it is shared by several processors
   **/
  dim_t i, j;
  PhidalMatrix *AA;
  int nb, gnb;

  AA = &(A->M);
  nb = 0;
  for(i=AA->tli;i<=AA->bri;i++)
    for(j=AA->ria[i];j<AA->ria[i+1];j++)
      if(A->rlead[j] == A->proc_id)
	nb++;

  MPI_Allreduce(&nb, &gnb, EE(1), COMM_INT, MPI_SUM, mpicom);
  return gnb;

}


int PhidalDistrMatrix_NumberContrib(PhidalDistrMatrix *L, PhidalDistrHID *DBL)
{

  /** This function count the number of contribution (matrix products)
      that will be necessary to compute the factorization of L in ALL
      LOCALLY consistent mode **/

  dim_t i, j, k;
  int jj;
  int kl1, kl2;
  int *key1, *key2;
  int k1, k2, ind;
  int *pset1=NULL, *pset2=NULL;
  PhidalMatrix *LL;
  PhidalHID *BL;
  int nb, gnb;

  BL = &DBL->LHID;
  LL = &(L->M);
  nb = 0;

  for(i=LL->tlj;i<LL->brj;i++)
    {
      for(j=LL->cia[i]+1;j<LL->cia[i+1];j++)
	{
	  
	  jj = LL->cja[j];
	  kl1 = BL->block_keyindex[jj+1] - BL->block_keyindex[jj];
	  key1 = BL->block_key + BL->block_keyindex[jj];
	  
	  ind = L->cind[j];
	  if(ind >= 0)
	    {
	      k1 = L->pset_index[ind+1]-L->pset_index[ind];
	      pset1 = L->pset + L->pset_index[ind];
	    }
	  else
	    k1 = -1;
	  for(k=j;k<LL->cia[i+1];k++)
	    {
	      jj = LL->cja[k];
	      kl2 = BL->block_keyindex[jj+1] - BL->block_keyindex[jj];
	      key2 = BL->block_key + BL->block_keyindex[jj];
	      if(is_intersect_key(kl1, key1, kl2, key2) != 1)
		continue;

	      ind = L->cind[k];
	      if(ind >= 0)
		{
		  k2 = L->pset_index[ind+1] - L->pset_index[ind];
		  pset2 = L->pset + L->pset_index[ind];
		}
	      else
		k2 = -1;
	      
	      if(k1 == -1 || k2 == -1)
		nb++;
	      else
		if(choose_leader(k1, pset1, k2, pset2) == L->proc_id)
		  nb++;

	    }

	}
    }


  MPI_Allreduce(&nb, &gnb, EE(1), COMM_INT,MPI_SUM, DBL->mpicom);
  return gnb;

}


void PhidalDistrMatrix_BuildVirtualMatrix(dim_t tli, dim_t tlj, dim_t bri, dim_t brj, PhidalDistrMatrix *DA, 
					  PhidalDistrMatrix *DV, PhidalDistrHID *DBL)
{
  /********************************************************/
  /* This function copy a phidal matrix a in a virtual    */
  /* PhidalMatrix which all cs block are pointer to those */
  /* in the original matrix                               */
  /* NB: the scaling is not copied                        */
  /********************************************************/
  PhidalMatrix *A, *V;
  PhidalHID *BL;
  int *tmp;
  int i, j, k, nnp, jcol, ind;

  PhidalDistrMatrix_Init(DV);
  DV->proc_id = DA->proc_id;
  DV->nproc = DA->nproc;
  

  BL = &DBL->LHID;
  A = &DA->M;
  V = &DV->M;

  PhidalMatrix_BuildVirtualMatrix(tli, tlj, bri, brj, A, V, BL);

  DV->rind = (INTL *)malloc(sizeof(INTL)*V->bloknbr);
  DV->rlead = (int *)malloc(sizeof(int)*V->bloknbr);
  DV->cind = (INTL *)malloc(sizeof(INTL)*V->bloknbr);
  DV->clead = (int *)malloc(sizeof(int)*V->bloknbr);


  /** use tmp to convert rind and cind for the matrix V **/
  if(DA->sharenbr > 0)
    tmp = (int *)malloc(sizeof(int)*DA->sharenbr);
  else
    tmp = NULL;
  
  /** The pset_index in V might be shorter than DA->sharenbr **/
  /** we do that to spare a traversal of the matrix block **/
  DV->pset_index = (INTL *)malloc(sizeof(INTL)*(DA->sharenbr+1));

  k = 0;
  nnp = 0;
  DV->sharenbr = 0;
  /*DV->maxproc = 0;*/
  for(i=tli;i<=bri;i++)
    {
      for(j=A->ria[i];j<A->ria[i+1];j++)
	if(A->rja[j] >= tlj)
	  break;

      for(;j<A->ria[i+1];j++)
	{
	  jcol = A->rja[j];

	  if(jcol > brj)
	    break;
	  ind = DA->rind[j];
#ifdef DEBUG_M
	  assert(ind < DA->sharenbr);
#endif
	  if(ind >= 0)
	    {

	      /*if(DA->pset_index[ind+1]-DA->pset_index[ind] > DV->maxproc)
		DV->maxproc = DA->pset_index[ind+1]-DA->pset_index[ind];*/

	      DV->pset_index[DV->sharenbr] = nnp;

	      nnp += DA->pset_index[ind+1]-DA->pset_index[ind];
	      tmp[ind] = DV->sharenbr;
	      DV->rind[k] = DV->sharenbr;
	      DV->sharenbr++;
	    }
	  else
	    DV->rind[k] = -1;

	  DV->rlead[k] = DA->rlead[j];
	  k++;
	}
    }
  DV->pset_index[DV->sharenbr] = nnp;

  if(nnp > 0)
    DV->pset = (mpi_t *)malloc(sizeof(mpi_t)*nnp);
  
  k = 0;
  for(i=tlj;i<=brj;i++)
    {
      for(j=A->cia[i];j<A->cia[i+1];j++)
	if(A->cja[j] >= tli)
	  break;

      for(;j<A->cia[i+1];j++)
	{
	  jcol = A->cja[j];

	  if(jcol > bri)
	    break;
	  ind = DA->cind[j];

	  if(ind >= 0)
	    {
	      memcpy(DV->pset + DV->pset_index[tmp[ind]], DA->pset + DA->pset_index[ind], 
		     sizeof(mpi_t)*(DA->pset_index[ind+1]-DA->pset_index[ind]));
	      DV->cind[k] = tmp[ind];
	    }
	  else
	    DV->cind[k] = -1;

	  DV->clead[k] = DA->clead[j];
	  k++;
	}
    }
  
  DV->pset_index = realloc(DV->pset_index, sizeof(INTL)*DV->sharenbr);
  if(tmp != NULL)
    free(tmp);
}



void PhidalDistrMatrix_ColMult2(REAL *vec, PhidalDistrMatrix *A, PhidalDistrHID *DBL)
{
  PhidalMatrix_ColMult2(vec, &A->M, &DBL->LHID);
}

void PhidalDistrMatrix_RowMult2(REAL *vec, PhidalDistrMatrix *A, PhidalDistrHID *DBL)
{
  PhidalMatrix_RowMult2(vec, &A->M, &DBL->LHID);
}

long PhidalDistrMatrix_NNZ(PhidalDistrMatrix *A)
{
  /***********************************/
  /* Return the local number of nnz  */
  /***********************************/
  return PhidalMatrix_NNZ(&A->M);
}

long PhidalDistrMatrix_NNZ_All(PhidalDistrMatrix *A, MPI_Comm mpicom)
{
  /***********************************/
  /* Return the global number of nnz  */
  /***********************************/
  long nnz, gnnz;
  nnz = PhidalMatrix_NNZ(&A->M);
  MPI_Allreduce(&nnz, &gnnz, 1, MPI_LONG, MPI_SUM, mpicom);
  return gnnz;
}

long PhidalDistrPrec_NNZ(PhidalDistrPrec *p)
{
  long nnz = 0;
  
#ifdef DEBUG_M
  assert(p != NULL);
#endif

  if(p->forwardlev == 0)
    {
      nnz = PhidalDistrMatrix_NNZ(PREC_L_SCAL(p));

      if(p->symmetric == 0)
	nnz += PhidalDistrMatrix_NNZ(PREC_U_SCAL(p));

      if(p->symmetric == 1)
	nnz += PREC_L_SCAL(p)->M.dim1;
    }

  if(p->forwardlev > 0)
    {
      nnz =  PhidalDistrMatrix_NNZ(PREC_L_SCAL(p));
      if(p->symmetric == 0)
	nnz += PhidalDistrMatrix_NNZ(PREC_U_SCAL(p));

      if(p->symmetric == 1)
	nnz += PREC_L_SCAL(p)->M.dim1;
      if(PREC_E(p)->M.virtual == 0)
	nnz += PhidalDistrMatrix_NNZ(PREC_E(p));
      if(p->symmetric == 0 && PREC_F(p)->M.virtual == 0)
	nnz += PhidalDistrMatrix_NNZ(PREC_E(p));
	

      if(p->schur_method == 1) 
	if(PREC_S_SCAL(p)->M.virtual == 0)
	  nnz += PhidalDistrMatrix_NNZ(PREC_S_SCAL(p));

      if(p->schur_method == 2) 
	if(PREC_B(p)->M.virtual == 0)
	  nnz += PhidalDistrMatrix_NNZ(PREC_B(p));
      
      nnz += PhidalDistrPrec_NNZ(p->nextprec);
    }
  
  return nnz;
}


long PhidalDistrPrec_NNZ_All(PhidalDistrPrec *p, MPI_Comm mpicom)
{
  long nnz = PhidalDistrPrec_NNZ(p);
  long gnnz;
  
  MPI_Allreduce(&nnz, &gnnz, 1, MPI_LONG, MPI_SUM, mpicom);
  return gnnz;
}

void PhidalDistrMatrix_UnsymScale(int nbr, PhidalDistrMatrix *DA, PhidalDistrHID *DBL, REAL *scalerow, REAL *scalecol, REAL *iscalerow, REAL *iscalecol)
{
  dim_t i, k;
  REAL *scalerowtmp, *iscalerowtmp, *scalecoltmp, *iscalecoltmp;

#ifdef DEBUG_M
  assert(DA->M.symmetric == 0);
#endif

  if(nbr > 1)
    { 
      for(i=0;i<DA->M.dim1;i++)
	{
	  iscalecol[i] = 1.0;
	  iscalerow[i] = 1.0;
	}

      iscalecoltmp = (REAL *)malloc(sizeof(REAL) * DA->M.dim2);
      iscalerowtmp = (REAL *)malloc(sizeof(REAL) * DA->M.dim1);
      scalerowtmp = (REAL *)malloc(sizeof(REAL) * DA->M.dim1);
      scalecoltmp = (REAL *)malloc(sizeof(REAL) * DA->M.dim2);
    }
  else
    {
      iscalecoltmp = iscalecol;
      scalecoltmp = scalecol;
      iscalerowtmp = iscalerow;
      scalerowtmp = scalerow;
    }
  
  for(k=0;k<nbr;k++)
    {
      /*** Unsymetric scale ***/
      PhidalDistrMatrix_ColNorm2(DA, DBL, iscalecoltmp);
      for(i=0;i<DA->M.dim1;i++)
	scalecoltmp[i] = 1.0/iscalecoltmp[i];
      
      PhidalDistrMatrix_ColMult2(scalecoltmp, DA, DBL);
      PhidalDistrMatrix_RowNorm2(DA, DBL, iscalerowtmp);
      
      for(i=0;i<DA->M.dim1;i++)
	scalerowtmp[i] = 1.0/iscalerowtmp[i];
      
      PhidalDistrMatrix_RowMult2(scalerowtmp, DA, DBL);
      if(nbr > 1)
	for(i=0;i<DA->M.dim1;i++)
	  {
	    iscalecol[i] *= iscalecoltmp[i];
	    iscalerow[i] *= iscalerowtmp[i];
	  }
    }

  if(nbr > 1)
    {
      for(i=0;i<DA->M.dim1;i++)
	{
	  scalecol[i] = 1.0/iscalecol[i];
	  scalerow[i] = 1.0/iscalerow[i];
	}
      
      free(iscalecoltmp);
      free(scalecoltmp);
      free(iscalerowtmp);
      free(scalerowtmp);
    }
  
}



void PhidalDistrMatrix_GetUdiag(PhidalDistrMatrix *U, COEF *diag)
{
  /****************************************************************************/
  /* This function return the diagonal of a upper triangular phidal matrix    */
  /* ONLY FOR THE LEADER PROCESSOR                                            */
  /****************************************************************************/
  dim_t i, k;
  csptr csU;
  int ind;
  ind = 0;

  
  for(i=U->M.tli;i<=U->M.bri;i++)

      {
	
#ifdef DEBUG_M
	assert(U->M.rja[U->M.ria[i]] == i);
#endif
	csU = U->M.ra[U->M.ria[i]];
	if(U->proc_id == U->rlead[U->M.ria[i]])
	  for(k=0;k<csU->n;k++)
	    {
#ifdef DEBUG_M
	      assert(csU->ja[k][0] == k);
#endif
	      diag[ind++] = csU->ma[k][0];
	    }
	else
	  {
	    bzero(diag+ind, sizeof(COEF)*csU->n);
	    ind += csU->n;
	  }
      }

}

void PhidalDistrMatrix_SetUdiag(PhidalDistrMatrix *U, COEF *diag)
{
  /****************************************************************************/
  /* This function return the diagonal of a upper triangular phidal matrix    */
  /****************************************************************************/
  dim_t i, k;
  csptr csU;
  int ind;
  ind = 0;
  for(i=U->M.tli;i<=U->M.bri;i++)
      {
#ifdef DEBUG_M
	assert(U->M.rja[U->M.ria[i]] == i);
#endif
	csU = U->M.ra[U->M.ria[i]];
	if(U->proc_id == U->rlead[U->M.ria[i]])
	  for(k=0;k<csU->n;k++)
	    {
#ifdef DEBUG_M
	      assert(csU->ja[k][0] == k);
#endif
	      csU->ma[k][0] = diag[ind++];
	    }
	else
	  ind += csU->n;
      }
}





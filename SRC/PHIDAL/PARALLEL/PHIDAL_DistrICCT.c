/* @authors P. HENON */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>
#include "phidal_parallel.h"


void PHIDAL_DistrICCT(flag_t job, PhidalDistrMatrix *A, PhidalDistrMatrix *L, COEF *D, REAL droptol, REAL *droptab, REAL fillrat, PhidalDistrHID *DBL, PhidalOptions *option, PrecInfo *info)
{
  /*********************************************************************************/
  /* This function computes an ICCT factorization of the matrix A in parallel.     */
  /* Fill-in in the factor is only permit in the phidal pattern                    */
  /* If job == 0 the matrix A is left unchanged                                    */
  /* If job == 1 the factorization is done in place (A is a void matrix            */
  /* when returned                                                                 */
  /*********************************************************************************/


  if(A->M.tli != A->M.tlj || A->M.bri != A->M.brj)
    {
      fprintfv(5, stderr, "Error in PHIDAL_DistrICCT : Matrix A is not a square matrix \n");
      exit(-1);
    }


  /** 2- Copy the strictly lower triangular part of A into L **/
  PhidalDistrMatrix_Setup(A->M.tli, A->M.tlj, A->M.bri, A->M.brj, "L", "N", option->locally_nbr, L, DBL);
  if(job == 0)
    {
      PhidalDistrMatrix_Copy(A, L, DBL);
      if(info != NULL)
	PrecInfo_AddNNZ(info, PhidalDistrMatrix_NNZ(L));
    }
  else
    PhidalDistrMatrix_Cut(A, L, DBL);

#ifdef DEBUG_M
  {
    /*int nb;*/
    /** Count the total number of blocs **/
    /*nb = PhidalDistrMatrix_NB(L);
    if(L->proc_id == 0)
    fprintfv(5, stderr, "TOTAL number of blocks in L is = %d \n", nb);*/
    
    /** Count number of theorical contribution **/
    /*nb = PhidalDistrMatrix_NumberContrib(L, DBL);
    if(L->proc_id == 0)
    fprintfv(5, stderr, "TOTAL number of contribution to factorize L is = %d \n", nb);*/
  }
#endif
  /*** @@@ OIMBE Attention si on utilise le multiniveaux il faudra passer le
  parametre job dans l'interface de PHIDAL_DistrICCT pour pouvoir
    garder les blocs partages sur tous les processeur (invLt en a
    besoins) ***/
  /*fprintfd(stderr, "Proc %d : ON RENTRE DANS ICCT \n", DBL->proc_id);
    fprintfd(stderr, " ICCT nnz = %ld \n", (long)info->nnzP);*/
  PhidalDistrMatrix_ICCT(0, L, D, droptol, droptab, fillrat, DBL, option, info);
  /*fprintfd(stderr, " AFTER ICCT nnz = %ld \n", (long)info->nnzP);
    fprintfd(stderr, " AFTER L nnz = %ld \n", (long)PhidalDistrMatrix_NNZ(L));*/
}


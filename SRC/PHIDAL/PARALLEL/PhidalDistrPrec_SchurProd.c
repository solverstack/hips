/* @authors P. HENON */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>

#define PARALLEL

#include "phidal_parallel.h"



void PhidalDistrPrec_SchurProd(PhidalDistrPrec *P, PhidalDistrHID *DBL, COEF *x, COEF *y)
{
  /**************************************************************/
  /* This function compute the implicit Schur Product           */
  /* y = S.x    where S is not stored                           */
  /* eg y = B.x - E.U^-1.L^1.F.x (for unsymmetric)              */
  /**************************************************************/

  /*** @@OIMBE PROBLEM SI PERMUTATION ????? B n'EST PAS PERMUTE ****/

  dim_t i;
  COEF *t;
  
  PhidalDistrMatrix *E, *F, *L, *U, *B;
  COEF *D;

#ifdef DEBUG_M
  assert(P->schur_method == 2);
#endif

  E = PREC_E(P);
  F = PREC_F(P);
  L = PREC_L_SCAL(P);
  U = PREC_U_SCAL(P);
  B = PREC_B(P);
  D = P->D;


  t = (COEF *)malloc(sizeof(COEF)*L->M.dim1);

  PhidalDistrMatrix_MatVec(0, B, DBL, x, y);

  /*  if(P->levelnum > 0)*/
    {
      PhidalDistrMatrix_MatVec(0, F, DBL, x, t);

      PhidalDistrMatrix_Lsolve(1, L, t, t, DBL);
      if(P->symmetric == 1)
	for(i=0;i<PREC_L_SCAL(P)->M.dim1;i++)
	  t[i] *= D[i];
      if(P->symmetric == 1)
	PhidalDistrMatrix_Usolve(1, U, t, t, DBL); /** Unitary diagonale **/
      else
	PhidalDistrMatrix_Usolve(0, U, t, t, DBL);

      PhidalDistrMatrix_MatVecSub(0, E, DBL, t, y);
    }
    /*else
    {
      PHIDAL_MatVec(&F->M, &DBL->LHID, x, t);

      PhidalMatrix_Lsolve(1, &L->M, t, t, &DBL->LHID);
      if(P->symmetric == 1)
	for(i=0;i<P->L->M.dim1;i++)
	  t[i] *= D[i];
      if(P->symmetric == 1)
	PhidalMatrix_Usolve(1, &U->M, t, t, &DBL->LHID); 
      else
	PhidalMatrix_Usolve(0, &U->M, t, t, &DBL->LHID);
      
      PHIDAL_MatVecSub(&E->M, &DBL->LHID, t, y);
      }*/
  
  free(t);
}

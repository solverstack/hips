/* @authors P. HENON */

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <math.h>
#include "phidal_parallel.h"



void PHIDAL_SetMatrixCoef(flag_t op, flag_t over_add, flag_t numflag, dim_t n1, INTL *ia1, dim_t *ja, COEF *ma, dim_t *nodelist1,
			  flag_t symmetric, flag_t rsa,   PhidalDistrMatrix *A, PhidalDistrHID *DBL)
{
  /*****************************************************************/
  /* This function replaces the entry of a phidal matrix by those  */
  /* in a csr matrix.                                              */
  /* nodelist gives the unknown that are in the csr matrix listed  */
  /* with a global index : it is the base uses to spawn the csr    */
  /* matrix                                                        */
  /* op = 0 : matrix is created and fill with the coefficient      */
  /* op = 1 : coefficient are added to the matrix                  */
  /* op = 2 : matrix is nullified and reinitialised with the coeff */
  /* op = 3 : Coeff are replaced by the new ones                   */
  /* symmetric = 1 matrix is symmetric : we will store only the    */
  /* upper triang. CSR part of the matrix                          */              
  /* rsa = 1 : only the upper triangular part is given as input    */
  /*  NB : the rsa function is taken into account even if the      */
  /*  option symmetric == 0 ; i.e a symmetric contribution can be  */
  /*  added in a unsymmetric matrix                                */
  /*                                                               */
  /* over_add = 0 : coefficient are set by the leader              */
  /* over_add = 1 : coefficient are added for shared blocks        */
  /*****************************************************************/
  
  INTL i/* , k */;
  /*   int ii; */
  INTL *ib;
  dim_t *jb;
  COEF *mb;

  PhidalHID *BL;
  struct SparRow mat;
  dim_t *liperm;
  char *UPLO;
  dim_t *orig2loc, *loc2orig;
  dim_t size;
  dim_t *nodelist, *flagtab;
  INTL *ia, n;

  /*****************************/
  /******* Suppress Zeros ******/
  /*****************************/
  CSR_SuppressZeros(numflag, n1, ia1, ja, ma);

  /** Convert the matrix in C numbering (start from 0 instead of 1) **/
  if(numflag == 1)
    CSR_Fnum2Cnum(ja, ia1, n1);


  BL = &DBL->LHID;
  orig2loc = DBL->orig2loc;

  /**** Compute the ia in order to get a local matrix corresponding to loc2orig *****/
  loc2orig = DBL->loc2orig;
  n = DBL->LHID.n;
  flagtab = (dim_t *)malloc(sizeof(dim_t)*n);
  bzero(flagtab, sizeof(dim_t)*n);
  for(i=0;i<n1;i++)
    flagtab[orig2loc[nodelist1[i]]] = 1;
  
  /** Search the local unknown that are not in nodelist1 **/
  /** I store them in flagtab **/
  size = 0;
  for(i=0;i<n;i++)
    if(flagtab[i] == 0)
      flagtab[size++] = loc2orig[i];
#ifdef DEBUG_M
  assert(size+n1 == n);
#endif

  nodelist = (dim_t *)malloc(sizeof(dim_t)*n);
  memcpy(nodelist, nodelist1, sizeof(dim_t)*n1);
  for(i=0;i<size;i++)
    nodelist[n1+i] = flagtab[i];

  ia = (INTL *)malloc(sizeof(INTL)*(n+1));
  memcpy(ia, ia1, sizeof(INTL)*(n1+1));
  for(i=n1+1;i<n+1;i++)
    ia[i] = ia1[n1];
  
#ifdef DEBUG_M
  bzero(flagtab, sizeof(dim_t)*n);
  for(i=0;i<n;i++)
    flagtab[orig2loc[nodelist[i]]]++;

  for(i=0;i<n;i++)
    assert(flagtab[i] == 1);
#endif

  free(flagtab);
  /********/
  if(op == 0)
    {
      PhidalDistrMatrix_Init(A);
      A->proc_id = DBL->proc_id;
    }

  /*fprintfd(stderr, "CALL PHIDAL_SetMatrixCoef with op = %ld sym = %ld rsa = %ld \n", (long)op, (long)symmetric, (long)rsa);*/


#ifdef DEBUG_M
  /** Check that the csr matrix fits in the local phidal matrix **/
  assert(n <= BL->n);
  for(i=0;i<n;i++)
    if(orig2loc[nodelist[i]] < 0 || orig2loc[nodelist[i]] >= BL->n)
      {
	fprintfd(stderr, "Proc %d nodelist[%d] = %d orig = %d n = %d \n", 
		DBL->proc_id, i, nodelist[i], orig2loc[nodelist[i]], BL->n);
	assert(orig2loc[nodelist[i]] >= 0);
	assert(orig2loc[nodelist[i]] < BL->n);
      }
#endif

  /** Compute the "local user to local HID" permutation vector **/
  /** liperm[i] is the new label of unknown i (in the local csr matrix) 
      in the phidal local matrix **/
  liperm = (dim_t *)malloc(sizeof(dim_t)*BL->n);
  for(i=0;i<BL->n;i++)
    liperm[i] = -1;

  for(i=0;i<n;i++)
    liperm[i] = orig2loc[nodelist[i]];



  free(nodelist);



  /** Need this because nodelist can be a subset of the local unknowns and there is a problem 
   in CS_Perm **/
  for(i=0;i<BL->n;i++)
    if(liperm[i] == -1)
      liperm[i] = i;


#ifdef DEBUG_M
  for(i=0;i<n;i++)
    {
      assert(liperm[i] >= 0);
      assert(liperm[i] < BL->n);
    }
#endif
 
  /** Transform the csr matrix into a sparrow matrix that represents 
      the local domain given by the HID structure **/
#ifdef DEBUG_M
  assert(n<=BL->n);
#endif

  /*if(n != BL->n)
    {
    fprintfv(5, stderr, "Il faut changer CSRcs pour qu'il n'initialise pas la matrice (CSinit) \n");
    assert(n == BL->n);
    }*/

  if(rsa == 1)
    {
      /** Symmetrize the matrix.**/
      /** NB: the permutation modify a triangular matrix for symmetric = 1**/
      PHIDAL_SymmetrizeMatrix(2, 0, n, ia, ja, ma, &ib, &jb, &mb);
    }
  else
    {
      ib = ia;
      jb = ja;
      mb = ma;
    }
  
  initCS(&mat, BL->n);
  CSRcs_InaRow(n, mb, jb, ib, &mat);

  if(rsa == 1)
    {
      free(ib);
      if(jb != NULL)
	free(jb);
      if(mb != NULL)
	free(mb);
    }

  CS_Perm(&mat, liperm);
  free(liperm);

  if(symmetric == 1)
    CS_GetUpper(1, "N", &mat, &mat);





  /**** IMPORTANT: WE HAVE TO SORT THE MATRIX IN ORDER THAT 
	THE PHIDAL MATRIX IS SORTED : THE FILL DENSE MATRIX FUNCTION
	NEEDS THAT APPARENTLY *****/
  /*fprintfd(stderr, "TRIE DOIT ETRE DEPLACE DANS REMPLISSAGE DBMATRIX \n");
    ascend_column_reorder(&mat);*/

  switch(op)
    {
    case 0 :
      /** Copy the coefficient in the local part of the matrix **/
      if(symmetric == 1)
	UPLO = "L";  /***@@OIMBE ATTENTION inverse of CscCopy see  PhidalDistrMatrix_Build **/
      else
	UPLO = "N";
      PhidalDistrMatrix_Build(symmetric, UPLO, &mat, A, DBL);
      break;

    case 1 :
      /** Add the coefficient to the matrix **/
      /*printferr("SetMatrixCoeff Add operation not yet implemented \n");*/
      if(symmetric == 1)
	PhidalMatrix_CscCopy(1, &mat, "U", &A->M, BL); /** csc matrix in symmetric ! **/
      else
	PhidalMatrix_CsrCopy(1, &mat, "N", &A->M, BL); /** csc matrix in symmetric ! **/
      break;

    case 2 :
      /** PhidalMatrix_Reinit(&A->M); c'est fait dans CsrCopy **/
      if(symmetric == 1)
	PhidalMatrix_CscCopy(2, &mat, "U", &A->M, BL); /** csc matrix in symmetric ! **/
      else
	PhidalMatrix_CsrCopy(2, &mat, "N", &A->M, BL); /** csc matrix in symmetric ! **/
      break;

    case 3 :
      /** PhidalMatrix_Reinit(&A->M); c'est fait dans CsrCopy **/
      if(symmetric == 1)
	PhidalMatrix_CscCopy(3, &mat, "U", &A->M, BL); /** csc matrix in symmetric ! **/
      else
	PhidalMatrix_CsrCopy(3, &mat, "N", &A->M, BL); /** csc matrix in symmetric ! **/
      break;

    }

  /** Do not need this anymore **/
  cleanCS(&mat);


  /** ATTENTION pour le add op == 1 : cela marche uniquement si 
      les block ne sont connus que par leur leader **/

  /** Commit the changes to neighbor processors **/
  if(over_add == 0)
    {
      /** job == 0: blocks are only known by their leader **/
      PhidalDistrMatrix_GatherCoef(0, 0, A, DBL);
    }
  else
    {
      /** job == 0: blocks are only known by their leader **/
      /** op == 1 : contribution are added **/
      PhidalDistrMatrix_GatherCoef(0, 1, A, DBL);
    }

  if(op != 0)
    {
      /** Need to recompute the communication vector since 
	  new coefficients have been added  **/
      if(A->commvec.init == 1)
	{
	  PhidalCommVec_Clean(&A->commvec);
	  PhidalCommVec_Setup(0, A, &A->commvec, DBL);
	}
    }

  free(ia);

  if(numflag == 1)
    /** Reconvert the matrix in Fortran numbering (start from 0 instead of 1) **/
    CSR_Cnum2Fnum(ja, ia1, n1);

}

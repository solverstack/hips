/* @authors P. HENON */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>

#include "phidal_parallel.h"


int PhidalDistrMatrix_Check(PhidalDistrMatrix *DM, PhidalDistrHID *DBL)
{
  int i, j, k, ind;
  PhidalHID *BL;
  PhidalMatrix *M;

  BL = &DBL->LHID;
  M = &DM->M;
  PhidalMatrix_Check(M, BL);
  
  for(i=M->tli; i<=M->bri;i++)
    {
      for(k=M->ria[i];k<M->ria[i+1];k++)
	{
	  ind = DM->rind[k];
	  if(DM->rlead[k] != DM->proc_id)
	    assert(ind >= 0);

	  if(ind >= 0 && (DM->pset_index[ind+1] - DM->pset_index[ind] <= 1))
	    {
	      fprintfd(stderr, "Error: block (%ld %ld) rind = %ld  \n", (long)i, (long)M->rja[k], (long)ind);
	      assert(DM->pset_index[DM->rind[k]+1] - DM->pset_index[DM->rind[k]] > 1);
	    }
	}
    }

  for(j=M->tlj; j<=M->brj;j++)
    {
      for(k=M->cia[j];k<M->cia[j+1];k++)
	{
	  ind = DM->cind[k];
	  if(DM->clead[k] != DM->proc_id)
	    assert(ind >= 0);
	  if(ind >= 0 && (DM->pset_index[ind+1] - DM->pset_index[ind] <= 1))
	    {
	      fprintfd(stderr, "Error: block (%ld %ld) cind = %ld \n", (long)i, (long)M->cja[k], (long)ind);
	      assert(DM->pset_index[ind+1] - DM->pset_index[ind] > 1);
	    }
	}
    }

  


  return 0;
}

/* @authors P. HENON */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>

#define PARALLEL

#include "phidal_parallel.h"


void PHIDAL_DistrPrecond(PhidalDistrMatrix *DA, PhidalDistrPrec *P, PhidalDistrHID *DBL, PhidalOptions *option)
{
#ifdef DEBUG_M
  assert(DA->M.symmetric == option->symmetric);
#endif
  
  /** Correct the options **/
  PhidalOptions_Fix(option, &DBL->LHID);

  /** Print the options **/
  if(option->verbose > 0 && DBL->proc_id == 0)
    PhidalOptions_Print(stdout, option);

  if(DA->M.symmetric == 0)
    PHIDAL_DistrMLILUT(DA, P, DBL, option);
  else
    {
      /*if(P->info != NULL)
	PrecInfo_AddNNZ(P->info, DA->M.dim1);*/
      PHIDAL_DistrMLICCT(DA, P, DBL, option);
    }
}


void PHIDAL_DistrMLILUT(PhidalDistrMatrix *DA, PhidalDistrPrec *P, PhidalDistrHID *DBL, PhidalOptions *option)
{
  REAL *droptab;
  PhidalMatrix *A;
  PhidalHID *BL;
  dim_t i;

  A = &DA->M;
  BL = &DBL->LHID;

  if(option->forwardlev == -1)
    option->forwardlev = BL->nlevel-1;

  if(option->forwardlev >= BL->nlevel || option->forwardlev < 0 )
    {
      option->forwardlev = BL->nlevel-1;
      /*fprintfv(5, stderr, "Invalid parameter : number of forward levels %d total number of level %d \n", option->forwardlev, BL->nlevel);*/
      fprintfv(5, stderr, "WARNING: the number of forwardlev has been reset to %d \n",option->forwardlev);  
    }

#ifdef DEBUG_M
  if(option->forwardlev > 0)
    assert(BL->block_levelindex[BL->nlevel]-BL->block_levelindex[1]  > 0);
#endif
  

  /**** On some processors the last levels could be void we have to set forwardlev 
	to avoid these void levels *****/
  for(i=BL->nlevel-1;i>=0;i--)
    if(BL->block_levelindex[i+1]-BL->block_levelindex[i] == 0)
      {
	option->forwardlev = MIN(i-1, option->forwardlev);
	/*fprintfv(5, stderr, "\n Proc %d : forwardlev = %d \n\n",	DBL->proc_id, option->forwardlev );*/
      }
    else
      break;

  if(option->forwardlev<0)
    {
      fprintfv(5, stderr, "ALL LEVELS ARE VOID !!! %d\n", DBL->proc_id);
      MPI_Abort(DBL->mpicom, -1);
    }


#ifdef DEBUG_M
  assert(option->forwardlev == 0 || option->forwardlev == 1);
  /** Le reste n'est pas implemente **/
  /*assert(option->schur_method == 0 || option->schur_method == 2);*/
#endif


  /*** If droptab != NULL then the dropping will be made according to the norm of the column ***/
  /*if(option->scale > 0)
    {
    droptab = (REAL *)malloc(sizeof(REAL)*A->dim1);
    bzero(droptab, sizeof(REAL)*A->dim1);
    }
    else*/
  droptab = NULL;
  PhidalDistrPrec_MLILUT(0, option->forwardlev, droptab,  DA, P, DBL, option);


  /******************************************************/
  /*  Initialize the communication structure for solve  */
  /******************************************************/
  PhidalCommVec_PrecSetup(P, DBL);

  if(droptab != NULL)
    free(droptab);
}

void  PhidalDistrPrec_MLILUT(int_t levelnum, int forwardlev, REAL *droptab, PhidalDistrMatrix *DA, PhidalDistrPrec *P, PhidalDistrHID *DBL, PhidalOptions *option)
{
  REAL droptol;
  REAL *scalerow=NULL, *scalecol=NULL;
  REAL *iscalerow=NULL, *iscalecol=NULL;
  dim_t i;
  chrono_t t1, t2, ttotal;
  char c;
  PhidalMatrix *A;
  PhidalHID *BL;
  
  A = &DA->M;
  BL = &DBL->LHID;

#ifdef DEBUG_M
  assert(A->dim1>0);
#endif

  PhidalDistrPrec_Init(P);

  P->dim = A->dim1;
  P->schur_method = option->schur_method;
  P->forwardlev = forwardlev;

#ifdef SCALE_ALL
  if(option->scale > 0  && levelnum >= 1)/** TO SCALE RECURSIVELY ALL THE LEVEL **/
#else
    if(option->scale > 0 && levelnum == 0)  /** TO SCALE ONLY THE MATRIX A **/
#endif
      {
	scalerow = (REAL *)malloc(sizeof(REAL)*A->dim1);
	iscalerow = (REAL *)malloc(sizeof(REAL)*A->dim1);
	scalecol = (REAL *)malloc(sizeof(REAL)*A->dim1);
	iscalecol = (REAL *)malloc(sizeof(REAL)*A->dim1);

	if(option->scale == 1)
	  {
	    /*** Unsymmetric scale ***/
	    PhidalDistrMatrix_UnsymScale(option->scalenbr, DA, DBL, scalerow, scalecol, iscalerow, iscalecol);
	
	  }
	else 
	  {
	    /*** Symmetric scale ***/
	    /*** SCALE = 2 ***/

	    /*** Symmetric scale ***/
	    /*PhidalDistrMatrix_ColNorm2(DA, DBL, iscaletab);*/
	    /** Temporaire **/
	    PhidalDistrMatrix_RowNorm2(DA, DBL, iscalerow);
	    PhidalDistrMatrix_ColNorm2(DA, DBL, iscalecol);
	    
	    /** Take the square root of the norm such that a term 
		aij = aij / (sqrt(rowi)*sqrt(colj)) **/
	    for(i=0;i<A->dim1;i++)
	      iscalecol[i] = sqrt(iscalecol[i]);
	    for(i=0;i<A->dim1;i++)
	      iscalerow[i] = sqrt(iscalerow[i]); 
	    for(i=0;i<A->dim1;i++)
	      scalecol[i] = 1.0/iscalecol[i];
	    for(i=0;i<A->dim1;i++)
	      scalerow[i] = 1.0/iscalerow[i];
	    
	    

	    PhidalDistrMatrix_ColMult2(scalecol, DA, DBL);
	    PhidalDistrMatrix_RowMult2(scalerow, DA, DBL);
	    
	    /**** Compute the droptab ******/
	    /**** MUST BE THERE : AFTER THE SCALING ****/
	    /**** BE CAREFUL USE A DROPTAB ONLY IF A SCALING IS DONE !! ***/
	    /*if(droptab != NULL)
	      PhidalDistrMatrix_ColNorm2(DA, DBL, droptab);*/
	  }

	/** Temporaire **/
	if(droptab != NULL)
	  PhidalDistrMatrix_RowNorm2(DA, DBL, droptab);
      }

  /********************************************/
  /** Dropping in the Schur complement matrix **/
  /********************************************/
  /** A AJOUTER **/
  /*if(levelnum>0 && option->dropSchur > 0)
    PhidalMatrix_DropT(A, option->dropSchur, droptab, BL);*/

  /*** Compute the numerical threshold to use at this level in the factorization **/
  if(levelnum == 0)
    droptol = option->droptol0;
  else
    {
#ifdef DEBUG_M
      assert(levelnum>=1);
#endif
      droptol = option->droptol1 * pow(option->droptolrat, (REAL)(levelnum-1.0));
    }


  if(forwardlev == 0)
    {
      /**** just factorize the level *****/
      if (levelnum == 0) c = 'M'; else c = 'S';
      fprintfv(5, stdout, "  Numeric Factorisation (%c)\n",c); 
      t1 = dwalltime();
       
      P->symmetric = 0;

      P->levelnum =  levelnum;
      M_MALLOC(P->LU, D(SCAL));
      PREC_L_SCAL(P) = (PhidalDistrMatrix *)malloc(sizeof(PhidalDistrMatrix));
      PREC_U_SCAL(P) = (PhidalDistrMatrix *)malloc(sizeof(PhidalDistrMatrix));
      P->D = NULL;
      if(option->pivoting == 1)
	{
	  P->pivoting = 1;
	  P->permtab = (int *)malloc(sizeof(int)*A->dim1);
	  /** OIMBE WORK TO DO IN PARALLEL ?**/
	  assert(0);
	}

      /*if(option->verbose >= 1)
        fprintfv(5, stdout, "Numerical threshold to factorize ALL levels >= %d = %g \n", levelnum, droptol);    */

      if(option->schur_method != 1 && levelnum > 0)
        {
          /** The factorization is done in place (A is a void matrix in return **/
          PHIDAL_DistrILUCT(1, DA, PREC_L_SCAL(P), PREC_U_SCAL(P), droptol, droptab, option->fillrat, DBL, option, P->info);
          if(P->info != NULL)
            {
              if(DA->M.virtual == 0)
                PrecInfo_SubNNZ(P->info, PhidalDistrMatrix_NNZ(DA));
            }
          PhidalDistrMatrix_Clean(DA);
        }
      else
        PHIDAL_DistrILUCT(0, DA, PREC_L_SCAL(P), PREC_U_SCAL(P),  droptol, droptab, option->fillrat, DBL, option, P->info);

      t2 = dwalltime(); ttotal = t2-t1;
      if(DBL->proc_id == 0)
	fprintfv(5, stdout, "   %c : Numeric Factorisation in %g seconds\n", c, t2-t1);
      goto unscale;
    }

  if(forwardlev == 1 && option->schur_method != 1)
    {
      /** In this case the GEMM and ICCT are done in one step to 
	  save the storage of an intermediate schur complement **/
      PhidalDistrPrec_MLILUTForward(levelnum, droptab, DA, P, DBL, option);
      goto unscale;
    }

  if(forwardlev > 0)
    {
      PhidalDistrPrec_MLILUTForward(levelnum, droptab, DA, P, DBL, option);
      /*** Go to next level ***/
      P->nextprec = (PhidalDistrPrec *)malloc(sizeof(PhidalDistrPrec));
      PhidalDistrPrec_Init(P->nextprec);
      P->nextprec->prevprec = P;
      P->nextprec->info = P->info;

      /** Important to call again PhidalPrec_MLICCT because of scaling **/
      if(droptab != NULL)
	PhidalDistrPrec_MLILUT(levelnum+1, forwardlev-1, droptab+(BL->block_index[PREC_S_SCAL(P)->M.tli]-BL->block_index[A->tli])
			       , PREC_S_SCAL(P), P->nextprec, DBL, option);
      else
	PhidalDistrPrec_MLILUT(levelnum+1, forwardlev-1, NULL, PREC_S_SCAL(P), P->nextprec, DBL, option);

      goto unscale;
    }


 unscale:
   

#ifdef SCALE_ALL
  if(option->scale >  0 && levelnum == 1) /** TO SCALE RECURSIVELY ALL THE LEVEL **/
#else
    if(option->scale >  0 && levelnum == 0) /** TO SCALE ONLY THE MATRIX A **/
#endif
      {
	/** Unscale the matrix and the preconditioner **/
	if(levelnum == 0 || option->schur_method == 1)
	  {
	    PhidalDistrMatrix_ColMult2(iscalecol, DA, DBL);
	    PhidalDistrMatrix_RowMult2(iscalerow, DA, DBL);
	  }

	PhidalDistrPrec_UnsymmetricUnscale(scalerow, iscalerow, scalecol, iscalecol, P, DBL);
      
	free(scalerow);
	free(scalecol);
	free(iscalerow);
	free(iscalecol);
      }

#ifdef DEBUG_M
  /*if(levelnum == 0)
    PhidalPrec_PrintInfo(P, BL);*/
#endif

}

void  PhidalDistrPrec_MLILUTForward(int_t levelnum, REAL *droptab, PhidalDistrMatrix *DA, PhidalDistrPrec *P, PhidalDistrHID *DBL, PhidalOptions *option)
{
  /**** A revoir: liberer le complement de Schur si schur_method == 0 ****/
  int Mstart, Mend, Sstart, Send;
  PhidalDistrMatrix M, G, W;
  int  *wki1, *wki2;
  COEF *wkd;
  Heap heap;
  int maxB;
  dim_t i;
  REAL droptol; 
  REAL* droptabtmp;
  chrono_t t1, t2, ttotal=0;
  char c;
  PhidalMatrix *A;
  PhidalHID *BL;
  INTL nnztmp=-1;

  A = &DA->M;
  BL = &DBL->LHID;

#ifdef DEBUG_M
  assert(levelnum < BL->nlevel);
#endif
  P->dim = A->dim1;
  P->symmetric = 0;
  P->levelnum =  levelnum;
  Mstart   =  BL->block_levelindex[levelnum];
  Mend     =  BL->block_levelindex[levelnum+1]-1;
  Sstart   =  BL->block_levelindex[levelnum+1];
  Send     =  BL->nblock-1;  

  /** Treat the case where the levelnum is void ! **/
  if(Mstart > Mend)
    {
      fprintfv(5, stderr, "Proc %d: the level %d is void \n", DBL->proc_id, levelnum);
      MPI_Abort(DBL->mpicom, -1);
      exit(-1);
    }

  M_MALLOC(P->LU, D(SCAL));
  PREC_L_SCAL(P) = (PhidalDistrMatrix *)malloc(sizeof(PhidalDistrMatrix));
  PREC_U_SCAL(P) = (PhidalDistrMatrix *)malloc(sizeof(PhidalDistrMatrix));
  P->D = NULL;


  /*** Compute the numerical threshold to use at this level in the factorization **/
  if(levelnum == 0)
    droptol = option->droptol0;
  else
    {
#ifdef DEBUG_M
      assert(levelnum>=1);
#endif
      droptol = option->droptol1 * pow(option->droptolrat, (REAL)(levelnum-1.0));
    }
  /*if(option->verbose >= 1)
    fprintfv(5, stdout, "Numerical threshold to factorize the level %d = %g \n", levelnum, droptol);	*/

  /*** Factorize the Level ***/
  PhidalDistrMatrix_Init(&M);
  PhidalDistrMatrix_BuildVirtualMatrix(Mstart, Mstart, Mend, Mend, DA, &M, DBL); 
  if(option->pivoting == 1)
    {
      P->pivoting = 1;
      P->permtab = (int *)malloc(sizeof(int)*M.M.dim1);
      /*** OIMBE : WORK TO DDO IN PARALLEL ? ***/
      assert(0);
    }

  M_MALLOC(P->EF, D(SCAL));
  PREC_E(P) = (PhidalDistrMatrix *)malloc(sizeof(PhidalDistrMatrix));
  PREC_F(P) = (PhidalDistrMatrix *)malloc(sizeof(PhidalDistrMatrix));

  if (levelnum == 0) c = 'M'; else c = 'S';
  fprintfv(5, stdout, "  Numeric Factorisation (%c)\n",c); 

  t1  = dwalltime();
  if(option->schur_method != 1 && levelnum > 0)
    {
      fprintfd(stderr, "level %ld avant ILUCT %ld \n", (long)levelnum, (long)P->info->nnzP);
      PHIDAL_DistrILUCT(1, &M, PREC_L_SCAL(P), PREC_U_SCAL(P), droptol, droptab, option->fillrat, DBL, option, P->info);
      fprintfd(stderr, "level %ld apres ILUCT %ld \n", (long)levelnum, (long)P->info->nnzP);

      PhidalDistrMatrix_Setup(Sstart, Mstart, Send, Mend, "N", "N" , option->locally_nbr, PREC_E(P), DBL);
      PhidalDistrMatrix_Cut(DA, PREC_E(P), DBL);
      PhidalDistrMatrix_Setup(Mstart, Sstart, Mend, Send, "N", "N" , option->locally_nbr, PREC_F(P), DBL);
      PhidalDistrMatrix_Cut(DA, PREC_F(P), DBL);
    }
  else
    {
      PHIDAL_DistrILUCT(0, &M, PREC_L_SCAL(P), PREC_U_SCAL(P), droptol, droptab, option->fillrat, DBL, option, P->info);
      PhidalDistrMatrix_BuildVirtualMatrix(Sstart, Mstart, Send, Mend, DA, PREC_E(P), DBL);
      PhidalDistrMatrix_BuildVirtualMatrix(Mstart, Sstart, Mend, Send, DA, PREC_F(P), DBL);
    }
  
  PhidalDistrMatrix_Clean(&M);

  t2 = dwalltime(); ttotal += t2-t1;
  fprintfv(5, stdout, "   %c : Numeric Factorisation in %g seconds\n", c, t2-t1);

  /************************************************/
  /* COMPUTE G= Lt^-1.E                           */
  /************************************************/
#ifdef DROP_TRSM
  droptol = option->dropSchur;
#endif

  fprintfv(5, stdout, " TRSM M / E\n");       
  t1  = dwalltime(); 

  maxB = 0;
  for(i=A->tli;i<=A->bri;i++)
    if(BL->block_index[i+1]-BL->block_index[i] > maxB)
      maxB = BL->block_index[i+1]-BL->block_index[i];

  /** Need that for CSCrowMultCSRcol (see RowCompact) **/
  Heap_Init(&heap, maxB);
  wki1 = (int *)malloc(sizeof(int)*MAX(maxB, BL->nblock));
  wki2 = (int *)malloc(sizeof(int)*A->dim1);
  wkd = (COEF *)malloc(sizeof(COEF)*maxB);

  PhidalDistrMatrix_Init(&G);

  /** IMPORTANT :G is always computed using a consistent block pattern **/
  /*PhidalMatrix_Setup(PREC_E(P)->tli, PREC_E(P)->tlj, PREC_E(P)->bri, PREC_E(P)->brj, "N", "N", BL->nlevel, &G, BL);*/

  PhidalDistrMatrix_Setup(PREC_E(P)->M.tli, PREC_E(P)->M.tlj, PREC_E(P)->M.bri, PREC_E(P)->M.brj, "N", "N", option->locally_nbr, &G, DBL);
  PhidalDistrMatrix_Copy(PREC_E(P), &G, DBL);
  /* == 0
    if(P->info != NULL)
    PrecInfo_AddNNZ(P->info, PhidalDistrMatrix_NNZ(&G));
    if(P->info != NULL)
    nnztmp = - PhidalDistrMatrix_NNZ(&G);
  */

  /*if(option->pivoting == 1)*/
  /** Important to pivot G and not E because E is a virtual matrix pointing in the last upper schur complement **/
  /*PhidalDistrMatrix_ColPerm(&G, P->permtab, DBL); */
  PHIDAL_InvUT(&PREC_U_SCAL(P)->M, &G.M, option->droptolE, droptab, option->fillrat, BL, &heap, wki1, wki2, wkd);
  
  if(P->info != NULL)
    {
      /*nnztmp += PhidalDistrMatrix_NNZ(&G);
	PrecInfo_AddNNZ(P->info, nnztmp);*/
      PrecInfo_AddNNZ(P->info, PhidalDistrMatrix_NNZ(&G));
    }



#ifdef SYMMETRIC_DROP
  droptabtmp = (REAL *)malloc(sizeof(REAL)*PREC_U_SCAL(P)->M.dim1);
  
#ifdef TYPE_COMPLEX
  {
    COEF* droptabtmpc = (COEF *)malloc(sizeof(COEF)*PREC_U_SCAL(P)->M.dim1);
    PhidalMatrix_GetUdiag(&PREC_U_SCAL(P)->M, droptabtmpc);
    for(i=0;i<PREC_U_SCAL(P)->M.dim1;i++)
      droptabtmp[i] = coefabs(1.0/droptabtmpc[i]);
    free(droptabtmpc);
  }
#else /* TYPE_COMPLEX */
  PhidalMatrix_GetUdiag(&PREC_U_SCAL(P)->M, droptabtmp);
  for(i=0;i<PREC_U_SCAL(P)->M.dim1;i++)
    droptabtmp[i] = coefabs(1.0/droptabtmp[i]);
#endif /* TYPE_COMPLEX */

  if(droptab != NULL)
    for(i=0;i<PREC_U_SCAL(P)->M.dim1;i++)
      droptabtmp[i] *= droptab[i];

#else /* SYMMETRIC_DROP */
  droptabtmp = droptab;
#endif
  PhidalDistrMatrix_Init(&W);
  PhidalDistrMatrix_Setup(PREC_F(P)->M.tli, PREC_F(P)->M.tlj, PREC_F(P)->M.bri, PREC_F(P)->M.brj, "N", "N", option->locally_nbr, &W, DBL);
  PhidalDistrMatrix_Copy(PREC_F(P), &W, DBL);
  /*if(P->info != NULL)
    PrecInfo_AddNNZ(P->info, PhidalDistrMatrix_NNZ(&W));
  if(P->info != NULL)
  nnztmp = - PhidalDistrMatrix_NNZ(&W);*/
 
  PHIDAL_InvLT(&PREC_L_SCAL(P)->M, &W.M, option->droptolE, droptabtmp, option->fillrat, BL, wki1, wki2, wkd);
  
  if(P->info != NULL)
    {
      /*nnztmp += PhidalDistrMatrix_NNZ(&W);
	PrecInfo_AddNNZ(P->info, nnztmp);*/
      PrecInfo_AddNNZ(P->info, PhidalDistrMatrix_NNZ(&W));
    }

  

#ifdef SYMMETRIC_DROP
  free(droptabtmp);
#endif /* SYMMETRIC_DROP */
   
  free(wki1);
  free(wki2);
  free(wkd);
  Heap_Exit(&heap); 

  t2  = dwalltime(); ttotal += t2-t1;
  fprintfv(5, stdout, "  TRSM in %g seconds\n\n", t2-t1);
   
  /*** Compute the schur complement on the remaining levels ****/
  fprintfv(5, stdout, " GEMM E/E -> S\n"); 
  t1  = dwalltime(); 

  M_MALLOC(P->S,  D(SCAL));
  PREC_S_SCAL(P) = (PhidalDistrMatrix *)malloc(sizeof(PhidalDistrMatrix));
  if(option->schur_method != 1)
    PhidalDistrMatrix_Setup(Sstart, Sstart, Send, Send, "N", "N", option->locally_nbr, PREC_S_SCAL(P), DBL);
  else
    PhidalDistrMatrix_Setup(Sstart, Sstart, Send, Send, "N", "N", BL->nlevel, PREC_S_SCAL(P), DBL);
  
  


  
  if(option->schur_method == 2)
    {
      M_MALLOC(P->B,  D(SCAL));
      PREC_B_SCAL(P) = (PhidalDistrMatrix *)malloc(sizeof(PhidalDistrMatrix));
      PhidalDistrMatrix_Init(PREC_B_SCAL(P));
      if(levelnum == 0)
	PhidalDistrMatrix_BuildVirtualMatrix(Sstart, Sstart, Send, Send, DA, PREC_B_SCAL(P), DBL);
      else
	{
	  PhidalDistrMatrix_Setup(Sstart, Sstart, Send, Send, "L", "N",
			     option->locally_nbr, PREC_B_SCAL(P), DBL);
	  /*PhidalMatrix_Setup(Sstart, Sstart, Send, Send, "L", "N", BL->nlevel, PREC_B_SCAL(P), BL);*/
	  PhidalDistrMatrix_Copy(DA, PREC_B_SCAL(P), DBL);
	  if(P->info != NULL)
	    PrecInfo_AddNNZ(P->info, PhidalDistrMatrix_NNZ(PREC_B_SCAL(P)));
	  
	}
    }

  if(option->schur_method != 1 && levelnum > 0)
    PhidalDistrMatrix_Cut(DA, PREC_S_SCAL(P), DBL);
  else
    {
      PhidalDistrMatrix_Copy(DA, PREC_S_SCAL(P), DBL);
      if(P->info != NULL)
	PrecInfo_AddNNZ(P->info, PhidalDistrMatrix_NNZ(PREC_S_SCAL(P)));
    }

  if(option->schur_method != 1 && levelnum > 0)
    {
      if(P->info != NULL)
	{
	  if(DA->M.virtual == 0)
	    PrecInfo_SubNNZ(P->info, PhidalDistrMatrix_NNZ(DA));
	}
      PhidalDistrMatrix_Clean(DA);
    }
  
  if(option->forwardlev == levelnum+1 && option->schur_method != 1)
    {
      P->nextprec = (PhidalDistrPrec *)malloc(sizeof(PhidalDistrPrec));
      PhidalDistrPrec_Init(P->nextprec);
      P->nextprec->prevprec = P;
      P->nextprec->info = P->info;
      
      /** OIMBE Passer Precinfo dans cette fonction ***/
      PhidalDistrPrec_GEMM_ILUCT(levelnum+1, droptab, &G, &W, PREC_S_SCAL(P), P->nextprec, DBL, option);

      if(P->info != NULL)
	PrecInfo_SubNNZ(P->info, PhidalDistrMatrix_NNZ(PREC_S_SCAL(P)));

      PhidalDistrMatrix_Clean(PREC_S_SCAL(P));
      free(PREC_S_SCAL(P));
      PREC_S_SCAL(P) = NULL;
    }
  else
    {
      /** G is destroyed inside this function **/
      /** Local schur product !! (no need to implement a parallel
	  version **/
      if(P->info != NULL)
	nnztmp =  - PhidalDistrMatrix_NNZ(PREC_S_SCAL(P));
      PhidalMatrix_ILUTSchur(1, &G.M, &W.M, &PREC_S_SCAL(P)->M, BL, option);
      if(P->info != NULL)
	{
	  nnztmp += PhidalDistrMatrix_NNZ(PREC_S_SCAL(P));
	  PrecInfo_AddNNZ(P->info, nnztmp);
	}

      if(P->info != NULL)
	nnztmp =  - PhidalDistrMatrix_NNZ(PREC_S_SCAL(P));
      PhidalDistrMatrix_GatherCoef(0, 1, PREC_S_SCAL(P), DBL);
      if(P->info != NULL)
	{
	  nnztmp +=  PhidalDistrMatrix_NNZ(PREC_S_SCAL(P));
	  PrecInfo_AddNNZ(P->info, nnztmp);
	}

    }


  if(P->info != NULL)
    {
      PrecInfo_SubNNZ(P->info, PhidalDistrMatrix_NNZ(&G));
      PrecInfo_SubNNZ(P->info, PhidalDistrMatrix_NNZ(&W));
    }
  PhidalDistrMatrix_Clean(&G);
  PhidalDistrMatrix_Clean(&W);

  t2  = dwalltime(); ttotal += t2-t1;
  fprintfv(5, stdout, "  GEMM in %g seconds\n\n", t2-t1);
}



/* @authors P. HENON */

#include <stdio.h>
#include <stdlib.h>
#include <string.h> /* memcpy */
#include <assert.h>

#include "phidal_parallel.h"
#include "phidal_struct.h"

#include "symbol.h"
#include "solver.h"

#include "db_struct.h"
#include "block.h"

void SymbolMatrix_GetLocal(SymbolMatrix *symbglob, SymbolMatrix *symbloc, PhidalHID *globBL, PhidalDistrHID *DBL)
{
  int *mask;
  dim_t i, j;

#ifdef DEBUG_M
  assert(symbglob->nodenbr == DBL->globn);
#endif

  mask = (int *)malloc(sizeof(int)*DBL->globn);
  bzero(mask,  sizeof(int)*DBL->globn);

  
  for(i=0;i<globBL->nblock;i++)
    if(DBL->glob2loc_blocknum[i] >= 0)
      {
	for(j=globBL->block_index[i];j<globBL->block_index[i+1];j++)
	  mask[j] = 1;
      }

  SymbolMatrix_Extract(symbglob, mask, symbloc);

  
  free(mask);
}



void SymbolMatrix_Extract(SymbolMatrix *symbin, int *mask, SymbolMatrix *symbout) 
{
  /**************************************************************/
  /* This function extracts a subSymbolMatrix of a SymbolMatrix */
  /* the subsymbol is built according to the mask               */
  /* any unknwons such that mask[i] != 0  will be in the sub    */
  /* symbol matrix                                              */
  /* NOTE: for convenience mask is modified by this function    */
  /*      (because it is used to store the local numbering of   */
  /*      the unknowns                                          */
  /**************************************************************/
  dim_t i, j, k, m;
  int *cblknewnum;

  SymbolMatrix_Init(symbout);

  /*** Compute the local numbering of the unknowns ***/
  j=0;
  for(i=0;i<symbin->nodenbr;i++)
    if(mask[i] == 0)
      mask[i] = -1;
    else
      mask[i] = j++;

  symbout->nodenbr = j;


  /*** Compute the local numbering of the cblk ***/
  cblknewnum = (int *)malloc(sizeof(int)*symbin->cblknbr);
  j=0;
  m=0;
  for(i=0;i<symbin->cblknbr;i++)
    if(mask[symbin->ccblktab[i].fcolnum] != -1)
      {
#ifdef DEBUG_M
	{
	  dim_t t;
	  for(t=symbin->ccblktab[i].fcolnum;t<=symbin->ccblktab[i].lcolnum;t++)
	    assert(mask[t] != -1);
	}
#endif
	for(k=symbin->bcblktab[i].fbloknum; k<=symbin->bcblktab[i].lbloknum;k++)
	  if(mask[symbin->bloktab[k].frownum] != -1)
	    {
#ifdef DEBUG_M
	      {
		dim_t t;
		for(t=symbin->bloktab[k].frownum;t<=symbin->bloktab[k].lrownum;t++)
		  assert(mask[t] != -1);
	      }
	      /*assert(mask[symbin->bloktab[k].lrownum] != -1);*/
#endif
	      m++;
	    }
#ifdef DEBUG_M
	  else
	    {
	      dim_t t;
	      for(t=symbin->bloktab[k].frownum;t<=symbin->bloktab[k].lrownum;t++)
		assert(mask[t] == -1);
	    }
#endif

	cblknewnum[i] = j++;
      }
    else
      {
#ifdef DEBUG_M
	{
	  dim_t t;
	  for(t=symbin->ccblktab[i].fcolnum;t<=symbin->ccblktab[i].lcolnum;t++)
	    assert(mask[t] == -1);
	}
	/*assert(mask[symbin->ccblktab[i].lcolnum] == -1);*/
#endif
	cblknewnum[i] = -1;
      }
  symbout->cblknbr = j;
  symbout->bloknbr = m;


  symbout->ccblktab = (SymbolCCblk *)malloc(sizeof(SymbolCCblk)* symbout->cblknbr);
  symbout->bcblktab = (SymbolBCblk *)malloc(sizeof(SymbolBCblk)* symbout->cblknbr);
  symbout->bloktab = (SymbolBlok *)malloc(sizeof(SymbolBlok)* symbout->bloknbr);
  j = 0;
  m = 0;
  for(i=0;i<symbin->cblknbr;i++)
    if(mask[symbin->ccblktab[i].fcolnum] != -1)
      {
	symbout->ccblktab[j].fcolnum = mask[symbin->ccblktab[i].fcolnum];
	symbout->ccblktab[j].lcolnum = mask[symbin->ccblktab[i].lcolnum];

	symbout->bcblktab[j].fbloknum = m;
	for(k=symbin->bcblktab[i].fbloknum; k<=symbin->bcblktab[i].lbloknum;k++)
	  if(mask[symbin->bloktab[k].frownum] != -1)
	    {
#ifdef DEBUG_M
	      assert(mask[symbin->bloktab[k].lrownum] != -1);
	      assert(mask[symbin->bloktab[k].lrownum] < symbout->nodenbr);
	      assert(cblknewnum[symbin->bloktab[k].cblknum] != -1);
#endif
	      symbout->bloktab[m].frownum = mask[symbin->bloktab[k].frownum];
	      symbout->bloktab[m].lrownum = mask[symbin->bloktab[k].lrownum];
	      symbout->bloktab[m].cblknum = cblknewnum[symbin->bloktab[k].cblknum];
	      m++;
	    }
	symbout->bcblktab[j].lbloknum = m-1;
	j++;
      }


  /** Fill stride and hdim **/
  /*symbout->stride = (int *)malloc(sizeof(int)*symbout->cblknbr);
  symbout->hdim = (int *)malloc(sizeof(int)*symbout->cblknbr);
  for(i=0;i<symbout->cblknbr;i++)
    {
      m = 0;

      for(j=symbout->bcblktab[i].fbloknum;j<=symbout->bcblktab[i].lbloknum;j++)
	m +=  symbout->bloktab[j].lrownum-symbout->bloktab[j].frownum+1;

      symbout->stride[i] = m;
      symbout->hdim[i] = m;
      }*/

  symbout->stride = NULL;
  symbout->hdim = NULL;
  
  symbout->tli = 0;
  symbout->facedecal = 0;
  symbout->virtual = 0;
  symbout->cblktlj = 0;

  free(cblknewnum);
}







void SymbolMatrix_FillCheck(int n, INTL *ia, dim_t *ja, SymbolMatrix *symbmtx)
{
  /**************************************************************/
  /* Function for debugging:                                    */
  /* Check if the csr matrix fits in the symbmtx                */
  /**************************************************************/
  dim_t i, j, k;
  int *mask;
  assert(ia[0] == 0); /** C numbering **/

  mask = (int *)malloc(sizeof(int)*n);

  for(k=0;k<symbmtx->cblknbr;k++)
    {
      bzero(mask, sizeof(int)*n);
      for(i=symbmtx->bcblktab[k].fbloknum;i<=symbmtx->bcblktab[k].lbloknum;i++)
	for(j=symbmtx->bloktab[i].frownum;j<=symbmtx->bloktab[i].lrownum;j++)
	  mask[j] = 1;
      
      for(i=symbmtx->ccblktab[k].fcolnum;i<=symbmtx->ccblktab[k].lcolnum;i++)
	for(j=ia[i];j<ia[i+1];j++)
	  {
	    if(ja[j] < i) /** only subdiagonal term **/
	      continue; 
	    assert(ja[j] >= 0);
	    assert(ja[j] < n);
	    if(mask[ja[j]] != 1)
	      fprintfd(stderr, "Coef (%d %d) not in symbol \n", ja[j], i);
	    /*assert(mask[ja[j]] == 1);*/
	  }
	
    }

  free(mask);
}

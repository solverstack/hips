/* @authors P. HENON */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>

#include "phidal_parallel.h"



void CSR_Reduce(flag_t job, flag_t op, mpi_t master, dim_t n, INTL *ia, dim_t *ja, COEF *a, INTL **iar, dim_t **jar, COEF **ar, MPI_Comm mpicom)
{
  /**********************************************************************************/
  /* This function gather and add a csr matrix distributed among all the processors */
  /* each csr part must be numbered with the global labels                          */
  /* job = 0 : coefficient are not taken into account                               */
  /* op = 0 : MAX based on modulus                                                  */
  /* op = 1 : Add                                                                   */
  /* NOTE: ia, ja are deallocated in this function                                  */
  /**********************************************************************************/
  mpi_t proc_id, nproc;
  INTL i, nnzmax, p;
  INTL *gia=NULL, *iat;
  dim_t *gja=NULL, *jat;
  
  INTL *tmp = NULL;
  COEF *ga=NULL, *at=NULL;
  MPI_Status status;

  MPI_Comm_rank(mpicom, &proc_id);
  MPI_Comm_size(mpicom, &nproc);

  if(nproc == 1)
    return;

#ifdef DEBUG_M
  assert(n>0);
#endif


  tmp = (INTL *)malloc(sizeof(INTL)*n);

  /** First, sum up the number of nnz in each row  **/
  for(i=0;i<n;i++)
    tmp[i] = ia[i+1]-ia[i];
  

  if(proc_id == master)
    {
      gia = (INTL *)malloc(sizeof(INTL)*(n+1));
      bzero(gia, sizeof(INTL)*(n+1));
    }

    MPI_Reduce(tmp, gia, FF(n), COMM_BIG_INT, MPI_SUM, master, mpicom);

  if(proc_id == master)
    {
      i = 0;
      MPI_Reduce(&i, &nnzmax, FF(1), COMM_BIG_INT, MPI_MAX, master, mpicom);
    }
  else
    MPI_Reduce(ia+n, &nnzmax, FF(1), COMM_BIG_INT, MPI_MAX, master, mpicom);
  
  /*for(i=0;i<n+1;i++)
    fprintfd(stderr, "P %d ia[%d] = %d \n", proc_id, i, ia[i]);*/

  if(proc_id == master)
    {
      /** Setup gia **/
      for(i=1;i<n;i++)
	gia[i] += gia[i-1];

      /** Reshift gia **/
      for(i=n;i>0;i--)
	gia[i] = gia[i-1];
      gia[0] = 0;

      /*for(i=0;i<n+1;i++)
	fprintfd(stderr, "gia[%ld] = %ld \n", i, gia[i]);*/

      fprintfd(stderr, "TOTAL NUMBER OF NNZEROS ON MASTER = %ld \n", (long)gia[n]);
      
      if(gia[n]>0)
	{
	  gja = (dim_t *)malloc(sizeof(dim_t)*gia[n]);
	  if(job != 0)
	    ga = (COEF *)malloc(sizeof(COEF)*gia[n]);
	}
      else
	{
	  gja = NULL;
	  if(job != 0)
	    ga = NULL;
	}

    }
  else
    free(tmp);

  if(proc_id != master)
    {
      MPI_Send(ia, FF(n+1), COMM_BIG_INT, master, proc_id, mpicom);
      if(ia[n] > 0)
	{
	  MPI_Send(ja, EE(ia[n]), COMM_INT, master, proc_id, mpicom);
	  if(job != 0)
	    MPI_Send(a, CC(ia[n]), MPI_COEF_TYPE, master, proc_id, mpicom);
	}

      free(ia);
      if(ja != NULL)
	free(ja);
      if(job != 0 && a != NULL)
	free(a);
    }
  else
    {
      iat = (INTL *)malloc(sizeof(INTL)*(n+1));
      if(nnzmax>0)
	{
	  jat = (dim_t *)malloc(sizeof(dim_t)*nnzmax);
	  if(job != 0)
	    at = (COEF *)malloc(sizeof(COEF)*nnzmax);
	}
      else
	{
	  jat = NULL;
	  if(job != 0)
	    at = NULL;
	}

      /*** First : put the local matrix of master in the global matrix **/
      for(i=0;i<n;i++)
	{
	  if(ia[i]==ia[i+1])
	    continue;
	  
	  if(ia[i+1]-ia[i]>0)
	    {
	      memcpy(gja + gia[i], ja + ia[i], sizeof(dim_t)*(ia[i+1]-ia[i]));
	      if(job != 0)
		memcpy(ga + gia[i], a + ia[i], sizeof(COEF)*(ia[i+1]-ia[i]));
	    }

	  gia[i] += ia[i+1]-ia[i];
	}
      

      /*** Serial receive but better for memory ***/
      for(p=0;p<nproc;p++)
	if(p!=master)
	  {
	    /*** OIMBE : recevoir de n'importe qui **/
	    MPI_Recv(iat, FF(n+1), COMM_BIG_INT, p, p, mpicom, &status);
#ifdef DEBUG_M
	    assert(iat[n] <= nnzmax);
#endif
	    if(iat[n] > 0)
	      {
		MPI_Recv(jat, FF(iat[n]), COMM_BIG_INT, p, p, mpicom, &status);
		if(job != 0)
		  MPI_Recv(at, CC(iat[n]), MPI_COEF_TYPE, p, p, mpicom, &status);
		
		/** Add this matrix **/
		
		for(i=0;i<n;i++)
		  {
		    if(iat[i]==iat[i+1])
		      continue;
		    
		    memcpy(gja + gia[i], jat + iat[i], sizeof(dim_t)*(iat[i+1]-iat[i]));
		    if(job != 0)
		      memcpy(ga + gia[i], at + iat[i], sizeof(COEF)*(iat[i+1]-iat[i]));
		    
		    gia[i] += iat[i+1]-iat[i];
		  }
	      }
	    
	  }

      /** Reshift gia **/
      for(i=n;i>0;i--)
	gia[i] = gia[i-1];
      gia[0] = 0;

      /** Add the local matrix **/
      free(ia);
      if(ja != NULL)
	free(ja);

      free(iat);
      if(jat != NULL)
	free(jat);
      if(job != 0)
	{
	  if(a != NULL)
	    free(a);
	  if(at != NULL)
	    free(at);
	}

      /*CSR_Write(stderr, n, gia, gja, ga);
	dumpcsr(stderr, ga, gja, gia, n);*/
      if(gia[n]>0)
	CSR_SuppressDouble(op, n, gia, gja, ga);


#ifdef DEBUG_M
      checkCSR(n, gia, gja, ga, 0);
#endif
    }

  if(proc_id == master)
    {
      *iar = gia;
      *jar = gja;
      if(job != 0)
	*ar = ga;

      free(tmp);
    }


}


INTS DistrCSR_Loc2Glob(dim_t ln, dim_t *unknownlist, INTL *lia, dim_t *ja, COEF *a, dim_t n, INTL **ia2R, dim_t **ja2R, COEF **a2R)
{
  /** Put unknownlist in C numbering **/
  INTL *ia2;
  dim_t *ja2;
  COEF *a2;
  dim_t i;

#ifdef DEBUG_M
  assert(lia[0] == 0); /** C numbering **/
#endif
  
  ia2 = (INTL *)malloc(sizeof(INTL) * (n+1));
  CHECK_MALLOC(ia2);
  ja2 = (INTS *)malloc(sizeof(INTS)* lia[ln]);
  CHECK_MALLOC(ja2);
  if(a != NULL)
    {
      a2 = (COEF *)malloc(sizeof(COEF)* lia[ln]);
      CHECK_MALLOC(a2);
    }
  
  /** Compute ia **/
  bzero(ia2, sizeof(INTL)*(n+1));
  for(i=0;i<ln;i++)
    ia2[unknownlist[i]+1] = lia[i+1]-lia[i];
  
  for(i=1;i<=n;i++)
    ia2[i] += ia2[i-1];
  

  for(i=0;i<ln;i++)
    memcpy(ja2 + ia2[unknownlist[i]], ja + lia[i], sizeof(INTS)*(lia[i+1]-lia[i])); 
  
  if(a!=NULL)
    for(i=0;i<ln;i++)
      memcpy(a2 + ia2[unknownlist[i]], a + lia[i], sizeof(COEF)*(lia[i+1]-lia[i])); 

  *ia2R = ia2;
  *ja2R = ja2;
  if(a!=NULL)
    *a2R = a2;

  return HIPS_SUCCESS;
}

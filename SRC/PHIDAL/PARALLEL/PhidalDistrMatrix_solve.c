/* @authors P. HENON */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>
#include <mpi.h>
#include "phidal_parallel.h"

void PhidalDistrMatrix_Lsolve(flag_t unitdiag, PhidalDistrMatrix *DL, COEF *x, COEF *b, PhidalDistrHID *DBL)
{
  /*****************************************************************************************/
  /* This function compute x = L^-1.b                                                      */
  /* If unitdiag == 1 then the diagonal is considered to be unitary (stored or not stored) */
  /*   THIS CONCERNS THE DIAGONAL NOT THE BLOCK DIAGONAL                                   */
  /* Work also in place (i.e. x == b)                                                      */
  /*****************************************************************************************/
  dim_t i, j, k;
  int *bind;
  COEF *xptr, *xiptr;
  int offset;
  PhidalMatrix *L;
  PhidalHID *BL;
  PhidalCommVec *commvec;
  mpi_t proc_id;

  BL = &DBL->LHID;
  proc_id = DL->proc_id;
  L = &(DL->M);
  commvec = &(DL->commvec);
  bind = BL->block_index;
  offset = bind[L->tli];

#ifdef DEBUG_M
  assert(L->dim1 == L->dim2);
  assert(L->tli == L->tlj);
  assert(L->bri == L->brj);
  assert(commvec->init == 1);
#endif

  if(x != b)
    memcpy(x, b, sizeof(COEF)*L->dim1);


  /*bzero(commvec->t, sizeof(COEF)*L->dim1);*/ /*NO NEED */
  /** IMPORTANT: we store the contribution for x[i] part in x 
      so it must be initialized to zero where the processor is not
      leader **/
  for(i=L->tli;i<=L->bri;i++)
    if(DBL->row_leader[i] != proc_id)
      bzero(x + bind[i]-offset, sizeof(COEF)*(bind[i+1]-bind[i]));

  memcpy(commvec->loc_cnt, commvec->loc_ctrb, sizeof(int)*(L->bri-L->tli+1));

  /** Start the persistent receive requests **/
  for(i=L->tli;i<=L->bri;i++)
    {
      if(commvec->out_ctrb[i-L->tli] > 0)
	{
#ifdef TRACE_COM
	  fprintfd(stderr, "Proc %d poste ctrb receive %d \n", DL->proc_id, DBL->loc2glob_blocknum[i]);
#endif
	  if(MPI_Startall(commvec->out_ctrb[i-L->tli], commvec->recv_rqtab[i-L->tli]) != MPI_SUCCESS)
	    {
	      fprintfd(stderr, "Error in start recv contrib for vec %d \n", i);
	      assert(0);
	    }
	}
      if(commvec->out_ctrb[i-L->tli] == -1)
	{
#ifdef TRACE_COM
	  fprintfd(stderr, "Proc %d poste vector receive %d \n", DL->proc_id, DBL->loc2glob_blocknum[i]);
#endif 
	  if(MPI_Start(commvec->recv_rqtab[i-L->tli]) != MPI_SUCCESS)
	    {
	      fprintfd(stderr, "Error in start recv vector for vec %d \n", i);
	      assert(0);
	    }
	}
    }

  /** We use a column algorithm 
      that allows to overlap communication and computation ***/
  for(j=L->tlj;j<=L->brj;j++)
    {
      xptr = x+(bind[j]-offset);
#ifdef DEBUG_M
      assert(L->cja[L->cia[j]] == j);
#endif
      if(DBL->row_leader[j] == proc_id)
	{
	  if(commvec->out_ctrb[j-L->tlj] > 0)
	    {
	      /** Receive the contribution from non leader processors **/
	      PhidalCommVec_ReceiveVecAdd(1, j-L->tlj, xptr, commvec); 
	      /*fprintfd(stderr, "Proc %d receive ctrb %d = %g \n", DL->proc_id, DBL->loc2glob_blocknum[j], 
		norm2(commvec->recv_buffer[j-L->tlj], commvec->veclen[j-L->tlj]));*/
	    }

	  /** Solve **/
	  if(L->csc == 0)
	    CSR_Lsol(unitdiag, L->ca[L->cia[j]], xptr, xptr);
	  else
	    CSC_Lsol(unitdiag, L->ca[L->cia[j]], xptr, xptr);

	  if(DBL->row_leader[j] == proc_id && DBL->block_psetindex[j+1]-DBL->block_psetindex[j]>1)
	    {
	      /** Send the final vector to non-leader processor **/
	      for(k=DBL->block_psetindex[j];k<DBL->block_psetindex[j+1];k++)
		if(DBL->block_pset[k] != proc_id)
		  MPI_Send(xptr, CC(bind[j+1]-bind[j]), MPI_COEF_TYPE, DBL->block_pset[k], DBL->loc2glob_blocknum[j], DBL->mpicom); 
	      /*memcpy(commvec->t + bind[j]-offset, xptr, sizeof(COEF) * (commvec->veclen[j-L->tlj]));
		fprintfd(stderr, "Proc %d send final x %d \n", DL->proc_id, DBL->loc2glob_blocknum[j]);
	      MPI_Startall(commvec->out_ctrb[j-L->tlj], commvec->send_rqtab[j-L->tlj]);
	      MPI_Waitall(commvec->out_ctrb[j-L->tlj], commvec->send_rqtab[j-L->tlj], commvec->status);*/
	    }
	}
      else
	{
#ifdef DEBUG_M
	  assert(commvec->out_ctrb[j-L->tlj] == -1);
#endif
	  /** Receive the vector from the row_leader **/
	  if(MPI_Wait(commvec->recv_rqtab[j-L->tlj], commvec->status) != MPI_SUCCESS)
	    {
	      fprintfd(stderr, "Error in PhidalDistrMatrix_LSolve for vector %d \n" ,j);
	      MPI_Abort(DBL->mpicom, - 1);
	    }

	  memcpy(xptr, commvec->t+(bind[j]-offset), sizeof(COEF) * (commvec->veclen[j-L->tlj]));
	}
	
      /*** Multiply x[j]  by the off-diagonal blocks **/
      for(k=L->cia[j]+1;k<L->cia[j+1];k++)
#ifndef POUR_DEBUG
	if(DL->clead[k] == proc_id && L->ca[k]->nnzr > 0) 
#else
	if(DL->clead[k] == proc_id) 
#endif
	  {
	    i = L->cja[k];
	    xiptr = x+(bind[i]-offset);
	    
	    /*fprintfd(stderr, "Proc %d: norm %d before ctrb = %g \n", DBL->proc_id,DBL->loc2glob_blocknum[i],  
	      norm2(xiptr, bind[i+1]-bind[i]));*/
	    if(L->csc == 0)
	      matvecz(L->ca[k], xptr, xiptr, xiptr); 
	    else
	      CSC_matvecz(L->ca[k], xptr, xiptr, xiptr); 

	    /*fprintfd(stderr, "Proc %d: norm %d after ctrb = %g \n", DBL->proc_id, DBL->loc2glob_blocknum[i], 
	      norm2(xiptr, bind[i+1]-bind[i]));*/

	    if(DBL->row_leader[i] != proc_id && --(commvec->loc_cnt[i-L->tli]) == 0 )
	      {
		/** Send the contribution to row_leader processor
		 **/
		/*fprintfd(stderr, "Proc %d send ctrb %d Norm = %g \n", DL->proc_id, DBL->loc2glob_blocknum[i], 
		  norm2(xiptr, bind[i+1]-bind[i]));*/
		
		MPI_Send(xiptr, CC(bind[i+1]-bind[i]), MPI_COEF_TYPE, DBL->row_leader[i], 
			 DBL->loc2glob_blocknum[i], DBL->mpicom); 
	      }
	  }
    }
  
  /*for(i=L->tli;i<=L->bri;i++)
    if(DBL->row_leader[i] == proc_id)
      fprintfd(stderr, "Proc %d norm2b[%d] = %g \n", DL->proc_id, DBL->loc2glob_blocknum[i],
      norm2(x+(BL->block_index[i]-offset), BL->block_index[i+1]-BL->block_index[i]));*/
}



void PhidalDistrMatrix_Usolve(flag_t unitdiag, PhidalDistrMatrix *DU, COEF *x, COEF *b, PhidalDistrHID *DBL)
{
  /*****************************************************************************************/
  /* This function computes x = U^-1.b                                                     */
  /* If unitdiag == 1 then the diagonal is considered to be unitary (stored or not stored) */
  /*   THIS CONCERNS THE DIAGONAL NOT THE BLOCK DIAGONAL                                   */
  /* Work also in place (i.e. x == b)                                                      */
  /*****************************************************************************************/
  dim_t i, j, k;
  int *bind;
  COEF *xptr, *xiptr;
  int offset;
  PhidalMatrix *U;
  PhidalHID *BL;
  PhidalCommVec *commvec;
  mpi_t proc_id;

  BL = &DBL->LHID;
  proc_id = DU->proc_id;
  U = &(DU->M);
  commvec = &(DU->commvec);
  bind = BL->block_index;
  offset = bind[U->tli];


#ifdef DEBUG_M
  assert(U->dim1 == U->dim2);
  assert(U->tli == U->tlj);
  assert(U->bri == U->brj);
  assert(commvec->init == 1);
#endif
  if(x != b)
    memcpy(x, b, sizeof(COEF)*U->dim1);


  /*bzero(commvec->t, sizeof(COEF)*U->dim1);*/ /*NO NEED */
  /** IMPORTANT: we store the contribution for x[i] part in x 
      so it must be initialized to zero where the processor is not
      leader **/
  for(i=U->tli;i<=U->bri;i++)
    if(DBL->row_leader[i] != proc_id)
      bzero(x + bind[i]-offset, sizeof(COEF)*(bind[i+1]-bind[i]));

  memcpy(commvec->loc_cnt, commvec->loc_ctrb, sizeof(int)*(U->bri-U->tli+1));

  /** Start the persistent receive requests **/
  for(i=U->bri;i>=U->tli;i--)
    {
      if(commvec->out_ctrb[i-U->tli] > 0)
	{
	  /*fprintfd(stderr, "Proc %d poste ctrb receive %d \n", DU->proc_id, i);*/
	  if(MPI_Startall(commvec->out_ctrb[i-U->tli], commvec->recv_rqtab[i-U->tli]) != MPI_SUCCESS)
	    {
	      fprintfd(stderr, "Error in start recv contrib for vec %d \n", i);
	      assert(0);
	    }
	}
      if(commvec->out_ctrb[i-U->tli] == -1)
	if(MPI_Start(commvec->recv_rqtab[i-U->tli]) != MPI_SUCCESS)
	  {
	    fprintfd(stderr, "Error in start recv vector for vec %d \n", i);
	    assert(0);
	  }
    }

  /** We use a column algorithm 
      that allows to overlap communication and computation ***/
  for(j=U->brj;j>=U->tlj;j--)
    {
      xptr = x+(bind[j]-offset);
#ifdef DEBUG_M
      assert(U->cja[U->cia[j+1]-1] == j);
#endif
      if(DBL->row_leader[j] == proc_id)
	{
	  if(commvec->out_ctrb[j-U->tlj] > 0)
	    {
	      /** Receive the contribution from non leader processors **/
	      PhidalCommVec_ReceiveVecAdd(1, j-U->tlj, xptr, commvec); 
	      /*fprintfd(stderr, "Proc %d receive ctrb %d = %g \n", DU->proc_id, DBL->loc2glob_blocknum[j], 
		norm2(commvec->recv_buffer[j-U->tlj], commvec->veclen[j-U->tlj]));*/
	    }

	  /** Solve **/
	  if(U->csc == 0)
	    CSR_Usol(unitdiag, U->ca[U->cia[j+1]-1], xptr, xptr);
	  else
	    CSC_Usol(unitdiag, U->ca[U->cia[j+1]-1], xptr, xptr);

	  if(DBL->row_leader[j] == proc_id && DBL->block_psetindex[j+1]-DBL->block_psetindex[j]>1)
	    {
	      /** Send the final vector to non-leader processor **/
	      for(k=DBL->block_psetindex[j];k<DBL->block_psetindex[j+1];k++)
		if(DBL->block_pset[k] != proc_id)
		  MPI_Send(xptr, CC(bind[j+1]-bind[j]), MPI_COEF_TYPE, DBL->block_pset[k], DBL->loc2glob_blocknum[j], DBL->mpicom); 
	      /*memcpy(commvec->t + bind[j]-offset, xptr, sizeof(COEF) * (commvec->veclen[j-U->tlj]));
		fprintfd(stderr, "Proc %d send final x %d \n", DU->proc_id, DBL->loc2glob_blocknum[j]);
	      MPI_Startall(commvec->out_ctrb[j-U->tlj], commvec->send_rqtab[j-U->tlj]);
	      MPI_Waitall(commvec->out_ctrb[j-U->tlj], commvec->send_rqtab[j-U->tlj], commvec->status);*/
	    }
	}
      else
	{
#ifdef DEBUG_M
	  assert(commvec->out_ctrb[j-U->tlj] == -1);
#endif
	  /** Receive the vector from the row_leader **/
	  if(MPI_Wait(commvec->recv_rqtab[j-U->tlj], commvec->status) != MPI_SUCCESS)
	    {
	      fprintfd(stderr, "Error in PhidalDistrMatrix_LSolve for vector %d \n" ,j);
	      MPI_Abort(DBL->mpicom, - 1);
	    }

	  memcpy(xptr, commvec->t+(bind[j]-offset), sizeof(COEF) * (commvec->veclen[j-U->tlj]));
	}
	
      /*** Multiply x[j]  by the off-diagonal blocks **/
      for(k=U->cia[j+1]-2;k>=U->cia[j];k--)
	/*for(k=U->cia[j];k<U->cia[j+1]-1;k++)*/
#ifndef POUR_DEBUG
	if(DU->clead[k] == proc_id && U->ca[k]->nnzr > 0)
#else
	if(DU->clead[k] == proc_id)
#endif
	  {
	    i = U->cja[k];
	    xiptr = x+(bind[i]-offset);
	    
	    /*fprintfd(stderr, "Proc %d: norm %d before ctrb = %g \n", DBL->proc_id,DBL->loc2glob_blocknum[i],  
	      norm2(xiptr, bind[i+1]-bind[i]));*/
	    if(U->csc == 0)
	      matvecz(U->ca[k], xptr, xiptr, xiptr); 
	    else
	      CSC_matvecz(U->ca[k], xptr, xiptr, xiptr); 

	    /*fprintfd(stderr, "Proc %d: norm %d after ctrb = %g \n", DBL->proc_id, DBL->loc2glob_blocknum[i], 
	      norm2(xiptr, bind[i+1]-bind[i]));*/

	    if(DBL->row_leader[i] != proc_id && --(commvec->loc_cnt[i-U->tli]) == 0 )
	      {
		/** Send the contribution to row_leader processor
		 **/
		/*fprintfd(stderr, "Proc %d send ctrb %d Norm = %g \n", DU->proc_id, DBL->loc2glob_blocknum[i], 
		  norm2(xiptr, bind[i+1]-bind[i]));*/
		
		MPI_Send(xiptr, CC(bind[i+1]-bind[i]), MPI_COEF_TYPE, DBL->row_leader[i], 
			 DBL->loc2glob_blocknum[i], DBL->mpicom); 
	      }
	  }
    }
  
  /*for(i=U->tli;i<=U->bri;i++)
    if(DBL->row_leader[i] == proc_id)
      fprintfd(stderr, "Proc %d norm2b[%d] = %g \n", DU->proc_id, DBL->loc2glob_blocknum[i],
      norm2(x+(BL->block_index[i]-offset), BL->block_index[i+1]-BL->block_index[i]));*/
}




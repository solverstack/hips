/* @authors P. HENON */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>

#include "phidal_parallel.h"


void PhidalDistrMatrix_RowNorm2(PhidalDistrMatrix *DA, PhidalDistrHID *DBL, REAL *normtab)
{
  /**************************************************/
  /* Gives the norms 2 of each rows                 */
  /* NOTE: all the processors know the norms of the */
  /* rows corresponding to its local matrix         */
  /* A communicator must have been setup before     */
  /* calling this function (like a matrix-vector    */
  /* product)                                       */
  /**************************************************/

  dim_t i, k;
#if defined(TYPE_COMPLEX) || defined(DEBUG_M)
  dim_t j;
#endif
  PhidalMatrix *A;
  PhidalHID *BL;
  PhidalCommVec *commvec;
  mpi_t proc_id;
  dim_t *bind;
  COEF *normtab2;
  REAL *normptr;
  COEF *normptr2;
#ifdef TYPE_COMPLEX
  /** In complex we have to use a complex vector to store 
      real du to the compatibility with CommVec buffers ***/
  normtab2 = (COEF *)malloc(sizeof(COEF) * DA->M.dim1);
  bzero(normtab2, sizeof(COEF)*DA->M.dim1);
#else
  normtab2 = normtab;
#endif

  BL = &DBL->LHID;
  proc_id = DBL->proc_id;
  commvec = &(DA->commvec);
  A = &(DA->M);
  bind = BL->block_index;

#ifdef DEBUG_M
  assert(commvec->init == 1);
#endif
  
  /*******************************************/
  /** Start the persistent receive requests **/
  /*******************************************/
  for(i=A->bri;i>=A->tli;i--)
    {
      if(commvec->out_ctrb[i-A->tli] > 0)
	if(MPI_Startall(commvec->out_ctrb[i-A->tli], commvec->recv_rqtab[i-A->tli]) != MPI_SUCCESS)
	  {
	    fprintfd(stderr, "Error in start recv contrib for vec %d \n", i);
	    assert(0);
	  }

      if(commvec->out_ctrb[i-A->tli] == -1)
	if(MPI_Start(commvec->recv_rqtab[i-A->tli]) != MPI_SUCCESS)
	  {
	    fprintfd(stderr, "Error in start recv vector for vec %d \n", i);
	    assert(0);
	  }
    }


  /** Initialize the contribution counters **/
  memcpy(commvec->loc_cnt, commvec->loc_ctrb, sizeof(int)*(A->bri-A->tli+1));
  bzero(normtab, sizeof(REAL)*A->dim1);
  
  for(i=A->bri;i>=A->tli;i--)
    {
      normptr = normtab + bind[i] - bind[A->tli];
      normptr2 = normtab2 + bind[i] - bind[A->tli];

      for(k=A->ria[i];k<A->ria[i+1];k++)
	if(DA->rlead[k] == proc_id &&  A->ra[k]->nnzr >0)
	  {
	    if(A->csc == 0)
	      CSRrownorm2(A->ra[k], normptr);
	    else
	      CSRcolnorm2(A->ra[k], normptr);


#ifdef TYPE_COMPLEX
	    for(j=0;j<bind[i+1]-bind[i];j++)
	      normptr2[j] = (COEF)normptr[j];

#ifdef DEBUG_M
	    for(j=0;j<bind[i+1]-bind[i];j++)
	      {
		if(! (normptr[j] >= 0) )
		  fprintfd(stderr, "ERROR normptr[%d] = %g \n", j, normptr[j]);
		assert(normptr[j]>=0);
	      }

	    for(j=0;j<bind[i+1]-bind[i];j++)
	      {
		assert(CIMAG(normptr2[j])==0);
		assert(CREAL(normptr2[j])>=0);
		assert(coefabs(normptr2[j])>=0);
	      }
#endif
#endif

#ifdef DEBUG_M
	    assert(commvec->loc_cnt[i-A->tli]>0);
#endif
	    if(--commvec->loc_cnt[i-A->tli] == 0 && DBL->row_leader[i] != proc_id)
	      {
		/**** Send Ctrb i ***/
		MPI_Send(normptr2, CC(bind[i+1]-bind[i]), MPI_COEF_TYPE, DBL->row_leader[i], DBL->loc2glob_blocknum[i], DBL->mpicom); 
		break;
	      }
	  }
      
      if(A->symmetric == 1)
	{
	  /** Treat the symmetric part **/
#ifdef DEBUG_M
	  assert(A->csc == 1);
	  assert(A->cja[A->cia[i]] == i);
#endif
	  if(DA->clead[A->cia[i]] == proc_id && A->ca[A->cia[i]]->nnzr > 0)
	    {
	      if(A->csc == 0)
		CSR_NoDiag_colnorm2(A->ca[A->cia[i]], normptr);
	      else
		CSR_NoDiag_rownorm2(A->ca[A->cia[i]], normptr);

#ifdef TYPE_COMPLEX
	      for(j=0;j<bind[i+1]-bind[i];j++)
		normptr2[j] = normptr[j];
	      	    
#ifdef DEBUG_M
	      for(j=0;j< bind[i+1]-bind[i];j++)
		{
		  assert(CIMAG(normptr2[j])==0);
		  assert(CREAL(normptr2[j])>=0);
		  assert(coefabs(normptr2[j])>=0);
		}
#endif
#endif

#ifdef DEBUG_M
	      assert(commvec->loc_cnt[i-A->tli]>0);
#endif
	      if(--commvec->loc_cnt[i-A->tli] == 0 && DBL->row_leader[i] != proc_id) 
		MPI_Send(normptr2, CC(bind[i+1]-bind[i]), MPI_COEF_TYPE, DBL->row_leader[i], DBL->loc2glob_blocknum[i], DBL->mpicom);
	    }

	  for(k=A->cia[i]+1;k<A->cia[i+1];k++)
	    if(DA->clead[k] == proc_id && A->ca[k]->nnzr > 0)
	      {
		if(A->csc == 0)
		  CSRcolnorm2(A->ca[k], normptr);
		else
		  CSRrownorm2(A->ca[k], normptr);

#ifdef TYPE_COMPLEX
		for(j=0;j<bind[i+1]-bind[i];j++)
		  normptr2[j] = (COEF)normptr[j];
#ifdef DEBUG_M
		for(j=0;j< bind[i+1]-bind[i];j++)
		  {
		    assert(CIMAG(normptr2[j])==0);
		    assert(CREAL(normptr2[j])>=0);
		    assert(coefabs(normptr2[j])>=0);
		  }
#endif
#endif

#ifdef DEBUG_M
		assert(commvec->loc_cnt[i-A->tli]>0);
#endif

		if(--commvec->loc_cnt[i-A->tli] == 0 && DBL->row_leader[i] != proc_id) 
		  {
		    MPI_Send(normptr2, CC(bind[i+1]-bind[i]), MPI_COEF_TYPE, DBL->row_leader[i], DBL->loc2glob_blocknum[i], DBL->mpicom);
		    break;
		  }
	      }
	}

      /**** Send Ctrb i ***/
      /*if(DBL->row_leader[ i ] != proc_id) 
	MPI_Send(normptr2, CC(bind[i+1]-bind[i]), MPI_COEF_TYPE, DBL->row_leader[i], DBL->loc2glob_blocknum[i], DBL->mpicom);*/
    }
    

  /*** Receive the contributions where the processor is the owner of the facing diagonal block ***/
  for(i=A->bri;i>=A->tli;i--)
    {

      normptr = normtab + bind[i] - bind[A->tli];
      normptr2 = normtab2 + bind[i] - bind[A->tli];

      if(commvec->out_ctrb[ i-A->tli ] > 0)
	{
	  PhidalCommVec_ReceiveVecAdd(1, i-A->tli, normptr2, commvec); 
	}
      
   

      if(DBL->row_leader[i] == proc_id)
	{

#ifdef DEBUG_M
	  for(j=0;j< bind[i+1]-bind[i];j++)
	    {
	      assert(CIMAG(normptr2[j])==0);
	      assert(CREAL(normptr2[j])>=0);
	      assert(coefabs(normptr2[j])>=0);
	    }
#endif


	  /** square root **/
	  for(k=0;k<bind[i+1]-bind[i];k++)
	    normptr2[k] = coefsqrt(normptr2[k]);

	  /** Send the vector to row non-leader processor **/
	  if(DBL->block_psetindex[i+1]-DBL->block_psetindex[i]>1)
	    for(k=DBL->block_psetindex[i];k<DBL->block_psetindex[i+1];k++)
	      if(DBL->block_pset[k] != DBL->proc_id)
		MPI_Send(normptr2, CC(bind[i+1]-bind[i]), MPI_COEF_TYPE, DBL->block_pset[k],DBL->loc2glob_blocknum[i], DBL->mpicom);
	  
	}
    }
  
  /*** Receive the vector y parts where the processor is not the
       row_leader **/
  for(i=A->bri;i>=A->tli;i--)
    if(commvec->out_ctrb[ i-A->tli ] == -1)
      {
	if(MPI_Wait(commvec->recv_rqtab[i-A->tli], commvec->status) != MPI_SUCCESS)
	  {
	    fprintfd(stderr, "Error in PhidalDistrMatrix_Norm for vector %d \n" ,i);
	    assert(0);
	  }
	normptr2 = normtab2 + bind[i] - bind[A->tli];
	memcpy(normptr2, commvec->t + bind[i]- bind[A->tli], sizeof(COEF)* (bind[i+1]-bind[i]));
#ifdef DEBUG_M
	for(j=0;j< bind[i+1]- bind[i];j++)
	  {
	    assert(CIMAG(normptr2[j])==0);
	    assert(CREAL(normptr2[j])>=0);
	    assert(coefabs(normptr2[j])>=0);
	  }
#endif

      }
#ifdef DEBUG_M
  for(i=0;i<DA->M.dim1;i++)
    {
      assert(CREAL(normtab2[i])>=0);
      assert(coefabs(normtab2[i])>=0);
    }
#endif

#ifdef TYPE_COMPLEX
  for(i=0;i<DA->M.dim1;i++)
    normtab[i] = CREAL(normtab2[i]);
  free(normtab2);
#endif
}


void PhidalDistrMatrix_ColNorm2(PhidalDistrMatrix *DA, PhidalDistrHID *DBL, REAL *normtab)
{
  /**************************************************/
  /* Gives the norms 2 of each columns              */
  /* NOTE: all the processors know the norms of the */
  /* rows corresponding to its local matrix         */
  /* A communicator must have been setup before     */
  /* calling this function (like a matrix-vector    */
  /* product)                                       */
  /**************************************************/

  int j, k/* , i */;

  PhidalMatrix *A;
  PhidalHID *BL;
  PhidalCommVec *commvec;

  mpi_t proc_id;
  int *bind;
  REAL *normptr;
  COEF *normptr2;
  COEF *normtab2;
#ifdef TYPE_COMPLEX
  /** In complex we have to use a complex vector to store 
      real du to the compatibility with CommVec buffers ***/
  normtab2 = (COEF *)malloc(sizeof(COEF) * DA->M.dim2);
  bzero(normtab2, sizeof(COEF)*DA->M.dim2);
#else
  normtab2 = normtab;
#endif
  BL = &DBL->LHID;
  proc_id = DBL->proc_id;

  /***** IMPORTANT REMARK *****/
  /** We cannot use loc_ctrb and out_ctrb because it depends 
      of the number of contributions by rows that can be different from those
      by rows  ***/

  /*commvec = &(DA->commvec);*/
  commvec = (PhidalCommVec *)malloc(sizeof(PhidalCommVec));
  PhidalCommVec_Init(commvec);
  PhidalCommVec_Setup(3, DA, commvec, DBL);

  A = &(DA->M);
  bind = BL->block_index;


#ifdef DEBUG_M
  assert(commvec->init == 1);

  assert(A->tli == A->tlj); /** comvec pas bon sinon **/
  assert(A->bri == A->brj);
#endif
  
  /*******************************************/
  /** Start the persistent receive requests **/
  /*******************************************/
  for(j=A->brj;j>=A->tlj;j--)
    {
      if(commvec->out_ctrb[j-A->tlj] > 0)
	if(MPI_Startall(commvec->out_ctrb[j-A->tlj], commvec->recv_rqtab[j-A->tlj]) != MPI_SUCCESS)
	  {
	    fprintfd(stderr, "Error in start recv contrib for vec %d \n", j);
	    assert(0);
	  }
      
      if(commvec->out_ctrb[j-A->tlj] == -1)
	if(MPI_Start(commvec->recv_rqtab[j-A->tlj]) != MPI_SUCCESS)
	  {
	    fprintfd(stderr, "Error in start recv vector for vec %d \n", j);
	    assert(0);
	  }
    }

  /** Initialize the contribution counters **/
  memcpy(commvec->loc_cnt, commvec->loc_ctrb, sizeof(int)*(A->brj-A->tlj+1));
  bzero(normtab, sizeof(REAL)*A->dim2);


  for(j=A->brj;j>=A->tlj;j--)
    {
      normptr = normtab + bind[j] - bind[A->tlj];
      normptr2 = normtab2 + bind[j] - bind[A->tlj];

      for(k=A->cia[j];k<A->cia[j+1];k++)
	if(DA->clead[k] == proc_id &&  A->ca[k]->nnzr >0)
	  {
	    if(A->csc == 0)
	      CSRcolnorm2(A->ca[k], normptr);
	    else
	      CSRrownorm2(A->ca[k], normptr);

#ifdef TYPE_COMPLEX
	    { 
	      dim_t i;
	      for(i=0;i<bind[j+1]-bind[j];i++)
		normptr2[i] = (COEF)normptr[i];
	    }
#endif
	    
#ifdef DEBUG_M
	    assert(commvec->loc_cnt[j-A->tlj]>0);
#endif
	    if(--commvec->loc_cnt[j-A->tlj] == 0 && DBL->row_leader[j] != proc_id)
	      {
		/**** Send Ctrb i ***/
		MPI_Send(normptr2, CC(bind[j+1]-bind[j]), MPI_COEF_TYPE, DBL->row_leader[j], DBL->loc2glob_blocknum[j], DBL->mpicom); 
		break;
	      }


	  }

      if(A->symmetric == 1)
	{
	  /** Treat the symmetric part **/
#ifdef DEBUG_M
	  assert(A->csc == 1);
	  assert(A->rja[A->ria[j+1]-1] == j);
#endif
	  for(k=A->ria[j];k<A->ria[j+1]-1;k++)
	    if(DA->rlead[k] == proc_id && A->ra[k]->nnzr > 0)
	      {
		if(A->csc == 0)
		  CSRrownorm2(A->ra[k], normptr);
		else
		  CSRcolnorm2(A->ra[k], normptr);
		
#ifdef TYPE_COMPLEX
		{ 
		  dim_t i;
		  for(i=0;i<bind[j+1]-bind[j];i++)
		    normptr2[j] = normptr[i];
		}
#endif
#ifdef DEBUG_M
		assert(commvec->loc_cnt[j-A->tlj]>0);
#endif
		
		if(--commvec->loc_cnt[j-A->tlj] == 0 && DBL->row_leader[j] != proc_id) 
		  {
		    MPI_Send(normptr2, CC(bind[j+1]-bind[j]), MPI_COEF_TYPE, DBL->row_leader[j], DBL->loc2glob_blocknum[j], DBL->mpicom);
		    break;
		  }
		
	      }
	  
	  if(DA->rlead[A->ria[j+1]-1] == proc_id && A->ra[A->ria[j+1]-1]->nnzr > 0)
	    {
	      if(A->csc == 0)
		CSR_NoDiag_rownorm2(A->ra[A->ria[j+1]-1], normptr);
	      else
		CSR_NoDiag_colnorm2(A->ra[A->ria[j+1]-1], normptr);

#ifdef TYPE_COMPLEX
	      {
		dim_t i;
		for(i=0;i<bind[j+1]-bind[j];i++)
		  normptr2[i] = normptr[i];
	      }
#endif
	      
#ifdef DEBUG_M
	      assert(commvec->loc_cnt[j-A->tlj]>0);
#endif
	      if(--commvec->loc_cnt[j-A->tlj] == 0 && DBL->row_leader[j] != proc_id) 
		{
		  MPI_Send(normptr2, CC(bind[j+1]-bind[j]), MPI_COEF_TYPE, DBL->row_leader[j], DBL->loc2glob_blocknum[j], DBL->mpicom);
		  break;
		}
	    }
	}

    }
    

  /*** Receive the contributions where the processor is the owner of the facing diagonal block ***/
  for(j=A->brj;j>=A->tlj;j--)
    {

      normptr = normtab + bind[j] - bind[A->tlj];
      normptr2 = normtab2 + bind[j] - bind[A->tlj];

      if(commvec->out_ctrb[ j-A->tlj ] > 0)
	{
	  PhidalCommVec_ReceiveVecAdd(1, j-A->tlj, normptr2, commvec); 
	}
      
      if(DBL->row_leader[j] == proc_id)
	{

	  /** square root **/
	  for(k=0;k<bind[j+1]-bind[j];k++)
	    normptr2[k] = coefsqrt(normptr2[k]);
	  
	  /** Send the vector to row non-leader processor **/
	  if(DBL->block_psetindex[j+1]-DBL->block_psetindex[j]>1)
	    for(k=DBL->block_psetindex[j];k<DBL->block_psetindex[j+1];k++)
	      if(DBL->block_pset[k] != DBL->proc_id)
		MPI_Send(normptr2, CC(bind[j+1]-bind[j]), MPI_COEF_TYPE, DBL->block_pset[k],DBL->loc2glob_blocknum[j], DBL->mpicom);
	  
	}
    }
  
  /*** Receive the vector y parts where the processor is not the
       row_leader **/
  for(j=A->brj;j>=A->tlj;j--)
    if(commvec->out_ctrb[ j-A->tlj ] == -1)
      {
	if(MPI_Wait(commvec->recv_rqtab[j-A->tlj], commvec->status) != MPI_SUCCESS)
	  {
	    fprintfd(stderr, "Error in PhidalDistrMatrix_ColNorm for vector %d \n" ,j);
	    assert(0);
	  }
	normptr2 = normtab2 + bind[j] - bind[A->tlj];
	memcpy(normptr2, commvec->t + bind[j]- bind[A->tlj], sizeof(COEF)*(bind[j+1]-bind[j]));
      }

#ifdef TYPE_COMPLEX
  {
    dim_t i;
    for(i=0;i<DA->M.dim1;i++)
      normtab[i] = CREAL(normtab2[i]);
    free(normtab2);
  }
#endif

  PhidalCommVec_Clean(commvec);
  free(commvec);
  
}

/* @authors P. HENON */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>

#define PARALLEL

#include "phidal_parallel.h"

#define COMM_MEMRATIO 1.0

/*#define TRACE_COM*/


void PhidalFactComm_Setup(PhidalFactComm *FC, PhidalDistrMatrix *DM, PhidalDistrHID *DBL)
{
  dim_t i, j, k, m;
  int ind;
  BlockComm *bc;
  int nbcoli, nbcolj;
  int maxnbcol;
  PhidalHID *BL;
  PhidalMatrix *M;

  PhidalFactComm_Init(FC);
  
  BL = &(DBL->LHID);
  M = &(DM->M);
  
  FC->comm_memratio = COMM_MEMRATIO;

  FC->proc_id = DM->proc_id;
  /* printf("-->%d\n", DM->sharenbr); */
  FC->bcomnbr = DM->sharenbr;

  FC->mpicom = DBL->mpicom;

  if (DM->sharenbr > 0) 
    FC->bcomtab = (BlockComm *)malloc(sizeof(BlockComm) * DM->sharenbr);
  else 
    FC->bcomtab = NULL;

  FC->maxproc = 1; /** Need at least one buffer in cstab **/ 
  maxnbcol = 1;

  for(i=M->tlj; i<= M->brj; i++)
    for(k=M->cia[i];k<M->cia[i+1];k++)
      {
	ind = DM->cind[k];
	nbcolj =  BL->block_index[i+1]-BL->block_index[i];
	if(nbcolj> maxnbcol)
	  maxnbcol = nbcolj;

	if(ind >= 0)
	  {
	    bc = FC->bcomtab+ind;
	    if(DM->clead[k] == DM->proc_id)
	      {
#ifdef DEBUG_M
		assert(DM->pset_index[ind+1]-DM->pset_index[ind] > 1);
#endif
		BlockComm_Init(bc, DM->pset_index[ind+1]-DM->pset_index[ind] - 1);
		m = 0;
		for(j=DM->pset_index[ind];j<DM->pset_index[ind+1];j++)
		  if(DM->pset[j] != DM->proc_id)
		    bc->proctab[m++] = DM->pset[j];
		if(m  > FC->maxproc)
		  FC->maxproc = m;
	      }
	    else
	      {
		BlockComm_Init(bc, 1);
		bc->proctab[0] = DM->clead[k];
	      }
	    nbcoli = BL->block_index[M->cja[k]+1]-BL->block_index[M->cja[k]];
	    if(nbcoli > maxnbcol)
	      maxnbcol = nbcoli;
	    bc->buffsize = buffer_size((int)CSnnz(M->ca[k]),nbcoli, nbcolj, FC->comm_memratio);
	    /*fprintfv(5, stderr, "Proc %d block (%d %d) dim %d %d size of buffer = %d \n", DM->proc_id, M->cja[k], i,
	      nbcoli, nbcolj, bc->buffsize);*/

	  }
      }
  
  if(FC->maxproc >0)
    {
      FC->cstab = (csptr)malloc(sizeof(struct SparRow)*FC->maxproc);
      FC->status = (MPI_Status *)malloc(sizeof(MPI_Status)*FC->maxproc);
    }

  
  
  for(k=0;k<FC->maxproc;k++)
    initCS(FC->cstab+k, maxnbcol);

  if(maxnbcol > 0)
    {
      FC->jrev = (dim_t *)malloc(sizeof(dim_t)*maxnbcol);
      for(i=0;i<maxnbcol;i++)
	FC->jrev[i] = -1;
      FC->tmpj = (dim_t *)malloc(sizeof(dim_t)*maxnbcol);
      FC->tmpa = (COEF *)malloc(sizeof(COEF)*maxnbcol);
    }



}

void PhidalFactComm_PosteCtrbReceiveLU(PhidalFactComm *FCL, PhidalFactComm *FCU, 
				       PhidalDistrMatrix *DL, PhidalDistrMatrix *DU,
				       dim_t tli, dim_t bri)
{
  int i, /* j, */ k, ind;
/*   BlockComm *bc; */
  PhidalMatrix *L, *U;
  L = &(DL->M);
  U = &(DU->M);

#ifdef DEBUG_M
  assert(L->tli <= tli);
  assert(L->bri >= bri);
  assert(L->tli == L->tlj);
  assert(U->tli == U->tlj);
  assert(L->bri == L->brj);
  assert(U->bri == U->brj);
#endif

  /*fprintfd(stderr, "COMML : buf %ld posted %ld \n", FCL->bufmem, FCL->posted);
    fprintfd(stderr, "COMMU : buf %ld posted %ld \n", FCU->bufmem, FCU->posted);*/

  for(i=tli;i<=bri;i++)
    {
      for(k=U->ria[i];k<U->ria[i+1];k++)
	{
	  ind = DU->rind[k];
	  if(ind >= 0 && DU->rlead[k] == DU->proc_id)
	    {
#ifdef TRACE_COM
	      fprintfd(stderr, "Proc %d poste ctrb receive for block U %d %d \n", DU->proc_id, i, U->rja[k]);
#endif
	      poste_block_receive(ind, CTRB_TAG_U, FCU);
	    }
	}

      for(k=L->cia[i];k<L->cia[i+1];k++)
	{
	  if(L->cja[k] == i)
	    continue;
	  
	  ind = DL->cind[k];
	  if(ind >= 0 && DL->clead[k] == DL->proc_id)
	    {
#ifdef TRACE_COM
	      fprintfd(stderr, "Proc %d poste ctrb receive for block L %d %d \n", DL->proc_id, L->cja[k],i);
#endif
	      poste_block_receive(ind, CTRB_TAG_L, FCL);
	    }
	}
    }
  /*fprintfd(stderr, "COMML : buf %ld posted %ld \n", FCL->bufmem, FCL->posted);
    fprintfd(stderr, "COMMU : buf %ld posted %ld \n", FCU->bufmem, FCU->posted);*/

}



void PhidalFactComm_PosteCtrbReceiveLUMem(PhidalFactComm *FCL, PhidalFactComm *FCU, 
				       PhidalDistrMatrix *DL, PhidalDistrMatrix *DU)
{
  INTL i, /* j, */ k, ind;
/*   BlockComm *bc; */
  PhidalMatrix *L, *U;
  INTL rowstart;

  L = &(DL->M);
  U = &(DU->M);

  if(FCL->colind == -1)
    return;

#ifdef DEBUG_M
  assert(FCL->colind == FCU->colind);
#endif

  if(FCL->colind < DL->M.tlj)
    FCL->colind = DL->M.tlj;
  if(FCU->colind < DU->M.tlj)
    FCU->colind = DU->M.tlj;


  fprintfd(stderr, "COMML : colind %ld k %ld \n", (long)FCL->colind, (long)FCL->rowind);
  fprintfd(stderr, "COMML : buf %ld posted %ld \n", (long)FCL->bufmem, (long)FCL->posted);
  fprintfd(stderr, "COMMU : colind %ld k %ld \n", (long)FCU->colind, (long)FCU->rowind);
  fprintfd(stderr, "COMMU : buf %ld posted %ld \n", (long)FCU->bufmem, (long)FCU->posted);
  for(i=FCU->colind;i<=DL->M.brj;i++)
    {
      if(i == FCU->colind)
	rowstart = MAX(FCU->rowind, U->ria[i]) ;
      else
	rowstart = U->ria[i];


      for(k=rowstart;k<U->ria[i+1];k++)
	{
	  ind = DU->rind[k];
	  if(ind >= 0 && DU->rlead[k] == DU->proc_id)
	    {
	      if(FCU->bufmem >= FCU->bufmem_max || FCU->posted >= FCU->posted_max )
		{
		  FCU->colind = i;  
		  FCU->rowind = k;
		  goto comml;
		}

#ifdef TRACE_COM
	      fprintfd(stderr, "Proc %d poste ctrb receive for block U %d %d \n", DU->proc_id, i, U->rja[k]);
#endif
	      poste_block_receive(ind, CTRB_TAG_U, FCU);
	    }
	}
      FCU->rowind = 0;

    comml:
      if(i == FCL->colind)
	rowstart = MAX(FCL->rowind, L->cia[i]) ;
      else
	rowstart = L->cia[i];


      for(k=rowstart;k<L->cia[i+1];k++)
	{
	  if(L->cja[k] == i)
	    continue;
	  
	  ind = DL->cind[k];
	  if(ind >= 0 && DL->clead[k] == DL->proc_id)
	    {

	      if(FCL->bufmem >= FCL->bufmem_max || FCL->posted >= FCL->posted_max )
		{
		  FCL->colind = i;  
		  FCL->rowind = k;
		  fprintfd(stderr, "RETURN COMML : colind %d k %d \n", FCL->colind, FCL->rowind);
		  fprintfd(stderr, "RETURN COMMU : colind %d k %d \n", FCU->colind, FCU->rowind);
  

		  return;;
		}
#ifdef TRACE_COM
	      fprintfd(stderr, "Proc %d poste ctrb receive for block L %d %d \n", DL->proc_id, L->cja[k],i);
#endif     
	      poste_block_receive(ind, CTRB_TAG_L, FCL);
	     
	    }
	}
      FCL->rowind = 0;
    }
  fprintfd(stderr, "END COMML : colind %d k %d \n", FCL->colind, FCL->rowind);
  fprintfd(stderr, "END COMMU : colind %d k %d \n", FCU->colind, FCU->rowind);
  

  FCL->colind = -1;  
  FCL->rowind = -1;
  FCU->colind = -1;  
  FCU->rowind = -1;

}

void PhidalFactComm_PosteCtrbReceive(PhidalFactComm *FC, PhidalDistrMatrix *DM, dim_t tlj, dim_t brj)
{
  INTL /* i, */ j, k, ind;
  /*   BlockComm *bc; */
  PhidalMatrix *M;
  M = &(DM->M);

#ifdef DEBUG_M
  /* assert(M->tli <= tli); */
  /* assert(M->bri >= bri); */
  assert(M->tlj <= tlj);
  assert(M->brj >= brj);
#endif
  for(j=tlj;j<=brj;j++)
    {
      for(k=M->cia[j];k<M->cia[j+1];k++)
	{
	  ind = DM->cind[k];
	  if(ind >= 0 && DM->clead[k] == DM->proc_id)
	    poste_block_receive(ind, CTRB_TAG, FC);
	  
	}
    }
}


void PhidalFactComm_PosteCtrbReceiveMem(PhidalFactComm *FC, PhidalDistrMatrix *DM)
{
  INTL /* i, */ j, k=-1, ind;
  PhidalMatrix *M;
  INTL rowstart;

  M = &(DM->M);
  if(FC->colind < DM->M.tlj)
    FC->colind = DM->M.tlj;
  

  for(j=FC->colind;j<=DM->M.brj;j++)
    {
      if(j == FC->colind)
	rowstart = MAX(FC->rowind, M->cia[j]) ;
      else
	rowstart = M->cia[j];

      for(k=rowstart;k<M->cia[j+1];k++)
	{
	  ind = DM->cind[k];
	  if(ind >= 0 && DM->clead[k] == DM->proc_id)
	    poste_block_receive(ind, CTRB_TAG, FC);
	  if(FC->bufmem >= FC->bufmem_max || FC->posted >= FC->posted_max )
	    break;
	}
      
    }

  FC->colind = j;  
  FC->rowind = k;
}

void poste_block_receive(int indnum, mpi_t tag, PhidalFactComm *FC)
{
  dim_t k;
  BlockComm *bc;
#ifdef DEBUG_M
  assert(indnum >= 0);
#endif

  bc = FC->bcomtab+indnum;
  /*fprintfv(5, stderr, "indnum %d rqnbr %d \n", indnum, bc->rqnbr);*/
  
  if(bc->posted == 1) { /** The messages are already posted **/
    fprintfd(stderr, "Proc %d : poste_block_receive : receive already posted\n", FC->proc_id);
    return;
  }

  for(k=0;k<bc->rqnbr;k++)
    {
#ifdef DEBUG_M
      assert(bc->buffsize>0);
#endif
      
      bc->ibufftab[k] = (dim_t *)malloc(sizeof(dim_t)*(bc->buffsize));
      FC->bufmem += sizeof(dim_t)*bc->buffsize;

      bc->cbufftab[k] = (COEF *)malloc(sizeof(COEF)*(bc->buffsize));
      FC->bufmem += sizeof(COEF)*bc->buffsize;

      assert( bc->ibufftab[k] != NULL);
      assert( bc->cbufftab[k] != NULL);
#ifdef TRACE_COM
      fprintfd(stderr, "Proc %d : poste_block_receive from proc %d\n", FC->proc_id, bc->proctab[k]);
#endif
      /** Poste the Irecv for indice and coeff **/
      if(MPI_Irecv( bc->ibufftab[k], EE(bc->buffsize), COMM_INT, bc->proctab[k], 
		   tag, FC->mpicom, bc->irqtab+k) != MPI_SUCCESS)
	{
	  fprintfd(stderr, "Proc %d : Error in poste_block_receive \n", FC->proc_id);
	  MPI_Abort(FC->mpicom, -1);
	}
      FC->posted++;


      if(MPI_Irecv( bc->cbufftab[k], CC(bc->buffsize), MPI_COEF_TYPE, bc->proctab[k], 
		    tag, FC->mpicom, bc->crqtab+k) != MPI_SUCCESS)
	{
	  fprintfd(stderr, "Proc %d : Error in poste_block_receive \n", FC->proc_id);
	  MPI_Abort(FC->mpicom, -1);
	}
      FC->posted++;

    }
  bc->posted = 1;

  /*fprintfd(stderr, "Poste_block_receive : buf %ld posted %ld \n", FC->bufmem, FC->posted);*/



  
}


int receive_contrib(flag_t mode, csptr mat, int indnum, PhidalFactComm *FC)
{
  /*---------------------------------------/
  / Receive contributions for submatrix mat/
  / identified by indnum                   /
  / and add them to these blocks           /
  /---------------------------------------*/
  dim_t i;
  BlockComm *bc;


  bc = FC->bcomtab+indnum;

 

  if(mode == ASYNCHRONOUS)
    {
      int flag;
      MPI_Testall(bc->rqnbr, bc->irqtab, &flag, FC->status);
      if(flag != TRUE)
	return NOT_READY;
      MPI_Testall(bc->rqnbr, bc->crqtab, &flag, FC->status);
      if(flag != TRUE)
	return NOT_READY;
    }

  /** RECEIVE INDICE **/
#ifdef TRACE_COM
  fprintfd(stderr, "Proc %d receive contribution \n", FC->proc_id);
#endif

  if(MPI_Waitall(bc->rqnbr, bc->irqtab, FC->status)
     != MPI_SUCCESS)
    {
      fprintfd(stderr, "Proc %d, Error in receive_ctrb for block indnum = %d \n", FC->proc_id, indnum);
      MPI_Abort(FC->mpicom, -1);
    }

#ifdef TRACE_COM
  fprintfd(stderr, "DONE-->Proc %d receive contribution \n", FC->proc_id);
#endif

  /** RECEIVE COEFF **/
  if(MPI_Waitall(bc->rqnbr, bc->crqtab, FC->status) 
     != MPI_SUCCESS)
    {
      fprintfd(stderr, "Proc %d, Error in receive_ctrb for block indnum = %d \n", FC->proc_id, indnum);
      MPI_Abort(FC->mpicom, -1);
    }

  /** ADD CONTRIBUTIONS **/
  add_contrib(mat, bc, FC->cstab, FC->tmpj, FC->tmpa, FC->jrev);

  /** Free the receive buffers **/
  for(i=0;i<bc->rqnbr;i++)
    if(bc->ibufftab[i] != NULL)
      {
	free(bc->ibufftab[i]);
	bc->ibufftab[i] = NULL;
	FC->bufmem -= sizeof(dim_t)*bc->buffsize;
	FC->posted--;
      }
  for(i=0;i<bc->rqnbr;i++)
    if(bc->cbufftab[i] != NULL)
      {
	free(bc->cbufftab[i]);
	bc->cbufftab[i] = NULL;
	FC->bufmem -= sizeof(COEF)*bc->buffsize;
	FC->posted--;
      }

  bc->posted = 0;
  return READY;
}



int receive_gather(flag_t mode, csptr mat, int indnum, PhidalFactComm *FC)
{
  /*---------------------------------------/
  / Receive contributions for submatrix mat/
  / identified by indnum                   /
  / and add them to these blocks           /
  /---------------------------------------*/
  dim_t i;
  BlockComm *bc;


  bc = FC->bcomtab+indnum;

  if(mode == ASYNCHRONOUS)
    {
      int flag;
      MPI_Testall(bc->rqnbr, bc->irqtab, &flag, FC->status);
      if(flag != TRUE)
	return NOT_READY;
      MPI_Testall(bc->rqnbr, bc->crqtab, &flag, FC->status);
      if(flag != TRUE)
	return NOT_READY;
    }

  /** RECEIVE INDICE **/
  /*fprintfd(stderr, "Proc %d : wait receive IND from %d procs\n", FC->proc_id, bc->rqnbr);*/
  if(MPI_Waitall(bc->rqnbr, bc->irqtab, FC->status)
     != MPI_SUCCESS)
    {
      fprintfd(stderr, "Proc %d, Error in receive_ctrb for block indnum = %d \n", FC->proc_id, indnum);
      MPI_Abort(FC->mpicom, -1);
    }
 
  /** RECEIVE COEFF **/
  /*fprintfd(stderr, "Proc %d : wait receive COEFF from %d procs\n", FC->proc_id, bc->rqnbr);*/
  if(MPI_Waitall(bc->rqnbr, bc->crqtab, FC->status) 
     != MPI_SUCCESS)
    {
      fprintfd(stderr, "Proc %d, Error in receive_ctrb for block indnum = %d \n", FC->proc_id, indnum);
      MPI_Abort(FC->mpicom, -1);
    }

  /** ADD CONTRIBUTIONS **/
  gather_contrib(mat, bc, FC->cstab, FC->tmpj, FC->tmpa, FC->jrev);

  /** Free the receive buffers **/
  for(i=0;i<bc->rqnbr;i++)
    if(bc->ibufftab[i] != NULL)
      {
	free(bc->ibufftab[i]);
	bc->ibufftab[i] = NULL;
	FC->bufmem -= sizeof(dim_t)*bc->buffsize;
	FC->posted--;
      }
  for(i=0;i<bc->rqnbr;i++)
    if(bc->cbufftab[i] != NULL)
      {
	free(bc->cbufftab[i]);
	bc->cbufftab[i] = NULL;
	FC->bufmem -= sizeof(COEF)*bc->buffsize;
	FC->posted--;
      }

  bc->posted = 0;
  return READY;
}





int receive_matrix(flag_t mode, csptr mat, int indnum, PhidalFactComm *FC)
{
  /***** ALWAYS USE SYNCHRONOUS MODE BECAUSE THERE IS NO TAG ON
	 THE MESSAGE ******/

  /*-------------------------------------------/
  / Receive a SparRow matrix from the leader   /
  / of this matrix                             /
  /-------------------------------------------*/
  BlockComm *bc;


  bc = FC->bcomtab+indnum;

#ifdef DEBUG_M
  assert(bc->rqnbr == 1);
#endif

  if(mode == ASYNCHRONOUS)
    {
      int flag;
      MPI_Test(bc->irqtab, &flag, FC->status);
      if(flag != TRUE)
	return NOT_READY;
      MPI_Test(bc->crqtab, &flag, FC->status);
      if(flag != TRUE)
	return NOT_READY;
    }
#ifdef TRACE_COM
  fprintfd(stderr, "Proc %d receive matrix \n", FC->proc_id);
#endif
  /** RECEIVE INDICE **/
  if(MPI_Wait(bc->irqtab, FC->status)
     != MPI_SUCCESS)
    {
      fprintfd(stderr, "Proc %d, Error in receive_matrix for block indnum = %d \n", FC->proc_id, indnum);
      MPI_Abort(FC->mpicom, -1);
    }
 
  /** RECEIVE COEFF **/
  if(MPI_Wait(bc->crqtab, FC->status) 
     != MPI_SUCCESS)
    {
      fprintfd(stderr, "Proc %d, Error in receive_matrix for block indnum = %d \n", FC->proc_id, indnum);
      MPI_Abort(FC->mpicom, -1);
    }
#ifdef TRACE_COM
  fprintfd(stderr, "DONE-->Proc %d receive matrix \n", FC->proc_id);
#endif
  /** UNPACK THE MATRIX IN mat **/
  unpack_matrix(FC->cstab, bc->ibufftab[0], bc->cbufftab[0]);
  CS_SetNonZeroRow(FC->cstab);
  CS_Copy(FC->cstab, mat);
  /*CS_SetNonZeroRow(mat);*/



  /** Free the receive buffers **/
  if(bc->ibufftab[0] != NULL)
    {
      free(bc->ibufftab[0]);
      bc->ibufftab[0] = NULL;
      FC->bufmem -= sizeof(dim_t)*bc->buffsize;
      FC->posted--;
    }
  if(bc->cbufftab[0] != NULL)
    {
      free(bc->cbufftab[0]);
      bc->cbufftab[0] = NULL;
      FC->bufmem -= sizeof(COEF)*bc->buffsize;
      FC->posted--;
    }

  bc->posted = 0;
  return READY;
}


void send_diagonal(COEF *diag,  int size, int indnum, PhidalFactComm *FC)
{
  dim_t k;
  BlockComm *bc;
  bc = FC->bcomtab+indnum;
  for(k=0;k<bc->rqnbr;k++)
    MPI_Send(diag, CC(size), MPI_COEF_TYPE, bc->proctab[k], DIAG_TAG, FC->mpicom);
}

void send_matrix(csptr mat, int indnum, mpi_t tag, PhidalFactComm *FC)
{
  dim_t k;
  BlockComm *bc;
  dim_t *index_buffptr = NULL;
  COEF *coeff_buffptr = NULL;
  int_t index_size_packed, coeff_size_packed;
  

  bc = FC->bcomtab+indnum;
  

  
  index_buffptr = (dim_t *)malloc(sizeof(dim_t)*bc->buffsize);
  assert(index_buffptr != NULL);

  coeff_buffptr = (COEF *)malloc(sizeof(COEF)*(bc->buffsize+1)); /** +1 : see pack_matrix function **/
  assert(coeff_buffptr != NULL);

  /** Pack contrib of block (i,j) into buffers **/
  index_size_packed = 0;
  coeff_size_packed = 0;

  if(pack_matrix(mat, index_buffptr, coeff_buffptr, &index_size_packed, &coeff_size_packed, bc->buffsize)!=0)
    {
      fprintferr(stderr, "Error in pack_matrix: unsufficient memory allocated for buffer \n");
      fprintferr(stderr, "Number of nnz in matrix to pack = %ld Dim = %ld \n", (long)CSnnz(mat), (long)mat->n);
      fprintferr(stderr, "buffer size in NNZ = %ld, try a higher value for comm_memratio \n", (long)bc->buffsize);
      MPI_Abort(FC->mpicom, -1);
    } 

#ifdef DEBUG_M
  assert(coeff_size_packed < bc->buffsize);
#endif
  
  /** The dest. of the message is the same set of processor 
      than the one from which the local processor waits for message **/
  for(k=0;k<bc->rqnbr;k++)
    {
#ifdef TRACE_COM
      fprintfd(stderr, "Proc %ld send matrix to proc %ld \n", (long)FC->proc_id, (long)bc->proctab[k]);
#endif
      /** Send indice buffer **/
      MPI_Send(index_buffptr, EE(index_size_packed), COMM_INT, bc->proctab[k], tag, FC->mpicom);
      
      /** Send coeff buffer **/
      MPI_Send(coeff_buffptr, CC(coeff_size_packed), MPI_COEF_TYPE, bc->proctab[k], tag, FC->mpicom);
#ifdef TRACE_COM
      fprintfd(stderr, "DONE--- Proc %d send matrix to proc %d \n", FC->proc_id, bc->proctab[k]);
#endif
    }


  free(index_buffptr);
  free(coeff_buffptr);

}



int pack_matrix(csptr mat, dim_t *index_buff, COEF *coeff_buff, int_t *index_size_packed, int_t *coeff_size_packed, int_t limitsize)
{
  dim_t i, k;
  int ind;

  /*** PACK THE COEFFICIENT INDICES **/
  index_buff[0] = 0;
  for(i=0;i<mat->n;i++)
    {
      index_buff[0] += mat->nnzrow[i];
#ifdef DEBUG_M
      assert(mat->nnzrow[i] >= 0);
#endif
    }
  
  index_buff[1] = mat->n;

  ind = 2;

  if(ind + mat->n > limitsize)
    return -1;

  for(i=0;i<mat->n;i++)
    {
      index_buff[ind] = mat->nnzrow[i];
      ind++;
    }

  /*for(i=0;i<mat->n;i++)
    {
      if( mat->nnzrow[i] > 0)
      {*/
  for(k=0;k<mat->nnzr;k++)
    {
      i = mat->nzrtab[k];
      
      if(ind + mat->nnzrow[i] > limitsize)
	return -1;
      
      memcpy(&(index_buff[ind]), mat->ja[i], mat->nnzrow[i]*sizeof(dim_t));
      ind += mat->nnzrow[i];
    }

  (*index_size_packed) = ind;

  /*** PACK THE COEFFICIENTS  ***/
  ind = 0;
  /*for(i=0;i<mat->n;i++)
    {
    if(mat->nnzrow[i]>0)*/
  for(k=0;k<mat->nnzr;k++)
    {
      i = mat->nzrtab[k];
      if(ind + mat->nnzrow[i] > limitsize)
	return -1;
      memcpy(coeff_buff+ind, mat->ma[i], mat->nnzrow[i]*sizeof(COEF));
      ind += mat->nnzrow[i];
    }

  coeff_buff[ind + 1] = 0.0;
  (*coeff_size_packed) = ind + 1; /** +1 is to avoid the case where the matrix is empty **/
  

#ifdef DEBUG_M
  if(ind != index_buff[0])
    {
      fprintfd(stderr, "ERROR IN PACK MATRIX ind = %d index_buff[0] = %d   index_buff[1] = %d mat->n = %d \n", ind, index_buff[0], index_buff[1], mat->n);
      exit(-1);
    }
#endif

  return 0;
}

void unpack_matrix(csptr amat, dim_t *index_buff, COEF *coeff_buff)
{
  /*-----------------------------------------------/
  /  Unpack the matrix stored in the buffer        /
  /  in a SparRow structure                        /
  /  WARNING : amat must have be initialized       /
  /   to a void sparse matrix with sufficent       /
  /     dimension                                  /
  /-----------------------------------------------*/

  dim_t i;
  int ind;
  
  /** UNPACK INDICES **/
  amat->n = index_buff[1];
  ind = 2;
  for(i=0;i<amat->n;i++)
    {
      amat->nnzrow[i] = index_buff[ind];
#ifdef DEBUG_M
      assert(amat->nnzrow[i]>=0);
#endif
      ind++;
    }
  
  for(i=0;i<amat->n;i++)
    {
      if( amat->nnzrow[i] > 0)
	{
	  amat->ja[i] = &(index_buff[ind]);
	  ind += amat->nnzrow[i];
	}
      else
	amat->ja[i] = NULL;
    }
  
  /** UNPACK COEFFICIENTS **/
  ind = 0;
  for(i=0;i<amat->n;i++)
    {
      if(amat->nnzrow[i]>0)
	{
	  amat->ma[i] = &(coeff_buff[ind]);
	  ind += amat->nnzrow[i];
	}
      else
	amat->ma[i] = NULL;
    }

#ifdef DEBUG_M
   if(ind != index_buff[0])
    {
      fprintfd(stderr, "ERROR IN UNPACK MATRIX ind = %d index_buff[0] = %d \n", ind, index_buff[0]);
      exit(-1);
    }
#endif

}


mpi_t buffer_size(int nnzB, int nbcoli, int nbcolj, REAL commratio)
{
  int buffsize;
  int nnz;


  /** @@ OIMBE a revoir : mieux vaut donner un fillratio par rapport a
      nnzA **/
  if(nbcoli*nbcolj > MIN_BUFF_SIZE)
    nnz = MIN(nbcoli*nbcolj*commratio, nbcoli*nbcolj);
  else
    nnz = MIN_BUFF_SIZE;

  buffsize = nnz + 2*MAX(nbcoli,nbcolj) + 6;
#ifdef DEBUG_M
  assert(buffsize > 0);
#endif
  return buffsize;
}



void add_contrib(csptr mat, BlockComm *bc, csptr cstab, int *tmpj, COEF *tmpa, int *jrev)
{
  int /* i, */ k;
  /*   csptr t; */

  if(mat->inarow != 0)
    CSunrealloc(mat);

#ifdef DEBUG_M
  assert(mat->inarow == 0);
  assert(bc->rqnbr>0);
#endif

  for(k=0;k<bc->rqnbr;k++)
    unpack_matrix(cstab+k, bc->ibufftab[k], bc->cbufftab[k]);
  
  /** Add the contributions in matrix mat **/
  CS_MatricesAdd(mat, bc->rqnbr, cstab, tmpj, tmpa, jrev);
}


void gather_contrib(csptr mat, BlockComm *bc, csptr cstab, int *tmpj, COEF *tmpa, int *jrev)
{
  dim_t k;
/*   csptr t; */
  if(mat->inarow != 0)
    CSunrealloc(mat);
  
#ifdef DEBUG_M
  assert(mat->inarow == 0);
  assert(bc->rqnbr>0);
#endif

  for(k=0;k<bc->rqnbr;k++)
    unpack_matrix(cstab+k, bc->ibufftab[k], bc->cbufftab[k]);
  
  /** Add the contributions in matrix mat **/
  CS_MatricesGather(mat, bc->rqnbr, cstab, tmpj, tmpa, jrev);

}


/******************************************/
/******************************************/
/***  communication routines for vector ***/
/***      (matrix-vector operations)    ***/
/******************************************/
/******************************************/



void PhidalCommVec_Setup(flag_t job, PhidalDistrMatrix *DM, PhidalCommVec *commvec, PhidalDistrHID *DBL)
{
  /***************************************************/
  /* This function sets up the commicator for matrix */
  /* vector operations                               */
  /* Depending on job the communicator enables :     */
  /* job = 0: matrix-vector multiplication           */
  /* job = 1: forward triangular solve               */
  /* job = 2: backward triangular solve              */
  /* job = 3: column comm Colnorm2                   */
  /***************************************************/

  int i, j, k, veclen, offseti;

  PhidalMatrix *M;
  PhidalHID *BL;
  BL = &DBL->LHID;
  /*PhidalCommVec *commvec;*/
  /*  commvec = &(DM->commvec);*/

  M = &(DM->M);

#ifdef DEBUG_M
  assert(commvec->init == 0);
#endif

  /*PhidalCommVec_Init(commvec);*/

  commvec->init = 1;
  commvec->recv_rqtab  = (MPI_Request **)malloc(sizeof(MPI_Request *) * (M->bri-M->tli+1));
  commvec->recv_buffer = (COEF **)     malloc(sizeof(COEF *) * (M->bri-M->tli+1) );

  commvec->out_ctrb = (int_t *)malloc(sizeof(int_t) * (M->bri-M->tli+1));
  commvec->out_proctab = (mpi_t **)malloc(sizeof(mpi_t *) * (M->bri-M->tli+1));

  commvec->veclen = (int_t *)malloc(sizeof(int_t) * (M->bri-M->tli+1));
  commvec->out_cnt = (int_t *)malloc(sizeof(int_t) * (M->bri-M->tli+1));
  commvec->loc_ctrb =   (int_t *)malloc(sizeof(int_t) * (M->bri-M->tli+1));
  commvec->loc_cnt =   (int_t *)malloc(sizeof(int_t) * (M->bri-M->tli+1));
  commvec->t = (COEF *)malloc(sizeof(COEF) * M->dim1);

  if(DBL->adjpnbr>0)
    commvec->status = (MPI_Status *)malloc(sizeof(MPI_Status)*(DBL->adjpnbr));
  else
    commvec->status = NULL;

  commvec->rqnbr = M->bri-M->tli+1;

  bzero(commvec->veclen, sizeof(int_t)*(M->bri-M->tli+1));

  switch(job){
  case 0:
    /***** Set the counters ****/
    if(M->symmetric == 0)
      {
	/** Count  the number of contributions in each row **/
	for(i=M->tli;i<= M->bri;i++)
	  {
	    commvec->loc_ctrb[i-M->tli] = 0;
	    for(j=M->ria[i];j<M->ria[i+1];j++)
#ifndef POUR_DEBUG
	      if(M->ra[j]->nnzr > 0 && DM->rlead[j] == DM->proc_id)
#else
		if(DM->rlead[j] == DM->proc_id)
#endif
		  commvec->loc_ctrb[i-M->tli]++;
	  }
      }
    else
      {
	/** Count  the number of contributions in each row **/
	for(i=M->tli;i<= M->bri;i++)
	  {
	    commvec->loc_ctrb[i-M->tli] = 0;
	    for(j=M->ria[i];j<M->ria[i+1];j++)
#ifndef POUR_DEBUG
	      if(M->ra[j]->nnzr > 0 && DM->rlead[j] == DM->proc_id)
#else
	      if(DM->rlead[j] == DM->proc_id)
#endif
		commvec->loc_ctrb[i-M->tli]++;
	    
	    /** Symmetric part **/
	    for(j=M->cia[i];j<M->cia[i+1];j++)
#ifndef POUR_DEBUG
	      if(M->ca[j]->nnzr > 0 && DM->clead[j] == DM->proc_id)
#else
	      if(DM->clead[j] == DM->proc_id)
#endif
		commvec->loc_ctrb[i-M->tli]++;
	  }
      }

    /***********************************************************************/
    /* out_ctrb est modifie plus bas pour faire une compte plus precis des */
    /* contributions reels en ne tenant pas compte des blocs nulles        */
    /* ce bout de code ne sert donc pas si le reste est active             */
    /***********************************************************************/
    for(i=M->tli;i<= M->bri;i++)
      {
	if(DBL->row_leader[i] == DM->proc_id)
	  commvec->out_ctrb[i-M->tli] = DBL->block_psetindex[i+1] - DBL->block_psetindex[i]-1;
	else
	  commvec->out_ctrb[i-M->tli] = -1;
      }

    break;

  case 1: 
  
  case 2:

    /** Count  the number of contributions in each row **/
    for(i=M->tli;i<= M->bri;i++)
      {
	commvec->loc_ctrb[i-M->tli] = 0;
	for(j=M->ria[i];j<M->ria[i+1];j++)
#ifndef POUR_DEBUG
	  if(M->ra[j]->nnzr > 0 && DM->rlead[j] == DM->proc_id)
#else
	  if(DM->rlead[j] == DM->proc_id) 
#endif
	    commvec->loc_ctrb[i-M->tli]++;
      }
    
    break;

  case 3:
      /*** Compute the commvec setup for columns ***/
    /***** Set the counters ****/
    if(M->symmetric == 0)
      {
	/** Count  the number of contributions in each row **/
	for(j=M->tlj;j<= M->brj;j++)
	  {
	    commvec->loc_ctrb[j-M->tlj] = 0;
	    for(i=M->cia[j];i<M->cia[j+1];i++)
#ifndef POUR_DEBUG
	      if(M->ca[i]->nnzr > 0 && DM->clead[i] == DM->proc_id)
#else
		if(DM->clead[i] == DM->proc_id)
#endif
		  commvec->loc_ctrb[j-M->tlj]++;
	  }
      }
    else
      {
	/** Count  the number of contributions in each column **/
	for(j=M->tlj;j<= M->brj;j++)
	  {
	    commvec->loc_ctrb[j-M->tlj] = 0;
	    for(i=M->cia[j];i<M->cia[j+1];i++)
#ifndef POUR_DEBUG
	      if(M->ca[i]->nnzr > 0 && DM->clead[i] == DM->proc_id)
#else
	      if(DM->clead[i] == DM->proc_id)
#endif
		commvec->loc_ctrb[j-M->tlj]++;
	    
	    /** Symmetric part **/
	    for(i=M->ria[j];i<M->ria[j+1];i++)
#ifndef POUR_DEBUG
	      if(M->ra[i]->nnzr > 0 && DM->rlead[i] == DM->proc_id)
#else
	      if(DM->rlead[i] == DM->proc_id)
#endif
		commvec->loc_ctrb[j-M->tlj]++;
	  }
      }
    break;

  case 4:
    /***** Set the counters ****/
    if(M->symmetric == 0)
      {
	/** Count  the number of contributions in each row **/
	for(i=M->tli;i<= M->bri;i++)
	  {
	    commvec->loc_ctrb[i-M->tli] = 0;
	    for(j=M->ria[i];j<M->ria[i+1];j++)
		if(DM->rlead[j] == DM->proc_id)
		  commvec->loc_ctrb[i-M->tli]++;
	  }
      }
    else
      {
	/** Count  the number of contributions in each row **/
	for(i=M->tli;i<= M->bri;i++)
	  {
	    commvec->loc_ctrb[i-M->tli] = 0;
	    for(j=M->ria[i];j<M->ria[i+1];j++)
	      if(DM->rlead[j] == DM->proc_id)
		commvec->loc_ctrb[i-M->tli]++;
	    
	    /** Symmetric part **/
	    for(j=M->cia[i];j<M->cia[i+1];j++)
	      if(DM->clead[j] == DM->proc_id)
		commvec->loc_ctrb[i-M->tli]++;
	  }
      }

    /***********************************************************************/
    /* out_ctrb est modifie plus bas pour faire une compte plus precis des */
    /* contributions reels en ne tenant pas compte des blocs nulles        */
    /* ce bout de code ne sert donc pas si le reste est active             */
    /***********************************************************************/
    for(i=M->tli;i<= M->bri;i++)
      {
	if(DBL->row_leader[i] == DM->proc_id)
	  commvec->out_ctrb[i-M->tli] = DBL->block_psetindex[i+1] - DBL->block_psetindex[i]-1;
	else
	  commvec->out_ctrb[i-M->tli] = -1;
      }

    break;


  default:
    fprintfd(stderr, "Invalid Argument 'job' in  PhidalCommVec_Setup \n");
    exit(-1);

  }

  /**********************************************************/
  /** Set the loc_ctrb=0 to -1  in order to differentiate  **/
  /** them from the complete ctrb                          **/
  /**********************************************************/
  /** NO NEED OF THAT IN THEORY : BUT "on est jamais trop prudent...
      pour eviter des bugs" ***/
  for(i=M->tli;i<= M->bri;i++)
    if(commvec->loc_ctrb[i-M->tli]==0)
      commvec->loc_ctrb[i-M->tli] = -1;
  

  
  /***************************************************************/
  /** Compute the list of row where the local proc contributes  **/
  /** for each adjacent processor                               **/
  /** We also compute the exact number of contribution from     **/
  /** adjacent processors (we eliminate the false contributions **/
  /** induced by null blocks                                    **/
  /***************************************************************/


  /**** OIMBE A FAIRE : EVITER CE BLOCK POUR job == 4 ****/
  {
    dim_t *proc2adjp, *adjp_blocknbrLOC=NULL, *adjp_blocknbrEXT=NULL;
    dim_t **blistLOC=NULL, **blistEXT=NULL;
    MPI_Request *rqtab=NULL;
      
    proc2adjp = (dim_t *)malloc(sizeof(dim_t)*DBL->nproc);
    bzero(proc2adjp, sizeof(dim_t)*DBL->nproc);
    for(i=0;i<DBL->adjpnbr;i++)
      proc2adjp[ DBL->adjptab[i] ] = i;
    
    /*** Compute the list of connector owned by each adjacent processor ***/
    if(DBL->adjpnbr>0)
      adjp_blocknbrLOC = (dim_t *)malloc(sizeof(dim_t)*DBL->adjpnbr);
    bzero(adjp_blocknbrLOC, sizeof(dim_t)*DBL->adjpnbr);

    for(i=M->tli;i<= M->bri;i++)
      if(commvec->loc_ctrb[i-M->tli] > 0 && DBL->row_leader[i] != DBL->proc_id)
	adjp_blocknbrLOC[ proc2adjp[DBL->row_leader[i]]]++;
      
    if(DBL->adjpnbr>0)
      blistLOC = (dim_t **)malloc(sizeof(dim_t *)*DBL->adjpnbr);
    for(i=0;i<DBL->adjpnbr;i++)
      if(adjp_blocknbrLOC[i] > 0)
	blistLOC[i] = (dim_t *)malloc(sizeof(dim_t)*adjp_blocknbrLOC[i]);
      else
	blistLOC[i] = NULL;

    if(DBL->adjpnbr>0)
      bzero(adjp_blocknbrLOC, sizeof(dim_t)*DBL->adjpnbr);
    for(i=M->tli;i<= M->bri;i++)
      if(commvec->loc_ctrb[i-M->tli] > 0 && DBL->row_leader[i] != DBL->proc_id)
	{
	  k = proc2adjp[DBL->row_leader[i]];
	  blistLOC[k][adjp_blocknbrLOC[k]++] = DBL->loc2glob_blocknum[i];
	}
      
    /*fprintfd(stderr, "Proc %d Local block nbr: \n", DBL->proc_id);
    for(i=0;i<DBL->adjpnbr;i++)
    fprintfd(stderr, "to proc %d = %d \n", DBL->adjptab[i], adjp_blocknbrLOC[i]);*/

    if(DBL->adjpnbr>0)
      {
	adjp_blocknbrEXT = (dim_t *)malloc(sizeof(dim_t)*DBL->adjpnbr);
	memcpy(adjp_blocknbrEXT, adjp_blocknbrLOC, sizeof(dim_t)*DBL->adjpnbr);
      }

   
    for(i=0;i<DBL->adjpnbr;i++)
      {
#ifdef TRACE_COM
	fprintfd(stderr, "MPI_Sendrecv i= %ld proc %ld with proc %ld \n", (long)i, (long)DBL->proc_id, (long)DBL->adjptab[i]);
#endif
#ifdef DEBUG_M
	assert(DBL->proc_id !=  DBL->adjptab[i]);
	assert(commvec->status != NULL);
#endif
	MPI_Sendrecv_replace(adjp_blocknbrEXT+i, EE(1), COMM_INT, DBL->adjptab[i], CTRB_TAG, DBL->adjptab[i], CTRB_TAG, 
			     DBL->mpicom, commvec->status);
      }


    /*fprintfd(stderr, "Proc %d EXT block nbr: \n", DBL->proc_id);
    for(i=0;i<DBL->adjpnbr;i++)
    fprintfd(stderr, "from proc %d = %d \n", DBL->adjptab[i], adjp_blocknbrEXT[i]);*/
    
    if(DBL->adjpnbr>0)
      {
	blistEXT = (dim_t **)malloc(sizeof(dim_t *)*DBL->adjpnbr);
	rqtab = (MPI_Request *)malloc(sizeof(MPI_Request)*DBL->adjpnbr);
      }
    for(i=0;i<DBL->adjpnbr;i++)
      if(adjp_blocknbrEXT[i] > 0)
	{
	  blistEXT[i] = (dim_t *)malloc(sizeof(dim_t)*adjp_blocknbrEXT[i]);
	  assert(DBL->proc_id !=  DBL->adjptab[i]);
	  MPI_Irecv(blistEXT[i], EE(adjp_blocknbrEXT[i]), COMM_INT, DBL->adjptab[i], LEAD_TAG, DBL->mpicom, rqtab+i);
	}
      
    for(i=0;i<DBL->adjpnbr;i++)
      if(adjp_blocknbrLOC[i] > 0)
	{
	  assert(DBL->proc_id !=  DBL->adjptab[i]);
	  MPI_Send(blistLOC[i], EE(adjp_blocknbrLOC[i]), COMM_INT, DBL->adjptab[i], LEAD_TAG, DBL->mpicom);
	}
    /** Receive the list of contribution from adjacent processors **/
    /** and convert them in local numbering **/
    for(i=0;i<DBL->adjpnbr;i++)
      if(adjp_blocknbrEXT[i] > 0)
	{
	  assert(commvec->status!=NULL);
	  MPI_Wait(rqtab+i, commvec->status);
	  for(k=0;k<adjp_blocknbrEXT[i];k++)
	    blistEXT[i][k] = DBL->glob2loc_blocknum[blistEXT[i][k]];
	}


    /*** Compute the processor tab  ***/
    bzero(commvec->out_ctrb, sizeof(int_t)*(M->bri-M->tli+1));
    for(i=0;i<DBL->adjpnbr;i++)
      for(k=0;k<adjp_blocknbrEXT[i];k++)
	commvec->out_ctrb[blistEXT[i][k]-M->tli]++;
      
      
   
    for(i=0;i<M->bri-M->tli+1;i++)
      if(commvec->out_ctrb[i]>0)
	commvec->out_proctab[i] = (mpi_t *)malloc(sizeof(mpi_t)*commvec->out_ctrb[i]);

    bzero(commvec->out_ctrb, sizeof(int_t)*(M->bri-M->tli+1));
    for(i=0;i<DBL->adjpnbr;i++)
      for(k=0;k<adjp_blocknbrEXT[i];k++)
	{
	  j = blistEXT[i][k] - M->tli;
	  commvec->out_proctab[j][commvec->out_ctrb[j]++] = DBL->adjptab[i];
	}

    for(i=M->tli;i<= M->bri;i++)
      if(DBL->row_leader[i] != DM->proc_id)
	commvec->out_ctrb[i-M->tli] = -1;
      
    free(proc2adjp);
    for(i=0;i<DBL->adjpnbr;i++)
      if(adjp_blocknbrLOC[i]>0)
	free(blistLOC[i]);
    for(i=0;i<DBL->adjpnbr;i++)
      if(adjp_blocknbrEXT[i]>0)
	free(blistEXT[i]);
    if(DBL->adjpnbr>0)
      {
	free(blistLOC);
	free(blistEXT);
	free(adjp_blocknbrLOC);
	free(adjp_blocknbrEXT);
	free(rqtab);
      }

    /*-----------------------------*/
    /* Set persistent MPI requests */
    /*-----------------------------*/
    offseti = BL->block_index[M->tli];
    for(i=M->tli;i<= M->bri;i++)
      {
	veclen = DBL->LHID.block_index[i+1]-DBL->LHID.block_index[i];
	commvec->veclen[i-M->tli] = veclen;

	if(commvec->out_ctrb[i-M->tli] > 0)
	  {
	    /** Set the receive request for contribution vectors from
		non-leader processors **/
	    
	    commvec->recv_buffer[i-M->tli] = (COEF *)malloc(sizeof(COEF)*veclen*commvec->out_ctrb[i-M->tli]);
	    commvec->recv_rqtab[i-M->tli] = (MPI_Request *)malloc(sizeof(MPI_Request)*commvec->out_ctrb[i-M->tli]);
	    
	    for(j=0;j<commvec->out_ctrb[i-M->tli];j++)
	      {
		assert(commvec->out_proctab[i-M->tli][j] != DBL->proc_id);
		MPI_Recv_init(commvec->recv_buffer[i-M->tli]+j*veclen, CC(veclen), MPI_COEF_TYPE, commvec->out_proctab[i-M->tli][j], 
			      DBL->loc2glob_blocknum[i], DBL->mpicom, commvec->recv_rqtab[i-M->tli]+j);
	      }
	  }
	else
	  {
	    if(commvec->out_ctrb[i-M->tli] == -1)
	      {
#ifdef DEBUG_M
		assert(DBL->row_leader[i] != DM->proc_id);
#endif
		commvec->recv_rqtab[i-M->tli] = (MPI_Request *)malloc(sizeof(MPI_Request));

		/** Init the receive for the final vector value from
		    the leader **/
		MPI_Recv_init(commvec->t+BL->block_index[i]-offseti, CC(veclen), MPI_COEF_TYPE, DBL->row_leader[i], 
			      DBL->loc2glob_blocknum[i], DBL->mpicom, commvec->recv_rqtab[i-M->tli]);

	      }
	    else
	      commvec->recv_rqtab[i-M->tli] = NULL;
	  }
      }

  }
  
}

void PhidalCommVec_PrecSetup(PhidalDistrPrec *P, PhidalDistrHID *DBL)
{
#ifdef DEBUG_M
  if(P->forwardlev > 0)
    assert(P->nextprec != NULL);
  else
    assert(P->nextprec == NULL);
#endif

  if(P->forwardlev  > 0)
    {
#ifdef DEBUG_M
      assert(PREC_E(P) != NULL);
      assert(PREC_F(P) != NULL);
#endif
      /*if(P->levelnum>0)*/
	{
	  PhidalCommVec_Setup(0, PREC_E(P), &PREC_E(P)->commvec, DBL);
	  PhidalCommVec_Setup(0, PREC_F(P), &PREC_F(P)->commvec, DBL);
	}
      if(P->schur_method == 1)
	PhidalCommVec_Setup(0, PREC_S_SCAL(P),  &PREC_S_SCAL(P)->commvec, DBL);
      
      if(P->schur_method == 2)
	PhidalCommVec_Setup(0, PREC_B(P), &PREC_B(P)->commvec, DBL);
      
      if(P->nextprec != NULL)
	PhidalCommVec_PrecSetup(P->nextprec, DBL);
    }
#ifdef DEBUG_M
  assert(PREC_L_SCAL(P) != NULL);
  assert(PREC_U_SCAL(P) != NULL);
#endif 
  /*if(P->levelnum>0)*/
      {
	PhidalCommVec_Setup(1, PREC_L_SCAL(P), &PREC_L_SCAL(P)->commvec, DBL);
	PhidalCommVec_Setup(2, PREC_U_SCAL(P), &PREC_U_SCAL(P)->commvec, DBL); 
      }
}

void PhidalCommVec_ReceiveVecAdd(flag_t job, int num, COEF *y, PhidalCommVec *commvec)
{
  /***************************/
  /** job = 0 max (abs)    ***/
  /** job = 1 add          ***/
  /***** *********************/
  dim_t i, j;
  int veclen;
  COEF *ptr;
#ifdef DEBUG_M
  assert(commvec->init == 1);
  assert(commvec->out_ctrb[num]>0);
#endif

  if(MPI_Waitall(commvec->out_ctrb[num], commvec->recv_rqtab[num], commvec->status) != MPI_SUCCESS)
    {
      fprintfd(stderr, "Error in PhidalCommVec_ReceiveVecAdd for request %d \n" ,num);
      assert(0);
    }
  
  /** add the contribution in y **/
  ptr = commvec->recv_buffer[num];
  veclen = commvec->veclen[num];
#ifdef DEBUG_M
  assert(veclen > 0);
#endif
  if(job == 1)
    {
      for(i=0;i<commvec->out_ctrb[num];i++)
	{
	  for(j=0;j<veclen;j++)
	    y[j] += ptr[j];
	  ptr += veclen;
	}
    }
  else
    {
      /*memcpy(y, ptr, sizeof(COEF)*veclen);*/
      for(i=0;i<commvec->out_ctrb[num];i++)
	{
	  for(j=0;j<veclen;j++)
	    if(coefabs(ptr[j]) > coefabs(y[i]))
	      y[j] = ptr[j];
	  ptr += veclen;
	}
    }

}


COEF dist_ddot(mpi_t proc_id, COEF *x, COEF *y, dim_t tli, dim_t bri, PhidalDistrHID *DBL)
{
  /**********************************************************************************/
  /* This function does the dot product <x,y>                                       */
  /* in parallel                                                                    */
  /* IMPORTANT; we consider that x (and y) correspond to the vector indices between */
  /* the absolute indices [block_index[tli]:block_index[bri+1]-1]                   */
  /*                                                                                */
  /**********************************************************************************/

  dim_t i;
  COEF tloc, ro;
  int bl;
  int *block_index;
  int *block_keyindex;
  int *block_key;
  blas_t stride = 1;
  int offset;

  block_index = DBL->LHID.block_index;
  block_keyindex = DBL->LHID.block_keyindex;
  block_key = DBL->LHID.block_key;
  
  tloc = 0;
  
  offset = block_index[tli];
  for(i=tli;i<=bri;i++)
    {
      
      /*if(proc_id == block_key[ block_keyindex[i] ])*/
      if(proc_id == DBL->row_leader[i])
	{
	  bl = block_index[i+1] - block_index[i];
#ifdef TYPE_REAL
	  tloc += BLAS_DOT(bl, x + block_index[i]-offset, stride, y+block_index[i]-offset, stride);
#endif
#ifdef TYPE_COMPLEX
	  /** CAREFULL <x,y> != <y,x> in complex by default my_zdotc(x,y) make conj(x)*y **/
	  tloc += my_zdotc(bl, x + block_index[i]-offset, stride, y+block_index[i]-offset, stride);
	  /*{
	    dim_t h;
	    for(h= block_index[i]-offset;h< block_index[i]-offset+bl;h++)
	      {
		tloc += CONJ(x[h])*y[h];
		fprintfd(stderr, "Proc %d, tloc[%d] = (%g+%gi) (%g+%gi) (%g+%gi) \n", proc_id, h, CONJ(x[h]), y[h], CONJ(x[h])*y[h]);
	      }
	      }*/
#endif
	}
    }

  MPI_Allreduce(&tloc, &ro, CC(1), MPI_COEF_TYPE, MPI_SUM, DBL->mpicom);
  return ro;
}
  

#ifdef TODO
void PhidalCommVec_poste_receive(PhidalDistrPrec *P, PhidalDistrHID *DBL)
{
  /** Start the persistent receive requests **/
  for(i=L->tli;i<=L->bri;i++)
    {
      if(commvec->out_ctrb[i-L->tli] > 0)
	{
#ifdef TRACE_COM
	  fprintfd(stderr, "Proc %d poste ctrb receive %d \n", DL->proc_id, DBL->loc2glob_blocknum[i]);
#endif
	  if(MPI_Startall(commvec->out_ctrb[i-L->tli], commvec->recv_rqtab[i-L->tli]) != MPI_SUCCESS)
	    {
	      fprintfd(stderr, "Error in start recv contrib for vec %d \n", i);
	      assert(0);
	    }
	}
      if(commvec->out_ctrb[i-L->tli] == -1)
	{
#ifdef TRACE_COM
	  fprintfd(stderr, "Proc %d poste vector receive %d \n", DL->proc_id, DBL->loc2glob_blocknum[i]);
#endif 
	  if(MPI_Start(commvec->recv_rqtab[i-L->tli]) != MPI_SUCCESS)
	    {
	      fprintfd(stderr, "Error in start recv vector for vec %d \n", i);
	      assert(0);
	    }
	}
    }
}
#endif

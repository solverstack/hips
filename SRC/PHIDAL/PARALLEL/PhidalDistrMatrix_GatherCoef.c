/* @authors P. HENON */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <assert.h>
#include <math.h>

#include "phidal_parallel.h"




void PhidalDistrMatrix_GatherCoef(flag_t job, int op, PhidalDistrMatrix *DM, PhidalDistrHID *DBL)
{
  /*************************************************************/
  /* This function gather the coefficients in the matrix part  */
  /* that are shared by several processor                      */
  /* the common matrix blocks are summed on the leader         */
  /* processor, then an entry M(i,j) will be the sum of all    */
  /* entries in all processor local matrix                     */
  /* the entry M(i,j) of the matrix will be the one            */
  /* that is on the leader processor                           */
  /* job = 0: on return the shared block are only known by     */
  /* the leader processor (no duplicated matrices)             */
  /* job = 1: all the processor know the shared matrix blocks  */
  /* op  = 0: the shared block are merged  if two aij are      */
  /* different then the one with maximum modulus is retained   */
  /* op  = 1: shared blocks are summed on the leader processor */
  /*       the results is knowns by other processor if job = 1 */
  /*************************************************************/

  dim_t i,k/* , */ /* s, */ /* start ,end */;
  PhidalFactComm FC;
  PhidalHID *BL;
  PhidalMatrix *M;
  BL = &DBL->LHID;
  M = &DM->M;
  
  /*** Allocate the communicator of the matrix ***/
  PhidalFactComm_Setup(&FC, DM, DBL);

#ifdef TRACE_COM
 {
   dim_t j;
   
   for(i=0;i<BL->nblock;i++)
     {
       fprintfd(stderr, "Connector %d = ",DBL->loc2glob_blocknum[i]);
       for(j=DBL->block_psetindex[i];j<DBL->block_psetindex[i+1];j++)
	 fprintfd(stderr, "%d ", DBL->block_pset[j]);
       fprintfd(stderr, "\n");
     }


 }
#endif

#ifndef OLD_COMM_AHEAD
  /*** Poste receive for contribution blocks **/ 
  /*fprintfv(5, stderr, "Proc %d posteCtrbReceive \n", DBL->proc_id);*/
 PhidalFactComm_PosteCtrbReceive(&FC, DM, M->tlj, MIN(M->tlj+COMM_AHEAD-1, M->brj));
 
 /** Send Non leader blocks **/
 for(k=M->tlj;k<=M->brj;k++)
   {
     if(k+COMM_AHEAD <= M->brj)
       PhidalFactComm_PosteCtrbReceive(&FC, DM, k+COMM_AHEAD, k+COMM_AHEAD);

     for(i=M->cia[k];i<M->cia[k+1];i++)
       {
	 if(DM->cind[i] >= 0 && DM->clead[i] != DM->proc_id)
	   {
#ifdef TRACE_COM
	     fprintfd(stderr, "Proc %d send (%d,%d) to proc %d \n", DBL->proc_id, DBL->loc2glob_blocknum[M->cja[i]], DBL->loc2glob_blocknum[k], DM->clead[i]);
#endif
	     send_matrix(M->ca[i], DM->cind[i], CTRB_TAG, &FC);
	     reinitCS(M->ca[i]);
	     if(job == 1)
	       poste_block_receive(DM->cind[i], LEAD_TAG,  &FC);
	   }
	 
	 /** Receive Leader blocks **/
	 if(DM->cind[i] >= 0 && DM->clead[i] == DM->proc_id)
	   {
#ifdef TRACE_COM
	     fprintfd(stderr, "Proc %d receive_gather (%d,%d) \n", DBL->proc_id, DBL->loc2glob_blocknum[M->cja[i]], DBL->loc2glob_blocknum[k]);
#endif
	     if(op == 0)
	       receive_gather(SYNCHRONOUS, M->ca[i], DM->cind[i], &FC);
	     else
	       receive_contrib(SYNCHRONOUS, M->ca[i], DM->cind[i], &FC);
	     
	     if(job == 1) /** Send matrix to non leader block **/
	       send_matrix(M->ca[i], DM->cind[i], LEAD_TAG, &FC);
	   }
	 
	 /** Receive block from leader processor (job == 1) **/
	 if(job == 1)
	   if(DM->cind[i] >= 0 && DM->clead[i] != DM->proc_id)
	     {
	       /*fprintfv(5, stderr, "Proc %d receive  (%d,%d) from proc %d\n", DBL->proc_id, M->cja[i], k,DM->clead[i] );*/
	       receive_matrix(SYNCHRONOUS, M->ca[i], DM->cind[i], &FC); 
	     }
       }
   }
#else
 
 for(start=M->tlj;start<=M->brj;start+=COMM_AHEAD)
   {
     
     end = MIN(M->brj, start+COMM_AHEAD-1);
     /*** Poste receive for contribution blocks **/ 
     /*fprintfv(5, stderr, "Proc %d posteCtrbReceive \n", DBL->proc_id);*/
     PhidalFactComm_PosteCtrbReceive(&FC, DM, start, end);
     
     /** Send Non leader blocks **/
     for(k=start;k<=end;k++)
       for(i=M->cia[k];i<M->cia[k+1];i++)
	 if(DM->cind[i] >= 0 && DM->clead[i] != DM->proc_id)
	   {
#ifdef TRACE_COM
	     fprintfd(stderr, "Proc %d send (%d,%d) to proc %d \n", DBL->proc_id, DBL->loc2glob_blocknum[M->cja[i]], DBL->loc2glob_blocknum[k], DM->clead[i]);
#endif
	     send_matrix(M->ca[i], DM->cind[i], CTRB_TAG, &FC);
	     reinitCS(M->ca[i]);
	     if(job == 1)
	       poste_block_receive(DM->cind[i], LEAD_TAG,  &FC);
	   }
     
     /** Receive Leader blocks **/
     for(k=start;k<=end;k++)
	for(i=M->cia[k];i<M->cia[k+1];i++)
	  if(DM->cind[i] >= 0 && DM->clead[i] == DM->proc_id)
	    {
#ifdef TRACE_COM
	      fprintfd(stderr, "Proc %d receive_gather (%d,%d) \n", DBL->proc_id, DBL->loc2glob_blocknum[M->cja[i]], DBL->loc2glob_blocknum[k]);
#endif
	      if(op == 0)
		receive_gather(SYNCHRONOUS, M->ca[i], DM->cind[i], &FC);
	      else
		receive_contrib(SYNCHRONOUS, M->ca[i], DM->cind[i], &FC);

	      if(job == 1) /** Send matrix to non leader block **/
		send_matrix(M->ca[i], DM->cind[i], LEAD_TAG, &FC);
	    }

      /** Receive block from leader processor (job == 1) **/
      if(job == 1)
	for(k=start;k<=end;k++)
	  for(i=M->cia[k];i<M->cia[k+1];i++)
	    if(DM->cind[i] >= 0 && DM->clead[i] != DM->proc_id)
	      {
		/*fprintfv(5, stderr, "Proc %d receive  (%d,%d) from proc %d\n", DBL->proc_id, M->cja[i], k,DM->clead[i] );*/
		receive_matrix(SYNCHRONOUS, M->ca[i], DM->cind[i], &FC); 
	      }
    }
#endif

  PhidalFactComm_Clean(&FC);
#ifdef DEBUG_M
  PhidalDistrMatrix_Check(DM, DBL);
#endif

}

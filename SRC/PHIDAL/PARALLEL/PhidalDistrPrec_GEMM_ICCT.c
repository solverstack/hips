/* @authors P. HENON */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>

#define PARALLEL

#include "phidal_parallel.h"


void PhidalDistrPrec_GEMM_ICCT(int_t levelnum, REAL *droptab, PhidalDistrMatrix *G, PhidalDistrMatrix *A, COEF* DG, 
			       PhidalDistrPrec *P, PhidalDistrHID *BL, PhidalOptions *option)
{
  PhidalDistrMatrix *VL;
  chrono_t t1, t2;
  COEF *D;

#ifdef DEBUG_M
  assert(option->schur_method != 1);
  assert(A->M.tli == A->M.tlj);
  assert(A->M.bri == A->M.brj);
  assert(G->M.tli == A->M.tli);
  assert(G->M.bri == A->M.bri);
  assert(G->M.brj == A->M.tlj-1);
  assert(levelnum == option->forwardlev);
#endif

 
  t1 = dwalltime();
  P->dim = A->M.dim1;
  P->symmetric = 1;
  P->schur_method = 2;
  P->forwardlev = 0;
  P->levelnum =  levelnum;

  VL = (PhidalDistrMatrix *)malloc(sizeof(PhidalDistrMatrix));
  PhidalDistrMatrix_Init(VL);
  PhidalDistrMatrix_Setup(G->M.tlj, G->M.tlj, A->M.bri, A->M.bri, "L", "N", option->locally_nbr, VL, BL);

  PhidalDistrMatrix_Cut(A, VL, BL);
  PhidalDistrMatrix_Cut(G, VL, BL);
  

  D = (COEF *)malloc(sizeof(COEF)*VL->M.dim1);

  PrecInfo_AddNNZ(P->info, G->M.dim1);  
  memcpy(D, DG, sizeof(COEF)*G->M.dim2);
  
  PhidalDistrMatrix_ICCT_Restrict(A->M.tli, 0, VL, D, option->droptol1, droptab, option->fillrat, BL, option, P->info);

  M_MALLOC(P->LU, D(SCAL));
  PREC_L_SCAL(P) = (PhidalDistrMatrix *)malloc(sizeof(PhidalDistrMatrix));
  PhidalDistrMatrix_Setup(A->M.tli, A->M.tli, A->M.bri, A->M.bri, "L", "N", option->locally_nbr, PREC_L_SCAL(P), BL);
  PhidalDistrMatrix_Cut(VL, PREC_L_SCAL(P), BL);

  P->D = (COEF *)malloc(sizeof(COEF)*A->M.dim1);
  assert(G->M.dim2+A->M.dim1 == VL->M.dim1);
  memcpy(P->D, D+G->M.dim2, sizeof(COEF)*A->M.dim1);
  free(D);
  PhidalDistrMatrix_Clean(VL);
  free(VL);

  /*** Construct U as a virtual matrix equal to the transpose of L ***/
  PREC_U_SCAL(P) = (PhidalDistrMatrix *)malloc(sizeof(PhidalDistrMatrix));
  PhidalDistrMatrix_Init(PREC_U_SCAL(P));
  PhidalDistrMatrix_BuildVirtualMatrix(PREC_L_SCAL(P)->M.tli, PREC_L_SCAL(P)->M.tlj, PREC_L_SCAL(P)->M.bri, PREC_L_SCAL(P)->M.brj, PREC_L_SCAL(P), PREC_U_SCAL(P), BL); 
  PhidalDistrMatrix_Transpose(PREC_U_SCAL(P));
  t2 = dwalltime();

  

  fprintfv(5, stdout, "Proc %d Time in GEMM_ICCT = %g \n", BL->proc_id, t2-t1);
}

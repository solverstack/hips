/* @authors P. HENON */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>

#include "phidal_parallel.h"


void PhidalDistrMatrix_MatVec(flag_t job, PhidalDistrMatrix *DM, PhidalDistrHID *DBL, COEF *x, COEF *y)
{
  /*****************************************************************/
  /* This function does y = M.x                                    */
  /* NOTE: At the end only the part of y for which the proc is the */
  /* leader is set to the correct value                            */
  /* x must be known for each row a processor deals with even if it*/
  /* is not the leader                                             */
  /* job == 1 : all the processors know the result on overlaped part */
  /* DOES ALL THE PROCESSOR NEEDS TO NOW THE RESULTS WEHRE THEY ARE
     NOT LEADER ???????       */
  /* OIMBE ATTENTION IN PREVIOUS PHIDAL THE VALUE OF Y WAS SET     */
  /* EVEN IF THE PROCESSOR WAS NOT THE LEADER                      */
  /*****************************************************************/
  dim_t i,j,k;
  int offseti, offsetj;
  COEF *yptr;
  int *bind;
  PhidalMatrix *M;
  PhidalHID *BL;
  PhidalCommVec *commvec;
  mpi_t proc_id;

  BL = &DBL->LHID;
  proc_id = DM->proc_id;
  M = &(DM->M);
  commvec = &(DM->commvec);

#ifdef DEBUG_M
  assert(commvec->init == 1);
#endif
  /** Start the persistent receive requests **/
  for(i=M->bri;i>=M->tli;i--)
    {
#ifdef DEBUG_M
      if(commvec->out_ctrb[i-M->tli] > 0)
	assert(DBL->row_leader[i] == proc_id);
      if(commvec->out_ctrb[i-M->tli] == -1)
	assert(DBL->row_leader[i] != proc_id);
#endif
      if(commvec->out_ctrb[i-M->tli] > 0)
	if(MPI_Startall(commvec->out_ctrb[i-M->tli], commvec->recv_rqtab[i-M->tli]) != MPI_SUCCESS)
	  {
	    fprintfd(stderr, "Error in start recv contrib for vec %d \n", i);
	    assert(0);
	  }

      if(job == 1)
	if(commvec->out_ctrb[i-M->tli] == -1)
	  if(MPI_Start(commvec->recv_rqtab[i-M->tli]) != MPI_SUCCESS)
	    {
	      fprintfd(stderr, "Error in start recv vector for vec %d \n", i);
	      assert(0);
	    }
    }

  bind = BL->block_index;
  bzero(y,sizeof(COEF)*M->dim1);
  offseti = bind[M->tli];
  offsetj = bind[M->tlj];

  /** Initialize the contribution counters **/
  memcpy(commvec->loc_cnt, commvec->loc_ctrb, sizeof(int_t)*(M->bri-M->tli+1));

  for(i=M->bri;i>=M->tli;i--)
    {
      yptr = y+(bind[i]-offseti);
      for(k=M->ria[i];k<M->ria[i+1];k++)
	if(DM->rlead[k] == proc_id && M->ra[k]->nnzr >0)
	  {
	    j = M->rja[k];
	    if(M->csc == 0)
	      matvec_add(M->ra[k], x + (bind[j]-offsetj), yptr);
	    else
	      CSC_matvec_add(M->ra[k], x + (bind[j]-offsetj), yptr);
#ifdef DEBUG_M
	    assert(commvec->loc_cnt[i-M->tli]>0);
#endif

	    if(--commvec->loc_cnt[i-M->tli] == 0 && DBL->row_leader[ i ] != proc_id)
	      {
		/**** Send Ctrb i ***/
		MPI_Send(yptr, CC(bind[i+1]-bind[i]), MPI_COEF_TYPE, DBL->row_leader[i], DBL->loc2glob_blocknum[i], DBL->mpicom); 
		break;
	      }
	  }

      if(M->symmetric == 1)
	{
	  /** Treat the symmetric part **/
#ifdef DEBUG_M
	  assert(M->csc == 1); /** Could change one day so keep the case csc == 0**/
	  assert(M->cja[M->cia[i]] == i);
#endif
	  if(DM->clead[M->cia[i]] == proc_id && M->ca[M->cia[i]]->nnzr > 0)
	    {
	      if(M->csc == 0)
		CSC_NoDiag_matvec_add(M->ca[M->cia[i]], x + (bind[i]-offsetj), yptr); 
	      else
		NoDiag_matvec_add(M->ca[M->cia[i]], x + (bind[i]-offsetj), yptr);
#ifdef DEBUG_M
	      assert(commvec->loc_cnt[i-M->tli]>0);
#endif
	      if(--commvec->loc_cnt[i-M->tli] == 0 && DBL->row_leader[i] != proc_id) 
		MPI_Send(yptr, CC(bind[i+1]-bind[i]), MPI_COEF_TYPE, DBL->row_leader[i], DBL->loc2glob_blocknum[i], DBL->mpicom); 
	    }
	  
	  for(k=M->cia[i]+1;k<M->cia[i+1];k++)
	    if(DM->clead[k] == proc_id && M->ca[k]->nnzr >0)
	      {
		j = M->cja[k];
		if(M->csc == 0)
		  CSC_matvec_add(M->ca[k], x+(bind[j]-offsetj), yptr); 
		else
		  matvec_add(M->ca[k], x+(bind[j]-offsetj), yptr); 
#ifdef DEBUG_M
		assert(commvec->loc_cnt[i-M->tli]>0);
#endif
		if(--commvec->loc_cnt[i-M->tli] == 0 && DBL->row_leader[i] != proc_id) 
		  MPI_Send(yptr, CC(bind[i+1]-bind[i]), MPI_COEF_TYPE, DBL->row_leader[i], DBL->loc2glob_blocknum[i], DBL->mpicom); 
	      }
	}

        
    }
  
  /*** Receive the contributions where the processor is the owner of the facing diagonal block ***/
  for(i=M->bri;i>=M->tli;i--)
    {
      if(commvec->out_ctrb[ i-M->tli ] > 0)
	{
	  yptr = y+(bind[i]-offseti);
	  /*fprintfd(stderr, "Proc %d receiveVecAdd for vec %d \n", DM->proc_id, i);*/
	  PhidalCommVec_ReceiveVecAdd(1, i-M->tli, yptr, commvec); 
	}

      
      if(job == 1)
	if(DBL->row_leader[i] == proc_id && DBL->block_psetindex[i+1]-DBL->block_psetindex[i]>1)
	  {
	    /** Send the vector to row non-leader processor **/
	    for(j=DBL->block_psetindex[i];j<DBL->block_psetindex[i+1];j++)
	      if(DBL->block_pset[j] != DBL->proc_id)
		MPI_Send(yptr, CC(bind[i+1]-bind[i]), MPI_COEF_TYPE, DBL->block_pset[j],DBL->loc2glob_blocknum[i], DBL->mpicom);
	  }

    }
  
  /*** Receive the vector y parts where the processor is not the
       row_leader **/
  if(job == 1)
    for(i=M->bri;i>=M->tli;i--)
      if(commvec->out_ctrb[ i-M->tli ] == -1)
	{
#ifdef DEBUG_M
	  assert(commvec->status != NULL);
#endif
	  if(MPI_Wait(commvec->recv_rqtab[i-M->tli], commvec->status) != MPI_SUCCESS)
	    {
	      fprintfd(stderr, "Error in PhidalDistrMatrix_Matvec for vector %d \n" ,i);
	      assert(0);
	    }
	yptr = y+(BL->block_index[i]-offseti);
	memcpy(yptr, commvec->t + (BL->block_index[i]-offseti), sizeof(COEF)* commvec->veclen[i-M->tli]);
      }
  
  

  /*for(i=M->tli;i<=M->bri;i++)
    if(DBL->row_leader[i] == proc_id)
    fprintfd(stderr, "Proc %d norm2b[%d] = %g \n", DM->proc_id, DBL->loc2glob_blocknum[i],
      norm2(y+(BL->block_index[i]-offseti), BL->block_index[i+1]-BL->block_index[i]));*/
}


void PhidalDistrMatrix_MatVecAdd(flag_t job, PhidalDistrMatrix *DM, PhidalDistrHID *DBL, COEF *x, COEF *y)
{
  dim_t i;
  COEF *t;
  t = (COEF *)malloc(sizeof(COEF)*DM->M.dim1);
  PhidalDistrMatrix_MatVec(job, DM, DBL, x, t);

  for(i=0;i<DM->M.dim1;i++)
    y[i] += t[i];
  free(t);
}


void PhidalDistrMatrix_MatVecSub(flag_t job, PhidalDistrMatrix *DM, PhidalDistrHID *DBL, COEF *x, COEF *y)
{
  dim_t i;
  COEF *t;
  t = (COEF *)malloc(sizeof(COEF)*DM->M.dim1);
  PhidalDistrMatrix_MatVec(job, DM, DBL, x, t);

  for(i=0;i<DM->M.dim1;i++)
    y[i] -= t[i];
  free(t);
}





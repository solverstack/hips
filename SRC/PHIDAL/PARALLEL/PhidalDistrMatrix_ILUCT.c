/* @authors P. HENON */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>

#include "phidal_parallel.h"


void PhidalDistrMatrix_ILUCT(flag_t job, PhidalDistrMatrix *L, PhidalDistrMatrix *U, REAL droptol, REAL *droptab, REAL fillrat, PhidalDistrHID *DBL, PhidalOptions *option, PrecInfo *info)
{
  /******************************************************************************************/
  /* This function computes the incomplete factorization A = L.U of a  matrix               */
  /* This is a similar function to PhidalMatrix_ILUTP (without pivoting)                    */
  /* BUT the block algorithm is made column wisely as in ILUC                               */
  /* On entry:                                                                              */
  /*   L is the lower triangular part of the matrix to factorize in CSR format              */
  /*   U is the upper triangular part of the matrix to factorize in CSR format              */
  /* on return:                                                                             */
  /* L the strictly lower triangular factor (unitary on the diagonal not stored) in CSR     */
  /* U the upper triangular factor (unitary on the diagonal not stored) in CSR              */
  /******************************************************************************************/
  PhidalDistrMatrix_ILUCT_Restrict(L->M.tli, job, L, U, droptol, droptab, fillrat, DBL, option, info);
}

void PhidalDistrMatrix_ILUCT_Restrict(int START, flag_t job, PhidalDistrMatrix *L, PhidalDistrMatrix *U, REAL droptol, REAL *droptab, REAL fillrat, PhidalDistrHID *DBL, PhidalOptions *option, PrecInfo *info)
{
  int i, k;
  int ii, jj, kk;
  int *wki1, *wki2;
  COEF *wkd;
  csptr csL, csU;
  csptr *csrtab1;
  csptr *csrtab2;
  int nnb;
  Heap heap;
  REAL *droptabtmp;
  REAL *dropptr;
  
  CellCSDistr *Lfirstcol, *Ufirstrow;
  csptr *list1, *list2;
  int *listindex;
  int nk, *jak, *rindk;
  csptr *rak;
  PhidalFactComm FCL, FCU;
  PhidalHID *BL;
  PhidalMatrix *LL, *UU;
  
#ifdef DEBUG_M
  int toto = 0;
  int totok;
#endif

#ifdef DEBUG_M
  PhidalDistrMatrix_Check(L, DBL);
  PhidalDistrMatrix_Check(U, DBL);
  assert(START >= L->M.tli);
  assert(START <= L->M.bri);
#endif



  LL = &(L->M);
  UU = &(U->M);
  BL = &(DBL->LHID);

  /*MPI_Barrier(DBL->mpicom);*/
  /*  fprintferr(stderr, "FACTO PROC %d \n", L->proc_id);*/

  /*** Allocate the communicator of the matrix ***/
  PhidalFactComm_Setup(&FCL, L, DBL);
  PhidalFactComm_Setup(&FCU, U, DBL);

#ifdef TRACE_COM
  {
    dim_t j;
      for(i=LL->tli;i<=LL->bri;i++)
	for(j=LL->cia[i];j<LL->cia[i+1];j++)
	  if(L->cind[j] >= 0)
	    fprintfv(5, stderr, "Block L (%d %d) lead %d \n", DBL->loc2glob_blocknum[LL->cja[j]], DBL->loc2glob_blocknum[i],  L->clead[j]);
    
    for(i=UU->tli;i<=UU->bri;i++)
      for(j=UU->ria[i];j<UU->ria[i+1];j++)
	if(U->rind[j] >= 0)
	  fprintfv(5, stderr, "Block U (%d %d) lead %d \n", DBL->loc2glob_blocknum[i], DBL->loc2glob_blocknum[UU->rja[j]], U->rlead[j]);
  }
#endif





  /*** Find the largest diagonal block in this matrix ****/
  ii = 0;

  for(i=START;i<=LL->bri;i++)
    if(BL->block_index[i+1]-BL->block_index[i] > ii)
      ii = BL->block_index[i+1]-BL->block_index[i];

  Heap_Init(&heap, ii);
  wki1= (int *)malloc(sizeof(int)*ii);
  wki2= (int *)malloc(sizeof(int)*ii);
  wkd = (COEF *)malloc(sizeof(COEF)*ii);

  ii = 0;

  for(k=START;k<=LL->bri;k++)
    if(LL->ria[k+1] - LL->ria[k] > ii)
      ii = LL->ria[k+1] - LL->ria[k];

  for(k=START;k<=UU->brj;k++)
    if(UU->cia[k+1] - UU->cia[k] > ii)
      ii = UU->cia[k+1] - UU->cia[k];
  if(ii>0)
    {
      jak = (int *)malloc(sizeof(int)*ii);
      rindk = (int *)malloc(sizeof(int)*ii);
      rak = (csptr *)malloc(sizeof(csptr)*ii);
    }
  else
    {
      jak = NULL;
      rak = NULL;
      rindk = NULL;
    }

  ii=0;
  for(k=LL->tlj;k<=LL->brj;k++)
    {
      nnb = (LL->ria[k+1]-LL->ria[k] - 1) * (UU->ria[k+1]-UU->ria[k]);
      if( nnb > ii)
	ii = nnb;
      nnb = (LL->cia[k+1]-LL->cia[k] - 1) * (UU->cia[k+1]-UU->cia[k]);
      if( nnb > ii)
	ii = nnb;
    }

  list1 = (csptr *)malloc(sizeof(csptr)* (ii+1));
  list2 = (csptr *)malloc(sizeof(csptr)* (ii+1));
  listindex = (int *)malloc(sizeof(int)*(LL->bri-START+1));

  Lfirstcol = (CellCSDistr *)malloc(sizeof(CellCSDistr)*(LL->brj+1));
  for(k=LL->tlj;k<=LL->brj;k++)
    {
      Lfirstcol[k].nnz = LL->cia[k+1]-LL->cia[k];
      Lfirstcol[k].ja  = LL->cja + LL->cia[k];
      Lfirstcol[k].ma  = LL->ca + LL->cia[k];
      Lfirstcol[k].pind = L->cind + LL->cia[k]; 
    }
  Ufirstrow = (CellCSDistr *)malloc(sizeof(CellCSDistr)*(UU->bri+1));
  for(k=UU->tli;k<=UU->bri;k++)
    {
      Ufirstrow[k].nnz = UU->ria[k+1]-UU->ria[k];
      Ufirstrow[k].ja  = UU->rja + UU->ria[k];
      Ufirstrow[k].ma  = UU->ra + UU->ria[k];
      Ufirstrow[k].pind = U->rind + UU->ria[k]; 
   }
  

#ifdef SYMMETRIC_DROP
  droptabtmp = (REAL *)malloc(sizeof(REAL)*LL->dim1);
  if(droptab != NULL)
    memcpy(droptabtmp, droptab, sizeof(REAL)*LL->dim1);
  else
    for(i=0;i<LL->dim1;i++)
      droptabtmp[i] = 1.0;
#else
  droptabtmp = droptab;
#endif
  
  
#ifndef OLD_COMM_AHEAD
  PhidalFactComm_PosteCtrbReceiveLU(&FCL, &FCU, L, U, START, MIN(START+COMM_AHEAD-1, LL->bri));
#endif
  
  for(k=START;k<=LL->brj;k++)
    {
      
#ifdef OLD_COMM_AHEAD
#ifndef COMM_MEM
      if ((k-START) % COMM_AHEAD == 0)
	PhidalFactComm_PosteCtrbReceiveLU(&FCL, &FCU, L, U, k, MIN(k+COMM_AHEAD-1, LL->bri));
#else
      PhidalFactComm_PosteCtrbReceiveLUMem(&FCL, &FCU, L, U);
#endif

#else
      if(k+COMM_AHEAD <= LL->bri)
	PhidalFactComm_PosteCtrbReceiveLU(&FCL, &FCU, L, U, k+COMM_AHEAD, MIN(k+COMM_AHEAD, LL->bri));
#endif

      /*** Factorize the column block k of L and the row k of U ***/
      csL =  LL->ca[ LL->cia[k] ];
      csU =  UU->ra[ UU->ria[k] ];
#ifdef DEBUG_M
      assert(LL->cja[LL->cia[k]] == k);
      assert(UU->rja[UU->ria[k]] == k);
#endif
      
      /*if(k>LL->tlj)*/
	{
	  /**********************************************/
	  /* Compute the row-block U(k, k:n)            */
	  /**********************************************/
	  nk = 0;
	  for(jj=LL->ria[k];jj<LL->ria[k+1]-1;jj++)
	    if(LL->ra[jj]->nnzr > 0) 
	      {
		jak[nk] = LL->rja[jj];
		rak[nk] = LL->ra[jj];
		rindk[nk] = L->rind[jj];
		nk++;
	      }
	  
	  if(nk>0)
	    {

	      CellCSDistr_ListUpdate(Ufirstrow, k,  nk,  jak);
	      CellCSDistr_IntersectList(DBL->proc_id, Ufirstrow,
					nk,  jak, rindk, rak, L->pset_index, L->pset,
					UU->ria[k+1]-UU->ria[k], UU->rja+UU->ria[k],U->pset_index, U->pset, 
					listindex, list1, list2);

#ifdef DEBUG_M
	      for(jj=0;jj<(UU->ria[k+1]-UU->ria[k]);jj++)
		{
		  toto += listindex[jj];
		  totok += listindex[jj];
		}
#endif
	    }
	  else
	    {
	      bzero(listindex, sizeof(int)*(UU->ria[k+1]-UU->ria[k]));
	    }

	  /** Treat the contribution to non local block in priority **/
	  for(ii=0;ii<UU->ria[k+1]-UU->ria[k];ii++)
	    {
	      jj = ii+UU->ria[k];
	      if(U->rlead[jj] != U->proc_id)
		{
		  i = UU->rja[jj];
		  
		  /*** NO DROPPING HERE ***/
		  /** List 1 contains block in the  U part and list 2
		      in L part ***/
		  csrtab2 = list1 + ii*nk;   
		  csrtab1 = list2 + ii*nk;
		  nnb     = listindex[ii];
		  if(nnb>0)
		    {
		      if(info != NULL)
			PrecInfo_SubNNZ(info, CSnnz( UU->ra[jj]));
		      CSRrowMultCSRcol(0.0, NULL, -1.0, nnb, -1.0, csrtab1, csrtab2, 
				       UU->ra[jj], BL->block_index[i+1]-BL->block_index[i], wki1, wki2, wkd); /*** NO DROPPING HERE ***/
		      if(info != NULL)
			PrecInfo_AddNNZ(info, CSnnz( UU->ra[jj]));
		    }
		  /** Send the contrib to the leader processor of this
		      matrix **/
#ifdef TRACE_COM
		  fprintfv(5, stderr, "PROC %d send ctrb U %d %d to proc %d \n", U->proc_id, DBL->loc2glob_blocknum[k], 
			  DBL->loc2glob_blocknum[UU->rja[jj]], U->rlead[jj]);
#endif
		  send_matrix(UU->ra[jj], U->rind[jj], CTRB_TAG_U, &FCU); 

		  if(info != NULL)
		    PrecInfo_SubNNZ(info, CSnnz(UU->ra[jj]));
		  reinitCS(UU->ra[jj]);
		
		  /** Poste the receive for the factorized matrix that
		      the leader processor will send **/
		  
		  if(i == k)
		    {
#ifdef TRACE_COM
		      fprintfv(5, stderr, "PROC %d poste block L %d %d from proc %d \n", L->proc_id, DBL->loc2glob_blocknum[k], DBL->loc2glob_blocknum[LL->cja[LL->cia[k]]], L->clead[LL->cia[k]]);
#endif
		      poste_block_receive(L->cind[LL->cia[k]], LEAD_TAG_L, &FCL);
		    }
#ifdef TRACE_COM
		  fprintfv(5, stderr, "PROC %d poste block U %d %d from proc %d \n", U->proc_id, DBL->loc2glob_blocknum[k], 			      DBL->loc2glob_blocknum[UU->rja[jj]], U->rlead[jj]);
#endif
		  poste_block_receive(U->rind[jj], LEAD_TAG_U, &FCU);
		}
	    }
	      


	  /** Treat the contribution to local block **/
	  for(ii=0;ii<UU->ria[k+1]-UU->ria[k];ii++)
	    {
	      jj = ii+UU->ria[k];
	      if(U->rlead[jj] == U->proc_id)
		{
		  i = UU->rja[jj];
		  
		  /*** NO DROPPING HERE ***/
		  /** List 1 contains block in the  U part and list 2
		      in L part ***/
		  csrtab2 = list1 + ii*nk;   
		  csrtab1 = list2 + ii*nk;
		  nnb     = listindex[ii];
		  if(nnb>0)
		    {
		      if(info != NULL)
			PrecInfo_SubNNZ(info, CSnnz(UU->ra[jj]));
		      CSRrowMultCSRcol(0.0, NULL, -1.0, nnb, -1.0, csrtab1, csrtab2, 
				       UU->ra[jj], BL->block_index[i+1]-BL->block_index[i], wki1, wki2, wkd); /*** NO DROPPING HERE ***/
		      if(info != NULL)
			PrecInfo_AddNNZ(info, CSnnz(UU->ra[jj]));
		    }
		}
	    }
	  


	  /**********************************************/
	  /* Compute the column-block L(k+1:n,k)        */
	  /**********************************************/
	  nk = 0;
	  for(jj=UU->cia[k];jj<UU->cia[k+1]-1;jj++)
	    if(UU->ca[jj]->nnzr > 0)  /*THE OTHER PROC DO NOT KNOW ABOUT THIS SO 
					IT CREATES AN ERROR */
	      {
		jak[nk] = UU->cja[jj];
		rak[nk] = UU->ca[jj];
		rindk[nk] = U->cind[jj];
		nk++;
	      }

	  if(nk>0)
	    {
	      CellCSDistr_ListUpdate(Lfirstcol, k+1,  nk,  jak);
	      /** Search all the "leader" products to do **/
	      CellCSDistr_IntersectList(DBL->proc_id, Lfirstcol,  
					nk,  jak, rindk, rak, U->pset_index, U->pset,
					LL->cia[k+1]-LL->cia[k]-1, LL->cja+LL->cia[k]+1, L->pset_index, L->pset,
					listindex, list1, list2);
	    }
	  else
	    {
	      bzero(listindex, sizeof(int)*(LL->cia[k+1]-LL->cia[k]-1));
	    }

#ifdef DEBUG_M
	  for(jj=0;jj<(LL->cia[k+1]-LL->cia[k]-1);jj++)
	    {
	      toto += listindex[jj];
	      totok += listindex[jj];
	    }
#endif

	  /** Treat the contribution to non local block in priority **/
	  for(ii=0;ii<LL->cia[k+1]-LL->cia[k]-1;ii++)
	    {
	      jj = ii+LL->cia[k]+1;
	      if(L->clead[jj] != L->proc_id)
		{
		  i = LL->cja[ii+LL->cia[k]+1];
		  /*** Compute L(i,k) = L(i,k) - L(i, 0:k-1).L(k, 0:k-1)t **/	      
		  /*** NO DROPPING HERE ***/
		  csrtab1 = list1 + ii*nk;
		  csrtab2 = list2 + ii*nk;
		  nnb     = listindex[ii];
		  if(nnb>0)
		    {
		      if(info != NULL)
			PrecInfo_SubNNZ(info, CSnnz( LL->ca[jj]));
		      CSRrowMultCSRcol(0.0, NULL, -1.0, nnb, -1.0, csrtab1, csrtab2, 
				       LL->ca[jj], BL->block_index[k+1]-BL->block_index[k], wki1, wki2, wkd); /*** NO DROPPING HERE ***/
		      if(info != NULL)
			PrecInfo_AddNNZ(info, CSnnz( LL->ca[jj])); 
		    }
		  /** Send the contrib to the leader processor of this
		      matrix **/
#ifdef TRACE_COM
		  fprintfv(5, stderr, "PROC %d send ctrb L %d %d to proc %d \n", L->proc_id,  
			  DBL->loc2glob_blocknum[LL->cja[jj]], DBL->loc2glob_blocknum[k], L->clead[jj]);
#endif
		  send_matrix(LL->ca[jj], L->cind[jj], CTRB_TAG_L, &FCL); 
		  if(info != NULL)
		    PrecInfo_SubNNZ(info, CSnnz(LL->ca[jj]));
		  reinitCS(LL->ca[jj]);
		 
#ifdef TRACE_COM
		  fprintfv(5, stderr, "PROC %d poste block L %d %d from proc %d \n", L->proc_id, 
			  DBL->loc2glob_blocknum[LL->cja[jj]], DBL->loc2glob_blocknum[k], L->clead[jj]);
#endif


		  /** Poste the receive for the factorized matrix that
		      the leader processor will send **/
		  poste_block_receive(L->cind[jj], LEAD_TAG_L, &FCL);
		}
	    }

	  /** Treat the contribution to local block **/
	  for(ii=0;ii<LL->cia[k+1]-LL->cia[k]-1;ii++)
	    {
	      jj = ii+LL->cia[k]+1;
	      if(L->clead[jj] == L->proc_id)
		{
		  i = LL->cja[ii+LL->cia[k]+1];
		  /*** Compute L(i,k) = L(i,k) - L(i, 0:k-1).L(k, 0:k-1)t **/	      
		  /*** NO DROPPING HERE ***/
		  csrtab1 = list1 + ii*nk;
		  csrtab2 = list2 + ii*nk;
		  nnb     = listindex[ii];
		  if(nnb>0)
		    {
		      if(info != NULL)
			PrecInfo_SubNNZ(info, CSnnz( LL->ca[jj]));
		      CSRrowMultCSRcol(0.0, NULL, -1.0, nnb, -1.0, csrtab1, csrtab2, 
				       LL->ca[jj], BL->block_index[k+1]-BL->block_index[k], wki1, wki2, wkd); /*** NO DROPPING HERE ***/
		      if(info != NULL)
			PrecInfo_AddNNZ(info, CSnnz( LL->ca[jj])); 
		    }
		}
	    }
	  
	  
	  
	}
      
	/** Deallocate blocks which column indices are < START **/
	for(ii=LL->ria[k];ii<LL->ria[k+1];ii++)
	  {
	    if( LL->rja[ii] >= START)
	      break;
	  
	    if(L->rlead[ii] == DBL->proc_id)
	      {
#ifndef PIC_INITIAL
		if(info != NULL)
		  PrecInfo_SubNNZ(info, CSnnz(LL->ra[ii]));
#endif
		reinitCS(LL->ra[ii]);
	
	      }
	  }

	/** Deallocate blocks which row indices are < START **/
	for(ii=UU->cia[k];ii<UU->cia[k+1];ii++)
	  {
	    if(UU->cja[ii] >= START)
	      break;

	    if(U->rlead[ii] == DBL->proc_id)
	      {
#ifndef PIC_INITIAL
		if(info != NULL)
		  PrecInfo_SubNNZ(info, CSnnz(UU->ca[ii]));
#endif
		reinitCS(UU->ca[ii]);
	      }
	  }

    

      /*** Factorize the diagonal block matrix A(k,k) ***/
      /**** NOTE: A(k,k) is stored in U(k,k)          ***/
      kk = BL->block_index[k]-BL->block_index[LL->tlj];
      if(U->rlead[ UU->ria[k]] == U->proc_id)
	{
#ifdef DEBUG_M
	  assert(L->clead[ LL->cia[k]] == L->proc_id);
#endif
	  if(info != NULL)
	    PrecInfo_SubNNZ(info, CSnnz(csU));


	  /*** Receive contribution for the diagonal block ***/
	  if(U->rind[UU->ria[k]] >= 0)
	    {
#ifdef TRACE_COM
	      fprintfv(5, stderr, "PROC %d RECEIVE the contrib for diagonal block U %d (loc %d) \n", U->proc_id, DBL->loc2glob_blocknum[k], k);
#endif
	      receive_contrib(SYNCHRONOUS, csU, U->rind[UU->ria[k]], &FCU);
	    }


	  /*fprintfv(5, stderr, "PROC %d factorize diagonal block %d \n", L->proc_id, k);*/
	  if(droptab == NULL)
	    CS_ILUT(csU, csL, csU, droptol, NULL, option->fillrat, &heap, wki1, wki2, wkd);
	  else
	    CS_ILUT(csU, csL, csU, droptol, droptab+kk, option->fillrat, &heap, wki1, wki2, wkd);

	  if(info != NULL)
	    {
	      PrecInfo_AddNNZ(info, CSnnz(csL));
	      PrecInfo_AddNNZ(info, CSnnz(csU));
	    }

	  if(L->cind[LL->cia[k]] >= 0)
	    {
#ifdef TRACE_COM
	      fprintfv(5, stderr, "PROC %d send L[%d] to non leader proc:\n", L->proc_id, DBL->loc2glob_blocknum[k]);
#endif
	      send_matrix(csL, L->cind[LL->cia[k]], LEAD_TAG_L, &FCL);
#ifdef TRACE_COM
	      fprintfv(5, stderr, "PROC %d send U[%d] to non leader proc \n", U->proc_id, DBL->loc2glob_blocknum[k]);
#endif
	      send_matrix(csU, U->rind[UU->ria[k]], LEAD_TAG_U, &FCU);
	    }
	  
	}
      else
	{
	  /** Receive the factorized diagonal block **/
#ifdef TRACE_COM
	  fprintfv(5, stderr, "PROC %d receive DIAG L %d \n", L->proc_id, DBL->loc2glob_blocknum[k]);
#endif
	  receive_matrix(SYNCHRONOUS, csL, L->cind[LL->cia[k]], &FCL); 	 

#ifdef TRACE_COM
	  fprintfv(5, stderr, "PROC %d receive DIAG U %d \n", U->proc_id, DBL->loc2glob_blocknum[k]);
#endif
	  receive_matrix(SYNCHRONOUS, csU, U->rind[UU->ria[k]], &FCU); 

	}

      /*** RESPECT THE ORDER ***/
    
      /*** 1-- Divide the column block matrices of L by U ***/
      for(ii=LL->cia[k]+1;ii<LL->cia[k+1];ii++)
	if(L->clead[ii] == L->proc_id)
	  {
	    i = LL->cja[ii];

	    if(info != NULL)
	      PrecInfo_SubNNZ(info, CSnnz(LL->ca[ii]));

	    /*** Receive the contributions for this block ****/
	    if(L->cind[ii] >= 0)
	      {
#ifdef TRACE_COM
		fprintfv(5, stderr, "PROC %d RECEIVE contrib L %d %d from proc %d \n", L->proc_id, DBL->loc2glob_blocknum[k], DBL->loc2glob_blocknum[LL->cja[ii]], L->clead[ii]);
#endif
		receive_contrib(SYNCHRONOUS, LL->ca[ii], L->cind[ii], &FCL);
	      }

	    if(droptab == NULL)
	      CSR_CSR_InvUT(csU, LL->ca[ii], droptol, NULL, fillrat, &heap, wki1, wki2, wkd);
	    else
	      CSR_CSR_InvUT(csU, LL->ca[ii], droptol, droptab+kk, fillrat, &heap, wki1, wki2, wkd);
	    

	    if(info != NULL)
	      PrecInfo_AddNNZ(info, CSnnz(LL->ca[ii]));




#ifdef OPTIM_FEW_NZR
	    /***** Sort the row of the matrice :need that for
		   CSRrowMultCSRcol optim ****/
	    ascend_column_reorder( LL->ca[ii]);
#endif
	    /** Send the factorized matrix to the other (non leader) processors  **/
	    if(L->cind[ii] >= 0)
	      {
#ifdef TRACE_COM
		fprintfv(5, stderr, "PROC %d send factorized diag block %d %d \n", 
			L->proc_id, LL->cja[ii], DBL->loc2glob_blocknum[k]); 
#endif
		send_matrix(LL->ca[ii], L->cind[ii], LEAD_TAG_L, &FCL); 
	      }
	  }
      

#ifdef SYMMETRIC_DROP
#ifdef DEBUG_M
      assert(droptabtmp != NULL);
#endif
      dropptr = droptabtmp+kk;
      for(i=0;i<csU->n;i++)
	{
#ifdef DEBUG_M
	  assert(csU->ja[i][0] == i);
#endif
	  dropptr[i] /= coefabs(csU->ma[i][0]);
	}
#endif

      /*** 2-- Divide the row block matrices of U by L ***/
      for(ii=UU->ria[k]+1;ii<UU->ria[k+1];ii++)
	if(U->rlead[ii] == U->proc_id)
	  {
	    i = UU->rja[ii];

	    if(info != NULL)
	      PrecInfo_SubNNZ(info, CSnnz(UU->ra[ii]));

	    /*** Receive the contributions for this block ****/
	    if(U->rind[ii] >= 0)
	      {
#ifdef TRACE_COM
		fprintfv(5, stderr, "PROC %d RECEIVE ctrb U %d %d from proc %d \n", U->proc_id, 
			DBL->loc2glob_blocknum[UU->rja[ii]], DBL->loc2glob_blocknum[k], U->rlead[ii]);
#endif
		receive_contrib(SYNCHRONOUS, UU->ra[ii], U->rind[ii], &FCU);
	      }

	    if(droptabtmp != NULL)
	      CSR_CSR_InvLT(csL, UU->ra[ii],  BL->block_index[i+1]-BL->block_index[i], droptol, droptabtmp+kk, fillrat, wki1, wki2, wkd);
	    else
	      CSR_CSR_InvLT(csL, UU->ra[ii],  BL->block_index[i+1]-BL->block_index[i], droptol, NULL, fillrat, wki1, wki2, wkd);

	    if(info != NULL)
	      PrecInfo_AddNNZ(info, CSnnz(UU->ra[ii]));


	    /** Send the factorized matrix to the other (non leader) processors  **/
	    if(U->rind[ii] >= 0)
	      {
#ifdef TRACE_COM
		fprintfv(5, stderr, "PROC %d send factorized block U %d %d \n", 
			U->proc_id, k, UU->rja[ii]); 
#endif
		send_matrix(UU->ra[ii], U->rind[ii], LEAD_TAG_U, &FCU); 
	      }

	 

	  }


      if(job == 0)
	{
	  /**** Deallocate the non local block in L(k,:) *****/
	  for(ii=LL->ria[k];ii<LL->ria[k+1];ii++)
	    if(L->rlead[ii] != DBL->proc_id)
	      {
		if(info != NULL)
		  PrecInfo_SubNNZ(info, CSnnz(LL->ra[ii]));
		reinitCS(LL->ra[ii]);
	
	      }

	  /**** Deallocate the non local block in U(:,k) *****/
	  for(ii=UU->cia[k];ii<UU->cia[k+1];ii++)
	    if(U->clead[ii] != DBL->proc_id)
	      {
		if(info != NULL)
		  PrecInfo_SubNNZ(info, CSnnz(UU->ca[ii]));
		reinitCS(UU->ca[ii]);
	
	      }
	}

    




      /** Receive the non-local block in column L(k+1:n , k) **/
      /** @@ OIMBE on peut faire qqchose de + compliquer pour retarder
	  la reception de ces blocs ***/
      for(ii=LL->cia[k]+1;ii<LL->cia[k+1];ii++)
	if(L->clead[ii] != L->proc_id)
	  {
	    i = LL->cja[ii];
#ifdef DEBUG_M
	    assert(L->cind[ii] >= 0);
#endif
	    /*** Receive the matrix from the leader processor ****/
#ifdef TRACE_COM
	    fprintfv(5, stderr, "PROC %d RECEIVE factorized block L %d %d from proc %d \n", L->proc_id, 
		    DBL->loc2glob_blocknum[k], DBL->loc2glob_blocknum[LL->cja[ii]],  L->clead[ii]);
#endif
	    
	    if(info != NULL)
	      PrecInfo_SubNNZ(info, CSnnz(LL->ca[ii]));
	    receive_matrix(SYNCHRONOUS, LL->ca[ii], L->cind[ii], &FCL);
	    if(info != NULL)
	      PrecInfo_AddNNZ(info, CSnnz(LL->ca[ii]));
	    
	  }


      /** Receive the non-local block in column U(k, k+1:n) **/
      /** @@ OIMBE on peut faire qqchose de + compliquer pour retarder
	  la reception de ces blocs ***/
      for(ii=UU->ria[k]+1;ii<UU->ria[k+1];ii++)
	if(U->rlead[ii] != L->proc_id)
	  {
	    i = UU->rja[ii];
#ifdef DEBUG_M
	    assert(U->rind[ii] >= 0);
#endif
	    /*** Receive the matrix from the leader processor ****/
#ifdef TRACE_COM
	    fprintfv(5, stderr, "PROC %d RECEIVE factorized block U %d %d from proc %d \n", U->proc_id, 
		    DBL->loc2glob_blocknum[UU->rja[ii]], DBL->loc2glob_blocknum[k], U->rlead[ii]);
#endif
	    if(info != NULL)
	      PrecInfo_SubNNZ(info, CSnnz(UU->ra[ii]));
	    receive_matrix(SYNCHRONOUS, UU->ra[ii], U->rind[ii], &FCU);
	    if(info != NULL)
	      PrecInfo_AddNNZ(info, CSnnz(UU->ra[ii]));
	    
	  }
    }

  /** Deallocate the communicators **/
  PhidalFactComm_Clean(&FCL);
  PhidalFactComm_Clean(&FCU);

#ifdef DEBUG_M
 {
   int global;
   global = 0;
   MPI_Allreduce(&toto, &global, EE(1), COMM_INT ,MPI_SUM, DBL->mpicom);
   if(DBL->proc_id == 0)
     fprintfv(5, stderr, "TOTAL number of contributions = %d \n", global);

 }
#endif


  Heap_Exit(&heap);
  free(wki1);
  free(wki2);
  free(wkd);

  if(jak != NULL)
    free(jak);
  if(rak != NULL)
    free(rak);
  if(rindk != NULL)
    free(rindk); 


  free(Lfirstcol);
  free(Ufirstrow);
  free(listindex);
  free(list1);
  free(list2);


  
#ifdef SYMMETRIC_DROP
  free(droptabtmp);
#endif

#ifdef DEBUG_M
  PhidalDistrMatrix_Check(L, DBL);
  PhidalDistrMatrix_Check(U, DBL);
#endif

}

/* @authors P. HENON */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>

#define PARALLEL

#include "phidal_parallel.h"


void PhidalDistrPrec_GEMM_ILUCT(int_t levelnum, REAL *droptab, PhidalDistrMatrix *G, PhidalDistrMatrix *W, PhidalDistrMatrix *A, PhidalDistrPrec *P, PhidalDistrHID *BL, PhidalOptions *option)
{
  PhidalDistrMatrix *VL, *VU;
  PhidalMatrix *M;
  chrono_t t1, t2;
  dim_t i;


#ifdef DEBUG_M
  assert(option->schur_method != 1);
  assert(A->M.tli == A->M.tlj);
  assert(A->M.bri == A->M.brj);
  assert(G->M.tli == A->M.tli);
  assert(G->M.bri == A->M.bri);
  assert(G->M.brj == A->M.tlj-1);
  assert(W->M.tlj == A->M.tlj);
  assert(W->M.brj == A->M.brj);
  assert(W->M.bri == A->M.tli-1);
  assert(levelnum == option->forwardlev);
#endif

  t1 = dwalltime();
  
  P->dim = A->M.dim1;
  P->symmetric = option->symmetric;
  P->schur_method = 2;
  P->forwardlev = 0;
  P->levelnum =  levelnum;

 
  /** IMPORTANT: Cut U before L **/
  VU = (PhidalDistrMatrix *)malloc(sizeof(PhidalDistrMatrix));
  PhidalDistrMatrix_Init(VU);
  PhidalDistrMatrix_Setup(G->M.tlj, G->M.tlj, A->M.bri, A->M.bri, "U", "N", option->locally_nbr, VU, BL);
  PhidalDistrMatrix_Cut(A, VU, BL);


  VL = (PhidalDistrMatrix *)malloc(sizeof(PhidalDistrMatrix));
  PhidalDistrMatrix_Init(VL);
  PhidalDistrMatrix_Setup(G->M.tlj, G->M.tlj, A->M.bri, A->M.bri, "L", "N", option->locally_nbr, VL, BL);
  PhidalDistrMatrix_Cut(A, VL, BL);
  /*** Reset the block diagonal to null matrices **/
  M = &(VL->M);
  for(i=A->M.tli;i<=A->M.brj;i++)
    reinitCS(M->ca[ M->cia[i]]);

#ifdef DEBUG_M
  assert(G->M.dim2+A->M.dim2 == VL->M.dim2);
  assert(W->M.dim1+A->M.dim1 == VL->M.dim2);
#endif
  PhidalDistrMatrix_Cut(G, VL, BL);
  PhidalDistrMatrix_Cut(W, VU, BL);

  PhidalDistrMatrix_ILUCT_Restrict(A->M.tli, 0, VL, VU, option->droptol1, droptab, option->fillrat, BL, option, P->info);

  M_MALLOC(P->LU, D(SCAL));
  PREC_L_SCAL(P) = (PhidalDistrMatrix *)malloc(sizeof(PhidalDistrMatrix));
  PhidalDistrMatrix_Setup(A->M.tli, A->M.tli, A->M.bri, A->M.bri, "L", "N", option->locally_nbr, PREC_L_SCAL(P), BL);
  PhidalDistrMatrix_Cut(VL, PREC_L_SCAL(P), BL);
  PREC_U_SCAL(P) = (PhidalDistrMatrix *)malloc(sizeof(PhidalDistrMatrix));
  PhidalDistrMatrix_Setup(A->M.tli, A->M.tli, A->M.bri, A->M.bri, "U", "N", option->locally_nbr, PREC_U_SCAL(P), BL);
  PhidalDistrMatrix_Cut(VU, PREC_U_SCAL(P), BL);

  /*** OIMBE attention retirer VL de mem si corrige pas peak dans GEMM_I..T **/
  if(P->info != NULL)
    {
      /*fprintfd(stderr, "VLnnz = %ld \n", (long)PhidalDistrMatrix_NNZ(VL)); == 0*/
      PrecInfo_SubNNZ(P->info, PhidalDistrMatrix_NNZ(VL));
      /*fprintfd(stderr, "VUnnz = %ld \n", (long)PhidalDistrMatrix_NNZ(VU)); == 0*/
      PrecInfo_SubNNZ(P->info, PhidalDistrMatrix_NNZ(VU));
    }
  PhidalDistrMatrix_Clean(VL);
  PhidalDistrMatrix_Clean(VU);
  free(VL);
  free(VU);

  t2 = dwalltime();
  fprintfv(5, stderr, "Proc %d: Time in GEMM_ILUCT = %g \n", BL->proc_id, t2-t1);
}

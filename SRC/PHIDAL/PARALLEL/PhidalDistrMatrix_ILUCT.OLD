/* @release_exclude */
/* @authors P. HENON */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>

#include "phidal_paralllel.h"



void PhidalDistrMatrix_ILUCT(PhidalDistrMatrix *L, PhidalDistrMatrix *U, double droptol, double *droptab, double fillrat, PhidalDistrHID *BL, PhidalOptions *option)
{
  /******************************************************************************************/
  /* This function computes the incomplete factorization A = L.U of a  matrix               */
  /* This is a similar function to PhidalMatrix_ILUTP (without pivoting)                    */
  /* BUT the block algorithm is made column wisely as in ILUC                               */
  /* On entry:                                                                              */
  /*   L is the lower triangular part of the matrix to factorize in CSR format              */
  /*   U is the upper triangular part of the matrix to factorize in CSR format              */
  /* on return:                                                                             */
  /* L the strictly lower triangular factor (unitary on the diagonal not stored) in CSR     */
  /* U the upper triangular factor (unitary on the diagonal not stored) in CSR              */
  /******************************************************************************************/
  int i, j, k;
  int ii, jj, kk;
  int *wki1, *wki2;
  double *wkd;
  csptr csL, csU;
  csptr *csrtab1;
  csptr *csrtab2;
  int nnb;
  Heap heap;
  double *droptabtmp;
  double *dropptr;


#ifdef DEBUG_M
  PhidalMatrix_Check(L, BL);
  PhidalMatrix_Check(U, BL);
#endif

  /*** Find the largest diagonal block in this matrix ****/
  ii = 0;
  for(i=L->tli;i<=L->bri;i++)
    if(BL->block_index[i+1]-BL->block_index[i] > ii)
      ii = BL->block_index[i+1]-BL->block_index[i];

  Heap_Init(&heap, ii);
  wki1= (int *)malloc(sizeof(int)*ii);
  wki2= (int *)malloc(sizeof(int)*ii);
  wkd = (double *)malloc(sizeof(double)*ii);

#ifdef SYMMETRIC_DROP
  droptabtmp = (double *)malloc(sizeof(double)*L->dim1);
  if(droptab != NULL)
    memcpy(droptabtmp, droptab, sizeof(double)*L->dim1);
  else
    for(i=0;i<L->dim1;i++)
      droptabtmp[i] = 1.0;
#else
  droptabtmp = droptab;
#endif
  
  csrtab1 = (csptr *)malloc(sizeof(csptr)* (L->bri-L->tli+1));
  csrtab2 = (csptr *)malloc(sizeof(csptr)* (L->bri-L->tli+1));

  for(k=L->tlj;k<=L->brj;k++)
    {
      /*** Factorize the column block k of L and the row k of U ***/

      csL =  L->ca[ L->cia[k] ];
      csU =  U->ra[ U->ria[k] ];
#ifdef DEBUG_M
      assert(L->cja[L->cia[k]] == k);
      assert(U->rja[U->ria[k]] == k);
#endif
      if(k>L->tlj)
	{
	  /**********************************************/
	  /* Compute the column-block L(k+1:n,k)        */
	  /**********************************************/
	  for(ii=L->cia[k]+1;ii<L->cia[k+1];ii++)
	    {
	      i = L->cja[ii];
#ifdef DEBUG_M
	      assert(L->ca[ii]->n == BL->block_index[i+1]-BL->block_index[i]);
	      CS_Check(L->ca[ii], BL->block_index[k+1]-BL->block_index[k]);
#endif

	      /*** Compute L(i,k) = L(i,k) - L(i, 0:k-1).L(k, 0:k-1)t **/	      
	      
	      /*** @@ TRIER LES PRODUITS LEADER ****/
	      CS_IntersectRow(L->tlj, k-1, 
			      L->ria[i+1]-L->ria[i], L->rja + L->ria[i], L->ra + L->ria[i], 
			      U->cia[k+1]-U->cia[k], U->cja + U->cia[k], U->ca + U->cia[k], 
			      &nnb, csrtab1, csrtab2);

	      /***** @@@ FAIRE LES LOCAUX ****/

	      /**** @@@ RECEVOIR LES BLOCK NON LEADER DE L'INTERSECTION ****/

	      if(nnb>0)
		CSRrowMultCSRcol(0.0, NULL, -1.0, nnb, -1.0, csrtab1, csrtab2, 
				 L->ca[ii], BL->block_index[k+1]-BL->block_index[k], wki1, wki2, wkd); /*** NO DROPPING HERE ***/

	      /**** @@ ENVOYER LE RESULTAT A BLOCK LEADER ****/
	      /***  @@ SE DEBARASSER DES NON LEADER DANS LA LIGNE i ***/
	    }

	  /**********************************************/
	  /* Compute the row-block U(k, k:n)            */
	  /**********************************************/
	  for(ii=U->ria[k];ii<U->ria[k+1];ii++)
	    {
	      i = U->rja[ii];
#ifdef DEBUG_M
	      assert(U->ra[ii]->n == BL->block_index[k+1]-BL->block_index[k]);
	      CS_Check(U->ra[ii], BL->block_index[i+1]-BL->block_index[i]);
#endif

	      /*** Compute L(i,k) = L(i,k) - L(i, 0:k-1).L(k, 0:k-1)t **/	      
	      CS_IntersectRow(L->tlj, k-1, 
			      L->ria[k+1]-L->ria[k], L->rja + L->ria[k], L->ra + L->ria[k], 
			      U->cia[i+1]-U->cia[i], U->cja + U->cia[i], U->ca + U->cia[i], 
			      &nnb, csrtab1, csrtab2);
	      if(nnb>0)
		CSRrowMultCSRcol(0.0, NULL, -1.0, nnb, -1.0, csrtab1, csrtab2, 
				 U->ra[ii], BL->block_index[i+1]-BL->block_index[i], wki1, wki2, wkd); /*** NO DROPPING HERE ***/
	    }
	  

	}

      /*** Factorize the diagonal block matrix A(k,k) ***/

      /*** @@@ SI LEADER FAIRE LA FACTO SINON RECEVOIR LE BLOC LEADER ***/
      kk = BL->block_index[k]-BL->block_index[L->tlj];
      if(droptab == NULL)
	CS_ILUT(csU, csL, csU, droptol, NULL, option->fillrat, &heap, wki1, wki2, wkd);
      else
	CS_ILUT(csU, csL, csU, droptol, droptab+kk, option->fillrat, &heap, wki1, wki2, wkd);

      /*** RESPECT THE ORDER ***/
    

      /*** @@@@ POUR LES BLOCK LOCAUX ****/
      /*** 1-- Divide the column block matrices of L by U ***/
      for(ii=L->cia[k]+1;ii<L->cia[k+1];ii++)
	{
	  i = L->cja[ii];
	  if(droptab == NULL)
	    CSR_CSR_InvUT(csU, L->ca[ii], droptol, NULL, fillrat, &heap, wki1, wki2, wkd);
	  else
	    CSR_CSR_InvUT(csU, L->ca[ii], droptol, droptab+kk, fillrat, &heap, wki1, wki2, wkd);
	}

#ifdef SYMMETRIC_DROP
#ifdef DEBUG_M
      assert(droptabtmp != NULL);
#endif
      dropptr = droptabtmp+kk;
      for(i=0;i<csU->n;i++)
	{
#ifdef DEBUG_M
	  assert(csU->ja[i][0] == i);
#endif
	  dropptr[i] /= fabs(csU->ma[i][0]);
	}
#endif

      /*** @@@@ POUR LES BLOCK LOCAUX ****/
      /*** 2-- Divide the row block matrices of U by L ***/
      for(ii=U->ria[k]+1;ii<U->ria[k+1];ii++)
	{
	  i = U->rja[ii];
	  if(droptabtmp != NULL)
	    CSR_CSR_InvLT(csL, U->ra[ii],  BL->block_index[i+1]-BL->block_index[i], droptol, droptabtmp+kk, fillrat, wki1, wki2, wkd);
	  else
	    CSR_CSR_InvLT(csL, U->ra[ii],  BL->block_index[i+1]-BL->block_index[i], droptol, NULL, fillrat, wki1, wki2, wkd);
	}
    }

  Heap_Exit(&heap);
  free(wki1);
  free(wki2);
  free(wkd);
  free(csrtab1);
  free(csrtab2);

#ifdef SYMMETRIC_DROP
  free(droptabtmp);
#endif

#ifdef DEBUG_M
  PhidalMatrix_Check(L, BL);
#endif

}

/* @authors P. HENON */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>
#include "phidal_parallel.h"


/*#define VEC_ALLSHARE*/


void  PhidalDistrVec_Reduce(flag_t op, COEF *y, PhidalDistrMatrix *DM, PhidalDistrHID *DBL)
{
  /******************************************************/
  /* vector y is gathered on the leader row processor   */
  /* op = 0 : value with MAX modulus is retained        */
  /******************************************************/

  dim_t i;
#ifdef VEC_ALLSHARE
  int j;
#endif
  dim_t offseti, offsetj;
  COEF *yptr;
  dim_t *bind;
  PhidalMatrix *M;
  PhidalHID *BL;
  PhidalCommVec *commvec;
  mpi_t proc_id;

  if(DBL->nproc == 1)
    return;

  BL = &DBL->LHID;
  proc_id = DM->proc_id;
  M = &(DM->M);


  /** Init the commvec in order it does not take into account zero block **/
  /** It is the same problem than in PhidalDistrMatrix_ColNorm2          **/
  /** OIMBE A revoir si usage intensif car couteux  */
  /*commvec = &(DM->commvec);*/
  commvec = (PhidalCommVec *)malloc(sizeof(PhidalCommVec));
  PhidalCommVec_Init(commvec);
  PhidalCommVec_Setup(4, DM, commvec, DBL);

  /** Start the persistent receive requests **/
  for(i=M->bri;i>=M->tli;i--)
    {
#ifdef DEBUG_M
      if(commvec->out_ctrb[i-M->tli] > 0)
	assert(DBL->row_leader[i] == proc_id);
      if(commvec->out_ctrb[i-M->tli] == -1)
	assert(DBL->row_leader[i] != proc_id);
#endif

      if(commvec->out_ctrb[i-M->tli] > 0)
	if(MPI_Startall(commvec->out_ctrb[i-M->tli], commvec->recv_rqtab[i-M->tli]) != MPI_SUCCESS)
	  {
	    fprintfd(stderr, "Error in start recv contrib for vec %d \n", i);
	    assert(0);
	  }

#ifdef VEC_ALLSHARE
      if(commvec->out_ctrb[i-M->tli] == -1)
	if(MPI_Start(commvec->recv_rqtab[i-M->tli]) != MPI_SUCCESS)
	  {
	    fprintfd(stderr, "Error in start recv vector for vec %d \n", i);
	    assert(0);
	  }
#endif
    }

  bind = BL->block_index;

  offseti = bind[M->tli];
  offsetj = bind[M->tlj];

  for(i=M->bri;i>=M->tli;i--)
    {
      yptr = y+(bind[i]-offseti);
      if(DBL->row_leader[ i ] != proc_id)
	MPI_Send(yptr, CC(bind[i+1]-bind[i]), MPI_COEF_TYPE, DBL->row_leader[i], DBL->loc2glob_blocknum[i], DBL->mpicom); 
    }
  
  /*** Receive the contributions where the processor is the owner of the facing diagonal block ***/
  for(i=M->bri;i>=M->tli;i--)
    {
      if(commvec->out_ctrb[ i-M->tli ] > 0)
	{
	  yptr = y+(bind[i]-offseti);
	  /*fprintfd(stderr, "Proc %d receiveVecAdd for vec %d \n", DM->proc_id, i);*/
	  PhidalCommVec_ReceiveVecAdd(op, i-M->tli, yptr, commvec); 
	}
      
#ifdef VEC_ALLSHARE
	if(DBL->row_leader[i] == proc_id && DBL->block_psetindex[i+1]-DBL->block_psetindex[i]>1)
	  {
	    /** Send the vector to row non-leader processor **/
	    for(j=DBL->block_psetindex[i];j<DBL->block_psetindex[i+1];j++)
	      if(DBL->block_pset[j] != DBL->proc_id)
		MPI_Send(yptr, CC(bind[i+1]-bind[i]), MPI_COEF_TYPE, DBL->block_pset[j],DBL->loc2glob_blocknum[i], DBL->mpicom);
	  }
#endif
    }
  
  /*** Receive the vector y parts where the processor is not the
       row_leader **/
#ifdef VEC_ALLSHARE
    for(i=M->bri;i>=M->tli;i--)
      if(commvec->out_ctrb[ i-M->tli ] == -1)
	{
#ifdef DEBUG_M
	  assert(commvec->status != NULL);
#endif
	  if(MPI_Wait(commvec->recv_rqtab[i-M->tli], commvec->status) != MPI_SUCCESS)
	    {
	      fprintfd(stderr, "Error in PhidalDistrMatrix_Matvec for vector %d \n" ,i);
	      assert(0);
	    }
	yptr = y+(BL->block_index[i]-offseti);
	memcpy(yptr, commvec->t + (BL->block_index[i]-offseti), sizeof(COEF)* commvec->veclen[i-M->tli]);
      }
#endif


    
    /** Reinit the commvec **/
    PhidalCommVec_Clean(commvec);
    free(commvec);
}

void PhidalDistrVec_Gather(mpi_t proc_root, COEF *y, COEF *yloc, PhidalDistrHID *DBL)
{
  dim_t i;
  dim_t *iperm=NULL;
  mpi_t *dpls=NULL, *eedpls=NULL, *recvsize=NULL, *eerecvsize=NULL; 
  dim_t *liperm;
  COEF *yyloc;
  mpi_t eeln, ln, n;
  PhidalHID *BL;
  mpi_t proc_id;
  COEF *ytmp;

  proc_id = DBL->proc_id;
  BL = &DBL->LHID;
  n = DBL->globn;

  if(DBL->nproc == 1)
    {
      for(i=0;i<n;i++)
	y[DBL->loc2orig[i]] = yloc[i];
      return;
    }
  
  /** Gather on the root the solution **/
  if(proc_id == proc_root)
    {
      iperm = (dim_t *)malloc(sizeof(dim_t)*n);
      dpls = (mpi_t *)malloc(sizeof(mpi_t)*DBL->nproc);
      recvsize = (mpi_t *)malloc(sizeof(mpi_t)*DBL->nproc);
      eedpls = (mpi_t *)malloc(sizeof(mpi_t)*DBL->nproc);
      eerecvsize = (mpi_t *)malloc(sizeof(mpi_t)*DBL->nproc);
    }

  ln = 0;
  for(i=0;i<BL->nblock;i++)
    if(DBL->row_leader[i] == proc_id)
      ln += BL->block_index[i+1]-BL->block_index[i];

  liperm = (dim_t *)malloc(sizeof(dim_t)*ln);
  yyloc = (COEF *)malloc(sizeof(COEF)*ln);

  eeln = EE(ln);
  MPI_Gather(&eeln, EE(1), COMM_INT, recvsize, EE(1), COMM_INT, proc_root, DBL->mpicom);
  /*MPI_Gather(&ln, 1, MPI_INT, recvsize, 1, MPI_INT, proc_root, DBL->mpicom);*/
#ifdef DEBUG_M
  if(proc_id == proc_root)
    {
      dim_t sum;
      sum = 0;
      for(i=0;i<DBL->nproc;i++)
	sum += recvsize[i];
      assert(sum == EE(n));
      /**assert(sum == n);**/
    }
#endif
  if(proc_id == proc_root)
    {
      for(i=DBL->nproc-1;i>0;i--)
	dpls[i] = recvsize[i-1];
      dpls[0] = 0;
      
      for(i=1;i<DBL->nproc;i++)
	dpls[i] += dpls[i-1];
    }

  ln = 0;
  for(i=0;i<BL->nblock;i++)
    if(DBL->row_leader[i] == proc_id)
      {
	memcpy(liperm + ln, DBL->loc2orig + BL->block_index[i], sizeof(dim_t)*(BL->block_index[i+1]-BL->block_index[i]));
	memcpy(yyloc + ln, yloc + BL->block_index[i], sizeof(COEF)*(BL->block_index[i+1]-BL->block_index[i]));
	ln +=  BL->block_index[i+1]-BL->block_index[i];
      }
	       
  /** Gather the ordering at root **/
  if(proc_id == proc_root)
    for(i=0;i<DBL->nproc;i++)
      {
	eedpls[i] = EE(dpls[i]);
	eerecvsize[i] = EE(recvsize[i]);
      }
  
  /*if(proc_id == proc_root)
    for(i=0;i<DBL->nproc;i++)
      {
	fprintfd(stderr, "dpls[%ld] = %ld recv[%ld] = %ld \n", (long)i, (long)dpls[i], (long)i, (long)recvsize[i]);
	}*/


  MPI_Gatherv(liperm, eeln, COMM_INT, iperm, eerecvsize, eedpls, COMM_INT, proc_root, DBL->mpicom);
  /*MPI_Gatherv(liperm, ln, MPI_INT, iperm, recvsize, dpls, MPI_INT, proc_root, DBL->mpicom);*/
  
  if(proc_id == proc_root)
    {
      free(eedpls);
      free(eerecvsize);
    }
  free(liperm);

#ifdef DEBUG_M
  if(proc_id == proc_root)
    {
      dim_t *iiperm ;
      iiperm = (dim_t *)malloc(sizeof(dim_t)*n);
      bzero(iiperm, sizeof(dim_t)*n);
      for(i=0;i<n;i++)
	{
	  assert(iiperm[iperm[i]] == 0);
	  iiperm[iperm[i]] = 1;
	}
      free(iiperm);
    }
#endif

  
  MPI_Gatherv(yyloc, CC(ln), MPI_COEF_TYPE, y, recvsize, dpls, MPI_COEF_TYPE, proc_root, DBL->mpicom);
  free(yyloc);
  
  if(proc_id == proc_root)
    {
      free(dpls);
      free(recvsize);

      ytmp = (COEF *)malloc(sizeof(COEF)*n);

      memcpy(ytmp, y, sizeof(COEF)*n);
      for(i=0;i<n;i++)
	y[iperm[i]] = ytmp[i];
      
      free(ytmp);
      free(iperm);
    }

  

}


/*
void PhidalDistrVec_Scatter(mpi_t proc_root, COEF *y, mpi_t proc_id, COEF *yloc, PhidalDistrMatrix *DM, PhidalDistrHID *DBL)
{
  PhidalMatrix *M;
  PhidalHID *BL;
  PhidalCommVec *commvec;
  dim_t offseti,
  mpi_t proc_id;
  COEF *yptr;
  dim_t *bind;
  dim_t i;

  BL = &DBL->LHID;
  proc_id = DM->proc_id;
  M = &(DM->M);
  commvec = &(DM->commvec);
  
  if(DBL->nproc == 1)
    {
      if(y != yloc)
	memcpy(yloc, y, sizeof(COEF)*A->dim1);
      return;
    }

#ifdef DEBUG_M
  assert(commvec->init == 1);
#endif 

  bind = BL->block_index;
  offseti = bind[M->tli];

  if(proc_id == proc_root)
    for(i=M->tli;i<=M->bri;i++)
      {
	yptr = y+(bind[i]-offseti);
	if(DBL->row_leader[ i ] != proc_id)
	  MPI_Send(yptr, CC(bind[i+1]-bind[i]), MPI_COEF_TYPE, DBL->row_leader[i], DBL->loc2glob_blocknum[i], DBL->mpicom); 
      }

      }*/

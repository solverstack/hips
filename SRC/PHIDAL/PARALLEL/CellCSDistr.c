/* @authors P. HENON */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <assert.h>
#include <math.h>
#include "phidal_parallel.h"


/** Local function **/
void CellCSDistr_ListUpdate(CellCSDistr *celltab, int num,  int n,  dim_t *ja)
{
  dim_t i;
  CellCSDistr *c;

  for(i=0;i<n;i++)
    {
      c = celltab+ja[i];

      while(c->nnz > 0)
	if( *(c->ja) < num || c->ma[0]->nnzr == 0 ) 
	  {
	    c->nnz--;
	    c->ja++;
	    c->pind++;
	    c->ma++;
	  }
	else
	  break;
    }
}


void CellCSDistr_ListCopy(CellCSDistr *src, CellCSDistr *dest,  int n,  dim_t *ja)
{
  dim_t i;
  for(i=0;i<n;i++)
    dest[ja[i]] = src[ja[i]];
}



void CellCSDistr_GetCSList(int num, CellCSDistr *celltab, int n,  dim_t *ja, csptr *ma, int *nnb, csptr *csrtab1, csptr *csrtab2)
{
  int i, ind;
  CellCSDistr *c;

  ind = 0;
   for(i=0;i<n;i++)
    {
      c = celltab+ja[i];
      
      while(c->nnz > 0)
	if( *(c->ja) < num  || c->ma[0]->nnzr == 0)
	  {
	    c->nnz--;
	    c->ja++;
	    c->pind++;
	    c->ma++;
	  }
	else
	  break;
      
      if( *(c->ja) == num)
	{
	  csrtab1[ind] = c->ma[0];
	  csrtab2[ind] = ma[i];
	  ind++;
	}
    }
   *nnb = ind;
}

void CellCSDistr_IntersectList(mpi_t proc_id, CellCSDistr *celltab, 
			       int ncol,  int *colja, int *colpind, csptr *colma,  INTL *pset_index1, mpi_t *pset1,
			       int nrow, int *rowja, INTL *pset_index2, mpi_t *pset2,
			       int *nnztab, csptr *listA, csptr *listB)
{

  int jj, ii, i, j;
  mpi_t *ps1, *ps2;
  INTL k1, k2, l1, l2;
  int leader;

  csptr cmat;
  int nnz;
  dim_t *ja;
  INTL *pind;
  csptr *ma;
  csptr **listAtab, **listBtab;

  if(nrow == 0 || ncol == 0)
    return;

  /***** Find the size of all the rowlist *****/
  listAtab = (csptr **)malloc(sizeof(csptr *)*nrow);
  listBtab = (csptr **)malloc(sizeof(csptr *)*nrow);

  for(i=0;i<nrow;i++)
    {
      listAtab[i] = listA+i*ncol;
      listBtab[i] = listB+i*ncol;
    }

  bzero(nnztab, sizeof(int)*nrow);

  for(jj=0;jj<ncol;jj++)
    {
      cmat = colma[jj];
      j = colja[jj];
      nnz = celltab[j].nnz;
      ja = celltab[j].ja;
      pind = celltab[j].pind;
      ma = celltab[j].ma;
      k1 = colpind[jj];
      ii = 0;
      i = 0;
      while(ii<nnz && i <nrow)
	{
	  if(ja[ii] < rowja[i])
	    {
	      ii++;
	      continue;
	    }

	  if(ja[ii] > rowja[i])
	    {
	      i++;
	      continue;
	    }

	  

	  /** ja[ii] == ja[i] **/
	  if(ma[ii]->nnzr > 0)
	    {
	      k2 = pind[ii];
	      
	      if(k1 != -1 && k2 != -1)
		{
		  ps1 = pset1 + pset_index1[k1];
		  ps2 = pset2 + pset_index2[k2];
		  l1 = pset_index1[k1+1] - pset_index1[k1];
		  l2 = pset_index2[k2+1] - pset_index2[k2];

		  leader = choose_leader(l1, ps1, l2, ps2);
		}
	      else
		leader = proc_id;

	      if(proc_id == leader)/** @@ OIMBE load balance temporaire **/ 
		{
		  listAtab[i][nnztab[i]] = ma[ii];
		  listBtab[i][nnztab[i]] = cmat;
		  nnztab[i]++;
		}
	    }
	  ii++;
	  i++;
	}
    }

  free(listAtab);
  free(listBtab);
}



int choose_leader(int l1, mpi_t *pset1, int l2, mpi_t *pset2)
{
  /*** TEMPORARY: return the minimun element of the intersection 
       of two processor set ****/
  
  int k1, k2;
  
#ifdef DEBUG_M
  /** Check pset1 and pset2 are sorted **/
  for(k1=1;k1<l1;k1++)
    assert(pset1[k1] > pset1[k1-1]);
  
  for(k2=1;k2<l2;k2++)
    assert(pset2[k2] > pset2[k2-1]);
#endif

#define GOOD
#ifdef GOOD
  k1 = 0;
  k2 = 0;
  while(k1<l1 && k2 <l2)
    {
      if(pset1[k1] < pset2[k2])
	{
	  k1++;
	  continue;
	}

      if(pset1[k1] > pset2[k2])
	{
	  k2++;
	  continue;
	} 

      /** pset1[k1] == pset2[k2] **/
      return pset1[k1];
      
    }
#else
  k1 = l1-1;
  k2 = l2-1;
  while(k1>=0 && k2 >=0)
    {
      if(pset1[k1] < pset2[k2])
	{
	  k2--;
	  continue;
	}

      if(pset1[k1] > pset2[k2])
	{
	  k1--;
	  continue;
	} 

      /** pset1[k1] == pset2[k2] **/
      return pset1[k1];
      
    }
#endif

#ifdef DEGUG_M
  assert(0); /** if this point is reached that means the intersection
		 is void: that should not happen **/
#endif
  return -1;
}

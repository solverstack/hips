/* @authors P. HENON */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>
#include <mpi.h>

#define PARALLEL

#include "phidal_parallel.h"


void PhidalDistrHID_Init(PhidalDistrHID *DBL)
{
  PhidalHID_Init(&(DBL->LHID));
  DBL->nproc = 0;
  DBL->globn = 0;
  DBL->gnblock = 0;
  DBL->block_psetindex = NULL;
  DBL->block_pset = NULL;
  DBL->orig2loc = NULL;
  DBL->loc2orig = NULL;
  DBL->loc2glob_blocknum = NULL;
  DBL->glob2loc_blocknum = NULL;
  DBL->dom2proc = NULL;
  DBL->row_leader = NULL;
  DBL->adjpnbr = 0;
  DBL->adjptab = NULL;
  DBL->mpicom = MPI_COMM_WORLD;
 

}

void PhidalDistrHID_Clean(PhidalDistrHID *DBL)
{

  if( DBL->block_psetindex != NULL)
    free( DBL->block_psetindex);
  if( DBL->block_pset != NULL)
    free( DBL->block_pset);
  if(DBL->orig2loc != NULL)
    free(DBL->orig2loc);
  if(DBL->loc2orig != NULL)
    free(DBL->loc2orig);
  if(DBL->loc2glob_blocknum != NULL)
    free( DBL->loc2glob_blocknum);
  if(DBL->glob2loc_blocknum != NULL)
    free( DBL->glob2loc_blocknum);
  if(DBL->dom2proc != NULL)
    free( DBL->dom2proc);
  if(DBL->row_leader != NULL)
    free(DBL->row_leader);
  if(DBL->adjptab != NULL)
    free(DBL->adjptab);
  PhidalHID_Clean(&(DBL->LHID));  
}

void PhidalDistrMatrix_Init(PhidalDistrMatrix *a)
{
#ifdef DEBUG_M
  assert(a!=NULL);
#endif
  /** Parallel **/

  a->proc_id = 0;
  a->nproc = 0;
  a->clead = NULL;
  a->cind = NULL;
  a->rlead = NULL;

  a->rind = NULL;
  a->pset_index = NULL;
  a->pset = NULL;
  a->sharenbr = 0;
  PhidalMatrix_Init(&a->M);
  /* init commvec */
  PhidalCommVec_Init(&a->commvec);

}

void PhidalDistrMatrix_Clean(PhidalDistrMatrix *a)
{
#ifdef DEBUG_M
  assert(a!=NULL);
#endif
  PhidalMatrix_Clean(&(a->M));
  if(a->clead != NULL)
    free(a->clead);
  if(a->cind != NULL)
    free(a->cind);
  if(a->rlead != NULL)
    free(a->rlead);
  if(a->rind != NULL)
    free(a->rind);
  if(a->pset != NULL)
    free(a->pset);
  if(a->pset_index != NULL)
    free(a->pset_index);
  a->sharenbr = 0;
  
  PhidalCommVec_Clean(&a->commvec);

}

void PhidalDistrPrec_Init(PhidalDistrPrec *p)
{
  p->symmetric = 0;
  p->dim = 0;
  p->levelnum = 0;
  p->forwardlev = 0;

  p->D = NULL;


  /* PREC_B(p) = NULL; */
  /*p->C = NULL;  OPTIMREC forward */
  p->LU = NULL; 
  p->EF = NULL; 
  p->S = NULL;  
  p->B = NULL;
  /*M_MALLOC(p->LU, D(SCAL));
  M_MALLOC(p->EF, D(SCAL));
  M_MALLOC(p->S,  D(SCAL));
  M_MALLOC(p->B,  D(SCAL));

  PREC_L_SCAL(p) = NULL;
  PREC_U_SCAL(p) = NULL;
  PREC_E(p) = NULL;
  PREC_F(p) = NULL;
  PREC_B(p) = NULL;*/

  p->prevprec = NULL;
  p->nextprec = NULL;

  p->schur_method = 0;
  p->pivoting = 0;
  p->permtab = NULL;


/*   p->tol_schur = 1; */
  /*p->info = NULL;*/
}

void PhidalDistrPrec_Clean(PhidalDistrPrec *p)
{
#ifdef DEBUG_M
  if(p->forwardlev > 0)
    assert(p->nextprec != NULL);
  else
    assert(p->nextprec == NULL);
#endif

 
  if(p->forwardlev  > 0)
    {
      if(p->nextprec != NULL)
	{
	  PhidalDistrPrec_Clean(p->nextprec);
	  free(p->nextprec);
	}
      
      if(p->EF != NULL) {
	if(PREC_E(p) != NULL)
	  {
	    PhidalCommVec_Clean(&PREC_E(p)->commvec);
	    PhidalDistrMatrix_Clean(PREC_E(p));
	    free(PREC_E(p));
	  }
	
	if(PREC_F(p) != NULL)
	  {
	    PhidalCommVec_Clean(&PREC_F(p)->commvec);
	    PhidalDistrMatrix_Clean(PREC_F(p));
	    free(PREC_F(p));
	  }
      }

      if(p->S != NULL) {  
	if(PREC_S_SCAL(p) != NULL)
	  {
	    if(p->schur_method == 1)
	      {
		PhidalCommVec_Clean(&PREC_S_SCAL(p)->commvec);
		PhidalDistrMatrix_Clean(PREC_S_SCAL(p));
	      }
	    
	    free(PREC_S_SCAL(p));
	  }

      }

      if (p->B != NULL) {
	if(PREC_B(p) != NULL)
	  {
	    PhidalCommVec_Clean(&PREC_B(p)->commvec);
#warning TODO
	    /* PhidalDistrMatrix_Clean(PREC_B(p)); */
	    /* free(PREC_B(p)); */
	  }
	/* M_CLEAN(PREC_B(p)); */
      }
      
      /*if(p->C != NULL) OPTIMREC Forward 
	{
	PhidalDistrMatrix_Clean(p->C);
	free(p->C);
	}*/
    }
  
  if (p->LU != NULL) {
    if(PREC_L_SCAL(p) != NULL)
      {
	PhidalCommVec_Clean(&PREC_L_SCAL(p)->commvec);
	PhidalDistrMatrix_Clean(PREC_L_SCAL(p));
	free(PREC_L_SCAL(p));
      }
  
    if(PREC_U_SCAL(p) != NULL)
      {
	PhidalCommVec_Clean(&PREC_U_SCAL(p)->commvec);
	PhidalDistrMatrix_Clean(PREC_U_SCAL(p));
	free(PREC_U_SCAL(p));
      }
    M_CLEAN(p->LU);
  }

  
  if(p->EF != NULL)
    M_CLEAN(p->EF);

  if(p->S != NULL)
    M_CLEAN(p->S);
  if(p->B != NULL)
    M_CLEAN(p->B);

  if(p->symmetric == 1 && p->D != NULL)
    free(p->D);

  if(p->pivoting == 1)
    free(p->permtab);


  p->symmetric = 0;
  p->dim = 0;
  p->levelnum = 0;
  p->forwardlev = 0;
  p->schur_method = 0;


  p->pivoting = 0;
  /*   p->tol_schur = 1; */

  /*if (p->info != NULL) {
    free(p->info);
    PrecInfo_Clean(p->info);
    }*/
}

void PhidalDistrPrec_Fake(PhidalDistrPrec* p, PhidalDistrMatrix* L, PhidalDistrMatrix* U, COEF* D, int rsa, PhidalOptions* option) {
/*   p->L = L; */
/*   p->U = U; */
/*   p->D = D; */
    
/*   p->symmetric = rsa;  */
/*   p->dim = L->M.dim1; /\* A->dim1 *\/ */
/*   p->levelnum = 0; */
/*   p->forwardlev = 0; */
  
/*   p->EF=NULL; */
/* /\*   p->F=NULL; *\/ */
/*   p->S=NULL; */
/*   PREC_B(p)=NULL; */
/*   p->prevprec=NULL; */
/*   p->nextprec=NULL; */
/*   p->schur_method = option->schur_method; */
/*   p->pivoting = option->pivoting; */
/*   p->permtab = NULL; /\* todo *\/ */
/* /\*   p->tol_schur = 1;  /\\* todo *\\/ *\/ */
  exit(1);
}

void PhidalCommVec_Init(PhidalCommVec *commvec)
{
  commvec->init = 0;
  commvec->rqnbr = 0;
  commvec->veclen = NULL;
  commvec->recv_rqtab = NULL;
  commvec->t = NULL;
  commvec->recv_buffer = NULL;
  commvec->loc_ctrb = NULL;
  commvec->loc_cnt = NULL;
  commvec->status = NULL;
  commvec->out_ctrb = NULL;
  commvec->out_proctab = NULL;
  commvec->out_cnt = NULL;
  commvec->mpicom = MPI_COMM_WORLD;
  commvec->posted_max = VEC_POSTED_MAX;
  commvec->posted = 0;
  commvec->bufmem_max = VEC_BUFMEM_MAX; 
  commvec->bufmem = 0;
}

void PhidalCommVec_Clean(PhidalCommVec *commvec)
{
  dim_t i, j;

  if(commvec->init != 1)
    return;

  
  if(commvec->veclen != NULL)
    free(commvec->veclen);

  for(i=0;i<commvec->rqnbr;i++)
    {
      if(commvec->out_ctrb[i] > 0)
	{
	  for(j=0;j<commvec->out_ctrb[i];j++)
	    MPI_Request_free(commvec->recv_rqtab[i]+j);
	  free(commvec->recv_rqtab[i]);
	  free(commvec->recv_buffer[i]);
	  free(commvec->out_proctab[i]);
	}
      if(commvec->out_ctrb[i] == -1)
	{
	  MPI_Request_free(commvec->recv_rqtab[i]);
	  free(commvec->recv_rqtab[i]);
	}
    }
  
  if(commvec->recv_rqtab != NULL)
    free(commvec->recv_rqtab);

  if(commvec->recv_buffer != NULL)
    free(commvec->recv_buffer);

  

  if(commvec->loc_ctrb != NULL)
    free(commvec->loc_ctrb);
  if(commvec->loc_cnt != NULL)
    free(commvec->loc_cnt);
  
  if(commvec->t != NULL)
    free(commvec->t);
  

  if(commvec->status != NULL)
    free(commvec->status);
  
  if(commvec->out_ctrb != NULL)
    free(commvec->out_ctrb);
  if(commvec->out_proctab != NULL)
    free(commvec->out_proctab);
  if(commvec->out_cnt != NULL)
    free(commvec->out_cnt);

  commvec->init = 0;
}



void BlockComm_Init(BlockComm *b, int srcnbr)
{
#ifdef DEBUG_M
  assert(srcnbr > 0);
#endif
  b->posted = 0;
  b->rqnbr = srcnbr;
  b->proctab = (int *)malloc(sizeof(int)*srcnbr);
  b->irqtab = (MPI_Request *)malloc(sizeof(MPI_Request)*srcnbr);
  b->crqtab = (MPI_Request *)malloc(sizeof(MPI_Request)*srcnbr);
  b->ibufftab = (int **)malloc(sizeof(int *)*srcnbr);
  b->cbufftab = (COEF **)malloc(sizeof(COEF *)*srcnbr);
}


void BlockComm_Clean(BlockComm *b)
{
/*   dim_t i; */
/*   MPI_Status status; */

  free(b->proctab);
  free(b->irqtab);
  free(b->crqtab);
  free(b->ibufftab);
  free(b->cbufftab);
}

void PhidalFactComm_Init(PhidalFactComm *FC)
{
  FC->bcomtab = NULL;
  FC->proc_id = -1;
  FC->bcomnbr = 0;
  FC->maxproc = 0;
  FC->cstab = NULL;
  FC->tmpj = NULL;
  FC->tmpa = NULL;
  FC->jrev = NULL;
  FC->comm_memratio = 1.0;
  FC->status = NULL;
  FC->mpicom = MPI_COMM_WORLD;
  
  FC->posted_max = FACT_POSTED_MAX;
  FC->posted = 0;
  FC->bufmem_max = FACT_BUFMEM_MAX; 
  FC->bufmem = 0;
  FC->rowind = 0;
  FC->colind = 0;
}


void PhidalFactComm_Clean(PhidalFactComm *FC)
{
  dim_t k;
  csptr t;


  for(k=0;k<FC->bcomnbr;k++)
    BlockComm_Clean(FC->bcomtab+k);

  if(FC->bcomnbr > 0)
    free(FC->bcomtab);

  if(FC->status != NULL)
    free(FC->status);

  /** Set the matrices used to unpack to null **/      
  for(k=0;k<FC->maxproc;k++)
    {
      t = FC->cstab+k;
      bzero(t->nnzrow, sizeof(int)*t->n);
      bzero(t->ja, sizeof(int *)*t->n);
      bzero(t->ma, sizeof(COEF *)*t->n);
      CS_SetNonZeroRow(FC->cstab+k);
      cleanCS(FC->cstab+k);
    }
  if(FC->cstab) free(FC->cstab);
  if(FC->jrev)  free(FC->jrev);
  if(FC->tmpj)  free(FC->tmpj);
  if(FC->tmpa)  free(FC->tmpa);

  FC->bcomnbr = 0;
}

#ifdef OLDPRECINFO
void PrecInfo_Max(PrecInfo * info, REAL MnnzA, REAL *ratio_max,
		  REAL *peakratio_max, MPI_Comm comm)
{
    REAL ratio, peakratio;
    if (info == NULL)
	return;

    ratio = (REAL) info->nnzP / MnnzA;
    peakratio = (REAL) info->peak / MnnzA;


    MPI_Reduce(&ratio, ratio_max, 1, MPI_REAL_TYPE, MPI_MAX, 0, comm);
    MPI_Reduce(&peakratio, peakratio_max, 1, MPI_REAL_TYPE, MPI_MAX, 0, comm);
}
#endif

void PrecInfo_Max(PrecInfo * info, REAL MnnzA, REAL *ratio_max,
		  REAL *peakratio_max, MPI_Comm comm)
{
  INTL fill_loc, peak_loc;
  INTL fill_max, peak_max;

  if (info == NULL)
    return;

  fill_loc = info->nnzP;
  peak_loc = info->peak;
   
  MPI_Reduce(&fill_loc, &fill_max, FF(1), COMM_BIG_INT, MPI_MAX, 0, comm);
  MPI_Reduce(&peak_loc, &peak_max, FF(1), COMM_BIG_INT, MPI_MAX, 0, comm);

  if (ratio_max != 0) { /* proc_id == 0*/ assert(peakratio_max != 0);
    *ratio_max     = (REAL) fill_max / MnnzA;
    *peakratio_max = (REAL) peak_max / MnnzA;
  }
}


void PrecInfo_MaxThese(PrecInfo * info, int nproc, REAL *ratio_max_ref1,
		       REAL *peakratio_max_ref1, MPI_Comm comm)
{
  INTL fill_loc, peak_loc;
  INTL fill_max, peak_max;
  INTL fill_sum, peak_sum;

  if (info == NULL)
    return;

  fill_loc = info->nnzP;
  peak_loc = info->peak;

  MPI_Reduce(&fill_loc, &fill_max, FF(1), COMM_BIG_INT, MPI_MAX, 0, comm);
  MPI_Reduce(&peak_loc, &peak_max, FF(1), COMM_BIG_INT, MPI_MAX, 0, comm);

  MPI_Reduce(&fill_loc, &fill_sum, FF(1), COMM_BIG_INT, MPI_SUM, 0, comm);
  MPI_Reduce(&peak_loc, &peak_sum, FF(1), COMM_BIG_INT, MPI_SUM, 0, comm);

  if (ratio_max_ref1 != 0) { /* proc_id == 0*/ assert(peakratio_max_ref1 != 0);
    /*printf("TEST1 : %ld %ld %d\n", (long)fill_max, (long)fill_sum, nproc);*/

    *ratio_max_ref1     = (REAL) fill_max / (fill_sum/nproc);
    *peakratio_max_ref1 = (REAL) peak_max / (peak_sum/nproc);
  }
}

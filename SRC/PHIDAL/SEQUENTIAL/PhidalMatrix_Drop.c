/* @authors P. HENON */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <assert.h>
#include <math.h>

#include "phidal_sequential.h"

#ifdef OLD
/* Just drop the small entry in a "phidal col" of a symmetric PhidalMatrix */
void PhidalMatrix_DropTCol_Sym(int j, PhidalMatrix *A, REAL droptol, REAL *dropptr)
{  
  dim_t k;

  for(k=A->cia[j];k<A->cia[j+1];k++)
    if(A->cja[k] != j) /** Not a diagonal matrix **/ 	 
      CS_DropT(A->ca[k], droptol, dropptr); 	 
    else 	 
      CS_DropT_SaveDiag(A->ca[k], droptol, dropptr);
}

/* Just drop the small entry in a "phidal col" of a unsymmetric PhidalMatrix */
/* TODO : a factoriser en passant en paramètre ria ou cia ... */
void PhidalMatrix_DropTCol_Unsym(int i, PhidalMatrix *A, REAL droptol, REAL *dropptr)
{  
  dim_t k;

  for(k=A->ria[i];k<A->ria[i+1];k++)
    if(A->rja[k] != i) /** Not a diagonal matrix **/ 	 
      CS_DropT(A->ra[k], droptol, dropptr); 	 
    else 	 
      CS_DropT_SaveDiag(A->ra[k], droptol, dropptr);
 
}
#endif

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <assert.h>
#include <math.h>

#include "phidal_sequential.h"



void PhidalMatrix_DropT(PhidalMatrix *A, REAL droptol, REAL *droptab, PhidalHID *BL)
{
  /*****************************************************************/
  /* This function drop the small entry in a phidal matrix         */
  /* The function dropps by row when he matrix is not symmetric    */
  /* and by column when it is symmetric (only lower triangular part*/
  /* is stored                                                     */
  /* all entry in row i <= droptol*droptab[i] are dropped          */
  /* NOTE: the diagonal terms are never dropped                    */
  /*****************************************************************/
  dim_t i, j, k;
  REAL *dropptr;

  if(A->symmetric == 0)
    {
      assert(A->csc == 0);
      for(i=A->tli;i<=A->bri;i++)
	{
	  if(droptab != NULL)
	    dropptr = droptab + (BL->block_index[i]- BL->block_index[A->tli]);
	  else
	    dropptr = NULL;

	  for(k=A->ria[i];k<A->ria[i+1];k++)
	    if(A->rja[k] != i) /** Not a diagonal matrix **/
	      CS_DropT(A->ra[k], droptol, dropptr);
	    else
	      CS_DropT_SaveDiag(A->ra[k], droptol, dropptr);
	}
    }
  else
    {
      assert(A->csc == 1);
      for(j=A->tlj;j<=A->brj;j++)
	{
	  if(droptab != NULL)
	    dropptr = droptab + (BL->block_index[j]- BL->block_index[A->tlj]);
	  else
	    dropptr = NULL;
	  
	  for(k=A->cia[j];k<A->cia[j+1];k++)
	    if(A->cja[k] != j) /** Not a diagonal matrix **/
	      CS_DropT(A->ca[k], droptol, dropptr);
	    else
	      CS_DropT_SaveDiag(A->ca[k], droptol, dropptr);
	}
    }

  
}

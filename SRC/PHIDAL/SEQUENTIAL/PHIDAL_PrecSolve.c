/* @authors P. HENON */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>

#include "phidal_sequential.h"

/** Local functions **/
INTS PhidalPrec_SolveForward(REAL tol, int_t itmax, PhidalPrec *P, COEF *x, COEF *b, PhidalHID *BL, PhidalOptions *option, dim_t *itertab, REAL *resnormtab);

INTS PHIDAL_PrecSolve(REAL tol, PhidalPrec *P, COEF *x, COEF *b, PhidalHID *BL, PhidalOptions *option, dim_t *itertab, REAL *resnormtab)
{
  PhidalMatrix *L, *U;
  int_t itmax;
  dim_t i;
  COEF *D;

  L = PREC_L_SCAL(P);
  U = PREC_U_SCAL(P);
  D = P->D;
 
  if(P->forwardlev == 0)
    {

#if defined(ILUT_WHOLE_SCHUR) || defined(ILUT_SOLVE)  

      if(P->symmetric == 1)
	{
	  CSC_Lsol(1, P->csL, b, x); 
	  for(i=0;i<P->csL->n;i++)
	    x[i] *= D[i];
	}
      else
	CSR_Lsol(1, P->csL, b, x); 
#else
      PhidalMatrix_Lsolve(1, L, x, b, BL);
      if(P->symmetric == 1)
	for(i=0;i<L->dim1;i++)
	  x[i] *= D[i];
#endif 
      /*for(i=0;i<P->dim;i++)
	fprintfv(5, stderr, "D[%d] = %g ;", i, D[i]);
	fprintfv(5, stderr, "\n");*/

      /*for(i=0;i<P->dim;i++)
	fprintfv(5, stderr, "b[%d] = %g ;", i, b[i]);
	fprintfv(5, stderr, "\n");
	for(i=0;i<P->dim;i++)
 	fprintfv(5, stderr, "x[%d] = %g ;", i, x[i]);
	fprintfv(5, stderr, "\n");*/


#if defined(ILUT_WHOLE_SCHUR) || defined(ILUT_SOLVE)
      if(P->symmetric == 1)
	CSC_Ltsol(1, P->csL, x, x); 
      else
	CSR_Usol(0, P->csU, x, x); 
#else
      if(P->symmetric == 1)
	PhidalMatrix_Usolve(1, U, x, x, BL); /** Unitary diagonale **/
      else
	PhidalMatrix_Usolve(0, U, x, x, BL);
#endif 

      if(P->pivoting == 1)
	/** permute back x **/
	VecInvPermute(L->tli, L->bri, x, P->permtab, BL);
    } else {

      itmax = option->itmaxforw;
      for(i=P->forwardlev; i<option->forwardlev;i++)
	itmax = (int)(itmax*option->itforwrat);

      CHECK_RETURN(PhidalPrec_SolveForward(tol, itmax, P, x, b, BL, option, itertab, resnormtab));
      if(P->pivoting == 1)
	/** permute back x **/
	VecInvPermute(L->tli, L->bri, x, P->permtab, BL);
    }
  return HIPS_SUCCESS;
}


INTS PhidalPrec_SolveForward(REAL tol, int_t itmax, PhidalPrec *P, COEF *x, COEF *b, PhidalHID *BL, PhidalOptions *option, dim_t *itertab, REAL *resnormtab)
{
  PhidalMatrix *L, *U;
  PhidalMatrix *S, *E, *F;
  /*PhidalMatrix *C;*/
  COEF *y;
  COEF *D;

  REAL tolrat;

  dim_t i;
  dim_t g;

  L = PREC_L_SCAL(P);
  E = PREC_E(P);
  U = PREC_U_SCAL(P);
  F = PREC_F(P);
  D = P->D;
  S = PREC_S_SCAL(P);


  y = (COEF *)malloc(sizeof(COEF)*P->dim);
  memcpy(y, b, sizeof(COEF)*P->dim);
      
  /*** Restriction operation ***/
  PhidalMatrix_Lsolve(1, L, x, b, BL);
  
  if(P->symmetric == 1)
    for(i=0;i<L->dim1;i++)
      x[i] *= D[i];

  if(P->symmetric == 1)
    PhidalMatrix_Usolve(1, U, x, x, BL); /** unitary diagonale **/
  else
    PhidalMatrix_Usolve(0, U, x, x, BL);

  PHIDAL_MatVecSub(E, BL, x, y+L->dim1);
 
#ifdef DEBUG_M
  if(P->schur_method == 1)
    assert(S->dim1 == BL->n - BL->block_index[BL->block_levelindex[P->levelnum]]-L->dim1);
#endif


  tolrat = norm2(b, P->dim) / norm2(y+L->dim1, P->dim - PREC_L_SCAL(P)->dim1);
    

  if(option->schur_method == 0 || itmax <= 1)
    {
      CHECK_RETURN(PHIDAL_PrecSolve(tol*tolrat, P->nextprec, x+L->dim1, y+L->dim1, BL, option, itertab, resnormtab));
    }
  else
    {
      int iter=-1;
      flag_t verbose = (option->verbose >= 4)?2:0;
      
      if(option->krylov_method != 1)
	{
	  if(P->schur_method == 2)
	    {
	      CHECK_RETURN(HIPS_Fgmresd_PH(verbose, tol*tolrat, itmax, P->nextprec, BL, option, y+L->dim1, x+L->dim1, stdout, itertab, resnormtab));
	    }
	  else
	    {
	      CHECK_RETURN(HIPS_Fgmresd_PH_PH(verbose, tol*tolrat, itmax, S, P->nextprec, BL, option, y+L->dim1, x+L->dim1, stdout, itertab, resnormtab));
	    }
	}
      else
	{
	  if(P->schur_method == 2)
	    {
	      CHECK_RETURN(HIPS_PCG_PH(verbose, tol*tolrat, itmax, P->nextprec, BL, option, y+L->dim1, x+L->dim1, stdout, itertab, resnormtab));
	    }
	  else
	    {
	      CHECK_RETURN(HIPS_PCG_PH_PH(verbose, tol*tolrat, itmax, S, P->nextprec, BL, option, y+L->dim1, x+L->dim1, stdout, itertab, resnormtab));
	    }
	}
      
      /* if(option->verbose >= 3) */
	{
	  for(g=0;g<P->levelnum+1;g++)
	    fprintfv(1, stdout, "    ");
	  fprintfv(1, stdout, "Level %ld : inner iterations = %ld \n", (long)(P->levelnum+1),(long)iter);
	}

    }

  /*** Prolongation operation ***/
  PHIDAL_MatVecSub(F, BL, x+L->dim1, y);
  PhidalMatrix_Lsolve(1, L, x, y, BL);

  if(P->symmetric == 1)
    for(i=0;i<L->dim1;i++)
      x[i] *= D[i];

  if(P->symmetric == 1)
    PhidalMatrix_Usolve(1, U, x, x, BL); /** unitary diagonale **/
  else
    PhidalMatrix_Usolve(0, U, x, x, BL);

  free(y);
  
  return HIPS_SUCCESS;
}


void VecInvPermute(dim_t tli, dim_t bri, COEF *x, dim_t *permtab, PhidalHID *BL)
{
  COEF *y, *xptr, *yptr;
  int *pptr;
  dim_t i, k;
  int s, e, ss;
  int size;

  s = BL->block_index[tli];
  e = BL->block_index[bri+1]-1;

  y = (COEF *)malloc(sizeof(COEF)*( e-s+1));
  memcpy(y, x, sizeof(COEF)*(e-s+1));
  for(k=tli;k<=bri;k++)
    {
      ss = BL->block_index[k]-s;
      xptr = x + ss;
      yptr = y + ss;
      pptr = permtab + ss;

      size = BL->block_index[k+1] - BL->block_index[k];
      for(i=0;i<size;i++)
	xptr[i] = yptr[pptr[i]];
    }
  free(y);
}

/* @authors P. HENON */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "phidal_sequential.h"




void PhidalMatrix_Build(flag_t symmetric, char *UPLO, csptr A, PhidalMatrix *M, PhidalHID *BL)
{
  /****************************************************************************/
  /* This function build a phidal matrix from a csr matrix                    */
  /* On entry:                                                                */
  /*   symmetric : if =1 the matrix M will be a CSC lower triangular          */
  /*                Phidal Matrix                                             */
  /*   UPLO: "L": build the lower block triangular matrix  from a             */
  /*         "U": build the upper block triangular matrix from a              */
  /*         "N": build the whole matrix                                      */
  /*   This parameter is usefull for symmetric matrix you need only one half  */
  /*   a   : the matrix you want to transform into PHIDAL matrix              */
  /*   BL  : the phidal hierarchical decomposition structure                  */
  /* On return:                                                               */
  /*   m   : the matrix a in PHIDAL form                                      */
  /****************************************************************************/


  if(symmetric == 1)
    {
      if(strcmp(UPLO, "U") == 0)
	UPLO = "L";
      else
	{
	  if(strcmp(UPLO, "L") == 0)
	    UPLO = "U";
	  else
	    {
	      fprintfd(stderr, "PhidalMatrix_Build Error : if symmetric == 1 then UPLO must be L or U \n");
	      exit(-1);
	    }
	}
      PhidalMatrix_Setup(0, 0, BL->nblock-1, BL->nblock-1, UPLO, "N", 0, M, BL);

      PhidalMatrix_CsrCopy(0, A, "U", M, BL);


      PhidalMatrix_Transpose(M);
      M->symmetric = 1;
      M->csc = 1;

    }
  else
    {
      PhidalMatrix_Setup(0, 0, BL->nblock-1, BL->nblock-1, UPLO, "N", 0, M, BL);
      M->symmetric = 0;
      M->csc = 0;
      PhidalMatrix_CsrCopy(0, A, "N", M, BL);
    }
  

#ifdef DEBUG_M
  PhidalMatrix_Check(M, BL);
#endif

}




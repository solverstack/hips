/* @authors P. HENON */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>

#include "phidal_sequential.h"



void PHIDAL_MatrixBuild(csptr A, PhidalMatrix *M, PhidalHID *BL, PhidalOptions *option)
{
  if(option->symmetric == 0) {
    PhidalMatrix_Build(0, "N", A, M, BL);
  }
  else {
    PhidalMatrix_Build(1, "L", A, M, BL);
  }
}

void PHIDAL_Precond(PhidalMatrix *A, PhidalPrec *P, PhidalHID *BL, PhidalOptions *option)
{
#ifdef DEBUG_M
  assert(A->symmetric == option->symmetric);
#endif
  
  /** Correct the options **/
  PhidalOptions_Fix(option, BL);

  /** Print the options **/
  if(option->verbose > 0)
    PhidalOptions_Print(stdout, option);

  if(A->symmetric == 0)
    PHIDAL_MLILUTPrec(A, P, BL, option);
  else
    PHIDAL_MLICCTPrec(A, P, BL, option);

}


INTS PHIDAL_Solve( PhidalMatrix *A, PhidalPrec *P, PhidalHID *BL, PhidalOptions *option, COEF *rhs, COEF * x, dim_t *itertab, REAL *resnormtab)
{

  /** There is no flexible PCG so the first level is always
      fgmres **/

  if(option->krylov_method != 1 || (option->forwardlev > 0 && option->schur_method != 0))
    {
      CHECK_RETURN(HIPS_Fgmresd_PH_PH(option->verbose, option->tol, option->itmax, A, P, BL, option, rhs, x, option->fd, itertab, resnormtab));
    }
  else
    {
      CHECK_RETURN(HIPS_PCG_PH_PH(option->verbose, option->tol, option->itmax, A, P, BL, option, rhs, x, option->fd, itertab, resnormtab));
    }
  
  /* if(option->verbose > 0) */
  if(itertab != NULL)
    fprintfv(1, stdout, "Number of outer iterations %d \n", itertab[0]);

  return HIPS_SUCCESS;
}


/* @authors P. HENON */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>
/*#include "psparslib.h"
#include "heads.h"
#include "mlbd_struct.h"*/

#include "phidal_sequential.h"

/*** @@ OIMBE : write a good interface for a PHIDAL_TRSM ***/


void PHIDAL_InvLT(PhidalMatrix *L, PhidalMatrix *M, REAL droptol, REAL *droptab, REAL fillrat, PhidalHID *BL, int *wki1, int *wki2, COEF *wkd)
{
  /*********************************************************************/
  /* this function does M = L^-1.M                                     */
  /* with a dropping criterion drotol                                  */
  /* L is a lower triangular PhidalMatrix                              */
  /* M is a PhidalMatrix                                               */ 
  /* The block pattern of M must be the same as C                      */
  /*********************************************************************/
  int i, j, ii, kk;
  int mj;
  int nnb;
  csptr *csrtab1;
  csptr *csrtab2;
  csptr *list1, *list2;
  int *listindex;
  int nk, *jak;
  csptr *rak;
  csptr csL;
  CellCS *Mfirstrow;

#ifdef DEBUG_M
  assert(L->tli == L->tlj);
  assert(L->tli == M->tli);
  assert(L->bri == M->bri);
#endif


  ii = 0;
  for(j=L->tli;j<=L->bri;j++)
    if(L->ria[j+1] - L->ria[j]-1 > ii)
      ii = L->ria[j+1] - L->ria[j]-1;
  if(ii>0)
    {
      jak = (int *)malloc(sizeof(int)*ii);
      rak = (csptr *)malloc(sizeof(csptr)*ii);
    }
  else
    {
      jak = NULL;
      rak = NULL;
    }

  ii=0;
  for(i=L->tli;i<=L->bri;i++)
    {
      nnb = (L->ria[i+1]-L->ria[i] - 1) * (M->ria[i+1]-M->ria[i]);
      if( nnb > ii)
	ii = nnb;
    }
  
  list1 = (csptr *)malloc(sizeof(csptr)* (ii+1));
  list2 = (csptr *)malloc(sizeof(csptr)* (ii+1));
  listindex = (int *)malloc(sizeof(int)*(M->brj-M->tlj+1));
  Mfirstrow = (CellCS *)malloc(sizeof(CellCS)*(M->bri+1));
  for(i=M->tli;i<=M->bri;i++)
    {
      Mfirstrow[i].nnz = M->ria[i+1]-M->ria[i];
      Mfirstrow[i].ja  = M->rja + M->ria[i];
      Mfirstrow[i].ma  = M->ra + M->ria[i];
    }


  /** Inversion of each row block of M **/
  for(i=M->tli;i<=M->bri;i++)
    {
      
      nk = 0;
      for(ii=L->ria[i];ii<L->ria[i+1]-1;ii++)
	if(L->ra[ii]->nnzr > 0)
	  {
	    jak[nk] = L->rja[ii];
	    rak[nk] = L->ra[ii];
	    nk++;
	  }

      if(nk>0 && i>M->tli)
	{
	  CellCS_IntersectList(Mfirstrow, nk,  jak, rak, M->ria[i+1]-M->ria[i], M->rja+M->ria[i], 
			       listindex, list1, list2);
	  for(ii=0;ii<M->ria[i+1]-M->ria[i];ii++)
	    {
	      j = M->rja[ii+M->ria[i]];
	      
	      /*** Compute M(i,j) = M(i,j) - L(i, 0:i-1).M(j, 0:j-1) **/	      
	      /*** NO DROPPING HERE ***/
	      csrtab2 = list1 + ii*nk;
	      csrtab1 = list2 + ii*nk;
	      nnb     = listindex[ii];
	      if(nnb>0)
		CSRrowMultCSRcol(0.0, NULL, -1.0, nnb, -1.0, csrtab1, csrtab2, 
				 M->ra[ii+M->ria[i]], BL->block_index[j+1]-BL->block_index[j], 
				 wki1, wki2, wkd); 
	    }
	}
      

      
      /*** Divide the row block i matrices by the diagonal factor ***/
#ifdef DEBUG_M
      assert(L->cja[ L->cia[i] ] == i);
#endif
      csL = L->ca[ L->cia[i] ];
      kk = BL->block_index[i]-BL->block_index[L->tli];
      for(ii=M->ria[i];ii<M->ria[i+1];ii++)
	{
	  if(M->ra[ii]->nnzr == 0)
	    continue;
	  /** Mij = Lii^-1.Mij **/
	  j = M->rja[ii];
	  mj = BL->block_index[j+1]-BL->block_index[j];
	  if(droptab != NULL)
	    CSR_CSR_InvLT( csL, M->ra[ii], mj,  droptol, droptab+kk, fillrat, wki1, wki2, wkd);
	  else
	    CSR_CSR_InvLT( csL, M->ra[ii], mj,  droptol, NULL, fillrat, wki1, wki2, wkd);
	}
    }

  if(jak != NULL)
    free(jak);
  if(rak != NULL)
    free(rak);
  free(listindex);
  free(list1);
  free(list2);
  free(Mfirstrow);
}


void PHIDAL_InvUT(PhidalMatrix *U, PhidalMatrix *M, REAL droptol, REAL *droptab, REAL fillrat, PhidalHID *BL, Heap *heap, int *wki1, int *wki2, COEF *wkd)
{
  /*********************************************************************/
  /* this function does M = M.U^-1                                     */
  /* with a dropping criterion drotol                                  */
  /* L is a lower triangular PhidalMatrix                              */
  /* M is a PhidalMatrix                                               */ 
  /*********************************************************************/
 

  int i, j, jj, kk;
  int nnb;
  csptr *csrtab1;
  csptr *csrtab2;
  csptr *list1, *list2;
  int *listindex;
  int nk, *jak;
  csptr *rak;
  csptr csU;
  CellCS *Mfirstcol;

#ifdef DEBUG_M
  assert(U->tli == U->tlj);
  assert(U->tlj == M->tlj);
  assert(U->brj == M->brj);
#endif

  jj = 0;
  for(j=U->tlj;j<=U->brj;j++)
    if(U->cia[j+1] - U->cia[j]-1 > jj)
      jj = U->cia[j+1] - U->cia[j]-1;
  if(jj>0)
    {
      jak = (int *)malloc(sizeof(int)*jj);
      rak = (csptr *)malloc(sizeof(csptr)*jj);
    }
  else
    {
      jak = NULL;
      rak = NULL;
    }

  jj=0;
  for(j=U->tlj;j<=U->brj;j++)
    {
      nnb = (U->cia[j+1]-U->cia[j] - 1) * (M->cia[j+1]-M->cia[j]);
      if( nnb > jj)
	jj = nnb;
    }
  
  list1 = (csptr *)malloc(sizeof(csptr)* (jj+1));
  list2 = (csptr *)malloc(sizeof(csptr)* (jj+1));
  listindex = (int *)malloc(sizeof(int)*(M->bri-M->tli+1));
  Mfirstcol = (CellCS *)malloc(sizeof(CellCS)*(M->brj+1));
  for(j=M->tlj;j<=M->brj;j++)
    {
      Mfirstcol[j].nnz = M->cia[j+1]-M->cia[j];
      Mfirstcol[j].ja  = M->cja + M->cia[j];
      Mfirstcol[j].ma  = M->ca + M->cia[j];
    }


  /** Inversion of each block column of M **/
  for(j=M->tlj;j<=M->brj;j++)
    {
      
      nk = 0;
      for(jj=U->cia[j];jj<U->cia[j+1]-1;jj++)
	if(U->ca[jj]->nnzr > 0)
	  {
	    jak[nk] = U->cja[jj];
	    rak[nk] = U->ca[jj];
	    nk++;
	  }
      
      if(nk>0 && j>M->tlj)
	{
	  CellCS_IntersectList(Mfirstcol, nk,  jak, rak, M->cia[j+1]-M->cia[j], M->cja+M->cia[j], 
			       listindex, list1, list2);

	  kk = BL->block_index[j+1]-BL->block_index[j];
	  for(jj=0;jj<M->cia[j+1]-M->cia[j];jj++)
	    {
	      i = M->cja[jj+M->cia[j]];
	      
	      /*** Compute M(i,j) = M(i,j) - L(i, 0:i-1).M(j, 0:j-1) **/	      
	      /*** NO DROPPING HERE ***/
	      csrtab1 = list1 + jj*nk;
	      csrtab2 = list2 + jj*nk;
	      nnb     = listindex[jj];
	      if(nnb>0)
		CSRrowMultCSRcol(0.0, NULL, -1.0, nnb, -1.0, csrtab1, csrtab2, 
				 M->ca[jj+M->cia[j]], kk, 
				 wki1, wki2, wkd); 
	    }
	}
      
      
      
      /*** Divide the row block i matrices by the diagonal factor ***/
#ifdef DEBUG_M
      assert(U->rja[ U->ria[j] ] == j);
#endif
      csU = U->ra[ U->ria[j] ];
      kk = BL->block_index[j]-BL->block_index[U->tlj];
      for(jj=M->cia[j];jj<M->cia[j+1];jj++)
	{
	  if(M->ca[jj]->nnzr == 0)
	    continue;

	  /** Mij = Mij.Ujj^-1**/
	  if(droptab != NULL)
	    CSR_CSR_InvUT( csU, M->ca[jj], droptol, droptab+kk, fillrat, heap, wki1, wki2, wkd);
	  else
	    CSR_CSR_InvUT( csU, M->ca[jj], droptol, NULL, fillrat, heap, wki1, wki2, wkd);

	}
    }

  if(jak != NULL)
    free(jak);
  if(rak != NULL)
    free(rak);
  free(listindex);
  free(list1);
  free(list2);
  free(Mfirstcol);

}




void PHIDAL_ICCT_InvLT(PhidalMatrix *L, PhidalMatrix *M, REAL droptol, REAL *droptab, REAL fillrat, PhidalHID *BL, int *wki1, int *wki2, COEF *wkd)
{
  /*********************************************************************/
  /* this function does Mt = L^-1.Mt                                   */
  /* with a dropping criterion drotol                                  */
  /* L is a lower triangular PhidalMatrix (block matrices are in CSC   */
  /* M is a PhidalMatrix  (blockmatrices are in CSC)                   */ 
  /* The block pattern of M must be the same as C                      */
  /*********************************************************************/
  int i, j, ii, kk;
  int nnb;
  csptr csL;
  csptr *csrtab1;
  csptr *csrtab2;
  csptr *list1, *list2;
  int *listindex;
  int nk, *jak;
  csptr *rak;
  CellCS *firstcol;
  cell_int *celltab;
  cell_int **cellptrtab;


#ifdef DEBUG_M
  assert(L->tli == L->tlj);
  assert(L->tlj == M->tlj);
  assert(L->brj == M->brj);
#endif

  ii = 0;
  for(j=L->tli;j<=L->bri;j++)
    if(L->ria[j+1] - L->ria[j]-1 > ii)
      ii = L->ria[j+1] - L->ria[j]-1;
  if(ii>0)
    {
      jak = (int *)malloc(sizeof(int)*ii);
      rak = (csptr *)malloc(sizeof(csptr)*ii);
    }
  else
    {
      jak = NULL;
      rak = NULL;
    }

  
  /*** Find the maximum row cumulate block size ****/
  nk = 0;
  for(i=L->tli;i<=L->bri;i++)
    {
      ii = 0;
      for(j=L->ria[i];j<L->ria[i+1];j++)
	ii+= L->ra[j]->n;
      if(ii > nk)
	nk = ii;
    }
  for(i=M->tli;i<=M->bri;i++)
    {
      ii = 0;
      for(j=M->ria[i];j<M->ria[i+1];j++)
	ii+= M->ra[j]->n;
      if(ii > nk)
	nk = ii;
    }

  if(nk>0)
    celltab = (cell_int *)malloc(sizeof(cell_int)*nk);
  else
    celltab = NULL;

  /** Find the maximum row dimension of a block **/
  nk = 0;
  for(i=L->tli;i<=L->bri;i++)
    {
      ii = BL->block_index[i+1]-BL->block_index[i];
      if(ii>nk)
	nk = ii;
    }
  cellptrtab = (cell_int **)malloc(sizeof(cell_int *)*nk);



  ii=0;
  for(j=L->tlj;j<=L->brj;j++)
    {
      nnb = (L->ria[j+1]-L->ria[j] - 1) * (M->cia[j+1]-M->cia[j]);
      if( nnb > ii)
	ii = nnb;
    }
  
  list1 = (csptr *)malloc(sizeof(csptr)* (ii+1));
  list2 = (csptr *)malloc(sizeof(csptr)* (ii+1));
  listindex = (int *)malloc(sizeof(int)*(M->bri-M->tli+1));
  firstcol = (CellCS *)malloc(sizeof(CellCS)*(M->brj+1));
  for(j=M->tlj;j<=M->brj;j++)
    {
      firstcol[j].nnz = M->cia[j+1]-M->cia[j];
      firstcol[j].ja  = M->cja + M->cia[j];
      firstcol[j].ma  = M->ca + M->cia[j];
    }

  /** Inversion of each block column of M **/
  for(j=M->tlj;j<=M->brj;j++)
    {
      
      nk = 0;
      for(ii=L->ria[j];ii<L->ria[j+1]-1;ii++)
	if(L->ra[ii]->nnzr > 0)
	  {
	    jak[nk] = L->rja[ii];
	    rak[nk] = L->ra[ii];
	    nk++;
	  }

      if(nk>0 && j>M->tlj)
	{
	  CellCS_IntersectList(firstcol, nk,  jak, rak, M->cia[j+1]-M->cia[j], M->cja+M->cia[j], 
			       listindex, list1, list2);
	  for(ii=0;ii<M->cia[j+1]-M->cia[j];ii++)
	    {
	      i = M->cja[ii+M->cia[j]];
	      
	      /*** Compute M(i,j) = M(i,j) - L(j, 0:j-1).M(i, 0:j-1)t **/	      
	      /*** NO DROPPING HERE ***/
	      csrtab1 = list1 + ii*nk;
	      csrtab2 = list2 + ii*nk;
	      nnb     = listindex[ii];
	      if(nnb>0)
		CSCrowMultCSRcol(0.0, NULL, -1.0, nnb, -1.0, csrtab1, csrtab2, 
				 M->ca[ii+M->cia[j]], BL->block_index[i+1]-BL->block_index[i], 
				 wki1, wki2, wkd, celltab, cellptrtab); 
	      
	    }
	}
      

      
      /*** Divide the column block matrices by the diagonal factor ***/
      csL = L->ca[L->cia[j]];
      kk = BL->block_index[j]-BL->block_index[L->tlj];
      for(ii=M->cia[j];ii<M->cia[j+1];ii++)
	{
	  i = M->cja[ii];
	  if(droptab != NULL)
	    CSC_CSR_InvLT(csL, M->ca[ii],  BL->block_index[i+1]-BL->block_index[i], droptol, droptab+kk, fillrat, 
			  wki1, wki2, wkd, celltab, cellptrtab);
	  else
	    CSC_CSR_InvLT(csL, M->ca[ii],  BL->block_index[i+1]-BL->block_index[i], droptol, NULL, fillrat, 
			  wki1, wki2, wkd, celltab, cellptrtab);
	}

    }

  if(jak != NULL)
    free(jak);
  if(rak != NULL)
    free(rak);
  
  if(celltab != NULL)
    free(celltab);
  if(cellptrtab != NULL)
    free(cellptrtab);

  free(listindex);
  free(list1);
  free(list2);
  free(firstcol);
}

/* @authors P. HENON */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>

#include "phidal_sequential.h"

void PHIDAL_ICCT(flag_t job, PhidalMatrix *A, PhidalMatrix *L, COEF *D, REAL droptol, REAL *droptab, REAL fillrat, PhidalHID *BL, PhidalOptions *option, PrecInfo *info)
{
  /**********************************************************************/
  /* This function computes an ICCT factorization of the matrix A.     */
  /* Fill-in in the factor is only permit in the phidal pattern         */
  /* If job == 0 the matrix A is left unchanged                         */
  /* If job == 1 the factorization is done in place (A is a void matrix */
  /* when returned                                                      */
  /**********************************************************************/
  if(A->tli != A->tlj || A->bri != A->brj)
    {
      fprintfv(5, stderr, "Error in PHIDAL_ICCT : Matrix A is not a square matrix \n");
      exit(-1);
    }

  /** 2- Copy the strictly lower triangular part of A into L **/
  PhidalMatrix_Setup(A->tli, A->tlj, A->bri, A->brj, "L", "N", option->locally_nbr, L, BL);
  if(job == 0)
    {
      PhidalMatrix_Copy(A, L, BL);
      if(info != NULL)
	PrecInfo_AddNNZ(info, PhidalMatrix_NNZ(L));
    }
  else
    PhidalMatrix_Cut(A, L, BL);

  PhidalMatrix_ICCT(L, D, droptol, droptab, fillrat, BL, option, info);


}



/* @authors P. HENON */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>

#include "phidal_sequential.h"

void PhidalMatrix_ICCT(PhidalMatrix *L, COEF *D, REAL droptol, REAL *droptab, REAL fillrat, PhidalHID *BL, PhidalOptions *option, PrecInfo *info)
{
  /******************************************************************************************/
  /* This function computes the incomplete factorization A = L.D.Lt of a symmetric matrix   */
  /* On entry:                                                                              */
  /*   L is the lower triangular part of the matrix to factorize in CSC format              */
  /* on return:                                                                             */
  /* L the strictly lower triangular factor (unitary on the diagonal not stored) in CSC     */
  /* D is the INVERSE of the diagonal factor                                                */                                                                     
  /* NOTE: L is sorted by row index on return                                               */
  /******************************************************************************************/
#ifdef DEBUG_M
  assert(L->symmetric == 1);
#endif

#ifdef OPTIM_ILUT
  
#else
  PhidalMatrix_ICCT_Restrict(L->tli, L, D, droptol, droptab, fillrat, BL, option, info);
#endif
}

void PhidalMatrix_ICCT_Restrict(int START, PhidalMatrix *L, COEF *D, REAL droptol, REAL *droptab, REAL fillrat, PhidalHID *BL, PhidalOptions *option, PrecInfo *info)
{
  dim_t i, j, k; 
  int nk, *jak;
  csptr *rak;
  int ii, jj, kk;
  int *wki1, *wki2;
  COEF *wkd;
  csptr csL;
  csptr *csrtab1, *list1;
  csptr *csrtab2, *list2;
  int *listindex;
  REAL *droptabtmp;
  COEF *D2;
  int nnb;
  CellCS *firstcol;
  cell_int *celltab;
  cell_int **cellptrtab;


  /* { */
/*     char filename[100]; */
/*     sprintf(filename, "ph.txt"); */
    
/*     FILE* stream = fopen(filename, "w"); */
/*     dumpPhidalMatrix(stream, L); */
/*     fclose(stream); */
/*   } */


#ifdef DEBUG_M
  assert(START >= L->tli);
  assert(START <= L->bri);
#endif

  /*** Find the largest diagonal block in this matrix ****/
  ii = 0;
  /*for(i=L->tli;i<=L->bri;i++)*/
  for(i=START;i<=L->bri;i++)
    if(BL->block_index[i+1]-BL->block_index[i] > ii)
      ii = BL->block_index[i+1]-BL->block_index[i];
  wkd = (COEF *)malloc(sizeof(COEF)*ii);
  /*wki1= (int *)malloc(sizeof(int)*ii);
    wki2= (int *)malloc(sizeof(int)*ii);*/
  /** Need that for CSCrowMultCSRcol (see RowCompact) **/
  wki1= (int *)malloc(sizeof(int)*MAX(BL->nblock, ii)); 
  wki2= (int *)malloc(sizeof(int)*L->dim1);


  /*** Find the maximum row cumulate block size ****/
  nk = 0;
  /*for(i=L->tli;i<=L->bri;i++)*/
  for(i=START;i<=L->bri;i++)
    {
      ii = 0;
      for(j=L->ria[i];j<L->ria[i+1];j++)
	ii+= L->ra[j]->n;
      if(ii > nk)
	nk = ii;
    }
  
  if(nk>0)
    celltab = (cell_int *)malloc(sizeof(cell_int)*nk);
  else
    celltab = NULL;

  /** Find the maximum row dimension of a block **/
  nk = 0;
  for(i=START;i<=L->bri;i++)
    {
      ii = BL->block_index[i+1]-BL->block_index[i];
      if(ii>nk)
	nk = ii;
    }
  cellptrtab = (cell_int **)malloc(sizeof(cell_int *)*nk);

  ii = 0;
  for(k=START;k<=L->bri;k++)
    if(L->ria[k+1] - L->ria[k]-1 > ii)
      ii = L->ria[k+1] - L->ria[k]-1;
  if(ii>0)
    {
      jak = (int *)malloc(sizeof(int)*ii);
      rak = (csptr *)malloc(sizeof(csptr)*ii);
    }
  else
    {
      jak = NULL;
      rak = NULL;
    }

  ii=0;
  for(k=START;k<=L->brj;k++)
    {
      nnb = (L->ria[k+1]-L->ria[k] - 1) * (L->cia[k+1]-L->cia[k]);
      if( nnb > ii)
	ii = nnb;
    }

  list1 = (csptr *)malloc(sizeof(csptr)* (ii+1));
  list2 = (csptr *)malloc(sizeof(csptr)* (ii+1));
  /*listindex = (int *)malloc(sizeof(int)*(L->bri-L->tli+1));*/
  listindex = (int *)malloc(sizeof(int)*(L->bri-START+1));


  firstcol = (CellCS *)malloc(sizeof(CellCS)*(L->brj+1));
  for(k=L->tlj;k<=L->brj;k++)
    {
      firstcol[k].nnz = L->cia[k+1]-L->cia[k];
      firstcol[k].ja  = L->cja + L->cia[k];
      firstcol[k].ma  = L->ca + L->cia[k];
    }


  D2 = (COEF *)malloc(sizeof(COEF)*L->dim1);
  
  droptabtmp = (REAL *)malloc(sizeof(REAL)*L->dim1);
  if(droptab != NULL)
    memcpy(droptabtmp, droptab, sizeof(REAL)*L->dim1);
  else
    for(i=0;i<L->dim1;i++)
      droptabtmp[i] = 1.0;

  /*for(k=L->tlj;k<=L->brj;k++)*/
  for(k=START;k<=L->brj;k++)
    {

      /*** Factorize the column block k ***/
      nk = 0;
      for(jj=L->ria[k];jj<L->ria[k+1]-1;jj++) /* TODO : a incorporer dans la boucle !! */
	if(L->ra[jj]->nnzr > 0)
	  {
	    jak[nk] = L->rja[jj];
	    rak[nk] = L->ra[jj];
	    nk++;
	  }
      
      csL =  L->ca[ L->cia[k] ];

      if(nk > 0 && k>L->tlj )
	{
	  /*** Compute L(k,k) = L(k,k) - L(k, 0:k-1).D-1.L(k, 0:k-1)t **/
	  nnb = 0;
	  ii = 0;

	  /*** Compute the dropping tab ***/
	  for(jj=0;jj<nk;jj++)
	    {
	      j = jak[jj];
	      memcpy(D2+ii, D+BL->block_index[j]-BL->block_index[L->tlj], 
		     sizeof(COEF)* (BL->block_index[j+1]-BL->block_index[j]));
	      ii += BL->block_index[j+1]-BL->block_index[j];
	    } 
	  if(info != NULL)
	    PrecInfo_SubNNZ(info, CSnnz(csL));
	  CSCrowICCprod(0.0, NULL, -1.0, nk, rak, csL, D2, wki1, wki2, wkd, celltab, cellptrtab);  /*** NO DROPPING HERE ***/
	  if(info != NULL)
	    PrecInfo_AddNNZ(info, CSnnz(csL));

	  /***********************/
	  /* COLUMN-WISE SEARCH  */
	  /***********************/
	  CellCS_ListUpdate(firstcol, k+1,  nk,  jak);
	  CellCS_IntersectList(firstcol, nk,  jak, rak, L->cia[k+1]-L->cia[k]-1, L->cja+L->cia[k]+1, 
			       listindex, list1, list2);

	  for(ii=0;ii<L->cia[k+1]-L->cia[k]-1;ii++)
	    {
	      i = L->cja[ii+L->cia[k]+1];
	      
	      /*** Compute L(i,k) = L(i,k) - L(i, 0:k-1).L(k, 0:k-1)t **/	      
	      /*** NO DROPPING HERE ***/
	      csrtab1 = list1 + ii*nk;
	      csrtab2 = list2 + ii*nk;
	      nnb     = listindex[ii];
	      if(nnb>0)
		{
		  if(info != NULL)
		    PrecInfo_SubNNZ(info, CSnnz(L->ca[ii+L->cia[k]+1]));

		  CSCrowMultCSRcol(0.0, NULL, -1.0, nnb, -1.0, csrtab1, csrtab2, 
				   L->ca[ii+L->cia[k]+1], BL->block_index[i+1]-BL->block_index[i], wki1, wki2, wkd, celltab, cellptrtab); 

		  if(info != NULL)
		    PrecInfo_AddNNZ(info, CSnnz(L->ca[ii+L->cia[k]+1]));
		}
	      
	    }

	}

      

      /** Deallocate blocks which column indices are < START **/
      for(ii=L->ria[k];ii<L->ria[k+1];ii++)
	{
	  if(L->rja[ii] >= START)
	    break;

	  reinitCS(L->ra[ii]);
	  if(info != NULL)
	    PrecInfo_SubNNZ(info, CSnnz(L->ra[ii]));
	}




      /*** Factorize the diagonal block matrix of the column k ***/
      kk = BL->block_index[k]-BL->block_index[L->tlj];
      if(info != NULL)
	PrecInfo_SubNNZ(info, CSnnz(csL));

      if(droptab == NULL)
	CS_ICCT(csL, D+kk, droptol, NULL, fillrat, wki1, wki2, wkd, celltab, cellptrtab, option->shiftdiag);
      else
	CS_ICCT(csL, D+kk, droptol, droptab+kk, fillrat, wki1, wki2, wkd, celltab, cellptrtab, option->shiftdiag);

      if(info != NULL)
	PrecInfo_AddNNZ(info, CSnnz(csL));



      /**  Multiply the droptab by the diagonal elements **/
      ii = BL->block_index[k+1]-BL->block_index[L->tlj];
      for(i=kk;i<ii;i++)
	droptabtmp[i] /= coefabs(D[i]);
      /*** Divide the column block matrices by the diagonal factor ***/
      for(ii=L->cia[k]+1;ii<L->cia[k+1];ii++)
	{
	  i = L->cja[ii];
	  
	  /*fprintfv(5, stderr, "INVLT csL = %g M(%d, %d) = %g \n",  CSnormFrob(csL), i, k, CSnormFrob(L->ca[ii]));*/
	  if(info != NULL)
	    PrecInfo_SubNNZ(info, CSnnz(L->ca[ii]));

	  CSC_CSR_InvLT(csL, L->ca[ii],  BL->block_index[i+1]-BL->block_index[i], droptol, droptabtmp+kk, fillrat, wki1, wki2, wkd, celltab, cellptrtab);

	  if(info != NULL)
	    PrecInfo_AddNNZ(info, CSnnz(L->ca[ii]));

	  /*fprintfv(5, stderr, "AFTER INVLT csL = %g M(%d, %d) = %g \n",  CSnormFrob(csL), i, k, CSnormFrob(L->ca[ii]));*/
	  
	}

     

    }
  if(celltab != NULL)
    free(celltab);
  if(cellptrtab != NULL)
    free(cellptrtab);


  if(jak != NULL)
    free(jak);
  if(rak != NULL)
    free(rak);
  
  free(firstcol);
  free(listindex);
  free(list1);
  free(list2);

  free(D2);
  free(droptabtmp);
  free(wki1);
  free(wki2);
  free(wkd);

 

/* /\*   if (k==START+1) { *\/ */
/*     char filename[100]; */
/*     sprintf(filename, "ph.txt"); */
    
/*     FILE* stream = fopen(filename, "w"); */
/*     dumpPhidalMatrix(stream, L); */
/*     fclose(stream); */
/*     exit(1); */
/* /\*   } *\/ */

#ifdef DEBUG_M
  /*PhidalMatrix_Check(L, BL);*/
#endif

}

/* @authors P. HENON */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>

#include "phidal_sequential.h"

void PHIDAL_ILUTP(flag_t job, int pivoting, PhidalMatrix *A, PhidalMatrix *L, PhidalMatrix *U, REAL droptol, REAL *droptab, REAL fillrat, dim_t *permtab, PhidalHID *BL, PhidalOptions *option, PrecInfo *info)
{
  /**********************************************************************/
  /* This function computes an ILUTP factorization of the matrix A.     */
  /* Fill-in in the factor is only permit in the phidal pattern         */
  /* If job == 0 the matrix A is left unchanged                         */
  /* If job == 1 the factorization is done in place (A is a void matrix */
  /* when returned                                                      */
  /**********************************************************************/
  dim_t i;

  if(A->tli != A->tlj || A->bri != A->brj)
    {
      fprintfv(5, stderr, "Error in PHIDAL_ILUTP : Matrix A is not a square matrix \n");
      exit(-1);
    }


  /*** NB IT IS IMPORTANT TO CUT U BEFORE L BECAUSE DIAGONAL BLOCK ARE SUPPOSED TO BE IN U ***/
  /** 1- Copy the upper triangular part of A into U (**/
  PhidalMatrix_Setup(A->tli, A->tlj, A->bri, A->brj, "U", "N", option->locally_nbr, U, BL);

  if(job == 0)
    {
      PhidalMatrix_Copy(A, U, BL);
      if(info != NULL)
	PrecInfo_AddNNZ(info, PhidalMatrix_NNZ(U));
    }
  else
    PhidalMatrix_Cut(A, U, BL);


  /** 2- Copy the strictly lower triangular part of A into L **/
  PhidalMatrix_Setup(A->tli, A->tlj, A->bri, A->brj, "L", "N", option->locally_nbr, L, BL);
  if(job == 0)
    {
      PhidalMatrix_Copy(A, L, BL);
      if(info != NULL)
	PrecInfo_AddNNZ(info, PhidalMatrix_NNZ(L));
    }
  else
    PhidalMatrix_Cut(A, L, BL);


  /*** Reset the block diagonal to null matrices **/
  for(i=L->tli;i<=L->brj;i++)
    {
      reinitCS(L->ca[ L->cia[i]]);
      if(info != NULL)
	PrecInfo_AddNNZ(info, CSnnz(L->ca[ L->cia[i]]));
    }
  
  /*fprintfv(5, stderr, "PhidalMatrix NNZ A =%ld PhidalMatrix NNZ L = %ld PhidalMatrix NNZ U = %ld Somme = %ld\n", 
	  (long)PhidalMatrix_NNZ(A), (long)PhidalMatrix_NNZ(L), (long)PhidalMatrix_NNZ(U),
	  (long)PhidalMatrix_NNZ(L)+(long)PhidalMatrix_NNZ(U) );*/
  
  if(pivoting == 1)
    {
      PhidalMatrix_ILUTP(pivoting, L, U, droptol, droptab, fillrat, permtab, BL, option, info);
    }
  else
    PhidalMatrix_ILUCT(L, U, droptol, droptab, fillrat, BL, option, info);


}



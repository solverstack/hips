/* @authors P. HENON */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>

#include "phidal_sequential.h"



void PhidalMatrix_ILUTSchur(flag_t job, PhidalMatrix *G, PhidalMatrix *W, PhidalMatrix *S, PhidalHID *BL, PhidalOptions *option)
{
  /*********************************************************************/
  /* Compute the schur complement S = S-G*W                            */
  /* Fill-in in S respects the block pattern of S                      */
  /*                                                                   */
  /* PHIDAL block pattern                                              */
  /* If job == 1 then G and W are destroy along the Schur complement   */
  /* computing                                                         */
  /*********************************************************************/
  int  *wki1, *wki2;
  COEF *wkd;
  int i, ii, k, kk;
  int maxB, nnb;
  csptr *csrtab1, *csrtab2;
  CellCS *Gfirstcol, *Wfirstrow;
  csptr *list1, *list2;
  int *listindex;
  int nk, *jak;
  csptr *rak;


#ifdef DEBUG_M
  assert(S->tli == S->tlj);
  assert(S->bri == S->brj);
  assert(G->tli == S->tli);
  assert(G->bri == S->bri);
  assert(W->tlj == S->tlj);
  assert(W->brj == S->brj);
#endif


  maxB = 0;
  for(i=S->tli;i<=S->bri;i++)
    if(BL->block_index[i+1]-BL->block_index[i] > maxB)
      maxB = BL->block_index[i+1]-BL->block_index[i];

  wkd = (COEF *)malloc(sizeof(COEF)*maxB);
  wki1 = (int *)malloc(sizeof(int)*maxB);
  wki2 = (int *)malloc(sizeof(int)*maxB);


  ii = 0;
  for(k=G->tli;k<=G->bri;k++)
    if(G->ria[k+1] - G->ria[k] > ii)
      ii = G->ria[k+1] - G->ria[k];
  for(k=W->tlj;k<=W->brj;k++)
    if(W->cia[k+1] - W->cia[k] > ii)
      ii = W->cia[k+1] - W->cia[k];
  if(ii>0)
    {
      jak = (int *)malloc(sizeof(int)*ii);
      rak = (csptr *)malloc(sizeof(csptr)*ii);
    }
  else
    {
      jak = NULL;
      rak = NULL;
    }

  ii=0;
  for(k=S->tli;k<=S->bri;k++)
    {
      nnb = (G->ria[k+1]-G->ria[k]) * (S->ria[k+1]-S->ria[k]);
      if( nnb > ii)
	ii = nnb;
    }
  for(k=S->tlj;k<=S->brj;k++)
    {
      nnb = (W->cia[k+1]-W->cia[k]) * (S->cia[k+1]-S->cia[k]);
      if( nnb > ii)
	ii = nnb;
    }


  list1 = (csptr *)malloc(sizeof(csptr)* (ii+1));
  list2 = (csptr *)malloc(sizeof(csptr)* (ii+1));
  listindex = (int *)malloc(sizeof(int)*(S->bri-S->tli+1));

  Gfirstcol = (CellCS *)malloc(sizeof(CellCS)*(G->brj+1));
  for(k=G->tlj;k<=G->brj;k++)
    {
      Gfirstcol[k].nnz = G->cia[k+1]-G->cia[k];
      Gfirstcol[k].ja  = G->cja + G->cia[k];
      Gfirstcol[k].ma  = G->ca + G->cia[k];
    }
  Wfirstrow = (CellCS *)malloc(sizeof(CellCS)*(W->bri+1));
  for(k=W->tli;k<=W->bri;k++)
    {
      Wfirstrow[k].nnz = W->ria[k+1]-W->ria[k];
      Wfirstrow[k].ja  = W->rja + W->ria[k];
      Wfirstrow[k].ma  = W->ra + W->ria[k];
    }


  for(k=S->tlj;k<=S->brj;k++)
    {
      for(kk = S->cia[k]; kk < S->cia[k+1];kk++)
	if(S->cja[kk] == k)
	  break;
#ifdef DEBUG_M
      assert(S->cja[kk] == k);
      assert(S->rja[kk] == k);
#endif
      /**********************************************/
      /* Compute the column-block S(k+1:n,k)        */
      /**********************************************/
      nk = 0;
      for(ii=W->cia[k];ii<W->cia[k+1];ii++)
	if(W->ca[ii]->nnzr > 0)
	  {
	    jak[nk] = W->cja[ii];
	    rak[nk] = W->ca[ii];
	    nk++;
	  }
      
      if(nk>0)
	{
	  CellCS_ListUpdate(Gfirstcol, k+1,  nk,  jak);
	  CellCS_IntersectList(Gfirstcol, nk,  jak, rak, S->cia[k+1]-kk-1, S->cja+kk+1, 
			       listindex, list1, list2);
	  
	  for(ii=0;ii<S->cia[k+1]-kk-1;ii++)
	    {
	      i = S->cja[ii+kk+1];
	      /*** Compute L(i,k) = L(i,k) - L(i, 0:k-1).L(k, 0:k-1)t **/	      
	      /*** NO DROPPING HERE ***/
	      csrtab1 = list1 + ii*nk;
	      csrtab2 = list2 + ii*nk;
	      nnb     = listindex[ii];
	      if(nnb>0)
		CSRrowMultCSRcol(0.0, NULL, -1.0, nnb, -1.0, csrtab1, csrtab2, 
				 S->ca[ii+kk+1], BL->block_index[k+1]-BL->block_index[k], wki1, wki2, wkd); /*** NO DROPPING HERE ***/
	    }
	}

      /**********************************************/
      /* Compute the row-block S(k , k:n)           */
      /**********************************************/
      nk = 0;
      for(ii=G->ria[k];ii<G->ria[k+1];ii++)
	if(G->ra[ii]->nnzr > 0)
	  {
	    jak[nk] = G->rja[ii];
	    rak[nk] = G->ra[ii];
	    nk++;
	  }
      
      if(nk>0)
	{
	  
	  CellCS_ListUpdate(Wfirstrow, k,  nk,  jak);
	  CellCS_IntersectList(Wfirstrow, nk,  jak, rak, S->ria[k+1]-kk, S->rja+kk, 
			       listindex, list1, list2);
	  
	  for(ii=0;ii<S->ria[k+1]-kk;ii++)
	    {
	      i = S->rja[ii+kk];
	      
	      /*** Compute L(i,k) = L(i,k) - L(i, 0:k-1).L(k, 0:k-1)t **/	      
	      /*** NO DROPPING HERE ***/
	      /** List 1 contains block in the  W part and list 2
		  in G part ***/
	      csrtab2 = list1 + ii*nk;   
	      csrtab1 = list2 + ii*nk;
	      nnb     = listindex[ii];
	      if(nnb>0)
		CSRrowMultCSRcol(0.0, NULL, -1.0, nnb, -1.0, csrtab1, csrtab2, 
				 S->ra[ii+kk], BL->block_index[i+1]-BL->block_index[i], wki1, wki2, wkd); /*** NO DROPPING HERE ***/
	    }

	}
      
      if(job == 1)
	{ 
	  /*** Destroy row block k of G and column block k of W ***/
	  for(i=G->ria[k];i<G->ria[k+1];i++)
	    reinitCS(G->ra[i]);
	  for(i=W->cia[k];i<W->cia[k+1];i++)
	    reinitCS(W->ca[i]);
	}
      
    }

  free(wki1);
  free(wki2);
  free(wkd);
  
  if(jak != NULL)
    free(jak);
  if(rak != NULL)
    free(rak);
  free(Gfirstcol);
  free(Wfirstrow);
  free(listindex);
  free(list1);
  free(list2);



}



void PhidalMatrix_ICCTSchur(flag_t job, PhidalMatrix *G, COEF *D, PhidalMatrix *S, PhidalHID *BL, PhidalOptions *option)
{
  /*********************************************************************/
  /* Compute the schur complement S = S-G.D.Gt                         */
  /* Fill-in in S respects the block pattern of S                      */
  /* PHIDAL block pattern                                              */
  /* If job == 1 then G is  destroy along the Schur complement         */
  /* computing                                                         */
  /*********************************************************************/
  int  *wki1, *wki2;
  COEF *wkd;
  COEF *D2;
  csptr *csrtab1, *csrtab2;
  int i, ii, j,jj, k;
  int nnb;
  int maxB;
  int nk, *jak;
  csptr *rak;
  CellCS *firstcol;
  int *listindex;
  csptr *list1, *list2;
  cell_int *celltab;
  cell_int **cellptrtab;


#ifdef DEBUG_M
  assert(S->symmetric == 1);
  assert(S->csc == 1);
  assert(S->tli == S->tlj);
  assert(S->bri == S->brj);
  assert(G->tli == S->tli);
  assert(G->bri == S->bri);
  PhidalMatrix_Check(G, BL);
#endif


  maxB = 0;
  for(i=S->tli;i<=S->bri;i++)
    if(BL->block_index[i+1]-BL->block_index[i] > maxB)
      maxB = BL->block_index[i+1]-BL->block_index[i];


  wkd = (COEF *)malloc(sizeof(COEF)*maxB);
  /*wki1 = (int *)malloc(sizeof(int)*maxB);
    wki2 = (int *)malloc(sizeof(int)*maxB);*/

  /** Need these sizes for CSCrowMultCSRcol (see RowCompact) **/
  wki1= (int *)malloc(sizeof(int)*MAX(BL->nblock, maxB)); 
  wki2= (int *)malloc(sizeof(int)*G->dim2);



  ii = 0;
  for(k=G->tli;k<=G->bri;k++)
    if(G->ria[k+1] - G->ria[k] > ii)
      ii = G->ria[k+1] - G->ria[k];

  if(ii>0)
    {
      jak = (int *)malloc(sizeof(int)*ii);
      rak = (csptr *)malloc(sizeof(csptr)*ii);
    }
  else
    {
      jak = NULL;
      rak = NULL;
    }


  /*** Find the maximum row cumulate block size ****/
  nk = 0;
  for(i=G->tli;i<=G->bri;i++)
    {
      ii = 0;
      for(j=G->ria[i];j<G->ria[i+1];j++)
	ii+= G->ra[j]->n;
      if(ii > nk)
	nk = ii;
    }
  if(nk>0)
    celltab = (cell_int *)malloc(sizeof(cell_int)*nk);
  else
    celltab = NULL;
  
  /** Find the maximum row dimension of a block **/
  nk = 0;
  for(i=G->tli;i<=G->bri;i++)
    {
      ii = BL->block_index[i+1]-BL->block_index[i];
      if(ii>nk)
	nk = ii;
    }
  cellptrtab = (cell_int **)malloc(sizeof(cell_int *)*nk);



  ii=0;
  for(k=S->tlj;k<=S->brj;k++)
    {
      nnb = (G->ria[k+1]-G->ria[k] ) * (S->cia[k+1]-S->cia[k]);
      if( nnb > ii)
	ii = nnb;
    }

  list1 = (csptr *)malloc(sizeof(csptr)* (ii+1));
  list2 = (csptr *)malloc(sizeof(csptr)* (ii+1));
  listindex = (int *)malloc(sizeof(int)*(S->bri-S->tli+1));
  firstcol = (CellCS *)malloc(sizeof(CellCS)*(G->brj+1));
  for(k=G->tlj;k<=G->brj;k++)
    {
      firstcol[k].nnz = G->cia[k+1]-G->cia[k];
      firstcol[k].ja  = G->cja + G->cia[k];
      firstcol[k].ma  = G->ca + G->cia[k];
    }

  D2 = (COEF *)malloc(sizeof(COEF)* G->dim2);
  for(k=S->tlj;k<=S->brj;k++)
    {
      
      nk = 0;
      for(jj=G->ria[k];jj<G->ria[k+1];jj++)
	if(G->ra[jj]->nnzr > 0)
	  {
	    rak[nk] = G->ra[jj];
	    jak[nk] = G->rja[jj];
	    nk++;
	  }
      
      if(nk == 0)
	continue;

      /*** Compute S(k,k) = S(k,k) - G(k, 0:k-1).D-1.G(k, 0:k-1)t **/

      /*** Copy the diagonal part that match G(k, 0:k)***/
      ii = 0;
      for(jj=0;jj<nk;jj++)
	{
	  j = jak[jj];
	  memcpy(D2+ii, D+BL->block_index[j]-BL->block_index[G->tlj], sizeof(COEF)* (BL->block_index[j+1]-BL->block_index[j]));
	  ii += BL->block_index[j+1]-BL->block_index[j];
	} 

#ifdef DEBUG_M
      assert(S->cja[S->cia[k]] == k);
#endif
      /*** NO DROPPING HERE ***/
      CSCrowICCprod(0.0, NULL, -1.0, nk, rak, S->ca[S->cia[k]], D2, wki1, wki2, wkd, celltab, cellptrtab);



      /***********************/
      /* COLUMN-WISE SEARCH  */
      /***********************/
      CellCS_ListUpdate(firstcol, k+1,  nk,  jak);
      CellCS_IntersectList(firstcol, nk,  jak, rak, S->cia[k+1]-S->cia[k]-1, S->cja+S->cia[k]+1, listindex, list1, list2);
      for(ii=0;ii<S->cia[k+1]-S->cia[k]-1;ii++)
	{
	  i = S->cja[S->cia[k]+1+ii];

#ifdef DEBUG_M
	  assert(S->ca[ii+S->cia[k]+1]->n == BL->block_index[k+1]-BL->block_index[k]);
	  CS_Check(S->ca[ii+S->cia[k]+1], BL->block_index[i+1]-BL->block_index[i]);
	  assert(i>k);
#endif
	  /*** Compute L(i,k) = L(i,k) - L(i, 0:k-1).L(k, 0:k-1)t **/	      
	  /*** NO DROPPING HERE ***/
	  csrtab1 = list1 + ii*nk;
	  csrtab2 = list2 + ii*nk;
	  nnb     = listindex[ii];
	  if(nnb>0)
	    /*** NO DROPPING HERE ***/
	    CSCrowMultCSRcol(0.0, NULL, -1.0, nnb, -1.0, csrtab1, csrtab2, 
			     S->ca[S->cia[k]+1+ii], BL->block_index[i+1]-BL->block_index[i], 
			     wki1, wki2, wkd, celltab, cellptrtab); 
	  
	}
      
      if(job == 1)
	{ 
	  /*** Destroy the row k of G ***/
	  for(i=G->ria[k];i<G->ria[k+1];i++)
	    reinitCS(G->ra[i]);
	}
  
    }

  if(celltab != NULL)
    free(celltab);
  if(cellptrtab != NULL)
    free(cellptrtab);


  if(jak != NULL)
    free(jak);
  if(rak != NULL)
    free(rak);
  free(firstcol);
  free(listindex);
  free(list1);
  free(list2);

  free(D2);
  free(wki1);
  free(wki2);
  free(wkd);
}



#ifdef OLD
REAL PhidalMatrix_SchurError(PhidalMatrix *G, PhidalMatrix *W, PhidalMatrix *S, PhidalHID *BL)
{
  dim_t i,j,k;
  int jcol;
  int *isInS;
  REAL schurerr;
  REAL *Gnorm;
  REAL *Wnorm;

  if(G->bloknbr == 0 || W->bloknbr == 0)
    return 0.0; /** No schur complement error **/

  /** Compute the norm of the block of G and W **/
  Gnorm = (REAL *)malloc(sizeof(REAL)*G->bloknbr);
  Wnorm = (REAL *)malloc(sizeof(REAL)*W->bloknbr);
  for(k=0;k<G->bloknbr;k++)
    Gnorm[k] = CSnorm1(G->ra[k]);
  for(k=0;k<G->bloknbr;k++)
    Wnorm[k] = CSnorm1(W->ra[k]);

  isInS = (int *)malloc(sizeof(int)*(BL->nblock));
  bzero(isInS, sizeof(int)*BL->nblock);

  schurerr = 0.0;
  for(i=G->tli;i<G->bri;i++)
    {
      /** Unpack row i of S in isInS **/
      for(k=S->ria[i];k<S->ria[i+1];k++)
	isInS[S->rja[k]] = 1; 
      
      for(k=G->ria[i];k<G->ria[i+1];k++)
	{
	  jcol = G->rja[k];
	  for(j=W->ria[jcol];j<W->ria[jcol+1];j++)
	    if(isInS[W->rja[j]] != 1)
	      {
		/*fprintfv(5, stderr, "BLOCK %d %d is not in pattern \n", i, W->rja[j]);*/
		schurerr += Gnorm[k]*Wnorm[j];
	      }
	}

      /** Reinit isInS **/
      for(k=S->ria[i];k<S->ria[i+1];k++)
	isInS[S->rja[k]] = 0; 
    }
  
  free(Gnorm);
  free(Wnorm);
  free(isInS);
  return schurerr;
}
#endif

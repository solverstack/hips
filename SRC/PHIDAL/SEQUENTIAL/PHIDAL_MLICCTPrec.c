/* @authors P. HENON */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>

#include "phidal_sequential.h"

/*** local function ***/
void  PhidalPrec_MLICCTForward(int_t levelnum, REAL *droptab,  PhidalMatrix *A, PhidalPrec *P, PhidalHID *BL, PhidalOptions *option);

void PHIDAL_MLICCTPrec(PhidalMatrix *A, PhidalPrec *P, PhidalHID *BL, PhidalOptions *option)
{
  REAL *droptab;

#ifdef DEBUG_M
  assert(A->symmetric == 1);
#endif

  if(option->forwardlev == -1)
    option->forwardlev = BL->nlevel-1;

  if(option->forwardlev >= BL->nlevel || option->forwardlev < 0 )
    {
      fprintfv(5, stderr, "Invalid parameter : number of forward levels %d total number of level %d \n", option->forwardlev, BL->nlevel);
      exit(-1);
    }
  PhidalPrec_Init(P);

  if(option->verbose < 2)
    assert(P->info == NULL);

  /*** If droptab != NULL then the dropping will be made according to the norm of the column ***/
  /*if(option->scale > 0)
    {
    droptab = (REAL *)malloc(sizeof(REAL)*A->dim1);
    bzero(droptab, sizeof(REAL)*A->dim1);
    }
    else*/
  droptab = NULL;
  PhidalPrec_MLICCT(0, option->forwardlev, droptab,  A, P, BL, option);
  if(droptab != NULL)
    free(droptab);
}

void  PhidalPrec_MLICCT(int_t levelnum, int forwardlev, REAL *droptab, PhidalMatrix *A, PhidalPrec *P, PhidalHID *BL, PhidalOptions *option)
{
  REAL droptol;
  REAL *scaletab=NULL;
  REAL *iscaletab=NULL;
  dim_t i;
  chrono_t t1, t2, ttotal=0;
  char c;
#ifdef DEBUG_M
  assert(A->dim1>0);
#endif
  P->dim = A->dim1;
  P->schur_method = option->schur_method;
  P->forwardlev = forwardlev;

#ifdef SCALE_ALL
  if(option->scale > 0  && levelnum >= 1)/** TO SCALE RECURSIVELY ALL THE LEVEL **/
#else
    if(option->scale > 0 && levelnum == 0)  /** TO SCALE ONLY THE MATRIX A **/
#endif
      {
	scaletab = (REAL *)malloc(sizeof(REAL)*A->dim1);
	iscaletab = (REAL *)malloc(sizeof(REAL)*A->dim1);
	/*** Symmetric scale ***/
	PhidalMatrix_ColNorm2(A, BL, iscaletab);

	/** Take the square root of the norm such that a term 
	    aij = aij / (sqrt(rowi)*sqrt(colj)) **/
	for(i=0;i<A->dim1;i++)
	  iscaletab[i] = sqrt(iscaletab[i]); 
	for(i=0;i<A->dim1;i++)
	  scaletab[i] = 1.0/iscaletab[i];

	PhidalMatrix_RowMult2(scaletab, A, BL);
	PhidalMatrix_ColMult2(scaletab, A, BL);

	/**** Compute the droptab ******/
	/**** MUST BE THERE : AFTER THE SCALING ****/
	/**** BE CAREFUL USE A DROPTAB ONLY IF A SCALING IS DONE !! ***/
	if(droptab != NULL)
	  {
	    PhidalMatrix_ColNorm2(A, BL, droptab);
	    /*fprintfv(5, stderr, "NORM dropetab = %g \n", norm2(droptab, A->dim1));*/
	    /*for(i=0;i<A->dim1;i++)
	      droptab[i] = 1.0;*/
	  }
      }

  /********************************************/
  /** Dropping in the Schur complement matrix **/
  /********************************************/
  if(levelnum>0 && option->dropSchur > 0)
    PhidalMatrix_DropT(A, option->dropSchur, droptab, BL);

  /*** Compute the numerical threshold to use at this level in the factorization **/
  if(levelnum == 0)
    droptol = option->droptol0;
  else
    {
#ifdef DEBUG_M
      assert(levelnum>=1);
#endif
      droptol = option->droptol1 * pow(option->droptolrat, (REAL)(levelnum-1.0));
    }

  if(forwardlev == 0)
    {

      /**** just factorize the level *****/

      if (levelnum == 0) c = 'M'; else c = 'S';
      fprintfv(5, stdout, "  Numeric Factorisation (%c)\n",c); 
      /*t1 = dwalltime();*/
       
      P->symmetric = 1;

      P->levelnum =  levelnum;
   
      P->D = (COEF *)malloc(sizeof(COEF)*A->dim1);
      if(option->pivoting == 1)
	{
	  P->pivoting = 1;
	  P->permtab = (int *)malloc(sizeof(int)*A->dim1);
	  assert(0);
	}

      /*if(option->verbose >= 1)
	fprintfv(5, stdout, "Numerical threshold to factorize ALL levels >= %d = %g \n", levelnum, droptol);	*/

#ifdef ILUT_WHOLE_SCHUR 
      fprintfv(5, stderr, "ILUT_WHOLE_SCHUR ACTIVATED\n");
      
      P->csL = (csptr)malloc(sizeof(struct SparRow));
      if(option->schur_method != 1 && levelnum > 0)
	PhidalMatrix2SparRow(1, A, P->csL, BL);  /** Destroy A **/
      else
	PhidalMatrix2SparRow(0, A, P->csL, BL); /** Do not destroy A **/
      
      {
	COEF *wkd;
	int *wki1, *wki2;
	cell_int *firsttab;
	cell_int **LrowList;
	wkd = (COEF *)malloc(sizeof(COEF)*A->dim1);
	wki1= (int *)malloc(sizeof(int)*A->dim1);
	wki2= (int *)malloc(sizeof(int)*A->dim1);
	firsttab = (cell_int *)malloc(sizeof(cell_int)*A->dim1);
	LrowList = (cell_int **)malloc(sizeof(cell_int *)*A->dim1);

	t1 = dwalltime();
	CS_ICCT(P->csL, P->D, droptol, droptab, option->fillrat, wki1, wki2, wkd, firsttab, LrowList);
	t2 = dwalltime(); ttotal += t2-t1;

	free(wkd);
	free(wki1);
	free(wki2);
	free(firsttab);
	free(LrowList);
      }
      PREC_L_SCAL(P) = NULL;
      PREC_U_SCAL(P) = NULL;
      P->csU = NULL;
#else
      M_MALLOC(P->LU, SCAL);
      PREC_L_SCAL(P) = (PhidalMatrix *)malloc(sizeof(PhidalMatrix));
      
      t1 = dwalltime();
      if(option->schur_method != 1 && levelnum > 0)
	{
	  /** The factorization is done in place (A is a void matrix in return **/
	  PHIDAL_ICCT(1, A, PREC_L_SCAL(P), P->D, droptol, droptab, option->fillrat, BL, option, P->info);
	  PhidalMatrix_Clean(A);
	}
      else
	PHIDAL_ICCT(0, A, PREC_L_SCAL(P), P->D, droptol, droptab, option->fillrat, BL, option, P->info);
      t2 = dwalltime(); ttotal += t2-t1;
#ifdef ILUT_SOLVE
      fprintfv(5, stderr, "ILUT_SOLVE ACTIVATED \n");
      P->csL = (csptr)malloc(sizeof(struct SparRow));
      PhidalMatrix2SparRow(1, PREC_L_SCAL(P), P->csL, BL);  /** Destroy L **/
      PhidalMatrix_Clean(PREC_L_SCAL(P));
      free(PREC_L_SCAL(P));
      PREC_L_SCAL(P)=NULL;
      PREC_U_SCAL(P)=NULL;
      P->csU = NULL;
#else 
      /*** Construct U as a virtual matrix equal to the transpose of L ***/
      PREC_U_SCAL(P) = (PhidalMatrix *)malloc(sizeof(PhidalMatrix));
      PhidalMatrix_Init(PREC_U_SCAL(P));
      PhidalMatrix_BuildVirtualMatrix(PREC_L_SCAL(P)->tli, PREC_L_SCAL(P)->tlj, PREC_L_SCAL(P)->bri, PREC_L_SCAL(P)->brj, PREC_L_SCAL(P), PREC_U_SCAL(P), BL); 
      PhidalMatrix_Transpose(PREC_U_SCAL(P));
#endif /** ILUT_SOLVE **/

#endif /** ILUT_WHOL_SCHUR **/

      /*for(i=0;i<P->dim;i++)
	fprintfv(5, stderr, " %g ", P->D[i]);
	fprintfv(5, stderr, "\n");*/

      /*t2 = dwalltime(); ttotal += t2-t1;*/
      fprintfv(5, stdout, "   %c : Numeric Factorisation in %g seconds\n", c, t2-t1);
      goto unscale;
    }

#if !defined(ILUT_WHOLE_SCHUR) && !defined(ILUT_SOLVE) 
  if(forwardlev == 1 && option->schur_method != 1)
    {
      /** In this case the GEMM and ICCT are done in one step to 
	  save the storage of an intermediate schur complement **/
      PhidalPrec_MLICCTForward(levelnum, droptab, A, P, BL, option);
      
      goto unscale;
    }
#endif

  if(forwardlev > 0)
    {
      PhidalPrec_MLICCTForward(levelnum, droptab, A, P, BL, option);
      /*** Go to next level ***/
      P->nextprec = (PhidalPrec *)malloc(sizeof(PhidalPrec));
      P->nextprec->info = P->info;
      PhidalPrec_Init(P->nextprec);
      P->nextprec->prevprec = P;

      /** Important to call again PhidalPrec_MLICCT because of scaling **/
      if(droptab != NULL)
	PhidalPrec_MLICCT(levelnum+1, forwardlev-1, droptab+(BL->block_index[PREC_S_SCAL(P)->tli]-BL->block_index[A->tli])
			  , PREC_S_SCAL(P), P->nextprec, BL, option);
      else
	PhidalPrec_MLICCT(levelnum+1, forwardlev-1, NULL, PREC_S_SCAL(P), P->nextprec, BL, option);

      goto unscale;
    }


 unscale:
   

#ifdef SCALE_ALL
  if(option->scale >  0 && levelnum == 1) /** TO SCALE RECURSIVELY ALL THE LEVEL **/
#else
    if(option->scale >  0 && levelnum == 0) /** TO SCALE ONLY THE MATRIX A **/
#endif
      {
	/** Unscale the matrix and the preconditioner **/
	if(levelnum == 0 || option->schur_method == 1)
	  {
	    PhidalMatrix_ColMult2(iscaletab, A, BL);
	    PhidalMatrix_RowMult2(iscaletab, A, BL);
	  }

	PhidalPrec_SymmetricUnscale(scaletab, iscaletab, P, BL);
      
	free(scaletab);
	free(iscaletab);
      }

#ifdef DEBUG_M
  /*if(levelnum == 0)
    PhidalPrec_PrintInfo(P, BL);*/
#endif

}

void  PhidalPrec_MLICCTForward(int_t levelnum, REAL *droptab, PhidalMatrix *A, PhidalPrec *P, PhidalHID *BL, PhidalOptions *option)
{
  /**** A revoir: liberer le complement de Schur si schur_method == 0 ****/
  int Mstart, Mend, Sstart, Send;
  PhidalMatrix M, G;
  int  *wki1, *wki2;
  COEF *wkd;
  int maxB;
  dim_t i;
  REAL droptol; 
  COEF *D;
  REAL *droptabtmp;
  chrono_t t1, t2, ttotal=0;
  char c;
#ifdef DEBUG_M
  assert(levelnum < BL->nlevel);
#endif
  P->dim = A->dim1;
  P->symmetric = 1;
  P->levelnum =  levelnum;
  Mstart   =  BL->block_levelindex[levelnum];
  Mend     =  BL->block_levelindex[levelnum+1]-1;
  Sstart   =  BL->block_levelindex[levelnum+1];
  Send     =  BL->nblock-1;  

  
  M_MALLOC(P->LU, SCAL);
  PREC_L_SCAL(P) = (PhidalMatrix *)malloc(sizeof(PhidalMatrix));
  PREC_U_SCAL(P) = (PhidalMatrix *)malloc(sizeof(PhidalMatrix));
  P->D = (COEF *)malloc(sizeof(COEF)*A->dim1);


  /*** Compute the numerical threshold to use at this level in the factorization **/
  if(levelnum == 0)
    droptol = option->droptol0;
  else
    {
#ifdef DEBUG_M
      assert(levelnum>=1);
#endif
      droptol = option->droptol1 * pow(option->droptolrat, (REAL)(levelnum-1.0));
    }

   
  /*if(option->verbose >= 1)
    fprintfv(5, stdout, "Numerical threshold to factorize the level %d = %g \n", levelnum, droptol);	*/
      

  /*** Factorize the Level ***/
  PhidalMatrix_Init(&M);
  PhidalMatrix_BuildVirtualMatrix(Mstart, Mstart, Mend, Mend, A, &M, BL); 
  if(option->pivoting == 1)
    {
      P->pivoting = 1;
      P->permtab = (int *)malloc(sizeof(int)*M.dim1);
      assert(0);
    }

  M_MALLOC(P->EF, SCAL);
  PREC_E(P) = (PhidalMatrix *)malloc(sizeof(PhidalMatrix));
  PREC_F(P) = (PhidalMatrix *)malloc(sizeof(PhidalMatrix));

  if (levelnum == 0) c = 'M'; else c = 'S';
  fprintfv(5, stdout, "  Numeric Factorisation (%c)\n",c); 

  t1  = dwalltime();
  if(option->schur_method != 1 && levelnum > 0)
    {
      PHIDAL_ICCT(1, &M, PREC_L_SCAL(P), P->D, droptol, droptab, option->fillrat, BL, option, P->info);
      PhidalMatrix_Setup(Sstart, Mstart, Send, Mend, "N", "N" , option->locally_nbr, PREC_E(P), BL);
      PhidalMatrix_Cut(A, PREC_E(P), BL);
    }
  else
    {
      PHIDAL_ICCT(0, &M, PREC_L_SCAL(P), P->D, droptol, droptab, option->fillrat, BL, option, P->info);
      PhidalMatrix_BuildVirtualMatrix(Sstart, Mstart, Send, Mend, A, PREC_E(P), BL);
    }
  PhidalMatrix_Clean(&M);

  /*** Construct F as a virtual matrix equal to the transpose of E ***/
  PhidalMatrix_Init(PREC_F(P));
  PhidalMatrix_BuildVirtualMatrix(PREC_E(P)->tli, PREC_E(P)->tlj, PREC_E(P)->bri, PREC_E(P)->brj, PREC_E(P), PREC_F(P), BL); 
  PhidalMatrix_Transpose(PREC_F(P));

  t2 = dwalltime(); ttotal += t2-t1;
  fprintfv(5, stdout, "   %c : Numeric Factorisation in %g seconds\n", c, t2-t1);

  /************************************************/
  /* COMPUTE G= Lt^-1.E                           */
  /************************************************/
#ifdef DROP_TRSM
  droptol = option->droptol1;
#endif

  fprintfv(5, stdout, " TRSM M / E\n");       
  t1  = dwalltime(); 

  maxB = 0;
  for(i=A->tli;i<=A->bri;i++)
    if(BL->block_index[i+1]-BL->block_index[i] > maxB)
      maxB = BL->block_index[i+1]-BL->block_index[i];

  /** Need that for CSCrowMultCSRcol (see RowCompact) **/
  wki1 = (int *)malloc(sizeof(int)*MAX(maxB, BL->nblock));
  wki2 = (int *)malloc(sizeof(int)*A->dim1);
  wkd = (COEF *)malloc(sizeof(COEF)*maxB);

  PhidalMatrix_Init(&G);
  /** IMPORTANT :G is always computed using a consistent block pattern **/
  /*PhidalMatrix_Setup(PREC_E(P)->tli, PREC_E(P)->tlj, PREC_E(P)->bri, PREC_E(P)->brj, "N", "N", BL->nlevel, &G, BL);*/
  PhidalMatrix_Setup(PREC_E(P)->tli, PREC_E(P)->tlj, PREC_E(P)->bri, PREC_E(P)->brj, "N", "N", option->locally_nbr, &G, BL);
  PhidalMatrix_Copy(PREC_E(P), &G, BL);
   
  if(option->pivoting == 1)
    /** Important to pivot G and not E because E is a virtual matrix pointing in the last upper schur complement **/
    PhidalMatrix_ColPerm(&G, P->permtab, BL); 
   
  D = P->D;
  droptabtmp = (REAL *)malloc(sizeof(REAL)*PREC_L_SCAL(P)->dim1);
  if(droptab != NULL)
    for(i=0;i<PREC_L_SCAL(P)->dim1;i++)
      droptabtmp[i] = droptab[i]*1.0/coefabs(D[i]);
  else
    for(i=0;i<PREC_L_SCAL(P)->dim1;i++)
      droptabtmp[i] = 1.0/coefabs(D[i]);
   
  PHIDAL_ICCT_InvLT(PREC_L_SCAL(P), &G, option->droptolE, droptabtmp, option->fillrat, BL, wki1, wki2, wkd);
  free(droptabtmp);
   
  /*** Construct U as a virtual matrix equal to the transpose of L ***/
  PhidalMatrix_Init(PREC_U_SCAL(P));
  PhidalMatrix_BuildVirtualMatrix(PREC_L_SCAL(P)->tli, PREC_L_SCAL(P)->tlj, PREC_L_SCAL(P)->bri, PREC_L_SCAL(P)->brj, PREC_L_SCAL(P), PREC_U_SCAL(P), BL); 
  PhidalMatrix_Transpose(PREC_U_SCAL(P));
   
  free(wki1);
  free(wki2);
  free(wkd);

  t2  = dwalltime(); ttotal += t2-t1;
  fprintfv(5, stdout, "  TRSM in %g seconds\n\n", t2-t1);
   
  /*** Compute the schur complement on the remaining levels ****/
  fprintfv(5, stdout, " GEMM E/E -> S\n"); 
  t1  = dwalltime(); 

  M_MALLOC(P->S,  SCAL);
  PREC_S_SCAL(P) = (PhidalMatrix *)malloc(sizeof(PhidalMatrix));
  if(option->schur_method != 1)
    PhidalMatrix_Setup(Sstart, Sstart, Send, Send, "L", "N", option->locally_nbr, PREC_S_SCAL(P), BL);
  else
    PhidalMatrix_Setup(Sstart, Sstart, Send, Send, "L", "N", BL->nlevel, PREC_S_SCAL(P), BL);
  
  if(option->schur_method == 2)
    {
      M_MALLOC(P->B,  SCAL);
      PREC_B(P) = (PhidalMatrix *)malloc(sizeof(PhidalMatrix));
      PhidalMatrix_Init(PREC_B(P));
      if(levelnum == 0)
	PhidalMatrix_BuildVirtualMatrix(Sstart, Sstart, Send, Send, A, PREC_B(P), BL);
      else
	{
	  PhidalMatrix_Setup(Sstart, Sstart, Send, Send, "L", "N",
			     option->locally_nbr, PREC_B(P), BL);
	  /*PhidalMatrix_Setup(Sstart, Sstart, Send, Send, "L", "N", BL->nlevel, PREC_B(P), BL);*/
	  PhidalMatrix_Copy(A, PREC_B(P), BL);
	}
    }

  if(option->schur_method != 1 && levelnum > 0)
    PhidalMatrix_Cut(A, PREC_S_SCAL(P), BL);
  else
    PhidalMatrix_Copy(A, PREC_S_SCAL(P), BL);

  if(option->schur_method != 1 && levelnum > 0)
    PhidalMatrix_Clean(A);

 /** OIMBE Pour ILUT_SOLVE vaudrait mieux utiliser PhidalPrec_GEMM_ILUCT
      et faire la transformation apres ! **/
#if defined(ILUT_WHOLE_SCHUR) || defined(ILUT_SOLVE)
  PhidalMatrix_ICCTSchur(1, &G, P->D, PREC_S_SCAL(P), BL, option);
#else
  if(option->forwardlev == levelnum+1 && option->schur_method != 1)
    {
      P->nextprec = (PhidalPrec *)malloc(sizeof(PhidalPrec));
      PhidalPrec_Init(P->nextprec); /* suffisant ?? */
      P->nextprec->prevprec = P;
      PhidalPrec_GEMM_ICCT(levelnum+1, droptab, &G, PREC_S_SCAL(P), P->D, P->nextprec, BL, option);
      PhidalMatrix_Clean(PREC_S_SCAL(P));
      free(PREC_S_SCAL(P));
      PREC_S_SCAL(P) = NULL;
    }
  else
    {
      /** G is destroyed inside this function **/
      PhidalMatrix_ICCTSchur(1, &G, P->D, PREC_S_SCAL(P), BL, option);
    }
#endif


  PhidalMatrix_Clean(&G);

  t2  = dwalltime(); ttotal += t2-t1;
  fprintfv(5, stdout, "  GEMM in %g seconds\n\n", t2-t1);

}


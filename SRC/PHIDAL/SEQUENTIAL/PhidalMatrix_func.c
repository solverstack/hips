/* @authors P. HENON */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>

#include "phidal_sequential.h"

void PhidalMatrix_Setup(dim_t tli, dim_t tlj, dim_t bri, dim_t brj, char *UPLO, char *DIAG, int_t locally_nbr, PhidalMatrix *m, PhidalHID *BL)
{
  PhidalMatrix_Setup2(tli, tlj, bri, brj, UPLO, DIAG, locally_nbr, m, BL);

  /*TEST, a supprimer quand OK */
  assert(m->virtual == 0);
 
}

void PhidalMatrix_Reinit(PhidalMatrix *M)
{
  dim_t i;
  for(i=0;i<M->bloknbr;i++)
    reinitCS(M->bloktab+i);
}

void PhidalMatrix_CsrCopy(flag_t job, csptr a, char *DIAG, PhidalMatrix *m, PhidalHID *BL)
{
  /******************************************************/
  /* Convert a csr matrix into a phidal matrix          */
  /* if DIAG = "L" then only the lower triangular part  */
  /* of the diagonal block are copied                   */
  /* if DIAG = "U" then only the upperer triangular part*/
  /* of the diagonal block are copied                   */
  /* If DIAG = N then all the diagonal block are copied */
  /* job = 0 : initialize the phidal matrix with the    */
  /*         csr matrix                                 */
  /* job = 1 : add the csr matrix to the phidal matrix  */
  /* job = 2 : reinitialize the phidal matrix with the  */
  /*           csr                                      */
  /* job = 3 : substitute the coefficient by the new    */
  /*           ones given in the csr                    */
  /******************************************************/   
  dim_t i, j, k, l, jpos;
  dim_t ii, jj;
  dim_t *brow2nnz;
  dim_t *node2block;
  dim_t nrow;
  dim_t *tmpj, *ja, *newja=NULL, *jrev=NULL;
  /* dim_t *tmprev; */
  COEF *tmpa, *ma, *newma=NULL;
  flag_t diag=-1;
  dim_t *jrow;
  COEF *marow;
  dim_t nnzrow;
  dim_t *block_index;
  csptr bm;

  /******************************************/
  /*** Extract the blocks from the matrix ***/
  /******************************************/
#ifdef DEBUG_M
  assert(m->virtual == 0);
  assert(m->csc == 0);
#endif

  if(strcmp(DIAG, "N") == 0)
    diag = 0;
  else
    {
      if(strcmp(DIAG, "L") == 0)
	diag = 1;
      else
	if(strcmp(DIAG, "U") == 0)
	   diag = 2;
    }


  block_index = BL->block_index;

  tmpa = (COEF *)malloc(sizeof(COEF)*(BL->n));
  tmpj = (dim_t *)malloc(sizeof(dim_t)*(BL->n));

  if(job == 1 || job == 3)
    {
      /** @@@OIMBE Peut allouer moins **/
      newma = (COEF *)malloc(sizeof(COEF)*(BL->n));
      newja = (dim_t *)malloc(sizeof(dim_t)*(BL->n));
      jrev = (dim_t *)malloc(sizeof(dim_t)*(BL->n));
      for(l=0;l<BL->n;l++)
	jrev[l] = -1;
    }
  node2block = (dim_t *)malloc(sizeof(dim_t)*(BL->n));
  brow2nnz = (dim_t *)malloc(sizeof(dim_t)*BL->nblock);
  

  for(k=0;k<BL->nblock;k++)
    for(i=block_index[k];i<block_index[k+1];i++)
      node2block[i] = k;

  if(job == 2)
    PhidalMatrix_Reinit(m);

  bzero(brow2nnz, sizeof(dim_t)*BL->nblock);      
  for(i=m->tli;i<=m->bri;i++)
    {
      ii = block_index[i+1]-block_index[i];

      if(job == 0)
	for(k = m->ria[i];k<m->ria[i+1];k++)
	  initCS(m->ra[k], ii);  


      for(ii=block_index[i];ii<block_index[i+1];ii++)
	{
	  /***** Split the line i from the csr matrix into a line for each block of the block row i ****/
	  jrow = a->ja[ii];
	  marow = a->ma[ii];
	  nnzrow = a->nnzrow[ii];
	  
	  if(nnzrow == 0)
	    continue;

	  /* */
	  for(jj = 0; jj < nnzrow;jj++)
	    {
	      k = node2block[jrow[jj]];
	      tmpj[ block_index[k] + brow2nnz[k] ] = jrow[jj] - block_index[k];
	      tmpa[ block_index[k] + brow2nnz[k] ] = marow[jj];
	      brow2nnz[k]++;
	    }
#ifdef DEBUG_M
	  jj = 0;
	  for(j=0;j<BL->nblock;j++)
	    jj+=brow2nnz[j];
	  if(jj != nnzrow)
	    fprintfd(stderr, "jj %d nnzrow %d \n", jj, nnzrow);
	  assert(jj == nnzrow);
#endif


	  for(k = m->ria[i];k<m->ria[i+1];k++)
	    {
	      j = m->rja[k];
	      bm = m->ra[k];
	      
#ifdef DEBUG_M
	      assert(ii-block_index[i] < bm->n);
#endif
	      jj = ii-block_index[i];

	      
	      if(job == 0 || job == 2)
		{
		  if(brow2nnz[j]>0)
		    {
		      bm->nnzrow[jj] = brow2nnz[j];
		      bm->ja[ jj ] = (dim_t *)malloc(sizeof(dim_t)*brow2nnz[j]);
		      bm->ma[ jj ] = (COEF *)malloc(sizeof(COEF)*brow2nnz[j]);
		      memcpy( bm->ja[ jj ], tmpj + block_index[j], sizeof(dim_t)*brow2nnz[j]);
		      memcpy( bm->ma[ jj ], tmpa + block_index[j], sizeof(COEF)*brow2nnz[j]);
		    }

		}
	      else
		{
		  if(brow2nnz[j]>0)
		    {
		      if(bm->inarow == 1)
			CSunrealloc(bm);


		      ja =  tmpj + block_index[j];
		      ma =  tmpa + block_index[j];
		      nrow = bm->nnzrow[jj];
		      if(nrow == 0)
			{
			  bm->nnzrow[jj] = brow2nnz[j];
			  bm->ja[ jj ] = (dim_t *)malloc(sizeof(dim_t)*brow2nnz[j]);
			  bm->ma[ jj ] = (COEF *)malloc(sizeof(COEF)*brow2nnz[j]);
			  memcpy( bm->ja[ jj ], ja, sizeof(dim_t)*brow2nnz[j]);
			  memcpy( bm->ma[ jj ], ma, sizeof(COEF)*brow2nnz[j]);
			}
		      else
			{
			  memcpy( newja, bm->ja[jj], sizeof(dim_t)*nrow);
			  memcpy( newma, bm->ma[jj], sizeof(COEF)*nrow);
			  free(bm->ja[ jj ]);
			  free(bm->ma[ jj ]);

			  for(l=0;l<nrow;l++)
			    jrev[newja[l]] = l;
			  
			  for(l=0;l<brow2nnz[j];l++)
			    {
			      jpos = jrev[ja[l]];
			      if(jpos < 0)
				{
				  newja[nrow] = ja[l];
				  newma[nrow] = ma[l];
				  jrev[ja[l]] = nrow++;
				}
			      else
				{
				  switch(job)
				    {
				    case 1 :
				      newma[jpos] += ma[l];
				      break;
				    case 3 :
				      newma[jpos] = ma[l];
				      break;
				    default :
				      assert(0);
				      
				    }
				}
				      
			    }

			  bm->nnzrow[jj] = nrow;
			  bm->ja[ jj ] = (dim_t *)malloc(sizeof(dim_t)*nrow);
			  bm->ma[ jj ] = (COEF *)malloc(sizeof(COEF)*nrow);
			  memcpy( bm->ja[ jj ], newja, sizeof(dim_t)*nrow);
			  memcpy( bm->ma[ jj ], newma, sizeof(COEF)*nrow);
			  
			  /*** Reset jrev ****/
			  for(l=0;l<nrow;l++)
			    jrev[newja[l]] = -1;
			}
		    }
		} 
	    }
	  /*** Reset brow **/
	  for(jj = 0; jj < nnzrow;jj++)
	    brow2nnz[node2block[jrow[jj]]] = 0;
	}
      
      for(k = m->ria[i];k<m->ria[i+1];k++)
	CS_SetNonZeroRow(m->ra[k]);

    
      for(k = m->ria[i];k<m->ria[i+1];k++)
	{
	  j = m->rja[k];
	  if(diag != 0 && i==j)
	    {
	      if(diag == 1)
		CS_GetLower(1, "N", m->ra[k], m->ra[k]);
	      else
		CS_GetUpper(1, "N", m->ra[k], m->ra[k]);
	    }
	  
	}


#ifdef REALLOC_BLOCK
      /** Realloc the small block **/
      for(k = m->ria[i];k<m->ria[i+1];k++)
	{
	  j = m->rja[k];
	  if(j != i) 
	    {
	      jj = BL->block_index[j+1]-BL->block_index[j];

	      /**** OIMBE @@ En parallele on ne peut pas reallouer 
		    les block partages car il faut pouvoir ajouter des
		    contribution venant d'autres processor ****/

	      if(m->ra[k]->n*jj <= SMALLBLOCK) 
		CSrealloc(m->ra[k]);
	    }
	}
#endif


    }

  if(job == 1 || job == 3)
    {
      free(jrev);
      free(newja);
      free(newma);
    }
  free(tmpj);
  free(tmpa);
  free(node2block);
  free(brow2nnz);

  
#ifdef DEBUG_M
  PhidalMatrix_Check(m, BL);
#endif
}



void PhidalMatrix_CscCopy(flag_t job, csptr a, char *DIAG, PhidalMatrix *m, PhidalHID *BL)
{
  /******************************************************/
  /* Convert a csc matrix into a phidal matrix          */
  /* if DIAG = "L" then only the lower triangular part  */
  /* of the diagonal block are copied                   */
  /* if DIAG = "U" then only the upperer triangular part*/
  /* of the diagonal block are copied                   */
  /* If DIAG = N then all the diagonal block are copied */
  /* job = 0 : initialize the phidal matrix with the    */
  /*         csr matrix                                 */
  /* job = 1 : add the csr matrix to the phidal matrix  */
  /* job = 2 : reinitialize the phidal matrix with the  */
  /*           csr                                      */
  /*           csr                                      */
  /* job = 3 : substitute the coefficient by the new    */
  /*           ones given in the csr                    */
  /******************************************************/   
  dim_t i, j, k, l, jpos;
  dim_t ii, jj;
  dim_t *brow2nnz;
  dim_t *node2block;
  dim_t nrow;
  dim_t *tmpj, *ja, *newja=NULL, *jrev=NULL;
  COEF *tmpa, *ma, *newma=NULL;
  flag_t diag=-1;
  dim_t *jrow;
  COEF *marow;
  dim_t nnzrow;
  dim_t *block_index;
  csptr bm;

  /******************************************/
  /*** Extract the blocks from the matrix ***/
  /******************************************/
#ifdef DEBUG_M
  assert(m->virtual == 0);
  assert(m->csc == 1);
#endif

  if(strcmp(DIAG, "N") == 0)
    diag = 0;
  else
    {
      if(strcmp(DIAG, "L") == 0)
	diag = 1;
      else
	if(strcmp(DIAG, "U") == 0)
	   diag = 2;
    }


  block_index = BL->block_index;

  tmpa = (COEF *)malloc(sizeof(COEF)*(BL->n));
  tmpj = (dim_t *)malloc(sizeof(dim_t)*(BL->n));

  if(job == 1 || job == 3)
    {
      /** @@@OIMBE Peut allouer moins mais bon ...faut chercher le max ***/
      newma = (COEF *)malloc(sizeof(COEF)*(BL->n));
      newja = (dim_t *)malloc(sizeof(dim_t)*(BL->n));
      jrev = (dim_t *)malloc(sizeof(dim_t)*(BL->n));
      for(l=0;l<BL->n;l++)
	jrev[l] = -1;
    }
  node2block = (dim_t *)malloc(sizeof(dim_t)*(BL->n));
  brow2nnz = (dim_t *)malloc(sizeof(dim_t)*BL->nblock);
  

  for(k=0;k<BL->nblock;k++)
    for(i=block_index[k];i<block_index[k+1];i++)
      node2block[i] = k;

  if(job == 2)
    PhidalMatrix_Reinit(m);

  bzero(brow2nnz, sizeof(dim_t)*BL->nblock);
  /*for(i=m->tli;i<=m->bri;i++)*/
  for(i=m->tlj;i<=m->brj;i++)
    {
      ii = block_index[i+1]-block_index[i];

      if(job == 0)
	/*for(k = m->ria[i];k<m->ria[i+1];k++)
	  initCS(m->ra[k], ii);  */
	for(k = m->cia[i];k<m->cia[i+1];k++)
	  initCS(m->ca[k], ii);  
      
      for(ii=block_index[i];ii<block_index[i+1];ii++)
	{
	  /***** Split the line i from the csr matrix into a line for each block of the block row i ****/
	  jrow = a->ja[ii];
	  marow = a->ma[ii];
	  nnzrow = a->nnzrow[ii];
	  
	  if(nnzrow == 0)
	    continue;


	  for(jj = 0; jj < nnzrow;jj++)
	    {
	      k = node2block[jrow[jj]];
	      tmpj[ block_index[k] + brow2nnz[k] ] = jrow[jj] - block_index[k];
	      tmpa[ block_index[k] + brow2nnz[k] ] = marow[jj];
	      brow2nnz[k]++;
	    }
#ifdef DEBUG_M
	  jj = 0;
	  for(j=0;j<BL->nblock;j++)
	    jj+=brow2nnz[j];
	  if(jj != nnzrow)
	    fprintfd(stderr, "jj %d nnzrow %d \n", jj, nnzrow);
	  assert(jj == nnzrow);
#endif


	  /*for(k = m->ria[i];k<m->ria[i+1];k++)*/
	  for(k = m->cia[i];k<m->cia[i+1];k++)
	    {
	      /*j = m->rja[k];
		bm = m->ra[k];*/
	      j = m->cja[k];
	      bm = m->ca[k];
	      
#ifdef DEBUG_M
	      assert(ii-block_index[i] < bm->n);
#endif
	      jj = ii-block_index[i];

	      
	      if(job == 0 || job == 2)
		{
		  if(brow2nnz[j]>0)
		    {
		      bm->nnzrow[jj] = brow2nnz[j];
		      bm->ja[ jj ] = (dim_t *)malloc(sizeof(dim_t)*brow2nnz[j]);
		      bm->ma[ jj ] = (COEF *)malloc(sizeof(COEF)*brow2nnz[j]);
		      memcpy( bm->ja[ jj ], tmpj + block_index[j], sizeof(dim_t)*brow2nnz[j]);
		      memcpy( bm->ma[ jj ], tmpa + block_index[j], sizeof(COEF)*brow2nnz[j]);
		    }

		}
	      else
		{
		  if(brow2nnz[j]>0)
		    {
		      if(bm->inarow == 1)
			CSunrealloc(bm);

		      ja =  tmpj + block_index[j];
		      ma =  tmpa + block_index[j];
		      nrow = bm->nnzrow[jj];
		      if(nrow == 0)
			{
			  bm->nnzrow[jj] = brow2nnz[j];
			  bm->ja[ jj ] = (dim_t *)malloc(sizeof(dim_t)*brow2nnz[j]);
			  bm->ma[ jj ] = (COEF *)malloc(sizeof(COEF)*brow2nnz[j]);
			  memcpy( bm->ja[ jj ], ja, sizeof(dim_t)*brow2nnz[j]);
			  memcpy( bm->ma[ jj ], ma, sizeof(COEF)*brow2nnz[j]);
			}
		      else
			{
			  memcpy( newja, bm->ja[jj], sizeof(dim_t)*nrow);
			  memcpy( newma, bm->ma[jj], sizeof(COEF)*nrow);
			  free(bm->ja[ jj ]);
			  free(bm->ma[ jj ]);

			  for(l=0;l<nrow;l++)
			    jrev[newja[l]] = l;
			  
			  for(l=0;l<brow2nnz[j];l++)
			    {
			      jpos = jrev[ja[l]];
			      if(jpos < 0)
				{
				  newja[nrow] = ja[l];
				  newma[nrow] = ma[l];
				  jrev[ja[l]] = nrow++;
				}
			      else
				{
				  switch(job)
				    {
				    case 1 :
				      newma[jpos] += ma[l];
				      break;
				    case 3 :
				      newma[jpos] = ma[l];
				      break;
				    default :
				      assert(0);
				      
				    }
				}
			    }

			  bm->nnzrow[jj] = nrow;
			  bm->ja[ jj ] = (dim_t *)malloc(sizeof(dim_t)*nrow);
			  bm->ma[ jj ] = (COEF *)malloc(sizeof(COEF)*nrow);
			  memcpy( bm->ja[ jj ], newja, sizeof(dim_t)*nrow);
			  memcpy( bm->ma[ jj ], newma, sizeof(COEF)*nrow);
			  
			  /*** Reset jrev ****/
			  for(l=0;l<nrow;l++)
			    jrev[newja[l]] = -1;
			}
		    }
		} 
	    }

	  /*** Reset brow **/
	  for(jj = 0; jj < nnzrow;jj++)
	    brow2nnz[node2block[jrow[jj]]] = 0;
	}
      
      /*for(k = m->ria[i];k<m->ria[i+1];k++)
	CS_SetNonZeroRow(m->ra[k]);*/
      for(k = m->cia[i];k<m->cia[i+1];k++)
	CS_SetNonZeroRow(m->ca[k]);

    
      /*for(k = m->ria[i];k<m->ria[i+1];k++)*/
      for(k = m->cia[i];k<m->cia[i+1];k++)
	{
	  /*j = m->rja[k];*/
	  j = m->cja[k];
	  if(diag != 0 && i==j)
	    {
	      /*if(diag == 1)
		CS_GetLower(1, "N", m->ra[k], m->ra[k]);
	      else
	      CS_GetUpper(1, "N", m->ra[k], m->ra[k]);*/
	      if(diag == 1)
		CS_GetLower(1, "N", m->ca[k], m->ca[k]);
	      else
		CS_GetUpper(1, "N", m->ca[k], m->ca[k]);
	    }
	  
	}


#ifdef REALLOC_BLOCK
      /** Realloc the small block **/
      /*for(k = m->ria[i];k<m->ria[i+1];k++)*/
      for(k = m->cia[i];k<m->cia[i+1];k++)
	{
	  /*j = m->rja[k];*/
	  j = m->cja[k];
	  if(j != i) 
	    {
	      jj = BL->block_index[j+1]-BL->block_index[j];

	      /**** OIMBE @@ En parallele on ne peut pas reallouer 
		    les block partages car il faut pouvoir ajouter des
		    contribution venant d'autres processor ****/

	      /*if(m->ra[k]->n*jj <= SMALLBLOCK) 
		CSrealloc(m->ra[k]);*/
	      if(m->ca[k]->n*jj <= SMALLBLOCK) 
		CSrealloc(m->ca[k]);
	    }
	}
#endif


    }

  if(job == 1 || job == 3)
    {
      free(jrev);
      free(newja);
      free(newma);
    }
  free(tmpj);
  free(tmpa);
  free(node2block);
  free(brow2nnz);

  
#ifdef DEBUG_M
  PhidalMatrix_Check(m, BL);
#endif
}


void PhidalMatrix_Copy(PhidalMatrix *a, PhidalMatrix *b, PhidalHID *BL)
{
  /********************************************************/
  /* This function copy a phidal matrix a into another    */
  /* ONLY the block pattern of matrix b is copied from    */
  /* the matrix a                                         */
  /* NB: the scaling is not copied                        */
  /********************************************************/
  dim_t i, j;
  dim_t ja, jb;
  csptr csa;
  csptr csb;

#ifdef DEBUG_M
  assert(b->tli >= a->tli);
  assert(b->tlj >= a->tlj);
  assert(b->bri <= a->bri);
  assert(b->brj <= a->brj);
  /*PhidalMatrix_Check(a, BL);*/ /* Ralenti beaucoup le run */
#endif

#ifdef DEBUG_M
  assert(b->virtual == 0);
#endif

  if(b->tli == b->tlj && b->bri == b->brj)
    b->symmetric = a->symmetric;
  else
    b->symmetric = 0;

  b->csc = a->csc;

  for(i=b->tli;i<=b->bri;i++)
    {

      for(jb=b->ria[i];jb<b->ria[i+1];jb++)
	{
	  j = b->rja[jb];
	  csb = b->ra[jb];
	  if(a->csc == 0)
	    initCS(csb, BL->block_index[i+1] - BL->block_index[i]); 
	  else
	    initCS(csb, BL->block_index[j+1] - BL->block_index[j]); 
	}

      ja = a->ria[i];
      for(jb=b->ria[i];jb<b->ria[i+1];jb++)
	{
	  j = b->rja[jb];
	  while(ja < a->ria[i+1])
	    {
	      if(a->rja[ja] >= j)
		break;
	      ja++;
	    }
	  if(ja >= a->ria[i+1])
	    break;

	  if(a->rja[ja] == j)
	    {
	      csa = a->ra[ja];
	      csb = b->ra[jb];
	      CS_Copy(csa, csb);
	      ja++;
	    }
	}
    }

#ifdef DEBUG_M
  /*PhidalMatrix_Check(b, BL);*/
#endif

}

void PhidalMatrix_Add(PhidalMatrix *a, PhidalMatrix *b, PhidalHID *BL)
{
  /********************************************************/
  /* This function add a phidal matrix a into another     */
  /* ONLY the block pattern of matrix b is add from       */
  /* the matrix a                                         */
  /********************************************************/

  dim_t i, j;
  dim_t ja, jb;
  csptr csa;
  csptr csb;
  dim_t *tmpj, *jrev;
  COEF *tmpa;

#ifdef DEBUG_M
  assert(b->tli >= a->tli);
  assert(b->tlj >= a->tlj);
  assert(b->bri <= a->bri);
  assert(b->brj <= a->brj);
#endif

  j = 0;
  for(i=b->tli;i<=b->bri;i++)
    if(j < BL->block_index[i+1] - BL->block_index[i])
      j = BL->block_index[i+1] - BL->block_index[i];
  tmpj = (dim_t *)malloc(sizeof(dim_t)*j);
  jrev = (dim_t *)malloc(sizeof(dim_t)*j);
  tmpa = (COEF *)malloc(sizeof(COEF)*j);
  
  for(i=b->tli;i<=b->bri;i++)
    {
      ja = a->ria[i];
      for(jb=b->ria[i];jb<b->ria[i+1];jb++)
	{
	  j = b->rja[jb];
	  while(ja < a->ria[i+1])
	    {
	      if(a->rja[ja] >= j)
		break;
	      ja++;
	    }
	  if(ja >= a->ria[i+1])
	    break;

	  if(a->rja[ja] == j)
	    {
	      csa = a->ra[ja];
	      csb = b->ra[jb];
	      CS_MatricesAdd(csb, 1, csa, tmpj, tmpa, jrev);
	      ja++;
	    }
	}
    }

  free(tmpj);
  free(tmpa);
  free(jrev);

#ifdef DEBUG_M
  PhidalMatrix_Check(b, BL);
#endif

}



void PhidalMatrix_Cut(PhidalMatrix *a, PhidalMatrix *b, PhidalHID *BL)
{
  /********************************************************/
  /* This function copy a phidal matrix a into another    */
  /* ONLY the block pattern of matrix b is copied from    */
  /* the matrix a                                         */
  /* ALL the copied block are remove from a               */
  /* NB: the scaling is not copied                        */
  /* If a and b do not match ; then the intersection of a */
  /* with b is copied from a to b                         */
  /* THE PART OF B THAT ARE NOT IN A ARE LEFT UNCHANGED   */
  /* (POSSIBLY NULL RANK MATRICES)                        */
  /********************************************************/

  dim_t i, j;
  dim_t ja, jb;
  dim_t start, end;
  csptr csa;
  csptr csb;

#ifdef DEBUG_M
  assert(b->virtual == 0);
#endif

  if(b->tli == b->tlj && b->bri == b->brj)
    b->symmetric = a->symmetric;
  else
    b->symmetric = 0;

  b->csc = a->csc;

  start = MAX(a->tli, b->tli);
  end = MIN(a->bri, b->bri);
  for(i=start;i<=end;i++)
    {

      for(jb=b->ria[i];jb<b->ria[i+1];jb++)
	{
	  j = b->rja[jb];
	  if(j<a->tlj)
	    continue;
	  if(j>a->brj)
	    break;
	  csb = b->ra[jb];
	  if(a->csc == 0)
	    initCS(csb, BL->block_index[i+1] - BL->block_index[i]); 
	  else
	    initCS(csb, BL->block_index[j+1] - BL->block_index[j]); 
	}

      ja = a->ria[i];
      for(jb=b->ria[i];jb<b->ria[i+1];jb++)
	{
	  j = b->rja[jb];
	  while(ja < a->ria[i+1])
	    {
	      if(a->rja[ja] >= j)
		break;
	      ja++;
	    }
	  if(ja >= a->ria[i+1])
	    break;

	  if(a->rja[ja] == j)
	    {
	      csa = a->ra[ja];
	      csb = b->ra[jb];

	      CS_Move(csa, csb); 
	      /*CS_Copy(csa, csb);
		reinitCS(csa);*/
	      
	      /*cleanCS(csa);*/ /* Fait foirer PhidalMatrix_Check **/
	      ja++;
	    }
	}
    }

#ifdef DEBUG_M
  /*PhidalMatrix_Check(b, BL);*/
#endif
}



void PhidalMatrix_BuildVirtualMatrix(dim_t tli, dim_t tlj, dim_t bri, dim_t brj, PhidalMatrix *a, PhidalMatrix *v, PhidalHID *BL)
{
  /********************************************************/
  /* This function copy a phidal matrix a in a virtual    */
  /* PhidalMatrix which all cs block are pointer to those */
  /* in the original matrix                               */
  /* NB: the scaling is not copied                        */
  /********************************************************/
  dim_t i, j;
  dim_t nnb;
  dim_t jcol, jrow;

#ifdef DEBUG_M
  assert(tli >= a->tli);
  assert(tlj >= a->tlj);
  assert(bri <= a->bri);
  assert(brj <= a->brj);
#endif

  v->tli = tli;
  v->tlj = tlj;
  v->bri = bri;
  v->brj = brj;

  v->dim1 = BL->block_index[bri+1]-BL->block_index[tli]; 
  v->dim2 = BL->block_index[brj+1]-BL->block_index[tlj]; 
  v->virtual = 1;
  v->bloktab = NULL;
  
  if(tli == tlj && bri == brj)
    v->symmetric = a->symmetric;
  else
    v->symmetric = 0;
  v->csc = a->csc;


  if( (bri-tli+1)*(brj+1)  <= (brj-tlj+1)*(bri+1))
    {
      /******* Construct the pattern by row **********/

      /** Count number of block to create in v **/
      /* We also count the number of element by column */
      /* and store it in cia cia[i+1] will be the number of
	 element in the column i */
      nnb = 0;
      v->ria = (INTL *)malloc(sizeof(INTL) * (BL->nblock+1));
      v->cia = (INTL *)malloc(sizeof(INTL) * (BL->nblock+1));
      bzero(v->cia + tlj, sizeof(INTL)* (brj-tlj+1));
      for(i=tli;i<=bri;i++)
	{
	  v->ria[i] = nnb;
	  for(j=a->ria[i];j<a->ria[i+1];j++)
	    if(a->rja[j] >= tlj)
	      break;
	  for(;j<a->ria[i+1];j++)
	    {
	      jcol = a->rja[j];
	      if(jcol > brj)
		break;
	      v->cia[jcol+1]++;
	      nnb++;
	    }
	}
      v->ria[bri+1] = nnb;

      /** Get the true cia vector **/
      for(i=tlj+1;i<=brj+1;i++)
	v->cia[i] += v->cia[i-1];

      v->bloknbr = nnb;


      if(v->bloknbr > 0)
	{
	  v->rja = (dim_t *)malloc(sizeof(dim_t) * v->bloknbr);
	  v->ra = (csptr *)malloc(sizeof(csptr) * v->bloknbr);
	  v->cja = (dim_t *)malloc(sizeof(dim_t) * v->bloknbr);
	  v->ca = (csptr *)malloc(sizeof(csptr) * v->bloknbr);
	}

      /** Copy pointer to the cs blocks **/
      nnb = 0;
      for(i=tli;i<=bri;i++)
	{
	  v->ria[i] = nnb;
	  for(j=a->ria[i];j<a->ria[i+1];j++)
	    if(a->rja[j] >= tlj)
	      break;
	  
	  for(;j<a->ria[i+1];j++)
	    {
	      jcol = a->rja[j];
	      if(jcol > brj)
		break;
	      v->rja[nnb] = jcol;
	      v->ra[nnb] = a->ra[j];
	      nnb++;
	      v->cja[ v->cia[jcol] ] = i;
	      v->ca[  v->cia[jcol]++] = a->ra[j];
	    }
	}
      
      /** Reconstruct cia **/
      for(i=brj+1;i>tlj;i--)
	v->cia[i] = v->cia[i-1];
      v->cia[tlj] = 0;

#ifdef DEBUG_M
      assert(nnb == v->bloknbr);
      assert(nnb == v->cia[brj+1]);
#endif
    }
  else
    {
      /******* Construct the pattern by column **********/
       /** Count number of block to create in v **/
      /* We also count the number of element by row */
      /* and store it in ria ria[i+1] will be the number of
	 element in the column i */
      nnb = 0;
      v->ria = (INTL *)malloc(sizeof(INTL) * (BL->nblock+1));
      v->cia = (INTL *)malloc(sizeof(INTL) * (BL->nblock+1));
      bzero(v->ria + tli, sizeof(INTL)* (bri-tli+1));
      for(i=tlj;i<=brj;i++)
	{
	  v->cia[i] = nnb;
	  for(j=a->cia[i];j<a->cia[i+1];j++)
	    if(a->cja[j] >= tli)
	      break;
	  for(;j<a->cia[i+1];j++)
	    {
	      jrow = a->cja[j];
	      if(jrow > bri)
		break;
	      v->ria[jrow+1]++;
	      nnb++;
	    }
	}
      v->cia[brj+1] = nnb;

      /** Get the true ria vector **/
      for(i=tli+1;i<=bri+1;i++)
	v->ria[i] += v->ria[i-1];

      v->bloknbr = nnb;


      if(v->bloknbr > 0)
	{
	  v->rja = (dim_t *)malloc(sizeof(dim_t) * v->bloknbr);
	  v->ra = (csptr *)malloc(sizeof(csptr) * v->bloknbr);
	  v->cja = (dim_t *)malloc(sizeof(dim_t) * v->bloknbr);
	  v->ca = (csptr *)malloc(sizeof(csptr) * v->bloknbr);
	}

      /** Copy pointer to the cs blocks **/
      nnb = 0;
      for(i=tlj;i<=brj;i++)
	{
	  v->cia[i] = nnb;
	  for(j=a->cia[i];j<a->cia[i+1];j++)
	    if(a->cja[j] >= tli)
	      break;
	  
	  for(;j<a->cia[i+1];j++)
	    {
	      jrow = a->cja[j];
	      if(jrow > bri)
		break;
	      v->cja[nnb] = jrow;
	      v->ca[nnb] = a->ca[j];
	      nnb++;
	      v->rja[ v->ria[jrow] ] = i;
	      v->ra[  v->ria[jrow]++] = a->ca[j];
	    }
	}
      
      /** Reconstruct ria **/
      for(i=bri+1;i>tli;i--)
	v->ria[i] = v->ria[i-1];
      v->ria[tli] = 0;

#ifdef DEBUG_M
      assert(nnb == v->bloknbr);
      assert(nnb == v->ria[bri+1]);
#endif
    
    }

#ifdef DEBUG_M
  PhidalMatrix_Check(v, BL);
#endif
  
  
  
}


void PhidalMatrix_Realloc(PhidalMatrix *m)
{
  /***********************************************************/
  /* This function realloc each sparse block of the matrix   */
  /* contiguously in the memory space                        */
  /* That optimizes the memory space usage and can accelerate*/
  /* the matrix operation                                    */
  /***********************************************************/
  dim_t i;
  
  for(i=0;i<m->bloknbr;i++)
    CSrealloc(m->ra[i]);
  
}


long PhidalMatrix_NNZ(PhidalMatrix *m)
{
  /*********************************************/
  /* Return the number of non zero element in  */
  /* a PHIDAL matrix                           */
  /*********************************************/
  /** @@@OIMBE: Attention matrix symetrique non prise en compte **/
  long nnz;
  dim_t i;

  nnz = 0;

  for(i=0;i<m->bloknbr;i++)
    nnz += CSnnz(m->ra[i]);
  return nnz;
}

REAL  PhidalMatrix_NormFrob(PhidalMatrix *m)
{
  /*******************************************/
  /* Return the Frobenius norm of the matrix */
  /*******************************************/
  /** @@@OIMBE: Attention matrix symetrique non prise en compte **/
  REAL norm;
  dim_t i;
  norm = 0.0;
  for(i=0;i<m->bloknbr;i++)
    norm += CSnormSquare(m->ra[i]);
  
  return sqrt(norm);
}

REAL  PhidalMatrix_Norm1(PhidalMatrix *m)
{
  /***********************************/
  /* Return the norm 1 of the matrix */
  /***********************************/
  REAL norm;
  dim_t i;
  norm = 0.0;
  for(i=0;i<m->bloknbr;i++)
    norm += CSnorm1(m->ra[i]);
  
  return norm;
}



void PhidalMatrix_ColPerm(PhidalMatrix *m, dim_t *permtab, PhidalHID *BL)
{
  dim_t j, k;
  dim_t *permptr;
  if(m->csc == 0)
    {
      for(j=m->tlj; j<=m->brj;j++)
	{
	  permptr = permtab + BL->block_index[j]-BL->block_index[m->tlj];
	  for(k=m->cia[j];k<m->cia[j+1];k++)
	    CS_ColPerm(m->ca[k], permptr);
	}
    }
  else
    {
      /***** The matrices are stored in CSC format ****/
      for(j=m->tlj; j<=m->brj;j++)
	{
	  permptr = permtab + BL->block_index[j]-BL->block_index[m->tlj];
	  for(k=m->cia[j];k<m->cia[j+1];k++)
	    CS_RowPerm(m->ca[k], permptr);
	}
    }
}

void PhidalMatrix_RowPerm(PhidalMatrix *m, dim_t *permtab, PhidalHID *BL)
{
  dim_t i, k;
  dim_t *permptr;

  if(m->csc == 0)
    {
      for(i=m->tli; i<=m->bri;i++)
	{
	  permptr = permtab + BL->block_index[i]-BL->block_index[m->tli];
	  for(k=m->ria[i];k<m->ria[i+1];k++)
	    CS_RowPerm(m->ra[k], permptr);
	}
    }
  else
    {
      /***** The matrices are stored in CSC format ****/
      for(i=m->tli; i<=m->bri;i++)
	{
	  permptr = permtab + BL->block_index[i]-BL->block_index[m->tli];
	  for(k=m->ria[i];k<m->ria[i+1];k++)
	    CS_ColPerm(m->ra[k], permptr);
	}
    }
}




void PhidalMatrix_Transpose(PhidalMatrix *M)
{
  /********************************************************************/
  /* This function transpose a phidal matrix                          */
  /* exemple: if M is an upper phidal matrix in CSR block matrices    */
  /*  then it becomes a lower phidal matrix in CSC block matrices     */
  /********************************************************************/
  dim_t tmp;
  INTL *tmpptr;
  dim_t *tmpptr2;
  struct SparRow **tmpcsr;

  /** The principle is really simple: the block rows of the phidal matrix 
      becomes the column of the transpose phidal matrix **/
  tmp = M->dim1;
  M->dim1 = M->dim2;
  M->dim2 = tmp;

  tmp = M->tli;
  M->tli = M->tlj;
  M->tlj = tmp;

  tmp = M->bri;
  M->bri = M->brj;
  M->brj = tmp;

  
  tmpptr = M->ria;
  M->ria = M->cia;
  M->cia = tmpptr;
  
  tmpptr2 = M->rja;
  M->rja = M->cja;
  M->cja = tmpptr2;
  
  tmpcsr = M->ra;
  M->ra = M->ca;
  M->ca = tmpcsr;

  

  if(M->csc == 0)
    M->csc = 1;
  else
    M->csc = 0;

}

void PhidalMatrix_Transpose_SparMat(PhidalMatrix* m, flag_t job, char *UPLO, PhidalHID* BL) 
{
  /********************************************************************/
  /* This function transpose blocks of a phidal matrix                */
  /* exemple: if M is a phidal matrix in CSR block matrices           */
  /*  then it becomes a phidal matrix in CSC block matrices           */
  /********************************************************************/

  dim_t i,j;
  dim_t coldim;
  
  for(i=m->tlj;i<=m->brj;i++) {
    for(j=m->cia[i];j<m->cia[i+1];j++) {

      /* printfd("** %d (%d) %d\n", m->ca[j]->n, */
      /* 	     BL->block_index[i+1] - BL->block_index[i], */
      /* 	     BL->block_index[m->cja[j]+1] - BL->block_index[m->cja[j]]); */

      if ((strcmp(UPLO, "N") == 0) 
	  || ((strcmp(UPLO, "U") == 0) && (i>=m->cja[j]))  /* que le haut */
	  || ((strcmp(UPLO, "L") == 0) && (i<=m->cja[j]))) /* que le bas */
	{ 
	  assert(BL->block_index[m->cja[j]+1] - BL->block_index[m->cja[j]] == m->ca[j]->n);
	  
	  coldim = BL->block_index[i+1] - BL->block_index[i];
	  
	  if (job != 3) /* see CS_Transpose declaration */
	    CS_Transpose(job, m->ca[j], coldim);
	  else /* don't call CSrealloc on diag block */
	    CS_Transpose(1 + (m->cja[j] == i), m->ca[j], coldim);
	}
    }
  }
}


/*** For debugging **/
void PhidalMatrix_PrintBLockNNZ(FILE *file, PhidalMatrix *A)
{
  dim_t i, j;
  for(i=A->tli;i<=A->bri;i++)
    for(j=A->ria[i];j<A->ria[i+1];j++)
      {
	fprintfd(file, "[%d %d]= %ld  ", i, A->rja[j], CSnnz(A->ra[j]));
	fprintfd(file, "\n");
      }
}

/*** For debugging **/
void PhidalMatrix_Print(FILE *file, PhidalMatrix *A)
{
  dim_t i, j;
  for(i=A->tli;i<=A->bri;i++)
    for(j=A->ria[i];j<A->ria[i+1];j++)
      {
	fprintfd(file, "BLOCK [%d, %d] =  \n", i, A->rja[j]);
	dumpCS(0, file, A->ra[j]);
	fprintfd(file, "\n");
      }
}


void PhidalMatrix_GetUdiag(PhidalMatrix *U, COEF *diag)
{
  /****************************************************************************/
  /* This function return the diagonal of a upper triangular phidal matrix    */
  /****************************************************************************/
  dim_t i, k;
  csptr csU;
  dim_t ind;
  ind = 0;
  for(i=U->tli;i<=U->bri;i++)
    {
#ifdef DEBUG_M
      assert(U->rja[U->ria[i]] == i);
#endif
      csU = U->ra[U->ria[i]];
      for(k=0;k<csU->n;k++)
	{
#ifdef DEBUG_M
	  assert(csU->ja[k][0] == k);
#endif
	  diag[ind++] = csU->ma[k][0];
	}
    }
}

void PhidalMatrix_SetUdiag(PhidalMatrix *U, COEF *diag)
{
  /****************************************************************************/
  /* This function return the diagonal of a upper triangular phidal matrix    */
  /****************************************************************************/
  dim_t i, k;
  csptr csU;
  dim_t ind;
  ind = 0;
  for(i=U->tli;i<=U->bri;i++)
    {
#ifdef DEBUG_M
      assert(U->rja[U->ria[i]] == i);
#endif
      csU = U->ra[U->ria[i]];
      for(k=0;k<csU->n;k++)
	{
#ifdef DEBUG_M
	  assert(csU->ja[k][0] == k);
#endif
	  csU->ma[k][0] = diag[ind++];
	}
    }
}

void PhidalMatrix_RowMult(COEF *vec, PhidalMatrix *A, PhidalHID *BL)
{
  dim_t i, k;
  COEF *vecptr;

  assert(A->virtual == 0);

  if(A->csc == 0)
    {
      for(i=A->tli;i<=A->bri;i++)
	{
	  vecptr = vec + BL->block_index[i] -BL->block_index[A->tli];
	  for(k=A->ria[i];k<A->ria[i+1];k++)
	    CS_RowMult(vecptr, A->ra[k]);
	}
    }
  else
    { /** The matrices are in CSC format **/
      for(i=A->tli;i<=A->bri;i++)
	{
	  vecptr = vec + BL->block_index[i] -BL->block_index[A->tli];
	  for(k=A->ria[i];k<A->ria[i+1];k++)
	    CS_ColMult(vecptr, A->ra[k]);
	}
    }

}

void PhidalMatrix_RowMult2(REAL *vec, PhidalMatrix *A, PhidalHID *BL)
{
  dim_t i, k;
  REAL *vecptr;

  assert(A->virtual == 0);

  if(A->csc == 0)
    {
      for(i=A->tli;i<=A->bri;i++)
	{
	  vecptr = vec + BL->block_index[i] -BL->block_index[A->tli];
	  for(k=A->ria[i];k<A->ria[i+1];k++)
	    CS_RowMult2(vecptr, A->ra[k]);
	}
    }
  else
    { /** The matrices are in CSC format **/
      for(i=A->tli;i<=A->bri;i++)
	{
	  vecptr = vec + BL->block_index[i] -BL->block_index[A->tli];
	  for(k=A->ria[i];k<A->ria[i+1];k++)
	    CS_ColMult2(vecptr, A->ra[k]);
	}
    }

}


void PhidalMatrix_ColMult(COEF *vec, PhidalMatrix *A, PhidalHID *BL)
{
  dim_t j, k;
  COEF *vecptr;

  assert(A->virtual == 0);

  
  if(A->csc == 0)
    {
      for(j=A->tlj;j<=A->brj;j++)
	{
	  vecptr = vec + BL->block_index[j] -BL->block_index[A->tlj];
	  for(k=A->cia[j];k<A->cia[j+1];k++)
	    CS_ColMult(vecptr, A->ca[k]);
	}
    }
  else
    {/** The matrices are in CSC format **/
      for(j=A->tlj;j<=A->brj;j++)
	{
	  vecptr = vec + BL->block_index[j] -BL->block_index[A->tlj];
	  for(k=A->cia[j];k<A->cia[j+1];k++)
	    CS_RowMult(vecptr, A->ca[k]);
	}
    }
}

void PhidalMatrix_ColMult2(REAL *vec, PhidalMatrix *A, PhidalHID *BL)
{
  dim_t j, k;
  REAL *vecptr;

  assert(A->virtual == 0);

  
  if(A->csc == 0)
    {
      for(j=A->tlj;j<=A->brj;j++)
	{
	  vecptr = vec + BL->block_index[j] -BL->block_index[A->tlj];
	  for(k=A->cia[j];k<A->cia[j+1];k++)
	    CS_ColMult2(vecptr, A->ca[k]);
	}
    }
  else
    {/** The matrices are in CSC format **/
      for(j=A->tlj;j<=A->brj;j++)
	{
	  vecptr = vec + BL->block_index[j] -BL->block_index[A->tlj];
	  for(k=A->cia[j];k<A->cia[j+1];k++)
	    CS_RowMult2(vecptr, A->ca[k]);
	}
    }
}

REAL PhidalMatrix_RowDensity(PhidalMatrix *m)
{
  dim_t i, k;
  long totalrow = 0;
  long nnrow = 0;
  csptr b;

  for(i=m->tli;i<=m->bri;i++)
    for(k=m->ria[i];k<m->ria[i+1];k++)
      {
	b = m->ra[k];
	totalrow += b->n;
	nnrow += b->nnzr;
      }
  
  return ((REAL)nnrow/(REAL)totalrow);
}

void PhidalMatrix_SymScale(PhidalMatrix *A, PhidalHID *BL, REAL *scaletab, REAL *iscaletab)
{
  dim_t i;
#ifdef DEBUG_M
  assert(A->dim1 == A->dim2);
#endif

  if(A->symmetric == 1)
    {
      PhidalMatrix_ColNorm2(A, BL, iscaletab);
      for(i=0;i<A->dim1;i++)
	iscaletab[i] = sqrt(iscaletab[i]);
    }
  else
    {
      PhidalMatrix_ColNorm2(A, BL, iscaletab);
      PhidalMatrix_RowNorm2(A, BL, scaletab); /** Use scaletab as temp **/
      for(i=0;i<A->dim1;i++)
	iscaletab[i] = sqrt(sqrt(iscaletab[i])*sqrt(scaletab[i]));
    }
  
  for(i=0;i<A->dim1;i++)
    scaletab[i] = 1.0/iscaletab[i];
  
  PhidalMatrix_RowMult2(scaletab, A, BL);
  PhidalMatrix_ColMult2(scaletab, A, BL);
}

void PhidalMatrix_UnsymScale(dim_t nbr, PhidalMatrix *A, PhidalHID *BL, REAL *scalerow, REAL *scalecol, REAL *iscalerow, REAL *iscalecol)
{
  dim_t i, k;
  REAL *scalerowtmp, *iscalerowtmp, *scalecoltmp, *iscalecoltmp;
#ifdef DEBUG_M
  assert(A->symmetric == 0);
#endif
  if(nbr > 1)
    { 
      for(i=0;i<A->dim1;i++)
	{
	  iscalecol[i] = 1.0;
	  iscalerow[i] = 1.0;
	}

      iscalecoltmp = (REAL *)malloc(sizeof(REAL) * A->dim2);
      iscalerowtmp = (REAL *)malloc(sizeof(REAL) * A->dim1);
      scalerowtmp = (REAL *)malloc(sizeof(REAL) * A->dim1);
      scalecoltmp = (REAL *)malloc(sizeof(REAL) * A->dim2);
    }
  else
    {
      iscalecoltmp = iscalecol;
      scalecoltmp = scalecol;
      iscalerowtmp = iscalerow;
      scalerowtmp = scalerow;
    }
  
  for(k=0;k<nbr;k++)
    {
      /*** Unsymetric scale ***/
      PhidalMatrix_ColNorm2(A, BL, iscalecoltmp);
      for(i=0;i<A->dim1;i++)
	scalecoltmp[i] = 1.0/iscalecoltmp[i];
      PhidalMatrix_ColMult2(scalecoltmp, A, BL);
      
      PhidalMatrix_RowNorm2(A, BL, iscalerowtmp);
      for(i=0;i<A->dim1;i++)
	scalerowtmp[i] = 1.0/iscalerowtmp[i];
      PhidalMatrix_RowMult2(scalerowtmp, A, BL);

      if(nbr > 1)
	for(i=0;i<A->dim1;i++)
	  {
	    iscalecol[i] *= iscalecoltmp[i];
	    iscalerow[i] *= iscalerowtmp[i];
	  }

    }
  
  if(nbr > 1)
    {
      for(i=0;i<A->dim1;i++)
	{
	  scalecol[i] = 1.0/iscalecol[i];
	  scalerow[i] = 1.0/iscalerow[i];
	}
      
      free(iscalecoltmp);
      free(scalecoltmp);
      free(iscalerowtmp);
      free(scalerowtmp);
    } 
  
}

void PhidalMatrix2SparRow(flag_t job, PhidalMatrix *M, csptr mat, PhidalHID *BL)
{
  /*************************************************/
  /* This function transform a PhidalMatrix        */
  /* into one single SparRow matrix                */
  /* On return the SparRow matrix is oriented in   */
  /* the same way than the PhidalMatrix ; ie it is */
  /* a CSC or a CSR depending on M->csc flag       */
  /* job = 0: the matrix M is left unchanged       */
  /*     = 1: the matrix M contains no coefficient */
  /*          on return                            */
  /* NOTE: mat is initCS'ed inside this function   */
  /*************************************************/
  dim_t i, j, k, r, s;
  dim_t *jptr;
  COEF *aptr;
  csptr bi;
  dim_t offseti, offsetj;

  if(M->csc == 1)
    {
      initCS(mat, M->dim2);
      for(j=M->tlj;j<=M->brj;j++)
	{
	  offsetj = BL->block_index[j] - BL->block_index[M->tlj];

	  /*** Count the non-zeros in the block-column j ***/ 
	  for(k=M->cia[j];k<M->cia[j+1];k++)
	    {
	      bi = M->ca[k]; 
	      for(i=0;i<bi->nnzr;i++)
		{
		  r =  bi->nzrtab[i];
		  mat->nnzrow[r+offsetj] += bi->nnzrow[r];
		}
	    }
	  r = BL->block_index[j+1]-BL->block_index[j];
	  
	  for(i=offsetj;i<offsetj+r;i++)
	    if(mat->nnzrow[i] > 0)
	      {
		mat->ja[i] = (dim_t *)malloc(sizeof(dim_t)*mat->nnzrow[i]);
		mat->ma[i] = (COEF *)malloc(sizeof(COEF)*mat->nnzrow[i]);
	      }
	  
	  /*** Copy the non-zeros of column block j ***/
	  bzero( mat->nnzrow+offsetj, sizeof(dim_t)*(BL->block_index[j+1]-BL->block_index[j]));
	  for(k=M->cia[j];k<M->cia[j+1];k++)
	    {
	      i = M->cja[k];
	      offseti = BL->block_index[i]-BL->block_index[M->tli];
	      bi = M->ca[k]; 
	      for(i=0;i<bi->nnzr;i++)
		{
		  r =  bi->nzrtab[i];
		  jptr = mat->ja[r+offsetj] + mat->nnzrow[r+offsetj];
		  aptr = mat->ma[r+offsetj] + mat->nnzrow[r+offsetj];
		  for(s = 0; s < bi->nnzrow[r]; s++)
		    jptr[s] = bi->ja[r][s] + offseti;
		  memcpy(aptr, bi->ma[r], sizeof(COEF) * bi->nnzrow[r]); 

		  mat->nnzrow[r+offsetj] += bi->nnzrow[r];
		}
	    }

	  if(job == 1)
	    {
	      /** Destroy the column block j ***/
	      for(k=M->cia[j];k<M->cia[j+1];k++)
		reinitCS(M->ca[k]);
	    }
	}
    }
  else
    {
      initCS(mat, M->dim1);
      for(i=M->tli;i<=M->bri;i++)
	{
	  offseti = BL->block_index[i] - BL->block_index[M->tli];
	  
	  /*** Count the non-zeros in the block-column j ***/ 
	  for(k=M->ria[i];k<M->ria[i+1];k++)
	    {
	      bi = M->ra[k]; 
	      for(j=0;j<bi->nnzr;j++)
		{
		  r =  bi->nzrtab[j];
		  mat->nnzrow[r+offseti] += bi->nnzrow[r];
		}
	    }
	  r = BL->block_index[i+1]-BL->block_index[i];
	  
	  for(j=offseti;j<offseti+r;j++)
	    if(mat->nnzrow[j] > 0)
	      {
		mat->ja[j] = (dim_t *)malloc(sizeof(dim_t)*mat->nnzrow[j]);
		mat->ma[j] = (COEF *)malloc(sizeof(COEF)*mat->nnzrow[j]);
	      }
	  
	  /*** Copy the non-zeros of column block j ***/
	  bzero( mat->nnzrow+offseti, sizeof(dim_t)*(BL->block_index[i+1]-BL->block_index[i]));
	  for(k=M->ria[i];k<M->ria[i+1];k++)
	    {
	      j = M->rja[k];
	      offsetj = BL->block_index[j]-BL->block_index[M->tlj];
	      bi = M->ra[k]; 
	      for(j=0;j<bi->nnzr;j++)
		{
		  r =  bi->nzrtab[j];
		  jptr = mat->ja[r+offseti] + mat->nnzrow[r+offseti];
		  aptr = mat->ma[r+offseti] + mat->nnzrow[r+offseti];
		  for(s = 0; s < bi->nnzrow[r]; s++)
		    jptr[s] = bi->ja[r][s] + offsetj;
		  memcpy(aptr, bi->ma[r], sizeof(COEF) * bi->nnzrow[r]); 

		  mat->nnzrow[r+offseti] += bi->nnzrow[r];
		}
	    }

	  if(job == 1)
	    {
	      /** Destroy the column block j ***/
	      for(k=M->ria[i];k<M->ria[i+1];k++)
		reinitCS(M->ra[k]);
	    }
	}

    }

  CS_SetNonZeroRow(mat);	
}

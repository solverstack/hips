/* @authors P. HENON */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>

#include "phidal_sequential.h"


void PhidalPrec_GEMM_ILUCT(int_t levelnum, REAL *droptab, PhidalMatrix *G, PhidalMatrix *W, PhidalMatrix *A, 
			   PhidalPrec *P, PhidalHID *BL, PhidalOptions *option)
{
  PhidalMatrix *VL, *VU;
  chrono_t t1, t2;
  dim_t i;

#ifdef DEBUG_M
  assert(option->schur_method != 1);
  assert(A->tli == A->tlj);
  assert(A->bri == A->brj);
  assert(G->tli == A->tli);
  assert(G->bri == A->bri);
  assert(G->brj == A->tlj-1);
  assert(W->tlj == A->tlj);
  assert(W->brj == A->brj);
  assert(W->bri == A->tli-1);
  assert(levelnum == option->forwardlev);
#endif

  t1 = dwalltime();
  P->dim = A->dim1;
  P->symmetric = option->symmetric;
  P->schur_method = 2;
  P->forwardlev = 0;
  P->levelnum =  levelnum;

 
  /** IMPORTANT: Cut U before L **/
  VU = (PhidalMatrix *)malloc(sizeof(PhidalMatrix));
  PhidalMatrix_Init(VU);
  PhidalMatrix_Setup(G->tlj, G->tlj, A->bri, A->bri, "U", "N", option->locally_nbr, VU, BL);
  PhidalMatrix_Cut(A, VU, BL);


  VL = (PhidalMatrix *)malloc(sizeof(PhidalMatrix));
  PhidalMatrix_Init(VL);
  PhidalMatrix_Setup(G->tlj, G->tlj, A->bri, A->bri, "L", "N", option->locally_nbr, VL, BL);
  PhidalMatrix_Cut(A, VL, BL);
  /*** Reset the block diagonal to null matrices **/
  for(i=A->tli;i<=A->brj;i++)
    reinitCS(VL->ca[ VL->cia[i]]);

#ifdef DEBUG_M
  assert(G->dim2+A->dim2 == VL->dim2);
  assert(W->dim1+A->dim1 == VL->dim2);
#endif
  PhidalMatrix_Cut(G, VL, BL);
  PhidalMatrix_Cut(W, VU, BL);

  PhidalMatrix_ILUCT_Restrict(A->tli, VL, VU, option->droptol1, droptab, option->fillrat, BL, option, P->info);

  M_MALLOC(P->LU, SCAL);
  PREC_L_SCAL(P) = (PhidalMatrix *)malloc(sizeof(PhidalMatrix));
  PhidalMatrix_Setup(A->tli, A->tli, A->bri, A->bri, "L", "N", option->locally_nbr, PREC_L_SCAL(P), BL);
  PhidalMatrix_Cut(VL, PREC_L_SCAL(P), BL);
  PREC_U_SCAL(P) = (PhidalMatrix *)malloc(sizeof(PhidalMatrix));
  PhidalMatrix_Setup(A->tli, A->tli, A->bri, A->bri, "U", "N", option->locally_nbr, PREC_U_SCAL(P), BL);
  PhidalMatrix_Cut(VU, PREC_U_SCAL(P), BL);

  /*** OIMBE attention retirer VL de mem si corrige pas peak dans GEMM_I..T **/

  PhidalMatrix_Clean(VL);
  PhidalMatrix_Clean(VU);
  free(VL);
  free(VU);

  t2 = dwalltime();
  fprintfv(5, stderr, "Time in GEMM_ICCT = %g \n", t2-t1);
}

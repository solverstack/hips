/* @authors P. HENON */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>

#include "phidal_sequential.h"

void PhidalMatrix_ILUCT(PhidalMatrix *L, PhidalMatrix *U, REAL droptol, REAL *droptab, REAL fillrat, PhidalHID *BL, PhidalOptions *option, PrecInfo *info)
{
  /******************************************************************************************/
  /* This function computes the incomplete factorization A = L.U of a  matrix               */
  /* This is a similar function to PhidalMatrix_ILUTP (without pivoting)                    */
  /* BUT the block algorithm is made column wisely as in ILUC                               */
  /* On entry:                                                                              */
  /*   L is the lower triangular part of the matrix to factorize in CSR format              */
  /*   U is the upper triangular part of the matrix to factorize in CSR format              */
  /* on return:                                                                             */
  /* L the strictly lower triangular factor (unitary on the diagonal not stored) in CSR     */
  /* U the upper triangular factor (unitary on the diagonal not stored) in CSR              */
  /******************************************************************************************/
  PhidalMatrix_ILUCT_Restrict(L->tli, L, U, droptol, droptab, fillrat, BL, option, info);
}

void PhidalMatrix_ILUCT_Restrict(int START, PhidalMatrix *L, PhidalMatrix *U, REAL droptol, REAL *droptab, REAL fillrat, PhidalHID *BL, PhidalOptions *option, PrecInfo *info)
{
  dim_t i, k;
  int ii, jj, kk;
  int *wki1, *wki2;
  COEF *wkd;
  csptr csL, csU;
  csptr *csrtab1;
  csptr *csrtab2;
  int nnb;
  Heap heap;
  REAL *droptabtmp;
  REAL *dropptr;
  
  CellCS *Lfirstcol, *Ufirstrow;
  csptr *list1, *list2;
  int *listindex;
  int nk, *jak;
  csptr *rak;


#ifdef DEBUG_M
  PhidalMatrix_Check(L, BL);
  PhidalMatrix_Check(U, BL);
  assert(START >= L->tli);
  assert(START <= L->bri);
#endif


  /*** Find the largest diagonal block in this matrix ****/
  ii = 0;
  /*for(i=L->tli;i<=L->bri;i++)*/
  for(i=START;i<=L->bri;i++)
    if(BL->block_index[i+1]-BL->block_index[i] > ii)
      ii = BL->block_index[i+1]-BL->block_index[i];

  Heap_Init(&heap, ii);
  wki1= (int *)malloc(sizeof(int)*ii);
  wki2= (int *)malloc(sizeof(int)*ii);
  wkd = (COEF *)malloc(sizeof(COEF)*ii);


  ii = 0;
  /*for(k=L->tli;k<=L->bri;k++)*/
  for(k=START;k<=L->bri;k++)
    if(L->ria[k+1] - L->ria[k] > ii)
      ii = L->ria[k+1] - L->ria[k];
  /*for(k=U->tlj;k<=U->brj;k++)*/
  for(k=START;k<=U->brj;k++)
    if(U->cia[k+1] - U->cia[k] > ii)
      ii = U->cia[k+1] - U->cia[k];
  if(ii>0)
    {
      jak = (int *)malloc(sizeof(int)*ii);
      rak = (csptr *)malloc(sizeof(csptr)*ii);
    }
  else
    {
      jak = NULL;
      rak = NULL;
    }

  ii=0;
  /*for(k=L->tlj;k<=L->brj;k++)*/
  for(k=START;k<=L->brj;k++)
    {
      nnb = (L->ria[k+1]-L->ria[k] - 1) * (U->ria[k+1]-U->ria[k]);
      if( nnb > ii)
	ii = nnb;
      nnb = (L->cia[k+1]-L->cia[k] - 1) * (U->cia[k+1]-U->cia[k]);
      if( nnb > ii)
	ii = nnb;
    }

  list1 = (csptr *)malloc(sizeof(csptr)* (ii+1));
  list2 = (csptr *)malloc(sizeof(csptr)* (ii+1));
  /*listindex = (int *)malloc(sizeof(int)*(L->bri-L->tli+1));*/
  listindex = (int *)malloc(sizeof(int)*(L->bri-START+1));

  Lfirstcol = (CellCS *)malloc(sizeof(CellCS)*(L->brj+1));
  for(k=L->tlj;k<=L->brj;k++)
    {
      Lfirstcol[k].nnz = L->cia[k+1]-L->cia[k];
      Lfirstcol[k].ja  = L->cja + L->cia[k];
      Lfirstcol[k].ma  = L->ca + L->cia[k];
    }
  Ufirstrow = (CellCS *)malloc(sizeof(CellCS)*(U->bri+1));
  for(k=U->tli;k<=U->bri;k++)
    {
      Ufirstrow[k].nnz = U->ria[k+1]-U->ria[k];
      Ufirstrow[k].ja  = U->rja + U->ria[k];
      Ufirstrow[k].ma  = U->ra + U->ria[k];
    }
  

#ifdef SYMMETRIC_DROP
  droptabtmp = (REAL *)malloc(sizeof(REAL)*L->dim1);
  if(droptab != NULL)
    memcpy(droptabtmp, droptab, sizeof(REAL)*L->dim1);
  else
    for(i=0;i<L->dim1;i++)
      droptabtmp[i] = 1.0;
#else
  droptabtmp = droptab;
#endif
  
  /*for(k=L->tlj;k<=L->brj;k++)*/
  for(k=START;k<=L->brj;k++)
    {
      /*** Factorize the column block k of L and the row k of U ***/
      csL =  L->ca[ L->cia[k] ];
      csU =  U->ra[ U->ria[k] ];
#ifdef DEBUG_M
      assert(L->cja[L->cia[k]] == k);
      assert(U->rja[U->ria[k]] == k);
#endif
      
      if(k>L->tlj)
	{
	  
	  /**********************************************/
	  /* Compute the column-block L(k+1:n,k)        */
	  /**********************************************/
	  nk = 0;
	  for(jj=U->cia[k];jj<U->cia[k+1]-1;jj++)
	    if(U->ca[jj]->nnzr > 0)
	      {
		jak[nk] = U->cja[jj];
		rak[nk] = U->ca[jj];
		nk++;
	      }

	  if(nk>0)
	    {

	      CellCS_ListUpdate(Lfirstcol, k+1,  nk,  jak);
	      CellCS_IntersectList(Lfirstcol, nk,  jak, rak, L->cia[k+1]-L->cia[k]-1, L->cja+L->cia[k]+1, 
				   listindex, list1, list2);
	      
	      for(ii=0;ii<L->cia[k+1]-L->cia[k]-1;ii++)
		{
		  i = L->cja[ii+L->cia[k]+1];
		  
		  /*** Compute L(i,k) = L(i,k) - L(i, 0:k-1).L(k, 0:k-1)t **/	      
		  /*** NO DROPPING HERE ***/
		  csrtab1 = list1 + ii*nk;
		  csrtab2 = list2 + ii*nk;
		  nnb     = listindex[ii];
		  if(nnb>0)
		    {
		      if(info != NULL)
			PrecInfo_SubNNZ(info, CSnnz(L->ca[ii+L->cia[k]+1]));
		      CSRrowMultCSRcol(0.0, NULL, -1.0, nnb, -1.0, csrtab1, csrtab2, 
				     L->ca[ii+L->cia[k]+1], BL->block_index[k+1]-BL->block_index[k], wki1, wki2, wkd); /*** NO DROPPING HERE ***/
		      if(info != NULL)
			PrecInfo_AddNNZ(info, CSnnz(L->ca[ii+L->cia[k]+1]));
		    }
		}
	    }


	  /**********************************************/
	  /* Compute the row-block U(k, k:n)            */
	  /**********************************************/
	  nk = 0;
	  for(jj=L->ria[k];jj<L->ria[k+1]-1;jj++)
	    if(L->ra[jj]->nnzr > 0)
	      {
		jak[nk] = L->rja[jj];
		rak[nk] = L->ra[jj];
		nk++;
	      }
	  
	  if(nk>0)
	    {

	      CellCS_ListUpdate(Ufirstrow, k,  nk,  jak);
	      CellCS_IntersectList(Ufirstrow, nk,  jak, rak, U->ria[k+1]-U->ria[k], U->rja+U->ria[k], 
				   listindex, list1, list2);
	      
	      for(ii=0;ii<U->ria[k+1]-U->ria[k];ii++)
		{
		  i = U->rja[ii+U->ria[k]];
		  
		  /*** Compute L(i,k) = L(i,k) - L(i, 0:k-1).L(k, 0:k-1)t **/	      
		  /*** NO DROPPING HERE ***/
		  /** List 1 contains block in the  U part and list 2
		      in L part ***/
		  csrtab2 = list1 + ii*nk;   
		  csrtab1 = list2 + ii*nk;
		  nnb     = listindex[ii];
		  if(nnb>0)
		    {
		      if(info != NULL)
			PrecInfo_SubNNZ(info, CSnnz(U->ra[ii+U->ria[k]+1]));

		      CSRrowMultCSRcol(0.0, NULL, -1.0, nnb, -1.0, csrtab1, csrtab2, 
				       U->ra[ii+U->ria[k]], BL->block_index[i+1]-BL->block_index[i], wki1, wki2, wkd); /*** NO DROPPING HERE ***/
		      if(info != NULL)
			PrecInfo_AddNNZ(info, CSnnz(U->ra[ii+U->ria[k]+1]));
		    }
		}

	    }
	}

      /** Deallocate blocks which column indices are < START **/
      for(ii=L->ria[k];ii<L->ria[k+1];ii++)
	{
	  if(L->rja[ii] >= START)
	    break;

	  reinitCS(L->ra[ii]);
	  if(info != NULL)
	    PrecInfo_SubNNZ(info, CSnnz(L->ra[ii]));
	}

      /** Deallocate blocks which row indices are < START **/
      for(ii=U->cia[k];ii<U->cia[k+1];ii++)
	{
	  if(U->cja[ii] >= START)
	    break;

	  reinitCS(U->ca[ii]);
	  if(info != NULL)
	    PrecInfo_SubNNZ(info, CSnnz(U->ca[ii]));
	}

    

      /*** Factorize the diagonal block matrix A(k,k) ***/
      kk = BL->block_index[k]-BL->block_index[L->tlj];
      if(info != NULL)
	PrecInfo_SubNNZ(info, CSnnz(csU));

      if(droptab == NULL)
	CS_ILUT(csU, csL, csU, droptol, NULL, option->fillrat, &heap, wki1, wki2, wkd);
      else
	CS_ILUT(csU, csL, csU, droptol, droptab+kk, option->fillrat, &heap, wki1, wki2, wkd);

      if(info != NULL)
	{
	  PrecInfo_AddNNZ(info, CSnnz(csL));
	  PrecInfo_AddNNZ(info, CSnnz(csU));
	}
	  

      /*** RESPECT THE ORDER ***/
    
      /*** 1-- Divide the column block matrices of L by U ***/
      for(ii=L->cia[k]+1;ii<L->cia[k+1];ii++)
	{
	  i = L->cja[ii];
	  if(info != NULL)
	    PrecInfo_SubNNZ(info, CSnnz( L->ca[ii]));

	  if(droptab == NULL)
	    CSR_CSR_InvUT(csU, L->ca[ii], droptol, NULL, fillrat, &heap, wki1, wki2, wkd);
	  else
	    CSR_CSR_InvUT(csU, L->ca[ii], droptol, droptab+kk, fillrat, &heap, wki1, wki2, wkd);

	  if(info != NULL)
	    PrecInfo_AddNNZ(info, CSnnz( L->ca[ii]));


#ifdef OPTIM_FEW_NZR
	  /***** Sort the row of the matrice :need that for
		 CSRrowMultCSRcol optim ****/
	  ascend_column_reorder( L->ca[ii]);
#endif

	}


#ifdef SYMMETRIC_DROP
#ifdef DEBUG_M
      assert(droptabtmp != NULL);
#endif
      dropptr = droptabtmp+kk;
      for(i=0;i<csU->n;i++)
	{
#ifdef DEBUG_M
	  assert(csU->ja[i][0] == i);
#endif
	  dropptr[i] /= coefabs(csU->ma[i][0]);
	}
#endif

      /*** 2-- Divide the row block matrices of U by L ***/
      for(ii=U->ria[k]+1;ii<U->ria[k+1];ii++)
	{
	  i = U->rja[ii];
	  if(info != NULL)
	    PrecInfo_SubNNZ(info, CSnnz( U->ra[ii]));


	  if(droptabtmp != NULL)
	    CSR_CSR_InvLT(csL, U->ra[ii],  BL->block_index[i+1]-BL->block_index[i], droptol, droptabtmp+kk, fillrat, wki1, wki2, wkd);
	  else
	    CSR_CSR_InvLT(csL, U->ra[ii],  BL->block_index[i+1]-BL->block_index[i], droptol, NULL, fillrat, wki1, wki2, wkd);

	  if(info != NULL)
	    PrecInfo_AddNNZ(info, CSnnz( U->ra[ii]));



	}

  
    }

  Heap_Exit(&heap);
  free(wki1);
  free(wki2);
  free(wkd);

  if(jak != NULL)
    free(jak);
  if(rak != NULL)
    free(rak);
  free(Lfirstcol);
  free(Ufirstrow);
  free(listindex);
  free(list1);
  free(list2);


  
#ifdef SYMMETRIC_DROP
  free(droptabtmp);
#endif

#ifdef DEBUG_M
  PhidalMatrix_Check(L, BL);
#endif

}

/* @authors P. HENON */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>

#include "phidal_sequential.h"

void PhidalMatrix_Lsolve(flag_t unitdiag, PhidalMatrix *L, COEF *x, COEF *b, PhidalHID *BL)
{
  /*****************************************************************************************/
  /* This function compute x = L^-1.b                                                      */
  /* If unitdiag == 1 then the diagonal is considered to be unitary (stored or not stored) */
  /*   THIS CONCERNS THE DIAGONAL NOT THE BLOCK DIAGONAL                                   */
  /* Work also in place (i.e. x == b)                                                      */
  /*****************************************************************************************/
  dim_t i, j, k;
  int *bind;
  COEF *xptr;
  int offseti;
#ifdef DEBUG_M
  assert(L->dim1 == L->dim2);
  assert(L->tli == L->tlj);
  assert(L->bri == L->brj);
#endif

  bind = BL->block_index;
  offseti = bind[L->tli];

  if(x != b)
    memcpy(x, b, sizeof(COEF)*L->dim1);

  if(L->csc == 0)
    for(i=L->tli;i<=L->bri;i++)
      {
	xptr = x+(bind[i]-offseti);
	for(k=L->ria[i];k<L->ria[i+1]-1;k++)
	  {
	    j = L->rja[k];
	    matvecz(L->ra[k], x+(bind[j]-offseti), xptr, xptr); 
	  }
#ifdef DEBUG_M
	assert(L->rja[L->ria[i+1]-1] == i);
#endif
	CSR_Lsol(unitdiag, L->ra[L->ria[i+1]-1], xptr, xptr);
      }
  else
    for(i=L->tli;i<=L->bri;i++)
      {
	xptr = x+(bind[i]-offseti);
	for(k=L->ria[i];k<L->ria[i+1]-1;k++)
	  {
	    j = L->rja[k];
	    CSC_matvecz(L->ra[k], x+(bind[j]-offseti), xptr, xptr); 
	  }
#ifdef DEBUG_M
	assert(L->rja[L->ria[i+1]-1] == i);
#endif
	CSC_Lsol(unitdiag, L->ra[L->ria[i+1]-1], xptr, xptr);
      }
}


void PhidalMatrix_Usolve(flag_t unitdiag, PhidalMatrix *U, COEF *x, COEF *b, PhidalHID *BL)
{
  /*********************************************************/
  /* This function compute x = U^-1.b                      */
  /* Work also in place (i.e. x == b)                      */
  /* If unitdiag == 1 then the diagonal is considered to 
     be unitary (stored or not stored) */
  /*   THIS CONCERNS THE DIAGONAL NOT THE BLOCK DIAGONAL   */
  /*********************************************************/
  dim_t i, j, k;
  int *bind;
  COEF *xptr;
  int offseti;
#ifdef DEBUG_M
  assert(U->dim1 == U->dim2);
  assert(U->tli == U->tlj);
  assert(U->bri == U->brj);
#endif

  bind = BL->block_index;
  offseti = bind[U->tli];

  if(x != b)
    memcpy(x,b, sizeof(COEF)*U->dim1);

  if(U->csc == 0)
    for(j=U->brj;j>=U->tlj;j--)
      {
	xptr = x+(bind[j]-offseti);
#ifdef DEBUG_M
	assert(U->cja[U->cia[j+1]-1] == j);
#endif
	
	CSR_Usol(unitdiag, U->ca[U->cia[j+1]-1], xptr, xptr);
	
	for(k=U->cia[j+1]-2;k>=U->cia[j];k--)
	  {
	    i = U->cja[k];
	    matvecz(U->ca[k], xptr, x+(bind[i]-offseti), x+(bind[i]-offseti)); 
	  }

      }
  else
    for(j=U->brj;j>=U->tlj;j--)
      {
	xptr = x+(bind[j]-offseti);
#ifdef DEBUG_M
	assert(U->cja[U->cia[j+1]-1] == j);
#endif
	
	CSC_Usol(unitdiag, U->ca[U->cia[j+1]-1], xptr, xptr);
	
	for(k=U->cia[j+1]-2;k>=U->cia[j];k--)
	  {
	    i = U->cja[k];
	    CSC_matvecz(U->ca[k], xptr, x+(bind[i]-offseti), x+(bind[i]-offseti)); 
	  }
	
      }
}



/* @authors P. HENON */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>

#include "phidal_sequential.h"


void PhidalPrec_GEMM_ICCT(int_t levelnum, REAL *droptab, PhidalMatrix *G, PhidalMatrix *A, COEF* DG, 
		      PhidalPrec *P, PhidalHID *BL, PhidalOptions *option)
{
  PhidalMatrix *VL;
  chrono_t t1, t2;
  COEF *D;

#ifdef DEBUG_M
  /*assert(option->schur_method == 2);*/
  assert(option->schur_method != 1);
  assert(A->tli == A->tlj);
  assert(A->bri == A->brj);
  assert(G->tli == A->tli);
  assert(G->bri == A->bri);
  assert(G->brj == A->tlj-1);
  assert(levelnum == option->forwardlev);
#endif

  t1 = dwalltime();
  P->dim = A->dim1;
  P->symmetric = 1;
  P->schur_method = option->schur_method;
  P->forwardlev = 0;
  P->levelnum =  levelnum;

  VL = (PhidalMatrix *)malloc(sizeof(PhidalMatrix));
  PhidalMatrix_Init(VL);
  PhidalMatrix_Setup(G->tlj, G->tlj, A->bri, A->bri, "L", "N", option->locally_nbr, VL, BL);

  PhidalMatrix_Cut(A, VL, BL);
  PhidalMatrix_Cut(G, VL, BL);
  
  D = (COEF *)malloc(sizeof(COEF)*VL->dim1);
  memcpy(D, DG, sizeof(COEF)*G->dim2);
  PhidalMatrix_ICCT_Restrict(A->tli, VL, D, option->droptol1, droptab, option->fillrat, BL, option, P->info);

  M_MALLOC(P->LU, SCAL);
  PREC_L_SCAL(P) = (PhidalMatrix *)malloc(sizeof(PhidalMatrix));
  PhidalMatrix_Setup(A->tli, A->tli, A->bri, A->bri, "L", "N", option->locally_nbr, PREC_L_SCAL(P), BL);
  PhidalMatrix_Cut(VL, PREC_L_SCAL(P), BL);

  P->D = (COEF *)malloc(sizeof(COEF)*A->dim1);
  assert(G->dim2+A->dim1 == VL->dim1);
  memcpy(P->D, D+G->dim2, sizeof(COEF)*A->dim1);
  free(D);
  PhidalMatrix_Clean(VL);
  free(VL);

  /*** Construct U as a virtual matrix equal to the transpose of L ***/
  PREC_U_SCAL(P) = (PhidalMatrix *)malloc(sizeof(PhidalMatrix));
  PhidalMatrix_Init(PREC_U_SCAL(P));
  PhidalMatrix_BuildVirtualMatrix(PREC_L_SCAL(P)->tli, PREC_L_SCAL(P)->tlj, PREC_L_SCAL(P)->bri, PREC_L_SCAL(P)->brj, PREC_L_SCAL(P), PREC_U_SCAL(P), BL); 
  PhidalMatrix_Transpose(PREC_U_SCAL(P));

  t2 = dwalltime();
  fprintfv(5, stderr, "Time in GEMM_ICCT = %g \n", t2-t1);
}

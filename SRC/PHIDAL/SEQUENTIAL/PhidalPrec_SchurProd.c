/* @authors P. HENON */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>

#include "phidal_sequential.h"



void PhidalPrec_SchurProd(PhidalPrec *P, PhidalHID *BL, COEF *x, COEF *y)
{
  /**************************************************************/
  /* This function compute the implicit Schur Product           */
  /* y = S.x    where S is not stored                           */
  /* eg y = B.x - E.U^-1.L^1.F.x (for unsymmetric)              */
  /**************************************************************/

  /*** @@OIMBE PROBLEM SI PERMUTATION ????? B n'EST PAS PERMUTE ****/

  dim_t i;
  COEF *t;
  
  PhidalMatrix *E, *F, *L, *U, *B;
  COEF *D;

#ifdef DEBUG_M
  assert(P->schur_method == 2);
#endif

  E = PREC_E(P);
  F = PREC_F(P);
  L = PREC_L_SCAL(P);
  U = PREC_U_SCAL(P);
  B = PREC_B(P);
  D = P->D;


  t = (COEF *)malloc(sizeof(COEF)*L->dim1);

  PHIDAL_MatVec(B, BL, x, y);

  PHIDAL_MatVec(F, BL, x, t);
  PhidalMatrix_Lsolve(1, L, t, t, BL);
  if(P->symmetric == 1)
    for(i=0;i<PREC_L_SCAL(P)->dim1;i++)
      t[i] *= D[i];
  if(P->symmetric == 1)
    PhidalMatrix_Usolve(1, U, t, t, BL); /** Unitary diagonale **/
  else
    PhidalMatrix_Usolve(0, U, t, t, BL);
  PHIDAL_MatVecSub(E, BL, t, y);

  free(t);
}

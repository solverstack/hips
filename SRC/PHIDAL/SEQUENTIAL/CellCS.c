/* @authors P. HENON */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <assert.h>
#include <math.h>

#include "phidal_sequential.h"





void CellCS_ListUpdate(CellCS *celltab, int num,  int n,  dim_t *ja)
{
  dim_t i;
  CellCS *c;

  for(i=0;i<n;i++)
    {
      c = celltab+ja[i];

      while(c->nnz > 0)
	if( *(c->ja) < num || c->ma[0]->nnzr == 0)
	  {
	    c->nnz--;
	    c->ja++;
	    c->ma++;
	  }
	else
	  break;
    }
}


void CellCS_ListCopy(CellCS *src, CellCS *dest,  int n,  dim_t *ja)
{
  dim_t i;
  for(i=0;i<n;i++)
    dest[ja[i]] = src[ja[i]];
}


void CellCS_GetCSList(int num, CellCS *celltab, int n,  dim_t *ja, csptr *ma, int *nnb, csptr *csrtab1, csptr *csrtab2)
{
  int i, ind;
  CellCS *c;

  ind = 0;
   for(i=0;i<n;i++)
    {
      c = celltab+ja[i];
      
      while(c->nnz > 0)
	if( *(c->ja) < num || c->ma[0]->nnzr == 0)
	  {
	    c->nnz--;
	    c->ja++;
	    c->ma++;
	  }
	else
	  break;
      
      if( *(c->ja) == num)
	{
	  csrtab1[ind] = c->ma[0];
	  csrtab2[ind] = ma[i];
	  ind++;
	}
    }
   *nnb = ind;
}

void CellCS_IntersectList(CellCS *celltab, int ncol,  int *colja, csptr *colma, int nrow, int *rowja, 
			  int *nnztab, csptr *listA, csptr *listB)
{

  int jj, ii, i, j;

  csptr cmat;
  int nnz;
  dim_t *ja;
  csptr *ma;
  csptr **listAtab, **listBtab;

  if(nrow == 0 || ncol == 0)
    return;

  /***** Find the size of all the rowlist *****/
  listAtab = (csptr **)malloc(sizeof(csptr *)*nrow);
  listBtab = (csptr **)malloc(sizeof(csptr *)*nrow);

  for(i=0;i<nrow;i++)
    {
      listAtab[i] = listA+i*ncol;
      listBtab[i] = listB+i*ncol;
    }

  bzero(nnztab, sizeof(int)*nrow);

  for(jj=0;jj<ncol;jj++)
    {
      cmat = colma[jj];
      j = colja[jj];
      nnz = celltab[j].nnz;
      ja = celltab[j].ja;
      ma = celltab[j].ma;
      
      ii = 0;
      i = 0;
      while(ii<nnz && i <nrow)
	{
	  if(ja[ii] < rowja[i])
	    {
	      ii++;
	      continue;
	    }

	  if(ja[ii] > rowja[i])
	    {
	      i++;
	      continue;
	    }

	  /** ja[ii] == ja[i] **/
	  if(ma[ii]->nnzr > 0)
	    {
	      listAtab[i][nnztab[i]] = ma[ii];
	      listBtab[i][nnztab[i]] = cmat;
	      nnztab[i]++;
	    }
	  ii++;
	  i++;
	}
    }

  free(listAtab);
  free(listBtab);
}

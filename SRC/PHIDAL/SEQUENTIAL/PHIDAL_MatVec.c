/* @authors P. HENON */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>

#include "phidal_sequential.h"



void PHIDAL_MatVec(PhidalMatrix *M, PhidalHID *BL, COEF *x, COEF *y)
{
  /*****************************************************************/
  /* This function does y = M.x                                    */
  /*****************************************************************/
  dim_t i,j,k;
  int offseti, offsetj;
  COEF *xptr, *yptr;
  int *bind;


  bind = BL->block_index;

  bzero(y,sizeof(COEF)*M->dim1);
  offseti = bind[M->tli];
  offsetj = bind[M->tlj];
  
  if(M->symmetric == 0)
    {
      if(M->csc == 0)
	for(i=M->tli;i<=M->bri;i++)
	  {
	    yptr = y+(BL->block_index[i]-offseti);
	    for(k=M->ria[i];k<M->ria[i+1];k++)
	      {
		j = M->rja[k];
		matvec_add(M->ra[k], x + (bind[j]-offsetj), yptr);
	      }
	  }
      else
	for(i=M->tli;i<=M->bri;i++)
	  {
	    yptr = y+(BL->block_index[i]-offseti);
	    for(k=M->ria[i];k<M->ria[i+1];k++)
	      {
		j = M->rja[k];
		CSC_matvec_add(M->ra[k], x + (bind[j]-offsetj), yptr);
	      }
	  }
    }
  else
    {
      if(M->csc == 0)
	for(i=M->tli;i<=M->bri;i++)
	  {
	    yptr = y+(BL->block_index[i]-offseti);
	    xptr = x+(BL->block_index[i]-offseti);
	    for(k=M->ria[i];k<M->ria[i+1];k++)
	      {
		j = M->rja[k];
		if(j!=i)
		  matvec_add(M->ra[k], x + (bind[j]-offsetj), yptr);
		else
		  NoDiag_matvec_add(M->ra[k], x + (bind[j]-offsetj), yptr);
		
		matvec_add(M->ra[k], xptr, y + (bind[j]-offsetj));
	      }
	  }
      else
	for(i=M->tli;i<=M->bri;i++)
	  {
	    yptr = y+(BL->block_index[i]-offseti);
	    xptr = x+(BL->block_index[i]-offseti);
	    for(k=M->ria[i];k<M->ria[i+1];k++)
	      {
		j = M->rja[k];
		if(j!=i)
		  CSC_matvec_add(M->ra[k], x + (bind[j]-offsetj), yptr);
		else
		  CSC_NoDiag_matvec_add(M->ra[k], x + (bind[j]-offsetj), yptr);
		
		matvec_add(M->ra[k], xptr, y + (bind[j]-offsetj));
	      }
	  }
    }

}

void PHIDAL_MatVecAdd(PhidalMatrix *M, PhidalHID *BL, COEF *x, COEF *y)
{
  dim_t i;
  COEF *t;
  t = (COEF *)malloc(sizeof(COEF)*M->dim1);
  PHIDAL_MatVec(M, BL, x, t);

  for(i=0;i<M->dim1;i++)
    y[i] += t[i];
  free(t);
}


void PHIDAL_MatVecSub(PhidalMatrix *M, PhidalHID *BL, COEF *x, COEF *y)
{
  dim_t i;
  COEF *t;
  t = (COEF *)malloc(sizeof(COEF)*M->dim1);
  PHIDAL_MatVec(M, BL, x, t);

  for(i=0;i<M->dim1;i++)
    y[i] -= t[i];
  free(t);
}





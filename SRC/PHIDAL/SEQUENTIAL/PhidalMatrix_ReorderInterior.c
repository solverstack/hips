/* @authors P. HENON */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>
/*#include "metis.h"*/

#include "phidal_sequential.h"




void PhidalMatrix_ReorderInterior(PhidalMatrix *A, PhidalHID *BL, dim_t *permtab, dim_t *ipermtab)
{
  /************************************************************************/
  /* This function reorder the matrices corresponding to interior         */
  /* in the Phidal Hierarchical decomposition                             */
  /* in order to minimize fill-in in the preconditionner                  */
  /* This function uses METIS 4.0                                         */
  /* Return:                                                              */
  /* the reordered matrix A                                               */
  /* permtab: the permutation vector permtab[i] is the new number of      */
  /* unknown i                                                            */
  /* ipermtab:  the inverse permutation vector                            */
  /*    i.e. ipermtab[i] is the former number of node i before the        */
  /* reordering                                                           */
  /************************************************************************/
  dim_t i;
  PhidalMatrix M;
  int start, end;
  int ii;

  start = BL->block_levelindex[0];
  end = BL->block_levelindex[1]-1;

  PhidalMatrix_BuildVirtualMatrix(start, start, end, end, A, &M, BL);
  /** Loop over all the interior matrices **/
  for(i=M.tli;i<=M.bri;i++)
    {
#ifdef DEBUG_M
      assert(M.ria[i+1]-M.ria[i] == 1);
#endif
      ii = BL->block_index[i]-BL->block_index[M.tli];
      CS_Reorder(M.ra[0], ipermtab+ii, permtab+ii);
    }
  
  /*** Leave the remaining of the node (connector of the interface) unchanged ***/
  for(i=BL->block_index[1];i<A->dim1;i++)
    {
      permtab[i] = i;
      ipermtab[i] = i;
    }

  PhidalMatrix_Clean(&M);
}





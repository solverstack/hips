/* @authors P. HENON */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>

#include "phidal_sequential.h"

/** Local functions **/


void PhidalMatrix_RowNormINF(PhidalMatrix *A, PhidalHID *BL, REAL *normtab)
{
  dim_t i, j, k;
  REAL *normptr;

  bzero(normtab, sizeof(REAL)*A->dim1);

  if(A->csc == 0)
    for(i=A->tli;i<=A->bri;i++)
      {
	normptr = normtab + BL->block_index[i] -BL->block_index[A->tli];
	for(k=A->ria[i];k<A->ria[i+1];k++)
	  CSRrownormINF(A->ra[k], normptr);
      }
  else
    for(i=A->tli;i<=A->bri;i++)
      {
	normptr = normtab + BL->block_index[i] -BL->block_index[A->tli];
	for(k=A->ria[i];k<A->ria[i+1];k++)
	  CSRcolnormINF(A->ra[k], normptr);
      }
  
  if(A->symmetric == 1)
    {
      if(A->csc == 0)
	for(j=A->tlj;j<=A->brj;j++)
	  {
	    normptr = normtab + BL->block_index[j] -BL->block_index[A->tlj];
	    for(k=A->cia[j];k<A->cia[j+1];k++)
	      CSRcolnormINF(A->ca[k], normptr);
	  }
      else
	for(j=A->tlj;j<=A->brj;j++)
	  {
	    normptr = normtab + BL->block_index[j] -BL->block_index[A->tlj];
	    for(k=A->cia[j];k<A->cia[j+1];k++)
	      CSRrownormINF(A->ca[k], normptr);
	  }
    }

}

void PhidalMatrix_RowNorm1(PhidalMatrix *A, PhidalHID *BL, REAL *normtab)
{
  dim_t i, j, k;
  REAL *normptr;

  bzero(normtab, sizeof(REAL)*A->dim1);
  if(A->csc == 0)
    for(i=A->tli;i<=A->bri;i++)
      {
	normptr = normtab + BL->block_index[i] -BL->block_index[A->tli];
	for(k=A->ria[i];k<A->ria[i+1];k++)
	  CSRrownorm1(A->ra[k], normptr);
      }
  else
    for(i=A->tli;i<=A->bri;i++)
      {
	normptr = normtab + BL->block_index[i] -BL->block_index[A->tli];
	for(k=A->ria[i];k<A->ria[i+1];k++)
	  CSRcolnorm1(A->ra[k], normptr);
      }

  if(A->symmetric == 1)
    {
      if(A->csc == 0)
	for(j=A->tlj;j<=A->brj;j++)
	  {
	    normptr = normtab + BL->block_index[j] -BL->block_index[A->tlj];
#ifdef DEBUG_M
	    assert(A->cja[A->cia[j]] == j);
#endif
	    CSR_NoDiag_colnorm1(A->ca[A->cia[j]], normptr);
	    for(k=A->cia[j]+1;k<A->cia[j+1];k++)
	      CSRcolnorm1(A->ca[k], normptr);
	  }
      else
	for(j=A->tlj;j<=A->brj;j++)
	  {
	    normptr = normtab + BL->block_index[j] -BL->block_index[A->tlj];
#ifdef DEBUG_M
	    assert(A->cja[A->cia[j]] == j);
#endif
	    CSR_NoDiag_rownorm1(A->ca[A->cia[j]], normptr);
	    for(k=A->cia[j]+1;k<A->cia[j+1];k++)
	      CSRrownorm1(A->ca[k], normptr);
	  }
    }
}


void PhidalMatrix_RowNorm2(PhidalMatrix *A, PhidalHID *BL, REAL *normtab)
{
  dim_t i, j, k;
  REAL *normptr;

  bzero(normtab, sizeof(REAL)*A->dim1);
  if(A->csc == 0)
    for(i=A->tli;i<=A->bri;i++)
      {
	normptr = normtab + BL->block_index[i] -BL->block_index[A->tli];
	for(k=A->ria[i];k<A->ria[i+1];k++)
	  CSRrownorm2(A->ra[k], normptr);
      }
  else
    for(i=A->tli;i<=A->bri;i++)
      {
	normptr = normtab + BL->block_index[i] -BL->block_index[A->tli];
	for(k=A->ria[i];k<A->ria[i+1];k++)
	  CSRcolnorm2(A->ra[k], normptr);
      }

  if(A->symmetric == 1)
    {
      assert(A->csc == 1);
      if(A->csc == 0)
	for(j=A->tlj;j<=A->brj;j++)
	  {
	    normptr = normtab + BL->block_index[j] -BL->block_index[A->tlj];
#ifdef DEBUG_M
	    assert(A->cja[A->cia[j]] == j);
#endif
	    CSR_NoDiag_colnorm2(A->ca[A->cia[j]], normptr);
	    for(k=A->cia[j]+1;k<A->cia[j+1];k++)
	      CSRcolnorm2(A->ca[k], normptr);
	  }
      else
	for(j=A->tlj;j<=A->brj;j++)
	  {
	    normptr = normtab + BL->block_index[j] -BL->block_index[A->tlj];
#ifdef DEBUG_M
	    assert(A->cja[A->cia[j]] == j);
#endif
	    CSR_NoDiag_rownorm2(A->ca[A->cia[j]], normptr);
	    for(k=A->cia[j]+1;k<A->cia[j+1];k++)
	      CSRrownorm2(A->ca[k], normptr);
	  }
    }


  for(i=0;i<A->dim1;i++)
    normtab[i] = sqrt(normtab[i]);
}

void PhidalMatrix_ColNormINF(PhidalMatrix *A, PhidalHID *BL, REAL *normtab)
{
  dim_t i, j, k;
  REAL *normptr;

  bzero(normtab, sizeof(REAL)*A->dim2);
  if(A->csc == 0)
    for(j=A->tlj;j<=A->brj;j++)
      {
	normptr = normtab + BL->block_index[j] -BL->block_index[A->tlj];
	for(k=A->cia[j];k<A->cia[j+1];k++)
	  CSRcolnormINF(A->ca[k], normptr);
      }
  else
    for(j=A->tlj;j<=A->brj;j++)
      {
	normptr = normtab + BL->block_index[j] -BL->block_index[A->tlj];
	for(k=A->cia[j];k<A->cia[j+1];k++)
	  CSRrownormINF(A->ca[k], normptr);
      }

  if(A->symmetric == 1)
    {
      if(A->csc == 0)
	for(i=A->tli;i<=A->bri;i++)
	  {
	    normptr = normtab + BL->block_index[i] -BL->block_index[A->tli];
	    for(k=A->ria[i];k<A->ria[i+1];k++)
	      CSRrownormINF(A->ra[k], normptr);
	  }
      else
	for(i=A->tli;i<=A->bri;i++)
	  {
	    normptr = normtab + BL->block_index[i] -BL->block_index[A->tli];
	    for(k=A->ria[i];k<A->ria[i+1];k++)
	      CSRcolnormINF(A->ra[k], normptr);
	  }
    }


}

void PhidalMatrix_ColNorm1(PhidalMatrix *A, PhidalHID *BL, REAL *normtab)
{
  dim_t i, j, k;
  REAL *normptr;

  bzero(normtab, sizeof(REAL)*A->dim2);
  if(A->csc == 0)
    for(j=A->tlj;j<=A->brj;j++)
      {
	normptr = normtab + BL->block_index[j] -BL->block_index[A->tlj];
	for(k=A->cia[j];k<A->cia[j+1];k++)
	  CSRcolnorm1(A->ca[k], normptr);
      }
  else
    for(j=A->tlj;j<=A->brj;j++)
      {
	normptr = normtab + BL->block_index[j] -BL->block_index[A->tlj];
	for(k=A->cia[j];k<A->cia[j+1];k++)
	  CSRrownorm1(A->ca[k], normptr);
      }

  if(A->symmetric == 1)
    {
      if(A->csc == 0)
	for(i=A->tli;i<=A->bri;i++)
	  {
	    normptr = normtab + BL->block_index[i] -BL->block_index[A->tli];

	    for(k=A->ria[i];k<A->ria[i+1]-1;k++)
	      CSRrownorm1(A->ra[k], normptr);
#ifdef DEBUG_M
	    assert(A->rja[A->ria[i+1]-1] == i);
#endif
	    CSR_NoDiag_rownorm1(A->ra[A->ria[i+1]-1], normptr);
	  }
      else
	for(i=A->tli;i<=A->bri;i++)
	  {
	    normptr = normtab + BL->block_index[i] -BL->block_index[A->tli];

	    for(k=A->ria[i];k<A->ria[i+1]-1;k++)
	      CSRcolnorm1(A->ra[k], normptr);
#ifdef DEBUG_M
	    assert(A->rja[A->ria[i+1]-1] == i);
#endif
	    CSR_NoDiag_colnorm1(A->ra[A->ria[i+1]-1], normptr);
	  }
    }


}


void PhidalMatrix_ColNorm2(PhidalMatrix *A, PhidalHID *BL, REAL *normtab)
{
  dim_t i, j, k;
  REAL *normptr;

  bzero(normtab, sizeof(REAL)*A->dim2);
  if(A->csc == 0)
    for(j=A->tlj;j<=A->brj;j++)
      {
	normptr = normtab + BL->block_index[j] -BL->block_index[A->tlj];
	for(k=A->cia[j];k<A->cia[j+1];k++)
	  CSRcolnorm2(A->ca[k], normptr);
      }
  else
    for(j=A->tlj;j<=A->brj;j++)
      {
	normptr = normtab + BL->block_index[j] -BL->block_index[A->tlj];
	for(k=A->cia[j];k<A->cia[j+1];k++)
	  CSRrownorm2(A->ca[k], normptr);
      }

  
  if(A->symmetric == 1)
    {
      assert(A->csc == 1);
      if(A->csc == 0)
	for(i=A->tli;i<=A->bri;i++)
	  {
	    normptr = normtab + BL->block_index[i] -BL->block_index[A->tli];
	    for(k=A->ria[i];k<A->ria[i+1]-1;k++)
	      CSRrownorm2(A->ra[k], normptr);
#ifdef DEBUG_M
	    assert(A->rja[A->ria[i+1]-1] == i);
#endif
	    CSR_NoDiag_rownorm2(A->ra[A->ria[i+1]-1], normptr); 
	  }
      else
	for(i=A->tli;i<=A->bri;i++)
	  {
	    normptr = normtab + BL->block_index[i] -BL->block_index[A->tli];
	    for(k=A->ria[i];k<A->ria[i+1]-1;k++)
	      CSRcolnorm2(A->ra[k], normptr);
#ifdef DEBUG_M
	    assert(A->rja[A->ria[i+1]-1] == i);
#endif
	    CSR_NoDiag_colnorm2(A->ra[A->ria[i+1]-1], normptr);
	  }
    }

  for(j=0;j<A->dim2;j++)
    normtab[j] = sqrt(normtab[j]);

}

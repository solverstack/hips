/* @authors P. HENON */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>

#include "phidal_sequential.h"

/*** local function ***/
void  PhidalPrec_MLILUTForward(int_t levelnum, REAL *droptab,  PhidalMatrix *A, PhidalPrec *P, PhidalHID *BL, PhidalOptions *option);
void  PhidalPrec_MLILUT(int_t levelnum, int forwardlev, REAL *droptab,  PhidalMatrix *A, PhidalPrec *P, PhidalHID *BL, PhidalOptions *option);
void PHIDAL_MLILUTPrec(PhidalMatrix *A, PhidalPrec *P, PhidalHID *BL, PhidalOptions *option)
{
  REAL *droptab;
  
  /************** THIS IS DONE IN PhidalOptions_FIx *********
  if(option->forwardlev == -1 && option->backwardlev >= 0)
     option->forwardlev = BL->nlevel-1 -  option->backwardlev ;
  if(option->forwardlev >= 0 && option->backwardlev == -1)
    option->backwardlev =  BL->nlevel-1 -  option->forwardlev;
  ***********************************************************/

#ifdef DEBUG_M
  if(option->forwardlev >= BL->nlevel || option->forwardlev < 0 )
    {
      fprintfv(5, stderr, "Invalid parameter : number of forward levels %d total number of level %d \n", option->forwardlev, BL->nlevel);
      exit(-1);
    }
#endif

  PhidalPrec_Init(P);
  if(option->verbose >= 2)
    {
      P->info = (PrecInfo *)malloc(sizeof(PrecInfo));
      PrecInfo_Init(P->info);
    }
  /*** If droptab != NULL then the dropping will be made according to the norm of the row ***/
  /*if(option->scale==2)
    {
    droptab = (REAL *)malloc(sizeof(REAL)*A->dim1);
    bzero(droptab, sizeof(REAL)*A->dim1);
    }
    else*/
  droptab = NULL; /** If unsymmetric scaling is used droptab is useless **/

  PhidalPrec_MLILUT(0, option->forwardlev, droptab,  A, P, BL, option);
  if(droptab != NULL)
    free(droptab);

}

void  PhidalPrec_MLILUT(int_t levelnum, int forwardlev, REAL *droptab, PhidalMatrix *A, PhidalPrec *P, PhidalHID *BL, PhidalOptions *option)
{
  REAL droptol;
  REAL *scalerow=NULL, *iscalerow=NULL;
  REAL *scalecol=NULL, *iscalecol=NULL;
  dim_t i;
  chrono_t t1, t2, ttotal=0;
  char c;

#ifdef DEBUG_M
  assert(A->dim1>0);
#endif
  P->dim = A->dim1;
  P->schur_method = option->schur_method;
  P->forwardlev = forwardlev;
#ifdef SCALE_ALL  
  if(option->scale >  0 ) /** TO SCALE RECURSIVELY ALL THE LEVEL **/
#else
  if(option->scale >  0 && levelnum == 0) /** TO SCALE ONLY THE MATRIX A **/
#endif
    {
      scalerow = (REAL *)malloc(sizeof(REAL)*A->dim1);
      iscalerow = (REAL *)malloc(sizeof(REAL)*A->dim1);
      scalecol = (REAL *)malloc(sizeof(REAL)*A->dim1);
      iscalecol = (REAL *)malloc(sizeof(REAL)*A->dim1);

      if(option->scale == 1)
	{
	  /*** Unsymetric scale ***/
	  PhidalMatrix_UnsymScale(option->scalenbr, A, BL, scalerow, scalecol, iscalerow, iscalecol);
	}
      else 
	{
	  /*** Symmetric scale ***/
	  /* PhidalMatrix_SymScale is not equivalent to this: */
	  PhidalMatrix_ColNorm2(A, BL, iscalecol);
	  PhidalMatrix_RowNorm2(A, BL, iscalerow);

	  /** Take the square root of the norm such that a term 
	      aij = aij / (sqrt(rowi)*sqrt(colj)) **/
	  for(i=0;i<A->dim1;i++)
	    iscalecol[i] = sqrt(iscalecol[i]);
	  for(i=0;i<A->dim1;i++)
	    iscalerow[i] = sqrt(iscalerow[i]); 
	  for(i=0;i<A->dim1;i++)
	    scalecol[i] = 1.0/iscalecol[i];
	  for(i=0;i<A->dim1;i++)
	    scalerow[i] = 1.0/iscalerow[i];
	  
	  PhidalMatrix_ColMult2(scalecol, A, BL);
	  PhidalMatrix_RowMult2(scalerow, A, BL);	  
	}


      /**** Compute the droptab ******/
      /**** MUST BE THERE : AFTER THE SCALING ****/
      /**** BE CAREFUL USE A DROPTAB ONLY IF A SCALING IS DONE !! ***/
      if(droptab != NULL && option->scale == 2)
	{
	  PhidalMatrix_RowNorm2(A, BL, droptab);
	  /*fprintfv(5, stderr, "NORM dropetab = %g \n", norm2(droptab, A->dim1));*/
	  /*for(i=0;i<A->dim1;i++)
	    droptab[i] = 1.0;*/
	}
    }

  /********************************************/
  /** Dropping in the Schur complement matrix **/
  /********************************************/
  if(levelnum>0 && option->dropSchur > 0)
    PhidalMatrix_DropT(A, option->dropSchur, droptab, BL);


  /*** Compute the numerical threshold to use at this level in the factorization **/
  if(levelnum == 0)
    droptol = option->droptol0;
  else
    {
#ifdef DEBUG_M
      assert(levelnum>=1);
#endif
      droptol = option->droptol1 * pow(option->droptolrat, (REAL)(levelnum-1.0));
    }



  if(forwardlev == 0)
    {
      /**** just factorize the level *****/

      if (levelnum == 0) c = 'M'; else c = 'S';
      fprintfv(5, stdout, "  Numeric Factorisation (%c)\n",c); 
      t1 = dwalltime();

      P->levelnum =  levelnum;
   
      if(option->pivoting == 1)
	{
	  P->pivoting = 1;
	  P->permtab = (int *)malloc(sizeof(int)*A->dim1);
	}
      
      /*if(option->verbose >= 1)
	fprintfv(5, stdout, "Numerical threshold to factorize the last Schur complement (connector levels >= %d)  = %g \n", levelnum, droptol);	*/


#ifdef ILUT_WHOLE_SCHUR 
      fprintfv(5, stderr, "ILUT WHOLE \n");
      
      P->csL = (csptr)malloc(sizeof(struct SparRow));
      P->csU = (csptr)malloc(sizeof(struct SparRow));

      if(option->schur_method != 1 && levelnum > 0)
	PhidalMatrix2SparRow(1, A, P->csU, BL);  /** Destroy A **/
      else
	PhidalMatrix2SparRow(0, A, P->csU, BL); /** Do not destroy A **/
      
      {
	COEF *wkd;
	csptr csA;
	Heap htmp;
	int *wki1, *wki2;
	wkd = (COEF *)malloc(sizeof(COEF)*A->dim1);
	wki1= (int *)malloc(sizeof(int)*A->dim1);
	wki2= (int *)malloc(sizeof(int)*A->dim1);
	Heap_Init(&htmp, A->dim1);
	initCS(P->csL, P->csU->n);
	CS_ILUT(P->csU, P->csL, P->csU, droptol, droptab, option->fillrat, &htmp, wki1, wki2, wkd);

	free(wkd);
	free(wki1);
	free(wki2);
	Heap_Exit(&htmp);
      }
      PREC_L_SCAL(P) = NULL;
      PREC_U_SCAL(P) = NULL;
#else
      M_MALLOC(P->LU, SCAL);
      PREC_L_SCAL(P) = (PhidalMatrix *)malloc(sizeof(PhidalMatrix));
      PREC_U_SCAL(P) = (PhidalMatrix *)malloc(sizeof(PhidalMatrix));
      
      if( option->schur_method != 1 && levelnum > 0)
	{
	  /** The factorization is done in place (A is a void matrix in return **/
	  PHIDAL_ILUTP(1, option->pivoting, A, PREC_L_SCAL(P), PREC_U_SCAL(P), droptol, droptab, option->fillrat, P->permtab, BL, option, P->info);
	  PhidalMatrix_Clean(A);
	}
      else
	PHIDAL_ILUTP(0, option->pivoting, A, PREC_L_SCAL(P), PREC_U_SCAL(P), droptol, droptab, option->fillrat, P->permtab, BL, option, P->info);

#ifdef ILUT_SOLVE
      P->csL = (csptr)malloc(sizeof(struct SparRow));
      P->csU = (csptr)malloc(sizeof(struct SparRow));
      PhidalMatrix2SparRow(1, PREC_L_SCAL(P), P->csL, BL);  /** Destroy L **/
      PhidalMatrix2SparRow(1, PREC_U_SCAL(P), P->csU, BL);  /** Destroy L **/
      PhidalMatrix_Clean(PREC_L_SCAL(P));
      PhidalMatrix_Clean(PREC_U_SCAL(P));
      free(PREC_L_SCAL(P));
      free(PREC_U_SCAL(P));
      PREC_L_SCAL(P)=NULL;
      PREC_U_SCAL(P)=NULL;
#endif 



#endif

       t2 = dwalltime(); ttotal += t2-t1;
       fprintfv(5, stdout, "   %c : Numeric Factorisation in %g seconds\n", c, t2-t1);
      goto unscale;
    }
  
#if !defined(ILUT_WHOLE_SCHUR) && !defined(ILUT_SOLVE) 
  if(forwardlev == 1 && option->schur_method != 1)
    {
      /** In this case the GEMM and ICCT are done in one step to 
	  save the storage of an intermediate schur complement **/
      PhidalPrec_MLILUTForward(levelnum, droptab, A, P, BL, option);
      
      goto unscale;
    }
#endif

  if(forwardlev > 0)
    {
      PhidalPrec_MLILUTForward(levelnum, droptab, A, P, BL, option);
      /*** Go to next level ***/
      P->nextprec = (PhidalPrec *)malloc(sizeof(PhidalPrec));
      PhidalPrec_Init(P->nextprec);
      P->nextprec->prevprec = P;
      /** Important to call again PhidalPrec_MLILUT because of scaling **/
      if(droptab != NULL)
	PhidalPrec_MLILUT(levelnum+1, forwardlev-1, droptab+(BL->block_index[PREC_S_SCAL(P)->tli]-BL->block_index[A->tli])
			  , PREC_S_SCAL(P), P->nextprec, BL, option);
      else
	PhidalPrec_MLILUT(levelnum+1, forwardlev-1, NULL, PREC_S_SCAL(P), P->nextprec, BL, option);

      if(option->schur_method != 1 && levelnum > 0)
	{
	  free(PREC_S_SCAL(P)); /** S has been destroyed in the recursion **/
	  PREC_S_SCAL(P) = NULL;
	}
      
      goto unscale;
    }

 unscale:
  
#ifdef SCALE_ALL
  if(option->scale >  0 ) /** TO SCALE RECURSIVELY ALL THE LEVEL **/
#else
  if(option->scale >  0 && levelnum == 0) /** TO SCALE ONLY THE MATRIX A **/
#endif
    {
      /** Unscale the matrix and the preconditioner **/
      if(levelnum == 0 || option->schur_method == 1)
	{
	  PhidalMatrix_ColMult2(iscalecol, A, BL);
	  PhidalMatrix_RowMult2(iscalerow, A, BL);	
	}
      
      PhidalPrec_UnsymmetricUnscale(scalerow, iscalerow, scalecol, iscalecol, P, BL);
  
      free(scalerow);
      free(iscalerow);
      free(scalecol);
      free(iscalecol);
    }

  return;
}

void  PhidalPrec_MLILUTForward(int_t levelnum, REAL *droptab, PhidalMatrix *A, PhidalPrec *P, PhidalHID *BL, PhidalOptions *option)
{
  int Mstart, Mend, Sstart, Send;
  PhidalMatrix M, G, W;
  int  *wki1, *wki2;
  COEF *wkd;
  Heap heap;
  int maxB;
  dim_t i;
  REAL droptol;
  REAL *droptabtmp;
  chrono_t t1, t2, ttotal=0;
  char c;
#ifdef DEBUG_M
  assert(levelnum < BL->nlevel);
#endif

  t1 = dwalltime(); 

  P->dim = A->dim1;

  P->levelnum =  levelnum;
  Mstart   =  BL->block_levelindex[levelnum];
  Mend     =  BL->block_levelindex[levelnum+1]-1;
  Sstart   =  BL->block_levelindex[levelnum+1];
  Send     =  BL->nblock-1;  
  
  M_MALLOC(P->LU, SCAL);
  PREC_L_SCAL(P) = (PhidalMatrix *)malloc(sizeof(PhidalMatrix));
  PREC_U_SCAL(P) = (PhidalMatrix *)malloc(sizeof(PhidalMatrix));


  /*** Compute the numerical threshold to use at this level in the factorization **/
  if(levelnum == 0)
    droptol = option->droptol0;
  else
    {
#ifdef DEBUG_M
      assert(levelnum>=1);
#endif
      droptol = option->droptol1 * pow(option->droptolrat, (REAL)(levelnum-1.0));
    }
  
  /*if(option->verbose >= 1)
    fprintfv(5, stdout, "Numerical threshold to factorize level %d = %g \n", levelnum, droptol);*/

  /*** Factorize the Level ***/
  PhidalMatrix_Init(&M);
  PhidalMatrix_BuildVirtualMatrix(Mstart, Mstart, Mend, Mend, A, &M, BL); 
  if(option->pivoting == 1)
    {
      P->pivoting = 1;
      P->permtab = (int *)malloc(sizeof(int)*M.dim1);
    }
 
  M_MALLOC(P->EF, SCAL);
  PREC_E(P) = (PhidalMatrix *)malloc(sizeof(PhidalMatrix));
  PREC_F(P) = (PhidalMatrix *)malloc(sizeof(PhidalMatrix));

  if (levelnum == 0) c = 'M'; else c = 'S';
  fprintfv(5, stdout, "  Numeric Factorisation (%c)\n",c); 


  if( option->schur_method != 1 && levelnum > 0)
    {
      PHIDAL_ILUTP(1, option->pivoting, &M, PREC_L_SCAL(P), PREC_U_SCAL(P), droptol, droptab, option->fillrat, P->permtab, BL, option, P->info);
      PhidalMatrix_Setup(Sstart, Mstart, Send, Mend, "N", "N" , option->locally_nbr, PREC_E(P), BL);
      PhidalMatrix_Cut(A, PREC_E(P), BL);
      PhidalMatrix_Setup(Mstart, Sstart, Mend, Send, "N", "N" , option->locally_nbr, PREC_F(P), BL);
      PhidalMatrix_Cut(A, PREC_F(P), BL);
    }
  else
    {
      PHIDAL_ILUTP(0, option->pivoting, &M, PREC_L_SCAL(P), PREC_U_SCAL(P), droptol, droptab, option->fillrat, P->permtab, BL, option, P->info);
      PhidalMatrix_BuildVirtualMatrix(Sstart, Mstart, Send, Mend, A, PREC_E(P), BL);
      PhidalMatrix_BuildVirtualMatrix(Mstart, Sstart, Mend, Send, A, PREC_F(P), BL);
    }
  

  PhidalMatrix_Clean(&M);

  t2 = dwalltime(); ttotal += t2-t1;
  fprintfv(5, stdout, "   %c : Numeric Factorisation in %g seconds\n", c, t2-t1);

  /************************************************/
  /* COMPUTE W = L^-1.F and G = E.U^-1            */
  /************************************************/
#ifdef DROP_TRSM
  droptol = option->droptol1;
#endif

  fprintfv(5, stdout, " TRSM M / E\n");       
  t1  = dwalltime();
 
  maxB = 0;
  for(i=A->tli;i<=A->bri;i++)
    if(BL->block_index[i+1]-BL->block_index[i] > maxB)
      maxB = BL->block_index[i+1]-BL->block_index[i];
  
  Heap_Init(&heap, maxB);
  wki1 = (int *)malloc(sizeof(int)*maxB);
  wki2 = (int *)malloc(sizeof(int)*maxB);
  wkd = (COEF *)malloc(sizeof(COEF)*maxB);

  PhidalMatrix_Init(&W);
  PhidalMatrix_Init(&G);
  /** IMPORTANT :G and W are always computed using a consistent block pattern **/
  /*PhidalMatrix_Setup(PREC_E(P)->tli, PREC_E(P)->tlj, PREC_E(P)->bri, PREC_E(P)->brj, "N", "N", BL->nlevel, &G, BL);*/
  PhidalMatrix_Setup(PREC_E(P)->tli, PREC_E(P)->tlj, PREC_E(P)->bri, PREC_E(P)->brj, "N", "N", option->locally_nbr, &G, BL);
  PhidalMatrix_Copy(PREC_E(P), &G, BL);

  if(option->pivoting == 1)
    /** Important to pivot G and not E because E is a virtual matrix pointing in the last upper schur complement **/
    PhidalMatrix_ColPerm(&G, P->permtab, BL); 

  /*PhidalMatrix_Setup(PREC_F(P)->tli, PREC_F(P)->tlj, PREC_F(P)->bri, PREC_F(P)->brj, "N", "N", BL->nlevel, &W, BL);*/
  PhidalMatrix_Setup(PREC_F(P)->tli, PREC_F(P)->tlj, PREC_F(P)->bri, PREC_F(P)->brj, "N", "N", option->locally_nbr, &W, BL);
  PhidalMatrix_Copy(PREC_F(P), &W, BL);
  

  PHIDAL_InvUT(PREC_U_SCAL(P), &G, option->droptolE, droptab, option->fillrat, BL, &heap, wki1, wki2, wkd);

#ifdef SYMMETRIC_DROP
 {
#ifdef TYPE_COMPLEX
   COEF* diagtmp = (COEF *)malloc(sizeof(COEF)*PREC_U_SCAL(P)->dim1);
#else
#define diagtmp droptabtmp
#endif   
  
   droptabtmp = (REAL *)malloc(sizeof(REAL)*PREC_U_SCAL(P)->dim1);
   PhidalMatrix_GetUdiag(PREC_U_SCAL(P), diagtmp);
   for(i=0;i<PREC_U_SCAL(P)->dim1;i++)
     droptabtmp[i] = 1.0/coefabs(diagtmp[i]);
#ifdef TYPE_COMPLEX
   free(diagtmp);
#endif
   if(droptab != NULL)
     for(i=0;i<PREC_U_SCAL(P)->dim1;i++)
       droptabtmp[i] *= droptab[i];
 }
#else
  droptabtmp = droptab;
#endif
  PHIDAL_InvLT(PREC_L_SCAL(P), &W, option->droptolE, droptabtmp, option->fillrat, BL, wki1, wki2, wkd);

#ifdef SYMMETRIC_DROP
  free(droptabtmp);
#endif


  free(wki1);
  free(wki2);
  free(wkd);
  Heap_Exit(&heap);

  t2  = dwalltime(); ttotal += t2-t1;
  fprintfv(5, stdout, "  TRSM in %g seconds\n\n", t2-t1);

  /*** Compute the schur complement on the remaining levels ****/
  fprintfv(5, stdout, " GEMM E/E -> S\n"); 
  t1  = dwalltime(); 

  M_MALLOC(P->S, SCAL);
  PREC_S_SCAL(P) = (PhidalMatrix *)malloc(sizeof(PhidalMatrix));

  if(option->schur_method != 1)
    PhidalMatrix_Setup(Sstart, Sstart, Send, Send, "N", "N", option->locally_nbr, PREC_S_SCAL(P), BL);
  else
    PhidalMatrix_Setup(Sstart, Sstart, Send, Send, "N", "N", BL->nlevel, PREC_S_SCAL(P), BL);

  if(option->schur_method == 2)
    {
      M_MALLOC(P->B, SCAL);
      PREC_B(P) = (PhidalMatrix *)malloc(sizeof(PhidalMatrix));
      PhidalMatrix_Init(PREC_B(P));
      if(levelnum == 0)
	PhidalMatrix_BuildVirtualMatrix(Sstart, Sstart, Send, Send, A, PREC_B(P), BL);
      else
	{
	  PhidalMatrix_Setup(Sstart, Sstart, Send, Send, "N", "N", option->locally_nbr, PREC_B(P), BL);
	  /*PhidalMatrix_Setup(Sstart, Sstart, Send, Send, "N", "N", BL->nlevel, PREC_B(P), BL);*/
	  PhidalMatrix_Copy(A, PREC_B(P), BL);
	}
    }
  


  if(option->schur_method != 1 && levelnum > 0)
    PhidalMatrix_Cut(A, PREC_S_SCAL(P), BL);
  else
    PhidalMatrix_Copy(A, PREC_S_SCAL(P), BL);

  if(option->schur_method != 1 && levelnum > 0)
    PhidalMatrix_Clean(A);
  
  /** OIMBE Pour ILUT_SOLVE vaudrait mieux utiliser PhidalPrec_GEMM_ILUCT
      et faire la transformation apres ! **/
#if defined(ILUT_WHOLE_SCHUR) || defined(ILUT_SOLVE) 
  PhidalMatrix_ILUTSchur(1, &G, &W, PREC_S_SCAL(P), BL, option);
#else
  if(option->forwardlev == levelnum+1 && option->schur_method != 1)
    {
      P->nextprec = (PhidalPrec *)malloc(sizeof(PhidalPrec));
      PhidalPrec_Init(P->nextprec);
      P->nextprec->prevprec = P;
      PhidalPrec_GEMM_ILUCT(levelnum+1, droptab, &G, &W, PREC_S_SCAL(P), P->nextprec, BL, option);
      PhidalMatrix_Clean(PREC_S_SCAL(P));
      free(PREC_S_SCAL(P));
      PREC_S_SCAL(P) = NULL;
    }
  else 
    {
      /** G and W are destroyed inside this function **/
      PhidalMatrix_ILUTSchur(1, &G, &W, PREC_S_SCAL(P), BL, option);
    }
#endif

  PhidalMatrix_Clean(&G);
  PhidalMatrix_Clean(&W);

  t2  = dwalltime(); ttotal += t2-t1;
  fprintfv(5, stdout, "  GEMM in %g seconds\n\n", t2-t1);

}


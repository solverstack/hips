/* @authors P. HENON */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>

#include "phidal_sequential.h"
#include "phidal_common.h"

#ifndef PARALLEL /* compil only 1x */

void PhidalPrec_Info(PhidalPrec *p)
{
  fprintfd(stdout, "-------Level %d NNZ ----------------\n", p->levelnum);

#if defined(ILUT_WHOLE_SCHUR) || defined(ILUT_SOLVE)
  if(p->forwardlev == 0)
    {
      fprintfd(stdout, "L(%d) : %ld \n", p->levelnum, CSnnz(p->csL));
      if(p->symmetric == 0) {
	fprintfd(stdout, "U(%d) : %ld \n", p->levelnum, CSnnz(p->csU));
      } else {
	fprintfd(stdout, "D(%d) : %ld \n", p->levelnum, (long)p->csL->n);
      }
    }
  else
#else
  {
    fprintfd(stdout, "L(%d) : %ld \n", p->levelnum, PhidalMatrix_NNZ(PREC_L_SCAL(p)));
    if(p->symmetric == 0) {
      fprintfd(stdout, "U(%d) : %ld \n", p->levelnum, PhidalMatrix_NNZ(PREC_U_SCAL(p)));
    } else {
      fprintfd(stdout, "D(%d) : %ld \n", p->levelnum, (long)PREC_L_SCAL(p)->dim1);
    }
  }
#endif

  if(p->forwardlev > 0)
    {
      fprintfd(stdout, "E(%d) : %ld \n", p->levelnum, PhidalMatrix_NNZ(PREC_E(p)));
      if(p->symmetric == 0)
	fprintfd(stdout, "F(%d) : %ld \n", p->levelnum, PhidalMatrix_NNZ(PREC_F(p)));


      if(p->schur_method == 1) 
	fprintfd(stdout, "S(%d) : %ld \n",  p->levelnum, PhidalMatrix_NNZ(PREC_S_SCAL(p)));

      if(p->schur_method == 2) 
	fprintfd(stdout, "C(%d) : %ld \n",  p->levelnum, PhidalMatrix_NNZ(PREC_B(p)));

      PhidalPrec_Info(p->nextprec);
    }
  fprintfd(stdout, "------------------------------------\n");
}

#endif

#include "macro.h"
long CMD(_PhidalDistrPrec,NNZ)(_PhidalDistrPrec *p)
{
  long nnz = 0;

#ifdef DEBUG_M
  assert(p != NULL);
#endif

  if(p->forwardlev == 0)
    {
#if defined(ILUT_WHOLE_SCHUR) || defined(ILUT_SOLVE)
      nnz = CSnnz(p->csL);
      if(p->symmetric == 0)
	nnz += CSnnz(p->csU);
      
      if(p->symmetric == 1)
	nnz += p->csL->n;
#else
      nnz = PhidalMatrix_NNZ(S(PREC_L_SCAL(p)));

      if(p->symmetric == 0)
	nnz += PhidalMatrix_NNZ(S(PREC_U_SCAL(p)));

      if(p->symmetric == 1)
	nnz += S(PREC_L_SCAL(p))->dim1;
#endif

      return nnz;
    }

  if(p->forwardlev > 0)
    {
      nnz =  PhidalMatrix_NNZ(S(PREC_L_SCAL(p)));
      if(p->symmetric == 0)
	nnz += PhidalMatrix_NNZ(S(PREC_U_SCAL(p)));

      if(p->symmetric == 1)
	nnz += S(PREC_L_SCAL(p))->dim1;
      if(S(PREC_E(p))->virtual == 0)
	nnz += PhidalMatrix_NNZ(S(PREC_E(p)));
      if(p->symmetric == 0 && S(PREC_F(p))->virtual == 0)
	nnz += PhidalMatrix_NNZ(S(PREC_E(p)));
	

      if(p->schur_method == 1) 
	if(S(PREC_S_SCAL(p))->virtual == 0)
	  nnz += PhidalMatrix_NNZ(S(PREC_S_SCAL(p)));

      if(p->schur_method == 2) 
	if(S(PREC_B(p))->virtual == 0)
	  nnz += PhidalMatrix_NNZ(S(PREC_B(p)));
      
      nnz += CMD(_PhidalDistrPrec,NNZ)(p->nextprec);
      return nnz;
    }
  
  return nnz;
}

#ifndef PARALLEL /* compil only 1x */
/*  faire qu'une fois un DDOT ? */
void PhidalPrec_SymmetricUnscale(REAL *scaletab, REAL *iscaletab, PhidalPrec *P, PhidalHID *BL)
{
  dim_t i;
  COEF *D;

  /*****************************************************/
  /*  A = Dr^-1.A'.Dr^-1                               */
  /*  A = Dr^-1.L'.D'.L't.Dr^-1                        */
  /*  A = Dr^-1.L'.Dr . Dr^-1.D'.Dr^-1  . Dr.L't.Dr^-1 */
  /*  A =     L       .      D          . Lt           */
  /*****************************************************/

#ifdef DEBUG_M
  assert(P->symmetric == 1);
  assert(P->D != NULL);
#endif

  if(P->forwardlev == 0)
    {
#if defined(ILUT_WHOLE_SCHUR) || defined(ILUT_SOLVE)
      CS_ColMult2(iscaletab, P->csL); /** Correspond to a Row mult
					 since the CS is in fact a CSC **/
      CS_RowMult2(scaletab, P->csL);  /** Same remark **/
#else
      PhidalMatrix_RowMult2(iscaletab, PREC_L_SCAL(P), BL);
      PhidalMatrix_ColMult2(scaletab, PREC_L_SCAL(P), BL);
#endif
      D = P->D;
      for(i=0;i<P->dim;i++)
	D[i] *= scaletab[i]*scaletab[i]; /** This is the inverse of the diagonal **/
      return;
    }
  
  if(P->forwardlev > 0)
    {
      PhidalPrec_SymmetricUnscale(scaletab + PREC_L_SCAL(P)->dim1, iscaletab + PREC_L_SCAL(P)->dim1, P->nextprec, BL);
      
      
      PhidalMatrix_RowMult2(iscaletab, PREC_L_SCAL(P), BL);
      PhidalMatrix_ColMult2(scaletab, PREC_L_SCAL(P), BL);
      D = P->D;
      for(i=0;i<PREC_L_SCAL(P)->dim1;i++)
	D[i] *= scaletab[i]*scaletab[i]; /** This is the inverse of the diagonal **/
	  
      if(PREC_E(P)->virtual == 0)
	{
	  PhidalMatrix_RowMult2(iscaletab + PREC_L_SCAL(P)->dim1, PREC_E(P), BL);
	  PhidalMatrix_ColMult2(iscaletab, PREC_E(P), BL);
	} 
 
      if(P->schur_method == 1)
	{
#ifdef DEBUG_M
	  assert(PREC_S_SCAL(P)->virtual == 0);
#endif
	  PhidalMatrix_RowMult2(iscaletab + PREC_L_SCAL(P)->dim1, PREC_S_SCAL(P), BL);
	  PhidalMatrix_ColMult2(iscaletab + PREC_L_SCAL(P)->dim1, PREC_S_SCAL(P), BL);
	}

      if(P->schur_method == 2 && P->levelnum > 0)
	{
#ifdef DEBUG_M
	  assert(PREC_B(P)->virtual == 0);
#endif
	  PhidalMatrix_RowMult2(iscaletab + PREC_L_SCAL(P)->dim1, PREC_B(P), BL);
	  PhidalMatrix_ColMult2(iscaletab + PREC_L_SCAL(P)->dim1, PREC_B(P), BL);
	}
      

      return;
    }

  return;
 
}

void PhidalPrec_UnsymmetricUnscale(REAL *scalerowtab, REAL *iscalerowtab, REAL *scalecoltab, REAL *iscalecoltab, PhidalPrec *P, PhidalHID *BL)
{

  /*****************************************************/
  /*  A = Dr^-1.A'.Dc^-1                               */
  /*  A = Dr^-1.L'.U'.Dc^-1                            */
  /*  A = Dr^-1.L'.Dr   .   Dr^-1.U'.Dc^-1             */
  /*  A =     L       .      U                         */
  /*****************************************************/

  dim_t i;
  COEF *D;
 
#ifdef DEBUG_M
  assert(P->symmetric == 0);
  assert(P->D == NULL);
#endif

  if(P->forwardlev == 0)
    {
#if defined(ILUT_WHOLE_SCHUR) || defined(ILUT_SOLVE)
      CS_RowMult(iscalerowtab, P->csL); 
      CS_ColMult(scalerowtab, P->csL); 

      D = (REAL *)malloc(sizeof(REAL)*P->csL->n);
      /** Store the diagonal **/
      for(i=0;i<P->csU->n;i++)
	D[i] = P->csU->ma[i][0] ;

      CS_RowMult(iscalerowtab, P->csU);
      CS_ColMult(iscalecoltab, P->csU);
      for(i=0;i<P->csU->n;i++)
	D[i] *= scalerowtab[i]*scalecoltab[i]; /** D is the inverse of the diagonal **/
      /** Set the U diag **/
      for(i=0;i<P->csU->n;i++)
	P->csU->ma[i][0] = D[i];
#else
      PhidalMatrix_RowMult2(iscalerowtab, PREC_L_SCAL(P), BL);
      PhidalMatrix_ColMult2(scalerowtab, PREC_L_SCAL(P), BL);
      D = (COEF *)malloc(sizeof(COEF)*PREC_U_SCAL(P)->dim1);
      PhidalMatrix_GetUdiag(PREC_U_SCAL(P), D);
      PhidalMatrix_RowMult2(iscalerowtab, PREC_U_SCAL(P), BL);
      PhidalMatrix_ColMult2(iscalecoltab, PREC_U_SCAL(P), BL);

      for(i=0;i<PREC_U_SCAL(P)->dim1;i++)
	D[i] *= scalerowtab[i]*scalecoltab[i]; /** D is the inverse of the diagonal **/
      PhidalMatrix_SetUdiag(PREC_U_SCAL(P), D);
#endif
      
      free(D);
      return;
    }
  


  if(P->forwardlev > 0)
    {
      PhidalPrec_UnsymmetricUnscale(scalerowtab + PREC_L_SCAL(P)->dim1, iscalerowtab + PREC_L_SCAL(P)->dim1, scalecoltab+PREC_U_SCAL(P)->dim1, iscalecoltab+PREC_U_SCAL(P)->dim1, P->nextprec, BL);
      
      PhidalMatrix_RowMult2(iscalerowtab, PREC_L_SCAL(P), BL);
      PhidalMatrix_ColMult2(scalerowtab, PREC_L_SCAL(P), BL);

      D = (COEF *)malloc(sizeof(COEF)*PREC_U_SCAL(P)->dim1);
      PhidalMatrix_GetUdiag(PREC_U_SCAL(P), D);
      
      PhidalMatrix_RowMult2(iscalerowtab, PREC_U_SCAL(P), BL);
      PhidalMatrix_ColMult2(iscalecoltab, PREC_U_SCAL(P), BL);

      for(i=0;i<PREC_U_SCAL(P)->dim1;i++)
	D[i] *= scalerowtab[i]*scalecoltab[i]; /** D is the inverse of the diagonal **/
      PhidalMatrix_SetUdiag(PREC_U_SCAL(P), D);
      free(D);

      
	  
      if(PREC_E(P)->virtual == 0)
	{
	  PhidalMatrix_RowMult2(iscalerowtab + PREC_L_SCAL(P)->dim1, PREC_E(P), BL);
	  PhidalMatrix_ColMult2(iscalecoltab, PREC_E(P), BL);
	} 
      
      if(PREC_F(P)->virtual == 0)
	{
	  PhidalMatrix_RowMult2(iscalerowtab, PREC_F(P), BL);
	  PhidalMatrix_ColMult2(iscalecoltab + PREC_L_SCAL(P)->dim1, PREC_F(P), BL);
	} 
 

      if(P->schur_method == 1)
	{
#ifdef DEBUG_M
	  assert(PREC_S_SCAL(P)->virtual == 0);
#endif
	  PhidalMatrix_RowMult2(iscalerowtab + PREC_L_SCAL(P)->dim1, PREC_S_SCAL(P), BL);
	  PhidalMatrix_ColMult2(iscalecoltab + PREC_L_SCAL(P)->dim1, PREC_S_SCAL(P), BL);
	}

      if(P->schur_method == 2 && P->levelnum > 0)
	{
#ifdef DEBUG_M
	  assert(PREC_B(P)->virtual == 0);
#endif
	  PhidalMatrix_RowMult2(iscalerowtab + PREC_L_SCAL(P)->dim1, PREC_B(P), BL);
	  PhidalMatrix_ColMult2(iscalecoltab + PREC_L_SCAL(P)->dim1, PREC_B(P), BL);
	}


      return;
    }

  return;
 
}




/*** DEBUG Purpose ***/
void PhidalPrec_PrintInfo(PhidalPrec *P, PhidalHID *BL)
{
  
  if(P->forwardlev == 0)
    {
      if(P->symmetric == 0)
	{
	  fprintfd(stderr, "Level %d NNZ: L =%ld , U = %ld \n", P->levelnum, PhidalMatrix_NNZ(PREC_L_SCAL(P)), PhidalMatrix_NNZ(PREC_U_SCAL(P)));
	  fprintfd(stderr, "       Norm1: L =%g , U = %g \n", /* P->levelnum, */ PhidalMatrix_Norm1(PREC_L_SCAL(P)), PhidalMatrix_Norm1(PREC_U_SCAL(P)));
	}
      else
	{
	  fprintfd(stderr, "Level %d NNZ: L =%ld \n", P->levelnum, PhidalMatrix_NNZ(PREC_L_SCAL(P)));
	  fprintfd(stderr, "       Norm1: L =%g  \n", /* P->levelnum, */ PhidalMatrix_Norm1(PREC_L_SCAL(P)));
	}
      return;
    }
  if(P->forwardlev > 0)
    {
      PhidalPrec_PrintInfo(P->nextprec, BL);

      if(P->symmetric == 0)
	{
	  fprintfd(stderr, "Level %d NNZ: L =%ld , U = %ld E= F= S= \n", P->levelnum, PhidalMatrix_NNZ(PREC_L_SCAL(P)), PhidalMatrix_NNZ(PREC_U_SCAL(P)));
	  fprintfd(stderr, "       Norm1: L =%g , U = %g E= F= S= \n", /* P->levelnum, */ PhidalMatrix_Norm1(PREC_L_SCAL(P)), PhidalMatrix_Norm1(PREC_U_SCAL(P)));
	}
      else
	{
	  fprintfd(stderr, "Level %d NNZ: L =%ld E =%ld S = %ld \n", P->levelnum, PhidalMatrix_NNZ(PREC_L_SCAL(P)), PhidalMatrix_NNZ(PREC_E(P)), PhidalMatrix_NNZ(PREC_S_SCAL(P)));
	  fprintfd(stderr, "       Norm1: L =%g  E =%g  S = %g  \n", /* P->levelnum, */ PhidalMatrix_Norm1(PREC_L_SCAL(P)), PhidalMatrix_Norm1(PREC_E(P)), PhidalMatrix_Norm1(PREC_S_SCAL(P)));
	}

      return;
    }
}


#endif

/* @authors P. HENON */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>

#include "phidal_sequential.h"

/*** Local functions ***/
void PHIDAL_GEMMT_NN(REAL droptol, REAL *droptab, REAL fillrat, REAL alpha, PhidalMatrix *a, PhidalMatrix *b, PhidalMatrix *c, PhidalHID *BL, int *wki1, int *wki2, COEF *wkd);
void PHIDAL_GEMMT_NT(REAL droptol, REAL *droptab, REAL fillrat, REAL alpha, PhidalMatrix *a, PhidalMatrix *b, PhidalMatrix *c, PhidalHID *BL, int *wki1, int *wki2, COEF *wkd);

/*void PHIDAL_GEMM(char *TRANSA, char *TRANSB, char *TRANSC, PhidalMatrix *a, PhidalMatrix *b, PhidalMatrix *c, PhidalHID *BL, int *wki1, int *wki2, COEF *wkd)
  {
  PHIDAL_GEMMT(NULL, TRANSA, TRANSB, TRANSC, a, b, c, BL, wki1, wki2, wkd);
  }*/


void PHIDAL_GEMMT(REAL droptol, REAL *droptab, REAL fillrat, char *TRANSA, char *TRANSB, char *TRANSC, 
		  REAL alpha, PhidalMatrix *a, PhidalMatrix *b, PhidalMatrix *c, PhidalHID *BL, int *wki1, int *wki2, COEF *wkd)
{
  /************************************************************************************/
  /* This function performs (tranc)C = (transc)C + alpha.(transc)(transa)A.(transb)B  */
  /* This function admit fill-in element in C but                                     */ 
  /* any terms that is not create in the BLOCK pattern of C are dropped               */
  /************************************************************************************/

  int trA = 0;
  int trB = 0;
  int trC = 0;


 if(strcmp(TRANSA, "T") == 0)
    trA = 1;
  if(strcmp(TRANSB, "T") == 0)
    trB = 1;
  if(strcmp(TRANSC, "T") == 0)
    trC = 1;
  
  if(trA == 0 && trB == 0 && trC == 0)
    {
      PHIDAL_GEMMT_NN(droptol, droptab, fillrat, alpha, a, b, c, BL, wki1, wki2, wkd);
      return;
    }
  if(trA == 0 && trB == 1 && trC == 0)
    {
      PHIDAL_GEMMT_NT(droptol, droptab,fillrat, alpha, a, b, c, BL, wki1, wki2, wkd);
      return;
    }
  
  fprintfd(stderr, "PHIDAL_GEMM Not Yet implemented \n"); 
  exit(-1);
}


void PHIDAL_GEMMT_NN(REAL droptol, REAL *droptab, REAL fillrat, REAL alpha, PhidalMatrix *a, PhidalMatrix *b, PhidalMatrix *c, PhidalHID *BL, int *wki1, int *wki2, COEF *wkd)
{
  int jc;
  dim_t i, j;
  int nnb;
  int rind, cind;

  csptr *csrtab1;
  csptr *csrtab2;

#ifdef DEBUG_M
  assert(a->tli == c->tli);
  assert(a->bri == c->bri);
  assert(b->tlj == c->tlj);
  assert(b->brj == c->brj);
#endif

  csrtab1 = (csptr *)malloc(sizeof(csptr)* (a->brj-a->tlj+1));
  csrtab2 = (csptr *)malloc(sizeof(csptr)* (a->brj-a->tlj+1));

  for(i = c->tli; i <= c->bri; i++)
    for(jc = c->ria[i];jc < c->ria[i+1]; jc ++)
      {
	j = c->rja[jc];
	rind = a->ria[i];
	cind = b->cia[j];
	CS_IntersectRow(a->ria[i+1]-rind, a->rja + rind, a->ra + rind, 
			b->cia[j+1]-cind, b->cja + cind, b->ca + cind,
			&nnb, csrtab1, csrtab2);
	if(nnb >0)
	  {
	    if(droptab != NULL)
	      CSRrowMultCSRcol(droptol, droptab + (BL->block_index[i]-BL->block_index[c->tli]), fillrat, nnb, alpha, csrtab1, csrtab2, 
			       c->ra[jc],BL->block_index[j+1]-BL->block_index[j],  wki1, wki2, wkd);
	    else
	      CSRrowMultCSRcol(droptol, NULL , fillrat, nnb, alpha, csrtab1, csrtab2, 
			       c->ra[jc],BL->block_index[j+1]-BL->block_index[j], wki1, wki2, wkd);
	  }
	    
      }


  if(csrtab1 != NULL)
    free(csrtab1);
  if(csrtab2 != NULL)
    free(csrtab2);
}




void PHIDAL_GEMMT_NT(REAL droptol, REAL *droptab, REAL fillrat, REAL alpha, PhidalMatrix *a, PhidalMatrix *b, PhidalMatrix *c, PhidalHID *BL, int *wki1, int *wki2, COEF *wkd)
{
  assert(0);
}


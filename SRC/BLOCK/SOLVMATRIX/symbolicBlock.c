/* @authors J. GAIDAMOUR */

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>

#include "phidal_common.h"
#include "symbol.h"

int block(int lcolnum, int n, int *merge_col, BlokStruct* bloktab);

void symbolicBlok2(MatrixStruct* mtxstr, csptr P, dim_t cblknbr, dim_t *rangtab) {
  /** Difference with symbolicBLock is that this function use a
      compress form of the supernode in the matrix mat: mat[k] is 
      the supernode k **/
  dim_t i, k;
  int lcolnum;
  dim_t *ja;
  int nnznbr;
  BlokStruct *tmp_bloktab;


#ifdef DEBUG_M
  assert(P->n == cblknbr);
#endif
  
  tmp_bloktab = (BlokStruct *)malloc(sizeof(BlokStruct)*rangtab[cblknbr]);


  for(k=0;k<cblknbr;k++)
    {
      ja = P->ja[k];
      nnznbr = P->nnzrow[k];
      lcolnum = rangtab[k+1]-1;

      mtxstr[k].bloknbr = block(lcolnum, nnznbr, ja, tmp_bloktab);
      
      if (mtxstr[k].bloknbr != 0) { 
	mtxstr[k].bloktab = (BlokStruct *)malloc(sizeof(BlokStruct)*mtxstr[k].bloknbr);
	for(i=0;i<mtxstr[k].bloknbr;i++)
	  memcpy(mtxstr[k].bloktab+i, tmp_bloktab+i, sizeof(BlokStruct));
      } else { mtxstr[k].bloktab = NULL; }
      
      
    }
  
  free(tmp_bloktab);
}

void symbolicBlok(MatrixStruct* mtxstr, csptr mat, dim_t cblknbr, dim_t *rangtab) {
  dim_t k, i;
  int lcolnum;
  int *merge_col;
  int *tmp, *tmp2;
  BlokStruct *tmp_bloktab;
  int nnznbr, ind;


  /* Parcours des blocks colonnes */
  merge_col   = (int*)malloc(sizeof(int)*mat->n);
  tmp         = (int*)malloc(sizeof(int)*mat->n);
  tmp_bloktab = (BlokStruct *)malloc(sizeof(BlokStruct)*mat->n);

  for(k=0; k<cblknbr; k++) {
    ind = rangtab[k];
    memcpy(merge_col, mat->ja[ind], sizeof(int)*mat->nnzrow[ind]);
    nnznbr = mat->nnzrow[ind];
    for(i=rangtab[k]+1;i<rangtab[k+1];i++)
      {
	/** Cette fonction est dans PHIDAL/SRC/COMMON/set_func.c **/
	/** Realise la fusion de 2 listes triees croissantes **/
	UnionSet(merge_col, nnznbr, mat->ja[i], mat->nnzrow[i], tmp, &ind);
	
	/** echange merge_col et le resultat de la fusion **/
	nnznbr = ind;
	tmp2 = merge_col;
	merge_col = tmp;
	tmp = tmp2;
      }
#ifdef DEBUG_M
    /* TEST LONG ! */
    /*
      bzero(tmp, sizeof(int)*mat->n);
      for(i=rangtab[k];i<rangtab[k+1];i++)
      for(j=0;j<mat->nnzrow[i];j++) 
      tmp[mat->ja[i][j]] = 1;
      
      for(i=0;i<nnznbr;i++)
      tmp[merge_col[i]]++;
      
      for(i=0;i<mat->n;i++)
      assert(tmp[i] == 0 || tmp[i] == 2);
    */
#endif

    /** Maintenant merge_col contient la liste des non zeros 
	du cblk sous forme CSR **/

    /* Création des intervalles */
    lcolnum = rangtab[k+1] - 1;
    mtxstr[k].bloknbr = block(lcolnum, nnznbr, merge_col, tmp_bloktab);
    
    if (mtxstr[k].bloknbr != 0) { 
      mtxstr[k].bloktab = (BlokStruct *)malloc(sizeof(BlokStruct)*mtxstr[k].bloknbr);
      for(i=0;i<mtxstr[k].bloknbr;i++)
	memcpy(mtxstr[k].bloktab+i, tmp_bloktab+i, sizeof(BlokStruct));
    } else { mtxstr[k].bloktab = NULL; }


  }
  
  free(tmp_bloktab);
  free(tmp);
  free(merge_col);
}


















int block(int lcolnum, int n, int *merge_col, BlokStruct* bloktab) {   /* TODO : nom ! */
  dim_t j;
  int bloknbr;
  int frownum, lrownum;

  /* On place j apres le block diagonal */
  j = 0;
  while(j<n && merge_col[j] <= lcolnum)
    j++;

  /** On cherche les intervalles **/
  bloknbr = 0;
  while(j<n)
    {
      frownum = merge_col[j];
      while(j<n-1 && merge_col[j+1] == merge_col[j]+1)
	j++;
      lrownum = merge_col[j];
      bloktab[bloknbr].frownum = frownum;
      bloktab[bloknbr].lrownum = lrownum;
      bloknbr++;
      j++;
    }

  return bloknbr;
}

#ifdef TEST
#include <assert.h>
/* #undef Dprintf */
/* #define Dprintfv(5, args...) printf(args) */

int main() {
  dim_t i,j;

  int ja[] = {0,4,5,1,2,4,1,2,3,5,2,3,4,0,1,3,4,0,2,5};
  int ia[] = {0,3,6,10,13,17,19};

/*   int perm[]   = {0,1,2,3,4,5}; */
/*   int iperm[]  = {0,1,2,3,4,5}; */

  int perm[]   = {5,0,1,2,3,4};
  int iperm[]  = {1,2,3,4,5,0};

  int rangtab[] = {0, 1, 3, 4, 5, 6};
  dim_t cblknbr = 5;

  MatrixStruct* mtxstr = (MatrixStruct*)malloc(sizeof(MatrixStruct)*cblknbr);

  symbolicBlok(mtxstr, 20, ia, ja, perm, iperm, cblknbr, rangtab);  

/*   for(i=0; i<cblknbr; i++) { */
/*     Dprintfv(5, "%d\n",i); */
    
/*     for(j=0; j<mtxstr[i].bloknbr; j++) { */
/*       Dprintfv(5, "[%d %d] ",mtxstr[i].bloktab[j].frownum, mtxstr[i].bloktab[j].lrownum); */
/*     } */
/*     Dprintfv(5, "\n"); */
/*   } */

  int k=0;
  
/*   Test sans permutation */
/*   int resultn[] = {1,1,1,0,0}; */
/*   int result[] = {4,5, 3,5, 4,4}; */

/*    Test avec permutation */
  int resultn[] = {2,2,1,1,0};
  int result[] = {1,1, 3,3, 3,3, 5,5, 4,4, 5,5};

  for(i=0; i<cblknbr; i++) {
    assert(mtxstr[i].bloknbr == resultn[i]);

    for(j=0; j<mtxstr[i].bloknbr; j++) {
      assert(mtxstr[i].bloktab[j].frownum == result[k]); k++;
      assert(mtxstr[i].bloktab[j].lrownum == result[k]); k++;
    }

    free(mtxstr[i].bloktab); /* free(0) possible */
  }

  printfv(5, "OK.\n");

  free(mtxstr);

  return 0;
}

#endif

/* @authors J. GAIDAMOUR */

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include "block.h"

/* TODO */
/* void Dprintfv(5, const char *format, ...) { } */

int blokMerge(int l, CblkStruct*, CblkStruct*, BlokStruct* new);

/* besoin de rangtab pour le bloc diagonal */
void symbolicFacto(MatrixStruct* mtxstr, dim_t cblknbr, dim_t *treetab, dim_t *rangtab) {
  dim_t k;
  int newbloknbr;
  int facingcol;
  int lcolnum;
  BlokStruct* new;

  for(k=0;k<cblknbr;k++) { /*todo : le dernier est forcement skipped ?*/
    facingcol = treetab[k];
    if ((facingcol == k) || (facingcol == -1)) /*  = if node k is a root */ /* mettre une convention definitive ? */
      continue;                                /*    skip */

    if (facingcol >= cblknbr) /* phidal partition : if we compute fill-in for level 1 only */
      continue;
    
    lcolnum = rangtab[facingcol+1]-1;
    /*    printfv(5, "facingcol = %d lcolnum = %d\n", facingcol, lcolnum);*/
    
    /* parent(k) <- Bmerge(parent(k), k) */

    /* compute struct size */
    newbloknbr = blokMerge(lcolnum,&(mtxstr[facingcol]),&(mtxstr[k]), NULL);

    if (newbloknbr != 0) {
      new = (BlokStruct*)malloc(sizeof(BlokStruct)*newbloknbr);
      /* effective merge */
      blokMerge(lcolnum,&(mtxstr[facingcol]),&(mtxstr[k]), new);
    } else { new = NULL; }
    
    /* affect parent(k) */
    if (mtxstr[facingcol].bloknbr !=0) free(mtxstr[facingcol].bloktab); /*  can be ==NULL */ /* todo : virer le test*/
    mtxstr[facingcol].bloktab = new;
    mtxstr[facingcol].bloknbr = newbloknbr;
  }


}


/* compute merged intervall list */
/*TODO : optim, si y a pas de modif, le detecter */
int blokMerge(int parentlcolnum, CblkStruct* parentblk, CblkStruct* blk, BlokStruct* new) {
  int bloknbr = 0; /* new->bloknbr */
  int pb=0,b=0;   /* indices courants sur parentblk et blk */ /* TODO : b=1, bug de fin, refaire l'optim */

  /* skip blocks totalement inclus dans le bloc diagonal */
  /*todo : vraiment utile ? */
  while(b<blk->bloknbr) {
    if (blk->bloktab[b].lrownum <= parentlcolnum) {
      b++;
    } else break;
  }

  if (new != NULL) 
    if ((parentblk->bloknbr>0) && (blk->bloknbr>b)) /* else, frownum renseigné par la procédure de terminaison */
      new[0].frownum = MIN(blk->bloktab[b].frownum, parentblk->bloktab[0].frownum);

  while((b<blk->bloknbr) && (pb<parentblk->bloknbr)) {
    
    /* Case A1 */
    if (blk->bloktab[b].frownum > parentblk->bloktab[pb].lrownum +1) {
      /* Garder le block blk pour continuer, passer le block parentblk */
      
      if (new != NULL) {
	/* finalisation du block courant */
	new[bloknbr].lrownum = parentblk->bloktab[pb].lrownum;
      }
      
      bloknbr++;
      pb++;
      
      if (new != NULL) {
	/* début du block suivant */
	if (pb<parentblk->bloknbr) {
	  new[bloknbr].frownum = MIN(blk->bloktab[b].frownum, parentblk->bloktab[pb].frownum);
	  /* Dprintfv(5, "case1 %d\n",new[bloknbr].frownum); */
	}
      }
      
      continue;
    }

    /* Case A2 */
    if (blk->bloktab[b].lrownum+1 < parentblk->bloktab[pb].frownum) {
      /* Garder le block parentblk pour continuer, passer le block blk */
      
      if (new != NULL) {
	/* finalisation du block courant */
	new[bloknbr].lrownum = blk->bloktab[b].lrownum;
      }

      bloknbr++;
      b++;

      if (new != NULL) {
	/* debut du block suivant */
	if (b<blk->bloknbr) {
	  new[bloknbr].frownum = MIN(blk->bloktab[b].frownum, parentblk->bloktab[pb].frownum);
	  /* Dprintfv(5, "case2 %d\n",new[bloknbr].frownum); */
	}
      }

      continue;
    }

    /* Merging */
    /* On continue avec le blok qui va le plus loin */
    /*TODO  : doit obtenir le meme resultat en skipant cela avec new==NULL */
    if (blk->bloktab[b].lrownum </* ou<= */ parentblk->bloktab[pb].lrownum) { 
      b++; 
      if (b>=blk->bloknbr) {
	if (new != NULL) new[bloknbr].lrownum = parentblk->bloktab[pb].lrownum;
	bloknbr++;
	pb++;
      }
      
    } else { 
      pb++; 
      if (pb>=parentblk->bloknbr) {
	if (new != NULL) new[bloknbr].lrownum = blk->bloktab[b].lrownum;
	bloknbr++;
	b++;
      }
      
    }
    
    
    
  }


 
  if (pb>=parentblk->bloknbr) {
    /* il reste des blocks dans blk */
    /* Dprintfv(5, "Fin de parentblk atteinte\n"); */
    
    if (new != NULL) {
      while (b<blk->bloknbr) {
	new[bloknbr].frownum = blk->bloktab[b].frownum;
	new[bloknbr].lrownum = blk->bloktab[b].lrownum;
	bloknbr++;
	b++;
      }
    } else {
      bloknbr += blk->bloknbr - b;
    }

  } else {
    /* il reste des blocks dans parentblk */
    /* Dprintfv(5, "Fin de blk atteinte\n"); */
    
    if (new != NULL) {
      while (pb<parentblk->bloknbr) {
	new[bloknbr].frownum = parentblk->bloktab[pb].frownum;
	new[bloknbr].lrownum = parentblk->bloktab[pb].lrownum;
	bloknbr++;
	pb++;
      }
    } else {
      bloknbr += parentblk->bloknbr - pb;
    }

  }

  if (new != NULL) {
    if (new[0].frownum <= parentlcolnum) {
      new[0].frownum = parentlcolnum+1;
    }
  }
  
  return bloknbr;
}


#ifdef TEST
#include <assert.h>
#undef Dprintf
#define Dprintfv(5, args...)/*  printf(args) */

int main() {
  int i=0;
  CblkStruct col1;
  CblkStruct col2;

  col1.bloktab = (BlokStruct*)malloc(sizeof(BlokStruct)*2);
  col2.bloktab = (BlokStruct*)malloc(sizeof(BlokStruct)*4);

  col1.bloktab[i].frownum = 36;
  col1.bloktab[i].lrownum = 45;
  i++;

  col1.bloktab[i].frownum = 47;
  col1.bloktab[i].lrownum = 47;
  i++;

  col1.bloknbr = i;
  Dprintfv(5, "col1 : %d blocks\n",i);
  i = 0;

  col2.bloktab[i].frownum = 11;
  col2.bloktab[i].lrownum = 16;
  i++;

  col2.bloktab[i].frownum = 36;
  col2.bloktab[i].lrownum = 38;
  i++;

  col2.bloktab[i].frownum = 40;
  col2.bloktab[i].lrownum = 40;
  i++;

  col2.bloktab[i].frownum = 46;
  col2.bloktab[i].lrownum = 46;
  i++;

  col2.bloknbr = i;
  Dprintfv(5, "col2 : %d blocks\n",i);
  i = 0;


  /*   /\*   print interval list *\/ */
  /*   for (i=0; i<col1.bloknbr; i++) { */
  /*     printfv(5, "[%d,%d]\n", col1.bloktab[i].frownum, col1.bloktab[i].lrownum); */
  /*   } */
  /*   printfv(5, "\n"); */
  /*   /\*   print interval list *\/ */
  /*   for (i=0; i<col2.bloknbr; i++) { */
  /*     printfv(5, "[%d,%d]\n", col2.bloktab[i].frownum, col2.bloktab[i].lrownum); */
  /*   } */


  /*   compute struct size */
  int newbloknbr = blokMerge(11,&col1, &col2, NULL);
  Dprintfv(5, "new  : %d blocks\n",newbloknbr);
  
  BlokStruct* new = (BlokStruct*)malloc(sizeof(BlokStruct)*newbloknbr);
  
  /*   effective merge */
  blokMerge(11,&col1, &col2, new);

  /*   /\*   print interval list *\/ */
  /*   for (i=0; i<newbloknbr; i++) { */
  /*     printfv(5, "[%d,%d]\n", new[i].frownum, new[i].lrownum); */
  /*   } */

  printfv(5, "OK\n");
    
  free(col1.bloktab);
  free(col2.bloktab);
  free(new);

  return 0;
}

#endif

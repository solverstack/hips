/* @authors J. GAIDAMOUR */

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

/* #define COEF REAL */

#include <string.h> /* memcpy */
#include "block.h"

void SymbolMatrix_Init(SymbolMatrix* symbmtx) {
  memset(symbmtx, 0, sizeof(SymbolMatrix));
}

void VSolverMatrix_Clean(VSolverMatrix* vs) {
  SymbolMatrix_Clean(&vs->symbmtx);
  SolverMatrix_Clean(vs->solvmtx);

  vs->symbmtx.cblknbr  = 0;
  vs->symbmtx.ccblktab = NULL;
  vs->symbmtx.bcblktab = NULL;
  vs->symbmtx.stride  = NULL;
  vs->symbmtx.hdim = NULL;

  vs->symbmtx.bloknbr = 0;
  vs->symbmtx.bloktab = NULL;

  vs->symbmtx.nodenbr = 0;
  vs->symbmtx.tli = 0;
  vs->symbmtx.facedecal = 0;
  vs->symbmtx.cblktlj = 0;
  vs->symbmtx.virtual = 0;

  /**/

  memcpy(&vs->solvmtx->symbmtx, &vs->symbmtx, sizeof(SymbolMatrix));
  
  vs->solvmtx->coefnbr=0;
  vs->solvmtx->coefmax=0;
  
  vs->solvmtx->bloktab = NULL;
  vs->solvmtx->coeftab = NULL;
  
  vs->solvmtx->virtual=0;


}

/* Ou : ne pas faire un ADD, mais un cpy :) */
/* + simple en BLK */
void VSolverMatrix_CleanTmp(VSolverMatrix* a) {
    dim_t k,i;
    blas_t stride, hdim;
    COEF* bc;
    SymbolMatrix* symbmtx = &a->symbmtx;
    SolverMatrix* solvmtx = a->solvmtx;
    
    for (k=0; k<symbmtx->cblknbr; k++) {
      {
	hdim = symbmtx->hdim[k];
	if (hdim == 0) continue;
	
	bc = solvmtx->coeftab + solvmtx->bloktab[symbmtx->bcblktab[k].fbloknum].coefind;
	stride = symbmtx->stride[k];
	
	/**  **/
	for(i=symbmtx->ccblktab[k].fcolnum; i<=symbmtx->ccblktab[k].lcolnum; i++) {
	  bzero(bc, hdim*sizeof(COEF));
	  bc  += stride;
	}
	
      }
    }
}

/* Ou : ne pas faire un ADD, mais un cpy :) */
void VSolverMatrix_CleanTmpNODIAG(VSolverMatrix* a) {
    dim_t k,i;
    blas_t stride, hdim;
    COEF* bc;
    SymbolMatrix* symbmtx = &a->symbmtx;
    SolverMatrix* solvmtx = a->solvmtx;
    
    COEF ctmp;
    COEF* cptr;

    for (k=0; k<symbmtx->cblknbr; k++) {
      {
	hdim = symbmtx->hdim[k];
	if (hdim == 0) continue;
	
	bc = solvmtx->coeftab + solvmtx->bloktab[symbmtx->bcblktab[k].fbloknum].coefind;
	cptr = bc;
	stride = symbmtx->stride[k];
	
	/**  **/
	
	for(i=symbmtx->ccblktab[k].fcolnum; i<=symbmtx->ccblktab[k].lcolnum; i++) {
	  ctmp = cptr[0];
	  bzero(bc, hdim*sizeof(COEF));
	  cptr[0] = ctmp;
	  
	  bc    += stride;
	  cptr  += stride+1;
	}
	
      }
    }
}

void SymbolMatrix_Clean(SymbolMatrix* symbmtx) {
  if (symbmtx == NULL)
    return;
  assert((symbmtx->virtual == 0) || (symbmtx->virtual == 1));

  /* bcblktab */
  if (symbmtx->bcblktab != NULL)/*(NULL : 3)*/ free(symbmtx->bcblktab);
  
  /* hdim (stride always NULL or virtual) */
  if (symbmtx->hdim != NULL)/*(NULL : -0 or 3)*/ free(symbmtx->hdim);
  
  /* ccblktab and bloktab */
  if (symbmtx->virtual == 0) {
    if (symbmtx->ccblktab != NULL)/*(NULL : 3)  */               free(symbmtx->ccblktab);
    if (symbmtx->bloktab != NULL) /* après Init(Récup de ONE) */ free(symbmtx->bloktab);
  } 
  
  /* TODO : Init et ReInit */
}

void SolverMatrix_Clean(SolverMatrix* solvmtx) {
  if (solvmtx == NULL) return;

  assert((solvmtx->virtual == NOVIRTUAL) || (solvmtx->virtual == COPY));

  if (solvmtx->virtual == NOVIRTUAL) /* TODO : utiliser symbmtx->virtual */
    SymbolMatrix_Clean(&solvmtx->symbmtx);

  if (solvmtx->virtual == NOVIRTUAL) {
    if(solvmtx->symbmtx.bloknbr != 0) {
#ifdef DEBUG_M
      assert(solvmtx->bloktab != NULL);/*après init*/
#endif
      free(solvmtx->bloktab);
    }
  }
 
#ifndef DEBUG_NOALLOCATION
  if (solvmtx->coefnbr != 0) {
    /* assert faux pour E en right looking avec coeftabE */ 
    /*     assert(solvmtx->coeftab != NULL); */
    if(solvmtx->coeftab != NULL)
      free(solvmtx->coeftab);
  }
#endif
  /* TODO : Init et ReInit */
}


void SymbolMatrix_VirtualCopy(SymbolMatrix *M, SymbolMatrix *Cpy) {
  memcpy(Cpy, M, sizeof(SymbolMatrix));
  /* pas de flag VIRTUAL */
}

/* for compatibility */
void freeSymbolMatrix(SymbolMatrix* symbmtx) { /* todo : a transformer en "Clean" */
  SymbolMatrix_Clean(symbmtx);
  free(symbmtx);
}

void freeSolverMatrix(SolverMatrix* solvmtx) {
  SolverMatrix_Clean(solvmtx);
  free(solvmtx);
}

void freeMatrixStruct(MatrixStruct* mtxstr, dim_t cblknbr) {
  dim_t k; 
  for(k=0; k<cblknbr; k++) {
    if (mtxstr[k].bloktab != NULL)
      free(mtxstr[k].bloktab); /* free(0) possible*/
  }
  free(mtxstr);
}


int calcCoefmax(SymbolMatrix* symbmtx1 /*M*/, SymbolMatrix* symbmtx /*L*/) {
  dim_t k,p;
  int coefmax=0;
  int coefind, coefnbr, delta;

  for(k=0; k<symbmtx->cblknbr; k++) {
    
    delta = 0;
    for(p=symbmtx->bcblktab[k].fbloknum/*+1*/; p<=symbmtx->bcblktab[k].lbloknum; p++) {
      coefind = symbmtx->bloktab[p].lrownum - symbmtx->bloktab[p].frownum+1;
      if(coefind > delta) /* optim à faire : sans if */
	delta = coefind;
    }

    coefnbr = symbmtx1->hdim[k] * delta;

    if(coefnbr > coefmax)
      coefmax = coefnbr;
  }

  return coefmax;
}

/* todo : pb pour E : tlj <-> tli brj <-> bri */
int DBMatrix_calcCoefmax(DBMatrix* M /*M*/, DBMatrix* L /*L*/) {
  dim_t i, j, k, p;
  int coefind, coefnbr, delta;

  int coefmax = 0;

  int *strideM;

  SymbolMatrix *symbL;

  for(i=M->tlj;i<=M->brj;i++) {

    /*todo : simplifier*/
    if (M->alloc == ONE) /* ONE ou CBLK */ {
      strideM = M->ca[M->cia[i]]->symbmtx.stride;
    }
    else if (M->alloc == CBLK) /* CBLK -o-u- R-B-L-K */ {
      strideM = M->a[i-M->tlj].symbmtx.stride;
    } else if (M->alloc == RBLK) {
      strideM = M->hdim + 
	M->ca[M->cia[i]]->symbmtx.facedecal- /*f*/M->a[0].symbmtx.facedecal;
    } else { /* BLK */
      strideM = M->hdim + 
	M->ca[M->cia[i]]->symbmtx.facedecal- /*f*/M->ca[0]->symbmtx.facedecal;
    }
    
#ifdef DEBUG_M
/*     if (M->alloc == CBLK) { */
/*       assert(M->a[i-M->tlj].symbmtx.stride == M->ca[M->cia[i]]->symbmtx.stride); */
/*     } */
#endif
    
    /* no block in M for i*/
    if (M->cia[i] == M->cia[i+1]) continue;
    
    for(k=0;k<M->ca[M->cia[i]]->symbmtx.cblknbr;k++) {
      delta = 0;
          
      for(j=L->cia[i];j<L->cia[i+1];j++) {
	symbL = &L->ca[j]->symbmtx;

	assert(L->ca[j]->symbmtx.cblknbr == M->ca[M->cia[i]]->symbmtx.cblknbr);

	for(p=symbL->bcblktab[k].fbloknum; p<=symbL->bcblktab[k].lbloknum; p++) {
	  coefind = symbL->bloktab[p].lrownum - symbL->bloktab[p].frownum+1;
	  if(coefind > delta) /* optim à faire : sans if */
	    delta = coefind;
	}
      }

      coefnbr = strideM[k] * delta;

      if(coefnbr > coefmax)
	coefmax = coefnbr;
    }
  }

  return coefmax;
}

int calc_Emax(DBMatrix* E) {
  dim_t i;
  SolverMatrix* solvE;
  int Emax=0; /* coefnbr max */
  for(i=E->tlj;i<=E->brj;i++)
    {
      solvE = &E->a[i];
      if (solvE->coefnbr > Emax)
	Emax = solvE->coefnbr;
    }
  
  return Emax;
}


void MatrixStruct2SymbolMatrix(SymbolMatrix* symbmtx, MatrixStruct* mtxstr, dim_t cblknbr, dim_t *rangtab) {
  dim_t k,j,p;
  int facingcblk;
/* #define TIME_DEBUG */
#ifdef TIME_DEBUG
  chrono_t t1  = dwalltime(); 
  chrono_t t2;
  printf("MatrixStruct2SymbolMatrix\n");
#endif

  /*TODO : utiliser SymbMatrix_init*/

  symbmtx->cblknbr   = cblknbr;         /*+ Number of column blocks           +*/
  symbmtx->facedecal = 0;
  symbmtx->tli = 0;
  symbmtx->virtual=0;
    
  symbmtx->ccblktab = (SymbolCCblk*)malloc(sizeof(SymbolCCblk)*cblknbr);    /*+ Array of column blocks [based] +*/
  symbmtx->bcblktab = (SymbolBCblk*)malloc(sizeof(SymbolBCblk)*cblknbr);    /*+ Array of column blocks [based] +*/
  symbmtx->hdim     = NULL;
  symbmtx->stride   = NULL;

#ifdef TIME_DEBUG
  REAL t3, t4;
  t3  = dwalltime(); 
#endif

  /**** Compute blocknbr and nodenbr */
  symbmtx->bloknbr = 0;
  symbmtx->nodenbr = rangtab[cblknbr] - rangtab[0];

#define INDEX
#ifdef INDEX
  /* build index */
  int *expanded_rangtab = (int*)malloc(sizeof(int)*(rangtab[cblknbr]));
  
  {
   dim_t i, k;

   i=rangtab[0];
   for(k=0; k<cblknbr; k++) {
     for(/*i=rangtab[k]*/; i<rangtab[k+1]; i++) {
       expanded_rangtab[i] = k;
     }
   }
  }
#endif

   for(k=0; k<cblknbr; k++) {
    /*     Dprintfv(5, "%d\n", k); */
    /*     Dprintfv(5, "blknbr= %d\n", mtxstr[k].bloknbr); */

    symbmtx->bloknbr += mtxstr[k].bloknbr +1; /* +1 = diagonal block;*/

/* #undef INDEX */ /* undef for debug here (assert) */    
#ifndef INDEX
    facingcblk = k+1;
#endif
    for(p=0;p<mtxstr[k].bloknbr;p++){
 
#ifndef INDEX
      while(mtxstr[k].bloktab[p].frownum > rangtab[facingcblk+1]-1/*.lcolnum*/)
	facingcblk++;
      assert(facingcblk == expanded_rangtab[mtxstr[k].bloktab[p].frownum]);
#else
      facingcblk = expanded_rangtab[mtxstr[k].bloktab[p].frownum];
#endif

      while(mtxstr[k].bloktab[p].lrownum > rangtab[facingcblk+1]-1/*.lcolnum*/) {
	symbmtx->bloknbr++; /* = bloc supplémentaire	*/
	facingcblk++;
      }
    }

  }
#ifdef TIME_DEBUG
  t4  = dwalltime(); 
  printf(" MatrixStruc2SymbolMatrix 1/2 in %g seconds\n\n", t4-t3);

  t3  = dwalltime(); 
#endif

  /**** Filling up data structure */
  symbmtx->bloktab = (SymbolBlok*)malloc(sizeof(SymbolBlok)*symbmtx->bloknbr);

  p=0; /* block index in symbmtx->bloktab*/
  for(k=0; k<cblknbr; k++) {
    symbmtx->ccblktab[k].fcolnum = rangtab[k];
    symbmtx->ccblktab[k].lcolnum = rangtab[k+1]-1;
    symbmtx->bcblktab[k].fbloknum = p;

    /* diagonal block */ /*todo : a ne pas distinguer*/
    symbmtx->bloktab[p].frownum = rangtab[k]; 
    symbmtx->bloktab[p].lrownum = rangtab[k+1]-1;
    symbmtx->bloktab[p].cblknum = k;
    p++;

#ifndef INDEX
    facingcblk = k+1;
#endif

    /* non-diag block (copy interval struct to blocktab) */
    for(j=0; j<mtxstr[k].bloknbr; j++, p++) {
      
      /* Dprintfv(5, "mtxstr[k=%d/%d].bloktab[j=%d].frownum = %d | mtxstr[k].bloknbr=%d\n",k,cblknbr,j,mtxstr[k].bloktab[j].frownum,mtxstr[k].bloknbr); */

#ifndef INDEX
      while(mtxstr[k].bloktab[j].frownum > rangtab[facingcblk+1]-1/*.lcolnum*/)
	facingcblk++;
#else
      facingcblk = expanded_rangtab[mtxstr[k].bloktab[j].frownum];
#endif

      symbmtx->bloktab[p].frownum = mtxstr[k].bloktab[j].frownum; 
      symbmtx->bloktab[p].cblknum = facingcblk;      
      /* Dprintfv(5, "assert %d < %d\n", facingcblk, cblknbr); */
      assert(facingcblk < cblknbr);
      /*   if (j==0) assert(symbmtx->bloktab[p].cblknum == treetab[k]); **       only for symbol  ATTENTION : root= -1   */

      while(mtxstr[k].bloktab[j].lrownum > rangtab[facingcblk+1]-1/*.lcolnum*/) {
	symbmtx->bloktab[p].lrownum = rangtab[facingcblk+1]-1;
	p++; facingcblk++;
	symbmtx->bloktab[p].frownum = rangtab[facingcblk]; /* Dprintfv(5, "frownum %d (%d)\n",p, symbmtx->bloktab[p].frownum); */
	
	symbmtx->bloktab[p].cblknum = facingcblk;
	assert(facingcblk < cblknbr);
      }
      
      symbmtx->bloktab[p].lrownum = mtxstr[k].bloktab[j].lrownum;
    }

    symbmtx->bcblktab[k].lbloknum = p-1; /*todo : deplacer les p++*/
  }
#ifdef TIME_DEBUG
  t4  = dwalltime(); 
  printf(" MatrixStruc2SymbolMatrix 2/2 in %g seconds\n\n", t4-t3);
#endif

#ifdef INDEX
  free(expanded_rangtab);
#endif

  /* Dprintfv(5, "pmax = %d\n",p); */
#ifdef TIME_DEBUG
  t2  = dwalltime(); 
  printf(" MatrixStruc2SymbolMatrix in %g seconds\n\n", t2-t1);
#endif
  assert(p == symbmtx->bloknbr);
}


/**** Compute coefnbr, stride, coefind */
void DBMatrix_Setup_solvermatrix(SolverMatrix* solvmtx, SymbolMatrix* symbmtx) {
  int k, p, stride;
  /*   SymbolMatrix* symbmtx = &solvmtx->symbmtx; */

  assert(symbmtx->bcblktab != NULL);
  
  solvmtx->virtual = NOVIRTUAL;
  if (symbmtx->bloknbr != 0)
    solvmtx->bloktab = (SolverBlok*)malloc(sizeof(SolverBlok)*symbmtx->bloknbr);
  else solvmtx->bloktab = 0;

  solvmtx->coefnbr = 0;
  for(k=0; k<symbmtx->cblknbr; k++) {
    stride = 0;
    
    /* block */
    for(p=symbmtx->bcblktab[k].fbloknum; p<=symbmtx->bcblktab[k].lbloknum; p++) {
      solvmtx->bloktab[p].coefind = solvmtx->coefnbr + stride;
      stride += symbmtx->bloktab[p].lrownum - symbmtx->bloktab[p].frownum + 1;
    }
    
    solvmtx->coefnbr += (symbmtx->ccblktab[k].lcolnum - symbmtx->ccblktab[k].fcolnum +1) * stride;
  }
}


/**** Compute coefnbr, stride, coefind */
void DBMatrix_Setup2_solvermatrix(SolverMatrix* solvmtx) {
  int k, p, stride;
  SymbolMatrix* symbmtx = &solvmtx->symbmtx;
  
  solvmtx->virtual = NOVIRTUAL;
  solvmtx->bloktab = (SolverBlok*)malloc(sizeof(SolverBlok)*symbmtx->bloknbr);
  symbmtx->stride = symbmtx->hdim = (int*)malloc(sizeof(int)*symbmtx->cblknbr);

  solvmtx->coefnbr = 0;
  for(k=0; k<symbmtx->cblknbr; k++) {
    stride = 0;
    
    /* block */
    for(p=symbmtx->bcblktab[k].fbloknum; p<=symbmtx->bcblktab[k].lbloknum; p++) {
      solvmtx->bloktab[p].coefind = solvmtx->coefnbr + stride;
      stride += symbmtx->bloktab[p].lrownum - symbmtx->bloktab[p].frownum + 1;
    }
    
    solvmtx->coefnbr += (symbmtx->ccblktab[k].lcolnum - symbmtx->ccblktab[k].fcolnum +1) * stride;
    symbmtx->stride[k] = stride;
  }
}


void DBMatrix_Symb2SolvMtx(SolverMatrix* solvmtx) {
#ifdef DEBUG_NOALLOCATION
  solvmtx->coeftab = NULL;
  return;
#endif

  if (solvmtx->coefnbr != 0) {
    solvmtx->coeftab = (COEF *)malloc(sizeof(COEF)*solvmtx->coefnbr);
    if(solvmtx->coeftab == NULL)
      {
	fprintferr(stderr, "ERROR : malloc of coefficients failed. Coefnbr was = %ld \n", (long)solvmtx->coefnbr);
	assert(0);
      }
  } else { solvmtx->coeftab = NULL; }

  memset(solvmtx->coeftab, 0, sizeof(COEF)*(solvmtx->coefnbr)); /*TODO : a virer*/
}

void DBMatrix_Symb2SolvMtx_(SolverMatrix* solvmtx, COEF* coeftab) {
#ifdef DEBUG_NOALLOCATION
  solvmtx->coeftab = NULL;
  return;
#endif

  if (solvmtx->coefnbr != 0) {
    solvmtx->coeftab = coeftab;
    assert(solvmtx->coeftab != NULL);
  } else { solvmtx->coeftab = NULL; }

  memset(solvmtx->coeftab, 0, sizeof(COEF)*(solvmtx->coefnbr)); /*TODO : a virer ?? */
}

#define OBSOLETE
#ifdef OBSOLETE
void csr2SolverMatrix(SolverMatrix* solvmtx, csptr mat) {
  SymbolMatrix* symbmtx;
  symbmtx = &solvmtx->symbmtx;

  DBMatrix_Setup2_solvermatrix(solvmtx);
  DBMatrix_Symb2SolvMtx(solvmtx);

  SparRow2SolverMatrix(0,0,mat,&solvmtx->symbmtx,solvmtx);
  solvmtx->coefmax = calcCoefmax(symbmtx, symbmtx);
}
#endif

#ifdef OBSOLETE2
void csr2SolverMatrix(SolverMatrix* solvmtx, csptr mat) {
  dim_t k,p;
  dim_t i,j;
  int width;

  int delta;
  int coefind, coefnbr;

  /*   int count; */

  int jx;
  REAL* ind;
  SymbolMatrix* symbmtx;

  /*   int first; */

  assert(solvmtx != NULL);  
  symbmtx = &solvmtx->symbmtx;

  solvmtx->cblktab = (SolverCblk*)malloc(sizeof(SolverCblk)*symbmtx->cblknbr);
  solvmtx->bloktab = (SolverBlok*)malloc(sizeof(SolverBlok)*symbmtx->bloknbr);
  /* Dprintfv(5, "> %p\n",solvmtx->bloktab); */

  /* TODO : use void SolverMatrix_Config(SolverMatrix* solvmtx) { */  
  /**** Compute coefnbr, stride, coefind */
  solvmtx->coefnbr = 0;
  for(k=0; k<symbmtx->cblknbr; k++) {
    width = symbmtx->cblktab[k].lcolnum - symbmtx->cblktab[k].fcolnum +1;

    /* diagonal block */
    p=symbmtx->cblktab[k].fbloknum;
    solvmtx->bloktab[p].coefind = solvmtx->coefnbr;
    solvmtx->cblktab[k].stride = width;
    p++;
    
    /* extra-diag block */
    for(; p<=symbmtx->cblktab[k].lbloknum; p++) {
      solvmtx->bloktab[p].coefind = solvmtx->coefnbr + solvmtx->cblktab[k].stride;
      solvmtx->cblktab[k].stride += symbmtx->bloktab[p].lrownum - symbmtx->bloktab[p].frownum + 1;
    }
    
    solvmtx->coefnbr += width * solvmtx->cblktab[k].stride;
    /* Dprintfv(5, "k=%d hdim= %d\n", k, symbmtx->cblktab[k].hdim); */
    symbmtx->cblktab[k].hdim = solvmtx->cblktab[k].stride; /*todo : a faire dans la partie création symbmtx + tot*/
  }

  /** Compute CoefMax **/
  solvmtx->coefmax = 0;
  for(k=0; k<symbmtx->cblknbr; k++) {
    delta = 0;
    for(p=symbmtx->cblktab[k].fbloknum/*+1*/; p<=symbmtx->cblktab[k].lbloknum; p++) {
      coefind = symbmtx->bloktab[p].lrownum - symbmtx->bloktab[p].frownum+1;
      if(coefind > delta)
	delta = coefind;
    }

    coefnbr = solvmtx->cblktab[k].stride * delta;

    if(coefnbr > solvmtx->coefmax)
      solvmtx->coefmax = coefnbr;
  }
    
  /*  Dprintfv(5, "COEFMAX : %d\n",solvmtx->coefmax);*/

  solvmtx->coeftab = (REAL *)malloc(sizeof(REAL)*solvmtx->coefnbr);
  assert(solvmtx->coeftab != NULL);

  ind = solvmtx->coeftab;

  /* Parcours des blocks colonnes */
  for(k=0; k<symbmtx->cblknbr; k++) {
    
    /* Parcours des colonnes du bloc colonnes */
    for(i = symbmtx->cblktab[k].fcolnum; i<=symbmtx->cblktab[k].lcolnum; i++) {
/*       printfv(5, "i: %d / %d\n",i,symbmtx->cblktab[symbmtx->cblknbr-1].lcolnum); */

/*       count = 0; */

      /* Copie des termes de 'a' dans 'solvmtx' pour la colonne i */
      jx=0;

      assert(mat->nnzrow[i] != 0);
      if(mat->nnzrow[i] != 0) /* quand meme bizarre ../MATRICES/BCSSTK15 10 */
	{
	  while(mat->ja[i][jx] < i/*ou fcol*/) { jx++; /* count++; */ }

	  /*       /\* TODO*\/ */
	  /*       p = symbmtx->cblktab[k].fbloknum; */
	  /*       first =  symbmtx->bloktab[p].frownum - symbmtx->tli; */
	  /*       while(mat->ja[i][jx] < first) { jx++; count++; } */
	  /*       /\**\/ */
	  assert(jx < mat->nnzrow[i]);
	}

      for(p=symbmtx->cblktab[k].fbloknum; p<=symbmtx->cblktab[k].lbloknum; p++) {
	for(j=symbmtx->bloktab[p].frownum; j<=symbmtx->bloktab[p].lrownum; j++) {
	  if (jx < mat->nnzrow[i])
	    if (mat->ja[i][jx] == j) {
	      *ind = mat->ma[i][jx];
	      jx++;/*  count++; */
	    } else { *ind = 0; }
	  else { *ind = 0; }  /*/todo : simplifier TODO : skip plus efficace. Trier ou non ?*/
	  
	  ind++;
	}
	
      }

/*       /\*\      TODO : Si facto complète*\/ */
/*       if (count != mat->nnzrow[i]) */
/* 	printfv(5, "%d %d\n", count, mat->nnzrow[i]); */
/*             assert(count == mat->nnzrow[i]); */
      
    }
          
  }

/*   printfv(5, "WARNING : count enable\n"); */
}



#endif

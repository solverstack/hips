/* @authors J. GAIDAMOUR, P. HENON */

#define OBSOLETE
#ifdef OBSOLETE
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#include <string.h>

#include <assert.h>
#include "solver.h"

#include "phidal_common.h"
#include "base.h"

#ifdef WITH_PASTIX
#include "pastix_calls.h"
#endif

#define MAX(x,y) (((x)<(y))?(y):(x))
#define MIN(x,y) (((x)>(y))?(y):(x))

/*TODO optim = access a coeftab multiple, les limiter*/
/* TODO : 2 blocknums */

/*idem pour bloknum, sans doute optimisable*/

#if defined(TYPE_REAL)
#define STATIC_PIV(arg) if(arg >= 0) arg = epsilon; else arg = -epsilon
#elif defined (TYPE_COMPLEX)
#define STATIC_PIV(arg) arg = epsilon
#else
#error TYPE_REAL / TYPE_COMPLEX undefined
#endif

/**** This function allows the static pivoting of small diagonals entries ****/
int LDLt_piv(int n, blas_t stride, COEF *coefftab, REAL epsilon)
{
  /******************************************************************************************/
  /* This function computes the LDLt    factorization of a dense  matrix stored in a single */
  /* vector by column                                                                       */
  /* On entry :                                                                             */
  /* n : dimension of the matrix                                                            */
  /* stride : stride to pass from a column to another in the vector                         */
  /* normA : the norm of matrix (used for static numerical pivoting)                        */
  /* epsilon : the tolerance that will be used in the accelerator (also used for pivoting)  */
  /* On return :                                                                            */
  /*  The matrix contains in its lower triangular part the L factor and the D factor is     */
  /* stored in the diagonal                                                                 */
  /******************************************************************************************/

  dim_t i, j;
  COEF *colptr, *facolptr;
  COEF f;
  int UN = 1;
  int len;

  for(i=0;i<n;i++)
    {
      /** Find the index of the next column  **/
      colptr = coefftab + i*stride;
#ifndef DEBUG_NOALLOCATION      

      /** Static pivoting **/
      if(coefabs(colptr[i]) < epsilon)
	{
	  fprintferr(stderr, "Pivot %ld is too small = "_coef_" : replace it by %g \n", (long)i, pcoef(colptr[i]), epsilon);

	  STATIC_PIV(colptr[i]);
	}

      /*piv = 1.0/colptr[i];*/
#endif
      /** Update lower triangular part L **/
      for(j=i+1;j<n;j++)
	{
#ifndef DEBUG_NOALLOCATION      
	  f = -colptr[j]/colptr[i]; /* == -colptr[j]*piv */
#endif
	  facolptr = coefftab + stride * j;
	  len = n-j;
	  BLAS_AXPY(len, f, colptr+j, UN, facolptr+j, UN);
#ifndef DEBUG_NOALLOCATION      
	  colptr[j] = -f;
#endif
	}
    }

  return 0;
}  


void LU(int n, COEF *coeftabL, blas_t strideL, COEF *coeftabU, blas_t strideU, REAL epsilon)
{
  dim_t i, j;
  COEF *colptrL, *colptrU, *facolptrL, *facolptrU;
  /* COEF piv; */
  COEF f;
  int len;
  int UN = 1;

#ifdef DEBUG_NOALLOCATION
  return;
#endif

  for(i=0;i<n;i++)
    {
      colptrU = coeftabU + i*strideU;
      colptrL = coeftabL + i*strideL;

#ifndef DEBUG_NOALLOCATION      
      /** Static pivoting **/
      if(coefabs(colptrU[i]) < epsilon)
	{
	  fprintferr(stderr, "Pivot %ld is too small = "_coef_" : replace it by %g \n", (long)i, pcoef(colptrU[i]), epsilon);
	  STATIC_PIV(colptrU[i]);
	}
#endif

      /* piv = 1.0/colptrU[i]; */

      for(j=i+1;j<n;j++)
	{
	  f = -colptrL[j]/colptrU[i]; /* == -colptrL[j]*piv */

	  facolptrL = coeftabL + strideL * (i+1);
	  len = j-(i+1);

	  BLAS_AXPY(len, f, colptrU+(i+1), UN, facolptrL+j, strideL);

	  facolptrU = coeftabU + strideU * j;
	  len = n-j;
	  BLAS_AXPY(len, f, colptrU+j, UN, facolptrU+j, UN);
   
	  colptrL[j] = -f;
	}
    }

} 


void VS2_ICCT(SolverMatrix* solvmtx, SymbolMatrix* symbmtx, COEF* F, COEF* W);


void print_HIPS_Matrix(SolverMatrix *solvmtx, SymbolMatrix *symbmtx) {
  COEF *ptr;
  int first_cblok, k, m, bloknum, numline, numcol, i, stride;

  ptr = solvmtx->coeftab + solvmtx->bloktab[symbmtx->bcblktab[0].fbloknum].coefind ;  
  
  first_cblok = symbmtx->facedecal;
  
  for(k = 0; k < symbmtx->cblknbr; k++) 
    {
      stride = symbmtx->stride[k];

      for(m = 0; m < symbmtx->ccblktab[k].lcolnum - symbmtx->ccblktab[k].fcolnum + 1; m++) {
	bloknum = symbmtx->bcblktab[k].fbloknum;	
	numline = symbmtx->bloktab[bloknum].frownum;
	numcol = symbmtx->ccblktab[k].fcolnum + m;
	
	for(i=0; i<stride; i++) {	  
	  
	  fprintf(stderr, "COEF : cblk=%d blk=%d col=%d line=%d %f\n", first_cblok+k, bloknum,numcol, numline, ptr[i]);
	  
	  if(numline == symbmtx->bloktab[bloknum].lrownum) {
	    bloknum++;
	    numline = symbmtx->bloktab[bloknum].frownum;
	  }
	  else
	    numline++;
	}
	
	ptr+=stride;
      }
    }
}

void VS_ICCT(VSolverMatrix* vsolvmtx, COEF* F, COEF* W) {
  /* Call to sequential factorization with HIPS */
  VS2_ICCT(vsolvmtx->solvmtx, &vsolvmtx->symbmtx, F, W);
}

#ifdef WITH_PASTIX
void VS_ICCT_pastix(pastix_struct *pastix_str, VSolverMatrix* vsolvmtx, COEF* F, COEF* W) {
  assert(pastix_str != NULL);
  pastix_init(pastix_str, &vsolvmtx->symbmtx);
  pastix_factorization(pastix_str, vsolvmtx->solvmtx, NULL, &vsolvmtx->symbmtx, 2);
  
  VS2_ICCT(vsolvmtx->solvmtx, &vsolvmtx->symbmtx, F, W);
}
#endif

void numericFacto(SolverMatrix* solvmtx)
{
  COEF *W, *F; /** buffers de travail **/

  /*** Allocation de buffers de travail ***/
  F = (COEF *)malloc(sizeof(COEF)*solvmtx->coefmax);
  assert(F != NULL);
  W = (COEF *)malloc(sizeof(COEF)*solvmtx->coefmax);
  assert(W != NULL);

  VS2_ICCT(solvmtx, &solvmtx->symbmtx, F, W);


  /** LibÃ©ration des buffers **/
  free(W);
  free(F);
}

void VS2_ICCT(SolverMatrix* solvmtx, SymbolMatrix* symbmtx, COEF* F, COEF* W) {
  /*************************************************************/
  /* This function performs in place the L.Lt factorization of */
  /* a symmetric sparse matrix structured in dense blocks of   */
  /* coefficients                                              */
  /*************************************************************/
  dim_t p, q, k, m;
  int UN=1;
  COEF alpha, beta;
  blas_t stride;
  int cdim, rdim, hdim, mdim;
  COEF *cc; 
  COEF *bc; 
  COEF *fc; 
  COEF *wc; 
  int facestride;
  int bloknum, facecblknum;
  int decalcol;
  int decalrow;

  assert(F != NULL);
  assert(W != NULL);

  for(k=0;k<symbmtx->cblknbr;k++)
    {
      cdim   = symbmtx->ccblktab[k].lcolnum - symbmtx->ccblktab[k].fcolnum + 1; /** Largeur du bloc colonne **/
      stride = symbmtx->stride[k];
      
      /**********************************/
      /* Factorisation du bloc diagonal */
      /**********************************/
      /** Calcul du pointeur dans coeftab vers le  dÃ©but des coefficients du bloc diagonal **/ 
      p  = symbmtx->bcblktab[k].fbloknum;
      cc = solvmtx->coeftab + solvmtx->bloktab[p].coefind;
      LDLt_piv(cdim, stride, cc, EPSILON);
      
      if(k == symbmtx->cblknbr -1)
	continue;  /** Pas de bloc extradiagonal le calcul est terminer **/

      /************************************************************/
      /* "Diviser" les blocs extra diagonaux par le bloc diagonal */
      /************************************************************/
      {
	/** On effectue l'operation : M = M.(L^-1)t  avec M = l'ensemble compacte des blocs extra-diagonaux du bloc colonne **/
	char *side = "R";
	char *uplo = "L";
	char *trans = "T";
	char *diag = "U";
	
	alpha = 1.0;
	
	/** Calcul du pointeur dans coeftab vers le  dÃ©but des coefficients du premier bloc extra diagonal **/ 
	bloknum = symbmtx->bcblktab[k].fbloknum+1; /** Indice du premier bloc extra-diagonal **/ /*todo = p+1*/

	bc = solvmtx->coeftab + solvmtx->bloktab[bloknum].coefind;

	rdim = symbmtx->hdim[k] - cdim;  /** Hauteur de la surface compactÃ©e des bloc extra-diagonaux **/
	BLAS_TRSM(side,  uplo, trans, diag, rdim, cdim, alpha, cc, stride, bc, stride);
	
	/** Calcul de F dans un buffer temporaire: on divise toutes les colonnes par leur terme diagonal  **/
	for(m=0;m<cdim;m++)
	  {
	    /** Pointeur vers la m-ieme colonne dans le bloc colonne **/
	    bc = solvmtx->coeftab + solvmtx->bloktab[bloknum].coefind + m*stride;

	    /** Pointeur vers la m-ieme colonne dans le buffer F **/
	    fc = F + m*stride;
#ifndef DEBUG_NOALLOCATION      
	    alpha = 1.0/ cc[m*stride + m]; /** alpha est le m-ieme terme diagonal du bloc diagonal **/
#endif
	    BLAS_COPY(rdim, bc, UN, fc, UN); /** On copie les bloc extra diagonaux dans F **/
	    BLAS_SCAL(rdim, alpha, fc, UN); /** On divise la m-ieme colonne par le terme diagonal **/
	  }
      }
    
      /** Boucle sur les blocs extra-diagonaux **/
      /*      rdim = stride - cdim;*/  /** rdim est la hauteur des bloc extra-diagonaux >= p dans le bloc colonne k **/
                             /** On l'initialise Ã  la heuteur totale des bloc extra diagonaux **/

      for(p=symbmtx->bcblktab[k].fbloknum+1;p<=symbmtx->bcblktab[k].lbloknum;p++)
	{

	  /*** Stocker dans W le produit des blocs {A(q,k), q>=p} avec Ft(p,k) ***/
	  char *opA = "N"; /** Ne pas transposer A(**) **/
	  char *opF = "T"; /** Transpose F **/

	  /** Calcul du pointeur dans coeftab vers le dÃ©but des coefficients du bloc extra-diagonal p **/ 
	  bc = solvmtx->coeftab + solvmtx->bloktab[p].coefind;

	  /** Calcul du pointeur dans F vers le debut des coefficients du bloc extra-diagonal p correspondant **/
	  bloknum = symbmtx->bcblktab[k].fbloknum + 1; /** Indice du premier bloc extra-diagonal**/ 
	  /* TODO : ne dépend que de k TODO : juste pour la ligne d'apres et pour la ligne du for(p*/
	  fc = F + solvmtx->bloktab[p].coefind -  solvmtx->bloktab[ bloknum ].coefind;

	  /** Calcul de la hauteur du bloc extra diagonal p **/
	  hdim = (symbmtx->bloktab[p].lrownum - symbmtx->bloktab[p].frownum+1); /* TODO : parenthèse */
	  alpha = 1.0;
	  beta = 0.0;

	  assert(stride*hdim <= solvmtx->coefmax);

	  BLAS_GEMM(opA, opF, rdim, hdim, cdim, alpha, bc, stride, fc, stride, beta, W, stride);
  
	  /****************************************************************************************/
	  /** Pour tout les blocs extra-diagonaux {A(j,k), j>= i} faire A(i,j) = A(i,j) - W(i, ) **/
	  /****************************************************************************************/
	  facecblknum = symbmtx->bloktab[p].cblknum - symbmtx->facedecal; /** Indice du bloc colonne en face du bloc extra-diagonal p **/

	  /** Stride du cblk en face du bloc extra-diagonal p **/
	  facestride = symbmtx->stride[facecblknum];

	  /** Nombre de colonne "Ã  gauche" de la zone modifiÃ©e dans le bloc colonne facecblknum **/
	  decalcol = symbmtx->bloktab[p].frownum - symbmtx->ccblktab[facecblknum].fcolnum;

	  /*** Largueur de la zone modifiÃ©e == hauteur du bloc p dans le bloc colonne k **/
	  mdim = symbmtx->bloktab[p].lrownum - symbmtx->bloktab[p].frownum+1;

	  bloknum = symbmtx->bcblktab[facecblknum].fbloknum; /** Indice du bloc diagonal du bloc colonne facebloknum **/

	  for(q=p;q<=symbmtx->bcblktab[k].lbloknum;q++) /** Pour tous les bloc extradiagonaux A(q,k) avec q >= p **/
	    {

	      /* bloknum = symbmtx->cblktab[facecblknum].fbloknum; */ /** Indice du bloc diagonal du bloc colonne facebloknum **/ /*TODODODO*/

	      /* skip block that cannot match */
	      while(symbmtx->bloktab[q].frownum > symbmtx->bloktab[bloknum].lrownum) {
		bloknum++;
		if (bloknum >= symbmtx->bcblktab[facecblknum].lbloknum+1/*todo le +1*/) goto fin;
	      }
	      
	      /* for every block that match */
	      while(symbmtx->bloktab[q].lrownum >= symbmtx->bloktab[bloknum].frownum) {

		/* assert ds le cas direct*/
/*		assert(symbmtx->bloktab[bloknum].frownum <= symbmtx->bloktab[q].frownum);*/
/*		assert(symbmtx->bloktab[bloknum].lrownum >= symbmtx->bloktab[q].lrownum);*/
		
		decalrow = symbmtx->bloktab[q].frownum - symbmtx->bloktab[bloknum].frownum;
		
		/** Calcul du pointeur de debut de la zone modifiÃ©e dans le bloc A(bloknum, facebloknum) **/
		bc = solvmtx->coeftab + solvmtx->bloktab[bloknum].coefind + decalcol*facestride + MAX(0,decalrow);
	      
		/** Calcul du pointeur de debut du bloc correspondant Ã  A(q,k) dans W **/
		wc = W + solvmtx->bloktab[q].coefind - solvmtx->bloktab[p].coefind + MAX(0,-decalrow);
		
		/*** Update de la contribution ***/
		hdim = MIN(symbmtx->bloktab[q].lrownum, symbmtx->bloktab[bloknum].lrownum) - 
		  MAX(symbmtx->bloktab[q].frownum, symbmtx->bloktab[bloknum].frownum) +1; /** Hauteur du bloc sur lequel on effectue la différence **/

/* 		printfv(5, "hdim= %d | %d %d | %d %d\n", hdim, symbmtx->bloktab[q].frownum, symbmtx->bloktab[q].lrownum, */
/* 		       symbmtx->bloktab[bloknum].frownum, symbmtx->bloktab[bloknum].lrownum); */

		/*assert cas direct*/
/*		assert(hdim == symbmtx->bloktab[q].lrownum -  symbmtx->bloktab[q].frownum +1);*/

		alpha = -1.0;
		for(m=0; m < mdim; m++)
		  {
		    BLAS_AXPY(hdim, alpha, wc, UN, bc, UN);
		    wc += stride;
		    bc += facestride;
		  }
		
		bloknum++;
		if (bloknum >= symbmtx->bcblktab[facecblknum].lbloknum+1/*todo +1*/) break;
	      }
	      
	    fin:
	      bloknum--;
	    }
	  
	  /** Mettre Ã  jour la hauteur des bloc extra diagonaux qui restent **/
	  rdim -= symbmtx->bloktab[p].lrownum - symbmtx->bloktab[p].frownum+1;
	}
      
      /** On recopie F dans les bloc extradiagonaux **/
      rdim = symbmtx->hdim[k] - cdim; /* hauteur des blocs extradiagonaux **/
      bloknum = symbmtx->bcblktab[k].fbloknum+1; /** Indice du premier bloc extra-diagonal **/
      for(m=0;m<cdim;m++)
	{
	  /** Pointeur vers la m-ieme colonne dans le bloc colonne **/
	  bc = solvmtx->coeftab + solvmtx->bloktab[bloknum].coefind + m*stride;
	  
	  /** Pointeur vers la m-ieme colonne dans le buffer F **/
	  fc = F + m*stride;
	  BLAS_COPY(rdim, fc, UN, bc, UN); /** On copie la colonne de F **/
	}

    }
}

#endif

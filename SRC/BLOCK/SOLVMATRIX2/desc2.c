/* @authors J. GAIDAMOUR */

#include <stdio.h>
#include <stdlib.h>
#include "phidal_common.h"
#include "solver.h"

#include "base.h"

#include <assert.h>
#include <string.h>

/* tmp */
/*#define MAX(X, Y) (X>Y?X:Y)*/

void VS2_LMATVEC(SolverMatrix* solvmtx, SymbolMatrix* symbmtx, COEF* b, COEF* x);

void VS_LMATVEC(VSolverMatrix* vsolvmtx, COEF* b, COEF* x) {
  VS2_LMATVEC(vsolvmtx->solvmtx, &vsolvmtx->symbmtx, b, x);
}

void VS2_LMATVEC(SolverMatrix* solvmtx, SymbolMatrix* symbmtx, COEF* b, COEF* x) {
  int k,p,p2;
  int /* j, */jxtmp;
  
  COEF* xptr;
  int height, width, stride;

  COEF* xtmp;
  int size;

  COEF minusone= -1.0, zero=0.0, one=1.0;
  char *transN = "N";
  int UN = 1;

/* #warning TODO */
  /*TOSDO !! !tmp*/
  for(k=0; k<symbmtx->cblknbr; k++) {
    symbmtx->nodenbr = MAX(symbmtx->nodenbr, symbmtx->hdim[k]);
  }

  /* printfv(5, "tli=%d\n",symbmtx->tli); */

  /* vecteur tmp */
  xtmp = (COEF*)malloc(sizeof(COEF)*symbmtx->nodenbr);

  /* parcours par blocs colonnes */
  for(k=0; k<symbmtx->cblknbr; k++) {
    width  = symbmtx->ccblktab[k].lcolnum - symbmtx->ccblktab[k].fcolnum +1;
    height = symbmtx->hdim[k];          /** Hauteur de la surface compactÃ©e des bloc extra-diagonaux **/
    if (height == 0) continue;
    stride = symbmtx->stride[k];
    p      = symbmtx->bcblktab[k].fbloknum;        /* triangular block */

    assert(p<symbmtx->bloknbr);
    
    xptr = x + symbmtx->ccblktab[k].fcolnum - symbmtx->tli;
    
    /* calcul des contributions (aggrégation des colonnes) */
    /* printfv(5, "%d symbmtx->hdim[k] %d stride\n", height, stride); */

    assert(symbmtx->ccblktab[k].fcolnum - symbmtx->tli + height < symbmtx->nodenbr); /* xptr */
    /* printfv(5, "%d %d\n", height, symbmtx->nodenbr); */


    assert(height <= symbmtx->nodenbr); /*xtmp*/


    BLAS_GEMV(transN, height, width, minusone /*coef alpha*/,
	      solvmtx->coeftab + solvmtx->bloktab[p].coefind, stride,
	      xptr, UN,       zero /*coef beta*/,
	      xtmp, UN);

    /* x := xtmp (out) */
    for(p2=p, jxtmp=0; p2<=symbmtx->bcblktab[k].lbloknum; p2++) {
      size = symbmtx->bloktab[p2].lrownum - symbmtx->bloktab[p2].frownum +1;
      BLAS_AXPY(size, one, xtmp + jxtmp, UN, x + symbmtx->bloktab[p2].frownum - symbmtx->tli, UN);
      jxtmp += size;
    }

  }

  free(xtmp);
}

void VS2_UMATVEC(SolverMatrix* solvmtx, SymbolMatrix* symbmtx, COEF* b, COEF* x);

void VS_UMATVEC(VSolverMatrix* vsolvmtx, COEF* b, COEF* x) {
  VS2_UMATVEC(vsolvmtx->solvmtx, &vsolvmtx->symbmtx, b, x);
}

void VS2_UMATVEC(SolverMatrix* solvmtx, SymbolMatrix* symbmtx, COEF* b, COEF* x) 
{
  int k,p,p2;
  int /* j, */jxtmp;
  
  COEF* xptr;
  int height, width, stride;

  COEF* xtmp;
  int size;

  COEF minusone= -1.0, one=1.0;
  char *transT = "T";
  int UN = 1;

  if (x!=b)
    BLAS_COPY(symbmtx->nodenbr, b, UN, x, UN);

  /* vecteur tmp */
  xtmp = (COEF*)malloc(sizeof(COEF)*symbmtx->nodenbr);

  /* parcours par blocs colonnes */
  for(k=symbmtx->cblknbr-1; k>=0; k--) {

    width  = symbmtx->ccblktab[k].lcolnum - symbmtx->ccblktab[k].fcolnum +1;
    height = symbmtx->hdim[k]; /** Hauteur de la surface compactÃ©e des bloc extra-diagonaux **/
    if (height == 0) continue;
    stride = symbmtx->stride[k];
    p    = symbmtx->bcblktab[k].fbloknum;        /* triangular block */
    xptr = x + symbmtx->ccblktab[k].fcolnum - symbmtx->tli;

    if(k != symbmtx->cblknbr - 1) {
      
      /* xtmp := x (in) */
      for(p2=p, jxtmp=0; p2<=symbmtx->bcblktab[k].lbloknum; p2++) {
      	size = symbmtx->bloktab[p2].lrownum - symbmtx->bloktab[p2].frownum +1;
      	BLAS_COPY(size, x + symbmtx->bloktab[p2].frownum - symbmtx->tli, UN, xtmp + jxtmp, UN);
      	jxtmp += size; /* todo : remplacer int jxtmp par int*, evite 1 operation  + x+ symbmtx aussi?*/
      }
      
      /* calcul des contributions (aggrégation des colonnes) */
      BLAS_GEMV(transT, height, width, minusone /*coef alpha*/, 
		solvmtx->coeftab + solvmtx->bloktab[p].coefind, stride, 
		xtmp, UN,       one /*coef beta*/,
		xptr, UN);
      
    }
  }

  free(xtmp);
}

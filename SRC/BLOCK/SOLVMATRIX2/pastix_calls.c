#ifdef WITH_PASTIX

#include "pastix_calls.h"


void print_PastixSolverMatrix(PastixSolverMatrix *s) {
  int k, bloknum;
  fprintf(stdout, "\n\nAFFICHAGE DE LA PastixSolverMatrix :\n\n");
  fprintf(stdout, "cblknbr = %d\n", s->symbmtx.cblknbr);
  fprintf(stdout, "nodenbr = %d\n", s->symbmtx.nodenbr);
  fprintf(stdout, "bloknbr = %d\n", s->symbmtx.bloknbr);
  fprintf(stdout, "coefnbr = %d\n", s->coefnbr);

  printf("\n Blocs-colonne :\n");
  for(k=0; k<s->symbmtx.cblknbr+1; k++) {
    fprintf(stdout, "cblk=%d fcolnum=%d lcolnum=%d bloknum=%d stride=%d\n", k, s->symbmtx.cblktab[k].fcolnum, s->symbmtx.cblktab[k].lcolnum, s->symbmtx.cblktab[k].bloknum, s->cblktab[k].stride);
  }

  printf("\n Blocs :\n");
  for(bloknum=0; bloknum<s->symbmtx.bloknbr; bloknum++) {
    fprintf(stdout, "blok=%d frownum=%d lrownum=%d cblk_en_face=%d coefind=%d\n", bloknum, s->symbmtx.bloktab[bloknum].frownum, s->symbmtx.bloktab[bloknum].lrownum, s->symbmtx.bloktab[bloknum].cblknum, s->bloktab[bloknum].coefind);
  }
  printf("\n");
}

void SolvMtx2PastixSolvMtx(SolverMatrix* solvmtx, SymbolMatrix* symbmtx, PastixSolverMatrix* sol) {

  int coefnbr, k, i, first_row, first_col, first_cblok, first_blok, bloknum, m, stride, first_blok_in_cblk_coefind;
  PASTIX_FLOAT *ptr;
  COEF *col;

  sol->symbmtx.baseval = 0;
  sol->symbmtx.cblknbr = symbmtx->cblknbr; 
  sol->symbmtx.bloknbr = symbmtx->bloknbr;
  sol->symbmtx.nodenbr = symbmtx->nodenbr; 
  
  coefnbr = 0;

  for(k = 0; k < symbmtx->cblknbr; k++)
    coefnbr += (symbmtx->ccblktab[k].lcolnum - symbmtx->ccblktab[k].fcolnum + 1) * symbmtx->hdim[k];    
 
  sol->coefnbr = coefnbr;

  sol->cblktab = (PastixSolverCblk*)malloc(symbmtx->cblknbr*sizeof(PastixSolverCblk));
  sol->bloktab = (PastixSolverBlok*)malloc(symbmtx->bloknbr*sizeof(PastixSolverBlok));
  sol->symbmtx.cblktab = (PastixSymbolCblk*)malloc((1+symbmtx->cblknbr)*sizeof(PastixSymbolCblk));
  sol->symbmtx.bloktab = (PastixSymbolBlok*)malloc(symbmtx->bloknbr*sizeof(PastixSymbolBlok));

  sol->coeftab = NULL;
  sol->ucoeftab = NULL;

  first_row = symbmtx->bloktab[symbmtx->bcblktab[0].fbloknum].frownum; 
  first_col = first_row;
  first_cblok = symbmtx->facedecal;
  first_blok = symbmtx->bcblktab[0].fbloknum;

  for(k = 0; k < symbmtx->cblknbr; k++) {
    sol->cblktab[k].stride = symbmtx->stride[k]; //stride or hdim ?
    sol->symbmtx.cblktab[k].fcolnum = symbmtx->ccblktab[k].fcolnum - first_col;
    sol->symbmtx.cblktab[k].lcolnum = symbmtx->ccblktab[k].lcolnum - first_col;
    sol->symbmtx.cblktab[k].bloknum = symbmtx->bcblktab[k].fbloknum - first_blok;
  }
  sol->symbmtx.cblktab[symbmtx->cblknbr].fcolnum = sol->symbmtx.cblktab[symbmtx->cblknbr-1].lcolnum + 1;
  sol->symbmtx.cblktab[symbmtx->cblknbr].lcolnum = sol->symbmtx.cblktab[symbmtx->cblknbr-1].lcolnum + 1;
  sol->symbmtx.cblktab[symbmtx->cblknbr].bloknum = symbmtx->bloknbr;

  k = 0;
  first_blok_in_cblk_coefind = solvmtx->bloktab[symbmtx->bcblktab[k].fbloknum].coefind;

  for(bloknum = first_blok; bloknum < first_blok + symbmtx->bloknbr; bloknum++) {
#ifdef PASTIX_NUMA_ALLOC
    sol->bloktab[bloknum-first_blok].coefind = solvmtx->bloktab[bloknum].coefind - first_blok_in_cblk_coefind;
#else
    sol->bloktab[bloknum-first_blok].coefind = solvmtx->bloktab[bloknum].coefind - solvmtx->bloktab[first_blok].coefind;
#endif
    sol->symbmtx.bloktab[bloknum-first_blok].frownum = symbmtx->bloktab[bloknum].frownum - first_row;
    sol->symbmtx.bloktab[bloknum-first_blok].lrownum = symbmtx->bloktab[bloknum].lrownum - first_row;
    sol->symbmtx.bloktab[bloknum-first_blok].cblknum = symbmtx->bloktab[bloknum].cblknum - first_cblok;

    sol->symbmtx.bloktab[bloknum-first_blok].levfval = 0; // pastix will not use it.
    
    if(bloknum == symbmtx->bcblktab[k].lbloknum) {
      k++;
      if(k != symbmtx->cblknbr)
	first_blok_in_cblk_coefind = solvmtx->bloktab[symbmtx->bcblktab[k].fbloknum].coefind;
    }
  }
 
}

void SolvMtx2CSC(SolverMatrix* solvmtx, SymbolMatrix* symbmtx, int *ncol, pastix_int_t **colptr, pastix_int_t **rows, pastix_float_t **values) {
  int k, m, bloknum, coefnbr, first_row, i_beg, dim_cblk;
  pastix_int_t i, cpt_col, cpt_val;
  COEF *ptr;
  blas_t stride;
 
  (*ncol) = symbmtx->nodenbr; 
  coefnbr = 0;
  for(k = 0; k < symbmtx->cblknbr; k++) {
    dim_cblk = symbmtx->ccblktab[k].lcolnum - symbmtx->ccblktab[k].fcolnum + 1;
    coefnbr += dim_cblk * symbmtx->hdim[k] - (dim_cblk*(dim_cblk-1))/2;  
  }

  (*colptr) = (pastix_int_t*)malloc(((*ncol)+1)*sizeof(pastix_int_t));
  (*rows)   = (pastix_int_t*)malloc(coefnbr*sizeof(pastix_int_t)); 
  (*values) = (pastix_float_t*)malloc(coefnbr*sizeof(pastix_float_t)); 

  cpt_col = 0;
  cpt_val = 0;
  
  first_row = symbmtx->bloktab[symbmtx->bcblktab[0].fbloknum].frownum;

  (*colptr)[cpt_col] = cpt_val+1;
  cpt_col++;

  for(k = 0; k < symbmtx->cblknbr; k++) 
    {
      stride = symbmtx->stride[k];

      for(m = 0; m < symbmtx->ccblktab[k].lcolnum-symbmtx->ccblktab[k].fcolnum+1; m++)
	{
	  for(bloknum = symbmtx->bcblktab[k].fbloknum; bloknum <= symbmtx->bcblktab[k].lbloknum; bloknum++) 
	    {
	      
	      ptr = solvmtx->coeftab + solvmtx->bloktab[bloknum].coefind + m*stride;
	      i_beg = (first_row + cpt_col-1 > symbmtx->bloktab[bloknum].frownum) ? first_row+cpt_col-1 : symbmtx->bloktab[bloknum].frownum;

	      for(i = i_beg; i <= symbmtx->bloktab[bloknum].lrownum; i++)
		{
		  (*rows)[cpt_val]   = i - first_row + 1; 
		  assert((i-first_row>=0) && (i-first_row<=(*ncol)));
		  (*values)[cpt_val] = ptr[i-symbmtx->bloktab[bloknum].frownum];
		  cpt_val++;
		}
	    }
	  (*colptr)[cpt_col] = cpt_val+1;
	  cpt_col++;
	}
    }
}

void SolvMtx2CSCu(SolverMatrix *solvL, SolverMatrix *solvU, SymbolMatrix* symbmtx, int *ncol, pastix_int_t **colptr, pastix_int_t **rows, pastix_float_t **values) {
  int col, k, coefnbr, rowspan, colspan, diag, first_row, line, offset, i, m, bloknum, stride;
  int *current_pos;
  COEF *ptrL, *ptrU;

  /*** Computation of coefnbr ***/
  coefnbr = 0;
  for(k = 0; k < symbmtx->cblknbr; k++) {
    colspan = symbmtx->ccblktab[k].lcolnum - symbmtx->ccblktab[k].fcolnum + 1;
    coefnbr += 2*colspan*symbmtx->hdim[k] - colspan*colspan;
  }

  /*** Initialization of colptr ***/
  (*ncol) = symbmtx->nodenbr;
  (*colptr) = (pastix_int_t*)malloc(((*ncol)+1)*sizeof(pastix_int_t));  
  for(col=0; col<(*ncol)+1; col++) (*colptr)[col] = 0;

  /*** Computation of colptr ***/
  first_row = symbmtx->tli;
  col = 0;
  /* After this loop, we will have colptr[i+1] = nb of elements in column i */
  for(k = 0; k < symbmtx->cblknbr; k++) 
    { 
      colspan = symbmtx->ccblktab[k].lcolnum - symbmtx->ccblktab[k].fcolnum + 1;
      
      for(bloknum = symbmtx->bcblktab[k].fbloknum; bloknum <= symbmtx->bcblktab[k].lbloknum; bloknum++) 
	{
	  rowspan = symbmtx->bloktab[bloknum].lrownum - symbmtx->bloktab[bloknum].frownum + 1;
	  
	  /* Lower part + diagonal blocks */
	  for(m=col; m<col+colspan; m++)
	    (*colptr)[m+1] += rowspan;
	  
	  /* Upper part */
	  if(bloknum != symbmtx->bcblktab[k].fbloknum)  // if bloknum is not a diagonal block
	    for(i=symbmtx->bloktab[bloknum].frownum; i <= symbmtx->bloktab[bloknum].lrownum; i++) 
	      (*colptr)[i-first_row+1] += colspan;  	  
	}
      col += colspan;	  
    } 
  /* After this loop, colptr[i] will contain its final value (0-based) */
  for(col=1; col<(*ncol)+1; col++) (*colptr)[col] += (*colptr)[col-1];

  /*** Initialization of rows and values ***/
  (*rows)   = (pastix_int_t*)malloc(coefnbr*sizeof(pastix_int_t)); 
  (*values) = (pastix_float_t*)malloc(coefnbr*sizeof(pastix_float_t));
  current_pos = (int*)malloc((*ncol)*sizeof(int));
  for(col=0; col<(*ncol); col++) current_pos[col] = (int)((*colptr)[col]);

  /*** Computation of rows and values ***/
  col = 0;
  for(k = 0; k < symbmtx->cblknbr; k++) 
    {
      stride = symbmtx->stride[k];

      for(m = 0; m < symbmtx->ccblktab[k].lcolnum-symbmtx->ccblktab[k].fcolnum+1; m++)
	{
	  for(bloknum = symbmtx->bcblktab[k].fbloknum; bloknum <= symbmtx->bcblktab[k].lbloknum; bloknum++) 
	    {
	      offset = 0;
	      diag = (bloknum == symbmtx->bcblktab[k].fbloknum);
	      ptrL = solvL->coeftab + solvL->bloktab[bloknum].coefind + m*stride;
	      ptrU = solvU->coeftab + solvU->bloktab[bloknum].coefind + m*stride;

	      for(i = symbmtx->bloktab[bloknum].frownum; i <= symbmtx->bloktab[bloknum].lrownum; i++)
		{
		  line = i-first_row;
		  
		  /* Lower part + diagonal blocks */
		  assert(current_pos[col]<(int)((*colptr)[col+1]));
		  (*rows)[current_pos[col]] = line;
		  (*values)[current_pos[col]] = ptrL[offset];
		  current_pos[col]++;
		  
		  /* Upper part */
		  if(!diag) {
		    assert(current_pos[line]<(int)((*colptr)[line+1]));
		    (*rows)[current_pos[line]] = col;
		    (*values)[current_pos[line]] = ptrU[offset]; 
		    current_pos[line]++;
		  }

		  offset++;
		}
	    }
	  assert(current_pos[col]==(int)((*colptr)[col+1]));
	  col++;
	}
    }

  /*** From 0-based to 1-based ***/
  for(col=0; col<(*ncol)+1; col++) (*colptr)[col]++;
  for(i=0; i<coefnbr; i++) (*rows)[i]++;
  
  free(current_pos);
}

void pastix_init(pastix_struct *p, SymbolMatrix *symbmtx) {
  PastixOrder *ordemesh;
  int i, first_col;
  p->pastix_data = NULL;

  /* Setting pastix default parameters in iparm and dparm */
  p->iparm[IPARM_MODIFY_PARAMETER] = API_NO;
  p->iparm[IPARM_START_TASK]       = API_TASK_INIT;

  pastix(NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, p->iparm, p->dparm);

  /* Allocation and initialization of pastix_data */
  p->iparm[IPARM_END_TASK]            = API_TASK_INIT;
  p->iparm[IPARM_MATRIX_VERIFICATION] = API_NO;
  p->iparm[IPARM_VERBOSE]             = API_VERBOSE_NOT;

  
  pastix((pastix_data_t**)(&(p->pastix_data)), 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, p->iparm, p->dparm);

  /* Initialisation of pastix_data->ordermesh */
  ordemesh = &(p->pastix_data->ordemesh);
  
  ordemesh->cblknbr = symbmtx->cblknbr;
  ordemesh->rangtab = malloc((symbmtx->cblknbr+1)*sizeof(PASTIX_INT));
  ordemesh->permtab = malloc(symbmtx->nodenbr*sizeof(PASTIX_INT));
  ordemesh->peritab = malloc(symbmtx->nodenbr*sizeof(PASTIX_INT));

  first_col = symbmtx->ccblktab[0].fcolnum;
  for(i=0; i<symbmtx->cblknbr; i++) 
    ordemesh->rangtab[i] = symbmtx->ccblktab[i].fcolnum - first_col;
  ordemesh->rangtab[symbmtx->cblknbr] = symbmtx->ccblktab[symbmtx->cblknbr-1].lcolnum + 1 - first_col;

  for(i=0; i<symbmtx->nodenbr; i++)  {
    ordemesh->permtab[i] = i;
    ordemesh->peritab[i] = i;
  }

  /* Setting the field "n" of pastix_data */
  p->pastix_data->n = symbmtx->nodenbr;
}

void pastix_free(pastix_struct *p) {
  p->iparm[IPARM_START_TASK]          = API_TASK_CLEAN;
  p->iparm[IPARM_END_TASK]            = API_TASK_CLEAN;

  pastix((pastix_data_t**)(&(p->pastix_data)), 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, p->iparm, p->dparm);
}

/* Pastix Factorization */
/* For the present only in the case of a symmetric matrix */
void pastix_factorization(pastix_struct *p, SolverMatrix *solvL, SolverMatrix *solvU, SymbolMatrix *symbmtx, int sym) {

  assert(sym == 1 || sym == 2);
  
  pastix_int_t ncol = 0;
  pastix_int_t *colptr = NULL;
  pastix_int_t *rows = NULL;
  pastix_float_t *values = NULL;
  PastixSolverMatrix *sol;  
  
  /* Conversion of matrix from (HIPS)SolverMatrix and (HIPS)SymbolMatrix format */
  /* to CSC format used by Pastix */
  if(sym == 2)
    SolvMtx2CSC(solvL, symbmtx, &ncol, &colptr, &rows, &values);
  else
    SolvMtx2CSCu(solvL, solvU, symbmtx, &ncol, &colptr, &rows, &values);

  /* Conversion of matrix from (HIPS)SolverMatrix and (HIPS)SymbolMatrix format */
  /* to (Pastix)SolverMatrix */
  sol = &(p->pastix_data->solvmatr);
  SolvMtx2PastixSolvMtx(solvL, symbmtx, sol);

  /* Call to Pastix */
  if(sym == 2) {
    p->iparm[IPARM_SYM]               = API_SYM_YES;
    p->iparm[IPARM_FACTORIZATION]     = API_FACT_LDLT;
  }
  else {
    p->iparm[IPARM_SYM]               = API_SYM_NO; 
    p->iparm[IPARM_FACTORIZATION]     = API_FACT_LU;
  }

  p->iparm[IPARM_MATRIX_VERIFICATION] = API_NO;
  p->iparm[IPARM_START_TASK]          = API_TASK_ANALYSE;
  p->iparm[IPARM_END_TASK]            = API_TASK_NUMFACT;
  p->iparm[IPARM_THREAD_NBR]          = 4;                // TODO

  fprintf(stdout,"Factorization with pastix ..."); fflush(stdout);
 
  pastix((pastix_data_t**)(&(p->pastix_data)), 0, ncol, colptr, rows, values, NULL, NULL, NULL, 1, p->iparm, p->dparm);

  fprintf(stdout," Done.\n"); fflush(stdout);

  /* Memory free*/
  free(colptr);
  free(rows);
  free(values);
}

void pastix_unscale_sym(pastix_struct *p, COEF *scaletab, COEF *iscaletab) {
  p->pastix_data->scaling   = API_YES;
  p->pastix_data->scalerowtab  = scaletab;
  p->pastix_data->iscalerowtab = iscaletab;
  pastix_unscale((pastix_data_t*)(p->pastix_data), API_YES);
}

void pastix_unscale_unsym(pastix_struct *p, COEF *scalerowtab, COEF *iscalerowtab, COEF *scalecoltab, COEF *iscalecoltab) {
  p->pastix_data->scaling   = API_YES;
  p->pastix_data->scalerowtab  = scalerowtab;
  p->pastix_data->iscalerowtab = iscalerowtab;
  p->pastix_data->scalecoltab  = scalecoltab;
  p->pastix_data->iscalecoltab = iscalecoltab;
  pastix_unscale((pastix_data_t*)(p->pastix_data), API_NO);
}

void pastix_solve(pastix_struct *p, COEF *b) {
  int ncol;
  ncol = p->pastix_data->n;
  p->iparm[IPARM_START_TASK] = API_TASK_SOLVE;
  p->iparm[IPARM_END_TASK]   = API_TASK_REFINE;
  p->iparm[IPARM_THREAD_NBR] = 4;                         // TODO

  fprintf(stdout,"Solve with pastix ..."); fflush(stdout);
  
  pastix((pastix_data_t**)(&(p->pastix_data)), 0, ncol, NULL, NULL, NULL, NULL, NULL, b, 1, p->iparm, p->dparm);

  fprintf(stdout," Done.\n"); fflush(stdout);
}

void pastix_TRSM(pastix_struct *p, flag_t unitdiag, SolverMatrix* M, SymbolMatrix* symbM) {}

#endif // WITH_PASTIX

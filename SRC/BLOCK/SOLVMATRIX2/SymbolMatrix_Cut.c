/* @authors J. GAIDAMOUR */

/*tmp*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h> /* memcpy */
#include <assert.h>

#include "block.h"

void SymbolMatrix_Cut(SymbolMatrix* symbA, SymbolMatrix* symbB,
		      dim_t tli, dim_t tlj, dim_t bri, dim_t brj) { 

  dim_t cblktlj, cblkbrj;
  int kA, kB, pA;

  /*** cblktlj and cblkbrj ***/
  kA=0;
  while(symbA->ccblktab[kA].fcolnum < tlj) {
    kA++;
    assert(kA<symbA->cblknbr); /*TODO*/
  }
  cblktlj = kA;

  while(symbA->ccblktab[kA].fcolnum <= brj) {
    kA++;
    if(kA>=symbA->cblknbr) { /*TODO : le kA++ apres*/
      goto end1;
    };
  }
 end1:
  cblkbrj = kA-1;

  /*** SymbolMatrix ***/

  symbB->cblknbr = symbA->cblknbr - (cblktlj + (symbA->cblknbr - cblkbrj - 1));
  symbB->nodenbr = symbA->ccblktab[cblkbrj].lcolnum - symbA->ccblktab[cblktlj].fcolnum +1;    

  /* blocknbr */
  symbB->bloknbr = 0;
  for(kA=cblktlj; kA<=cblkbrj; kA++) {
    pA=symbA->bcblktab[kA].fbloknum;
 
    if (pA> symbA->bcblktab[kA].lbloknum) goto end; 

    while(symbA->bloktab[pA].frownum < tli) {
      pA++;
      if (pA> symbA->bcblktab[kA].lbloknum) goto end;
    }

    if (pA> symbA->bcblktab[kA].lbloknum) goto end; /*TODO : simplifier*/

    while(symbA->bloktab[pA].frownum <= bri) {
      symbB->bloknbr++;
      pA++;
      if (pA> symbA->bcblktab[kA].lbloknum) goto end;
    }
     
  end:
    ;
  }

  symbB->virtual   = 1;

  symbB->bcblktab   = (SymbolBCblk*)malloc(sizeof(SymbolBCblk)*symbB->cblknbr);

  symbB->ccblktab   = symbA->ccblktab + cblktlj;

  symbB->hdim      = (int*)malloc(sizeof(int)*symbB->cblknbr);
  symbB->stride    = NULL;

  symbB->bloktab   = symbA->bloktab;

  symbB->facedecal = symbA->facedecal + cblktlj; 
  symbB->tli       = tli;
  symbB->cblktlj   = cblktlj;

  /*** Fill-in SymbolMatrix and SolverMatrix ***/
  for(kA=cblktlj, kB=0; kA<=cblkbrj; kA++, kB++) {
    symbB->hdim[kB] = 0;
    pA = symbA->bcblktab[kA].fbloknum;

    if (pA> symbA->bcblktab[kA].lbloknum) { 
      symbB->bcblktab[kB].fbloknum = pA;
      goto end2;
    }

    while(symbA->bloktab[pA].frownum < tli) {
      pA++;
      if (pA> symbA->bcblktab[kA].lbloknum) { 
	symbB->bcblktab[kB].fbloknum = pA;
	goto end2;
      }
    }

    if (pA> symbA->bcblktab[kA].lbloknum) { 
      symbB->bcblktab[kB].fbloknum = pA; /* pas de bloc, doit juste être > a lblocknum*/
      goto end2;
    }

    symbB->bcblktab[kB].fbloknum = pA;
    while(symbA->bloktab[pA].frownum <= bri) {
      symbB->hdim[kB] += symbA->bloktab[pA].lrownum - symbA->bloktab[pA].frownum +1;
      pA++;
      if (pA> symbA->bcblktab[kA].lbloknum) { goto end2; }
    }
    
  end2:
    symbB->bcblktab[kB].lbloknum = pA-1;

  }



}



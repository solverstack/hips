/* @authors J. GAIDAMOUR */

#ifdef OBSOLETE
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include "phidal_struct.h"

#include "solver.h"

#include "base.h"

void SolverMatrix_Div(SolverMatrix* A, SolverMatrix* D) {
  /************************************************************************************/
  /* This function performs A = A.D^1   (D diag)                                      */
  /************************************************************************************/

  SymbolMatrix *symbA = &(A->symbmtx);
  SymbolMatrix *symbD = &(D->symbmtx);

  dim_t k,m;
  int cdim, rdim;
  blas_t strideA, strideD;

  REAL *ac, *dc;

  REAL alpha;
  int UN = 1;

  /* Diviser A par la diagonale de D */
  for(k=0;k<symbA->cblknbr;k++)
    {
      cdim = symbA->ccblktab[k].lcolnum - symbA->ccblktab[k].fcolnum +1; /** Largeur du bloc colonne **/
      rdim = symbA->hdim[k]; /* the number of rows of the matrix A */

      strideA = symbA->stride[k];
      strideD = symbD->stride[k];

      ac = A->coeftab + A->bloktab[ symbA->bcblktab[k].fbloknum ].coefind;
      dc = D->coeftab + D->bloktab[ symbD->bcblktab[k].fbloknum ].coefind;

      for(m=0;m<cdim;m++) {
	alpha = 1.0/ dc[m*strideD + m]; /** alpha est l'inverse du m-ieme terme diagonal du bloc diagonal de D **/
	DSCAL(rdim, alpha, ac, UN); /** On divise la m-ieme colonne par le terme diagonal **/
	ac += strideA;
      }
    }
}


/*TODO : logique dans += ou calcul a chaque fois*/
/*TODO : changer SOPLAIN*/
#endif

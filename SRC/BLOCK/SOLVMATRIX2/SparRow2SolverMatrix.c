/* @authors J. GAIDAMOUR */

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>

#include "block.h"

/* TODO : tlj non utilisé, tli peut etre obtenu autrement */

#ifdef DEBUG_M 
#define FILLCOUNT
#endif

#ifdef FILLCOUNT
int countwarning = 0;
#endif

void SparRow2SolverMatrix(dim_t tli, dim_t tlj, csptr mat, SymbolMatrix* symbmtx, SolverMatrix* solvmtx) {
  int k,p/* , first */;
  int i,j,i2;

  int jx;
  COEF* ind;

#ifdef FILLCOUNT
  int count;
#endif

#ifdef DEBUG_NOALLOCATION
  return;
#endif

#ifdef DEBUG_ORDER
  ascend_column_reorder(mat);
#endif 

 if (symbmtx->bloknbr == 0) return;
 
 i = 0;
 
 ascend_column_reorder(mat);

/*  assert(tli == symbmtx->cblktab[0].fcolnum); /\* TODO : nom i et j !!! *\/ */
 

  /* Parcours des blocks colonnes */
  for(k=0; k<symbmtx->cblknbr; k++) {
    if (symbmtx->bcblktab[k].lbloknum - symbmtx->bcblktab[k].fbloknum +1 <= 0) { /* todo : utile ?*/
      i+= symbmtx->ccblktab[k].lcolnum - symbmtx->ccblktab[k].fcolnum +1;
      continue;
    }

    p=symbmtx->bcblktab[k].fbloknum;
    /* printfd("k=%d coeftab : %p coefind : %d\n",k, solvmtx->bloktab[p].coeftab, solvmtx->bloktab[p].coefind); */
    ind = solvmtx->coeftab + solvmtx->bloktab[p].coefind;
    /* printfd("coeftab %p coefind %d\n", solvmtx->coeftab, solvmtx->bloktab[p].coefind); */

    /* Parcours des colonnes du bloc colonnes */
    for(i2 = symbmtx->ccblktab[k].fcolnum; i2<=symbmtx->ccblktab[k].lcolnum; i++,i2++) { /* todo : i2 sert presque à rien */
      assert(i < mat->n);

      /* Copie des termes de 'a' dans 'solvmtx' pour la colonne i */
      if(mat->ja[i] == NULL) { 
	ind += symbmtx->stride[k];  
	/* printfd("stride %d\n", symbmtx->stride[k]); */
	continue;
      }

#ifdef FILLCOUNT
      count = 0;
#endif
      jx=0;
      
      /* pour le non sym */
      while(jx < mat->nnzrow[i]) {
	if(mat->ja[i][jx] < (symbmtx->bloktab[symbmtx->bcblktab[k].fbloknum].frownum - symbmtx->tli)) { 
	  /* TODO : simplifier */
	  jx++;
#ifdef FILLCOUNT
	  count++;
#endif
	} else break;
      }

      for(p=symbmtx->bcblktab[k].fbloknum; p<=symbmtx->bcblktab[k].lbloknum; p++) {
	/* Dprintfd("p=%d, symbmtx->bloktab[p].frownum = %d, symbmtx->bloktab[p].lrownum = %d\n",p, */
	/* 		symbmtx->bloktab[p].frownum,symbmtx->bloktab[p].lrownum); */
	for(j=symbmtx->bloktab[p].frownum - symbmtx->tli; j<=symbmtx->bloktab[p].lrownum - symbmtx->tli; j++) {
	  if (jx < mat->nnzrow[i])
	    if (mat->ja[i][jx] == j) {
	      /* 	      Dprintfd("jx=%d ja=%d value=%f\n", jx, mat->ja[i][jx], mat->ma[i][jx]); */
	      *ind = mat->ma[i][jx];
	      jx++; 
#ifdef FILLCOUNT	      
	      count++;
#endif
	    } else { *ind = 0; }
	  else { *ind = 0; }  /* todo : simplifier TODO : skip plus efficace. Trier ou non ? */
	  
	  ind++;
	}
	
/* 	printfd("%d %d\n", tlj, symbmtx->tli); */
/* 	assert(tlj == symbmtx->tli); */
	
      }

      /* printfd("symbmtx->stride[k] %d - symbmtx->hdim[k] %d\n", symbmtx->stride[k], symbmtx->hdim[k]); */

      assert(symbmtx->stride[k] >= symbmtx->hdim[k]);
      ind += symbmtx->stride[k] - symbmtx->hdim[k]; /*TODO : utiliser un kglobal*/
      
#ifdef FILLCOUNT
      if(count != mat->nnzrow[i]) {
	printfd("mat= %p\n", mat);
	printfd("k=%d\n", k);
	printfd("count=%d mat->nnzrow=%d\n", count, mat->nnzrow[i]);
	for(p=symbmtx->bcblktab[k].fbloknum; p<=symbmtx->bcblktab[k].lbloknum; p++) {
	  printfd("p=%d, [%d %d] [%d %d]\n",p,
		 symbmtx->bloktab[p].frownum,symbmtx->bloktab[p].lrownum,
		 symbmtx->bloktab[p].frownum - symbmtx->tli,symbmtx->bloktab[p].lrownum - symbmtx->tli);
	}
	for(jx=0; jx<mat->nnzrow[i]; jx++) {
	  printfd("jx=%d ja=%d value="_coef_"\n", jx, mat->ja[i][jx], pcoef(mat->ma[i][jx]));
	}
	
	assert(0);
      }
#endif
      
    }
    
  }
  
#ifdef FILLCOUNT
  if (countwarning == 0) {
    printfd("SparRow2SolverMatrix.c Warning : #define FILLCOUNT\n");
    countwarning=1;
  }
#endif
}

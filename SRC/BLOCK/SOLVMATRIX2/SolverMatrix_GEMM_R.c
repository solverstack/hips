/* @authors J. GAIDAMOUR */

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include "block.h"

#include "base.h"

#define MAX(x,y) (((x)<(y))?(y):(x))
#define MIN(x,y) (((x)>(y))?(y):(x))

/* TODO : modif a reporter sur la version SolverMatrix1 */

void SolverMatrix_GEMM_R(SolverMatrix* C, COEF alpha, SolverMatrix* A, SolverMatrix* B, SolverMatrix* D) {

  /************************************************************************************/
  /* This function performs C = C + alpha.A.Bt.D                                      */
  /************************************************************************************/

  SymbolMatrix *symbA = &(A->symbmtx);
  SymbolMatrix *symbB = &(B->symbmtx);
  SymbolMatrix *symbC = &(C->symbmtx);
  SymbolMatrix *symbD = &(D->symbmtx);

  dim_t k,p,q,m;
  int cdim, hdim, rdim, mdim;
  blas_t strideA, strideB, strideD;
  int bloknum;
  int decalcol, decalrow;
  int facecblknum, facestride;

  COEF *ac, *bc, *cc, *wc, *fc, *dc;
  COEF *F, *W;

  COEF alphadiag;

  /*COEF alpha, beta*/
  char *opA = "N"; /** Ne pas transposer A **/
  char *opB = "T"; /** Transpose B **/
  int UN = 1;
  COEF zero=0.0, one=1.0;
  
  /* Allocation des vecteurs temporaires */
  F = (COEF *)malloc(sizeof(COEF)*B->coefmax); 
  assert(F != NULL);

  W = (COEF *)malloc(sizeof(COEF)*A->coefmax);
  assert(W != NULL);

  for(k=0;k<symbA->cblknbr;k++)
    {
      cdim = symbA->ccblktab[k].lcolnum - symbA->ccblktab[k].fcolnum +1; /** Largeur du bloc colonne **/
      strideA = symbA->stride[k];
      strideB = symbB->stride[k];
      strideD = symbD->stride[k];
      if (strideA == 0) continue; /* phidal 100 ../MATRICES/BCSSTK15 */

      rdim = symbA->hdim[k]; /* the number of rows of the matrix A     */
      hdim = symbB->hdim[k]; /* the number of columns of the matrix Bt */

      bc = B->coeftab + B->bloktab[ symbB->bcblktab[k].fbloknum ].coefind;
      dc = D->coeftab + D->bloktab[ symbD->bcblktab[k].fbloknum ].coefind;

      /* "Diviser" les blocs de B par le bloc diagonal de D */
      fc = F; /** Pointeur vers la m-ieme colonne dans le buffer F **/
      for(m=0;m<cdim;m++) {
	alphadiag = 1.0 / dc[m*strideD + m]; /** alpha est l'inverse du m-ieme terme diagonal du bloc diagonal de D **/
	
	BLAS_COPY(hdim, bc, UN, fc, UN); /** On copie les bloc extra diagonaux dans F **/ /* hdim = the number of rows of B ! */
	BLAS_SCAL(hdim, alphadiag,  fc, UN); /** On divise la m-ieme colonne par le terme diagonal **/
	
	bc += strideB;
	fc += strideB;
      }


      for(p=symbB->bcblktab[k].fbloknum; p<=symbB->bcblktab[k].lbloknum; p++) {

	/** Calcul du pointeur dans coeftab vers le dÃ©but des coefficients du bloc extra-diagonal p **/ 
	ac = A->coeftab + A->bloktab[p].coefind;
	fc = F + B->bloktab[p].coefind - B->bloktab[ symbB->bcblktab[k].fbloknum ].coefind;

	/** Calcul de la hauteur du bloc extra diagonal p **/	
	hdim = symbB->bloktab[p].lrownum - symbB->bloktab[p].frownum+1;

	BLAS_GEMM(opA, opB, rdim, hdim, cdim, one, ac, strideA, fc, strideB, zero, W, strideA);



	facecblknum = symbB->bloktab[p].cblknum - symbC->facedecal;      /** Indice du bloc colonne en face du bloc extra-diagonal p **/ /*todo : faire pareil pour les autres*/
	facestride  = symbC->stride[facecblknum]; /** Stride du cblk en face du bloc extra-diagonal p **/

	decalcol = symbB->bloktab[p].frownum - symbC->ccblktab[facecblknum].fcolnum; /** Nombre de colonne "Ã  gauche" de la zone 
											    modifiÃ©e dans le bloc colonne facecblknum **/
	mdim = symbB->bloktab[p].lrownum - symbB->bloktab[p].frownum+1; /*** Largueur de la zone modifiÃ©e == hauteur du bloc p dans le bloc colonne k **/
	
	bloknum = symbC->bcblktab[facecblknum].fbloknum; /** Indice du 1er bloc de la colonne dans C **/



	for(q=p/*symbA->ccblktab[k].fbloknum TODO : A==B*/;q<=symbA->bcblktab[k].lbloknum;q++) /** Pour tous les bloc de A dans cblk k **/
	  {
	    
	    /* skip block that cannot match */

	    if (bloknum >= symbC->bcblktab[facecblknum].lbloknum+1) goto fin; /*1TODO*/

	    while(symbA->bloktab[q].frownum > symbC->bloktab[bloknum].lrownum) {
	      bloknum++;
	      if (bloknum >= symbC->bcblktab[facecblknum].lbloknum+1) goto fin; /*1TODO*/
	    }
	      
	    /* for every block that match */
	    while(symbA->bloktab[q].lrownum >= symbC->bloktab[bloknum].frownum) {

	      decalrow = symbA->bloktab[q].frownum - symbC->bloktab[bloknum].frownum;

	      /** Calcul du pointeur de debut de la zone modifiÃ©e dans le bloc A(bloknum, facebloknum) **/
	      cc = C->coeftab + C->bloktab[bloknum].coefind + decalcol*facestride + MAX(0,decalrow);
	      
	      /** Calcul du pointeur de debut du bloc correspondant Ã  A(q,k) dans W **/
	      wc = W + A->bloktab[q].coefind - A->bloktab[p].coefind + MAX(0,-decalrow);

	      /*** Update de la contribution ***/
	      hdim = MIN(symbA->bloktab[q].lrownum, symbC->bloktab[bloknum].lrownum) - 
		MAX(symbA->bloktab[q].frownum, symbC->bloktab[bloknum].frownum) +1; /** Hauteur du bloc sur lequel on effectue la différence **/

	      for(m=0; m < mdim; m++)
		{
		  BLAS_AXPY(hdim, alpha, wc, UN, cc, UN);

		  wc += strideA;
		  cc += facestride;
		}

	      bloknum++;
	      if (bloknum >= symbC->bcblktab[facecblknum].lbloknum+1) break; /*+1todo*/
	    }
	      
	  fin:
	    bloknum--;
	    ;
	  }

	/** Mettre Ã  jour la hauteur des bloc extra diagonaux qui restent **/
	rdim -= symbB->bloktab[p].lrownum - symbB->bloktab[p].frownum+1; /* todo A==B */
      }
      
    }
  
  free(F);
  free(W);

}

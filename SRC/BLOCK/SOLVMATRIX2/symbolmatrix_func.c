/* @authors J. GAIDAMOUR */

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "phidal_common.h"
#include "symbol.h"

REAL recursive_sum2(int a, int b, REAL (*fval)(int, const SymbolMatrix *), const SymbolMatrix * symbmtx);
/* REAL crout_hyb(int cblknum, const SymbolMatrix * symbmtx); */
REAL nnz2(int cblknum, const SymbolMatrix * symbmtx);
REAL nnz2N(int cblknum, const SymbolMatrix * symbmtx);

REAL SymbolMatrix_NNZ(SymbolMatrix *symbmtx)
{
  return recursive_sum2(0, symbmtx->cblknbr-1, nnz2, symbmtx);
}

/*TODO*/
REAL SymbolMatrixN_NNZ(SymbolMatrix *symbmtx)
{
  return recursive_sum2(0, symbmtx->cblknbr-1, nnz2N, symbmtx);
}

REAL recursive_sum2(int a, int b, REAL (*fval)(int, const SymbolMatrix *), const SymbolMatrix * symbmtx)
{
  if(a != b)
    return recursive_sum2(a, (a+b)/2, fval, symbmtx) 
         + recursive_sum2((a+b)/2+1, b, fval, symbmtx);
  
  return fval(a, symbmtx);
} 

#ifdef TODO 
/* adapter pour SOLVMTX2*/
REAL crout_hyb(int cblknum, const SymbolMatrix * symbmtx)
{ dim_t i;
  REAL gk = 0;
  REAL lk = 0;

  /* lk is the dimension of the diagonal blok */
  lk =(REAL)( symbmtx->ccblktab[cblknum].lcolnum - symbmtx->ccblktab[cblknum].fcolnum + 1);
      
  /* gk is the height of off-diag bloks */
  for(i=symbmtx->bcblktab[cblknum].bloknum+1;i<symbmtx->bcblktab[cblknum+1].bloknum;i++)
    gk += (REAL)(symbmtx->bloktab[i].lrownum - symbmtx->bloktab[i].frownum +1);

  return( (lk*lk*lk + 3*(gk+1)*lk*lk + (3*gk*gk -4 + 6*gk)*lk)/3 );

}
#endif

REAL nnz2(int cblknum, const SymbolMatrix * symbmtx)
{ dim_t i;
  REAL gk = 0;
  REAL lk = 0;

  /* lk is the dimension of the diagonal blok */
  lk = (REAL)(symbmtx->ccblktab[cblknum].lcolnum - symbmtx->ccblktab[cblknum].fcolnum + 1);
      
  /* gk is the height of off-diag bloks */
  for(i=symbmtx->bcblktab[cblknum].fbloknum+1;i<=symbmtx->bcblktab[cblknum].lbloknum;i++)
    gk +=(REAL)( symbmtx->bloktab[i].lrownum - symbmtx->bloktab[i].frownum +1);



  return( lk*(lk+1)/2 + gk*lk - lk);
}

/* TODO*/
REAL nnz2N(int cblknum, const SymbolMatrix * symbmtx)
{ dim_t i;
  REAL gk = 0;
  REAL lk = 0;

  /* lk is the dimension of the cblk */
  lk = (REAL)(symbmtx->ccblktab[cblknum].lcolnum - symbmtx->ccblktab[cblknum].fcolnum + 1);
      
  /* gk is the height of bloks */
  for(i=symbmtx->bcblktab[cblknum].fbloknum;i<=symbmtx->bcblktab[cblknum].lbloknum;i++)
    gk +=(REAL)( symbmtx->bloktab[i].lrownum - symbmtx->bloktab[i].frownum +1);

  return( gk*lk );
}

void SymbolMatrix_Expand(SymbolMatrix* symbol, dim_t dof) {
  dim_t k,p;

  if (dof == 1) return;

  assert(symbol != NULL);

  symbol->nodenbr = symbol->nodenbr*dof;

  /* **************************** */

  /* inutile */
  assert(symbol->tli == 0);
  assert(symbol->facedecal == 0);
  assert(symbol->cblktlj == 0);
  assert(symbol->virtual == 0);
 
  /* **************************** */

  /* parcours bloc colonne */ 
  for(k=0; k<symbol->cblknbr; k++) {
    symbol->ccblktab[k].fcolnum *= dof;
    symbol->ccblktab[k].lcolnum = (symbol->ccblktab[k].lcolnum+1)*dof-1 ;


    if (symbol->stride != NULL) 
      symbol->stride[k] *= dof;

    if (symbol->hdim != NULL) 
      symbol->hdim[k] *= dof;

    /* parcours blocs */ 
    for(p=symbol->bcblktab[k].fbloknum; p<=symbol->bcblktab[k].lbloknum; p++) {
      symbol->bloktab[p].frownum *= dof;
      symbol->bloktab[p].lrownum =  (symbol->bloktab[p].lrownum+1)*dof - 1;
    }
  }

  /* ** */
    
}


/**/
/* pas sur place */
/* todo : + rapide de faire sur place ou la copie en meme tmp ? */
void SymbolMatrix_Expand2(SymbolMatrix* src, SymbolMatrix* exp, dim_t dof) {
  dim_t k,p;

  assert(src != NULL);
  assert(exp != NULL);

  /* **************************** */

  exp->cblknbr = src->cblknbr;
  exp->bloknbr = src->bloknbr;
  exp->nodenbr = src->nodenbr*dof;

  /* **************************** */

  /* inutile */
  assert(src->tli == 0);
  assert(src->facedecal == 0);
  assert(src->cblktlj == 0);
  assert(src->virtual == 0);
  exp->tli = src->tli;
  exp->facedecal = src->facedecal;
  exp->cblktlj = src->cblktlj;
  exp->virtual = src->virtual;

  /* **************************** */

  if (exp != src) {
    exp->ccblktab = (SymbolCCblk*)malloc(exp->cblknbr*sizeof(SymbolCCblk));
    exp->bcblktab = (SymbolBCblk*)malloc(exp->cblknbr*sizeof(SymbolBCblk));
    
    if (src->stride != NULL) 
      exp->stride   = (int*)malloc(exp->cblknbr*sizeof(int));
    else exp->stride = NULL;
    if (src->hdim != NULL) 
      exp->hdim     = (int*)malloc(exp->cblknbr*sizeof(int));
    else exp->hdim = NULL;
  }

  /* **************************** */

  /* parcours bloc colonne */ 
  for(k=0; k<exp->cblknbr; k++) {
    exp->ccblktab[k].fcolnum = src->ccblktab[k].fcolnum*dof;
    exp->ccblktab[k].lcolnum = (src->ccblktab[k].lcolnum+1)*dof - 1;
   

    exp->bcblktab[k].fbloknum = src->bcblktab[k].fbloknum; /* todo : memcpy */   
    exp->bcblktab[k].lbloknum = src->bcblktab[k].lbloknum; /* todo : memcpy */
    
    if (exp->stride != NULL) 
      exp->stride[k] = src->stride[k]*dof;

    if (exp->hdim != NULL) 
      exp->hdim[k] = src->hdim[k]*dof;

    /* parcours blocs */ 
    for(p=exp->bcblktab[k].fbloknum; p<=exp->bcblktab[k].lbloknum; p++) {
      exp->bloktab[p].frownum = src->bloktab[p].frownum*dof;
      /*exp->bloktab[p].lrownum = src->bloktab[p].lrownum*dof;*/
      exp->bloktab[p].lrownum = (src->bloktab[p].lrownum+1)*dof - 1;
      exp->bloktab[p].cblknum = src->bloktab[p].cblknum;
    }
  }

  /* ** */
    
}

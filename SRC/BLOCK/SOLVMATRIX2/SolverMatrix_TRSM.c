/* @authors J. GAIDAMOUR */

/* #ifdef OBSOLETE */

#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#include <assert.h>
#include "block.h"

#include "base.h"

/*TODO : Virer les vecteurs temporaires qui ne servent pas.*/

/* TODO : a adapter au non direct */

/*SolverMatrix -> SolverMatrix*/
/*Symbol*/
/*coefind*/
/*.blocknum*/

#define MAX(x,y) (((x)<(y))?(y):(x))
#define MIN(x,y) (((x)>(y))?(y):(x))

void printLD(SolverMatrix* LD, char* filename);

/* TODO ! notation fc, opF ...*/

void VS2_InvLT(flag_t unitdiag, SolverMatrix* L, SymbolMatrix* symbL, SolverMatrix* M, SymbolMatrix* symbM, COEF* W);

void VS_InvLT(flag_t unitdiag, VSolverMatrix* L, VSolverMatrix* M, COEF* W) {

/*   /\*** Allocation de buffers de travail ***\/ */
/*   W = (COEF *)malloc(sizeof(COEF)*M->coefmax); */
/*   assert(W != NULL); */

  VS2_InvLT(unitdiag, L->solvmtx, &L->symbmtx, M->solvmtx, &M->symbmtx, W);

/*   /\** LibÃ©ration des buffers **\/ */
/*   free(W); */
}

void SolverMatrix_TRSM(flag_t unitdiag, SolverMatrix* L, SolverMatrix* M)
{

  COEF* W;

  /*** Allocation de buffers de travail ***/
  W = (COEF *)malloc(sizeof(COEF)*M->coefmax);
  assert(W != NULL);

  VS2_InvLT(unitdiag, L, &L->symbmtx, M, &M->symbmtx, W);

  /** LibÃ©ration des buffers **/
  free(W);
}

void VS2_InvLT(flag_t unitdiag, SolverMatrix* L, SymbolMatrix* symbL, SolverMatrix* M, SymbolMatrix* symbM, COEF* W) {
	
  /*********************************************************************/
  /* this function does M = L^-1.M                                     */
  /* L is a lower triangular SolverMatrix                              */
  /* M is a SolverMatrix                                               */ 
  /* The cblkblock pattern of M must be the same as L                  */
  /*********************************************************************/

  assert(W != NULL);

/*   SymbolMatrix *symbL; */
/*   SymbolMatrix *symbM; */

  dim_t p, q, k, m;
  int UN=1;
  COEF alpha, beta;
  blas_t strideL;
  blas_t strideM;
  int cdim, rdim, hdim, mdim;
  COEF *cc; 
  COEF *bc; 
  COEF *fc;
  COEF *wc; 
  int facestride;
  int bloknum, facecblknum;
  int bloknumM;
  int decalcol, decalrow;

/*   symbL = &(L->symbmtx); */
/*   symbM = &(M->symbmtx); */

  for(k=0;k<symbL->cblknbr;k++)
    {
      cdim   = symbM->ccblktab[k].lcolnum - symbM->ccblktab[k].fcolnum + 1; /** Largeur du bloc colonne **/
      strideL = symbL->stride[k];
      strideM = symbM->stride[k];
 
      /* if(strideM == 0) { /\* printfv(5, "TRSM CONTINUE\n"); *\/ continue; } */
      /*      if (p > symbL->bcblktab[k].lbloknum) continue; */

      /** Calcul du pointeur dans coeftab de L vers le dÃ©but des coefficients du bloc diagonal **/ 
      p = symbL->bcblktab[k].fbloknum;
      cc = L->coeftab + L->bloktab[p].coefind;
      
      /**********************************************************************/
      /* "Diviser" les blocs de M par le bloc diagonal de L                 */
      /**********************************************************************/
      {
	/** On effectue l'operation : M = M.(L^-1)t  avec M = l'ensemble compacte des blocs extra-diagonaux du bloc colonne de M **/
	char *side = "R";
	char *uplo = "L";
	char *trans = "T";
	char *diag; if(unitdiag==1) diag = "U"; else diag = "N";
	
	alpha = 1.0;

	rdim = symbM->hdim[k]; /** Hauteur de la surface compactÃ©e des blocs **/
	if (rdim == 0) continue;
	
	/** Calcul du pointeur dans coeftab vers le dÃ©but des coefficients de M */
	bloknumM = symbM->bcblktab[k].fbloknum; /** Indice du premier bloc (triangulaire) dans M **/ /*!pas M,*/
	bc = M->coeftab + M->bloktab[bloknumM].coefind;
/* 	printfv(5, "rdim %d cdim %d strideL %d M %d\n", rdim, cdim, strideL, strideM); */
	BLAS_TRSM(side, uplo, trans, diag, rdim, cdim, alpha, cc, strideL, bc, strideM);
	
	/* inutile*/
	if(k == symbL->cblknbr -1) /*DOTO : ne change rien,*/
	  continue;  /** Pas de bloc extradiagonal le calcul est terminer **/
      }

      /** Boucle sur les blocs de M **/
      rdim = symbM->hdim[k];  /** rdim est la hauteur des bloc >= p dans le bloc colonne k **/
                                      /** On l'initialise Ã  la heuteur totale **/
      /*TODO ! RDIM 2 fois, ici a virer*/

/*       continue; */

      for(p=symbL->bcblktab[k].fbloknum+1;p<=symbL->bcblktab[k].lbloknum;p++)
	{

	  /*** Stocker dans W le produit des blocs {A(q,k), q>=p} avec Ft(p,k) ***/
	  char *opA = "N"; /** Ne pas transposer A(**) **/
	  char *opF = "T"; /** Transpose F **/

	  /** Calcul du pointeur dans coeftab vers le dÃ©but des coefficients du bloc k **/ 
	  /*TODO : a mettre AVANT le for */
	  bc = M->coeftab + M->bloktab[ symbM->bcblktab[k].fbloknum ].coefind; 
	  /*todo, simplifer avec variable intermediaire*/


	  /** Calcul du pointeur dans F vers le debut des coefficients du bloc extra-diagonal p correspondant **/
	  fc = L->coeftab + L->bloktab[p].coefind;

	  /** Calcul de la hauteur du bloc p **/
	  hdim = symbL->bloktab[p].lrownum - symbL->bloktab[p].frownum +1;
	  alpha = 1.0;
	  beta = 0.0;

	  assert(strideM*hdim <= M->coefmax);
	  BLAS_GEMM(opA, opF, rdim, hdim, cdim, alpha, bc, strideM, fc, strideL, beta, W, strideM); /*TODO strideM*/

	  /****************************************************************************************/
	  /** Pour tout les blocs extra-diagonaux {A(j,k), j>= i} faire A(i,j) = A(i,j) - W(i, ) **/
	  /****************************************************************************************/
	  facecblknum = symbL->bloktab[p].cblknum - symbM->facedecal; /** Indice du bloc colonne en face du bloc extra-diagonal p **/

	  /** Stride du cblk en face du bloc p **/
	  facestride = symbM->stride[facecblknum];

	  /** Nombre de colonne "Ã  gauche" de la zone modifiÃ©e dans le bloc colonne facecblknum **/
	  decalcol = symbL->bloktab[p].frownum - symbL->ccblktab[facecblknum].fcolnum;

	  /*** Largueur de la zone modifiÃ©e == hauteur du bloc p dans le bloc colonne k **/
	  mdim = symbL->bloktab[p].lrownum - symbL->bloktab[p].frownum+1;

	  bloknum = symbM->bcblktab[facecblknum].fbloknum; /** Indice du 1er bloc colonne ds facebloknum **/

	  for(q=symbM->bcblktab[k].fbloknum;q<=symbM->bcblktab[k].lbloknum;q++) /** Pour tous les bloc de M **/
	    {

	      /* bloknum = symbM->bcblktab[facecblknum].fbloknum; */ /** Indice du bloc diagonal du bloc colonne facebloknum **/ /*TODODODO*/
	      
	      /* skip block that cannot match */
	      if (bloknum >= symbM->bcblktab[facecblknum].lbloknum+1/*todo le +1*/) goto fin;
	      while(symbM->bloktab[q].frownum > symbM->bloktab[bloknum].lrownum) {
		bloknum++;
		if (bloknum >= symbM->bcblktab[facecblknum].lbloknum+1/*todo le +1*/) goto fin;
	      }
	      
	      /* for every block that match */
	      while(symbM->bloktab[q].lrownum >= symbM->bloktab[bloknum].frownum) {

		/* assert ds le cas direct*/
		/*		assert(symbM->bloktab[bloknum].frownum <= symbM->bloktab[q].frownum);*/
		/*		assert(symbM->bloktab[bloknum].lrownum >= symbM->bloktab[q].lrownum);*/
		
		decalrow = symbM->bloktab[q].frownum - symbM->bloktab[bloknum].frownum;
		
		/** Calcul du pointeur de debut de la zone modifiÃ©e dans le bloc A(bloknum, facebloknum) **/
		bc = M->coeftab + M->bloktab[bloknum].coefind + decalcol*facestride + MAX(0,decalrow);

		/** Calcul du pointeur de debut du bloc correspondant Ã  A(q,k) dans W **/
		wc = W + M->bloktab[q].coefind - M->bloktab[symbM->bcblktab[k].fbloknum].coefind + MAX(0,-decalrow);
		
		/*** Update de la contribution ***/
		hdim = MIN(symbM->bloktab[q].lrownum, symbM->bloktab[bloknum].lrownum) - 
		  MAX(symbM->bloktab[q].frownum, symbM->bloktab[bloknum].frownum) +1; /** Hauteur du bloc sur lequel on effectue la différence **/

		/*assert cas direct*/
		/*assert(hdim == symbM->bloktab[q].lrownum -  symbM->bloktab[q].frownum +1);*/

		/*** Update de la contribution ***/
		alpha = -1.0;
		for(m=0; m < mdim; m++)
		  {
		    BLAS_AXPY(hdim, alpha, wc, UN, bc, UN);
		    wc += strideM;
		    bc += facestride;
		  }
		
		bloknum++;
		if (bloknum >= symbM->bcblktab[facecblknum].lbloknum+1/*todo +1*/) break;
	      }
	      
	    fin:
	      bloknum--;
	      ;
	    }
	}
      
    }
  
}



#ifdef TEST
#include <assert.h>
/* #undef Dprintf */
/* #define Dprintfv(5, args...) printf(args) */



SolverMatrix* createMatrixL() {
  /* attention, facing non renseigné*/
  SolverMatrix* solvmtx = (SolverMatrix*)malloc(sizeof(SolverMatrix)); 
  
  solvmtx->symbmtx.baseval = 0;
  solvmtx->symbmtx.cblknbr = 3;
  solvmtx->symbmtx.bloknbr = 6;
  solvmtx->symbmtx.nodenbr = 7;

  solvmtx->symbmtx.cblktab = (SymbolCblk*)malloc(sizeof(SymbolCblk)*  (solvmtx->symbmtx.cblknbr +1));
  solvmtx->symbmtx.bloktab = (SymbolBlok*)malloc(sizeof(SymbolBlok)*  solvmtx->symbmtx.bloknbr);

  solvmtx->cblktab = (SolverCblk*)malloc(sizeof(SolverCblk)*  solvmtx->symbmtx.cblknbr);
  solvmtx->bloktab = (SolverBlok*)malloc(sizeof(SolverBlok)*  solvmtx->symbmtx.bloknbr);

  solvmtx->coefnbr = 26;
  solvmtx->coefmax = 26;/*todo*/

  solvmtx->coeftab = (COEF*)malloc(sizeof(COEF)*solvmtx->coefnbr);

  int blk=0;

  /* col 0 */
  solvmtx->symbmtx.cblktab[0].fcolnum=0;
  solvmtx->symbmtx.cblktab[0].lcolnum=1;
  solvmtx->symbmtx.cblktab[0].bloknum=blk;
  solvmtx->cblktab[0].stride = 5;

  /* col 0 - diag */
  solvmtx->symbmtx.bloktab[blk].frownum = 0;
  solvmtx->symbmtx.bloktab[blk].lrownum = 1;
  solvmtx->symbmtx.bloktab[blk].cblknum = -1;
  solvmtx->bloktab[blk].coefind = 0;
  blk++;

  /* col 0 - block 1 */
  solvmtx->symbmtx.bloktab[blk].frownum = 3;
  solvmtx->symbmtx.bloktab[blk].lrownum = 3;
  solvmtx->symbmtx.bloktab[blk].cblknum = 1;
  solvmtx->bloktab[blk].coefind = 2;
  blk++;

  /* col 0 - block 2 */
  solvmtx->symbmtx.bloktab[blk].frownum = 5;
  solvmtx->symbmtx.bloktab[blk].lrownum = 6;
  solvmtx->symbmtx.bloktab[blk].cblknum = 2;
  solvmtx->bloktab[blk].coefind = 3;
  blk++;

  /* ------------------------------------ */
  /*   col 1 */
  solvmtx->symbmtx.cblktab[1].fcolnum=2;
  solvmtx->symbmtx.cblktab[1].lcolnum=4;
  solvmtx->symbmtx.cblktab[1].bloknum=blk;
  solvmtx->cblktab[1].stride = 4;

  /*col 1 - diag */
  solvmtx->symbmtx.bloktab[blk].frownum = 2;
  solvmtx->symbmtx.bloktab[blk].lrownum = 4;
  solvmtx->symbmtx.bloktab[blk].cblknum = -1;
  solvmtx->bloktab[blk].coefind = 10;
  blk++;
  
  /*col 1 - block 2 */
  solvmtx->symbmtx.bloktab[blk].frownum = 6;
  solvmtx->symbmtx.bloktab[blk].lrownum = 6;
  solvmtx->symbmtx.bloktab[blk].cblknum = 2;
  solvmtx->bloktab[blk].coefind = 13;
  blk++;

  /* ------------------------------------ */
  /*col 2 */
  solvmtx->symbmtx.cblktab[2].fcolnum=5;
  solvmtx->symbmtx.cblktab[2].lcolnum=6;
  solvmtx->symbmtx.cblktab[2].bloknum=blk;
  solvmtx->cblktab[2].stride = 2;

  /*col 2 - diag */
  solvmtx->symbmtx.bloktab[blk].frownum = 5;
  solvmtx->symbmtx.bloktab[blk].lrownum = 6;
  solvmtx->symbmtx.bloktab[blk].cblknum = -1;
  solvmtx->bloktab[blk].coefind = 22;
  blk++;

  /* ------------------------------------ */
  solvmtx->symbmtx.cblktab[3].bloknum=blk;

  assert( solvmtx->symbmtx.bloknbr == blk );

  /*  int coef[] = {1,2,4,6,8,0,3,5,7,9,5,6,5,1,0,6,3,2,0,0,8,3,2,1,0,3};*/
  int coef[] = {1,2,4,6,8,0,1,5,7,9,1,6,5,1,0,1,3,2,0,0,1,3,1,1,0,1};

  dim_t i;
  for (i=0; i<solvmtx->coefnbr; i++){
    solvmtx->coeftab[i] = coef[i];
  }

  dim_t k;
  for (k=0; k<solvmtx->symbmtx.cblknbr; k++)
    solvmtx->symbmtx.cblktab[k].hdim = solvmtx->cblktab[k].stride;

  return solvmtx;
}


SolverMatrix* createMatrixM() {
  SolverMatrix* solvmtx = (SolverMatrix*)malloc(sizeof(SolverMatrix)); 
  
  solvmtx->symbmtx.baseval = 0;
  solvmtx->symbmtx.cblknbr = 3;
  solvmtx->symbmtx.bloknbr = 6;
  solvmtx->symbmtx.nodenbr = 7;

  solvmtx->symbmtx.cblktab = (SymbolCblk*)malloc(sizeof(SymbolCblk)*  (solvmtx->symbmtx.cblknbr +1));
  solvmtx->symbmtx.bloktab = (SymbolBlok*)malloc(sizeof(SymbolBlok)*  solvmtx->symbmtx.bloknbr);

  solvmtx->cblktab = (SolverCblk*)malloc(sizeof(SolverCblk)*  solvmtx->symbmtx.cblknbr);
  solvmtx->bloktab = (SolverBlok*)malloc(sizeof(SolverBlok)*  solvmtx->symbmtx.bloknbr);

  solvmtx->coefnbr = 26;
  solvmtx->coefmax = 26;/*todo*/

  solvmtx->coeftab = (COEF*)malloc(sizeof(COEF)*solvmtx->coefnbr);

  int blk=0;

  /* col 0 */
  solvmtx->symbmtx.cblktab[0].fcolnum=0;
  solvmtx->symbmtx.cblktab[0].lcolnum=1;
  solvmtx->symbmtx.cblktab[0].bloknum=blk;
  solvmtx->cblktab[0].stride = 2;

  /* col 0 - block 0 */
  solvmtx->symbmtx.bloktab[blk].frownum = 3;
  solvmtx->symbmtx.bloktab[blk].lrownum = 3;
  solvmtx->symbmtx.bloktab[blk].cblknum = -1;
  solvmtx->bloktab[blk].coefind = 0;
  blk++;

  /* col 0 - block 1 */
  solvmtx->symbmtx.bloktab[blk].frownum = 5;
  solvmtx->symbmtx.bloktab[blk].lrownum = 5;
  solvmtx->symbmtx.bloktab[blk].cblknum = 1;
  solvmtx->bloktab[blk].coefind = 1;
  blk++;

  /* ------------------------------------ */
  /*   col 1 */
  solvmtx->symbmtx.cblktab[1].fcolnum=2;
  solvmtx->symbmtx.cblktab[1].lcolnum=4;
  solvmtx->symbmtx.cblktab[1].bloknum=blk;
  solvmtx->cblktab[1].stride = 4;
  
  /*col 1 - diag */
  solvmtx->symbmtx.bloktab[blk].frownum = 1;
  solvmtx->symbmtx.bloktab[blk].lrownum = 1;
  solvmtx->symbmtx.bloktab[blk].cblknum = -1;
  solvmtx->bloktab[blk].coefind = 4;
  blk++;
  
  /*col 1 - block 2 */
  solvmtx->symbmtx.bloktab[blk].frownum = 3;
  solvmtx->symbmtx.bloktab[blk].lrownum = 5;
  solvmtx->symbmtx.bloktab[blk].cblknum = -1;
  solvmtx->bloktab[blk].coefind = 5;
  blk++;

  /* ------------------------------------ */
  /*col 2 */
  solvmtx->symbmtx.cblktab[2].fcolnum=5;
  solvmtx->symbmtx.cblktab[2].lcolnum=6;
  solvmtx->symbmtx.cblktab[2].bloknum=blk;
  solvmtx->cblktab[2].stride = 5;

  /*col 2 - diag */
  solvmtx->symbmtx.bloktab[blk].frownum = 1;
  solvmtx->symbmtx.bloktab[blk].lrownum = 1;
  solvmtx->symbmtx.bloktab[blk].cblknum = -1;
  solvmtx->bloktab[blk].coefind = 16;
  blk++;

  /*col 2 - diag */
  solvmtx->symbmtx.bloktab[blk].frownum = 3;
  solvmtx->symbmtx.bloktab[blk].lrownum = 6;
  solvmtx->symbmtx.bloktab[blk].cblknum = -1;
  solvmtx->bloktab[blk].coefind = 17;
  blk++;

  /* ------------------------------------ */
  solvmtx->symbmtx.cblktab[3].bloknum=blk;

  assert( solvmtx->symbmtx.bloknbr == blk );

  int coef[] = {3,5,4,6,4,7,5,9,4,8,6,8,3,9,7,2,4,7,1,4,4,4,8,2,5,5};

  dim_t i;
  for (i=0; i<solvmtx->coefnbr; i++){
    solvmtx->coeftab[i] = coef[i];
  }

  dim_t k;
  for (k=0; k<solvmtx->symbmtx.cblknbr; k++)
    solvmtx->symbmtx.cblktab[k].hdim = solvmtx->cblktab[k].stride;

  return solvmtx;
}




int main() {
  SolverMatrix* A = createMatrixM();
  SolverMatrix* L = createMatrixL();


  /*printLD(A, "A.txt");*/
  /*printLD(L, "L.txt");*/

  SolverMatrix_TRSM(L, A);  

  /*printLD(A, "ATRSM.txt");*/

  printfv(5, "OK.\n");

/*   freeSolverMatrix(A); */
/*   freeSolverMatrix(L); */

  return 0;
}


#include <string.h>
REAL** outA(SolverMatrix* solvmtx) {
  dim_t n;

  dim_t k, p, i,j;
  REAL* ind;
  REAL**  mat;

  SymbolMatrix* symbmtx = &(solvmtx->symbmtx);
  n = symbmtx->nodenbr;

  mat = (REAL**)malloc(sizeof(REAL*)*n);
  for(i=0; i< n; i++) {
    mat[i] = (REAL*)malloc(sizeof(REAL)*n);
    bzero(mat[i], sizeof(REAL)*n);
  }

  ind = solvmtx->coeftab;

  for(k=0;k<symbmtx->cblknbr;k++) {

    for(i = symbmtx->cblktab[k].fcolnum; i<=symbmtx->cblktab[k].lcolnum; i++) {

      for(p=symbmtx->cblktab[k].bloknum;p<symbmtx->cblktabn[k+1].bloknum;p++) {

	for(j=symbmtx->bloktab[p].frownum; j<=symbmtx->bloktab[p].lrownum; j++) {
	  mat[i][j] = *ind;
	  ind++;
	}
      }
      
    }
  }
  
  return mat;

}


void printLD(SolverMatrix* LD, char* filename) {
  dim_t i,j;  
  int  n = LD->symbmtx.nodenbr; 

  REAL** LDLt = outA(LD);
  FILE * stream;
  stream = fopen(filename, "w");

  fprintfv(5, stream,"LD:= matrix([\n");

  for(j=0; j<n; j++) {    
    fprintfv(5, stream,"[");
    for(i=0; i<j; i++) {
      fprintfv(5, stream,"%.2f", LDLt[i][j]);
      fprintfv(5, stream,",");
    }

    fprintfv(5, stream,"%.2f", LDLt[i][j]);
    if (j!=n-1) fprintfv(5, stream,",");
    
    for(i=j+1; i<n; i++) {
      fprintfv(5, stream,"%.2f", LDLt[i][j]);
      if (i!=n-1) fprintfv(5, stream,",");
    }
    fprintfv(5, stream,"]");
    if (j!=n-1) fprintfv(5, stream,",\n");
  }
  fprintfv(5, stream,"]);\n");

  fclose(stream);
  
}


#endif
/* #endif */

/* @authors J. GAIDAMOUR */

#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#include <assert.h>
#include "solver.h"

#include "base.h"
#include "block.h"

void VS2_ICCTu(SolverMatrix* solvmtx, SymbolMatrix* symbmtxL, 
	       SolverMatrix* solvmtxU,COEF* W);

void VS_ICCTu(VSolverMatrix* vsolvmtxL, VSolverMatrix* vsolvmtxU, COEF* W) {
  
  VS2_ICCTu(vsolvmtxL->solvmtx, &vsolvmtxU->symbmtx, vsolvmtxL->solvmtx, W);

}

/* void Solver2DB(SolverMatrix* solvmtxL, SymbolMatrix* symbmtxL, DBMatrix* L) { */
/*   /\* symmetric; *\/ */
/*   /\* csc; *\/ */
/*   /\* dim1; *\/ */
/*   /\* dim2; *\/ */
/*   /\* tli, tlj, bri, brj; *\/ */

/*   /\* nodenbr; *\/ */
/*   L->cblknbr  = symbmtxL->cblknbr; */
/*   L->ccblktab = symbmtxL->ccblktab; */
/*   L->hdim     = symbmtxL->hdim; */
  
/*   /\*   INTL *cia; *\/ */
/*   /\*   INTL *cja; *\/ */
/*   /\*   VSolverMatrix **ca; *\/ */
  
/*   /\*   INTL *ria; *\/ */
/*   /\*   INTL *rja; *\/ */
/*   /\*   VSolverMatrix **ra; *\/ */

/*   L->bloknbr = symbmtxL->bloknbr; */
/*   /\* VSolverMatrix *bloktab; *\/ */

/*   /\* SymbolMatrix symbmtx; *\/ */
/*   L->coefmax = solvmtxL->coefmax; */

/*   L->alloc = ONE; */
/*   /\*   L->virtual =  *\/ */

/*   L->a = solvmtxL; */

/*   /\* SolverMatrix** cblktosolvmtx; *\/ */
/* } */

/* void VS2_ICCTu(SolverMatrix* solvL, SymbolMatrix* symbmtx,  */
/* 	       SolverMatrix* solvU, SymbolMatrix* symbmtxU, */
/* 	       COEF* F, COEF* W) { */
  
  
/*   /\*  DBMatrix L, U; *\/ */
/*   /\*   Solver2DB(solvmtxL, symbmtxL, &L); *\/ */
/*   /\*   Solver2DB(solvmtxU, symbmtxU, &U); *\/ */
  
/*   /\*   DBMatrix_FACTOu(&L, &U); *\/ */
/* } */


void DBMatrix_sFACTO_(int k, int cdim, 
		      SolverMatrix* solvL, SymbolMatrix* symbmtx, COEF* lc, int p, int rdim, blas_t stride,
		      COEF* uc,
		      COEF* W);

void VS2_ICCTu(SolverMatrix* solvL, SymbolMatrix* symbmtx, 
	       SolverMatrix* solvU, 
	       COEF* W) {
  
  int i=-1, ilast=-1;
  exit(1); /* BUG */


  dim_t p, k;

  COEF one = 1.0;
  char /* *sideL  = "L", */ *sideR  = "R";
  char /* *uploU  = "U", */ *uploL  = "L";
  char /* *transN = "N", */ *transT = "T";
  char *diagN  = "N", *diagU  = "U";

  blas_t stride;
  int cdim, rdim;
  COEF *ccL, *ccU; 
  COEF *bc, *lc, *uc; 

  assert(W != NULL);

  for(k=0;k<symbmtx->cblknbr;k++) {
    cdim   = symbmtx->ccblktab[k].lcolnum - symbmtx->ccblktab[k].fcolnum + 1; /** Largeur du bloc colonne **/
    stride = symbmtx->stride[k];

    /**********************************/
    /* Factorisation du bloc diagonal */
    /**********************************/
    /** Calcul du pointeur dans coeftab vers le  dÃ©but des coefficients du bloc diagonal **/ 
    p   = symbmtx->bcblktab[k].fbloknum;
    ccL = solvL->coeftab + solvL->bloktab[p].coefind;
    ccU = solvU->coeftab + solvU->bloktab[p].coefind;
      
    LU(cdim, ccL, stride, ccU, stride, EPSILON);
    
    /* TODO : voir desc2.c */      
    if ((i == ilast-1) && (k == symbmtx->cblknbr-1))
      continue;  /** Pas de bloc extradiagonal le calcul est terminer **/

    /************************************************************/
    /* "Diviser" les blocs extra diagonaux par le bloc diagonal */
    /************************************************************/
    {
      /** On effectue l'operation : M = M.(L^-1)t  avec M = l'ensemble compacte des blocs extra-diagonaux du bloc colonne **/

      p = symbmtx->bcblktab[k].fbloknum+1; /** Indice du premier bloc extra-diagonal **/
      if (symbmtx->bcblktab[k].fbloknum == symbmtx->bcblktab[k].lbloknum)
	continue; /*todo : voir le continue précédent */
	
      /* * * */ /** ==> L21 = A21 . (U11)^-1 **/
      /** Calcul du pointeur dans coeftab vers le  dÃ©but des coefficients du premier bloc extra diagonal **/ 

      bc = solvL->coeftab + solvL->bloktab[p].coefind;

      rdim = symbmtx->hdim[k] - cdim;  /** Hauteur de la surface compactÃ©e des bloc extra-diagonaux **/
      BLAS_TRSM(sideR,  uploL, transT, diagN, rdim, cdim, one, ccU, stride, bc, stride);
      /* * * */

      /* * * */ /** ==> U12 = (L11)^-1 . A12 **/
      /** Calcul du pointeur dans coeftab vers le dÃ©but des coefficients du premier bloc extra diagonal **/ 
      bc = solvU->coeftab + solvU->bloktab[p].coefind;
      BLAS_TRSM(sideR, uploL, transT, diagU, rdim, cdim, one, ccL, stride, bc, stride);
      /* * * */
    } 

    /** Boucle sur les blocs extra-diagonaux **/
    /** On l'initialise Ã  la heuteur totale des bloc extra diagonaux **/

    for(;p<=symbmtx->bcblktab[k].lbloknum; p++)
      {
	/** Calcul du pointeur dans coeftab vers le dÃ©but des coefficients du bloc extra-diagonal p **/ 
	lc = solvL->coeftab + solvL->bloktab[p].coefind;
	uc = solvU->coeftab + solvL->bloktab[p].coefind;

	/* sur L */
	DBMatrix_sFACTO_(k, cdim,
			 solvL, symbmtx, lc, p, rdim, stride,
			 uc,
			 W); /* TODO : supprimer des arguments et les "recalculer" dans la fonction ? */
	    
	/* sur U */
	DBMatrix_sFACTO_(k, cdim,
			 solvU, symbmtx, uc, p, rdim, stride,
			 lc,
			 W);

	/** Mettre Ã jour la hauteur des bloc extra diagonaux qui restent **/
	rdim -= symbmtx->bloktab[p].lrownum - symbmtx->bloktab[p].frownum+1;
      } /* p */
	
	
  } /* k */

}

void DBMatrix_sFACTO_(int k, int cdim, 
		     SolverMatrix* solvL, SymbolMatrix* symbmtx, COEF* lc, int p, int rdim, blas_t stride,
		     COEF* uc,
		     COEF* W){

  dim_t q, m;

  COEF *bc, *wc; 
  int hdim, mdim;

  SolverMatrix* facesolvmtx;
  SymbolMatrix* facesymbmtx;
  int facestride;
  int bloknum, facecblknum;
  int decalcol;
  int decalrow;

  int UN=1;
  COEF minusone = -1.0, zero = 0.0, one = 1.0;
  char *transN = "N", *transT = "T";

  /** Calcul de la hauteur du bloc extra diagonal p **/
  hdim = symbmtx->bloktab[p].lrownum - symbmtx->bloktab[p].frownum+1;

  /* assert(stride*hdim <= L->coefmax); */
       
  /*** Stocker dans W le produit des blocs {A(q,k), q>=p} avec Ft(p,k) ***/
  /*   printfv(5, "%d rdim=%d hdim=%d cdim=%d stride=%d stride=%d\n", ii, rdim, hdim, cdim, stride, stride); ii++; */
  BLAS_GEMM(transN, transT, rdim, hdim, cdim, one, lc, stride, uc, stride, zero, W, stride);

  /****************************************************************************************/
  /** Pour tout les blocs extra-diagonaux {A(j,k), j>= i} faire A(i,j) = A(i,j) - W(i, ) **/
  /****************************************************************************************/

  facesolvmtx = solvL;
  facesymbmtx = &facesolvmtx->symbmtx;
	    
  facecblknum = symbmtx->bloktab[p].cblknum - facesymbmtx->facedecal; 

  /** Stride du cblk en face du bloc extra-diagonal p **/
  facestride = facesymbmtx->stride[facecblknum];

  /** Nombre de colonne "Ã  gauche" de la zone modifiÃ©e dans le bloc colonne facecblknum **/
  decalcol = symbmtx->bloktab[p].frownum - facesymbmtx->ccblktab[facecblknum].fcolnum;

  /*** Largueur de la zone modifiÃ©e == hauteur du bloc p dans le bloc colonne k **/
  mdim = symbmtx->bloktab[p].lrownum - symbmtx->bloktab[p].frownum+1;

  bloknum = facesymbmtx->bcblktab[facecblknum].fbloknum; /** Indice du bloc diagonal du bloc colonne facebloknum **/

  for(q=p;q<=symbmtx->bcblktab[k].lbloknum;q++) /** Pour tous les bloc extradiagonaux A(q,k) avec q >= p **/
    {

      if (bloknum >= facesymbmtx->bcblktab[facecblknum].lbloknum+1/*todo le +1*/) goto fin; /* utile ici ? */

      /* skip block that cannot match */
      while(symbmtx->bloktab[q].frownum > facesymbmtx->bloktab[bloknum].lrownum) {
	bloknum++;
	if (bloknum >= facesymbmtx->bcblktab[facecblknum].lbloknum+1/*todo le +1*/) goto fin;
      }
	      
      /* for every block that match */
      while(symbmtx->bloktab[q].lrownum >= facesymbmtx->bloktab[bloknum].frownum) {

	/* assert ds le cas direct*/
	/*		assert(symbmtx->bloktab[bloknum].frownum <= symbmtx->bloktab[q].frownum);*/
	/*		assert(symbmtx->bloktab[bloknum].lrownum >= symbmtx->bloktab[q].lrownum);*/
		
	decalrow = symbmtx->bloktab[q].frownum - facesymbmtx->bloktab[bloknum].frownum;
		
	/** Calcul du pointeur de debut de la zone modifiÃ©e dans le bloc A(bloknum, facebloknum) **/
	bc = facesolvmtx->coeftab + facesolvmtx->bloktab[bloknum].coefind + decalcol*facestride + MAX(0,decalrow);
	      
	/** Calcul du pointeur de debut du bloc correspondant Ã  A(q,k) dans W **/
	wc = W + solvL->bloktab[q].coefind - solvL->bloktab[p].coefind + MAX(0,-decalrow);
		
	/*** Update de la contribution ***/
	hdim = MIN(symbmtx->bloktab[q].lrownum, facesymbmtx->bloktab[bloknum].lrownum) - 
	  MAX(symbmtx->bloktab[q].frownum, facesymbmtx->bloktab[bloknum].frownum) +1; /** Hauteur du bloc sur lequel on effectue la différence **/

	/* assert cas direct */
	/* assert(hdim == symbmtx->bloktab[q].lrownum -  symbmtx->bloktab[q].frownum +1); */

	for(m=0; m < mdim; m++)
	  {
	    BLAS_AXPY(hdim, minusone, wc, UN, bc, UN);
	    wc += stride;
	    bc += facestride;
	  }
		
	bloknum++;
	if (bloknum >= facesymbmtx->bcblktab[facecblknum].lbloknum+1/*todo +1*/) break;
      }
	      
    fin:
      bloknum--;
    }

}


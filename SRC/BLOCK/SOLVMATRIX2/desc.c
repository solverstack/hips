/* @authors J. GAIDAMOUR */

#define OBSOLETE
#ifdef OBSOLETE
#include <stdio.h>
#include <stdlib.h>

#include "solver.h"

#include "base.h"

#include <assert.h>
#include <string.h>
#include "phidal_common.h" /* printfdd */

void VS2_Lsol(flag_t unitdiag, SolverMatrix* solvmtx, SymbolMatrix* symbmtx, COEF* b, COEF* x);
void VS2_Usol(flag_t unitdiag, SolverMatrix* solvmtx, SymbolMatrix* symbmtx, COEF* b, COEF* x);

/* TODO : hdim==stride dans les tests à faire*/

void Lsolv(flag_t unitdiag, SolverMatrix* solvmtx, COEF *b, COEF *x)
{
  VS2_Lsol(unitdiag, solvmtx, &solvmtx->symbmtx, b, x);
}


void VS_Lsol(flag_t unitdiag, VSolverMatrix* vsolvmtx, COEF* b, COEF* x) {
  VS2_Lsol(unitdiag, vsolvmtx->solvmtx, &vsolvmtx->symbmtx, b, x);
}

void VS2_Lsol(flag_t unitdiag, SolverMatrix* solvmtx, SymbolMatrix* symbmtx, COEF* b, COEF* x) {

  /*---------------------------------------------------------------------
    | This function does the forward solve L x = b.
    | if unitdiag == 1 then the diagonal is considered to be unitary (stored or not stored)
    | Can be done in place.
    |----------------------------------------------------------------------
    | on entry:
    | solvmtx = the matrix (in SparRow form)
    | b     = a vector
    | unitdiag == 1 then the diagonal is considered to be unitary (stored or not stored)
    | on return
    | x     = the solution of L x = b 
    |--------------------------------------------------------------------*/
 
  int k,p,p2;
  int /* j, */jxtmp;

  COEF* xptr;
  int height, width, stride;

  COEF* xtmp;
  int size;

  COEF minusone= -1.0, zero=0.0, one=1.0;
  char *uploL = "L";
  char *transN = "N";
  char* diag = 0?"N":"U";
  int UN = 1;
  assert((unitdiag == 0) || (unitdiag == 1));

  assert(x==b);
  if (x!=b)
    BLAS_COPY(symbmtx->nodenbr, b, UN, x, UN);
  /* memcpy(x, b, sizeof(COEF)*solvmtx->);*/ /* DCPY ? */

  /* vecteur tmp */
  xtmp = (COEF*)malloc(sizeof(COEF)*symbmtx->nodenbr);

  /* parcours par blocs colonnes */
  for(k=0; k<symbmtx->cblknbr; k++) {
    width  = symbmtx->ccblktab[k].lcolnum - symbmtx->ccblktab[k].fcolnum +1;
    height = symbmtx->hdim[k] - width;          /** Hauteur de la surface compactÃ©e des bloc extra-diagonaux **/
    stride = symbmtx->stride[k];
    p    = symbmtx->bcblktab[k].fbloknum;        /* triangular block */
    /*     printf("k=%d/%d %d/%d\n", k, symbmtx->cblknbr-1, p, symbmtx->bloknbr); */
    assert(p<symbmtx->bloknbr);
    xptr = x + symbmtx->ccblktab[k].fcolnum - symbmtx->tli;
    
    /* résolution triangulaire */
    /*FIXME : y a un bug */
    BLAS_TRSV(uploL, transN, diag, width /*=h*/,
		solvmtx->coeftab + solvmtx->bloktab[p].coefind, stride, xptr, UN);

    if(k == symbmtx->cblknbr - 1)
      continue;  /** Pas de bloc extradiagonal le calcul est terminer **/
    
    /* calcul des contributions (aggrégation des colonnes) */
    BLAS_GEMV(transN, height, width, minusone /*coef alpha*/, 
		solvmtx->coeftab + solvmtx->bloktab[p+1].coefind, stride, 
		xptr, UN,       zero /*coef beta*/, 
		xtmp, UN);

    /* x := xtmp (out) */
    for(p2=p+1, jxtmp=0; p2<=symbmtx->bcblktab[k].lbloknum; p2++) {
      size = symbmtx->bloktab[p2].lrownum - symbmtx->bloktab[p2].frownum +1;
      BLAS_AXPY(size, one, xtmp + jxtmp, UN, x + symbmtx->bloktab[p2].frownum - symbmtx->tli, UN);
      jxtmp += size;
    }

  }

  free(xtmp);
}

void Usolv(flag_t unitdiag, SolverMatrix* solvmtx, COEF *b, COEF *x)
{
  VS2_Usol(unitdiag, solvmtx, &solvmtx->symbmtx, b, x);
}


void VS_Usol(flag_t unitdiag, VSolverMatrix* vsolvmtx, COEF* b, COEF* x) {
  VS2_Usol(unitdiag, vsolvmtx->solvmtx, &vsolvmtx->symbmtx, b, x);
}

void VS2_Usol(flag_t unitdiag, SolverMatrix* solvmtx, SymbolMatrix* symbmtx, COEF* b, COEF* x) 
{
  /*---------------------------------------------------------------------
    | This function does the backward solve U x = b.
    | Can be done in place.
    |----------------------------------------------------------------------
    | on entry:
    | solvmtx  = the matrix (in SparRow form)
    | b    = a vector
    |
    | on return
    | x     = the solution of U * x = b 
    |
    |---------------------------------------------------------------------*/

  int k,p,p2;
  int /* j, */jxtmp;
  
  COEF* xptr;
  int height, width, stride;

  COEF* xtmp;
  int size;

  COEF minusone= -1.0, one=1.0;
  char *uploL = "L";
  char *transT = "T";
  char* diag = 0?"N":"U";
  int UN = 1;
  assert((unitdiag == 0) || (unitdiag == 1));

  assert(x==b);
  if (x!=b)
    BLAS_COPY(symbmtx->nodenbr, b, UN, x, UN);

  /* vecteur tmp */
  xtmp = (COEF*)malloc(sizeof(COEF)*symbmtx->nodenbr);

  /* parcours par blocs colonnes */
  for(k=symbmtx->cblknbr-1; k>=0; k--) {

    width  = symbmtx->ccblktab[k].lcolnum - symbmtx->ccblktab[k].fcolnum +1;
    height = symbmtx->hdim[k] - width; /** Hauteur de la surface compactÃ©e des bloc extra-diagonaux **/
    stride = symbmtx->stride[k];
    p    = symbmtx->bcblktab[k].fbloknum;        /* triangular block */
    xptr = x + symbmtx->ccblktab[k].fcolnum - symbmtx->tli;

/*     printfd("w=%d h=%d s=%d\n", width, height, stride); */

    assert(width <= stride);

    if(k != symbmtx->cblknbr - 1) {
      
      /* xtmp := x (in) */
      for(p2=p+1, jxtmp=0; p2<=symbmtx->bcblktab[k].lbloknum; p2++) {
      	size = symbmtx->bloktab[p2].lrownum - symbmtx->bloktab[p2].frownum +1;
      	BLAS_COPY(size, x + symbmtx->bloktab[p2].frownum - symbmtx->tli, UN, xtmp + jxtmp, UN);
      	jxtmp += size; /* todo : remplacer int jxtmp par int*, evite 1 operation  + x+ symbmtx aussi?*/
      }
      
      if (symbmtx->hdim[k] == 0) continue;
      
      /* calcul des contributions (aggrégation des colonnes) */
      BLAS_GEMV(transT, height, width, minusone /*coef alpha*/, 
		  solvmtx->coeftab + solvmtx->bloktab[p+1].coefind, stride, 
		  xtmp, UN,       one /*coef beta*/,
		  xptr, UN);

    }
      
/*    if (stride == 0) continue; */

    /* résolution triangulaire */
    BLAS_TRSV(uploL, transT, diag, width /*=h*/,
	  solvmtx->coeftab + solvmtx->bloktab[p].coefind, stride, xptr, UN);
    
  }

  free(xtmp);
}

/* void Ltsolv(flag_t unitdiag, SolverMatrix* solvmtx, COEF *b, COEF *x) */
/* { */
/*   Usolv(unitdiag, solvmtx, b, x); */
/* } */

void Dsolv(SolverMatrix* solvmtx, COEF* b) 
{
  SymbolMatrix* symbmtx = &(solvmtx->symbmtx);
  dim_t k,i;
  blas_t stride;
  dim_t p;
  COEF* ptr;
  int j=0;
  
  for(k=0; k<symbmtx->cblknbr; k++) {
    p      = symbmtx->bcblktab[k].fbloknum;        /* triangular block */
    ptr    = solvmtx->coeftab + solvmtx->bloktab[p].coefind;
    stride = symbmtx->stride[k];
    for (i=symbmtx->ccblktab[k].fcolnum; i<=symbmtx->ccblktab[k].lcolnum; i++, j++) {
#ifndef DEBUG_NOALLOCATION      
      b[j] /= ptr[0];
#endif
      ptr += stride + 1;
    }
  }
}


void DcpyMatrixToDiag(SolverMatrix* solvmtx, COEF* Diag) 
{
  SymbolMatrix* symbmtx = &(solvmtx->symbmtx);
  dim_t k,i;
  blas_t stride;
  dim_t p;
  COEF* ptr;
  int j=0;
  
  for(k=0; k<symbmtx->cblknbr; k++) {
    p      = symbmtx->bcblktab[k].fbloknum;        /* triangular block */
    ptr    = solvmtx->coeftab + solvmtx->bloktab[p].coefind;
    stride = symbmtx->stride[k];
    for (i=symbmtx->ccblktab[k].fcolnum; i<=symbmtx->ccblktab[k].lcolnum; i++, j++) {
      Diag[j] = ptr[0]; /* *ptr */
      ptr += stride + 1;
    }
  }
}

void DcpyDiagToMatrix(SolverMatrix* solvmtx, COEF* Diag) 
{
  SymbolMatrix* symbmtx = &(solvmtx->symbmtx);
  dim_t k,i;
  blas_t stride;
  dim_t p;
  COEF* ptr;
  int j=0;
  
  for(k=0; k<symbmtx->cblknbr; k++) {
    p      = symbmtx->bcblktab[k].fbloknum;        /* triangular block */
    ptr    = solvmtx->coeftab + solvmtx->bloktab[p].coefind;
    stride = symbmtx->stride[k];
    for(i=symbmtx->ccblktab[k].fcolnum; i<=symbmtx->ccblktab[k].lcolnum; i++, j++) {
      if(ptr[0] != Diag[j]) printfd("Modif\n");
      ptr[0] = Diag[j];
      ptr += stride + 1;
    }
  }
}



#ifdef TEST
#include <assert.h>
/* #undef Dprintf */
/* #define Dprintfd(args...) printf(args) */
/* TODO : Solvermatrix!!*/
int main() {
  SolverMatrix* solvmtx = (SolverMatrix*)malloc(sizeof(SolverMatrix)); 
  
  solvmtx->symbmtx.baseval = 0;
  solvmtx->symbmtx.cblknbr = 4;
  solvmtx->symbmtx.bloknbr = 8;
  solvmtx->symbmtx.nodenbr = 11;

  solvmtx->symbmtx.cblktab = (SymbolCblk*)malloc(sizeof(SymbolCblk)*  (solvmtx->symbmtx.cblknbr +1));
  solvmtx->symbmtx.bloktab = (SymbolBlok*)malloc(sizeof(SymbolBlok)*  solvmtx->symbmtx.bloknbr);

  solvmtx->cblktab = (SolverCblk*)malloc(sizeof(SolverCblk)*  solvmtx->symbmtx.cblknbr);
  solvmtx->bloktab = (SolverBlok*)malloc(sizeof(SolverBlok)*  solvmtx->symbmtx.bloknbr);

  solvmtx->coefnbr = 26+11+11;

  solvmtx->coeftab = (COEF*)malloc(sizeof(COEF)*solvmtx->coefnbr);

  int blk=0;

  /* col 0 */
  solvmtx->symbmtx.cblktab[0].fcolnum=0;
  solvmtx->symbmtx.cblktab[0].lcolnum=2;
  solvmtx->symbmtx.cblktab[0].bloknum=blk;
  solvmtx->cblktab[0].stride = 6;

  /* col 0 - diag */
  solvmtx->symbmtx.bloktab[blk].frownum = 0;
  solvmtx->symbmtx.bloktab[blk].lrownum = 2;
  solvmtx->symbmtx.bloktab[blk].cblknum = -1;
  solvmtx->bloktab[blk].coefind = 0;
  blk++;

  /* col 0 - block 1 */
  solvmtx->symbmtx.bloktab[blk].frownum = 4;
  solvmtx->symbmtx.bloktab[blk].lrownum = 5;
  solvmtx->symbmtx.bloktab[blk].cblknum = -1;
  solvmtx->bloktab[blk].coefind = 3;
  blk++;

  /* col 0 - block 2 */
  solvmtx->symbmtx.bloktab[blk].frownum = 9;
  solvmtx->symbmtx.bloktab[blk].lrownum = 9;
  solvmtx->symbmtx.bloktab[blk].cblknum = -1;
  solvmtx->bloktab[blk].coefind = 5;
  blk++;

  /* ------------------------------------ */
  /*   col 1 */
  solvmtx->symbmtx.cblktab[1].fcolnum=3;
  solvmtx->symbmtx.cblktab[1].lcolnum=6;
  solvmtx->symbmtx.cblktab[1].bloknum=blk;
  solvmtx->cblktab[1].stride = 5;

  /*col 1 - diag */
  solvmtx->symbmtx.bloktab[blk].frownum = 3;
  solvmtx->symbmtx.bloktab[blk].lrownum = 6;
  solvmtx->symbmtx.bloktab[blk].cblknum = -1;
  solvmtx->bloktab[blk].coefind = 18;
  blk++;
  
  /*col 1 - block 2 */
  solvmtx->symbmtx.bloktab[blk].frownum = 8;
  solvmtx->symbmtx.bloktab[blk].lrownum = 8;
  solvmtx->symbmtx.bloktab[blk].cblknum = -1;
  solvmtx->bloktab[blk].coefind = 22;
  blk++;

  /* ------------------------------------ */
  /*col 2 */
  solvmtx->symbmtx.cblktab[2].fcolnum=7;
  solvmtx->symbmtx.cblktab[2].lcolnum=8;
  solvmtx->symbmtx.cblktab[2].bloknum=blk;
  solvmtx->cblktab[2].stride = 3;

  /*col 2 - diag */
  solvmtx->symbmtx.bloktab[blk].frownum = 7;
  solvmtx->symbmtx.bloktab[blk].lrownum = 8;
  solvmtx->symbmtx.bloktab[blk].cblknum = -1;
  solvmtx->bloktab[blk].coefind = 38;
  blk++;

  /*col 2 - block 1 */
  solvmtx->symbmtx.bloktab[blk].frownum = 9;
  solvmtx->symbmtx.bloktab[blk].lrownum = 9;
  solvmtx->symbmtx.bloktab[blk].cblknum = -1;
  solvmtx->bloktab[blk].coefind = 40;
  blk++;

  /* ------------------------------------ */
  /*col 3 */
  solvmtx->symbmtx.cblktab[3].fcolnum=9;
  solvmtx->symbmtx.cblktab[3].lcolnum=10;
  solvmtx->symbmtx.cblktab[3].bloknum=blk;
  solvmtx->cblktab[3].stride = 2;

  /*col 3 - diag */
  solvmtx->symbmtx.bloktab[blk].frownum = 9;
  solvmtx->symbmtx.bloktab[blk].lrownum = 10;
  solvmtx->symbmtx.bloktab[blk].cblknum = -1;
  solvmtx->bloktab[blk].coefind = 44;
  blk++;

  /* ------------------------------------ */
  solvmtx->symbmtx.cblktab[4].bloknum=blk;

  dim_t k;
  for (k=0; k<solvmtx->symbmtx.cblknbr; k++)
    solvmtx->symbmtx.cblktab[k].hdim = solvmtx->cblktab[k].stride;

  assert( solvmtx->symbmtx.bloknbr == blk );
  /* ------------------------------------ */

  int coef[] = {1,5,6,1,1,1,0,2,0,2,0,2,0,0,3,0,2,1,4,1,2,5,1,0,5,4,3,2,0,0,6,2,3,0,0,0,7,4,8,1,1,0,9,3,10,1,0,11};

  dim_t i;
  for (i=0; i<solvmtx->coefnbr; i++){
    solvmtx->coeftab[i] = coef[i];
  }

  int bdesc[] = {1, 7, 9, 4, 14, 41, 54, 8, 77, 53, 21};      /*descente*/
  int bremo[] = {50, 32, 25, 65, 68, 47, 43, 27, 39, 21, 11}; /*remontee*/

  COEF* b = (COEF*)malloc(sizeof(COEF)*solvmtx->symbmtx.nodenbr);
  
  /*TEST DESCENTE */
  for (i=0; i<solvmtx->symbmtx.nodenbr; i++)
    b[i] = bdesc[i];

  descente(solvmtx, b);
  
  for (i=0; i<solvmtx->symbmtx.nodenbr; i++)
    assert(b[i] == i+1);

  /*TEST REMONTEE */
  for (i=0; i<solvmtx->symbmtx.nodenbr; i++)
    b[i] = bremo[i];

  remontee(solvmtx, b);
  
  for (i=0; i<solvmtx->symbmtx.nodenbr; i++)
    assert(b[i] == i+1);

  printfd("OK.\n");

  free(solvmtx->coeftab);
  free(solvmtx->bloktab);
  free(solvmtx->cblktab);
  free(solvmtx->symbmtx.bloktab);
  free(solvmtx->symbmtx.cblktab);
  free(solvmtx);
  
  free(b);

  return 0;
}

#endif
#endif

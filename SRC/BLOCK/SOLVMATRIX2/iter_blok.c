/* @authors J. GAIDAMOUR */

#ifdef OBSOLETE
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>
#include <assert.h>

#include "solver.h"
#include "phidal_common.h"

/*TODO : ->n 15 fois */

/*#define IM_ALLOC_AT_START*/

extern REAL norm2(REAL *x, int n);

extern void Lsolv2(flag_t unitdiag, SolverMatrix* mata, COEF *b, COEF *x);
extern void Ltsolv2(flag_t unitdiag, SolverMatrix* mata, COEF *b, COEF *x);
extern void Dsolv2(SolverMatrix* solvmtx, COEF* b);

int_t fgmresd_blok2(mpi_t proc_id, csptr A, SolverMatrix* L,  PhidalOptions *option, 
		 COEF *rhs, COEF * x, FILE *fp)  
{

  chrono_t t1, t2; /** Used for timers **/
  COEF *wk1, *wk2;
  int i, i1, j, im, maxits,incx,n, out_flag,in_flag; 
  int ii,k,k1;
  COEF **vv, **hh, *hhloc, **w, *c, *s, *rs;
  COEF *sol, eps, eps1, tloc, ro, t, alpha, gam;
  int alloc_flag;


  int iters;


 /*------------------------------------------------------------------*
  |      PART1:  Initialization.                                     |
  *------------------------------------------------------------------*/
  /* retrieve information from input data structure */

  n = A->n;
  sol = x;
 
  im = option->krylov_im;
  maxits = option->itmax;
  eps = option->tol;
  
  /* Create working vector wk1 and wk2 */


  /* allocate memory for working local arrays */
  vv = (COEF **)malloc(sizeof(COEF *)*(im+2));
  /** Important to do that (see the free at the end) **/
  bzero(vv, sizeof(COEF *)*(im+2));

#ifdef IM_ALLOC_AT_START
  for(i = 0; i < im+1; i++) 
    vv[i] = (COEF *)malloc(sizeof(COEF)*(n+1));

#endif

  hh = (COEF **)malloc(sizeof(COEF *) * (im+1));
  for(i = 0; i < im; i++) {
    hh[i] = (COEF *)malloc(sizeof(COEF)*(im+2));
  }

  c = (COEF *)malloc(sizeof(COEF)*(im+1));
  s = (COEF *)malloc(sizeof(COEF)*(im+1));
  rs = (COEF *)malloc(sizeof(COEF)*(im+2));
  hhloc = (COEF *)malloc(sizeof(COEF)*(im+2));
  w = (COEF **)malloc(sizeof(COEF *)*(im+1));

  /** Important to do that (see the free at the end) **/
  bzero(w, sizeof(COEF *)*(im+1));

#ifdef IM_ALLOC_AT_START
  for(i = 0; i < im; i++) {
    w[i] = (COEF *)malloc(sizeof(COEF)*(n+1));
  }
#endif

  /*-----------------------------------------------------------------*
  |      PART2: Iteration.                                           |
  *------------------------------------------------------------------*/
  alpha = -1.0;
  incx = 1;
  out_flag = TRUE;
  /*ipar->iters = 0;*/
  iters = 0;
#ifndef IM_ALLOC_AT_START
  alloc_flag = TRUE; /** OIMBE to allocate the base iteratively **/
#endif
  /* outer loop starts here */
  while(out_flag) {
    
#ifndef IM_ALLOC_AT_START
    if(alloc_flag == TRUE)
      vv[0] = (COEF *)malloc(sizeof(COEF)*(n+1));
#endif

    /* wk1 points to sol, wk2 points to vv[0] */
    wk1 = sol;
    wk2 = vv[0];

    /* vv[0] = A*sol  */ 
    /*amxdis(dm, wk1, wk2);*/

    /*dbmatvec_prod(proc_id, A, wk1, wk2, BL, BK, SOLVENV);*/
    matvec(A, wk1, wk2);
    
    /* rhs - wk2 */
    for(j = 0; j < n; j++) {
      vv[0][j] = rhs[j] - vv[0][j];
    }

    /*tloc = DDOT(n, vv[0], incx, vv[0], incx);*/
    /*tloc = dist_ddot(proc_id, vv[0], vv[0], BL);
      MPI_Allreduce(&tloc,&ro,1,MPI_DOUBLE,MPI_SUM, MPI_COMM_WORLD);*/
    ro = DDOT(n, vv[0], incx, vv[0], incx);

    ro = sqrt(ro);
    
    if(proc_id == 0 && fp != NULL)
      fprintfv(5, fp, "Its %d   norm = %e \n", iters, ro);

    
    if(fabs(ro-ZERO) <= EPSILON) {
      out_flag = FALSE;
      break;
    }
    t = 1.0 / ro;
    DSCAL(n, t, vv[0], incx);
    /*if(ipar->iters == 0)*/
    if(iters == 0)
      eps1 = eps*ro;
    /* initialize 1-st term of rhs of hessenberg system */
    rs[0] = ro;
    i = -1;
    in_flag = TRUE;
    while(in_flag) {
      i++;

      /*ipar->iters++;*/
      iters++;

#ifndef IM_ALLOC_AT_START
      if(alloc_flag == TRUE)
	w[i] = (COEF *)malloc(sizeof(COEF)*(n+1));
#endif




      i1 = i + 1;
      /* wk1 points to vv[i] */
      wk1 = vv[i];
      /* wk2 points to w[i] */
      wk2 = w[i];



      /* preconditioning operation, wk2 = M^{-1} * wk1 */
      /**Copy wk1 because HID_solve modify it **/
      /*PHIDAL_solve(proc_id, wk2, temp, nproc, PREC, option);*/
      t1 = dwalltime();
      /** Solve L.x = b **/
      Lsolv2(1, L, wk1, wk2);

      /** Solve the diagonal **/
      Dsolv2(L, wk2);

      /** Solve Lt.x=b **/
      Ltsolv2(1, L, wk2, wk2);


      t2 = dwalltime();
      if(iters == 1)
        fprintfv(5, stdout, " DR  in %g seconds \n", t2-t1);

#ifndef IM_ALLOC_AT_START
      if(alloc_flag == TRUE)
	vv[i1] = (COEF *)malloc(sizeof(COEF)*(n+1));
#endif

      /*memcpy(wk2, wk1, sizeof(COEF)*n);*/ /*NO PRECOND */

      /* wk1 points to vv[i1] */
      wk1 = vv[i1];

      /* vv[i1] = A * (M^{-1}*vv[i]) */
      /*dbmatvec_prod(proc_id, A, wk2, wk1, BL, BK, SOLVENV);*/
      matvec(A, wk2, wk1);

      /* classical gram - schmidt */
      /*for(j = 0; j <= i; j++) 
	hhloc[j] = dist_ddot(proc_id, vv[j], vv[i1], BL);
      MPI_Allreduce(hhloc, hh[i], i+1, MPI_DOUBLE, MPI_SUM,
      MPI_COMM_WORLD);*/
      for(j = 0; j <= i; j++) 
	hh[i][j] = DDOT(n, vv[j], incx, vv[i1], incx);
      

      for(j = 0; j <= i; j++) {
	alpha = -hh[i][j];
	DAXPY(n, alpha, vv[j], incx, vv[i1], incx);
      }

      /*tloc = dist_ddot(proc_id, vv[i1], vv[i1], BL);
	MPI_Allreduce(&tloc, &t, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);*/
      t = DDOT(n, vv[i1], incx, vv[i1], incx);

      if(t<= 0.0)
	{
	  fprintfv(5, stderr, "problem in distributed dot product t= %e tloc = %e \n", t, tloc);
	  /*MPI_Abort(MPI_COMM_WORLD, -1);*/
	  exit(-1);
	}

      t = sqrt(t);
      hh[i][i1] = t;


      if(fabs(t) > 10e-50) {
	t = 1.0 / t;
	DSCAL(n, t, vv[i1], incx);
      }
      else
	{
	  fprintfv(5, stderr , "t IS NULL %e \n", t);
	  /*MPI_Abort(MPI_COMM_WORLD, -1);*/
	  exit(-1);
	}

      /* done with classical gram schimd and arnoldi step. now update
	 factorization of hh */
      if(i != 0) {
	for(k = 1; k <= i; k++) {
	  k1 = k - 1;
	  t = hh[i][k1];
	  hh[i][k1] = c[k1]*t + s[k1]*hh[i][k];
	  hh[i][k] = -s[k1]*t + c[k1]*hh[i][k];
	}
      }
      gam = sqrt(hh[i][i]*hh[i][i] + hh[i][i1]*hh[i][i1]);
      if(fabs(gam-ZERO) <= EPSILON)
	gam = EPSMAC;
      /* determine-next-plance-rotation */
      c[i] = hh[i][i]/gam;
      s[i] = hh[i][i1]/gam;
      rs[i1] = -s[i]*rs[i];
      rs[i] = c[i]*rs[i];
      /* determine res. norm and test for convergence */
      hh[i][i] = c[i]*hh[i][i] + s[i]*hh[i][i1];
      ro = fabs(rs[i1]);

      if(proc_id == 0 && fp != NULL)
	fprintfv(5, fp, " Iter %d     res %e \n", iters, ro); 
      if((i+1 >= im) || (ro <= eps1) || iters >= maxits) {
	in_flag = FALSE;
      }
    }
#ifndef IM_ALLOC_AT_START
    alloc_flag = FALSE;
#endif



    /* now compute solution first solve upper triangular system */
    rs[i] = rs[i]/hh[i][i];
    for(ii = 2; ii <= i+1; ii++) {
      k = i-ii+1;
      k1 = k+1;
      t = rs[k];
      for(j = k1; j <= i; j++) {
	t = t - hh[j][k]*rs[j];
      }
      rs[k] = t/hh[k][k];
    }
    /* done with back substitution. now form linear combination to get 
       solution */
    for(j = 0; j <= i; j++) {
      t = rs[j];
      DAXPY(n, t, w[j], incx, sol, incx);
    }
    /* test for return */

    if((ro <= eps1) || (iters >= maxits)) {
      out_flag = FALSE;
    }
  }
      
 /*----------------------------------------------------------------*
  |  PART3:    Release the working buffer                          |
  *----------------------------------------------------------------*/

  /* free memories for preconditioner and working vectors */
  for(i = 0; i < im; i++) {
    if(w[i] != NULL)
      free(w[i]);
  }
  free(w);

  for(i = 0; i < im+1; i++){
      if(vv[i] != NULL)
	free(vv[i]);
  }
  free(vv);
  for(i = 0; i < im; i++) {
    free(hh[i]);
  }
  free(hh);
  free(c); free(s); free(rs); free(hhloc);
  
  return iters;

} /* End of fgmresd() */


#endif

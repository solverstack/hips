/* @authors J. GAIDAMOUR */

#ifdef OBSOLETE
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include "solver.h"


/* regrouper les acces a tmp >> ??*/
/* todo : possiblité d'utiliser vecteur tmp de taille max(hdim) sur tous les k */
/* todo : repenser aux différentes possibilités pour savoir si on fait un copy blas ou + tot autre chose*/
/* TODO : faire + simple pour "x+ ..-tli" avec un xé = x-tli par ex*/
/* TODO : changer p et p2 */

void bmatvec2(SolverMatrix* solvmtx, COEF* x, COEF* y, char* UPLO) {
/*---------------------------------------------------------------------
| This function does the matrix vector product y = A x.
|----------------------------------------------------------------------
| on entry:
| solvmtx = the matrix (in SolverMatrix form)
| x       = a vector
|
| on return
| y     = the product A * x
| TODO : expliquer UPLO
|--------------------------------------------------------------------*/

  int k,p,p2;
  
  COEF* xptr;
  COEF* yptr;
  COEF* solvmtxptr;
  int height, width, stride;

  SymbolMatrix* symbmtx = &(solvmtx->symbmtx);

  /* vecteur tmp */
  COEF* tmp = (COEF*)malloc(sizeof(COEF)*symbmtx->nodenbr);
  int jtmp, size;

  COEF zero=0.0, one=1.0;
  char *uploL = "L";
  char *transN = "N", *transT = "T";
  int UN = 1;

  /* TODO dim1 / dim 2 */
  /*   printfv(5, "symbmtx->nodenbr = %d\n",symbmtx->nodenbr); */
  bzero(y, sizeof(COEF)*symbmtx->nodenbr);

  /* parcours par blocs colonnes */
  for(k=0; k<symbmtx->cblknbr; k++) {

    width  = symbmtx->ccblktab[k].lcolnum - symbmtx->ccblktab[k].fcolnum +1;
    height = symbmtx->hdim[k]; /** Hauteur de la surface compactÃ©e des bloc extra-diagonaux **/
    stride = symbmtx->stride[k];
    p    = symbmtx->bcblktab[k].fbloknum; /* triangular block */
    xptr = x + symbmtx->ccblktab[k].fcolnum - symbmtx->tli;
    yptr = y + symbmtx->ccblktab[k].fcolnum - symbmtx->tli;

    if(strcmp(UPLO, "N") == 0) {
      /* bloc triangulaire */

      /* y := alpha*A*x + beta*y */
      DSYMV(uploL, width, one /*alpha*/, 
	    solvmtx->coeftab + solvmtx->bloktab[p].coefind, stride, xptr, UN,
	    one /*beta*/, yptr, UN);

      if(k == symbmtx->cblknbr - 1)
	continue;  /** Pas de bloc extradiagonal le calcul est terminer **/ 
     
      /* skip diag block ... */
      height -= width; /** Hauteur de la surface compactÃ©e des bloc extra-diagonaux **/
      p++;
    }

    solvmtxptr = solvmtx->coeftab + solvmtx->bloktab[p].coefind;

    if ((strcmp(UPLO, "N") == 0) || (strcmp(UPLO, "U") == 0)) {

      /* tmp := x */
      for(p2=p, jtmp=0; p2<=symbmtx->bcblktab[k].lbloknum; p2++) {
      	size = symbmtx->bloktab[p2].lrownum - symbmtx->bloktab[p2].frownum +1;
      	DCOPY(size, x + symbmtx->bloktab[p2].frownum - symbmtx->tli, UN, tmp + jtmp, UN); 
      	jtmp += size;
      }
      
      /* calcul ligne */
      DGEMV(transT, height, width, one /*coef alpha*/, 
		solvmtxptr, stride, 
		tmp, UN,       one /*coef beta*/,
		yptr, UN);
    }

    if ((strcmp(UPLO, "N") == 0) || (strcmp(UPLO, "L") == 0)) {

      /* calcul colonne */
      DGEMV(transN, height, width, one /*coef alpha*/, 
		solvmtxptr, stride,
		xptr, UN,      zero /*coef beta*/,
		tmp, UN);
      
      /* y := tmp  */       
      for(p2=p, jtmp=0; p2<=symbmtx->bcblktab[k].lbloknum; p2++) {
	size = symbmtx->bloktab[p2].lrownum - symbmtx->bloktab[p2].frownum +1;
	DAXPY(size, one, tmp + jtmp, UN, y + symbmtx->bloktab[p2].frownum - symbmtx->tli, UN);
	jtmp += size;
      }
      
    }
  }
  
  free(tmp);
}



#endif

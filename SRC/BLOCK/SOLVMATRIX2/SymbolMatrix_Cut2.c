/* @authors J. GAIDAMOUR */

/*tmp*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h> /* memcpy */
#include <assert.h>

#include "block.h"

void SymbolMatrix_Cut2(SymbolMatrix* symbA, SymbolMatrix* symbB,
		       dim_t tli, dim_t tlj, dim_t bri, dim_t brj, alloc_t alloc) { 
  dim_t cblktlj, cblkbrj;
  int kA, kB, pA, pB;
 
  /*** cblktlj and cblkbrj ***/
  kA=0;
  while(symbA->ccblktab[kA].fcolnum < tlj) {
    kA++;
    assert(kA<symbA->cblknbr); /*TODO*/
  }
  cblktlj = kA;

  while(symbA->ccblktab[kA].fcolnum <= brj) {
    kA++;
    if(kA>=symbA->cblknbr) { /*TODO : le kA++ apres*/
      goto end1;
    };
  }
 end1:
  cblkbrj = kA-1;

  /*** SymbolMatrix ***/

  symbB->cblknbr = symbA->cblknbr - (cblktlj + (symbA->cblknbr - cblkbrj - 1));
  symbB->nodenbr = symbA->ccblktab[cblkbrj].lcolnum - symbA->ccblktab[cblktlj].fcolnum +1;    

  /* blocknbr */
  symbB->bloknbr = 0;
  for(kA=cblktlj; kA<=cblkbrj; kA++) {
    pA=symbA->bcblktab[kA].fbloknum;
 
    if (pA> symbA->bcblktab[kA].lbloknum) goto end; 

    while(symbA->bloktab[pA].frownum < tli) {
      pA++;
      if (pA> symbA->bcblktab[kA].lbloknum) goto end;
    }

    if (pA> symbA->bcblktab[kA].lbloknum) goto end; /*TODO : simplifier*/

    while(symbA->bloktab[pA].frownum <= bri) {
      symbB->bloknbr++;
      pA++;
      if (pA> symbA->bcblktab[kA].lbloknum) goto end;
    }
     
  end:
    ;
  }

  symbB->virtual=0;

  symbB->bcblktab   = (SymbolBCblk*)malloc(sizeof(SymbolBCblk)*symbB->cblknbr);


  /*TODO : ONE*/

  if (alloc == RBLK) {
    assert(cblktlj == 0);
    symbB->ccblktab = symbA->ccblktab + cblktlj; /*temporairement, puis NULL*/
  } else {
    symbB->ccblktab   = (SymbolCCblk*)malloc(sizeof(SymbolCCblk)*symbB->cblknbr);
    memcpy(symbB->ccblktab, symbA->ccblktab + cblktlj, symbB->cblknbr*sizeof(SymbolCCblk));
  }

  symbB->hdim       = NULL; 
  symbB->stride     = NULL;

  if (symbB->bloknbr != 0)
    symbB->bloktab    = (SymbolBlok*)malloc(sizeof(SymbolBlok)*symbB->bloknbr);
  else symbB->bloktab = NULL;

  symbB->facedecal = symbA->facedecal + cblktlj;
  symbB->tli       = tli;
  symbB->cblktlj   = 0; 

  /*** Fill-in SymbolMatrix and SolverMatrix ***/
  pB=0;
  for(kA=cblktlj, kB=0; kA<=cblkbrj; kA++, kB++) {
    symbB->bcblktab[kB].fbloknum = pB; 

    pA = symbA->bcblktab[kA].fbloknum;

    if (pA> symbA->bcblktab[kA].lbloknum) { 
      symbB->bcblktab[kB].fbloknum = pB;
      goto end2;
    }

    while(symbA->bloktab[pA].frownum < tli) {
      pA++;
      if (pA> symbA->bcblktab[kA].lbloknum) { 
	symbB->bcblktab[kB].fbloknum = pB;
	goto end2;
      }
    }

    if (pA> symbA->bcblktab[kA].lbloknum) { 
      symbB->bcblktab[kB].fbloknum = pB;
      goto end2;
    }

    assert(pB <= symbB->bloknbr);
    while(symbA->bloktab[pA].frownum <= bri) {
      symbB->bloktab[pB].frownum = symbA->bloktab[pA].frownum;
      symbB->bloktab[pB].lrownum = symbA->bloktab[pA].lrownum;
      symbB->bloktab[pB].cblknum = symbA->bloktab[pA].cblknum;

      pA++; pB++;
      if (pA> symbA->bcblktab[kA].lbloknum) goto end2;
    }
    
  end2:
    symbB->bcblktab[kB].lbloknum = pB-1;
    
  }



}



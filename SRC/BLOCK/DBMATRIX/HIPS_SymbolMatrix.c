/* @authors J. GAIDAMOUR, P. HENON */

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>

#include "symbol.h" /*MatrixStruct*/
#include "solver.h"
#include "phidal_sequential.h"
#include "block.h"

void HIPS_SymbolMatrix(flag_t verbose, int numflag, REAL rat, int_t locally_nbr, int n,  INTL *ia,  dim_t *ja, 
		       PhidalHID *BL, SymbolMatrix *symbmtx, dim_t *perm, dim_t *iperm)
{

  dim_t i, j;
  csptr G;
  csptr P;
  dim_t *tmp;

  dim_t cblknbr_l1, cblknbr, ind;
  MatrixStruct *mtxstr;
  dim_t *rangtab;
  dim_t *treetab;

  /** Convert the matrix in C numbering (start from 0 instead of 1) **/
  if(numflag == 1)
    CSR_Fnum2Cnum(ja, ia, n);



  /**************************************/
  /* Find supernode for each subdomain  */
  /**************************************/
  {
    int *dom2cblktab;

    PHIDAL_GetSupernodes(verbose, 0, n, ia, ja, BL, perm, iperm,
			 &dom2cblktab, &rangtab, &treetab);

    cblknbr_l1 = dom2cblktab[BL->locndom]; 
    free(dom2cblktab);
  }

  /******************************************/
  /* Convert the graph in SparRow structure */
  /******************************************/
  G = (csptr) malloc(sizeof(struct SparRow));
  initCS(G, n);
  tmp = (dim_t *)malloc(sizeof(dim_t)*(n+1)); /*A CHECKER*/
  /**** Convert and permute the matrix in sparrow form  ****/
  /**** The diagonal is not present in the CSR matrix, we have to put it in the matrix ***/
  bzero(tmp, sizeof(dim_t)*n);
  for(i=0;i<n;i++)
    {
      /*** IF THE GRAPH DOES NOT CONTAIN THE DIAGONAL WE ADD IT ***/
      ind = 1;
      for(j=ia[i];j<ia[i+1];j++)
	if(ja[j] == i)
	  ind = 0;

      if(ind == 1)
	{ /** Add the diagonal **/
	  tmp[0] = i; 
	  ind = 1; /** To avoid error later (useless) **/
	}

      for(j=ia[i];j<ia[i+1];j++)
	tmp[ind++] = ja[j];  
      G->nnzrow[i] = ind;
      G->ja[i] = (dim_t *)malloc(sizeof(dim_t)*ind);
      memcpy(G->ja[i], tmp, sizeof(dim_t)*ind);
      G->ma[i] = NULL;

    }


  /******************************************************/
  /* Permute the graph according to the HID permutation */
  /******************************************************/
  CS_Perm(G, perm); /* TODO : a réutiliser après ? */
  sort_row(G);
#ifdef DEBUG_M
  CS_Check(G, n);
#endif

  /*****************************************************/
  /* Apply the symbolic factorization in the LEVEL 0   */
  /*****************************************************/
  P = (csptr)malloc(sizeof(struct SparRow));
  SF_Direct(G, cblknbr_l1, rangtab, treetab, P);

  CS_SetNonZeroRow(G);
  cleanCS(G);
  free(G);

  /*******************************************/
  /* Apply amalgamation in the LEVEL 0       */
  /*******************************************/
  /* TODO : a isoler par #ifdef */

  memcpy(tmp, rangtab, (cblknbr_l1+1)*sizeof(dim_t));

  if(rat >= 0) /* -1 to disable */
    {
      dim_t *iperm2;
      iperm2 = (dim_t *)malloc(sizeof(dim_t)*n);
      amalgamate(rat, n, P, cblknbr_l1, tmp, treetab, &cblknbr_l1, &rangtab, iperm2);
      
      /** Update the permutation **/
      for(i=0;i<n;i++)
	iperm2[i] = iperm[iperm2[i]];
      memcpy(iperm, iperm2, sizeof(dim_t)*n);
      
      free(iperm2);
    }

  for(i=0;i<n;i++)
    perm[iperm[i]] = i;
  
  /*memcpy(BL->iperm, iperm, sizeof(dim_t)*n);
    memcpy(BL->perm, perm, sizeof(dim_t)*n);*/

  /********************************/
  /* Build the symbol matrix      */
  /********************************/
  /* Build a sorted list of intervalls for each column blocks of level 1 */
  cblknbr = cblknbr_l1 + BL->nblock - BL->block_levelindex[1];
  mtxstr = (MatrixStruct*)malloc(sizeof(MatrixStruct)*cblknbr);
  /** Get the MatrixStruct (interval lists instead of list of scalar) **/
  symbolicBlok2(mtxstr, P, cblknbr_l1, rangtab);

  CS_SetNonZeroRow(P);
  cleanCS(P);
  free(P);
  
  /* Symbolic factorization of the interface (LEVEL > 1) */
  if(cblknbr > cblknbr_l1)
    rangtab = (dim_t *)realloc(rangtab, sizeof(dim_t)*(cblknbr+1));
  HID2MatrixStruct(rangtab+cblknbr_l1, mtxstr+cblknbr_l1, BL, 1, locally_nbr);


  /* Build SymbolMatrix structure from mtxstr */ 
  MatrixStruct2SymbolMatrix(symbmtx, mtxstr, cblknbr, rangtab);
  freeMatrixStruct(mtxstr, cblknbr);
  
  /***************************/
  /* Check perm and iperm    */
  /***************************/

  /** Reconvert the matrix in Fortran numbering (start from 0 instead of 1) **/
  if(numflag == 1)
    CSR_Cnum2Fnum(ja, ia, n);


  free(rangtab);
  free(treetab);
  free(tmp);

}

int HIPS_CountNNZ_LOneCol(SymbolMatrix* symbA,
			  dim_t tli, dim_t tlj, dim_t bri, dim_t brj);

#undef printf
void HIPS_CountNNZ_L(flag_t job, SymbolMatrix *symbmtx, PhidalHID *BL) {
  /**************************************************/
  /* If job == 0 then count in each block-column    */
  /* the nnz in the interior domain (level 0)       */
  /* If job == 1 then count in each block-column    */
  /* the nnz in the full coulnum block (included    */
  /* coupling matrix G or W)                        */
  /**************************************************/

  dim_t i;
  int_t levelnum = 0;
  int_t levelnum_l = 1;

  int Mstart   = BL->block_levelindex[levelnum];
  int Mend     = BL->block_levelindex[levelnum+1]-1;

  if (Mend <= Mstart) printfd("Warning Mend / Mstart !\n"); /* si pas plusieurs levels */

  dim_t tlj = Mstart;
  dim_t brj = Mend;

  int jdeb, jlast;

  if(job != 0)
    levelnum_l = BL->nlevel;

  jdeb  = BL->block_levelindex[levelnum]; 
  jlast = BL->block_levelindex[levelnum_l]-1;

#ifdef DEBUG_M
  assert(tlj == 0);
#endif
  BL->block_domwgt = (INTL*)malloc(sizeof(INTL)*(brj+1));


  for(i=tlj; i<=brj; i++) {
    BL->block_domwgt[i] = HIPS_CountNNZ_LOneCol(symbmtx, 
						BL->block_index[jdeb], BL->block_index[i],
						BL->block_index[jlast+1]-1, BL->block_index[i+1]-1);
  }

#ifdef DEBUG_M
  {
    int nnz_tot=0;
    for(i=tlj; i<=brj; i++) {
      nnz_tot+=BL->block_domwgt[i];
      /* printfd("HIPS_CountNNZ : NNZtot : %d\n", nnz_tot); /\* a transformer en un test avec assert sur sum HID + NNZ Matrix *\/ */
    }
  }
#endif
}

int HIPS_CountNNZ_LOneCol(SymbolMatrix* symbA,
			  dim_t tli, dim_t tlj, dim_t bri, dim_t brj) { 
  dim_t cblktlj, cblkbrj;
  int kA, pA;
  dim_t c;
  int nnz=0; 

  /*** cblktlj and cblkbrj ***/
  /* A ne pas recalculer, ca se suit */
  kA=0;
  while(symbA->ccblktab[kA].fcolnum < tlj) {
    kA++;
    assert(kA<symbA->cblknbr); /*TODO*/
  }
  cblktlj = kA;

  while(symbA->ccblktab[kA].fcolnum <= brj) {
    kA++;
    if(kA>=symbA->cblknbr) { /*TODO : le kA++ apres*/
      goto end1;
    };
  }
 end1:
  cblkbrj = kA-1;

  /* blocknbr */
  for(kA=cblktlj; kA<=cblkbrj; kA++) {
    c = symbA->ccblktab[kA].lcolnum - symbA->ccblktab[kA].fcolnum +1;
    pA=symbA->bcblktab[kA].fbloknum;
 
    if (pA> symbA->bcblktab[kA].lbloknum) goto end; 

    while(symbA->bloktab[pA].frownum < tli) {
      pA++;
      if (pA> symbA->bcblktab[kA].lbloknum) goto end;
    }

    if (pA> symbA->bcblktab[kA].lbloknum) goto end; /*TODO : simplifier*/

    while(symbA->bloktab[pA].frownum <= bri) {
      nnz += c*(symbA->bloktab[pA].lrownum - symbA->bloktab[pA].frownum +1);

      pA++;
      if (pA> symbA->bcblktab[kA].lbloknum) goto end;
    }
     
  end:
    ;
  }

  /* A rapprocher de DBMatrix_NNZ */
  /* Remove upper part of diagonal block (+ diagonal) */
  for(kA=cblktlj; kA<=cblkbrj; kA++) {
    c = symbA->ccblktab[kA].lcolnum - symbA->ccblktab[kA].fcolnum +1;
    nnz -= (c*(c+1))/2 /* ordre important */;
    nnz += c; /* diag */
  }

  return nnz;

}

int HIPS_CountNNZ_EOneCol(csptr G,
			  int noused_tli, dim_t tlj, int not_used_bri, dim_t brj);

void HIPS_CountNNZ_E(dim_t n,  INTL *ia,  dim_t *ja, dim_t *perm, PhidalHID *BL) {
  int i,j/* ,k */;
  
  csptr G;
  dim_t *tmp;
  int ind;
  /******************************************/
  /* Convert the graph in SparRow structure */
  /******************************************/
  G = (csptr) malloc(sizeof(struct SparRow));
  initCS(G, n);
  tmp = (dim_t *)malloc(sizeof(dim_t)*(n+1)); /*A CHECKER*/
  /**** Convert and permute the matrix in sparrow form  ****/
  /**** The diagonal is not present in the CSR matrix, we have to put it in the matrix ***/
  bzero(tmp, sizeof(dim_t)*n);
  for(i=0;i<n;i++)
    {
      /*** IF THE GRAPH DOES NOT CONTAIN THE DIAGONAL WE ADD IT ***/
      ind = 1;
      for(j=ia[i];j<ia[i+1];j++)
	if(ja[j] == i)
	  ind = 0;

      if(ind == 1)
	{ /** Add the diagonal **/
	  tmp[0] = i; 
	  ind = 1; /** To avoid error later (useless) **/
	}

      for(j=ia[i];j<ia[i+1];j++)
	tmp[ind++] = ja[j];  
      G->nnzrow[i] = ind;
      G->ja[i] = (dim_t *)malloc(sizeof(dim_t)*ind);
      memcpy(G->ja[i], tmp, sizeof(dim_t)*ind);
      G->ma[i] = NULL;

    }
  free(tmp);

  /******************************************************/
  /* Permute the graph according to the HID permutation */
  /******************************************************/
  CS_Perm(G, perm); /* TODO : a réutiliser après ? */
  sort_row(G);
#ifdef DEBUG_M
  CS_Check(G, n);
#endif

  CS_SetNonZeroRow(G);

  int_t levelnum = 0;
  int Mstart   =  BL->block_levelindex[levelnum];
  int Mend     =  BL->block_levelindex[levelnum+1]-1;
/*   int Sstart   =  BL->block_levelindex[levelnum+1]; */
/*   int Send     =  BL->nblock-1;  */

  dim_t tlj = Mstart;
  dim_t brj = Mend;

  int jdeb  = BL->block_levelindex[levelnum+1];
  int jlast = BL->block_levelindex[BL->nlevel]-1;

  for(i=tlj; i<=brj; i++) {
    BL->block_domwgt[i] += HIPS_CountNNZ_EOneCol(G, 
						 BL->block_index[jdeb], BL->block_index[i],
						 BL->block_index[jlast+1]-1, BL->block_index[i+1]-1);
  }
 
#ifdef DEBUG_M
  {
    int nnz_tot=0;
    assert(Mend+1 == brj-tlj+1);
    for(i=0; i<=Mend; i++) {
      nnz_tot+=BL->block_domwgt[i];
    }
    
    /* nnz_tot == NNZ de E  : printfd("P->E %ld\n", PhidalMatrix_NNZ(P->E)); dans DMBATRIX_MLILUCCPrec */
    /* printfd("HIPS_CountNNZ_E : NNZtot : %d / (%d ou %d)\n", nnz_tot, CSnnz(G), ia[n]-1); */
  }
#endif

 cleanCS(G);
 free(G);
 
}



int HIPS_CountNNZ_EOneCol(csptr G,
			  dim_t tli, dim_t tlj, int not_used_bri, dim_t brj) { 
  /* bri : not used : pas de limite max : tous les niveaux après tli */

  dim_t i,k;
  dim_t *ja;
  int nnz=0;

  for(i=tlj;i<=brj;i++) /* <= ! */
    {
      ja = G->ja[i];
      for(k=0;k<G->nnzrow[i];k++)
	{
	  /* if (ja[k] >= tli) nnz++; /\* si non trier *\/ */
			      
	  /* si c'est trier, + simple : */
	  if (ja[k] >= tli) {
	    nnz += G->nnzrow[i]-k;
	    break;
	  }	      

	}
    }
  
  return nnz;
  
}

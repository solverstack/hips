/* @authors J. GAIDAMOUR */

#include <stdio.h>
#include <stdlib.h>

#include "block.h"

/* void copy(int size, COEF* a, int sa, COEF* b, int sb) { */
/*   dim_t i; */

/*   COEF* c = (COEF*)malloc(size*sizeof(COEF)); */
  
/*   printfv(5, "sizeofCOEF=%d\n", sizeof(COEF)); */

/*   for(i=0; i< size-1; i++) { */
/*         printfv(5, "i=%d/%d\n",i, size); */
/*     c[i] = a[i]; */
/*     b[i] = a[i]; */
/*   } */
  
/*   free(c); */
/* } */


void DBLsolv(flag_t unitdiag, DBMatrix *L, COEF *b, COEF *x);
void DBUsolv(flag_t unitdiag, DBMatrix *U, COEF *b, COEF *x);

void DBLsolv2(flag_t unitdiag, DBMatrix *L, COEF *b, COEF *x);
void DBUsolv2(flag_t unitdiag, DBMatrix *U, COEF *b, COEF *x);


/*Version 1 : ne marche pas avec une alloc par morceaux*/

void DBMatrix_Lsolv(flag_t unitdiag, DBMatrix *L, COEF *x, COEF *b, PhidalHID *BL) {
  /* Lsolv(unitdiag, &(L->solvmtx), b, x); /\*\TODO : les args sont inversé entre Lsolv et PhidalSequential*\/ */

  if (L->alloc != BLK)
    DBLsolv(unitdiag, L, b, x); /*TODO : les args sont inversé entre Lsolv et PhidalSequential*/
  else 
    DBLsolv2(unitdiag, L, b, x); /*TODO : les args sont inversé entre Lsolv et PhidalSequential*/
}

#ifdef WITH_PASTIX
void Solve_with_pastix(DBMatrix *L, COEF *b, COEF *x) {
  int i, ilast, cpt_col;
  int UN = 1;
  VSolverMatrix* csL;

  if (x!=b)
    BLAS_COPY(L->nodenbr, b, UN, x, UN);

  ilast = L->brj - L->tlj + 1;
  cpt_col = 0;
  
  for(i=0;i<ilast;i++) 
    {
      csL =  L->ca[ L->cia[L->tlj+i] ];
      pastix_solve(L->pastix_str+i, x + cpt_col);
      cpt_col += (&csL->symbmtx)->nodenbr;
    }
}
#endif // WITH_PASTIX

void DBLsolv(flag_t unitdiag, DBMatrix *L, COEF *b, COEF *x) {
  int i,k,p,p2;
  int /* j, */jxtmp;

  SolverMatrix* solvmtx;
  SymbolMatrix* symbmtx;
  int ilast;
  dim_t tli = L->a[0].symbmtx.tli;

  COEF* xptr;
  int height, width;
  blas_t stride;

  COEF* xtmp;
  int size;

  COEF minusone= -1.0, zero=0.0, one=1.0;
  char *uploL = "L";
  char *transN = "N";
  char* diag = unitdiag?"U":"N";
  int UN = 1;
  assert((unitdiag == 0) || (unitdiag == 1));

  if (x!=b)
    BLAS_COPY(L->nodenbr, b, UN, x, UN);

  /* vecteur tmp */
  xtmp = (COEF*)malloc(sizeof(COEF)*L->nodenbr);

  assert((L->alloc == ONE) || (L->alloc == CBLK));
  if (L->alloc == ONE) ilast = 1;
  else ilast = L->brj - L->tlj + 1;

  /* parcours par blocs colonnes */
  for(i=0;i<ilast;i++) {
    solvmtx = &L->a[i];//L->ca[L->cia[i+L->tlj]]->solvmtx;// 
    symbmtx = &solvmtx->symbmtx;

    /*   printfv(5, "%d %d\n", symbmtx->tli, fcolnum); */
    /*     assert(symbmtx->tli ==  fcolnum); */

    for(k=0; k<symbmtx->cblknbr; k++) {
      width  = symbmtx->ccblktab[k].lcolnum - symbmtx->ccblktab[k].fcolnum +1;

      height = symbmtx->hdim[k] - width; /** Hauteur de la surface compactÃ©e des bloc extra-diagonaux **/
      stride = symbmtx->stride[k];
      p      = symbmtx->bcblktab[k].fbloknum;        /* triangular block */ assert(p<symbmtx->bloknbr);
      xptr   = x + symbmtx->ccblktab[k].fcolnum - tli;
      
      /* résolution triangulaire */
      /*FIXME : y a un bug */
      BLAS_TRSV(uploL, transN, diag, width /*=h*/,
	    solvmtx->coeftab + solvmtx->bloktab[p].coefind, stride, xptr, UN);

      /*       if (/\* (i==L->brj) && *\/ (k == symbmtx->cblknbr - 1)) {       */
      if (p == symbmtx->bcblktab[k].lbloknum)
	continue; /* TODO : a reporter sur desc2.c*/

      /* calcul des contributions (aggrégation des colonnes) */
      BLAS_GEMV(transN, height, width, minusone /*coef alpha*/, 
	    solvmtx->coeftab + solvmtx->bloktab[p+1].coefind, stride, 
	    xptr, UN,       zero /*coef beta*/, 
	    xtmp, UN);
      
      /* x := xtmp (out) */
      for(p2=p+1, jxtmp=0; p2<=symbmtx->bcblktab[k].lbloknum; p2++) {
	size = symbmtx->bloktab[p2].lrownum - symbmtx->bloktab[p2].frownum +1;
	BLAS_AXPY(size, one, xtmp + jxtmp, UN, x + symbmtx->bloktab[p2].frownum - tli, UN);
	jxtmp += size;
      }
      
    }
  }
  free(xtmp);  
}

void DBMatrix_Usolv(flag_t unitdiag, DBMatrix *U, COEF *x, COEF *b, PhidalHID *BL) {
  /*   if (U->alloc == ONE) */
  /*     Usolv(unitdiag, &(U->solvmtx), b, x); */
  /* else  */
  if (U->alloc != BLK)
    DBUsolv(unitdiag, U, b, x);
  else 
    DBUsolv2(unitdiag, U, b, x);
}

void DBUsolv(flag_t unitdiag, DBMatrix *U, COEF *b, COEF *x) {
  int i, k,p,p2;
  int /* j, */jxtmp;

  SolverMatrix* solvmtx;
  SymbolMatrix* symbmtx;
  int ilast;
  dim_t tli = U->a[0].symbmtx.tli;

  COEF* xptr;
  int height, width, stride;

  COEF* xtmp;
  int size;

  COEF minusone= -1.0, one=1.0;
  char *uploL = "L";
  char *transT = "T";
  char* diag = unitdiag?"U":"N";
  int UN = 1;
  assert((unitdiag == 0) || (unitdiag == 1));

  if (x!=b)
    BLAS_COPY(U->nodenbr, b, UN, x, UN);

  /* vecteur tmp */
  xtmp = (COEF*)malloc(sizeof(COEF)*U->nodenbr);

  /*   assert((U->alloc == ONE) || (U->alloc == RBLK)); TODO Symm/Unsym */
  if (U->alloc == ONE) ilast = 1;
  else ilast = U->brj - U->tlj + 1;

  /* parcours par blocs colonnes */
  for(i=ilast-1;i>=0;i--) {                     /*todo : -1 */
    solvmtx = &U->a[i];
    symbmtx = &solvmtx->symbmtx;
    
    for(k=symbmtx->cblknbr-1; k>=0; k--) {
      
      width  = symbmtx->ccblktab[k].lcolnum - symbmtx->ccblktab[k].fcolnum +1;
      height = symbmtx->hdim[k] - width; /** Hauteur de la surface compactÃ©e des bloc extra-diagonaux **/
      stride = symbmtx->stride[k];
      p    = symbmtx->bcblktab[k].fbloknum;        /* triangular block */

      xptr = x + symbmtx->ccblktab[k].fcolnum - tli;
      
      if (p != symbmtx->bcblktab[k].lbloknum) {
	
	/* xtmp := x (in) */
	for(p2=p+1, jxtmp=0; p2<=symbmtx->bcblktab[k].lbloknum; p2++) {
	  size = symbmtx->bloktab[p2].lrownum - symbmtx->bloktab[p2].frownum +1;
	  BLAS_COPY(size, x + symbmtx->bloktab[p2].frownum - tli, UN, xtmp + jxtmp, UN);
	  jxtmp += size; /* todo : remplacer int jxtmp par int*, evite 1 operation  + x+ symbmtx aussi?*/
	}
	
	/* calcul des contributions (aggrégation des colonnes) */
	BLAS_GEMV(transT, height, width, minusone /*coef alpha*/,
		  solvmtx->coeftab + solvmtx->bloktab[p+1].coefind, stride,
		  xtmp, UN,       one /*coef beta*/,
		  xptr, UN);
	
      }
      
      /* résolution triangulaire */
      BLAS_TRSV(uploL, transT, diag, width /*=h*/,
	    solvmtx->coeftab + solvmtx->bloktab[p].coefind, stride, xptr, UN);
      
    }
    
  }
  free(xtmp);
}

void DBMatrix_Dsolv(DBMatrix *D, COEF* b) {
#ifdef DEBUG_NOALLOCATION
  return;
#endif

  if (D->alloc == BLK) {
    DBMatrix_Dsolv2(D,b);
    return;
  }

  /*   Dsolv(&(D->solvmtx), b); */

  SolverMatrix* solvmtx;
  SymbolMatrix* symbmtx;
  int k,i, ilast, ii, iiglobal;
  blas_t stride;
  dim_t p;
  COEF* ptr;
  
  assert((D->alloc == ONE) || (D->alloc == CBLK));
  if (D->alloc == ONE) ilast = 1;
  else ilast = D->brj - D->tlj + 1;

  /* parcours par blocs colonnes */
  iiglobal=0;
  for(i=0;i<ilast;i++) {
    solvmtx = &D->a[i];
    symbmtx = &solvmtx->symbmtx;
    
    for(k=0; k<symbmtx->cblknbr; k++) {  
      p      = symbmtx->bcblktab[k].fbloknum;        /* triangular block */
      ptr    = solvmtx->coeftab + solvmtx->bloktab[p].coefind;
      stride = symbmtx->stride[k];

      for (ii=symbmtx->ccblktab[k].fcolnum; ii<=symbmtx->ccblktab[k].lcolnum; ii++, iiglobal++) { /* TODO : nom de variables */
	b[iiglobal] /= ptr[0];
	ptr += stride + 1;
      }
    }
  }

}

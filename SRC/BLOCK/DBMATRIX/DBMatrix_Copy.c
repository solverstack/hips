/* @authors J. GAIDAMOUR */

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include "block.h"

#include <string.h> /* memset */

/* todo : remplissage de coefind faisable au moment du parcours et remplissage effectif ? */

/* todo : faire une fonction commune avec MatrixStruct2... pour */

void DBMatrix_Fill(char* UPLO, PhidalMatrix* P, DBMatrix* m, /* char* TRANS, */ PhidalHID* BL, int_t levelnum, int_t levelnum_l);

void DBMatrix_Copy(PhidalMatrix* mat, PhidalHID* BL, 
		   PhidalMatrix* Phidal, DBMatrix* DBM,
		   char* UPLO, int_t levelnum, int_t levelnum_l, flag_t fill) {

  dim_t tli = DBM->tli;
  dim_t tlj = DBM->tlj;
 
  PhidalMatrix* PhidalTmp;
    
  if (Phidal != NULL) 
    PhidalTmp = Phidal;
  else 
    PhidalTmp = (PhidalMatrix *)malloc(sizeof(PhidalMatrix));
  
  if (strcmp(UPLO, "U") == 0)/* U */
    PhidalMatrix_Transpose(mat);

  /* 2x en unsym .?*/  
  PhidalMatrix_BuildVirtualMatrix(DBM->tli, DBM->tlj, DBM->bri, DBM->brj, mat, PhidalTmp, BL); 
  /*utile ?*/ /* A deplacer pour fill == 0 */
  
  if (strcmp(UPLO, "U") == 0)/* U */
    PhidalMatrix_Transpose(mat);
  
  /* TODO */
  if (fill == BEFORE) {
    /*     char* TRANS; */
    if ((DBM->symmetric == 0) && (DBM->tli>DBM->tlj)) {
      assert(DBM->bri>DBM->brj);
    }
    
    /* if (DBM->symmetric == 1) { /\* printfv(5, "** Sym\n"); *\/ TRANS = "N"; } */
    
    if (DBM->symmetric == 0) {
      if ((strcmp(UPLO, "U") == 0)/* U */ || (tli<tlj) /* F */) {
	/* 	 TRANS = "N"; */
	
	if (tli<tlj)
	  PhidalMatrix_Transpose(PhidalTmp);
	
	DBMatrix_Transpose(DBM);
      } else { 
	/* 	 TRANS = "T";  */
	PhidalMatrix_Transpose_SparMat(PhidalTmp, /*job*/1, "N", BL); /* Transposer ou copier ? ==> copier  si c pour converser */
      }
    }
    
    DBMatrix_Fill(UPLO, PhidalTmp, DBM, /* TRANS, */ BL, levelnum, levelnum_l); /* a mettre ds MLICCTPrec ? */
    
    if (DBM->symmetric == 0) {
      if ((strcmp(UPLO, "U") == 0)/* U */ || (tli<tlj) /* F */) {

	if (tli<tlj)
	  PhidalMatrix_Transpose(PhidalTmp);
	
	DBMatrix_Transpose(DBM);
      } else {
	/* 	  if (Phidal != NULL) { */ /*TODO*/
	PhidalMatrix_Transpose(PhidalTmp);
	PhidalMatrix_Transpose_SparMat(PhidalTmp, /*job*/1, "N", BL);
	PhidalMatrix_Transpose(PhidalTmp);
	/* 	  } */
      }
    }
  }

  if (Phidal == NULL) {
    PhidalMatrix_Clean(PhidalTmp);
    free(PhidalTmp);
  }

}

void DBMatrix_Fill(char* UPLO, PhidalMatrix* P, DBMatrix* m, /* char* TRANS, */ PhidalHID* BL, int_t levelnum, int_t levelnum_l) {
  SolverMatrix* solver=NULL;

  int tl, br;
  INTL *ia; dim_t *ja;
  VSolverMatrix **a;
  INTL *Pia;
  dim_t *Pja;
  struct SparRow **Pa;
  
  if ((m->alloc == ONE) || (m->alloc == CBLK) || (m->alloc == BLK)) {
    tl = m->tlj;
    br = m->brj;
    ia = m->cia;
    ja = m->cja;
    a  = m->ca;

    Pia = P->cia;
    Pja = P->cja;
    Pa  = P->ca;
 
  } else {
    assert(m->alloc == RBLK);

    tl = m->tli;
    br = m->bri;
    ia = m->ria;
    ja = m->rja;
    a  = m->ra;

    Pia = P->ria;
    Pja = P->rja;
    Pa  = P->ra;
  }
  
  /* TODO : refactoriser */
  if (m->alloc == ONE) {
    int i0, i;
    solver = &m->a[0];
    DBMatrix_Symb2SolvMtx(solver);    
    
    for(i0=0,i=tl;i<=br;i0++,i++) { /* i = indice for dbmatrix and P */
      DBMatrix_AllocSymbmtx(i0, i, m, m->alloc, ia, ja, a, Pia, Pja, Pa, solver, "N",/* TRANS, */ BL);
    }
    
  } else {
    int i0, i;

    for(i0=0,i=tl;i<=br;i0++,i++) { /* i = indice for dbmatrix and P */
      if (m->alloc != BLK) {
	solver = &m->a[i0];
	DBMatrix_Symb2SolvMtx(solver);
      } else {
	dim_t j;
	for(j=ia[i];j<ia[i+1];j++) {
	  solver = a[j]->solvmtx; /* ne pas utiliser m->a[j] */
	  DBMatrix_Symb2SolvMtx(solver);
	}
      }
      DBMatrix_AllocSymbmtx(i0, i, m, m->alloc, ia, ja, a, Pia, Pja, Pa, solver, "N",/* TRANS, */ BL);
    }

  }
  
}

/* unused  : i0 */
void DBMatrix_AllocSymbmtx(int i0, int i, DBMatrix* m, alloc_t alloc,
			   INTL *ia, dim_t *ja, VSolverMatrix **a,
			   INTL *Pia, dim_t *Pja, struct SparRow **Pa, 
			   SolverMatrix* solver, char* TRANS, PhidalHID* BL)
{
#ifdef DEBUG_NOALLOCATION
  return;
#endif

  int j, j2;
  dim_t n=-1;
  int nn;

  SymbolMatrix* SM;

  j  =  ia[i];
  j2 = Pia[i]; 

  while(j<ia[i+1] && j2<Pia[i+1]) {
    if (ja[j] < Pja[j2]) { j++;  continue; }
    if (ja[j] > Pja[j2]) { j2++; continue; }
    
    assert(ja[j] == Pja[j2]);
    
    SM = &a[j]->symbmtx;

     if (strcmp(TRANS, "T") == 0)
       {
	 nn = SM->ccblktab[SM->cblknbr-1].lcolnum - SM->ccblktab[0].fcolnum +1;
	 n = Pa[j2]->n;
	 CS_Transpose(1, Pa[j2], nn);
	 
	 assert(BL->block_index[i+1] - BL->block_index[i] == nn);
	 
       }
     
     if ((m->alloc == ONE) || (m->alloc == CBLK) || (m->alloc == BLK)) {
       if (m->alloc == BLK) {
	 solver = a[j]->solvmtx; /* ne pas utiliser m->a[j] */
	 /* 	 solver = &m->a[j]; /\* pas joli *\/ */
       }
       
       SparRow2SolverMatrix(BL->block_index[i], BL->block_index[ja[j]], Pa[j2], SM, solver);
     } else if (alloc == RBLK) {
       SparRow2SolverMatrix(BL->block_index[ja[j]], BL->block_index[i], Pa[j2], SM, solver);
     }
     
     if (strcmp(TRANS, "T") == 0) {
       CS_Transpose(1, Pa[j2], n);	 /* TODO : a ne pas faire 2 fois, faire une copie */
     }

    j++;
    j2++;
  }
    
}


/* autre possiblité pour le remplissage : parcours ds le meme sens pour CBLK et RBLK, , mais mieux de travailler sur la meme solver, et puis c zoli*/

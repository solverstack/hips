/* @authors J. GAIDAMOUR */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>

#include "phidal_sequential.h"
#include "block.h"
#include "prec.h"

/*int PHIDAL_Fgmresd2(flag_t verbose, REAL tol, int_t itmax, PhidalMatrix *A, DBPrec *P2, PhidalPrec *P, PhidalHID *BL, 
  PhidalOptions *option, COEF *rhs, COEF * x, FILE *fp);*/

/** Local functions **/
INTS DBPrec_SolveForward(REAL tol, int_t itmax, DBPrec *P, COEF *x, COEF *b, PhidalHID *BL, PhidalOptions *option, dim_t *itertab, REAL *resnormtab);

INTS DBMATRIX_PrecSolve(REAL tol, DBPrec *P, COEF *x, COEF *b, PhidalHID *BL, PhidalOptions *option, dim_t *itertab, REAL *resnormtab)
{
  DBMatrix *L, *U;
  int_t itmax;
  dim_t i;

  L = PREC_L_BLOCK(P);
  U = PREC_U_BLOCK(P);

  if(P->forwardlev == 0)
    {
      DBMatrix_Lsolv(1, L, x, b, BL);
      
      if(P->symmetric == 1)
	DBMatrix_Dsolv(L, x);

      DBMatrix_Usolv(P->symmetric, U, x, x, BL);
    } else {
      itmax = option->itmaxforw;
      for(i=P->forwardlev; i<option->forwardlev;i++)
	itmax = (int)(itmax*option->itforwrat);

      CHECK_RETURN(DBPrec_SolveForward(tol, itmax, P, x, b, BL, option, itertab, resnormtab));
    }
  return HIPS_SUCCESS;
}
/* TODO : pivoting, /\** permute back x **\/ */

INTS DBPrec_SolveForward(REAL tol, int_t itmax, DBPrec *P, COEF *x, COEF *b, PhidalHID *BL, PhidalOptions *option, dim_t *itertab, REAL *resnormtab)
{
  int UN=1;
  int dim;

  DBMatrix *L, *U;
  DBMatrix *SL, *SU;
  PhidalMatrix *E, *F;
  COEF *y;

  REAL tolrat;

  dim_t g;

  L = PREC_L_BLOCK(P);
  E = PREC_E(P);
  U = PREC_U_BLOCK(P);
  F = PREC_F(P); 
  SL = PREC_SL_BLOCK(P);
  SU = PREC_SU_BLOCK(P);

  y = (COEF *)malloc(sizeof(COEF)*P->dim);
  memcpy(y, b, sizeof(COEF)*P->dim);
  
  /*** Restriction operation ***/

  DBMatrix_Lsolv(1, L, x, b, BL);
  
  if(P->symmetric == 1) 
    DBMatrix_Dsolv(L, x);

  DBMatrix_Usolv(P->symmetric, U, x, x, BL);
  
  PHIDAL_MatVecSub(E, BL, x, y+L->dim1);

#ifdef DEBUG_M
  if ((SL != 0) && (P->schur_method == 1))
    assert(SL->dim1 == BL->n - BL->block_index[BL->block_levelindex[P->levelnum]]-L->dim1);
#endif
    
  dim = P->dim - L->dim1;
  tolrat = BLAS_NRM2(P->dim,b,UN) / BLAS_NRM2(dim, y+L->dim1, UN); 
  
  if(option->schur_method == 0 || itmax <= 1)
    {
      if (P->nextprec != NULL)
	{
	  CHECK_RETURN(DBMATRIX_PrecSolve(tol*tolrat, P->nextprec, x+L->dim1, y+L->dim1, BL, option, itertab, resnormtab));
	}
      else
	{
	  CHECK_RETURN(PHIDAL_PrecSolve(tol*tolrat, P->phidalPrec, x+L->dim1, y+L->dim1, BL, option, itertab, resnormtab));
	}
    }
  else
    {
      int iter=-1;
      flag_t verbose = (option->verbose >= 4)?2:0;
      if (option->krylov_method != 1)
	{
	  if (P->nextprec != NULL)
	    {
	      if(P->schur_method == 2)
		{
		  CHECK_RETURN(HIPS_Fgmresd_DB(verbose, tol*tolrat, itmax, P->nextprec, BL, 
					       option, y+L->dim1, x+L->dim1, stdout, itertab, resnormtab));
		}
	      else /* (P->schur_method == 1) */
		{
		  CHECK_RETURN(HIPS_Fgmresd_DB_DB(verbose, tol*tolrat, itmax, SL, SU, P->nextprec, BL, 
						  option, y+L->dim1, x+L->dim1, stdout, itertab, resnormtab));
		}
	    } else {
	      
	      if(P->schur_method == 2)
		{
		  CHECK_RETURN(HIPS_Fgmresd_DB_PH(verbose, tol*tolrat, itmax, P, P->phidalPrec, BL,
						  option, y+L->dim1, x+L->dim1, stdout, itertab, resnormtab));
		}
	      else /* (P->schur_method == 1) */
		{
		  CHECK_RETURN(HIPS_Fgmresd_PH_PH(verbose, tol*tolrat, itmax, P->phidalS, P->phidalPrec, BL, 
						  option, y+L->dim1, x+L->dim1, stdout, itertab, resnormtab));
		}
	    }
	}
      else
	{
	  if (P->nextprec != NULL)
	    {
	      if(P->schur_method == 2)
		{
		  CHECK_RETURN(HIPS_PCG_DB(verbose, tol*tolrat, itmax, P->nextprec, BL, 
					   option, y+L->dim1, x+L->dim1, stdout, itertab, resnormtab));
		}
	      
	      else /* (P->schur_method == 1) */
		{
		  CHECK_RETURN(HIPS_PCG_DB_DB(verbose, tol*tolrat, itmax, SL, SU, P->nextprec, BL, 
					      option, y+L->dim1, x+L->dim1, stdout, itertab, resnormtab));
		}
	    } else {

	      if(P->schur_method == 2)
		{
		  CHECK_RETURN(HIPS_PCG_DB_PH(verbose, tol*tolrat, itmax, P, P->phidalPrec, BL,
					      option, y+L->dim1, x+L->dim1, stdout, itertab, resnormtab));
		}
	      else /* (P->schur_method == 1) */
		{
		  CHECK_RETURN(HIPS_PCG_PH_PH(verbose, tol*tolrat, itmax, P->phidalS, P->phidalPrec, BL, 
					      option, y+L->dim1, x+L->dim1, stdout, itertab, resnormtab));
		}
	    }
	}
 
      /* if(option->verbose >= 3) */
	{
	  for(g=0;g<P->levelnum+1;g++)
	    fprintfv(1, stdout, "    ");
	  fprintfv(1, stdout, "Level %ld : inner iterations = %ld \n", (long)(P->levelnum+1),(long)iter);
	}

    }

  /*** Prolongation operation ***/
  PHIDAL_MatVecSub(F, BL, x+L->dim1, y); 

  DBMatrix_Lsolv(1, L, x, y, BL);

  if(P->symmetric == 1) 
    DBMatrix_Dsolv(L, x);

  DBMatrix_Usolv(P->symmetric, U, x, x, BL);

  free(y);
  return HIPS_SUCCESS;

}

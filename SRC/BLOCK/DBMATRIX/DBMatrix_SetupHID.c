/* @authors J. GAIDAMOUR, P. HENON */

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h> /* strcmp */

/* TODO : factoriser avec une fonction Sparow2Csptr*/
/* TODO : mettre totalement à part les fonctions parallèles, pas mélanger */

#include "type.h"

int hazardous(int n, int seed);

#if defined(PH)
/* TODO : simplifer include */
#include "phidal_common.h"
#include "phidal_struct.h"
#include "macro.h"

enum virtual {NOVIRTUAL=0, VIRTUAL=1, COPY=2}; /* tmp */

#define FUNC  PhidalMatrix_Setup2
#define TYPE  PhidalMatrix
#define TYPE2 struct SparRow

#elif defined(DB)
#include "block.h"

#define FUNC  DBMatrix_Setup_HID
#define TYPE  DBMatrix
#define TYPE2 VSolverMatrix

#elif defined(DPH)
#include <mpi.h>
#include "db_parallel.h"
#include "block.h"
#define PARALLEL

#define FUNC  PhidalDistrMatrix_Setup
#define TYPE  PhidalDistrMatrix
#define TYPES PhidalMatrix
#define TYPE2 struct SparRow

#elif defined(DDB)
#include "block.h"
#include "db_parallel.h"
#include "phidal_parallel.h"
#define PARALLEL

#define FUNC  DBDistrMatrix_Setup_HID
#define TYPE  DBDistrMatrix
#define TYPES DBMatrix
#define TYPE2 VSolverMatrix

#else

#error macro undefined

#endif

#ifndef PARALLEL
#define HID PhidalHID
#else
#define HID PhidalDistrHID
void CMD(TYPE,Compute_rind)  (TYPE* DM, csptr P, PhidalDistrHID* DBL, dim_t *key, int *tmp, int *pt_tkl);
void CMD(TYPE,Compute_cind)  (TYPE* DM, csptr P, /* PhidalDistrHID* DBL, */ int *tmp);
void CMD(TYPE,Compute_leader)(TYPE* DM, csptr P, PhidalDistrHID* DBL, int tkl, dim_t *key);
#endif

/* TODO : utiliser CMD(,)*/
/* TODO : factoriser avec une fonction Sparow2Csptr*/
void FUNC(dim_t tli, dim_t tlj, dim_t bri, dim_t brj, char *UPLO, char *DIAG, int_t locally_nbr, TYPE *DM, HID *DBL) {
  dim_t i, j;
  csptr P;
  long nnz;
  int ind;

#ifndef PARALLEL 
  TYPE *M = DM; /* pas bo */
  PhidalHID *BL = DBL;
#else
  int tkl;
  dim_t *key;
  int *tmp;

  TYPES *M = &(DM->M);
  PhidalHID *BL = &(DBL->LHID);
#endif

#if defined(PH)
  PhidalMatrix_Init(DM);
#elif defined(DPH)
  PhidalDistrMatrix_Init(DM);
#elif defined(DDB)
/*   DBDistrMatrix_Init(DM); non, réinitialise le flag symmetric */
#endif

#ifdef PARALLEL 
  DM->proc_id = DBL->proc_id;
  DM->nproc = DBL->nproc;
#endif

#if defined(DB) || defined(DDB)
  M->csc = 1;
#endif
  M->virtual = NOVIRTUAL; /* ou ds _Copy ? ou dans Init */

  if(bri < 0)
    bri = BL->nblock-1;
  if(brj < 0)
    brj = BL->nblock-1;
  
  if(tli<0)
    tli = 0;
  if(tlj<0)
    tlj = 0;
#ifdef DEBUG_M
  assert(bri < BL->nblock);
  assert(brj < BL->nblock);
  assert(tli <= bri);
  assert(tlj <= brj);
#endif

  M->tli = tli;
  M->tlj = tlj;
  M->bri = bri;
  M->brj = brj;
  
  M->dim1 = BL->block_index[bri+1]-BL->block_index[tli]; 
  M->dim2 = BL->block_index[brj+1]-BL->block_index[tlj]; 
  
  if(locally_nbr > BL->nlevel-1 || locally_nbr < 0)
    locally_nbr = BL->nlevel-1;

  /*todo : fait deux fois pour S!*/
  /*todo : fait deux fois si PhidalSetup appelé avec locallynbr = option*/
  /*****************************************/
  /*** Compute the sparse block pattern ****/
  /*****************************************/
  P = (csptr)malloc(sizeof(struct SparRow));
  initCS(P, BL->nblock);
  
  phidal_block_pattern(tli, tlj, bri, brj, UPLO, DIAG, P, locally_nbr, BL);
  nnz = CSnnz(P);

  M->bloknbr = (int)nnz;
  if(M->bloknbr > 0) {
    M->bloktab = (TYPE2*)malloc(sizeof(TYPE2) * M->bloknbr);
#if defined(PH) || defined(DPH)
    for(i=0;i<M->bloknbr;i++)
      initCS(M->bloktab+i, 0);
#endif
  } else {
    M->bloktab = NULL;		       
  }

  M->ria = (INTL *)malloc(sizeof(INTL)* (BL->nblock+1));
  M->rja = (dim_t *)malloc(sizeof(dim_t)* (M->bloknbr));

  cs2csr(P, M->ria, M->rja, NULL);
  M->ra = (TYPE2 **)malloc(sizeof(TYPE2*)* (M->bloknbr));
  ind = 0;
  for(i=0;i<P->n;i++)
    for(j=0;j<P->nnzrow[i];j++)
      M->ra[ind++] = M->bloktab + (int)P->ma[i][j];

#ifdef PARALLEL
  /*** Compute rind ***/
  DM->rind = (INTL *)malloc(sizeof(INTL)* (M->bloknbr));
  tmp  = (int *)malloc(sizeof(int)* (M->bloknbr));
  key  = (dim_t *)malloc(sizeof(dim_t)* (DBL->nproc)); /* is freed in Compute_leader */

  CMD(TYPE,Compute_rind)(DM, P, DBL, key, tmp, &tkl);
  /*** ***/
#endif

  CS_Transpose(1, P, P->n);
  M->cia = (INTL *)malloc(sizeof(INTL)* (BL->nblock+1));
  M->cja = (dim_t *)malloc(sizeof(dim_t)* (M->bloknbr));

  cs2csr(P, M->cia, M->cja, NULL);
  M->ca = (TYPE2 **)malloc(sizeof(TYPE2*)* (M->bloknbr));
  /*   printfv(5, "(M->bloknbr) %d\n",(M->bloknbr)); */
  ind = 0;
  for(i=0;i<P->n;i++)
    for(j=0;j<P->nnzrow[i];j++)
      M->ca[ind++] = M->bloktab + (int)P->ma[i][j];

#ifdef PARALLEL
  /*** Compute cind ***/
  DM->cind = (INTL *)malloc(sizeof(INTL)* (M->bloknbr));
  CMD(TYPE,Compute_cind)(DM, P, tmp);
  /*** ***/
  free(tmp);
#endif

  cleanCS(P);
  free(P);

#ifdef PARALLEL
  /*** Compute the processor set of each block ***/
  CMD(TYPE,Compute_leader)(DM, P, DBL, tkl, key);
#endif
}

#ifdef PARALLEL
void CMD(TYPE,Compute_rind)(TYPE* DM, csptr P, PhidalDistrHID* DBL, dim_t *key, int *tmp, int *pt_tkl) {
  dim_t i, j, k;

  int ind;
  int kl, tkl;

  dim_t *block_psetindex = DBL->block_psetindex;
  mpi_t *block_pset      = DBL->block_pset;

  /*** Compute rind ***/
  DM->sharenbr = 0;
  ind = 0;
  tkl = 0;
  for(i=0;i<P->n;i++)
    if(block_psetindex[i+1]-block_psetindex[i] > 1)
      for(k=0;k<P->nnzrow[i];k++)
	{
	  j = P->ja[i][k];
	  /** Compute the key of block(i,j) **/
	  intersect_key(block_psetindex[i+1]-block_psetindex[i], block_psetindex[j+1]-block_psetindex[j], 
			block_pset+block_psetindex[i], block_pset+block_psetindex[j], key, &kl);
	  if(kl>1) /** This block is shared by several processors **/
	    {

	      /*{
		dim_t w;
 		fprintfv(5, stderr, "BLOCK %d %d \n", DBL->loc2glob_blocknum[i], DBL->loc2glob_blocknum[j]);
		fprintfv(5, stderr, "key i = ");
		for(w = block_psetindex[i]; w < block_psetindex[i+1];w++)
		fprintfv(5, stderr, "%d ", block_pset[w]);
		fprintfv(5, stderr, "\n");
		fprintfv(5, stderr, "key j = ");
		for(w = block_psetindex[j]; w < block_psetindex[j+1];w++)
		fprintfv(5, stderr, "%d ", block_pset[w]);
		fprintfv(5, stderr, "\n");
		}*/


	      tmp[(int)P->ma[i][k]] = DM->sharenbr; /** Need this tmp
							vector to compute cind **/
	      tkl += kl;
	      DM->rind[ind] = DM->sharenbr++;
	      
	    }
	  else
	    {
#ifdef DEBUG_M
	      assert(kl == 1);
	      assert(key[0] == DBL->proc_id);
#endif
	      tmp[(int)P->ma[i][k]] = -1;
	      DM->rind[ind] = -1;
	    }
	  
	  ind++;
	}
    else
      for(k=0;k<P->nnzrow[i];k++)
	{
#ifdef DEBUG_M
	  assert(block_psetindex[i+1]-block_psetindex[i] == 1);
	  if(block_pset[block_psetindex[i]] != DBL->proc_id)
	    fprintfd(stderr, "proc_id = %d != %d \n", DBL->proc_id, block_pset[block_psetindex[i]]);
  	  assert(block_pset[block_psetindex[i]] == DBL->proc_id);
#endif
	  tmp[(int)P->ma[i][k]] = -1;
	  DM->rind[ind] = -1;
	  /*fprintfv(5, stderr, "Block %d %d is not shared ind = %d \n", i,P->ja[i][k] ,DM->rind[ind]);*/
	  ind++;
	}
  
  *pt_tkl = tkl;

}

void CMD(TYPE,Compute_cind)(TYPE* DM, csptr P, /* PhidalDistrHID* DBL, */ int *tmp) {
  dim_t i,k;
  int ind = 0;

  TYPES *M = &(DM->M);

  for(i=M->tlj;i<=M->brj;i++)
    for(k=0;k<P->nnzrow[i];k++)
      DM->cind[ind++] = tmp[(int)P->ma[i][k]]; 
}


void CMD(TYPE,Compute_leader)(TYPE* DM, csptr P, PhidalDistrHID* DBL, int tkl, dim_t *key) {
  dim_t i, j, k;

  int ind;
  int kl;

  TYPES *M = &(DM->M);

  int *block_psetindex = DBL->block_psetindex;
  int *block_pset      = DBL->block_pset;

  /*** Compute the processor set of each block ***/
  DM->pset_index = (INTL *)malloc(sizeof(INTL)* (DM->sharenbr+1));
  if(tkl != 0)
    DM->pset       = (int *)malloc(sizeof(int)* tkl);
  else DM->pset = NULL;
  
  ind = 0;
  tkl = 0;
  /*dumpcsr(stdout, NULL, M->rja, M->ria, M->bri-M->tli+1); */
  for(i=M->tli;i<=M->bri;i++)
    {
      if(block_psetindex[i+1]-block_psetindex[i] == 1)
	continue;
      
      for(k=M->ria[i];k<M->ria[i+1];k++)
	{
	  j = M->rja[k];

	  intersect_key(block_psetindex[i+1]-block_psetindex[i], block_psetindex[j+1]-block_psetindex[j], 
			block_pset+block_psetindex[i],
			block_pset+block_psetindex[j], key, &kl);

	  if(kl>1)
	    {
#ifdef DEBUG_M
	      assert(DM->rind[k] == ind);
#endif
	      DM->pset_index[ind] = tkl;
	      memcpy(DM->pset+tkl, key, sizeof(int)*kl);
	      tkl+=kl;
	      ind++;
	    }
	}
    }
  DM->pset_index[ind] = tkl;
  free(key);

  /****************************************************************/
  /* Set the leader (TEMPORAIRE POUR TEST==>LOAD BALANCE A FAIRE) */  
  /****************************************************************/

  /**** ATTENTION METTRE LE MEME LEADER PAR ROW ET PAR COLUMN ***/
  DM->rlead = (int *)malloc(sizeof(int)*M->bloknbr);
  for(i=M->tli;i<=M->bri;i++)
    for(k=M->ria[i];k<M->ria[i+1];k++)
      {
	if(DM->rind[k] == -1)
	  DM->rlead[k] = DBL->proc_id;
	else
	  {
	    ind = hazardous(DM->pset_index[DM->rind[k]+1]-DM->pset_index[DM->rind[k]], DBL->loc2glob_blocknum[M->rja[k]]);
	    if(M->rja[k] == i)
	      DM->rlead[k]= DBL->row_leader[i];
	    /*DM->rlead[k] = DM->pset[ DM->pset_index[DM->rind[k]]];*/
	    else
	      { 
		/*DM->rlead[k] = DM->pset[ DM->pset_index[DM->rind[k]]+ind];*/
		/** Attention on prend le leader dans la colonne ! pour etre compatible avec 
		    le clead ***/
		j = DBL->row_leader[M->rja[k]];
		if(is_in_key2(j, DM->rind[k], DM->pset_index, DM->pset))
		  DM->rlead[k]= j;
		else
		  DM->rlead[k] = DM->pset[ DM->pset_index[DM->rind[k]]+ind];
	      }
	  }
      }

  DM->clead = (int *)malloc(sizeof(int)*M->bloknbr);
  for(j=M->tlj;j<=M->brj;j++)
    for(k=M->cia[j];k<M->cia[j+1];k++)
      if(DM->cind[k] == -1)
	DM->clead[k] = DM->proc_id;
      else
	{
	  ind = hazardous(DM->pset_index[DM->cind[k]+1]-DM->pset_index[DM->cind[k]], DBL->loc2glob_blocknum[j]);
	  if(M->cja[k] == j)
	    /*DM->clead[k] = DM->pset[ DM->pset_index[DM->cind[k]]];*/
	    DM->clead[k]= DBL->row_leader[j];
	  else
	    {
	      /*DM->clead[k] = DM->pset[ DM->pset_index[DM->cind[k]]+ind];*/
	      /** Attention on prend le leader dans la colonne ! pour etre compatible avec 
		  le rlead ***/
	      if(is_in_key2(DBL->row_leader[j], DM->cind[k], DM->pset_index, DM->pset))
		DM->clead[k]= DBL->row_leader[j];
	      else
		DM->clead[k] = DM->pset[ DM->pset_index[DM->cind[k]]+ind];
	    }
	}
  

}

#endif


/* @authors J. GAIDAMOUR */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>

#include "phidal_sequential.h"
/* #include "phidal_parallel.h" */
#include "block.h"

void fix(SymbolMatrix* symbmtx, alloc_t alloc) {
  /*FIX*/
  if (alloc == RBLK) {
    symbmtx->ccblktab = NULL;
  } else if (alloc == ONE) {
    SymbolMatrix_Init(symbmtx); 
  }

  /*tmp*/
  /* if (alloc == CBLK) */
/*     symbmtx->ccblktab = NULL; */
}

/* a voir */
void HDIM_tmp_avant(DBMatrix* M) {
  if (M->alloc == ONE) {
    assert(M->hdim == NULL);
    M->hdim = M->a->symbmtx.hdim;
    M->ccblktab = M->a->symbmtx.ccblktab;
  }
}

void HDIM_tmp_apres(DBMatrix* M) {
  if ((M->alloc == ONE) /* || (M->alloc == CBLK) */)  {
    M->hdim = NULL;
    M->ccblktab = NULL;
  }
  if (M->alloc == CBLK)  {
    M->ccblktab = NULL;
  }

}

void regroupe(COEF *block1, COEF *block2, int width, blas_t stride) {
  dim_t i;
  int size;
  int UN = 1;
  COEF *x = block1;
  COEF *y = block2+stride;
#ifdef DEBUG_NOALLOCATION
  return;
#endif

  for(i=0; i<width; i++) {
    size = width-i-1;
    BLAS_COPY(size, x+i+1, UN, y+i, stride);
    
    x+=stride;
    y+=stride;
  }
}



/* #ifdef SIZE */
/*   int sl=0, se=0, ss=0, ss2=0; */
/* #endif */

/* #ifdef SIZE */
/*     sl = DBMatrix_size(PREC_L(P),BL,0); */
/*     se = DBMatrix_size(P->E_DB,BL,0); */
/* #endif */

/* #ifdef SIZE */
/*     sl = DBMatrix_size(PREC_L(P),BL,0); */
/* #endif */

/* #ifdef SIZE */
/*     ss = DBMatrix_size(P->SL,BL,0); */
/* #endif */

/* #ifdef SIZE */
/* 	      ss2 = DBMatrix_size(m,BL,0); */
/* #endif */


/* #ifdef SIZE */
/*   printfv(5, "L       : "); print_size(sl); printf("\n"); */
/*   printfv(5, "E       : "); print_size(se); printf("\n"); */
/*   printfv(5, "S       : "); print_size(ss); printf("\n"); */
/*   printfv(5, "S2      : "); print_size(ss2); printf("\n"); */

/*   printfv(5, "-----------------\n"); */
/*   printfv(5, "Tot     : "); print_size(sl+se+ss+ss2); printf("\n"); */
/*   printfv(5, "  L+E   : "); print_size(sl+se); printf("\n"); */
/*   printfv(5, "  L+E+S : "); print_size(sl+se+ss); printf("\n"); */
/*   printfv(5, "  L+S+S2: "); print_size(sl+ss+ss2); printf("\n"); */
/*   printfv(5, "\n"); */
/* #endif */



/* ** */

/* /\* #define DEBUG_SCAL *\/ */
/* #ifdef DEBUG_SCAL */

/*   if (P->forwardlev == 0) { */
/*     fdumpPhidalMatrix("A.txt", m); */
/*     PhidalMatrix L; */
/*     DBMatrix2PhidalMatrix(PREC_L(P), &L, option->locally_nbr, BL, 0/\*alloc : todo 1*\/, 0/\*diagonale non sÃÂ©parÃÂ©e*\/); */
/*     fdumpPhidalMatrix("L.txt", &L); */
/*     exit(1); */
/*   } else { */

/*     /\* L *\/ */
/*     PhidalMatrix L; */
/*     DBMatrix2PhidalMatrix(PREC_L(P), &L, option->locally_nbr, BL, 0/\*alloc : todo 1*\/, 0/\*diagonale non sÃÂ©parÃÂ©e*\/); */
/*     fdumpPhidalMatrix("L.txt", &L); */
    
/*     /\* nextPrec->L *\/ */
/*     PhidalMatrix Ln; */
/*     DBMatrix2PhidalMatrix(P->nextprec->L, &Ln,  */
/* 			  option->locally_nbr, BL, 0/\*alloc : todo 1*\/, 0/\*diagonale non sÃÂ©parÃÂ©e*\/); */
/*     fdumpPhidalMatrix("Ln.txt", &Ln); */

/*     exit(1); */
/*   } */
/* #endif */


/* #ifdef DEBUG_M */
/*   assert((ALLOC_NOREC == ONE) || (ALLOC_NOREC == CBLK) || (ALLOC_NOREC == RBLK) || (ALLOC_NOREC == BLK)); */
/*   assert((ALLOC_L     == ONE) || (ALLOC_L     == CBLK) || (ALLOC_L     == RBLK) || (ALLOC_L     == BLK)); */
/*   assert((ALLOC_E     == ONE) || (ALLOC_E     == RBLK) || (ALLOC_E     == CBLK)); */
/*   assert((ALLOC_S     == ONE) || (ALLOC_S     == CBLK) || (ALLOC_S     == BLK )); */
/*   /\* Si fillin non compatible avec ALLOC_E et ALLOC_S, OK *\/ */

/* #ifdef GEMM_LEFT_LOOKING */
/*   if (ALLOC_FILL == INGEMM) { */
/*     if ((ALLOC_S == ONE) || (ALLOC_S == RBLK)) { /\* on pourrait faire quand meme remplir au fur et a mesure *\/ */
/*       printfv(5, "*** Warning : S is allocated and filled in before GEMM\n");  */
/*     } */
/*     if ((ALLOC_E == ONE) || (ALLOC_E == CBLK)) { */
/*       printfv(5, "*** Warning : S is unallocated after GEMM\n"); */
/*     } */
/*   } */

/* #ifndef NO_PIC_DECREASE */
/*   printfv(5, "***  Info : undef NO_PIC_DECREASE\n"); */
/* #else */
/*   printfv(5, "***  Info : define NO_PIC_DECREASE\n"); */
/* #endif */

/* #endif /\* GEMM_LEFT_LOOKING *\/ */

/* #endif /\* DEBUG_M *\/ */

/* #ifndef DEBUG_M */
/* #ifdef GEMM_LEFT_LOOKING */
/*   if (!(ALLOC_NOREC == ONE) && */
/*       (ALLOC_L      == ONE) && */
/*       (ALLOC_S2     == ONE) && */
/*       (ALLOC_E      == RBLK) && */
/*       (ALLOC_S      == CBLK) && */
/*       (ALLOC_FILL   == INGEMM)) */
/*     printfv(5, "***  Warning : options d'allocation\n"); */
/* #else  */
/*   if (!(ALLOC_NOREC == ONE) && */
/*       (ALLOC_L      == ONE) && */
/*       (ALLOC_S2     == ONE) && */
/*       (ALLOC_E      == CBLK) && */
/*       (ALLOC_S      == ONE) && */
/*       (ALLOC_FILL   == INGEMM)) */
/*     printfv(5, "***  Warning : options d'allocation\n"); */
/* #endif /\* GEMM_LEFT_LOOKING *\/ */
/* #endif */


/* / UNSYM / */

/* #ifdef DEBUG_M */
/*   assert((ALLOC_NOREC == ONE) || (ALLOC_NOREC == CBLK) || (ALLOC_NOREC == RBLK) || (ALLOC_NOREC == BLK)); */
/*   assert((ALLOC_L     == ONE) || (ALLOC_L     == CBLK) || (ALLOC_L     == RBLK) || (ALLOC_L     == BLK)); */
/*   assert((ALLOC_E     == ONE) || (ALLOC_E     == RBLK) || (ALLOC_E     == CBLK)); */
/*   assert((ALLOC_S     == ONE) || (ALLOC_S     == CBLK) || (ALLOC_S     == BLK )); */
/*   /\* Si fillin non compatible avec ALLOC_E et ALLOC_S, OK *\/ */

/* #ifdef GEMM_LEFT_LOOKING */
/*   if (ALLOC_FILL == INGEMM) { */
/*     if ((ALLOC_S == ONE) || (ALLOC_S == RBLK)) { /\* on pourrait faire quand meme remplir au fur et a mesure *\/ */
/*       printfv(5, "*** Warning : S is allocated and filled in before GEMM\n");  */
/*     } */
/*     if ((ALLOC_E == ONE) || (ALLOC_E == CBLK)) { */
/*       printfv(5, "*** Warning : S is unallocated after GEMM\n"); */
/*     } */
/*   } */

/* #ifndef NO_PIC_DECREASE */
/*   printfv(5, "***  Info : undef NO_PIC_DECREASE\n"); */
/* #else */
/*   printfv(5, "***  Info : define NO_PIC_DECREASE\n"); */
/* #endif */

/* #endif /\* GEMM_LEFT_LOOKING *\/ */

/* #endif /\* DEBUG_M *\/ */

/* #ifndef DEBUG_M */
/* #ifdef GEMM_LEFT_LOOKING */
/*   if (!(ALLOC_NOREC == ONE) && */
/*       (ALLOC_L      == ONE) && */
/*       (ALLOC_S2     == ONE) && */
/*       (ALLOC_E      == RBLK) && */
/*       (ALLOC_S      == CBLK) && */
/*       (ALLOC_FILL   == INGEMM)) */
/*     printfv(5, "***  Warning : options d'allocation\n"); */
/* #else  */
/*   if (!(ALLOC_NOREC == ONE) && */
/*       (ALLOC_L      == ONE) && */
/*       (ALLOC_S2     == ONE) && */
/*       (ALLOC_E      == CBLK) && */
/*       (ALLOC_S      == ONE) && */
/*       (ALLOC_FILL   == INGEMM)) */
/*     printfv(5, "***  Warning : options d'allocation\n"); */
/* #endif /\* GEMM_LEFT_LOOKING *\/ */
/* #endif */



void PhidalMatrix_InitScale(PhidalMatrix* A, REAL* scaletab, REAL* iscaletab, PhidalHID* BL) {
  dim_t i;
  
  fprintfd(stdout, "Scaling\n");
  
  /*** Symmetric scale ***/
  PhidalMatrix_ColNorm2(A, BL, iscaletab);

  /** Take the square root of the norm such that a term 
      aij = aij / (sqrt(rowi)*sqrt(colj)) **/
  for(i=0;i<A->dim1;i++)
    iscaletab[i] = sqrt(iscaletab[i]); 
  for(i=0;i<A->dim1;i++)
    scaletab[i] = 1.0/iscaletab[i];
  
}

#ifdef NON
void PhidalMatrix_InitScale_Unsym(PhidalMatrix* A, 
			      REAL* scalerow, REAL* iscalerow, 
			      REAL* scalecol, REAL* iscalecol, PhidalOptions* option, PhidalHID* BL) {
  dim_t i;
  
  fprintfv(5, stdout, "Scaling\n");
  
  if(option->scale == 1)
    {
      /*** Unsymetric scale ***/
      /* cf PhidalMatrix_UnsymScale(A, BL, scalerow, scalecol, iscalerow, iscalecol); */

      /*** Unsymetric scale ***/
      PhidalMatrix_ColNorm2(A, BL, iscalecol);
      for(i=0;i<A->dim1;i++)
	scalecol[i] = 1.0/iscalecol[i];

      PhidalMatrix_RowNorm2(A, BL, iscalerow);
      for(i=0;i<A->dim1;i++)
	scalerow[i] = 1.0/iscalerow[i];

    }
  else 
    {
      /*** Symmetric scale ***/
      /* PhidalMatrix_SymScale is not equivalent to this: */
      PhidalMatrix_ColNorm2(A, BL, iscalecol);
      PhidalMatrix_RowNorm2(A, BL, iscalerow);
      
      /** Take the square root of the norm such that a term 
	  aij = aij / (sqrt(rowi)*sqrt(colj)) **/
      for(i=0;i<A->dim1;i++)
	iscalecol[i] = sqrt(iscalecol[i]);
      for(i=0;i<A->dim1;i++)
	iscalerow[i] = sqrt(iscalerow[i]); 
      for(i=0;i<A->dim1;i++)
	scalecol[i] = 1.0/iscalecol[i];
      for(i=0;i<A->dim1;i++)
	scalerow[i] = 1.0/iscalerow[i];
      
    }
  
}
#endif

void PhidalMatrix_Scale(PhidalMatrix* A, REAL* scaletab, PhidalHID* BL) {
  PhidalMatrix_ColMult2(scaletab, A, BL);
  PhidalMatrix_RowMult2(scaletab, A, BL);
}

void PhidalMatrix_Scale_Unsym(PhidalMatrix* A, REAL* scalerow, REAL* scalecol, PhidalHID* BL) {
  PhidalMatrix_ColMult2(scalecol, A, BL);
  PhidalMatrix_RowMult2(scalerow, A, BL);	
}


    /*     printDBMatrix(&PREC_L(P), "B.txt"); */
    /*     printDBMatrix(&P->E_DB, "E.txt"); */
    /*     printDBMatrix(&P->SL, "C.txt"); */
    
    /*     printLD2(P->E_DB->a, "init_P.E.html",  "PE", 4); */
    /*     printLD2(P->F_DB->a, "init_P.F.html",  "PF", 4); */
    /*     printLD2(P->SL->a,   "init_P.SL.html", "PE", 2); */
    /*     printLD2(P->SU->a,   "init_P.SU.html", "PE", 3); */


    /*      cleanclean(P->E_DB); */
    /*      cleanclean(P->F_DB); */
     
    /*      memset(P->SL->a[0].coeftab,0,sizeof(COEF)*P->SL->a->coefnbr); */
    /*      memset(P->SU->a[0].coeftab,0,sizeof(COEF)*P->SU->a->coefnbr); */

    /*     printLD2(P->E_DB->a, "P.E.html", "PE", 4); */
    /*     printLD2(P->F_DB->a, "P.F.html", "PF", 4); */

    /*     if ((ALLOC_S != ONE) && (ALLOC_FILL == INGEMM)){ fillS */
    /*       PhidalMatrix_Transpose_SparMat(PhidalSL, BL); /\*ou ds les param. de DBMatrix_AllocSymbmtx*\/ */
    /*       /\*     PhidalMatrix_Transpose(PhidalSU); *\/ /\*dans le GEMM, dans les param. de DBMatrix_AllocSymbmtx*\/ */
    /*     } */


	  /*     printLD2(P->SL->a,  "P.avSL.html", "PE", 2); */
	  /*     printLD2(P->SU->a,  "P.avSU.html", "PE", 3); */



    /*     printDBMatrix(&PREC_L(P), "B.html"); */
    /*     printDBMatrix(&P->E_DB, "E.html"); */
    /*     printDBMatrix(&P->SL, "C.html"); */


    /*     P->F_DB = (DBMatrix*)malloc(sizeof(DBMatrix)); P->F_DB->symmetric = P->symmetric; */
    /*     DBMatrix_Init(P->F_DB); */
    /*     DBMatrix_VirtualCpy(P->E_DB, P->F_DB); */
    /*     DBMatrix_Transpose(P->F_DB); */


    /*     DBMatrix_Clean(P->F_DB); */
    /*     free(P->F_DB); */
    /*     P->F_DB=NULL; */


/* #ifdef DEBUG_COMPAR */
/* 	  PhidalMatrix Orig; */
/* 	  PhidalMatrix_BuildVirtualMatrix(P->phidalS->tli, P->phidalS->tlj, P->phidalS->bri, P->phidalS->brj, m, &Orig, BL); */
/* 	  PhidalMatrix_p(&Orig); */
/* 	  PhidalMatrix_p(P->phidalS); */
/* 	  PhidalMatrix_diff(&Orig, P->phidalS); */
/* 	  exit(1); */
/* #endif */


/* /\* #define DEBUG_SCAL *\/ */
/* #ifdef DEBUG_SCAL */

/*   if (P->forwardlev == 0) { */
/*     fdumpPhidalMatrix("A.txt", m); */
/*     PhidalMatrix LU; */

/*     DBMatrix2PhidalMatrix_Unsym(PREC_L(P), PREC_U(P), &LU,  */
/*                              option->locally_nbr, BL, 0/\*alloc : todo 1*\/); */
/*     fdumpPhidalMatrix("L.txt", &LU); */
/*     exit(1); */
/*   } else { */

/*     /\* L *\/ */
/*     PhidalMatrix L; */
/*     DBMatrix2PhidalMatrix_Unsym(PREC_L(P), PREC_U(P), &L, option->locally_nbr, BL, 0/\*alloc : todo 1*\/); */
/*     fdumpPhidalMatrix("L.txt", &L); */
    
/*     /\* nextPrec->L *\/ */
/*     PhidalMatrix Ln; */
/*     DBMatrix2PhidalMatrix_Unsym(P->nextprec->L, P->nextprec->U, &Ln,  */
/* 				option->locally_nbr, BL, 0/\*alloc : todo 1*\/); */
/*     fdumpPhidalMatrix("Ln.txt", &Ln); */

/*     exit(1); */
/*   } */
/* #endif */

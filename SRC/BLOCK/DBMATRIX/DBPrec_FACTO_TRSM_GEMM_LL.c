/* @authors J. GAIDAMOUR */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>

#include "phidal_sequential.h"
#include "block.h"
#include "prec.h"

#define _DBPrec_GEMM(P, alpha, fillS, PhidalS, BL)			\
  CMDu(DBMatrix,GEMM)(&PREC_S_BLOCK(P), alpha, S(PREC_EDB(P)), S(PREC_FDB(P)), S(PREC_L_BLOCK(P)), fillS, PhidalS, BL)


void DBMatrix_FACTO_LEV0(_UDBDistrMatrix* LU);
void DBMatrix_FACTO_LEV0u(_UDBDistrMatrix* LU);

long DBMatrix_NNZGemmPic(flag_t symmetric, DBMatrix* E, DBMatrix* S);

#ifdef SYMMETRIC
#define _DBMatrix_FACTO_LEV0(LU) DBMatrix_FACTO_LEV0(LU)
#else
#define _DBMatrix_FACTO_LEV0 DBMatrix_FACTO_LEV0u
#endif

#if defined(SYMMETRIC)
void CMD(_DBDistrPrec,FACTO_TRSM_GEMM_LL)(_DBDistrPrec* P, _UPhidalDistrMatrix* PhidalS, flag_t fillS, PhidalHID* BL, chrono_t* ttotal)

#elif defined(UNSYMMETRIC)
void CMD(_DBDistrPrec,FACTO_TRSM_GEMM_LLu)(_DBDistrPrec* P, _UPhidalDistrMatrix* PhidalS, flag_t fillS, PhidalHID* BL, chrono_t* ttotal)
#endif

 {
  chrono_t t1, t2;

  assert(P->forwardlev > 0);

  /*** FACTO ***/  
  fprintfd(stdout, " Numeric Factorisation (M)\n"); 
  t1  = dwalltime();

  _DBMatrix_FACTO_LEV0(&PREC_LU_BLOCK(P)); /* todo : sym*/

  t2  = dwalltime(); *ttotal += t2-t1;
  fprintfd(stdout, "  M : Numeric Factorisation in %g seconds\n\n", t2-t1);
  /*** ***/

  /***  TRSM ***/   
  fprintf(stdout, " TRSM M / E\n");       
  t1  = dwalltime(); 

#if defined(SYMMETRIC)
  DBMatrix_TRSM(1, S(PREC_L_BLOCK(P)), S(PREC_EDB(P)));
#elif defined(UNSYMMETRIC)
  DBMatrix_Transpose(S(PREC_U_BLOCK(P))); /* TODO !! */
  DBMatrix_TRSM(0, S(PREC_U_BLOCK(P)), S(PREC_EDB(P)));
  DBMatrix_Transpose(S(PREC_U_BLOCK(P)));

  DBMatrix_Transpose(S(PREC_FDB(P)));
  DBMatrix_TRSM(1, S(PREC_L_BLOCK(P)), S(PREC_FDB(P)));
  DBMatrix_Transpose(S(PREC_FDB(P)));
#endif

  t2  = dwalltime(); *ttotal += t2-t1;
  fprintfd(stdout, "  TRSM in %g seconds\n\n", t2-t1);
  /*** ***/
    
  /* */
  if (fillS == INGEMM) {
    /* todo : verifier que E est allou� de sorte 
       a �tre desallouable au fur et a mesure pour que le calcul soit exacte ... */
    PrecInfo_SubNNZ_(P->info, DBM_NNZ("N", S(PREC_EDB(P))), "E_DB");
    PrecInfo_AddNNZ_(P->info, DBMatrix_NNZGemmPic(P->symmetric, S(PREC_EDB(P)),/*S*/S(PREC_SL_BLOCK(P))), "LL : GEMM Pic");
  }
  /* */

  /*** GEMM ***/  
  fprintfd(stdout, " GEMM E/E -> S\n"); 
  t1  = dwalltime();     

  {
    UPhidalMatrix* myPhidalS=NULL;
    myPhidalS->L = S(PhidalS->L);
    myPhidalS->U = S(PhidalS->U);
#ifndef PARALLEL

    _DBPrec_GEMM(P, -1, fillS, myPhidalS, BL);
#else
    {
      UDBMatrix ARG; ARG.L=S(PREC_SL_BLOCK(P)); ARG.U=S(PREC_SU_BLOCK(P));
      CMDu(DBMatrix,GEMM)(&ARG, -1, S(PREC_EDB(P)), IFUNSYM(S(PREC_FDB(P)),) IFSYM(NULL,) S(PREC_L_BLOCK(P)), fillS, myPhidalS, BL);
    }
#endif
  }

  t2  = dwalltime(); *ttotal += t2-t1;
  fprintfd(stdout, "  GEMM in %g seconds\n\n", t2-t1);

  /*** FREE ***/
  if (fillS == INGEMM) { /* si le remplissage a Ã©tÃ© fait au fur et a mesure, on supprime PhidalS->L maintenant */
#ifndef PARALLEL
    DUP(PhidalMatrix_Clean, PhidalS->L, PhidalS->U);
#else
    DUP(PhidalDistrMatrix_Clean, PhidalS->L, PhidalS->U);
#endif

    DUP(free,PhidalS->L, PhidalS->U);
  }

  /*** ***/

  DUP(HDIM_tmp_apres,S(PREC_EDB(P)), S(PREC_FDB(P)));
    
#ifdef PARALLEL
  DBDistrMatrix_Clean(PREC_EDB(P));
#else
  DBMatrix_Clean(PREC_EDB(P));
#endif
  free(PREC_EDB(P));
  PREC_EDB(P)=NULL; /* useful ? */
  
#ifdef UNSYMMETRIC

#ifdef PARALLEL
  DBDistrMatrix_Clean(PREC_FDB(P));
#else
  DBMatrix_Clean(PREC_FDB(P));
#endif

  free(PREC_FDB(P));
  PREC_FDB(P)=NULL;
#endif
}

#ifndef PARALLEL
/* Facto spÃ©ciale lev 0 */
#ifdef SYMMETRIC
void DBMatrix_FACTO_LEV0(UDBMatrix* LU)
#else
void DBMatrix_FACTO_LEV0u(UDBMatrix* LU)
#endif
{  
  /* Cela peut-Ãªtre une facto + simple */
  /*   DBMatrix_FACTO_L(DBMatrix* L  */
  /*   DBMatrix_FACTO_Lu(DBMatrix* L, DBMatrix* U) */
 
  DBMatrix* L =LU->L;

#if defined(SYMMETRIC)
  if ((L->alloc != BLK) && (L->alloc != RBLK))
    DBMatrix_FACTO(LU);
  else
    DBMatrix_FACTO2(LU);
#elif defined(UNSYMMETRIC)
  if ((L->alloc != BLK) && (L->alloc != RBLK))
    DBMatrix_FACTOu(LU);
  else
    /* DBMatrix_FACTO2u(L, U); */ assert(0);
#endif
  

}
#endif

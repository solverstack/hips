/* @authors J. GAIDAMOUR */

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include <string.h>

#include "block.h"

void print_size(unsigned long size) {
  if (size < 1024) {
    printfd("%lu o", size);
  } else
    if (size < 1024*1024) {
      printfd("%0.2f Ko", (float)size/1024);
    } else
      if (size < 1024*1024*1024) {
	printfd("%0.2f Mo", (float)size/(1024*1024));
      } else
	printfd("%0.3f Go", (float)size/(1024*1024*1024));
}

/* /\*pas complet, +=, PHIDAL, ...*\/ */
/* unsigned long DBPrec_size(DBPrec* p, PhidalHID* BL, flag_t verbose) { */

/*   if (p->levelnum == 0) */
/*     fprintfd(stdout, "Taille des DBMatrix : --------------\n"); */

/*   fprintfd(stdout, "-------Level %d NNZ ----------------\n", p->levelnum); */
  
/*   fprintfd(stdout, "L(%d) : ", p->levelnum); DBMatrix_size(p->L,BL,1); */
  
/*   /\*   if(p->symmetric == 0) *\/ */
/*   /\*     fprintfd(stdout, "U(%d) : ", p->levelnum); p->U,1)); *\/ */
  
/*   if(p->forwardlev > 0) */
/*     { */
/*       fprintfd(stdout, "E(%d) : ", p->levelnum); DBMatrix_size(p->E_DB,BL,1); */
/*       if(p->symmetric == 0) */
/* 	fprintfd(stdout, "F(%d) : ", p->levelnum); DBMatrix_size(p->F_DB,BL,1); */
      
/*       if(p->schur_method == 1)  */
/* 	fprintfd(stdout, "S(%d) : ",  p->levelnum); DBMatrix_size(p->S,BL,1); */

/* /\*       if(p->schur_method == 2)  *\/ */
/* /\* 	fprintfd(stdout, "C(%d) : ",  p->levelnum); DBMatrix_size(p->B,BL,1); *\/ */

/*       DBPrec_size(p->nextprec, BL, verbose); */
/*     } */

/*   return 0; */
/* } */


/*TODO : se passer de l'arg BL*/
unsigned long DBMatrix_size(DBMatrix* db, PhidalHID* BL, flag_t verbose) {
  int i, ilast;
  unsigned long size=0;

  if(db==NULL) return 0;

  /* struct */
  size += sizeof(DBMatrix);
  
  if (db->virtual == NOVIRTUAL) {
    
    /* ccblktab and hdim */
    if(db->alloc == RBLK) {
      assert(db->ccblktab != NULL);
      assert(db->hdim != NULL);

      size += sizeof(SymbolCCblk) * db->cblknbr;
      size += sizeof(int*) * db->cblknbr;
    } else {
      assert(db->ccblktab == NULL);
      assert(db->hdim == NULL);
    }

    /* bloktab */
    for (i=0; i<db->bloknbr; i++)
      size += SymbolMatrix_size(&db->bloktab[i].symbmtx,verbose);
    
    /* ra et ca */
    size += 2*sizeof(SymbolMatrix*) * db->bloknbr;
    
    /* cia et ria */
    size += 2*sizeof(int)* (BL->nblock+1);
    
    /* cja et rja */
    size += 2*sizeof(int)* db->bloknbr;

    /* cblktosolvmtx */
    if (db->cblktosolvmtx != NULL) {
      size = sizeof(SolverMatrix*) * db->cblknbr;
    }
    
    if ((db->virtual == NOVIRTUAL) || (db->virtual == COPY)) {
      if (db->alloc == ONE) {
	ilast = 1;
      } else ilast = db->brj - db->tlj + 1; /* TODO */
      
      for (i=0; i<ilast; i++)
	size += SolverMatrix_size(&db->a[i], verbose);     
    }
   
  } 

  if (verbose) {
    print_size(size); printfd("\n");
  }

  return size;

}

unsigned long SolverMatrix_size(SolverMatrix* solvmtx, flag_t verbose) {
  unsigned long size=0;
  size += sizeof(SolverMatrix);

  assert((solvmtx->virtual == NOVIRTUAL) || (solvmtx->virtual == COPY));
  
  assert(solvmtx->bloktab != NULL);
#ifdef DEBUG_NOALLOCATION
  assert(solvmtx->coeftab != NULL);
#endif /* DEBUG_NOALLOCATION */

  if (solvmtx->virtual != COPY) size += sizeof(SolverBlok) * solvmtx->symbmtx.bloknbr/*/!\ bloknbr doit être correcte*/;
 
  size += sizeof(COEF*) * solvmtx->coefnbr;

  return size;
}

unsigned long SymbolMatrix_size(SymbolMatrix* symbmtx, flag_t verbose) {
  unsigned long size=0;

  size += sizeof(SymbolMatrix);

  /* bcblktab */
  if (symbmtx->bcblktab != NULL)/*(NULL : 3)*/ size += sizeof(SymbolBCblk) * symbmtx->cblknbr;
  
  /* hdim (stride always NULL or virtual) */
  if (symbmtx->hdim != NULL)/*(NULL : -0 or 3)*/ size += sizeof(int) * symbmtx->cblknbr;
  
  /* ccblktab and bloktab */
  if (symbmtx->virtual == 0) {
    if (symbmtx->ccblktab != NULL)/*(NULL : 3)*/ size += sizeof(SymbolCCblk) * symbmtx->cblknbr;
    assert(symbmtx->bloktab != NULL);
    size += sizeof(SymbolBlok) * symbmtx->bloknbr; 
  } 
  
  return size;
}

/* @authors J. GAIDAMOUR, P. HENON */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>
#include <assert.h>

#if defined(D_PH_PH) || defined(D_PH) || defined(D_PH_DB) || defined(D_DB_PH) || defined(D_DB) || defined(D_DB_DB) /* parallel */
#define PARALLEL
#include "phidal_parallel.h"
#endif

#if defined(DB_PH) || defined(PH_DB) || defined(DB) || defined(DB_DB) || defined(D_DB_PH) || defined(D_DB) || defined(D_DB_DB) 
#include "block.h"
#include "prec.h"
#endif

#if defined(D_DB_PH) || defined(D_PH_DB)
#include "db_parallel.h"
#endif

#include "phidal_sequential.h"

/* todo: include des .c */
/* todo : a voir pour la premiere it, si routine seq ou / / : exemple : pour le mix / / */
/* todo : a voir, eviter gmres sur 1 it quand facto exact lev=1 */

/*#define GUESS_SOLVE*/
/*#define IM_ALLOC_AT_START*/

#ifdef DEBUG_J
/* #undef malloc */
/* #define malloc(size) calloc(size, 1) */
#endif

#if defined(PH_PH)   /* init PH & M1 PH */

     COEF my_zdotc(int n, COEF *x, blas_t incx, COEF *y, blas_t incy) {
       dim_t i;
       COEF dot=0;
       assert(incx == 1);
       assert(incx == incy);

       for(i=0; i<n; i++) {
         dot += conj(x[i]) * y[i];
       }

       return dot;
    }

     INTS HIPS_Fgmresd_PH_PH(flag_t verbose, REAL tol, dim_t itmax, 	 
                             PhidalMatrix *A, PhidalPrec *P, 	 
                             PhidalHID *BL, PhidalOptions *option, 	 
                             COEF *rhs, COEF *x, FILE *fp, dim_t *itertab, REAL *resnormtab)

#elif defined(PH)    /* M2 PH */
     INTS HIPS_Fgmresd_PH   (flag_t verbose, REAL tol, dim_t itmax, 
			    PhidalPrec *P, 
			    PhidalHID *BL, PhidalOptions *option, 
			    COEF *rhs, COEF *x, FILE *fp, dim_t *itertab, REAL *resnormtab)
#elif defined(DB_PH) /* mix M2 */
     INTS HIPS_Fgmresd_DB_PH(flag_t verbose, REAL tol, dim_t itmax, 
			    DBPrec *Q, PhidalPrec *P, 
			    PhidalHID *BL, PhidalOptions *option, 
			    COEF *rhs, COEF * x, FILE *fp, dim_t *itertab, REAL *resnormtab)
#elif defined(PH_DB)   /* init DB */
     INTS HIPS_Fgmresd_PH_DB(flag_t verbose, REAL tol, dim_t itmax, 
			    PhidalMatrix *A, DBPrec *P, 
			    PhidalHID *BL, PhidalOptions *option, 
			    COEF *rhs, COEF *x, FILE *fp, dim_t *itertab, REAL *resnormtab)
#elif defined(DB)    /* M2 DB */
     INTS HIPS_Fgmresd_DB   (flag_t verbose, REAL tol, dim_t itmax, 
			    DBPrec *P, 
			    PhidalHID *BL, PhidalOptions *option, 
			    COEF *rhs, COEF *x, FILE *fp, dim_t *itertab, REAL *resnormtab)
#elif defined(DB_DB) /* M1 DB */
     INTS HIPS_Fgmresd_DB_DB(flag_t verbose, REAL tol, dim_t itmax, 
			    DBMatrix *L, DBMatrix *U, DBPrec *P, 
			    PhidalHID *BL, PhidalOptions *option, 
			    COEF *rhs, COEF *x, FILE *fp, dim_t *itertab, REAL *resnormtab)
#elif defined(CS_D1) /* CS 1 */
     INTS HIPS_Fgmresd_CS_D1(flag_t verbose, REAL tol, dim_t itmax, 
			    csptr A, csptr L,
			    PhidalOptions *option, 
			    COEF *rhs, COEF *x, FILE *fp, dim_t *itertab, REAL *resnormtab)
#elif defined(CS_D2) /* CS 2 */
     INTS HIPS_Fgmresd_CS_D2(flag_t verbose, REAL tol, dim_t itmax, 
			    csptr A, csptr L, COEF *D,
			    PhidalOptions *option, 
			    COEF *rhs, COEF *x, FILE *fp, dim_t *itertab, REAL *resnormtab)
#elif defined(D_PH_PH) /* parallel : init PH & M1 PH */
     INTS HIPS_DistrFgmresd_PH_PH(flag_t verbose, REAL tol, dim_t itmax, 
				 PhidalDistrMatrix *DA, PhidalDistrPrec *P,
				 PhidalDistrHID *DBL, PhidalOptions *option, 
				 COEF *rhs, COEF * x, FILE *fp, dim_t *itertab, REAL *resnormtab)  
#elif defined(D_PH) /* parallel : M2 PH */
     INTS HIPS_DistrFgmresd_PH(flag_t verbose, REAL tol, dim_t itmax, 
			      PhidalDistrPrec *P, 
			      PhidalDistrHID *DBL, PhidalOptions *option, 
			      COEF *rhs, COEF * x, FILE *fp, dim_t *itertab, REAL *resnormtab) 
#elif defined(D_DB_PH) /* mix M2 */
     INTS HIPS_DistrFgmresd_DB_PH(flag_t verbose, REAL tol, dim_t itmax, 
				 DBDistrPrec *Q, PhidalDistrPrec *P, 
				 PhidalDistrHID *DBL, PhidalOptions *option, 
				 COEF *rhs, COEF * x, FILE *fp, dim_t *itertab, REAL *resnormtab)
#elif defined(D_PH_DB)   /* parallel init DB */
     INTS HIPS_DistrFgmresd_PH_DB(flag_t verbose, REAL tol, dim_t itmax, 
				 PhidalDistrMatrix *DA, DBDistrPrec *P, 
				 PhidalDistrHID *DBL, PhidalOptions *option, 
				 COEF *rhs, COEF * x, FILE *fp, dim_t *itertab, REAL *resnormtab) 
#elif defined(D_DB)    /* M2 DB */
     INTS HIPS_DistrFgmresd_DB (flag_t verbose, REAL tol, dim_t itmax, 
				 DBDistrPrec *P, 
				 PhidalDistrHID *DBL, PhidalOptions *option, 
				 COEF *rhs, COEF *x, FILE *fp, dim_t *itertab, REAL *resnormtab)
#elif defined(D_DB_DB) /* M1 DB */
     INTS HIPS_DistrFgmresd_DB_DB(flag_t verbose, REAL tol, dim_t itmax, 
				 DBDistrMatrix *L, DBDistrMatrix *U, DBDistrPrec *P, 
				 PhidalDistrHID *DBL, PhidalOptions *option, 
				 COEF *rhs, COEF *x, FILE *fp, dim_t *itertab, REAL *resnormtab)
#endif

{
  COEF *wk1, *wk2;
  int i, i1, j, im, maxits, out_flag,in_flag; 
  blas_t incx;
  dim_t n;
#if defined(DB_PH) ||  defined(DB) || defined(D_PH_PH) || defined(D_PH_DB) || defined(D_PH) || defined(D_DB_PH) || defined(D_DB) || defined(D_DB_DB)
  dim_t tli=-1, bri=-1;
#endif
  int ii,k,k1;
  COEF **vv, **hh, **w, *c, *s, *rs;
  COEF d2c; 
  COEF *sol, alpha;
  REAL eps, eps1=-1, /* tloc, */ ro, gam, t;
  int alloc_flag;
#if defined(PH)        /* M2 PH */
  PhidalPrec *Q;
#elif defined(DB)      /* M2 DB */
  DBPrec *Q;
#elif defined(D_PH_PH) || defined(D_PH_DB) /* parallel : init PH & M1 PH */
  PhidalMatrix *A = &DA->M;
#elif defined(D_PH)    /* parallel : M2 PH */
  PhidalDistrPrec *Q;
#elif defined(D_DB)
  DBDistrPrec *Q;
#endif

#if defined(PARALLEL)
  mpi_t proc_id = DBL->proc_id;
#endif

  REAL tolrat;
  REAL normrhs=-1;
  dim_t iters;
  
  /** Used for timers **/
  REAL tin1,tin2;
  chrono_t t1,t2;
  chrono_t ttotal_PrecSolv=0, ttotal_SchurProd=0, ttotal_Iters=0;

#ifdef DEBUG_M

#if defined(PH_PH) || defined(PH_DB) || defined(D_PH_PH) || defined(D_PH_DB) 
  assert(A->dim1 == A->dim2);
#elif defined(PH) || defined(DB) || defined(D_PH) || defined(D_DB)
  assert(P->schur_method == 2);
  assert(P->prevprec != NULL);
#elif defined(DB_PH) || defined(D_DB_PH) /* mix M2 */
  assert(P->schur_method == 2);
#elif defined(DB_DB) || defined(D_DB_DB) /* M1 DB */
  assert(L != NULL);
  assert(S(L)->dim1 == S(L)->dim2);
#endif

#endif /* DEBUG_M */

 /*------------------------------------------------------------------*
  |      PART1:  Initialization.                                     |
  *------------------------------------------------------------------*/

  /* retrieve information from input data structure */

#if defined(PH_PH) || defined(PH_DB) || defined(D_PH_PH) || defined(D_PH_DB) 
  n = A->dim1;
#elif defined(PH) || defined(DB) || defined(D_PH) || defined(D_DB)
  Q = P->prevprec;
  n = P->dim;
#elif defined(DB_PH) || defined(D_DB_PH) /* mix M2 */
  n = P->dim;
#elif defined(DB_DB) || defined(D_DB_DB) /* M1 DB */
  n = S(L)->dim1;
#elif defined(CS_D1) || defined(CS_D2)
  n = A->n;
#endif

  sol = x;

#ifndef GUESS_SOLVE 
  /** Very important to do that (the end of this algorithms use sol to build the final solution !) **/
  bzero(sol, sizeof(COEF)*n);
#endif
 
  im = MIN(option->krylov_im, itmax);
  maxits = itmax;
  eps = tol;
  
  /* allocate memory for working local arrays */
  vv = (COEF **)malloc(sizeof(COEF *)*(im+2));
  
  /** Important to do that (see the free at the end) **/
  bzero(vv, sizeof(COEF *)*(im+2));

#ifdef IM_ALLOC_AT_START
  for(i = 0; i < im+1; i++) 
    vv[i] = (COEF *)malloc(sizeof(COEF)*(n+1));
#endif

  hh = (COEF **)malloc(sizeof(COEF *) * (im+1));
  for(i = 0; i < im; i++) {
    hh[i] = (COEF *)malloc(sizeof(COEF)*(im+2));
  }

  c = (COEF *)malloc(sizeof(COEF)*(im+1));
  s = (COEF *)malloc(sizeof(COEF)*(im+1));
  rs = (COEF *)malloc(sizeof(COEF)*(im+2));
  w = (COEF **)malloc(sizeof(COEF *)*(im+1));
  
  /** Important to do that (see the free at the end) **/
  bzero(w, sizeof(COEF *)*(im+1));

#ifdef IM_ALLOC_AT_START
  for(i = 0; i < im; i++) {
    w[i] = (COEF *)malloc(sizeof(COEF)*(n+1));
  }
#endif

  /*-----------------------------------------------------------------*
  |      PART2: Iteration.                                           |
  *------------------------------------------------------------------*/
  incx = 1;
  out_flag = TRUE;
  iters = 0;
  alloc_flag = TRUE; /** OIMBE to allocate the base iteratively **/

  /* outer loop starts here */
  while(out_flag) {
#ifndef IM_ALLOC_AT_START
    if(alloc_flag == TRUE)
      vv[0] = (COEF *)malloc(sizeof(COEF)*(n+1));
#endif

    /* wk1 points to sol, wk2 points to vv[0] */
    wk1 = sol;
    wk2 = vv[0];


    /** Must fo this step even if no guess solve because of restart **/
    /* vv[0] = A*sol  */ 

#if defined(PH_PH) || defined(PH_DB) /* init PH & M1 PH */ /* init DB */
    PHIDAL_MatVec(A, BL, wk1, wk2);
#elif defined(PH)    /* M2 PH */
    PhidalPrec_SchurProd(Q, BL, wk1, wk2);
#elif defined(DB_PH) ||  defined(DB) /* mix M2 */ /* M2 DB */
    DBPrec_SchurProd(Q, BL, wk1, wk2);
    tli = PREC_B(Q)->tli;
    bri = PREC_B(Q)->bri;
#elif defined(DB_DB) /* M1 DB */
    DBMATRIX_MatVec2(L, U, BL, wk1, wk2);
#elif defined(CS_D1) || defined(CS_D2)
    matvec(A, wk1, wk2);
#elif defined(D_PH_PH) || defined(D_PH_DB) /* parallel : init PH & M1 PH */
    PhidalDistrMatrix_MatVec(0, DA, DBL, wk1, wk2);
    tli = DA->M.tli;
    bri = DA->M.bri;
#elif defined(D_PH) /* parallel : M2 PH */
    /*assert(0);*/
    PhidalDistrPrec_SchurProd(Q, DBL, wk1, wk2); 
    tli = PREC_B(Q)->M.tli;
    bri = PREC_B(Q)->M.bri;
#elif defined(D_DB_PH) 
    DBDistrPrec_SchurProd(Q, DBL, wk1, wk2);
    tli = PREC_B(Q)->M.tli; 
    bri = PREC_B(Q)->M.bri;
#elif defined(D_DB)
    DBDistrPrec_SchurProd(Q, DBL, wk1, wk2);
    tli = PREC_B(Q)->M.tli;
    bri = PREC_B(Q)->M.bri;
#elif defined(D_DB_DB)
    DBDISTRMATRIX_MatVec2(L, U, DBL, wk1, wk2);
#endif

    /*for(i=0;i<n;i++)
      fprintfv(5, stderr, "vv[0][%d] ="_coef_"\n", i, pcoef(vv[0][i]));*/
    /* rhs - wk2 */
    for(j = 0; j < n; j++) {
      vv[0][j] = rhs[j] - vv[0][j];
    }
#ifndef PARALLEL   
    /*ro = BLAS_DOT(n, vv[0], incx, vv[0], incx);*/
    ro = BLAS_NRM2(n, vv[0], incx);
#else
    /*ro = dist_ddot(proc_id, vv[0], vv[0], 0, DBL->LHID.nblock-1, DBL);*/
    ro = dist_ddot(proc_id, vv[0], vv[0], tli, bri, DBL);
    ro = sqrt(ro);
#endif

    /*fprintfv(5, stderr, "RO = %e \n", ro);*/

    /*ro = sqrt(ro);*/
    if(iters == 0)
      normrhs = ro;


    
    

#if !defined(CS_D1) && !defined(CS_D2)
#ifndef PARALLEL
    if(verbose >= 2 && fp != NULL)
#else
    if(verbose >= 2 && fp != NULL && proc_id == 0)    
#endif
      {
	int ee;
	for(ee=0;ee < P->levelnum;ee++)
	  fprintfv(2, fp, "     ");
	fprintfv(2, fp, "GMRES Iter %d   |r|/|b| = %e \n", iters, ro/normrhs );
      }
#endif
    
    if(fabs(ro-ZERO) <= EPSILON) {
      out_flag = FALSE;
      break;
    }
    t = 1.0 / ro;
    d2c = t;
    BLAS_SCAL(n, d2c, vv[0], incx);
    
    if(iters == 0)
      eps1 = eps*ro; 

    /* initialize 1-st term of rhs of hessenberg system */
    rs[0] = ro;
    i = -1;
    in_flag = TRUE;
    while(in_flag) {
      tin1 = dwalltime();
      i++;
      iters++;
#ifndef IM_ALLOC_AT_START
      if(alloc_flag == TRUE)
	w[i] = (COEF *)malloc(sizeof(COEF)*(n+1));
#endif


      i1 = i + 1;
      /* wk1 points to vv[i] */
      wk1 = vv[i];
      /* wk2 points to w[i] */
      wk2 = w[i];

      /* preconditioning operation, wk2 = M^{-1} * wk1 */
      
      tolrat = normrhs/ro;
      /*tolrat = 1;*/
      /*fprintfv(5, stderr, "TOLRAT %g TOL %g TOL*TOLRAT %g \n", tolrat, tol, tol*tolrat);*/
      t1  = dwalltime();
#define PRECOND
#ifndef PRECOND
      memcpy(wk2, wk1, sizeof(COEF)*n);
#else
      

#if defined(PH_PH) || defined(PH) || defined(DB_PH)
      if(itertab != NULL && resnormtab != NULL)
	PHIDAL_PrecSolve((tol*tolrat), P, wk2, wk1, BL, option, itertab+1, resnormtab+1);
      else
	PHIDAL_PrecSolve((tol*tolrat), P, wk2, wk1, BL, option, NULL, NULL);
#elif defined(PH_DB) || defined(DB) || defined(DB_DB)  
      if(itertab != NULL && resnormtab != NULL)
	DBMATRIX_PrecSolve((tol*tolrat), P, wk2, wk1, BL, option, itertab+1, resnormtab+1);
      else
	DBMATRIX_PrecSolve((tol*tolrat), P, wk2, wk1, BL, option, NULL, NULL);
#elif defined(D_PH_PH) || defined(D_PH) || defined(D_DB_PH)
      if(itertab != NULL && resnormtab != NULL)
	PHIDAL_DistrPrecSolve((tol*tolrat), P, wk2, wk1, DBL, option, itertab+1, resnormtab+1);
      else
	PHIDAL_DistrPrecSolve((tol*tolrat), P, wk2, wk1, DBL, option, NULL, NULL);
#elif defined(D_PH_DB) || defined(D_DB) || defined(D_DB_DB)  
      if(itertab != NULL && resnormtab != NULL)
	DBDISTRMATRIX_PrecSolve((tol*tolrat), P, wk2, wk1, DBL, option, itertab+1, resnormtab+1);
      else
	DBDISTRMATRIX_PrecSolve((tol*tolrat), P, wk2, wk1, DBL, option, NULL, NULL);
#elif  defined(CS_D1) || defined(CS_D2) 
      {
        COEF *temp = (COEF *)malloc(sizeof(COEF)*n);
	
	/**Copy wk1 because HID_solve modify it **/
	memcpy(temp, wk1, sizeof(COEF)*n);
		
	/** Solve L.x = b **/
	CSC_Lsol(1, L, temp, wk2);
	
	free(temp);
	
	/** Solve the diagonal **/
#if defined(CS_D1)
	for(j=0;j<L->n;j++)
	  wk2[j] *= L->ma[j][0];
#else /* defined(CS_D2) */
	for(j=0;j<L->n;j++)
	  wk2[j] *= D[j];
#endif
	
	/** Solve Lt.x=b **/
	CSC_Ltsol(1, L, wk2, wk2);
      }
#endif

#endif /** PRECOND **/

      t2  = dwalltime(); ttotal_PrecSolv += t2-t1;
      /* fprintfv(5, stdout, " PrecSolve in %g seconds\n\n", t2-t1); */
      /*fprintfv(5, stdout, "Norm wk2 = %g \n", norm2(wk2, n));*/

#ifndef IM_ALLOC_AT_START
      if(alloc_flag == TRUE)
	vv[i1] = (COEF *)malloc(sizeof(COEF)*(n+1));
#endif

      /* wk1 points to vv[i1] */
      wk1 = vv[i1];

      /* vv[i1] = A * (M^{-1}*vv[i]) */
      t1  = dwalltime();

#if defined(PH_PH) || defined(PH_DB) /* init PH & M1 PH */ /* init DB */
      PHIDAL_MatVec(A, BL, wk2, wk1);
#elif defined(PH)    /* M2 PH */
      PhidalPrec_SchurProd(Q, BL, wk2, wk1);
#elif defined(DB_PH) || defined(DB)  /* mix M2 */ /* M2 DB */
      DBPrec_SchurProd(Q, BL, wk2, wk1);
#elif defined(DB_DB) /* M1 DB */
      DBMATRIX_MatVec2(L, U, BL, wk2, wk1);
#elif defined(CS_D1) || defined(CS_D2)
      matvec(A, wk2, wk1);
#elif defined(D_PH_PH) || defined(D_PH_DB) /* parallel : init PH & M1 PH */
      PhidalDistrMatrix_MatVec(0, DA, DBL, wk2, wk1);
#elif defined(D_PH) /* parallel : M2 PH */
      PhidalDistrPrec_SchurProd(Q, DBL, wk2, wk1); 
#elif defined(D_DB_PH) || defined(D_DB)
      DBDistrPrec_SchurProd(Q, DBL, wk2, wk1);
#elif defined(D_DB_DB) /* M1 DB */
      DBDISTRMATRIX_MatVec2(L, U, DBL, wk2, wk1);
#endif
    
      t2  = dwalltime(); ttotal_SchurProd += t2-t1;
      /*       fprintfv(5, stdout, " SchurProd in %g seconds\n\n", t2-t1); */

      /* classical gram - schmidt */
      for(j = 0; j <= i; j++) {
#ifndef PARALLEL   
	/** L'ordre est tres important en complex !!! **/

#ifdef TYPE_REAL
	hh[i][j] = BLAS_DOT(n, vv[j], incx, vv[i1], incx); 
#endif
#ifdef TYPE_COMPLEX
	hh[i][j] = my_zdotc(n, vv[j], incx, vv[i1], incx);
#endif

#else
      /*hh[i][j] = dist_ddot(proc_id, vv[j], vv[i1], 0, DBL->LHID.nblock-1, DBL);*/
      hh[i][j] = dist_ddot(proc_id, vv[j], vv[i1], tli, bri, DBL);
#endif
      }

      for(j = 0; j <= i; j++) {
	alpha = -hh[i][j];
	BLAS_AXPY(n, alpha, vv[j], incx, vv[i1], incx);
      }

#ifndef PARALLEL   
      /*t = BLAS_DOT(n, vv[i1], incx, vv[i1], incx);*/
      t = BLAS_NRM2(n, vv[i1], incx);
#else
      /*t = dist_ddot(proc_id, vv[i1], vv[i1], 0, DBL->LHID.nblock-1, DBL);*/
      t = dist_ddot(proc_id, vv[i1], vv[i1], tli, bri, DBL);
      t = sqrt(t);
#endif

#ifdef TYPE_REAL /*todo*/
      if(CREAL(t) <= 0.0)
	{
	  fprintferr(stderr, "problem in dot product t= "_coef_" \n", pcoef(t));
	  return HIPS_ERR_KRYLOV;
	}
#endif

      hh[i][i1] = t;

      if(fabs(t) > 10e-20) {

	t = 1.0 / t;
	d2c = t;
	BLAS_SCAL(n, d2c, vv[i1], incx);
      }
      else
	{
	  fprintferr(stderr , "t IS NULL %g \n", t);
	  return HIPS_ERR_KRYLOV;
	}

      /* done with classical gram schimd and arnoldi step. now update
	 factorization of hh */
      if(i != 0) {
	for(k = 1; k <= i; k++) {
	  k1 = k - 1;
	  d2c = hh[i][k1];
#ifdef TYPE_COMPLEX
	  hh[i][k1] = conj(c[k1])*d2c + conj(s[k1])*hh[i][k];
	  hh[i][k] = -s[k1]*d2c + c[k1]*hh[i][k];
#else
	  hh[i][k1] = c[k1]*d2c + s[k1]*hh[i][k];
	  hh[i][k] = -s[k1]*d2c + c[k1]*hh[i][k];
#endif
	  /*fprintfv(5, stderr, "SIN+COS = %g \n", coefabs(c[k1])*coefabs(c[k1]) + coefabs(s[k1])*coefabs(s[k1]));*/

	}
      }
#if defined(TYPE_REAL)
      gam = sqrt(hh[i][i]*hh[i][i] + hh[i][i1]*hh[i][i1]);
#elif defined(TYPE_COMPLEX)
      {
	REAL c2d;
	REAL abs = cabs(hh[i][i]);
	assert(CIMAG(hh[i][i1]) == 0);
	c2d =  hh[i][i1];
	gam = sqrt(abs*abs + c2d*c2d);
      }
#endif

      if(fabs(gam-ZERO) <= EPSILON)
	gam = EPSMAC;
      /* determine-next-plane-rotation */
      c[i] = hh[i][i]/gam;
      s[i] = hh[i][i1]/gam;
      rs[i1] = -s[i]*rs[i];
      rs[i] = CONJ(c[i])*rs[i];
      /* determine res. norm and test for convergence */
      hh[i][i] = CONJ(c[i])*hh[i][i] + s[i]*hh[i][i1];
      ro = coefabs(rs[i1]);

      tin2 = dwalltime(); 
      ttotal_Iters += tin2-tin1;

#if !defined(CS_D1) && !defined(CS_D2)
#ifndef PARALLEL
      if(verbose >= 2 && fp != NULL)
#else
      if(verbose >= 2 && fp != NULL && proc_id == 0)    
#endif
	{
	  int ee;
	  for(ee=0;ee < P->levelnum;ee++)
	    fprintfv(2, fp, "     ");
	  fprintfv(2, fp, "GMRES Iter %d   |r|/|b| = %e \n", iters, ro/normrhs );
	/*fprintfv(3, fp, "Iter %d     res %e \n", iters, ro); */
	  
/* 	  for(ee=0;ee < P->levelnum;ee++) */
/* 	    fprintfv(3, fp, "     "); */
/* 	  fprintfv(3, stdout, " Iter %d in %g seconds\n\n", iters, tin2-tin1); */
	}
#endif
      /*fprintferr(stderr, "ro %g eps %g eps1 %g \n", ro, eps, eps1);*/


      if((i+1 >= im) || (ro <= eps1) || iters >= maxits) {
	in_flag = FALSE;
      }
    }
#ifndef IM_ALLOC_AT_START
    alloc_flag = FALSE;
#endif

    /* now compute solution first solve upper triangular system */
    rs[i] = rs[i]/hh[i][i];
    for(ii = 2; ii <= i+1; ii++) {
      k = i-ii+1;
      k1 = k+1;
      d2c = rs[k];
      for(j = k1; j <= i; j++) {
	d2c = d2c - hh[j][k]*rs[j];
      }
      rs[k] = d2c/hh[k][k];
    }
    /* done with back substitution. now form linear combination to get 
       solution */
    for(j = 0; j <= i; j++) {
      d2c = rs[j];
      BLAS_AXPY(n, d2c, w[j], incx, sol, incx);
    }
    /* test for return */

    if((ro <= eps1) || (iters >= maxits)) {
      out_flag = FALSE;
    }
  }
      
 /*----------------------------------------------------------------*
  |  PART3:    Release the working buffer                          |
  *----------------------------------------------------------------*/

  for(i = 0; i < im; i++) {
    if(w[i] != NULL)
      free(w[i]);
  }
  free(w);

  for(i = 0; i < im+1; i++){
    if(vv[i] != NULL)
      free(vv[i]);
  }
  free(vv);
  for(i = 0; i < im; i++) {
    free(hh[i]);
  }
  free(hh);
  free(c); free(s); free(rs);
  
#if !defined(CS_D1) && !defined(CS_D2)
#ifndef PARALLEL
  if(P->forwardlev == 0)
#else
  if(P->forwardlev == 0 && proc_id == 0)
#endif
  {
    fprintfv(4, stdout, " One iter in %g seconds\n", ttotal_Iters/iters);
    fprintfv(4, stdout, " PrecSolve in %g seconds\n", ttotal_PrecSolv/iters);
    fprintfv(4, stdout, " ShurProd in %g seconds\n\n", ttotal_SchurProd/iters);
  }
#endif

  if(itertab != NULL)
    itertab[0] = iters;
  if(resnormtab != NULL)
    resnormtab[0] = ro/normrhs;
  return HIPS_SUCCESS;

} /* End of fgmresd() */


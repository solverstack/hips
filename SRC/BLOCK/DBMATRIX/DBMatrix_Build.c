/* @authors J. GAIDAMOUR */

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h> /* strcmp */

#include "block.h"

#ifdef PARALLEL
#include "db_parallel.h"
#endif

void CMD(_UDBDistrMatrix,Build)(PhidalMatrix* mat, 
				dim_t tli, dim_t tlj, dim_t bri, dim_t brj, 
				char *UPLO_L, char *UPLO_U, 
				int_t locally_nbr, 
				PhidalMatrix* PhidalL, PhidalMatrix* PhidalU, 
				_UDBDistrMatrix* DBM, 
				SymbolMatrix* symbmtx, _PhidalDistrHID *_DBL,
				int_t levelnum, int_t levelnum_l,
				alloc_t alloc, flag_t filling) {

  DBM->L = (_DBDistrMatrix*)malloc(sizeof(_DBDistrMatrix));
  S(DBM->L)->symmetric = mat->symmetric;

  CMD(_DBDistrMatrix,Setup)(tli, tlj, bri, brj, UPLO_L, "N", locally_nbr, 
			   DBM->L, symbmtx, _DBL, levelnum, levelnum_l, alloc);
  
  CMD(_DBDistrMatrix,Copy)(mat, _DBL, PhidalL, DBM->L, UPLO_L, levelnum, levelnum_l, filling);


  if (mat->symmetric == 1) {

    DBM->U = NULL;

  } else {

    DBM->U = (_DBDistrMatrix*)malloc(sizeof(_DBDistrMatrix));
    S(DBM->U)->symmetric = mat->symmetric;
    
    CMD(_DBDistrMatrix,SetupV)(DBM->L, DBM->U);
    CMD(_DBDistrMatrix,Transpose)(DBM->U);
    
    CMD(_DBDistrMatrix,Copy)(mat, _DBL, PhidalU, DBM->U, UPLO_U, levelnum, levelnum_l, filling);

  } 

}

/* @authors J. GAIDAMOUR */

#include <stdio.h>
#include <stdlib.h>

#include "block.h"

#define need(arg) if ((arg) == -1) { /* printfv(5, "no %s\n", #arg); */ return -1; }

/* Preconditionner size (in nnz) before GEMM */
long DBPrec_NNZ_Before(DBPrecMem* mem)
{
  need(mem->L);
  need(mem->E_DB);

  return mem->L + mem->E_DB;
}

/* Sans Gestion Pic : */

/* Memory pic (in nnz) during preconditionner computation WITHOUT pic reduction in GEMM */
/* Pendant GEMM */
long DBPrec_NNZ_In(DBPrecMem* mem)
{
  need(mem->L);
  need(mem->E_DB);
  need(mem->SL);

  return mem->L + mem->E_DB + mem->SL;
}

/* apres GEMM et conversion en phidal si dropping */
long DBPrec_NNZ_After(int Pschur_method, DBPrecMem* mem)
{
  need(mem->L);
  need(mem->SL);
  need(mem->LS);

  if (Pschur_method == 1)
    return mem->L + mem->SL   +    mem->LS;
  else 
    return mem->L + /* mem->SL+ */ mem->LS;
}

/* Avec Gestion Pic / LL */

/* Memory pic (in nnz) during preconditionner computation when we
   desallocate EU^-1 and allocate S in GEMM */
long DBPrec_NNZ_LL(DBPrecMem* mem)
{

  need(mem->L);
  need(mem->ll);

  return mem->L + mem->ll;
}

/* Avec Gestion Pic / RL */

/* TODO : réduire le nb de fonction */
long DBPrec_NNZ_RL(DBPrecMem* mem)
{

  need(mem->L);
  need(mem->SL);
  need(mem->Emax);

  return mem->L + mem->SL + mem->Emax;
}

long DBPrec_NNZ_RL_DROPE(DBPrecMem* mem)
{

  need(mem->L);
  need(mem->dropE);
  need(mem->Emax);

  return mem->L + mem->dropE + mem->Emax;
}

long DBPrec_NNZ_RL_DROPEAfter(DBPrecMem* mem)
{
  /* TODO : ajouter S */
  need(mem->L);
  need(mem->dropEAfter);

  return mem->L + mem->dropEAfter;
}

/* @authors J. GAIDAMOUR */

#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#include <assert.h>
#include "block.h"
#include "prec.h"

#include "base.h"

/* TODO : vÃ©rifier les clean dans ce fichier */


#if defined(SYMMETRIC)

void CMD(_DBDistrPrec,FACTO_TRSM_GEMM_RL)(_DBDistrPrec* P, flag_t fillE, _PhidalDistrHID* _DBL, PhidalOptions *option/*verbose*/) 
{

  DBMatrix* L = S(PREC_L_BLOCK(P));
  DBMatrix* E = S(PREC_EDB(P));
  DBMatrix* S = S(PREC_SL_BLOCK(P)); 
  PhidalMatrix* PhidalE = S(PREC_E(P));

  dim_t i,j;
  VSolverMatrix* csL;

  int coefmax;  
  COEF *tmpE, *tmpF, *tmpW;
  int *tabA, *tabC;
  
  int Emax;
  COEF* coeftabE;

  SolverMatrix* solvE;

  chrono_t t1, t2;  
  REAL tfacto=0, ttrsm=0, tgemm=0;

  /* Allocation des vecteurs temporaires */
  coefmax = MAX(E->coefmax, L->coefmax);

  tmpE = (COEF *)malloc(sizeof(COEF)*E->coefmax);
  assert(tmpE != NULL);

  tmpF = (COEF *)malloc(sizeof(COEF)*coefmax);
  assert(tmpF != NULL);

  tmpW = (COEF *)malloc(sizeof(COEF)*coefmax);
  assert(tmpW != NULL);

  tabA = (int*)malloc(sizeof(int)*E->dim1);
  tabC = (int*)malloc(sizeof(int)*S->dim1);
  assert(tabA != NULL);
  assert(tabC != NULL);


  /* TODO : pouvoir utiliser l'autre version aussi (rÃ©duction pic ? )*/
  Emax = calc_Emax(E);
  coeftabE = (COEF*)malloc(sizeof(COEF)*Emax);
  PrecInfo_AddNNZ_(P->info, Emax, "Emax");

#ifdef WITH_PASTIX
  if(option->use_pastix == 1)
    L->pastix_str = malloc((L->brj-L->tlj+1)*sizeof(pastix_struct));
#endif // WITH_PASTIX

  for(i=L->tlj;i<=L->brj;i++)
    {

      /* FACTO */
      csL =  L->ca[ L->cia[i] ];

      t1  = dwalltime(); 

#ifdef WITH_PASTIX
      if(option->use_pastix == 1)
          VS_ICCT_pastix(L->pastix_str+(i-L->tlj), csL, tmpF, tmpW);
      else
#endif // WITH_PASTIX
          VS_ICCT(csL, tmpF, tmpW);

      t2  = dwalltime(); tfacto += t2 - t1;

      /* TRSM */
      t1  = dwalltime(); 
      /* ici, assert(i == i-L->tli */
      /* if ((E->alloc == CBLK) && (fill == INGEMM)) */
      solvE = &E->a[i];

      DBMatrix_Symb2SolvMtx_(solvE, coeftabE);
      DBMatrix_AllocSymbmtx(i, i, E, CBLK, 
			    E->cia, E->cja, E->ca, 
			    PhidalE->cia, PhidalE->cja, PhidalE->ca, 
			    solvE, "N", LHID(_DBL)); /* TODO : N ? */
      /*     } */
      /*     } */

      t1  = dwalltime(); 
      if (/* (E->alloc == ONE) TODO! || */ (E->alloc == CBLK))
	{
	  E->a[i].coefmax = E->coefmax; /*fix*/
	  VS2_InvLT(1, csL->solvmtx, &csL->symbmtx, &E->a[i], &E->a[i].symbmtx, tmpW);
	  
	} else {

	  for(j=E->cia[i];j<E->cia[i+1];j++) { 
	    E->ca[j]->solvmtx->coefmax = E->coefmax; /*fix*/
	    VS_InvLT(1, csL, E->ca[j], tmpW);
	  }

	}
      t2  = dwalltime(); ttrsm += t2 - t1;

      /* GEMM */
      t1  = dwalltime(); 
      DBMatrix_GEMMpart(i, S, -1, E, L,
			tmpE, tmpF, tmpW, tabA, tabC);
      t2  = dwalltime(); tgemm += t2 - t1;

      /* if ((E->alloc == CBLK) && (fill == INGEMM)) */
      solvE->coeftab = NULL; /* don't desallocate coeftabE */
      SolverMatrix_Clean(solvE);
      
      for(j=E->cia[i];j<E->cia[i+1];j++) {
	SymbolMatrix_Clean(&E->ca[j]->symbmtx);
      }
    }

  /* if ((E->alloc == CBLK) && (fill == INGEMM)) */
  { /* do a function <=> DBMatrix2SolverMatrix */
    DBMatrix* L = E;
    free(L->bloktab); L->bloktab = NULL; /* has been freed */
    free(L->a); L->a = NULL;             /* has been freed */
  }

  PrecInfo_SubNNZ_(P->info, Emax, "Emax");
  free(coeftabE);

  free(tmpE);
  free(tmpF);
  free(tmpW);
  free(tabA);
  free(tabC);
  
  PRINT_TIME(5,"  M : Numeric Factorisation in %g seconds\n\n", tfacto);
  PRINT_TIME(5,"  TRSM in %g seconds\n\n", ttrsm);
  PRINT_TIME(5,"  GEMM in %g seconds\n\n", tgemm);

  DUP(HDIM_tmp_apres,S(PREC_EDB(P)), S(PREC_FDB(P)));
#ifdef PARALLEL
  DBDistrMatrix_Clean(PREC_EDB(P));
#else
  DBMatrix_Clean(PREC_EDB(P));
#endif
  free(PREC_EDB(P));
  PREC_EDB(P)=NULL; /* useful ? */

}

#else

void CMD(_DBDistrPrec,FACTO_TRSM_GEMM_RLu)(_DBDistrPrec* P, flag_t fill, _PhidalDistrHID* _DBL, PhidalOptions *option/*verbose*/)
{

  DBMatrix* L = S(PREC_L_BLOCK(P));
  DBMatrix* U = S(PREC_U_BLOCK(P));
  DBMatrix* E = S(PREC_EDB(P));
  DBMatrix* F = S(PREC_FDB(P));
  DBMatrix* SL = S(PREC_SL_BLOCK(P)); 
  DBMatrix* SU = S(PREC_SU_BLOCK(P)); 

  PhidalMatrix*  PhidalE = S(PREC_E(P));
  PhidalMatrix*  PhidalF = S(PREC_F(P));

  dim_t i,j;
  VSolverMatrix* csL, *csU;

  int coefmax;
  COEF *tmpE, *tmpF, *tmpW;
  int *tabA, *tabC;
  
  int Emax;
  COEF* coeftabE, *coeftabF;

  SolverMatrix* solvE, * solvF;

  chrono_t t1, t2;
  REAL tfacto=0, ttrsm=0, tgemm=0;

  /* Allocation des vecteurs temporaires */
  coefmax = MAX(E->coefmax, L->coefmax);

  tmpE = (COEF *)malloc(sizeof(COEF)*E->coefmax);
  assert(tmpE != NULL);

  tmpF = (COEF *)malloc(sizeof(COEF)*coefmax);
  assert(tmpF != NULL);

  tmpW = (COEF *)malloc(sizeof(COEF)*coefmax);
  assert(tmpW != NULL);

  tabA = (int*)malloc(sizeof(int)*E->dim1);
  tabC = (int*)malloc(sizeof(int)*SL->dim1);
  assert(tabA != NULL);
  assert(tabC != NULL);

  /* TODO : pouvoir utiliser l'autre version aussi (rÃ©duction pic ? )*/
  Emax = calc_Emax(E);
  coeftabE = (COEF*)malloc(sizeof(COEF)*Emax);
  coeftabF = (COEF*)malloc(sizeof(COEF)*Emax);
/*   printfd("Mem : E_part  : %d\n", Emax); */
/*   printfd("Mem : F_part  : %d\n", Emax); */
  PrecInfo_AddNNZ_(P->info, 2*Emax, "Emax (*2)");

  t1  = dwalltime();
#ifdef PARALLEL
  UDBMatrix ARG; ARG.L=S(PREC_L_BLOCK(P)); ARG.U=S(PREC_U_BLOCK(P));
  DBMatrix_FACTOu(&ARG);
#else
  DBMatrix_FACTOu(&PREC_LU_BLOCK(P));
#endif
  t2  = dwalltime(); tfacto += t2 - t1;

  for(i=L->tlj;i<=L->brj;i++)
    {

      csL =  L->ca[ L->cia[i] ];
      csU =  U->ca[ U->cia[i] ];

      assert(L->cia == U->ria); /* TODO : Ã  voir ... */
      assert(L->ria == U->cia);

      t1  = dwalltime(); 
      /* if ((E->alloc == CBLK) && (fill == INGEMM)) */
      solvE = &E->a[i];

      DBMatrix_Symb2SolvMtx_(solvE, coeftabE);
      DBMatrix_AllocSymbmtx(i, i, E, CBLK,
			    E->cia, E->cja, E->ca,
			    PhidalE->cia, PhidalE->cja, PhidalE->ca,
			    solvE, "T", LHID(_DBL));

      solvF = &F->a[i];

      DBMatrix_Symb2SolvMtx_(solvF, coeftabF);
      DBMatrix_AllocSymbmtx(i, i, F, RBLK, 
			    F->ria, F->rja, F->ra, 
			    PhidalF->ria, PhidalF->rja, PhidalF->ra, 
			    solvF, "N", LHID(_DBL));

      /*     } */
      /*     } */

      if (/* (E->alloc == ONE) TODO! || */ (E->alloc == CBLK))
	{
	  E->a[i].coefmax = E->coefmax; /*fix*/
	  F->a[i].coefmax = E->coefmax; /*fix*/
	  VS2_InvLT(0, csU->solvmtx, &csU->symbmtx, &E->a[i], &E->a[i].symbmtx, tmpW);
	  VS2_InvLT(1, csL->solvmtx, &csL->symbmtx, &F->a[i], &F->a[i].symbmtx, tmpW);
	} else {
	  
	  for(j=E->cia[i];j<E->cia[i+1];j++) {
	    E->ca[j]->solvmtx->coefmax = E->coefmax; /*fix*/
	    VS_InvLT(0, csU, E->ca[j], tmpW);
	  }
	  
	  for(j=F->ria[i];j<F->ria[i+1];j++) {
	    F->ra[j]->solvmtx->coefmax = E->coefmax; /*fix*/
	    VS_InvLT(1, csL, F->ra[j], tmpW);
	   }
	  
	}
      t2  = dwalltime(); ttrsm += t2 - t1;

      t1  = dwalltime(); 
      DBMatrix_GEMMpartu(i, SL, SU, -1, E, F, 
			 tmpE, tmpF, tmpW, tabA, tabC);
      
      t2  = dwalltime(); tgemm += t2 - t1;

      /* if ((E->alloc == CBLK) && (fill == INGEMM)) */
      solvE->coeftab = NULL; /* don't desallocate coeftabE */
      SolverMatrix_Clean(solvE);

      solvF->coeftab = NULL; /* don't desallocate coeftabF */
      SolverMatrix_Clean(solvF);

      for(j=E->cia[i];j<E->cia[i+1];j++) {
	SymbolMatrix_Clean(&E->ca[j]->symbmtx);
	/* SymbolMatrix_Clean(&F->ra[j]->symbmtx); */
      }

    }
  
  /* if ((E->alloc == CBLK) && (fill == INGEMM)) */
  { /* do a function <=> DBMatrix2SolverMatrix */
    DBMatrix* L = E;
    free(L->bloktab); L->bloktab = NULL; /* has been freed */
    free(L->a); L->a = NULL;             /* has been freed */
  }

  { /* do a function <=> DBMatrix2SolverMatrix */
    DBMatrix* L = F;
    free(L->bloktab); L->bloktab = NULL; /* has been freed */
    free(L->a); L->a = NULL;             /* has been freed */
  }

  PrecInfo_SubNNZ_(P->info, 2*Emax, "Emax (*2)");
  free(coeftabE);
  free(coeftabF);

  free(tmpE);
  free(tmpF);
  free(tmpW);
  free(tabA);
  free(tabC);

  PRINT_TIME(5,"  M : Numeric Factorisation in %g seconds\n\n", tfacto);
  PRINT_TIME(5,"  TRSM in %g seconds\n\n", ttrsm);
  PRINT_TIME(5,"  GEMM in %g seconds\n\n", tgemm);
  
  DUP(HDIM_tmp_apres,S(PREC_EDB(P)), S(PREC_FDB(P)));

#ifdef PARALLEL
  DBDistrMatrix_Clean(PREC_EDB(P));
#else
  DBMatrix_Clean(PREC_EDB(P));
#endif

  free(PREC_EDB(P));
  PREC_EDB(P)=NULL; /* useful ? */
  
/* #ifdef PARALLEL */
/* /\*   DBDistrMatrix_Clean(PREC_FDB(P)); *\/ */
/* /\* #else *\/ */
/*   DBMatrix_Clean(PREC_FDB(P)); */
/* #endif */

  if(S(PREC_FDB(P))->a != NULL)
    free(S(PREC_FDB(P))->a);
  if(S(PREC_FDB(P))->bloktab != NULL)
    free(S(PREC_FDB(P))->bloktab);
  if(S(PREC_FDB(P))->ra != NULL)
    free(S(PREC_FDB(P))->ra);
  if(S(PREC_FDB(P))->ca != NULL)
    free(S(PREC_FDB(P))->ca);


  free(PREC_FDB(P));
  PREC_FDB(P)=NULL;

}
#endif

/* @authors J. GAIDAMOUR */

/* todo : scale iscale : utiliser 1 seul vecteur */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>

#include "phidal_sequential.h"
#include "block.h"
#include "prec.h"

/* #define OLD_GATHER */

#ifndef ALLOC_NOREC
#warning ALLOC_NOREC not defined
#define ALLOC_NOREC ONE
#endif

#ifndef PARALLEL
#define dm m
#endif

void regroupe(COEF *block1, COEF *block2, int width, blas_t stride);

void _DBMATRIX_MLILUPrec(_PhidalDistrMatrix *dm, _DBDistrPrec *P, SymbolMatrix* symbmtx, _PhidalDistrHID *_DBL, PhidalOptions *option)
{
  chrono_t t1,t2,ttotal;

  int cut, last;
  int_t levelnum=0;
  int Mstart, Mend, Sstart=-1, Send=-1;
 
  _UPhidalDistrMatrix * PhidalS = (_UPhidalDistrMatrix*)malloc(sizeof(_UPhidalDistrMatrix));

  _PhidalDistrPrec* phidalPrecL0=NULL;

  REAL *scalerow  = NULL; IFUNSYM(REAL *scalecol  = NULL);
  REAL *iscalerow = NULL; IFUNSYM(REAL *iscalecol = NULL);

  flag_t fillS, fillE;
  flag_t dropE;

#ifdef PARALLEL
  PhidalMatrix *m  = &dm->M;
  PhidalHID    *BL = &_DBL->LHID;
#endif

  DBPrecMem mem;
  DBPrecMem_Init(&mem);
  DBPrecMem_SetA(m, &mem);

  /** DEBUG **/
  /*if(P->info != NULL)
    assert(P->info->nnzA == mem.A);*/


#ifdef GEMM_LEFT_LOOKING
  fillS = ALLOC_FILL;
  if ((fillS == INGEMM) && ((ALLOC_S == ONE) || (ALLOC_S == RBLK) || (ALLOC_S == BLK)/*BLK a faire et a suppr*/)) {
    fillS = BEFORE;
  }

  fillE = BEFORE;
#else
  fillE = INGEMM; assert(ALLOC_E == CBLK);
  fillS = BEFORE;
#endif

#ifdef USE_DB_PRECOND
  fprintfd(stderr,"*** USE_DB_PRECOND\n");
#else
  /* TMP */
  if ((option->droptolE == 0) && (option->droptol1 == 0)) {
    option->droptol1 = -1;
    /* assert(0); */
  } 
#endif

  dropE = (option->droptolE != 0); /** Version thresh dans le GEMM : dropE == 1 **/
  if (option->forwardlev == 0) dropE = 0;
  fprintfv(5, stdout, "DROP_E = %d\n", dropE);

  /* #ifdef DEBUG_M */
  if (dropE) {
    assert(option->schur_method == 2 || option->schur_method == 0);
    /* assert(option->forwardlev == 1); */
  }

  /* #endif DEBUG_M*/

  P->symmetric    = SYMVAL;  assert(P->symmetric == m->symmetric);
  P->dim          = m->dim1; /* ( == m->dim2 ) */
  P->forwardlev   = option->forwardlev;
  P->levelnum     = levelnum; 
  P->schur_method = option->schur_method;
  P->pivoting     = 0;
  
#ifdef PARALLEL
  if(dm->commvec.init == 0)
    PhidalCommVec_Setup(0, dm, &dm->commvec, DBL); /* pour RowNorm2 */
#endif

  if(option->scale > 0) {
    scalerow  = (REAL *)malloc(sizeof(REAL)*m->dim1); IFUNSYM(scalecol  = (REAL *)malloc(sizeof(REAL)*m->dim1));
    iscalerow = (REAL *)malloc(sizeof(REAL)*m->dim1); IFUNSYM(iscalecol = (REAL *)malloc(sizeof(REAL)*m->dim1));

#ifndef UNSYMMETRIC
    CMD(_PhidalDistrMatrix,InitScale)(dm, scalerow, iscalerow, IFUNSYM(scalecol, iscalecol, option,) _DBL);
    CMD(_PhidalDistrMatrix,Scale)(dm, scalerow, IFUNSYM(scalecol,) _DBL);
#else
    CMD(_PhidalDistrMatrix,UnsymScale)(option->scalenbr, dm, _DBL, scalerow, scalecol, iscalerow, iscalecol);
#endif
  }

  /* **** */
  /************************************************************************************************************/
  /************************************************************************************************************/
  
  PRINT_MSG("Build DBPrec\n");
  t1  = dwalltime(); 
  if (option->forwardlev == 1) {
    SymbolMatrix symbmtxCutL, symbmtxCutE, symbmtxCutS;

    if (BL->nlevel == 1) {
      printfv(5, " ERROR : domain size to big : there is only one level\n");
      printfv(5, "         number of forward recursion must be set to 0\n");
      exit(1);
    }

    cut = BL->block_index[BL->block_levelindex[1]]-1; /* todo : faire comme Sstart ... etc pour les noms */
    last = symbmtx->nodenbr -1; 
    
    assert(last == BL->block_index[BL->nblock]-1);
    assert(last == symbmtx->ccblktab[symbmtx->cblknbr-1].lcolnum);
    assert(cut != last); /* sinon, pas de level2, redondant avec BL->nlevel == 1 */

    {
      /* SymbolBCblk* pAtab = (SymbolBCblk*)malloc(sizeof(SymbolBCblk)*symbmtx->cblknbr); */
      /*       initpAtab(symbmtx, pAtab); */

      SymbolMatrix_Cut2(symbmtx, &symbmtxCutL,     0,     0,  cut, cut,  -1/* , pAtab */);        
      SymbolMatrix_Cut2(symbmtx, &symbmtxCutE, cut+1,     0, last, cut,  -1/* , pAtab */);
      if (!dropE)
	SymbolMatrix_Cut2(symbmtx, &symbmtxCutS, cut+1, cut+1, last, last, -1/* , pAtab */); /* TODO : -1 !*/    

      /* free(pAtab); */
    }
        
    Mstart   =  BL->block_levelindex[levelnum];
    Mend     =  BL->block_levelindex[levelnum+1]-1;
    Sstart   =  BL->block_levelindex[levelnum+1];
    Send     =  BL->nblock-1;  
    
    PRINT_MSG("---------------------------------- L ----------------------------------\n");
    M_MALLOC(P->LU, D(BLOCK));

    CMD(_UDBDistrMatrix, Build)(m, Mstart, Mstart, Mend, Mend, "L", "U", option->locally_nbr, NULL, NULL, &PREC_LU_BLOCK(P), &symbmtxCutL, 
				_DBL, levelnum, levelnum+1, ALLOC_L, BEFORE);
  
#if defined(SYMMETRIC)
  PREC_U_BLOCK(P) = PREC_L_BLOCK(P);
#endif

    fix(&symbmtxCutL, ALLOC_L);
    SymbolMatrix_Clean(&symbmtxCutL);

    PrecInfo_AddNNZ_(P->info, DBM_NNZ("L", S(PREC_L_BLOCK(P))), "L/U"); /* L et U */

    PRINT_MSG("---------------------------------- E ----------------------------------\n");
    M_MALLOC(P->EF_DB, D(BLOCK));
    M_MALLOC(P->EF, D(SCAL));

    PREC_E(P) = (_PhidalDistrMatrix *)malloc(sizeof(_PhidalDistrMatrix));

    if ((ALLOC_E == ONE) || (ALLOC_E == CBLK))
      CMD(_UDBDistrMatrix,Build)(m, Sstart, Mstart, Send, Mend, "N", "N", option->locally_nbr, NULL/* P->E */, NULL, &PREC_EFDB(P), &symbmtxCutE, _DBL,
			  levelnum+1, BL->nlevel, ALLOC_E, fillE);
    else if (ALLOC_E == RBLK)
      CMD(_UDBDistrMatrix,Build)(m, Sstart, Mstart, Send, Mend, "N", "N", option->locally_nbr, NULL/* P->E */, NULL, &PREC_EFDB(P), &symbmtxCutE, _DBL,
			  levelnum, levelnum+1, ALLOC_E, fillE);

    fix(&symbmtxCutE, ALLOC_E);
    SymbolMatrix_Clean(&symbmtxCutE);

    /* TODO ! grouper dans le Build normalement */
    CMD(_PhidalDistrMatrix,Init)(PREC_E(P));
    CMD(_PhidalDistrMatrix,BuildVirtualMatrix)(Sstart, Mstart, Send, Mend, dm, PREC_E(P), _DBL);
    
    DBPrecMem_SetE(S(PREC_E(P)), &mem);

    PRINT_MSG("---------------------------------- F ----------------------------------\n");
    PREC_F(P) = (_PhidalDistrMatrix *)malloc(sizeof(_PhidalDistrMatrix));

#if defined(SYMMETRIC)
    CMD(_PhidalDistrMatrix,Init)(PREC_F(P));
    CMD(_PhidalDistrMatrix,BuildVirtualMatrix)(S(PREC_E(P))->tli, S(PREC_E(P))->tlj, S(PREC_E(P))->bri, S(PREC_E(P))->brj, PREC_E(P), PREC_F(P), _DBL);
    CMD(_PhidalDistrMatrix,Transpose)(PREC_F(P));

#else 
    CMD(_PhidalDistrMatrix,BuildVirtualMatrix)(Mstart, Sstart, Mend, Send, dm, PREC_F(P), _DBL);
#endif

    /* FIXME pour TRSM */
    S(PREC_EDB(P))->coefmax = MAX(S(PREC_EDB(P))->coefmax, DBMatrix_calcCoefmax(S(PREC_EDB(P)), S(PREC_L_BLOCK(P))/*ouU*/));
#ifdef UNSYMMETRIC
    S(PREC_FDB(P))->coefmax = S(PREC_EDB(P))->coefmax;
#endif

    if (fillE == BEFORE)
      PrecInfo_AddNNZ_(P->info, DBM_NNZ("N", S(PREC_EDB(P))), "E_DB"); /* E et F */

    PRINT_MSG("---------------------------------- S --------------------------------\n");

    PhidalS->L = (_PhidalDistrMatrix *)malloc(sizeof(_PhidalDistrMatrix)); /* TODO : rename PhidalC */    
    CMD(_PhidalDistrMatrix, Init)(PhidalS->L); /* parce que utilise comme une phidal non distr meme en */ 

#if defined(UNSYMMETRIC)
    PhidalS->U = (_PhidalDistrMatrix *)malloc(sizeof(_PhidalDistrMatrix));
    CMD(_PhidalDistrMatrix, Init)(PhidalS->U); 
#endif
    if (!dropE) {

      M_MALLOC(P->S, D(BLOCK));

      if(option->schur_method != 1)
        CMD(_UDBDistrMatrix,Build)(m, Sstart, Sstart, Send, Send, "L", "U", option->locally_nbr, S(PhidalS->L), S(PhidalS->U), &PREC_S_BLOCK(P), &symbmtxCutS, _DBL, 
				  levelnum+1, BL->nlevel, ALLOC_S, fillS);
      else /* S est conserve et dc calcule de maniere exacte : remplissage locally consistant */
        CMD(_UDBDistrMatrix,Build)(m, Sstart, Sstart, Send, Send, "L", "U", BL->nlevel, S(PhidalS->L) ,S(PhidalS->U), &PREC_S_BLOCK(P), &symbmtxCutS, _DBL, 
                                  levelnum+1, BL->nlevel, ALLOC_S, fillS);
      
      if (fillS == BEFORE) {
        CMD(_PhidalDistrMatrix,Clean)(PhidalS->L);
        free(PhidalS->L);
      }
      
      if ((ALLOC_S != BLK) && (ALLOC_S != ONE)) {
	/* TODO : si pas de P->S construit ..., free quand meme */
	free(symbmtxCutS.bcblktab);
      }
      /*      free(symbmtxCutS.bloktab); */
      fix(&symbmtxCutS, ALLOC_S);
      SymbolMatrix_Clean(&symbmtxCutS);
   
#if defined(SYMMETRIC)
      PREC_SU_BLOCK(P) = PREC_SL_BLOCK(P);
#elif defined(UNSYMMETRIC)
      if (fillS == BEFORE) {
        /* CMD(_PhidalDistrMatrix,Clean)(PhidalS->U); */
        free(PhidalS->U);
      }
#endif

      if (fillS == BEFORE) {
        PrecInfo_AddNNZ_(P->info, DBM_NNZ("L", S(PREC_SL_BLOCK(P))), "S"); /* S */
      } 

    } else {

      if(option->schur_method != 1)
        PhidalMatrix_Setup(Sstart, Sstart, Send, Send, TO_STRING(SYMLET), "N", option->locally_nbr, S(PhidalS->L), BL);
      else
        PhidalMatrix_Setup(Sstart, Sstart, Send, Send, TO_STRING(SYMLET), "N", BL->nlevel, S(PhidalS->L), BL);
      
      PhidalMatrix_Copy(m, S(PhidalS->L), BL);
    }

    PRINT_MSG("----------------------------------------------------------------------\n");
#ifndef BUILD_C_FOR_ITCOUNT
    if(option->schur_method == 2) /* matrice "C" conservé */
#endif
      {
	M_MALLOC(P->B, D(SCAL));
        PREC_B(P) = (_PhidalDistrMatrix *)malloc(sizeof(_PhidalDistrMatrix)); /* TODO : PREC_B(P) ou P->nextprec->B ? */
        CMD(_PhidalDistrMatrix,Init)(PREC_B(P));
	CMD(_PhidalDistrMatrix,BuildVirtualMatrix)(Sstart, Sstart, Send, Send, dm, PREC_B(P), _DBL); /* Fait 2x 												 TODO : ne pas faire comme ca */
#ifdef PARALLEL
	PhidalCommVec_Setup(0, PREC_B(P), &PREC_B(P)->commvec, DBL);
#endif
	
	DBPrecMem_SetC(S(PREC_B(P)), &mem);
      }

  } else {
    Mstart   =  BL->block_levelindex[levelnum];
    Mend     =  BL->nblock-1;  

    PRINT_MSG("---------------------------------- L ----------------------------------\n");
    M_MALLOC(P->LU, D(BLOCK));

    /* TODO : un build virtual est inutile ici a l'interieur de la fct */
    CMD(_UDBDistrMatrix,Build)(m, Mstart, Mstart, Mend, Mend, "L", "U", option->locally_nbr, NULL, NULL, 
			       &PREC_LU_BLOCK(P), symbmtx, _DBL, levelnum, BL->nlevel, ALLOC_NOREC, BEFORE);

    fix(symbmtx, ALLOC_NOREC);
    SymbolMatrix_Clean(symbmtx);
    /*     free(symbmtx); */
    /*     symbmtx = NULL; */

#if defined(SYMMETRIC)
  PREC_U_BLOCK(P) = PREC_L_BLOCK(P);
#endif

    /**/
    PrecInfo_AddNNZ_(P->info, DBM_NNZ("L", S(PREC_L_BLOCK(P))), "L/U (2)"); /* L et U */
  }
    
  t2  = dwalltime(); 
  PRINT_TIME(5," Build DBPrec in %g seconds\n\n", t2-t1);

  CMD(_DBPrecMem,Set0)(P, &mem, dropE); /* OK */
   
  /************************************************************************************************************/
  /* Solve                         ****************************************************************************/
  /************************************************************************************************************/
  ttotal = 0;
  PRINT_MSG( "DB_Precond\n");

  if (P->forwardlev == 0) {
  
    PRINT_MSG( " Numeric Factorisation (M)\n"); 
    t1  = dwalltime();
    
    HDIM_tmp_avant(S(PREC_L_BLOCK(P)));
    CMDu(_DBDistrMatrix,FACTOFACTO)(&PREC_LU_BLOCK(P), option, _DBL);
    HDIM_tmp_apres(S(PREC_L_BLOCK(P)));

    t2  = dwalltime(); ttotal += t2-t1;
    PRINT_TIME(5,"  M : Numeric Factorisation in %g seconds\n\n", t2-t1);

  } else {

    HDIM_tmp_avant(S(PREC_L_BLOCK(P)));
    DUP(HDIM_tmp_avant, S(PREC_EDB(P)), S(PREC_FDB(P)));

#ifdef GEMM_LEFT_LOOKING
    assert(dropE == 0);
    
    CMDu(_DBDistrPrec,FACTO_TRSM_GEMM_LL)(P, PhidalS, fillS, BL, &ttotal); 
#else

    if (!dropE) {
      t1  = dwalltime();
      CMDu(_DBDistrPrec,FACTO_TRSM_GEMM_RL)(P, fillE, _DBL, option);
      t2  = dwalltime(); ttotal += t2-t1;
    }
    else {
      phidalPrecL0 = (_PhidalDistrPrec *)malloc(sizeof(_PhidalDistrPrec));
      /*CMD(_PhidalDistrPrec, Init)(phidalPrecL0);*/
      CMDu(_DBDistrPrec,FACTO_TRSM_GEMM_RL_DROPE)(P, phidalPrecL0, fillE, option, _DBL, &ttotal, &mem);
    }
#endif

    HDIM_tmp_apres(S(PREC_L_BLOCK(P)));
    /* DUP(HDIM_tmp_apres, S(PREC_EDB(P)), S(PREC_FDB(P))); */
  }

  if (!dropE) {
    if (P->forwardlev > 0) {
      levelnum++;
      
#ifdef PARALLEL

	  /* printDBMatrix(S(PREC_SL_BLOCK(P)), PREC_SL_BLOCK(P)->proc_id, "avt"); */

#ifndef OLD_GATHER

	  PhidalCommVec_Init(&PREC_SL_BLOCK(P)->commvec); /* todo : utiliser DBDistrMatrix_Init */

	  DBMatrixCommVec_Setup(0, PREC_SL_BLOCK(P),/*  &PREC_SL_BLOCK(P)->commvec, */ DBL);
	  DBDistrMatrix_GatherCoef(0, 1, PREC_SL_BLOCK(P), DBL); 
	  PhidalCommVec_Clean(&PREC_SL_BLOCK(P)->commvec);

#endif
	  /* printDBMatrix(S(PREC_SL_BLOCK(P)), PREC_SL_BLOCK(P)->proc_id, "apres"); */
#endif

      if (option->droptol1 != 0)
        {
          int dim = S(PREC_SL_BLOCK(P))->dim1;
          int scaletmp;

          /* cout traduction DB/Phidal non prise en compte */
	  PrecInfo_SubNNZ_(P->info, DBM_NNZ("L", S(PREC_SL_BLOCK(P))), "S");
         
          PRINT_MSG(" Convert DB2Phidal ...\n");
          t1  = dwalltime();
          
          P->phidalS = (_PhidalDistrMatrix*)malloc(sizeof(_PhidalDistrMatrix));
	  
          if(option->schur_method != 1) {

#ifndef PARALLEL
            PhidalMatrix_Init_fromDB(PREC_SL_BLOCK(P), P->phidalS, "L", option->locally_nbr, _DBL);
#else
            PhidalDistrMatrix_Init_fromDBDistr(PREC_SL_BLOCK(P), P->phidalS, "L", option->locally_nbr, _DBL);
#endif


            _DBMatrix2PhidalMatrix(&PREC_S_BLOCK(P), P->phidalS, option->locally_nbr, _DBL,  0/*contigue block of mem*/IFSYM(, 0/*non-unit diag*/));

      
          } else { /* S est conservÃÂÃÂ© et dc calculÃÂÃÂ© de maniÃÂÃÂ¨re exacte : remplissage locally consistant */
#ifndef PARALLEL
            PhidalMatrix_Init_fromDB(PREC_SL_BLOCK(P), P->phidalS, "L", BL->nlevel, _DBL);
#else
            PhidalDistrMatrix_Init_fromDBDistr(PREC_SL_BLOCK(P), P->phidalS, "L", BL->nlevel, _DBL);
#endif
            _DBMatrix2PhidalMatrix(&PREC_S_BLOCK(P), P->phidalS, BL->nlevel, _DBL,  0/*contigue block of mem*/ IFSYM(, 0/*non-unit diag*/));

          }
          assert(S(P->phidalS)->symmetric == SYMVAL);

          free(PREC_SL_BLOCK(P));
          PREC_SL_BLOCK(P) = NULL; /* DBMatrix_Clean in DBMatrix2PhidalMatrix() */

#ifdef UNSYMMETRIC
          free(PREC_SU_BLOCK(P));
          PREC_SU_BLOCK(P) = NULL; /* DBMatrix_Clean in DBMatrix2PhidalMatrix() */
#endif

          t2  = dwalltime(); ttotal += t2-t1;
	  PRINT_TIME(5,"  Convert DB2Phidal in %g seconds\n\n", t2-t1);
	  
	  CMD(_DBPrecMem,SetPhidalS)(P, &mem); /* OK */
	  /* PrecInfo_AddNNZ_(P->info, PhidalMatrix_NNZ(S(P->phidalS)), "phidalS"); ??  */

	  PRINT_MSG(" Call  PhidalPrec_MLI"TO_STRING(SYMMET)" ...\n");
	  t1  = dwalltime();
	  P->phidalPrec = (_PhidalDistrPrec*)malloc(sizeof(_PhidalDistrPrec));
	  CMD(_PhidalDistrPrec,Init)(P->phidalPrec);
	  
	  P->phidalPrec->symmetric = P->symmetric; 
	  P->phidalPrec->dim = dim;
	  P->phidalPrec->levelnum = levelnum;
	  P->phidalPrec->forwardlev = option->forwardlev -1;
	  P->phidalPrec->schur_method = option->schur_method;
	  P->phidalPrec->pivoting = 0;  /** 1 := column pivoting in ILUTP */
	  P->phidalPrec->info = P->info; /* shared */

	  /* P->phidalPrec->tol_schur; /\* non utilisÃÂ© ?*\/ */
	  
	  /* hack */
	  if (option->droptol1 == -1) {
	    option->droptol1 = 0;
	  }

	  assert(S(P->phidalS)->symmetric == SYMVAL);
	  assert(S(P->phidalS)->csc == SYMVAL);

	  scaletmp = option->scale; option->scale = 0; /*hack*/

#ifdef OLDCOUNT
	  /* printf("(avt) P->info=%ld", (long)P->info->nnzP);  */
	  long tmp=P->info->nnzP; long tmpA=P->info->peak;
#else
	  PrecInfo_AddNNZ_(P->info, CMD(_PhidalDistrMatrix,NNZ)(P->phidalS), "PhidalS (new count)");
#endif

#ifndef PARALLEL
	  
	  _PhidalPrec_MLILUT(levelnum, option->forwardlev -1, NULL/*droptab ?*/, P->phidalS, P->phidalPrec, BL, option);
#else /* PARALLEL */

/* 	  P->phidalPrec->L = (PhidalDistrMatrix *)malloc(sizeof(PhidalDistrMatrix)); */
/* 	  P->phidalPrec->D = (COEF *)malloc(sizeof(COEF)*P->phidalPrec->dim); */
#ifdef OLD_GATHER
	  fprintfv(5, stderr, "Warning : OLD_GATHER flag\n");

	  /* printPhidalMatrix(S(P->phidalS), P->phidalS->proc_id, "avt"); */

	  PhidalCommVec_Setup(0, P->phidalS, &P->phidalS->commvec, DBL);
	  PhidalDistrMatrix_GatherCoef(0, 1, P->phidalS, DBL); /** OIMBE: vaut mieux le faire sur la DB avant la conversion en phidalmatrix **/
	  PhidalCommVec_Clean(&P->phidalS->commvec);

	  /* printPhidalMatrix(S(P->phidalS), P->phidalS->proc_id, "apres"); */
#endif
	  
	  S(P->phidalS)->virtual =0;

#ifndef UNSYMMETRIC
	  PhidalDistrPrec_MLICCT(levelnum, option->forwardlev -1, NULL/* droptab */, P->phidalS, P->phidalPrec, DBL, option);
#else
	  PhidalDistrPrec_MLILUT(levelnum, option->forwardlev -1, NULL/* droptab */, P->phidalS, P->phidalPrec, DBL, option);
#endif

#ifdef OLDCOUNT
	  /* printf("(after) P->info=%ld\n", (long)P->info->nnzP);*/
	  P->info->nnzP=tmp;P->info->peak=tmpA;
#endif

	  PhidalCommVec_Setup(1, PREC_L_SCAL(P->phidalPrec), &PREC_L_SCAL(P->phidalPrec)->commvec, DBL);
	  PhidalCommVec_Setup(2, PREC_U_SCAL(P->phidalPrec), &PREC_U_SCAL(P->phidalPrec)->commvec, DBL); 

#endif /* PARALLEL */
#ifdef OLDCOUNT
	  PrecInfo_AddNNZ_(P->info, CMD(_PhidalDistrPrec,NNZ)(P->phidalPrec), "PhidalPrec (LS/US)");
#endif

          option->scale = scaletmp;

          if (P->schur_method != 1) {
            /* PhidalMatrix_Clean(PhidalS->L); */
            free(P->phidalS);
            P->phidalS = NULL;
          }
          
          assert(P->nextprec   == NULL);
          assert(((P->schur_method == 1) && (PREC_SL_BLOCK(P) == NULL) && (P->phidalS != NULL)) ||
                 ((P->schur_method != 1) && (PREC_SL_BLOCK(P) == NULL) && (P->phidalS == NULL)));
          
          t2  = dwalltime(); ttotal += t2-t1;
          PRINT_TIME(5,"  Call PhidalPrec_MLICCT in %g seconds\n\n", t2-t1);
	  
	  CMD(_DBPrecMem,Set1)(P, &mem); /* OK : par PhidalDistrPrec_... */
	} else { /* (option->droptol1 == 0) */
	  
#ifdef PARALLEL
/* 	  assert(0); */
#endif
	  t1  = dwalltime(); 
          P->nextprec = (_DBDistrPrec *)malloc(sizeof(_DBDistrPrec));
          CMD(_DBDistrPrec,Init)(P->nextprec);
          P->nextprec->symmetric = P->symmetric;
          P->nextprec->dim = S(PREC_SL_BLOCK(P))->dim1;
          
          P->nextprec->prevprec = P;
          
	  P->nextprec->forwardlev = option->forwardlev -1;
	  P->nextprec->levelnum = levelnum; /* todo : levelnum doublon de forward en terme d'info ? */
	  P->nextprec->schur_method = option->schur_method;
	  
#ifdef PARALLEL
	  /* printDBMatrix(S(PREC_SL_BLOCK(P)), PREC_SL_BLOCK(P)->proc_id, "db-avt"); */
#endif

	  if(option->schur_method != 1)
            {
	      M_MALLOC(P->nextprec->LU, D(BLOCK));
              /** The factorization is done in place (A is a void matrix in return) **/
#if defined(SYMMETRIC)
              PREC_L_BLOCK(P->nextprec) = PREC_U_BLOCK(P->nextprec) = PREC_SL_BLOCK(P); /* et ou PREC_SL_BLOCK(P)=NULL! */
#elif defined(UNSYMMETRIC)
              PREC_L_BLOCK(P->nextprec) = PREC_SL_BLOCK(P);
              PREC_U_BLOCK(P->nextprec) = PREC_SU_BLOCK(P);
#endif

            } else {
	    M_MALLOC(P->nextprec->LU, D(BLOCK));

#if defined(SYMMETRIC)
	    PREC_L_BLOCK(P->nextprec) = PREC_U_BLOCK(P->nextprec) = (_DBDistrMatrix*)malloc(sizeof(_DBDistrMatrix));
#elif defined(UNSYMMETRIC)
              PREC_L_BLOCK(P->nextprec) = (_DBDistrMatrix*)malloc(sizeof(_DBDistrMatrix));
              PREC_U_BLOCK(P->nextprec) = (_DBDistrMatrix*)malloc(sizeof(_DBDistrMatrix));
#endif    

#ifndef PARALLEL
	      /* si alloc BLK, ca ne marche pas a cause de copy3 */ assert(PREC_SL_BLOCK(P)->a  != NULL);
              _DBMatrix_Copy3(&PREC_S_BLOCK(P), &PREC_LU_BLOCK(P->nextprec), levelnum, option->locally_nbr, BL);
#else
              {
                DBDistrMatrix* m;
                SymbolMatrix symbmtxCutS;
                
                 m = PREC_L_BLOCK(P->nextprec);
            
                 DBDistrMatrix_Init(m); /* inutile ? */
                 m->M.symmetric = P->symmetric;
	    
                 m->M.alloc = ALLOC_S2;
                 DBDistrMatrix_Setup_HID(Sstart, Sstart, Send, Send, "L", "N", option->locally_nbr, m, DBL);
                 DBMatrix_HID2SymbolMatrix(&m->M, &symbmtxCutS, BL, 
                                           PREC_SL_BLOCK(P)->M.a[0].symbmtx.facedecal, 
                                           PREC_SL_BLOCK(P)->M.a[0].symbmtx.tli/*==fcolnum, pas besoin d'arg*/);
            
                 m->M.cblknbr = symbmtxCutS.cblknbr;
                 m->M.nodenbr = symbmtxCutS.nodenbr;
		 /* 	m->ccblktab = NULL; /\* car != RBLK*\/ */
		 { /* cf DBMatrix_Setup */
		   if (m->M.alloc == RBLK) {
		     m->M.ccblktab = symbmtxCutS.ccblktab; 
		     /*     symbmtxCutS.ccblktab = NULL; TODO! */
		   } else {
		     m->M.ccblktab = NULL;
		   }
	      
		   /* tmp */
		   if (m->M.alloc == CBLK) {
		     m->M.ccblktab = symbmtxCutS.ccblktab; 
		   }
		 }

		 /* m->hdim = NULL;     /\* car != RBLK*\/ */ /* DANS DBMATRIX_SETUP_SYMBOL !, alloc pour RBLK BLK */
		 DBDistrMatrix_Setup_SYMBOL(m, &symbmtxCutS, DBL, levelnum+1-1, BL->nlevel);
		 fix(&symbmtxCutS, ALLOC_S2);
		 SymbolMatrix_Clean(&symbmtxCutS);
	    
                 /***/
                 m->M.coefmax = DBMatrix_calcCoefmax(&m->M, &m->M);
            
                 /* A optim : quand c'est possible, utiliser DBMatrix_Cpy(PREC_SL_BLOCK(P), m); old */
                 /* A optim : reprendre les Cut dejÃÂÃÂ  rÃÂÃÂ©alisÃÂÃÂ©s pour S ! Ne refaire que Setup2 si necessaire */
            
                 /* Equivalent du DBMatrix_Copy, sauf que la source est une DBMatrix et non une phidal */
                 /* printfv(5, "%d %d\n",PREC_SL_BLOCK(P)->a[0].symbmtx.facedecal, m->a[0].symbmtx.facedecal); */
            
		 DBDistrMatrix_Copy2(PREC_SL_BLOCK(P), m);    /* TODO : uniformiser les noms */

              }
#endif

	      /**/
#if defined(SYMMETRIC)
              assert(P->nextprec   != NULL);
              assert(P->phidalPrec == NULL);
              assert(((P->schur_method == 1) && (PREC_SL_BLOCK(P) != NULL) && (P->phidalS == NULL)) ||
                     ((P->schur_method != 1) && (PREC_SL_BLOCK(P) == NULL) && (P->phidalS == NULL)));
              
#elif defined(UNSYMMETRIC)
#ifndef PARALLEL
              {
                _DBDistrMatrix* L = PREC_SL_BLOCK(P);
                _DBDistrMatrix* U = PREC_SU_BLOCK(P);
              
                dim_t i,k,p;
                int ilast;
	      
		COEF *solvmtxLptr, *solvmtxUptr;
		int width, stride;
	      
		SolverMatrix *solvL, *solvU;
		SymbolMatrix* symbmtx;
	      
		if (S(L)->alloc == ONE)
		  ilast = 1;
		else
		  ilast = S(L)->brj - S(L)->tlj + 1;
	      
		/* parcours par blocs colonnes */
		for(i=0;i<ilast;i++) {
		  solvL = &L->a[i];
		  solvU = &U->a[i];
		  symbmtx = &solvL->symbmtx;
		
		  for(k=0;k<symbmtx->cblknbr;k++) {
		    p    = symbmtx->bcblktab[k].fbloknum; /* triangular block */
		    width  = symbmtx->ccblktab[k].lcolnum - symbmtx->ccblktab[k].fcolnum +1;
		    stride = symbmtx->stride[k];
		    solvmtxLptr = solvL->coeftab + solvL->bloktab[p].coefind;
		    solvmtxUptr = solvU->coeftab + solvU->bloktab[p].coefind;
		  
		    regroupe(solvmtxLptr, solvmtxUptr, width, stride);
		  }
		}
	      }
#else
	      assert(0); /* PARALLEL UNSYM : TODO */
#endif 
#endif    
	      /**/
	    
	    } /* if option->schur_method != 1 */
	
	  t2  = dwalltime(); ttotal += t2-t1;
	  if(option->schur_method == 1)
            PRINT_TIME(5,"  Copy schur complement before facto in %g seconds (STRAT_1)\n\n", t2-t1);
    
          if(option->schur_method == 1)
            PrecInfo_AddNNZ_(P->info, DBM_NNZ("L", S(PREC_SL_BLOCK(P))), "SL"); /* S Copy STRAT 1 */

          /*** ***/
          CMD(_DBPrecMem,Set1)(P, &mem);
          PRINT_MSG( " Numeric Factorisation (S)\n"); 

          assert(S(PREC_L_BLOCK(P->nextprec))->alloc == S(PREC_SL_BLOCK(P))->alloc);

          HDIM_tmp_avant(S(PREC_L_BLOCK(P->nextprec)));
          
          t1  = dwalltime(); 

#ifndef PARALLEL

  #ifdef DEBUG_J

          DBMatrix_ICCT(PREC_L_BLOCK(P->nextprec), BL, option);

  #else /* DEBUG_J */

    #if defined(SYMMETRIC)
          if (PREC_L_BLOCK(P->nextprec)->alloc != BLK)
            DBMatrix_FACTO(&PREC_LU_BLOCK(P->nextprec));
          else
            DBMatrix_FACTO2(&PREC_LU_BLOCK(P->nextprec));
    #elif defined(UNSYMMETRIC)
          DBMatrix_FACTOu(&PREC_LU_BLOCK(P->nextprec));
    #endif

  #endif /* DEBUG_J */

#else /* PARALLEL */
	  printf("DBDistrMatrix_ICCT\n");
          DBDistrMatrix_ICCT(0, PREC_L_BLOCK(P->nextprec), DBL, option);
#endif /* PARALLEL */
        
          t2  = dwalltime(); ttotal += t2-t1;
          
          HDIM_tmp_apres(S(PREC_L_BLOCK(P->nextprec)));
          
          PRINT_TIME(5,"  S : Numeric Factorisation in %g seconds\n\n", t2-t1);
          /*** ***/
	  
	} /* droptol1 */
      
    } /* recursion */


  } else { /* dropE */
    t1  = dwalltime();

    assert(P->forwardlev > 0);
    assert(option->schur_method == 2 || option->schur_method == 0);

    M_MALLOC(phidalPrecL0->S,  D(SCAL));
    PREC_S_SCAL(phidalPrecL0) = PhidalS->L;

#ifdef SYMMETRIC
    phidalPrecL0->D = (COEF*)malloc(sizeof(COEF)*S(PREC_L_BLOCK(P))->nodenbr);
    DBMatrix_GetDiag(S(PREC_L_BLOCK(P)), phidalPrecL0->D);
#endif

    P->phidalPrec = (_PhidalDistrPrec *)malloc(sizeof(_PhidalDistrPrec)); /* == nextprec */
    CMD(_PhidalDistrPrec,Init)(P->phidalPrec);
    P->phidalPrec->info = P->info;

    /* P->phidalPrec->nextprec->prevprec = PhidalPrecL1; */

#ifdef OLDCOUNT
	  /* printf("(avt) P->info=%ld", (long)P->info->nnzP);  */
	  long tmp=P->info->nnzP; long tmpA=P->info->peak;
	  /* dans DBPrec_FACTO_TRSM_GEMM_RL_DROP.c, E est soustrait */
#else
	  PrecInfo_AddNNZ_(P->info, CMD(_PhidalDistrMatrix,NNZ)(PREC_S_SCAL(phidalPrecL0)), "PhidalC (new count)");
#endif

#ifdef SYMMETRIC
	  CMD(_PhidalDistrPrec,GEMM_ICCT)(levelnum+1, NULL/* droptab */, PREC_E(phidalPrecL0), PREC_S_SCAL(phidalPrecL0), phidalPrecL0->D, 
				    P->phidalPrec/* nextprec */, _DBL, option);
#else
	  CMD(_PhidalDistrPrec,GEMM_ILUCT)(levelnum+1, NULL/* droptab */, PREC_E(phidalPrecL0), PREC_F(phidalPrecL0), PREC_S_SCAL(phidalPrecL0),
				     P->phidalPrec/* nextprec */, _DBL, option);

#endif

#ifdef OLDCOUNT
	  /* printf("(after) P->info=%ld\n", (long)P->info->nnzP);*/
	  P->info->nnzP=tmp;P->info->peak=tmpA;
	  /* equivalent _DBPrecMem,SetDropEAfter */
	  {
	    _PhidalDistrPrec* P2 = P->phidalPrec;
	    long add = PhidalMatrix_NNZ(S(P2->L));
	    
	    if(P2->symmetric == 0)
	      add += PhidalMatrix_NNZ(S(P2->U));
	    else
	      add += (long)S(P2->L)->dim1;
	    
	    PrecInfo_AddNNZ_(P->info, add, "oldcount S dropE");
	  }
#endif


#ifdef SYMMETRIC
    free(phidalPrecL0->D); phidalPrecL0->D = NULL;
#endif

    CMD(_PhidalDistrMatrix,Clean)(PREC_S_SCAL(phidalPrecL0));
    free(PREC_S_SCAL(phidalPrecL0)); PREC_S_SCAL(phidalPrecL0) = NULL;
          
    /**/
    assert(PREC_EDB(P) == NULL);
    assert(PREC_FDB(P) == NULL);

    CMD(_PhidalDistrMatrix,Clean)(PREC_E(phidalPrecL0)); 
    free(PREC_E(phidalPrecL0)); PREC_E(phidalPrecL0) = NULL;

    M_CLEAN(phidalPrecL0->LU); phidalPrecL0->LU=NULL; /* hack tant que malloc dans PhidalPrec_Init pour appeller Clean (LU inutilisé) */
    CMD(_PhidalDistrPrec,Clean)(phidalPrecL0);
    free(phidalPrecL0);
    
    t2  = dwalltime(); ttotal += t2-t1;
    PRINT_TIME(5,"  GEMM + ICCT S in %g seconds\n\n", t2-t1);

#ifdef PARALLEL
    PhidalCommVec_Setup(1, PREC_L_SCAL(P->phidalPrec), &PREC_L_SCAL(P->phidalPrec)->commvec, DBL);
    PhidalCommVec_Setup(2, PREC_U_SCAL(P->phidalPrec), &PREC_U_SCAL(P->phidalPrec)->commvec, DBL); 
#endif

    CMD(_DBPrecMem,SetDropEAfter)(P->phidalPrec, &mem); /* OK : dans GEMM_ILUCT */

  } /* dropE */

  PRINT_TIME(5," DB_Precond in %g seconds \n\n", ttotal);
  /************************************************************************************************************/

  if(option->scale > 0) {
    /** Unscale the matrix **/
    /* if(levelnum == 0 || option->schur_method == 1) */
    CMDus(_PhidalDistrMatrix,Scale)(dm, iscalerow, IFUNSYM(iscalecol,) _DBL);

    /** Unscale the preconditioner **/
    CMDus(_DBDistrPrec,Unscale)(scalerow, iscalerow, IFUNSYM(scalecol, iscalecol,) P, _DBL);

#ifdef WITH_PASTIX
    if(option->use_pastix == 1) {
      DBMatrix* L = S(PREC_L_BLOCK(P));
      VSolverMatrix* csL; 
      int nodenbr = 0;
      int i;
      for(i=L->tlj;i<=L->brj;i++) {
	csL =  L->ca[ L->cia[i] ];

	IFSYM(pastix_unscale_sym(L->pastix_str+(i-L->tlj), scalerow + nodenbr, iscalerow + nodenbr));	  
	IFUNSYM(pastix_unscale_unsym(L->pastix_str+(i-L->tlj), scalerow + nodenbr, iscalerow + nodenbr, scalecol + nodenbr, iscalecol + nodenbr));

	nodenbr += (&csL->symbmtx)->nodenbr;
      }
    }
#endif // WITH_PASTIX

    DUP(free, scalerow, scalecol);
    DUP(free, iscalerow, iscalecol);

}

#ifndef PARALLEL

  /*#define MIX_LOC_STRICT*/
#ifdef MIX_LOC_STRICT
  PRINT_MSG("*** Warning : MIX_LOC_STRICT\n"); 
  assert(option->locally_nbr > 0);

  {
    DBMatrix* PnextprecL = (DBMatrix*)malloc(sizeof(DBMatrix));
#ifdef UNSYMMETRIC
    DBMatrix* PnextprecU = (DBMatrix*)malloc(sizeof(DBMatrix));
#endif

    _DBMatrix_Copy3(PREC_L_BLOCK(P->nextprec), PREC_U_BLOCK(P->nextprec), PnextprecL, PnextprecU, levelnum, 0/* option->locally_nbr */, BL);

    DUP(DBMatrix_Clean, PREC_L_BLOCK(P->nextprec), PREC_U_BLOCK(P->nextprec));

#if defined(SYMMETRIC)
    PREC_L_BLOCK(P->nextprec) = PREC_U_BLOCK(P->nextprec) = PnextprecL;
#elif defined(UNSYMMETRIC)
    PREC_L_BLOCK(P->nextprec) = PnextprecL;
    PREC_U_BLOCK(P->nextprec) = PnextprecU;
#endif
  }
#endif

#endif /* PARALLEL */

#ifdef PARALLEL
/* PhidalCommVec_PrecSetup(P, DBL); */

  if(P->forwardlev  > 0)
    {
      PhidalCommVec_Setup(0, PREC_E(P), &PREC_E(P)->commvec, DBL);
      PhidalCommVec_Setup(0, PREC_F(P), &PREC_F(P)->commvec, DBL);
      
      assert(P->schur_method != 1);
      /* if(P->schur_method == 1) */
      /* 	PhidalCommVec_Setup(0, P->S, DBL); */

      /* already done */
      /*       if(P->schur_method == 2) */
      /* 	PhidalCommVec_Setup(0, PREC_B(P), DBL); */

      /* already done */      
/*       if(P->phidalPrec != NULL) */
/*      PhidalCommVec_PrecSetup(P->phidalPrec, DBL); */
    }
  
  /*   PhidalCommVec_Setup(1, PREC_L_BLOCK(P), DBL); */
  /*   PhidalCommVec_Setup(2, PREC_U_BLOCK(P), DBL);  */
  
#endif

  CMD(_DBPrecMem,print)(P, &mem, _DBL, option->verbose);

#ifdef PARALLEL
 {
   DBPrecMem memsum;
   DBPrecMem_Init(&memsum);

   DBDistrPrecMem_reduce(P, &mem, &memsum, MPI_COMM_WORLD);

   if (DBL->proc_id == 0) { 
     printfv(5, "------------------------\n");
     DBDistrPrecMem_print(P, &memsum, NULL, option->verbose);
     printfv(5, "------------------------\n");
   }
/*  } */
/*  { */
#ifdef BUG
   DBPrecMem memmax;
   DBPrecMem_Init(&memmax);

   DBDistrPrecMem_reduceMAX(P, &mem, &memmax, MPI_COMM_WORLD);

   if (DBL->proc_id == 0) { 
     printfv(5, "------------------------\n");
/*      DBDistrPrecMem_print(P, &memmax, NULL, option->verbose); */
     double a = DBPrec_NNZ_After(P->schur_method, &memmax);
     double b = DBPrec_NNZ_After(P->schur_method, &memsum);
     /*TODO : attention, pas valide en M0 car After GEMM + important que nnz(P)final*/
     /* de meme si droptol */
/*      BUGGE, donne pas les meme results que l'autre affichage, mais l'autre affichaeg OK */
/* voir TEST0 ET TEST1 */
/*      printf("TEST0 : %lf %lf %d\n", a, b, DBL->nproc); */
/*      printf("BALANCE     : %g \n", a/(b/DBL->nproc)); */
/*      printf("BALANCE PIC : %g \n", a/(b/DBL->nproc)); */
     printfv(5, "------------------------\n");
   }
#endif
 }
#endif
 
 free(PhidalS); /* todo : faire mieux */
 
}

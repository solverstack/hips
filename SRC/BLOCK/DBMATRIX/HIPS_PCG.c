/* @authors J. GAIDAMOUR, P. HENON */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>
#include <assert.h>

#if defined(D_PH_PH) || defined(D_PH) || defined(D_PH_DB) || defined(D_DB_PH) /* parallel */
#define PARALLEL
#include "phidal_parallel.h"
#endif

#if defined(DB_PH) || defined(PH_DB) || defined(DB) || defined(DB_DB) || defined(D_DB_PH)
#include "block.h"
#include "prec.h"
#endif

#if defined(D_DB_PH) || defined(D_PH_DB)
#include "db_parallel.h"
#endif

#include "phidal_sequential.h"

/* todo: include des .c */

/*#define GUESS_SOLVE*/

#if defined(PH_PH)   /* init PH & M1 PH */
     INTS HIPS_PCG_PH_PH(flag_t verbose, REAL tol, dim_t itmax, 
			    PhidalMatrix *A, PhidalPrec *P, 
			    PhidalHID *BL, PhidalOptions *option, 
			    COEF *rhs, COEF *x, FILE *fp, dim_t *itertab, REAL *resnormtab)
#elif defined(PH)    /* M2 PH */
     INTS HIPS_PCG_PH   (flag_t verbose, REAL tol, dim_t itmax, 
			    PhidalPrec *P, 
			    PhidalHID *BL, PhidalOptions *option, 
			    COEF *rhs, COEF *x, FILE *fp, dim_t *itertab, REAL *resnormtab)
#elif defined(DB_PH) /* mix M2 */
     INTS HIPS_PCG_DB_PH(flag_t verbose, REAL tol, dim_t itmax, 
			    DBPrec *Q, PhidalPrec *P, 
			    PhidalHID *BL, PhidalOptions *option, 
			    COEF *rhs, COEF * x, FILE *fp, dim_t *itertab, REAL *resnormtab)
#elif defined(PH_DB)   /* init DB */
     INTS HIPS_PCG_PH_DB(flag_t verbose, REAL tol, dim_t itmax, 
			    PhidalMatrix *A, DBPrec *P, 
			    PhidalHID *BL, PhidalOptions *option, 
			    COEF *rhs, COEF *x, FILE *fp, dim_t *itertab, REAL *resnormtab)
#elif defined(DB)    /* M2 DB */
     INTS HIPS_PCG_DB   (flag_t verbose, REAL tol, dim_t itmax, 
			    DBPrec *P, 
			    PhidalHID *BL, PhidalOptions *option, 
			    COEF *rhs, COEF *x, FILE *fp, dim_t *itertab, REAL *resnormtab)
#elif defined(DB_DB) /* M1 DB */
     INTS HIPS_PCG_DB_DB(flag_t verbose, REAL tol, dim_t itmax, 
			    DBMatrix *L, DBMatrix *U, DBPrec *P, 
			    PhidalHID *BL, PhidalOptions *option, 
			    COEF *rhs, COEF *x, FILE *fp, dim_t *itertab, REAL *resnormtab)
#elif defined(CS_D1) /* CS 1 */
     INTS HIPS_PCG_CS_D1(flag_t verbose, REAL tol, dim_t itmax, 
			    csptr A, csptr L,
			    PhidalOptions *option, 
			    COEF *rhs, COEF *x, FILE *fp, dim_t *itertab, REAL *resnormtab)
#elif defined(CS_D2) /* CS 2 */
     INTS HIPS_PCG_CS_D2(flag_t verbose, REAL tol, dim_t itmax, 
			    csptr A, csptr L, COEF *D,
			    PhidalOptions *option, 
			    COEF *rhs, COEF *x, FILE *fp, dim_t *itertab, REAL *resnormtab)
#elif defined(D_PH_PH) /* parallel : init PH & M1 PH */
     INTS HIPS_DistrPCG_PH_PH(flag_t verbose, REAL tol, dim_t itmax, 
				 PhidalDistrMatrix *DA, PhidalDistrPrec *P,
				 PhidalDistrHID *DBL, PhidalOptions *option, 
				 COEF *rhs, COEF * x, FILE *fp, dim_t *itertab, REAL *resnormtab)  
#elif defined(D_PH) /* parallel : M2 PH */
     INTS HIPS_DistrPCG_PH(flag_t verbose, REAL tol, dim_t itmax, 
			      PhidalDistrPrec *P, 
			      PhidalDistrHID *DBL, PhidalOptions *option, 
			      COEF *rhs, COEF * x, FILE *fp, dim_t *itertab, REAL *resnormtab) 
#elif defined(D_DB_PH) /* mix M2 */
     INTS HIPS_DistrPCG_DB_PH(flag_t verbose, REAL tol, dim_t itmax, 
			     DBDistrPrec *Q, PhidalDistrPrec *P, 
			     PhidalDistrHID *DBL, PhidalOptions *option, 
			     COEF *rhs, COEF * x, FILE *fp, dim_t *itertab, REAL *resnormtab)
#elif defined(D_PH_DB)   /* parallel init DB */
     INTS HIPS_DistrPCG_PH_DB(flag_t verbose, REAL tol, dim_t itmax, 
			     PhidalDistrMatrix *DA, DBDistrPrec *P, 
			     PhidalDistrHID *DBL, PhidalOptions *option, 
			     COEF *rhs, COEF * x, FILE *fp, dim_t *itertab, REAL *resnormtab)  
#endif
{

#ifdef TYPE_REAL
  dim_t i, n;
  int its;
  COEF *r;
  REAL ro;
  REAL normb;
  REAL alpha;
  /* REAL tmploc; */
  REAL beta;
  COEF *p;
  COEF *Ap;
  COEF *z;
  COEF *tmp;
  COEF rz;
#ifndef PARALLEL
  blas_t incx = 1;
#endif
  REAL tolrat;

#if defined(PH)    /* M2 PH */
  PhidalPrec *Q;
#elif defined(DB)    /* M2 DB */
  DBPrec *Q;
#elif defined(D_PH_PH) || defined(D_PH_DB) /* parallel : init PH & M1 PH */
  PhidalMatrix *A = &DA->M;
#elif defined(D_PH)    /* parallel : M2 PH */
  PhidalDistrPrec *Q;
#endif

#if defined(PARALLEL)
  mpi_t proc_id = DBL->proc_id;
  dim_t tli, bri;
#endif

  /** timers **/
  chrono_t t1, t2; 
  /* REAL tin1,tin2; */
  chrono_t ttotal_PrecSolv=0/* ,ttotal_SchurProd=0, ttotal_Iters=0 */;

#ifdef DEBUG_M

#if defined(PH_PH) || defined(PH_DB) || defined(D_PH_PH) || defined(D_PH_DB) /* init PH & M1 PH */ /* init DB */
  assert(A->dim1 == A->dim2);
#elif defined(PH) || defined(DB) || defined(D_PH)    /* M2 PH */ /* M2 DB */
  assert(P->schur_method == 2);
  assert(P->prevprec != NULL);
#elif defined(DB_PH) || defined(D_DB_PH) /* mix M2 */
  assert(P->schur_method == 2);
#elif defined(DB_DB) /* M1 DB */
  assert(L != NULL);
  assert(L->dim1 == L->dim2);
#endif

#endif /* DEBUG_M */

#if defined(PH_PH) || defined(PH_DB)  || defined(D_PH_PH) || defined(D_PH_DB)/* init PH & M1 PH */ /* init DB */
  n = A->dim1;
#elif defined(PH) || defined(DB)  || defined(D_PH)    /* M2 PH */ /* M2 DB */
  Q = P->prevprec;
  n = P->dim;
#elif defined(DB_PH) || defined(D_DB_PH)/* mix M2 */
  n = P->dim;
#elif defined(DB_DB) /* M1 DB */
  n = L->dim1;
#elif defined(CS_D1) || defined(CS_D2)
  n = A->n;
#endif

  /******************/
  /* Initial Guess  */
  /******************/
  r = (COEF *)malloc(sizeof(COEF)*n);
  tmp = (COEF *)malloc(sizeof(COEF)*n);

#if defined(D_PH_PH) || defined(D_PH_DB) /* parallel : init PH & M1 PH */
    tli = DA->M.tli;
    bri = DA->M.bri;
#elif defined(D_PH) /* parallel : M2 PH */
    tli = PREC_B(P)->M.tli;
    bri = PREC_B(P)->M.bri;
#elif defined(D_DB_PH) 
    tli = S(PREC_B(P))->tli; 
    bri = S(PREC_B(P))->bri;
#endif

#ifdef GUESS_SOLVE
  /** Compute r = b - A.x0 **/
#if defined(PH_PH) || defined(PH_DB) /* init PH & M1 PH */ /* init DB */
    PHIDAL_MatVec(A, BL, x, r);
#elif defined(PH)    /* M2 PH */
    PhidalPrec_SchurProd(Q, BL, x, r);
#elif defined(DB_PH) ||  defined(DB) /* mix M2 */ /* M2 DB */
    DBPrec_SchurProd(Q, BL, x, r);
#elif defined(DB_DB) /* M1 DB */
    DBMATRIX_MatVec2(L, U, BL, x, r);
#elif defined(CS_D1) || defined(CS_D2)
    matvec(A, x, r);
#elif defined(D_PH_PH) || defined(D_PH_DB) /* parallel : init PH & M1 PH */
    PhidalDistrMatrix_MatVec(0, DA, DBL, x, r);
#elif defined(D_PH) /* parallel : M2 PH */
    PhidalDistrPrec_SchurProd(Q, DBL, x, r); 
#elif defined(D_DB_PH) 
    DBDistrPrec_SchurProd(Q, DBL, x, r);
#endif

  for(i=0;i<n;i++)
    r[i] = rhs[i] - r[i];
#else /** No Guess solution **/
  memcpy(r, rhs, sizeof(COEF)*n);
  bzero(x, sizeof(COEF)*n);
  

#endif
  
  /** compute norm(b) */
#ifndef PARALLEL
  normb = BLAS_DOT(n, rhs, incx, rhs, incx);
#else
  normb = dist_ddot(proc_id, rhs, rhs, tli, bri, DBL);
#endif
  normb = sqrt(normb);

  /** compute norm(r) **/
#ifndef PARALLEL
  ro = BLAS_DOT(n, r, incx, r, incx);
#else
  ro = dist_ddot(proc_id, r, r, tli, bri, DBL);
#endif
  ro = sqrt(ro);
  
  
  p = (COEF *)malloc(sizeof(COEF)*n);
  Ap = (COEF *)malloc(sizeof(COEF)*n);
  z = (COEF *)malloc(sizeof(COEF)*n);

  /** z = M^-1.r **/
  memcpy(tmp, r, sizeof(COEF)*n);
  t1  = dwalltime();
  /*tolrat = normb/ro;*/
  tolrat = 1;
  /*fprintfv(5, stderr, "TOLRAT %g \n", tolrat);*/
#if defined(PH_PH) || defined(PH) || defined(DB_PH)
  PHIDAL_PrecSolve(tol*tolrat, P, z, tmp, BL, option, NULL, NULL);
#elif defined(PH_DB) || defined(DB) || defined(DB_DB)  
  DBMATRIX_PrecSolve(tol*tolrat, P, z, tmp, BL, option, NULL, NULL);
#elif defined(D_PH_PH) || defined(D_PH) || defined(D_DB_PH)
  PHIDAL_DistrPrecSolve(tol*tolrat, P, z, tmp, DBL, option, NULL, NULL);
  
#elif defined(D_PH_DB)
  DBDISTRMATRIX_PrecSolve(tol*tolrat, P, z, tmp, DBL, option, NULL, NULL);
#else /* defined(CS_D1) || defined(CS_D2) */
  {
    dim_t j;
    
    /** Solve L.x = b **/
    CSC_Lsol(1, L, tmp, z);
    
    /** Solve the diagonal **/
#if defined(CS_D1)
    for(j=0;j<L->n;j++)
      z[j] *= L->ma[j][0];
#else /* defined(CS_D2) */
    for(j=0;j<L->n;j++)
      z[j] *= D[j];
#endif
    /** Solve Lt.x=b **/
    CSC_Ltsol(1, L, z, z);
  }
#endif
  t2  = dwalltime(); ttotal_PrecSolv += t2-t1;

  /** p0 = z0 **/
  memcpy(p, z, sizeof(COEF)*n);
  
  /** rz = (rj, zj) **/
#ifndef PARALLEL 
  rz = BLAS_DOT(n, r, incx, z, incx);
#else
  rz = dist_ddot(proc_id, r, z, tli, bri, DBL);
#endif

  its = 0;
#if !defined(CS_D1) && !defined(CS_D2) 
#ifndef PARALLEL
  if(verbose >= 2 && fp != NULL)
#else
    if(verbose >= 2 && fp != NULL  && proc_id == 0)    
#endif
    {
      int ee;
      for(ee=0;ee < P->levelnum;ee++)
	fprintfv(5, fp, "     ");

      fprintfv(5, fp, "pcg: iter %d ro/normb %e\n", its, ro/normb);
    }
#endif  
       
  while(ro/normb > option->tol && its < itmax)
    {
      /*fprintfv(5, stdout, "its %d rz %g ro %g, b %g tol %g itmax %d \n" ,its, rz, ro, normb, option->tol, itmax);*/

      /** Ap = A.p **/
#if defined(PH_PH) || defined(PH_DB) /* init PH & M1 PH */ /* init DB */
      PHIDAL_MatVec(A, BL, p, Ap);
#elif defined(PH)    /* M2 PH */
      PhidalPrec_SchurProd(Q, BL, p, Ap);
#elif defined(DB_PH) ||  defined(DB) /* mix M2 */ /* M2 DB */
      DBPrec_SchurProd(Q, BL, p, Ap);
#elif defined(DB_DB) /* M1 DB */
      DBMATRIX_MatVec2(L, U, BL, p, Ap);
#elif defined(CS_D1) || defined(CS_D2)
      matvec(A, p, Ap);
#elif defined(D_PH_PH) || defined(D_PH_DB) /* parallel : init PH & M1 PH */
      PhidalDistrMatrix_MatVec(0, DA, DBL, p, Ap);
#elif defined(D_PH) /* parallel : M2 PH */
      PhidalDistrPrec_SchurProd(Q, DBL, p, Ap); 
#elif defined(D_DB_PH) 
      DBDistrPrec_SchurProd(Q, DBL, p, Ap);
#endif





  /*********************** DEBUG *********/
  /*********************** DEBUG *********/
  /*********************** DEBUG *********/
  /*********************** DEBUG *********/
#ifdef TOTO
{
  REAL *m = (REAL *)malloc(sizeof(REAL)*n);
  REAL *l = (REAL *)malloc(sizeof(REAL)*n);
  dim_t q;
  REAL th;

  for(q=0;q<n;q++)
    {
      m[q] = 1.0;
      l[q] = 0.0;
    }
#ifndef PARALLEL
  th = BLAS_DOT(n, m, incx, m, incx);
#else
  th = dist_ddot(proc_id, m, m, tli, bri, DBL);
#endif
  th =sqrt(th);
  fprintfv(5, stderr, "\n M = %g \n ", th);

#if defined(D_PH_PH) || defined(D_PH_DB) /* parallel : init PH & M1 PH */
  PhidalDistrMatrix_MatVec(0, DA, DBL, m, l);
#elif defined(D_PH) /* parallel : M2 PH */
  /*PhidalDistrPrec_SchurProd(Q, DBL, m, l); */
  assert(0);
#endif
  
#ifndef PARALLEL
  th = BLAS_DOT(n, l, incx, l, incx);
#else
  th = dist_ddot(proc_id, l, l, tli, bri, DBL);
#endif
  th =sqrt(th);
  fprintfv(5, stderr, "\n L = %g \n ", th);
  exit(0);

}

#endif





      /** alpha = (r,z)/(A.p, p) **/
#ifndef PARALLEL   
      alpha = BLAS_DOT(n, Ap, incx, p, incx);
#else
      alpha = dist_ddot(proc_id, Ap, p, tli, bri, DBL);
#endif
      alpha = rz/alpha;
      
      /*fprintfv(5, stdout, "its %d alpha %g \n" ,its, alpha);*/

      /** x = x + alpha.p **/
      for(i=0;i<n;i++)
	x[i] = x[i] + alpha*p[i];  /* OIMBE BLAS AXPY */
      
      /** r = r - alpha.Ap **/
      for(i=0;i<n;i++)
	r[i] = r[i] - alpha*Ap[i];  /* OIMBE BLAS AXPY */

      /** z = M^-1.r **/
      memcpy(tmp, r, sizeof(REAL)*n);


#if defined(PH_PH) || defined(PH) || defined(DB_PH)
      if(itertab != NULL && resnormtab != NULL)
	PHIDAL_PrecSolve(tol*tolrat, P, z, tmp, BL, option, itertab+1, resnormtab+1);
      else
	PHIDAL_PrecSolve(tol*tolrat, P, z, tmp, BL, option, NULL, NULL);
#elif defined(PH_DB) || defined(DB) || defined(DB_DB)  
      if(itertab != NULL && resnormtab != NULL)
	DBMATRIX_PrecSolve(tol*tolrat, P, z, tmp, BL, option, itertab+1, resnormtab+1);
      else
	DBMATRIX_PrecSolve(tol*tolrat, P, z, tmp, BL, option, NULL, NULL);
#elif defined(D_PH_PH) || defined(D_PH) || defined(D_DB_PH)
      if(itertab != NULL && resnormtab != NULL)
	PHIDAL_DistrPrecSolve(tol*tolrat, P, z, tmp, DBL, option, itertab+1, resnormtab+1);
      else
	PHIDAL_DistrPrecSolve(tol*tolrat, P, z, tmp, DBL, option, NULL, NULL);
#elif defined(D_PH_DB)
      if(itertab != NULL && resnormtab != NULL)
	DBDISTRMATRIX_PrecSolve(tol*tolrat, P, z, tmp, DBL, option, itertab+1, resnormtab+1);
      else
	DBDISTRMATRIX_PrecSolve(tol*tolrat, P, z, tmp, DBL, option, NULL, NULL);
      
#else /* defined(CS_D1) || defined(CS_D2) */
      {
	dim_t j;

	/** Solve L.x = b **/
	CSC_Lsol(1, L, tmp, z);

	/** Solve the diagonal **/
#if defined(CS_D1)
	for(j=0;j<L->n;j++)
	  z[j] *= L->ma[j][0];
#else /* defined(CS_D2) */
	for(j=0;j<L->n;j++)
	  z[j] *= D[j];
#endif
	/** Solve Lt.x=b **/
	CSC_Ltsol(1, L, z, z);
      }
#endif
      t2 = dwalltime();
#ifndef PARALLEL   
      if(verbose >= 4 && its == 1 && fp != NULL )
#else
      if(verbose >= 4 && its == 1 && fp != NULL && proc_id == 0)
#endif	
	fprintfv(5, stdout, " DR  in %g seconds \n", t2-t1);

      
      /** beta = (rj+1, zj+1)/(rj, zj) **/
      beta = rz;
      /** rz = (rj, zj) **/
#ifndef PARALLEL   
      rz =  BLAS_DOT(n, r, incx, z, incx);
#else
      rz = dist_ddot(proc_id, r, z, tli, bri, DBL);
#endif
      beta = rz/beta;

      /** p = z + beta*p **/
      for(i=0;i<n;i++)
	p[i] = z[i] + beta*p[i]; /* OIMBE BLAS AXPY */
      
      /** Compute ro **/
#ifndef PARALLEL   
      ro = BLAS_DOT(n, r, incx, r, incx);
#else
      ro = dist_ddot(proc_id, r, r, tli, bri, DBL);
#endif
      ro = sqrt(ro);

      its++;
#if !defined(CS_D1) && !defined(CS_D2) 
#ifndef PARALLEL
      if(verbose >= 2 && fp != NULL)
#else
	if(verbose >= 2 && fp != NULL  && proc_id == 0)    
#endif
	{
	  int ee;
	  for(ee=0;ee < P->levelnum;ee++)
	    fprintfv(5, fp, "     ");

	  fprintfv(5, fp, "pcg: iter %d ro/normb %e\n", its, ro/normb);
	}
#endif
    }


  free(Ap);
  free(p);
  free(z);
  free(r);
  free(tmp);
  
  if(itertab != NULL)
    itertab[0] = its;
  if(resnormtab != NULL)
    resnormtab[0] = ro;

  return HIPS_SUCCESS;
#else
  fprintfv(5, stderr, "PCG pas bon en complex \n");
  exit(-1);
#endif
}


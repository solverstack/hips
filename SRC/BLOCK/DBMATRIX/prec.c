#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>

#include "phidal_sequential.h"
#include "block.h"

#include "prec.h"


void Prec2PhidalPrec(Prec* p1, PhidalPrec* p2) {

  /* unused flag_t type; */

  p2->symmetric  = p1->symmetric;
  p2->dim        = p1->dim;
  p2->levelnum   = p1->levelnum;
  p2->forwardlev = p1->forwardlev;


/*   LU; */
/*   PhidalMatrix *L; */
/*   PhidalMatrix *U; */
/*   COEF *D; */

/*  EF; */
/*   PhidalMatrix *E; */
/*   PhidalMatrix *F; */

/*  EF_DB; /\* tmp, a suppr *\/ */

/*  p2->B = p1->B; */
/*   PhidalMatrix *B; */

/*  p2->S; */
/*   PhidalMatrix *S; */

 assert(p1->precprec == NULL);
 assert(p1->nextprec == NULL);
 p2->prevprec = NULL;
 p2->nextprec = NULL;

 p2->schur_method = p1->schur_method;
 p2->info         = p1->info;

 p2->pivoting = p1->pivoting; 
 p2->permtab  = p1-> permtab;

}


void PhidalPrec2Prec(PhidalPrec* p1, Prec* p2) {

  /* unused flag_t type; */

  p2->symmetric  = p1->symmetric;
  p2->dim        = p1->dim;
  p2->levelnum   = p1->levelnum;
  p2->forwardlev = p1->forwardlev;


/*   LU; */
/*   PhidalMatrix *L; */
/*   PhidalMatrix *U; */
/*   COEF *D; */

/*  EF; */
/*   PhidalMatrix *E; */
/*   PhidalMatrix *F; */

/*  EF_DB; /\* tmp, a suppr *\/ */

/*  p2->B = p1->B; */
/*   PhidalMatrix *B; */

/*  p2->S; */
/*   PhidalMatrix *S; */

 assert(p1->precprec == NULL);
 assert(p1->nextprec == NULL);
 p2->prevprec = NULL;
 p2->nextprec = NULL;

 p2->schur_method = p1->schur_method;
 p2->info         = p1->info;

 p2->pivoting = p1->pivoting; 
 p2->permtab  = p1-> permtab;

}

/* @release_exclude */
/* @authors J. GAIDAMOUR, P. HENON */

#include <stdio.h>
#include <stdlib.h>

#include <string.h> /*memset*/

#include <assert.h>
#include "block.h"

void dumpCS2(dim_t num, FILE *fp, csptr A);

#ifdef TYPE_REAL 

#define DBM
#ifdef DBM
#define HTML

static int c = 0;

void print_vec(char* name, COEF* vec, int n) {
 
  printf("%d %s ", c, name); c++;
  int h; for(h=0; h<n; h++) {
    if (vec[h] == 0)
      printf("0 ", vec[h]);
    else
      printf("%e ", vec[h]);
  }
  printf("\n");
}


static int c2 = 0;
static COEF* t_vec = NULL;

void print_vec_diff(char* name, COEF* vec, int n) {

  if (t_vec == NULL) { 
    t_vec = malloc(sizeof(COEF)*n);
    memcpy(t_vec, vec, sizeof(COEF)*n);
    print_vec(name, vec, n); return; 
  }
  
  printf("<diff> %d %s ", c2, name); c2++;
  int h; for(h=0; h<n; h++) {
    //  if (vec[h] != t_vec[h])
      if (vec[h] == 0)
	printf("0 ", h, vec[h]-t_vec[h]);
      else
	printf("%e ", h, vec[h]-t_vec[h]);
  }
  printf("\n");

  memcpy(t_vec, vec, sizeof(COEF)*n);
}

void printLD2(SolverMatrix* AA, char* filename, char* mtxname, int opt);

void dumpSolverMatrix(SolverMatrix* solvmtx, SymbolMatrix* symbmtx, REAL** mat);
void MATLAB_dumpSolverMatrix(SolverMatrix* solvmtx, SymbolMatrix* symbmtx, FILE* fd);

REAL** alloc_a(int n) {
  int maxrow = n;
  dim_t i;
  REAL** mat = (REAL**)malloc(sizeof(REAL*)*n);

  for(i=0; i< n; i++) {
    mat[i] = (REAL*)malloc(sizeof(REAL)*maxrow);
    bzero(mat[i], sizeof(REAL)*maxrow);
  }

  return mat;
}


void print_a(REAL** mat, int n, int m, char* filename, char* mtxname, int opt) {
  dim_t i,j;
  FILE * stream;
  stream = fopen(filename, "w");
  
#ifndef HTML
  fprintf( stream,"%s:= matrix([\n", mtxname);
#else
  /*  fprintf( stream,"<table>\n"); */
#endif

  for(j=0; j<m; j++) {    

#ifndef HTML
    fprintf( stream,"[");
#else
    fprintf( stream,"<tr>\n");
#endif

    for(i=0; i<n; i++) {

      if (opt == 1) /* DIAG UNIT */
	if (i>=j) {
	  if (i>j)
#ifndef HTML
	    fprintf( stream,"%f", (REAL)0);
#else
	  fprintf( stream,"<td>%f</td>", (REAL)0);
#endif
	  else 
#ifndef HTML
	    fprintf( stream,"%f", (REAL)1);
#else
	  fprintf( stream,"<td>%f</td>", (REAL)1);
#endif
#ifndef HTML		    
	  if (i!=n-1) fprintf( stream,",");
#endif
	  continue;
	}
      if (opt == 2) /* AVEC DIAG */
	if (i>j) {
#ifndef HTML
	  fprintf( stream,"%f", (REAL)0);
#else
	  fprintf( stream,"<td>%f</td>", (REAL)0);
#endif
#ifndef HTML	
	  if (i!=n-1) fprintf( stream,",");
#endif
	  continue;
	}

      if (opt == 3) /* DIAG A 0 */
	if (i>=j) {
#ifndef HTML
	  fprintf( stream,"%f", (REAL)0);
#else
	  fprintf( stream,"<td>%f</td>", (REAL)0);
#endif
#ifndef HTML	
	  if (i!=n-1) fprintf( stream,",");
#endif
	  continue;
	}

      /*     if (mat[i][j] != 0)*/
#ifndef HTML
      fprintf( stream,"%f", mat[i][j]);
#else
      fprintf( stream,"<td>%f</td>", mat[i][j]);
#endif
      /*else*/
#ifndef HTML	
      /*fprintf( stream,"0");*/
      if (i!=n-1) fprintf( stream,",");
#else
      /*    fprintf( stream,"<td>0</td>");*/
#endif

    }
#ifndef HTML
    fprintf( stream,"]");
    if (j!=m-1) fprintf( stream,",\n");
#else
    fprintf( stream,"</tr>\n");
#endif
  }
#ifndef HTML
  fprintf( stream,"]);\n");
#else
  /*fprintf( stream,"</table>\n");*/
#endif
  fclose(stream);
  

}

void free_a(REAL** mat, int n) {
  dim_t i;
  for(i=0; i<n; i++) 
    free(mat[i]);
  free(mat);
}


void dumpDBMatrix(DBMatrix* L, char* filename) {
  int n = 48;
  int i, i0,j;

  SolverMatrix* solvmtx=NULL;
  SymbolMatrix* symbmtx;

  REAL** mat;

  printf( "*** debug : dump DBMatrix %p -> %s\n", L, filename);

  if (L->alloc == ONE) {
    solvmtx = &L->a[0];
    symbmtx = &solvmtx->symbmtx;
  }

  mat = alloc_a(n);
  
  for(i0=0,i=L->tlj; i<=L->brj; i0++, i++) {
    if (L->alloc == CBLK) {
      solvmtx = &L->a[i0];
    }

    for(j=L->cia[i];j<L->cia[i+1];j++){
      symbmtx = &L->ca[j]->symbmtx;
	
      if (L->alloc == RBLK) {
	solvmtx = &L->a[L->cja[j]-L->tli];
      }

      if (L->alloc == BLK) {
	solvmtx = L->ca[j]->solvmtx;
      }

/*       check(L, solvmtx, symbmtx); */
      dumpSolverMatrix(solvmtx, symbmtx, mat);

    }
  }

  print_a(mat, n, n, filename, "L", 2);
  free_a(mat, n);

  if (L->alloc == ONE)
    printLD2(L->a, "ref.txt", "L", 2);
}


void dumpSolverMatrix(SolverMatrix* solvmtx, SymbolMatrix* symbmtx, REAL** mat) {
  int k, p, i,j, i0;
  REAL* ind;
  
  blas_t stride;
  int trp;
  
  int decal;

  decal = 0; /*symbmtx->ccblktab[0].fcolnum;*/
  
  for(k=0;k<symbmtx->cblknbr;k++) {
    trp = symbmtx->bcblktab[k].fbloknum;
    stride = symbmtx->stride[k];
    if (stride == 0) continue;

    for(i = symbmtx->ccblktab[k].fcolnum, i0=0; i<=symbmtx->ccblktab[k].lcolnum; i++, i0++) {
      ind = solvmtx->coeftab + solvmtx->bloktab[trp].coefind + i0*stride;
      assert(ind != NULL);

      for(p=trp;p<=symbmtx->bcblktab[k].lbloknum;p++) {
	for(j=symbmtx->bloktab[p].frownum; j<=symbmtx->bloktab[p].lrownum; j++) {
	  mat[i-decal][j] = *ind;
	  ind++;
	}
      }
      
    }
  }

}


































REAL** profilLD2(SolverMatrix* solvmtx, dim_t *m) {
  dim_t n;
  int k, p, i,j, i0;
  REAL** mat;
  REAL* ind;

  blas_t stride;
  int trp;
  int maxrow;

  int decal;

  SymbolMatrix* symbmtx = &(solvmtx->symbmtx);
  n = symbmtx->nodenbr;

  mat = (REAL**)malloc(sizeof(REAL*)*n);

  maxrow=0;
  for(k=0;k<symbmtx->cblknbr;k++) {
    trp = symbmtx->bcblktab[k].fbloknum;
    for(p=trp;p<=symbmtx->bcblktab[k].lbloknum;p++) {
      if (symbmtx->bloktab[p].lrownum > maxrow) maxrow = symbmtx->bloktab[p].lrownum;
    }
  }
  maxrow++;
  *m=maxrow;

  for(i=0; i< n; i++) {
    mat[i] = (REAL*)malloc(sizeof(REAL)*maxrow);
    bzero(mat[i], sizeof(REAL)*maxrow);
  }

  decal = symbmtx->ccblktab[0].fcolnum;

  for(k=0;k<symbmtx->cblknbr;k++) {
    trp = symbmtx->bcblktab[k].fbloknum;
    stride = symbmtx->stride[k];
    if (stride == 0) continue;

    for(i = symbmtx->ccblktab[k].fcolnum, i0=0; i<=symbmtx->ccblktab[k].lcolnum; i++, i0++) {
      ind = solvmtx->coeftab + solvmtx->bloktab[trp].coefind + i0*stride;
      assert(ind != NULL);

      for(p=trp;p<=symbmtx->bcblktab[k].lbloknum;p++) {
	for(j=symbmtx->bloktab[p].frownum; j<=symbmtx->bloktab[p].lrownum; j++) {
	  mat[i-decal][j] = *ind;
	  ind++;
	}
      }
      
    }
  }

  return mat;

}

void printLD2(SolverMatrix* AA, char* filename, char* mtxname, int opt) {
  dim_t i,j;  
  dim_t m;

  int  n = AA->symbmtx.nodenbr; 
  REAL** mat = profilLD2(AA, &m);

  FILE * stream;
  stream = fopen(filename, "w");

#ifndef HTML
  fprintf( stream,"%s:= matrix([\n", mtxname);
#else
  /*  fprintf( stream,"<table>\n");*/
#endif

  for(j=0; j<m; j++) {    


#ifndef HTML
    fprintf( stream,"[");
#else
    fprintf( stream,"<tr>\n");
#endif

    for(i=0; i<n; i++) {

      if (opt == 1) /* DIAG UNIT */
	if (i>=j) {
	  if (i>j)
#ifndef HTML
	    fprintf( stream,"%f", (REAL)0);
#else
	  fprintf( stream,"<td>%f</td>", (REAL)0);
#endif
	  else 
#ifndef HTML
	    fprintf( stream,"%f", (REAL)1);
#else
	  fprintf( stream,"<td>%f</td>", (REAL)1);
#endif
#ifndef HTML		    
	  if (i!=n-1) fprintf( stream,",");
#endif
	  continue;
	}
      if (opt == 2) /* AVEC DIAG */
	if (i>j) {
#ifndef HTML
	  fprintf( stream,"%f", (REAL)0);
#else
	  fprintf( stream,"<td>%f</td>", (REAL)0);
#endif
#ifndef HTML	
	  if (i!=n-1) fprintf( stream,",");
#endif
	  continue;
	}

      if (opt == 3) /* DIAG A 0 */
	if (i>=j) {
#ifndef HTML
	  fprintf( stream,"%f", (REAL)0);
#else
	  fprintf( stream,"<td>%f</td>", (REAL)0);
#endif
#ifndef HTML	
	  if (i!=n-1) fprintf( stream,",");
#endif
	  continue;
	}

      /*     if (mat[i][j] != 0)*/
#ifndef HTML
      fprintf( stream,"%f", mat[i][j]);
#else
      fprintf( stream,"<td>%f</td>", mat[i][j]);
#endif
      /*else*/
#ifndef HTML	
      /*fprintf( stream,"0");*/
      if (i!=n-1) fprintf( stream,",");
#else
      /*    fprintf( stream,"<td>0</td>");*/
#endif

    }
#ifndef HTML
    fprintf( stream,"]");
    if (j!=m-1) fprintf( stream,",\n");
#else
    fprintf( stream,"</tr>\n");
#endif
  }
#ifndef HTML
  fprintf( stream,"]);\n");
#else
  /*fprintf( stream,"</table>\n");*/
#endif
  fclose(stream);
  
  for(i=0; i< n; i++) 
    free(mat[i]);
  free(mat);

}



#endif

#endif /* #ifdef TYPE_REAL */

void dumpPhidalMatrix(FILE* stream, PhidalMatrix* M) {
  dim_t i,j;

  if(M==NULL) {
    printf( "dumpPhidalMatrix : NULL matrix! \n");
    return;
  }

  /*   dumpcsr(stream, NULL, M->rja, M->ria, M->n); */

  for(i=M->tlj; i<=M->brj; i++) {
    fprintf( stream,"Colonne k = %d/%d\n", i, M->brj);
    for(j=M->cia[i];j<M->cia[i+1];j++){ 
      fprintf(stream, "bloc n°%d, ligne cja %d (id : %d)\n", j, M->cja[j], M->ca[j] - M->ca[0]);

/*       fprintf( stream,"j = %d/%d ===> (%d,%d)\n", j-M->cia[i], M->cia[i+1]-M->cia[i]-1, i, M->cja[j]); */
      ascend_column_reorder(M->ca[j]);
/*       printf("j=%d\n", j); */
      dumpCS2(1, stream, M->ca[j]); /* ATTENTION NUMEROTATION FORTRAN */ 
    }
  }
  
}

void fdumpPhidalMatrix(char* filename, PhidalMatrix* M) {
  FILE * fd = fopen(filename, "w");
  dumpPhidalMatrix(fd, M);
  fclose(fd);
}

#ifdef TYPE_REAL
	/*     printf( "tol = %e, itmax = %d\n", tol*tolrat, itmax); */
	    
/* 	    dumpPhidalMatrix(P->phidalS); */

/* 	    printf( "phidalPrec : symmetric %d, dim = %d, levelnum = %d, forwardlev = %d, L %p, U %p, D %p, E %p, F %p, S %p, B %p, prevprec %p, nextprec %p, schur_method %d, pivoting %d, permtab %p, tol_schur %lf\n",  */
/* 		   P->phidalPrec->symmetric, P->phidalPrec->dim, P->phidalPrec->levelnum,  */
/* 		   P->phidalPrec->forwardlev, P->phidalPrec->L, P->phidalPrec->U,  */
/* 		   P->phidalPrec->D, P->phidalPrec->E, P->phidalPrec->F,  */
/* 		   P->phidalPrec->S, P->phidalPrec->B, P->phidalPrec->prevprec,  */
/* 		   P->phidalPrec->nextprec, P->phidalPrec->schur_method, P->phidalPrec->pivoting,  */
/* 		   P->phidalPrec->permtab, P->phidalPrec->tol_schur); */
	    
/* 	    dumpPhidalMatrix(P->phidalPrec->L); */
/* 	    dumpPhidalMatrix(P->phidalPrec->U); */
/* 	    /\* D *\/ */
/* 	    {  */
/* 	      dim_t i; */
	      
/* 	      printf( "D : \n"); */
/* 	      for (i=0; i<P->phidalPrec->dim; i++) */
/* 		printf( "%lf ", P->phidalPrec->D[i]); */
/* 	      printf( "\n"); */
/* 	    } */
void VSolverMatrix_print(FILE* stream, VSolverMatrix* a, flag_t diag);

void CS_print2(mpi_t proc_id, char* file, int i, csptr M) {
  char filename[100];
  sprintf( filename, "cs/%d-%s-%d.txt", proc_id, file, i);
  
  FILE * fd = fopen(filename, "w");
  ascend_column_reorder(M);
  dumpCS(0, fd, M);
  fclose(fd);
}

void VSolverMatrix_print2(mpi_t proc_id, char* file, int i, VSolverMatrix* M, flag_t diag) {
  char filename[100];
  sprintf( filename, "vs/%d-%s-%d.txt", proc_id, file, i);
  
  FILE * fd = fopen(filename, "w");
  VSolverMatrix_print(fd, M, diag);
  fclose(fd);
}



#define DIAG 1
#define NORMAL 2


void VSolverMatrix_print(FILE* stream, VSolverMatrix* a, flag_t diag) {
/*   csptr cs = (csptr)malloc(sizeof(struct SparRow)); */

/*   initCS(cs, a->symbmtx.nodenbr); */
/*   VSolverMatrix2SparRow(SolverMatrix* solvmtx, SymbolMatrix* symbmtx, csptr cs, flag_t inarow, flag_t diag, flag_t unitdiag); */
  
/*   VSolverMatrix2SparRow(a->solvmtx, &a->symbmtx, cs, 0, i == Lja[j], 0); */

/*   dumpCS(0, stream, cs); */

  printf( "  a->cblknkb = %d\n", a->symbmtx.cblknbr);

  int k,p,j, i, i0;
  int trp; 
  COEF* ind;
  SymbolMatrix* symbmtx = &a->symbmtx;
  SolverMatrix* solvmtx = a->solvmtx;
  blas_t stride;

  
  for(k=0;k<symbmtx->cblknbr;k++) {
    trp = symbmtx->bcblktab[k].fbloknum;
    stride = symbmtx->stride[k];
    if (stride == 0) continue; /* plutot hdim */
    
    for(i = symbmtx->ccblktab[k].fcolnum, i0=0; i<=symbmtx->ccblktab[k].lcolnum; i++, i0++) {
      ind = solvmtx->coeftab + solvmtx->bloktab[trp].coefind + i0*stride;
      assert(ind != NULL);
      
      for(p=trp;p<=symbmtx->bcblktab[k].lbloknum;p++) {
	for(j=symbmtx->bloktab[p].frownum; j<=symbmtx->bloktab[p].lrownum; j++) {
	  if ((*ind != 0) && (diag == NORMAL)) 
	    fprintf( stream, "%d %d %e \n", i0, j - symbmtx->tli,  *ind);
	  else if ((*ind != 0) && (j - symbmtx->tli >= i0) && (diag == DIAG)) 
	    fprintf( stream, "%d %d %e \n", i0, j - symbmtx->tli,  *ind);
	  ind++;
	}
      }
    }
  }
}

#ifdef ACTIVE
void dumpSolverMatrix(SolverMatrix* solvmtx, SymbolMatrix* symbmtx, FILE* fd) {
  int k, p, i,j, i0;
  REAL* ind;
  
  blas_t stride;
  int trp;
  
  int decal;

  decal = 0; /*symbmtx->ccblktab[0].fcolnum;*/
  
  for(k=0;k<symbmtx->cblknbr;k++) {
    trp = symbmtx->bcblktab[k].fbloknum;
    stride = symbmtx->stride[k];
    if (stride == 0) continue;

    for(i = symbmtx->ccblktab[k].fcolnum, i0=0; i<=symbmtx->ccblktab[k].lcolnum; i++, i0++) {
      ind = solvmtx->coeftab + solvmtx->bloktab[trp].coefind + i0*stride;
      assert(ind != NULL);

      for(p=trp;p<=symbmtx->bcblktab[k].lbloknum;p++) {
	for(j=symbmtx->bloktab[p].frownum; j<=symbmtx->bloktab[p].lrownum; j++) {
	  fprintf( fd, "%d %d %lf\n", i-decal, j, *ind);
	  ind++;
	}
      }
      
    }
  }

}


void MATLAB_dumpDBMatrix(DBMatrix* L, char* filename) {
  int n = 48;
  int i, i0,j;

  SolverMatrix* solvmtx;
  SymbolMatrix* symbmtx;

  FILE* fd = fopen(filename, "w");

  printf( "*** debug : dump DBMatrix %p -> %s\n", L, filename);

  if (L->alloc == ONE) {
    solvmtx = &L->a[0];
    symbmtx = &solvmtx->symbmtx;
  }
  
  for(i0=0,i=L->tlj; i<=L->brj; i0++, i++) {
    if (L->alloc == CBLK) {
      solvmtx = &L->a[i0];
    }

    for(j=L->cia[i];j<L->cia[i+1];j++){
      symbmtx = &L->ca[j]->symbmtx;
	
      if (L->alloc == RBLK) {
	solvmtx = &L->a[L->cja[j]-L->tli];
      }

      if (L->alloc == BLK) {
	solvmtx = L->ca[j]->solvmtx;
      }

      MATLAB_dumpSolverMatrix(solvmtx, symbmtx, fd);

    }
  }

  fclose(fd);

}


#endif




void MATLAB_dumpSolverMatrix(SolverMatrix* solvmtx, SymbolMatrix* symbmtx, FILE* fd) {
  int k, p, i,j, i0;
  REAL* ind;
  
  blas_t stride;
  int trp;
  
  int decal;

  decal = symbmtx->ccblktab[0].fcolnum; /*todo : ONE only*/
  
  for(k=0;k<symbmtx->cblknbr;k++) {
    trp = symbmtx->bcblktab[k].fbloknum;
    stride = symbmtx->stride[k];
    if (stride == 0) continue;

    for(i = symbmtx->ccblktab[k].fcolnum, i0=0; i<=symbmtx->ccblktab[k].lcolnum; i++, i0++) {
      ind = solvmtx->coeftab + solvmtx->bloktab[trp].coefind + i0*stride;
      assert(ind != NULL);

      for(p=trp;p<=symbmtx->bcblktab[k].lbloknum;p++) {
	for(j=symbmtx->bloktab[p].frownum; j<=symbmtx->bloktab[p].lrownum; j++) {
	  if (j>=i-decal)
	    /* numerotation fortran => +1 */
	    fprintf( fd, "%d %d %lf\n", i-decal+1, j-decal+1, *ind);
	  ind++;
	}
      }
      
    }
  }

}

void MATLAB_dumpDBMatrix(DBMatrix* L, char* filename) {
/*   int i, i0,j; */

/*   SolverMatrix* solvmtx; */
/*   SymbolMatrix* symbmtx; */

  FILE* fd = fopen(filename, "w");

  assert(L->alloc == ONE);

  printf( "*** debug : dump DBMatrix %p -> %s\n", L, filename);

  MATLAB_dumpSolverMatrix(&L->a[0], &L->a[0].symbmtx, fd);

/*   if (L->alloc == ONE) { */
/*     solvmtx = &L->a[0]; */
/*     symbmtx = &solvmtx->symbmtx; */
/*   } */
  
/*   for(i0=0,i=L->tlj; i<=L->brj; i0++, i++) { */
/*     if (L->alloc == CBLK) { */
/*       solvmtx = &L->a[i0]; */
/*     } */

/*     for(j=L->cia[i];j<L->cia[i+1];j++){ */
/*       symbmtx = &L->ca[j]->symbmtx; */
	
/*       if (L->alloc == RBLK) { */
/* 	solvmtx = &L->a[L->cja[j]-L->tli]; */
/*       } */

/*       if (L->alloc == BLK) { */
/* 	solvmtx = L->ca[j]->solvmtx; */
/*       } */

/*       MATLAB_dumpSolverMatrix(solvmtx, symbmtx, fd); */

/*     } */
/*   } */

  fclose(fd);

}



void MATLAB_dumpSolverMatrix2(SolverMatrix* solvmtx, SymbolMatrix* symbmtx, FILE* fd_i, FILE* fd_j, FILE* fd_v) {
  int k, p, i,j, i0;
  REAL* ind;
  
  blas_t stride;
  int trp;
  
  int decal;

  decal = symbmtx->ccblktab[0].fcolnum; /*todo : ONE only*/
  
  for(k=0;k<symbmtx->cblknbr;k++) {
    trp = symbmtx->bcblktab[k].fbloknum;
    stride = symbmtx->stride[k];
    if (stride == 0) continue;

    for(i = symbmtx->ccblktab[k].fcolnum, i0=0; i<=symbmtx->ccblktab[k].lcolnum; i++, i0++) {
      ind = solvmtx->coeftab + solvmtx->bloktab[trp].coefind + i0*stride;
      assert(ind != NULL);

      for(p=trp;p<=symbmtx->bcblktab[k].lbloknum;p++) {
	for(j=symbmtx->bloktab[p].frownum; j<=symbmtx->bloktab[p].lrownum; j++) {
	  if (j>=i) {
	    /* numerotation fortran => +1 */
	    fprintf( fd_v, "%d %d %lf\n", i-decal+1, j-decal+1, *ind);
	    
		    /* fprintf( fd_i, "%d\n", i-decal+1); */
		    /* 	    fprintf( fd_j, "%d\n", j-decal+1); */
		    /* 	    fprintf( fd_v, "%lf\n", *ind); */
	  }
	  ind++;
	}
      }
      
    }
  }
}


void MATLAB_dumpDBMatrix2(DBMatrix* L, char* filename) {
/*   int i, i0,j; */

/*   SolverMatrix* solvmtx; */
/*   SymbolMatrix* symbmtx; */

  char filename_i[100];
  char filename_j[100];  
  char filename_v[100];

  sprintf( filename_i, "%s%s", filename, "i");
  sprintf( filename_j, "%s%s", filename, "j");
  sprintf( filename_v, "%s%s", filename, "v");

  FILE* fd_i = fopen(filename_i, "w");
  FILE* fd_j = fopen(filename_j, "w");
  FILE* fd_v = fopen(filename_v, "w");

  assert(L->alloc == ONE);

  printf( "*** debug : dump DBMatrix %p -> %s\n", L, filename);

  MATLAB_dumpSolverMatrix2(&L->a[0], &L->a[0].symbmtx, fd_i, fd_j, fd_v);

/*   if (L->alloc == ONE) { */
/*     solvmtx = &L->a[0]; */
/*     symbmtx = &solvmtx->symbmtx; */
/*   } */
  
/*   for(i0=0,i=L->tlj; i<=L->brj; i0++, i++) { */
/*     if (L->alloc == CBLK) { */
/*       solvmtx = &L->a[i0]; */
/*     } */

/*     for(j=L->cia[i];j<L->cia[i+1];j++){ */
/*       symbmtx = &L->ca[j]->symbmtx; */
	
/*       if (L->alloc == RBLK) { */
/* 	solvmtx = &L->a[L->cja[j]-L->tli]; */
/*       } */

/*       if (L->alloc == BLK) { */
/* 	solvmtx = L->ca[j]->solvmtx; */
/*       } */

/*       MATLAB_dumpSolverMatrix(solvmtx, symbmtx, fd); */

/*     } */
/*   } */

  fclose(fd_i);
  fclose(fd_j);
  fclose(fd_v);

}
#endif

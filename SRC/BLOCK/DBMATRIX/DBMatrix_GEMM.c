/* @authors J. GAIDAMOUR */

#include <stdio.h>
#include <stdlib.h>
#include <string.h> /* memset */
#include <assert.h>

#include "block.h"

#include "base.h"

#define DEBUG Dprintf
#define check(a,b,c)

#define MAX(x,y) (((x)<(y))?(y):(x))
#define MIN(x,y) (((x)>(y))?(y):(x))

/* TODO : stride */

/* TODO : suppr l'argument BL */

struct DBMatrixPart_ {
  INTL *ia;
  dim_t *ja;
  VSolverMatrix **a;
};
typedef struct DBMatrixPart_  DBMatrixPart;

void DBMatrix_sGEMM2_sym(int iB, COEF alpha,
			 DBMatrix* A, DBMatrixPart* Ap,
/* 			 DBMatrix* B, DBMatrixPart* Bp, */
			 int nk, dim_t *jak, VSolverMatrix** rak,
			 DBMatrix* C, DBMatrixPart* Cp,
			 DBMatrix* D,
			 int *tabA, int *tabC,
			 COEF* E, COEF* F, COEF* W, dim_t tli,
			 int Aalloc);

void DBMatrix_sGEMM_sym(int i, COEF alpha, 
			DBMatrix* A, INTL **Ap, /* TODO : faire un transpose Ã  la place */
/* 			DBMatrix* B, INTL **Bp, */
			int nk, dim_t *jak, VSolverMatrix** rak,
			DBMatrix* C, INTL **Cp,
			DBMatrix* D,	 
			int *tabA, int *tabC, 
			COEF* E, COEF* F, COEF* W, dim_t tli, int Aalloc) {
  
  DBMatrix_sGEMM2_sym(i, alpha,
		      A, (DBMatrixPart*)Ap,
/* 		      B, (DBMatrixPart*)Bp, */
		      nk, jak, rak,
		      C, (DBMatrixPart*)Cp,
		      D,
		      tabA, tabC, E, F, W, tli, Aalloc);
}





/* TODO : calcul sur la diag */
/* TODO : uniformiser les opÃ©ration TRSM GEMM Facto avec double one et double zero */

void DBMatrix_GEMM(UDBMatrix* CLU, COEF alpha, DBMatrix* A, DBMatrix* B_OLD, DBMatrix* D, 
                   flag_t fill, UPhidalMatrix* PhidalLU, PhidalHID *BL) {
  /************************************************************************************/
  /* This function performs C = C + alpha.A.D^-1.B                                    */
  /************************************************************************************/
  DBMatrix* C = CLU->L;

  int i0, iB;
 
  COEF *E, *F, *W;
    
  int *tabA, *tabC;
  
  PhidalMatrix* Phidal = PhidalLU->L;

  /* Allocation des vecteurs temporaires */
  E = (COEF *)malloc(sizeof(COEF)*A->coefmax);
  assert(E != NULL);

  F = (COEF *)malloc(sizeof(COEF)*A->coefmax);
  assert(F != NULL);

  W = (COEF *)malloc(sizeof(COEF)*A->coefmax);
  assert(W != NULL);

  tabA = (int*)malloc(sizeof(int)*A->dim1);
  tabC = (int*)malloc(sizeof(int)*C->dim1);
  assert(tabA != NULL);
  assert(tabC != NULL);
  assert(A->dim1 = C->dim1);

#ifdef DEBUG_M
  if (fill == INGEMM) {
    assert((C->alloc == CBLK) || (C->alloc == BLK));
  }
#endif

  assert(((A->alloc == ONE )/*  && (B->alloc == ONE ) */) ||
	 ((A->alloc == RBLK)/*  && (B->alloc == CBLK) */) ||
	 ((A->alloc == CBLK)/*  && (B->alloc == RBLK) */) );
  assert((D->alloc == ONE) || (D->alloc == CBLK));
  assert((C->alloc == ONE) || (C->alloc == CBLK) || (C->alloc == RBLK) || (C->alloc == BLK));

  assert(0); /* dans _sym, la boucle est pas bonne dans la version jak/rak */

  int nk;
  dim_t *jak;
  VSolverMatrix** rak;
  {
    int ii;
    dim_t k;
    DBMatrix* L = A;

    ii = 0;
    for(k=L->tli;k<=L->bri;k++)
      if(L->ria[k+1] - L->ria[k]/* -1 */ > ii)
	ii = L->ria[k+1] - L->ria[k]/* -1 */;
    if(ii>0)
      {
/* 	struct DBMatrixPart_ { */
/* 	  INTL *ia; */
/* 	  dim_t *ja; */
/* 	  VSolverMatrix **a; */
/* 	}; */

/* 	printfv(5, "ii=%d\n", ii); */
	jak = (dim_t *)malloc(sizeof(dim_t)*ii);
	rak = (VSolverMatrix **)malloc(sizeof(VSolverMatrix*)*ii);
      }
    else
      {
	jak = NULL;
	rak = NULL;
      }
  }

  /* parcours des cblk phidal (de B et C) */
  for(i0=0, iB=A->tli;iB<=A->bri;i0++, iB++) {

    /* faire une fonction */
    if ((fill == INGEMM) && ((C->alloc == CBLK) || (C->alloc == BLK))) {
      SolverMatrix* solvC = &C->a[i0];

      if (C->alloc != BLK) {
	DBMatrix_Symb2SolvMtx(solvC);
      } else {
	dim_t j;
	/* 	SolverMatrix solver; */
	for(j=C->cia[iB];j<C->cia[iB+1];j++) {
	  DBMatrix_Symb2SolvMtx(C->ca[j]->solvmtx);
	}
      }

      DBMatrix_AllocSymbmtx(i0, iB, C, CBLK, C->cia, C->cja, C->ca, Phidal->cia, Phidal->cja, Phidal->ca, solvC, "N", BL);
    }

    { /* TODO : memcpy ? */
      int jj;
      int k = iB;
      DBMatrix* L = A;

      nk = 0;
      for(jj=L->ria[k];jj<L->ria[k+1]/* -1 */;jj++)
	{
	  jak[nk] = L->rja[jj];
	  rak[nk] = L->ra[jj];
	  nk++;
	}
    }
    
    DBMatrix_sGEMM_sym(iB, alpha, A, &A->cia, nk, jak, rak, C, &C->cia, D,
		       tabA, tabC, E, F, W, A->tli, A->alloc);

    if ((fill == INGEMM) && (A->alloc == RBLK))
      SolverMatrix_Clean(&A->a[iB-A->tli]);

  }

  if ((fill == INGEMM) && (A->alloc == RBLK)) {
    free(A->a);
    A->a = NULL; /* used on DBMatrix_Clean(); */
  }

  /* TODO : clean avec alloc BLK ? */
  /* TODO : clean de la symbol ? */

  free(E);
  free(F);
  free(W);
  free(tabA);
  free(tabC);

  if(rak != NULL) {
    free(rak);
    free(jak);
  }

}

void DBMatrix_sGEMM2_sym(int iB, COEF alpha,
			 DBMatrix* A, DBMatrixPart* Ap,
			 /* DBMatrix* B, DBMatrixPart* Bp, */
			 int nk, dim_t *jak, VSolverMatrix** rak,
			 DBMatrix* C, DBMatrixPart* Cp,
			 DBMatrix* D,
			 int *tabA, int *tabC,
			 COEF* E, COEF* F, COEF* W, dim_t tli, int Aalloc) {
  
  SymbolMatrix *SA;
  SymbolMatrix *SB;
  
  SymbolMatrix *SD;

  SymbolMatrix *SC;

  SolverMatrix *solvA, *solvB, *solvD, *solvC;
  
  dim_t j;
  int iA,/* jB, */jA,jC, m;
  dim_t k;
  int pSA, pSB;
  int qSA;

  int cdim, hdim, rdim, rdim2, mdim;
  int Srdim;
  blas_t strideA, strideA2, strideB, strideD;
  int bloknum;
  int decalcol, decalrow;
  int /* facecblknum, */ facestride;

  COEF *ac, *bc, *cc, *wc, *fc, *dc;
  COEF *Ec, *ec;

  COEF alphadiag;

  COEF zero = 0.0, one = 1.0;
  char *opA = "N"; /** Ne pas transposer A **/
  char *opB = "T"; /** Transpose B **/
  int UN = 1;

  int tabi;
  int ii;
  
  /* parcours des blk phidal (de B) */
  /* XXXXXXXXXXXXX vrai boucle */
/*   int jB; */
/*   for(j=0, jB=B->ria[iB];jB<B->ria[iB+1];j++, jB++) { */
/*     SB = &B->ra[jB]->symbmtx; */
/*     solvB = B->ra[jB]->solvmtx; */
/*     iA = B->rja[jB]; */

  for(j=0; j<nk; j++) { /* Utilisé dans DBMatrix_GEMMpart */
    SB    = &rak[j]->symbmtx;
    solvB =  rak[j]->solvmtx;
    iA    =  jak[j];

    SD = &D->ca[D->cia[iA]]->symbmtx; /* Diagonal Matrix. first phidal block is diagonal block */
    solvD = D->ca[D->cia[iA]]->solvmtx; /* Diagonal Matrix. first phidal block is diagonal block */

    if(SB->bloknbr ==0) continue;
            
    /***********************/
    /* Recherche des blocs */
    /***********************/
    tabi=0;
    jA=Ap->ia[iA];      
    for(jC=Cp->ia[iB];jC<Cp->ia[iB+1];jC++) {
      /* Recherche du block correspondant dans A (s'il existe) */
      while(jA<Ap->ia[iA+1]) { if (Ap->ja[jA] < Cp->ja[jC]) jA++; else goto test; }
      continue;
    test:
      if (Ap->ja[jA] != Cp->ja[jC]) continue;
      assert(Ap->ja[jA] == Cp->ja[jC]);
	
      tabC[tabi] = jC; 
      tabA[tabi] = jA; tabi++;
    }
          
    /* parcours des cblk (de SB) */
    for(k=0;k<SB->cblknbr;k++) {

      cdim = SB->ccblktab[k].lcolnum - SB->ccblktab[k].fcolnum +1;

#ifdef DEBUG_M2
      if ((Aalloc == ONE || Aalloc == RBLK))
	assert(SB->cblktlj == SB->facedecal);
#endif

      strideA = strideA2 = A->hdim[k+SB->facedecal]; /* TODO : a calculer pour V2 */

      strideB = SB->stride[k];
      strideD = SD->stride[k];

      hdim = SB->hdim[k]; /* the number of columns of the matrix Bt */ /* todo : deplacer vers le bas */
      if (hdim ==0) continue;

      /**************************************************/
      /* ConcatÃ©nation des blocks de A en fonction de C */
      /**************************************************/
      rdim=0;
      Ec = E;
	
      for(ii=0;ii<tabi;ii++) {
	jA = tabA[ii];
	SA    = &Ap->a[jA]->symbmtx;
	solvA = Ap->a[jA]->solvmtx;
	if(Aalloc == RBLK) {
	  strideA2 = SA->stride[k];
	}
	if(Aalloc == BLK) {
	  strideA2 = SA->stride[k];
	}

	Srdim = SA->hdim[k]; /* the number of rows of the matrix A */
	if (Srdim == 0) continue;

	/* copie du cblk de SA dans E */
	pSA  = SA->bcblktab[k].fbloknum;
	check(A, solvA, SA);
	ac = solvA->coeftab + solvA->bloktab[pSA].coefind;
	ec = Ec;
	for(m=0;m<cdim;m++) {
	  BLAS_COPY(Srdim, ac, UN, ec, UN);
	  ac += strideA2;
	  ec += strideA;
	}
	Ec+=Srdim; rdim+=Srdim;
      }
      /* ** */

      /***********************************************************/
      /* "Diviser" le bloc de SB par le bloc diagonal de D dans F*/
      /***********************************************************/
      check(B, solvB, SB);
      check(D, solvD, SD);
      pSB=SB->bcblktab[k].fbloknum; /* ICI */
      assert(solvB->symbmtx.bloknbr > pSB);
      bc = solvB->coeftab + solvB->bloktab[pSB].coefind;
      dc = solvD->coeftab + solvD->bloktab[ SD->bcblktab[k].fbloknum ].coefind;

      fc = F; /** Pointeur vers la m-ieme colonne dans le buffer F **/

      /* 	assert(cdim*strideB <= Fsize); */
      for(m=0;m<cdim;m++) {
#ifndef DEBUG_NOALLOCATION      
	alphadiag = 1.0 / dc[m*strideD + m]; /** alpha est l'inverse du m-ieme terme diagonal du bloc diagonal de D **/
#endif
	BLAS_COPY(hdim, bc, UN, fc, UN);     /** On copie les bloc extra diagonaux dans F **/ /* hdim = the number of rows of B ! */
	BLAS_SCAL(hdim, alphadiag,  fc, UN); /** On divise la m-ieme colonne par le terme diagonal **/
	  
	bc += strideB;
	fc += strideB;
      }
      /*****************************************************/
      /* -----Affectation dans C                                */
      /*****************************************************/
	
      for(pSB=SB->bcblktab[k].fbloknum; pSB<=SB->bcblktab[k].lbloknum; pSB++) {
	fc = F + solvB->bloktab[pSB].coefind - solvB->bloktab[ SB->bcblktab[k].fbloknum ].coefind;

	mdim = SB->bloktab[pSB].lrownum - SB->bloktab[pSB].frownum+1; /*** Largueur de la zone modifiÃÂ©e 
									   == hauteur du bloc p dans le bloc colonne k **/
	/*****************************************************/
	/* GEMM                                              */
	/*****************************************************/
	/* 	  printfv(5, "GEMM : rdim %d, mdim %d, cdim %d, 1.0, ec, strideA %d, fc, strideB %d, 0.0, W, strideA %d\n", */
	/* 		 rdim, mdim, cdim, strideA, strideB, strideA); */
	if(strideA ==0) continue;
	BLAS_GEMM(opA, opB, rdim, mdim, cdim, one, E, strideA, fc, strideB, zero, W, strideA);

	/* *****************************/
	SC = &Cp->a[Cp->ia[iB]]->symbmtx;
	solvC = Cp->a[Cp->ia[iB]]->solvmtx; /* todo : inutile ? */

	/** Nombre de colonne "ÃÂ  gauche" de la zone  modifiÃÂ©e dans le bloc colonne facecblknum **/	  
	decalcol = SB->bloktab[pSB].frownum - SC->ccblktab[0].fcolnum; 
	  
	/* *****************************/
	/* parcours des blocs de la cblk nÂ°k de A */
	rdim2=0;
	  
	for(ii=0;ii<tabi;ii++) {
	  jA = tabA[ii];
	  jC = tabC[ii];
	    
	  assert(Ap->ja[jA] == Cp->ja[jC]);
	    
	  SC = &Cp->a[jC]->symbmtx; 
	  solvC = Cp->a[jC]->solvmtx;
	  SA = &Ap->a[jA]->symbmtx; 
	  solvA = Ap->a[jA]->solvmtx;

	  check(C, solvC, SC);
	  facestride  = SC->stride[0];   /** Stride du cblk en face du bloc extra-diagonal p **/

	  for(qSA=SA->bcblktab[k].fbloknum;qSA<=SA->bcblktab[k].lbloknum;qSA++) {
	    /* *****************************/
	      
	    bloknum = SC->bcblktab[0].fbloknum;     /** Indice du bloc **/
	      
	    /** Calcul du pointeur de debut du bloc correspondant ÃÂ  A(q,k) dans W **/
	    wc = W + rdim2 + solvA->bloktab[qSA].coefind - solvA->bloktab[ SA->bcblktab[k].fbloknum/*todo : rdim*/ ].coefind; /* +  */

	    /** Calcul du pointeur de debut de la zone modifiÃÂ©e dans le bloc C(bloknum, facebloknum) */
	    check(C, solvC, SC);
	    decalrow = SA->bloktab[qSA].frownum - SC->bloktab[bloknum].frownum; /* TODO SC ? C ? */
	    cc = solvC->coeftab + solvC->bloktab[bloknum].coefind + decalcol*facestride + decalrow;
	      
	    hdim = SA->bloktab[qSA].lrownum - SA->bloktab[qSA].frownum+1; /** Hauteur du bloc colonne qSA **/
	      
	    /*** Update de la contribution ***/
	    for(m=0; m < mdim; m++)
	      {
		BLAS_AXPY(hdim, alpha, wc, UN, cc, UN);
		wc += strideA;
		cc += facestride;
	      }
	  }
	    
	  rdim2 += SA->hdim[k]; /* the number of rows of the matrix A */ /* TODO : on peut aussi faire avancer wc au fur et a mesure */
	}

	/** Mettre ÃÂ  jour la hauteur des bloc extra diagonaux qui restent **/
	/* rdim -= SB->bloktab[pSB].lrownum - SB->bloktab[pSB].frownum+1; /\*a faire q=p*\/ */
      }
    }

  }
    
}

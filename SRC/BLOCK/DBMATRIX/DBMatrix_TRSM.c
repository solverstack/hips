/* @authors J. GAIDAMOUR */

#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#include <assert.h>
#include "block.h"

#include "base.h"

/* #define printfv(5, arg...) */

/*TODO : Virer les vecteurs temporaires qui ne servent pas.*/

/* TODO : a adapter au non direct */

/*SolverMatrix -> SolverMatrix*/
/*Symbol*/
/*coefind*/
/*.blocknum*/

/* kL et kM != peut etre eviter en faisant comme pour Phidal : tli bri */

/* Obligé de commencé a bloknum en haut ?*/

#define MAX(x,y) (((x)<(y))?(y):(x))
#define MIN(x,y) (((x)>(y))?(y):(x))

/* void printLD(SolverMatrix* LD, char* filename); */

/* TODO ! notation fc, opF ...*/

void DBMatrix_TRSM(flag_t unitdiag, DBMatrix* L, DBMatrix* M/* ,  PhidalHID *BL */) 
{

  /*********************************************************************/
  /* this function does M = L^-1.M                                     */
  /* L is a lower triangular SolverMatrix                              */
  /* M is a SolverMatrix                                               */ 
  /* The cblkblock pattern of M must be the same as L                  */
  /*********************************************************************/

  SolverMatrix* solvL=NULL;
  SymbolMatrix *symbL=NULL;  
  SolverMatrix* solvM=NULL;

  int i, i0, j;
  int p, q, kL, kglobal, m;
  COEF *W; /** buffers de travail **/
  int UN=1;
  COEF alpha, beta;
  blas_t strideL;
  blas_t strideM;
  int cdim, rdim, hdim, hdim2, mdim;
  COEF *cc; 
  COEF *bc; 
  COEF *fc;
  COEF *wc; 
  int facestride;
  int bloknum, facecblknum;
  int decalcol, decalrow;

  SymbolMatrix* symbMs;
  int klast;

  int klocal;
  blas_t strideM2;

  int pSA;
  COEF* ac;
  int Srdim;

  assert((L->alloc == ONE) || (L->alloc == CBLK));
  assert((M->alloc == ONE) || (M->alloc == CBLK) || (M->alloc == RBLK));

  /*** Allocation de buffers de travail ***/
  W = (COEF *)malloc(sizeof(COEF)*M->coefmax);
  assert(W != NULL);

  if (L->alloc == ONE) {   /* a suppr */
    solvL = L->a;      
    symbL = &solvL->symbmtx;
  }

  if (M->alloc == ONE) {
    solvM = M->a;
  }
 
  kglobal=0; kL=0; /* todo : deplacer dans les if d'avant */
  for(i0=0,i=L->tlj; i<=L->brj; i0++, i++) { /* todo : simplier, sans i0*/
    if (L->alloc == CBLK) {
      solvL = &L->a[i0];     
      symbL = &solvL->symbmtx;
      kL=0;
    }

    if (M->alloc == CBLK) {
      solvM = &M->a[i0];     
    }
      
    klocal=0;

    /* for(;kL<symbL->cblknbr;kL++, kglobal++, klocal++) */
    klast = kglobal + L->ca[L->cia[i]]->symbmtx.cblknbr;
    for(; kglobal<klast; kL++, kglobal++, klocal++)
      {
	cdim    = symbL->ccblktab[kL].lcolnum - symbL->ccblktab[kL].fcolnum + 1;/** Largeur du bloc colonne **/

	strideL = symbL->stride[kL];
	
	/*TODO : ne pas distinguer*/
	if ((M->alloc == ONE) || (M->alloc == RBLK))
	  strideM = M->hdim[kglobal];
	else  /* ONE ou CBLK */
	  strideM = solvM->symbmtx.stride[klocal];

#ifdef DEBUG_M
/*fx RBLK 	if ((M->alloc == ONE) || (M->alloc == RBLK)) */
/* 	  assert(strideM == solvM->symbmtx.stride[klocal]); */

	if (M->alloc == ONE)
	  assert(strideM == solvM->symbmtx.hdim[kglobal]);
	if (M->alloc == CBLK)
	  assert(strideM == solvM->symbmtx.hdim[klocal]);
#endif	

	if ((M->alloc == ONE) || (M->alloc == CBLK))
	  strideM2 = strideM;

	/** Calcul du pointeur dans coeftab de L vers le dÃ©but des coefficients du bloc diagonal **/ 
	p = symbL->bcblktab[kL].fbloknum;
	cc = solvL->coeftab + solvL->bloktab[p].coefind;
	
	/**********************************************************************/
	/* "Diviser" les blocs de M par le bloc diagonal de L                 */
	/**********************************************************************/
	{
	  /** On effectue l'operation : M = M.(L^-1)t  avec M = l'ensemble compacte des blocs extra-diagonaux du bloc colonne de M **/
	  int bloknumMs;

	  char *side = "R"; /* a sortir */
	  char *uplo = "L";
	  char *trans = "T";
	  char *diag; if(unitdiag==1) diag = "U"; else diag = "N";

	  alpha = 1.0;

	  /* parcours des blk phidal (de M) */
	  for(j=M->cia[i];j<M->cia[i+1];j++) {
	    symbMs = &M->ca[j]->symbmtx;

	    if(M->alloc == RBLK) {
	      solvM = &M->a[M->cja[j]-M->tli];
	      strideM2 = symbMs->stride[klocal];
/* 	      printfv(5, "%d stridem %d %d rdim %d\n", M->cja[j]-M->tli, strideM, strideM2, symbMs->bcblktab[klocal].hdim); */
/* 	      printfv(5, "%d %d\n", strideM2, symbMs->bcblktab[klocal].hdim); */
/* 	      assert(strideM2 == symbMs->bcblktab[klocal].hdim); */
	    }

	    /*Srdim au lieu de rdim*/
	    rdim = symbMs->hdim[klocal]; /** Hauteur de la surface compactÃ©e des blocs **/
	    if (rdim == 0) { continue; }

	    /** Calcul du pointeur dans coeftab vers le dÃ©but des coefficients de M */
	    bloknumMs = symbMs->bcblktab[klocal].fbloknum;   /** Indice du premier bloc (triangulaire) dans M **/ /*!pas M,*/
	    bc = solvM->coeftab + solvM->bloktab[bloknumMs].coefind;

/* 	    printfv(5, "rdim %d cdim %d strideL %d strideM %d\n", rdim, cdim, strideL, strideM2); */
	    BLAS_TRSM(side,  uplo, trans, diag, rdim, cdim, alpha, cc, strideL, bc, strideM2);
	  }
	}

	/** Boucle sur les blocs de M **/

	/** On l'initialise Ã  la heuteur totale **/
	/*TODO ! RDIM 2 fois, ici a virer*/
/* 	continue; */

	for(p=symbL->bcblktab[kL].fbloknum+1;p<=symbL->bcblktab[kL].lbloknum;p++)
	  {
	    char *opA = "N"; /** Ne pas transposer A(**) **/ /*a sortir*/
	    char *opF = "T"; /** Transpose F **/

	    /****************************************************************************************/
	    /** Pour tout les blocs extra-diagonaux {A(j,k), j>= i} faire A(i,j) = A(i,j) - W(i, ) **/
	    /****************************************************************************************/
	    j=M->cia[i];
	    symbMs = &M->ca[M->cia[i]]->symbmtx; /* 1ere */

	    if(M->cia[i] == M->cia[i+1])
	      continue;

	    /* Winit = M->a[M->cja[M->cia[i]]-M->tli].bloktab[M->ca[M->cia[i]]->bcblktab[klocal].fbloknum].coefind; */

	    /** Indice du bloc colonne en face du bloc extra-diagonal p de L **/
	    /* Info : facesolv alway current solvM != FACTO */

	    facecblknum = symbL->bloktab[p].cblknum - symbMs->facedecal; /* ?? a confronter a FACTO et ONE CBLK RBLK */
/* 	    printfv(5, "%d %d\n",symbMs->facedecal, symbMs->cblktlj); */

#ifdef DEBUG_M
	    if ((M->alloc == ONE) || (M->alloc == RBLK))
	      assert(symbMs->facedecal == symbMs->cblktlj);
#endif

	    /** Stride du cblk en face du bloc p **/
	    /*TODO : ne pas distinguer*/
	    if ((M->alloc == ONE) || (M->alloc == RBLK))
	      facestride = M->hdim[symbL->bloktab[p].cblknum /*facecblknumGlobal*/];
	    else  /* ONE ou CBLK */
	      facestride = solvM->symbmtx.stride[facecblknum];

	    /** Nombre de colonne "Ã  gauche" de la zone modifiÃ©e dans le bloc colonne facecblknum **/
	    decalcol = symbL->bloktab[p].frownum - symbMs->ccblktab[facecblknum].fcolnum;

	    /*** Largueur de la zone modifiÃ©e == hauteur du bloc p dans le bloc colonne k **/
	    mdim = symbL->bloktab[p].lrownum - symbL->bloktab[p].frownum+1; /* todo : = hdim */

	    /** Pour le GEMM, calcul du pointeur dans F vers le debut des coefficients du bloc extra-diagonal p correspondant **/
	    fc = solvL->coeftab + solvL->bloktab[p].coefind; /* a sortir, depend que de p */
	    /** Pour le GEMM, Calcul de la hauteur du bloc p **/
	    hdim = symbL->bloktab[p].lrownum - symbL->bloktab[p].frownum +1;
	    
	    for(/* j=M->cia[i] */;j<M->cia[i+1];j++) {
	      symbMs = &M->ca[j]->symbmtx;
	      if(M->alloc == RBLK) {
		solvM = &M->a[M->cja[j]-M->tli];
		facestride = symbMs->stride[facecblknum];
		strideM2   = symbMs->stride[klocal];
	      }
 
	      /* GEMM */
	      Srdim = symbMs->hdim[klocal]; 
	      if (Srdim ==0) continue;

	      pSA  = symbMs->bcblktab[klocal].fbloknum;
	      ac = solvM->coeftab + solvM->bloktab[pSA].coefind;
	      
	      alpha = 1.0;  /* a sortir */
	      beta = 0.0;   /* a sortir */

	      assert(strideM*hdim <= M->coefmax);
	      assert(strideM>=Srdim);
	      BLAS_GEMM(opA, opF, Srdim, hdim, cdim, alpha, ac, strideM2, fc, strideL, beta, W, strideM);

	      /* -- GEMM*/
	      /*** Stocker dans W le produit des blocs >>>>>{A(q,k), q>=p} avec Ft(p,k) ***/
	      
	      bloknum = symbMs->bcblktab[facecblknum].fbloknum; /** Indice du 1er bloc colonne ds facebloknum **/

	      for(q=symbMs->bcblktab[klocal].fbloknum;q<=symbMs->bcblktab[klocal].lbloknum;q++)
		{

		  /* skip block that cannot match */
		  if (bloknum >= symbMs->bcblktab[facecblknum].lbloknum+1/*todo le +1*/) goto fin;

		  while(symbMs->bloktab[q].frownum > symbMs->bloktab[bloknum].lrownum) {
		    bloknum++;
		    if (bloknum >= symbMs->bcblktab[facecblknum].lbloknum+1/*todo le +1*/) goto fin;
		  }
		  
		  /* for every block that match */
		  while(symbMs->bloktab[q].lrownum >= symbMs->bloktab[bloknum].frownum) {
		    
		    /* assert ds le cas direct*/
		    /*		assert(symbMs->bloktab[bloknum].frownum <= symbMs->bloktab[q].frownum);*/
		    /*		assert(symbMs->bloktab[bloknum].lrownum >= symbMs->bloktab[q].lrownum);*/
		    
		    decalrow = symbMs->bloktab[q].frownum - symbMs->bloktab[bloknum].frownum;
		    
		    /** Calcul du pointeur de debut du bloc correspondant Ã  A(q,k) dans W **/
		    wc = W + solvM->bloktab[q].coefind - solvM->bloktab[symbMs->bcblktab[klocal].fbloknum].coefind + MAX(0,-decalrow);

		    /* printfv(5, ">>> %d\n", solvM->bloktab[q].coefind - solvM->bloktab[symbMs->bcblktab[klocal].fbloknum].coefind  */
/* 			   + MAX(0,-decalrow)); */

/* 		    assert(solvM->bloktab[q].coefind - solvM->bloktab[M->ca[M->cia[i]]->bcblktab[klocal].fbloknum].coefind == */
/* 			    */
/* 			   solvM->bloktab[q].coefind - solvM->bloktab[symbMs->bcblktab[klocal].fbloknum].coefind); */

		    /** Calcul du pointeur de debut de la zone modifiÃ©e dans le bloc A(bloknum, facebloknum) **/
		    bc = solvM->coeftab + solvM->bloktab[bloknum].coefind + decalcol*facestride + MAX(0,decalrow);

/* 		    printfv(5, "bloknum = %d> %d %d %d\n", bloknum, solvM->bloktab[bloknum].coefind,  decalcol*facestride, MAX(0,decalrow)); */
		    
		    /*** Update de la contribution ***/
		    hdim2 = MIN(symbMs->bloktab[q].lrownum, symbMs->bloktab[bloknum].lrownum) - 
		      MAX(symbMs->bloktab[q].frownum, symbMs->bloktab[bloknum].frownum) +1; /** Hauteur du bloc sur lequel on effectue la différence **/
		    
		    /*assert cas direct*/
		    /*		assert(hdim == symbMs->bloktab[q].lrownum -  symbMs->bloktab[q].frownum +1);*/
		    
		    /*** Update de la contribution ***/
		    alpha = -1.0;
		    for(m=0; m < mdim; m++)
		      {
			/* printfv(5, "BLAS_AXPY : %d %p %p\n", hdim2, wc, bc); */
			BLAS_AXPY(hdim2, alpha, wc, UN, bc, UN);
			wc += strideM;
			bc += facestride;
		      }
		    
		    bloknum++;
		    if (bloknum >= symbMs->bcblktab[facecblknum].lbloknum+1/*todo +1*/) break;
		  }
		  
		fin:
		  bloknum--;
		  ;
		}
	    }


	  }
      
      }
  }

  /** LibÃ©ration des buffers **/
  free(W);
}

/* @authors J. GAIDAMOUR */

#include <stdio.h>
#include <stdlib.h>
#include "block.h"
#include "prec.h"

/* TODO :  a utiliser, a revoir nouvelle struct */
void DBMatrix_Init(DBMatrix *a)
{
#ifdef DEBUG_M
  assert(a!=NULL);
#endif
  a->csc = 0;
  a->symmetric = 0;
  a->dim1 = 0;
  a->dim2 = 0;
  a->tli = 0;
  a->tlj = 0;
  a->bri = 0;
  a->brj = 0;
  a->cia = NULL;
  a->cja = NULL;
  a->ca  = NULL;
  a->ria = NULL;
  a->rja = NULL;
  a->ra  = NULL;
  a->virtual = NOVIRTUAL;

  a->bloknbr = 0;
  a->bloktab = NULL;

  /* Init sur solvmtx Ã  appeller */
}

/* TODO : assert NULL au lieu de if*/
void DBMatrix_Clean(DBMatrix* a)
{
/*   printfv(5, "Clean\n"); */

  assert((a->virtual == NOVIRTUAL) || (a->virtual == VIRTUAL) || (a->virtual == COPY));

  dim_t i;
#ifdef DEBUG_M
  assert(a!=NULL);
#endif

  if (a->virtual == NOVIRTUAL) {
    if(a->cia != NULL)
      free(a->cia);
    if(a->cja != NULL)
      free(a->cja);
    if(a->ca != NULL)
      free(a->ca);
    if(a->ria != NULL)
      free(a->ria);
    if(a->rja != NULL)
      free(a->rja);
    if(a->ra != NULL)
      free(a->ra);
    
    if(a->alloc == RBLK) {
      assert(a->ccblktab != NULL);
      assert(a->hdim != NULL);

      free(a->ccblktab);
      free(a->hdim);
    } else {
      /*commentÃ© temporairement */
      /*       assert(a->ccblktab == NULL); */
      /*       assert(a->hdim == NULL); */

      /* S, CBLK, phidal */
      if(a->ccblktab != NULL)
	free(a->ccblktab);
      if(a->hdim != NULL)
	free(a->hdim);
    }

    if(a->bloktab != NULL) {
      for(i=0;i<a->bloknbr;i++)
	SymbolMatrix_Clean(&a->bloktab[i].symbmtx);

      if(a->alloc == BLK) {
	for(i=0;i<a->bloknbr;i++) {
	  SolverMatrix_Clean(a->bloktab[i].solvmtx);
	  free(a->bloktab[i].solvmtx);
	}
      }

      free(a->bloktab);
    }
    
  }

  if ((a->virtual == NOVIRTUAL) || (a->virtual == COPY)) {
    int i0, ilast;
    
    if (a->cblktosolvmtx != NULL) 
      free(a->cblktosolvmtx);

    if (a->alloc == ONE)
      ilast = 1;
    else if (a->alloc == CBLK)
      ilast = a->brj - a->tlj + 1;
    else 
      ilast = a->bri - a->tli + 1;
    
    if (a->a != NULL) { /* sur E, avec GEMM alloc/free */
      for(i0=0; i0<ilast; i0++) {
	SolverMatrix_Clean(&a->a[i0]);
      }
      free(a->a);
    }

  }

  a->symmetric = 0;
  a->csc = 0;
  a->dim1 = 0;
  a->dim2 = 0;
  a->tli = 0;
  a->tlj = 0;
  a->bri = 0;
  a->brj = 0;
  a->virtual = 0;
  a->bloknbr = 0;
  /* a completer ... */


  /* TMP, voir DBMatrix_SetupV(DBMatrix *M, DBMatrix *Cpy) */
   if (a->virtual == COPY) {
     if(a->ca != NULL)
       free(a->ca);
     if(a->ra != NULL)
       free(a->ra);
     
     if(a->bloktab != NULL) {
       if(a->alloc == BLK) {
	 for(i=0;i<a->bloknbr;i++) {
	   SolverMatrix_Clean(a->bloktab[i].solvmtx);
	   free(a->bloktab[i].solvmtx);
	 }
      }
       
       free(a->bloktab);
     }
   }


}



void DBPrec_Init(DBPrec *P)
{
  P->symmetric = 0;
  P->dim = 0;
  P->levelnum = 0;
  P->forwardlev = 0;
  P->LU = NULL;
  P->EF = NULL;
  P->EF_DB = NULL;
  P->S = NULL;
  P->B = NULL;

  P->prevprec = NULL;
  P->nextprec = NULL;

  P->schur_method = 0;
  /*   P->pivoting = 0; */
  /*   P->permtab = NULL; */
  
  /*   P->tol_schur = 1; */

  P->phidalS = NULL;
  P->phidalPrec = NULL;

/*   p->info = NULL; */
}


void DBPrec_Print(DBPrec *P)
{

  printfd("Prec : symmetric : %d dim : %d levelnum : %d forwardlev : %d schur_method : %d\n",   
         P->symmetric,
         P->dim,
         P->levelnum,
         P->forwardlev,
         P->schur_method);

  if(PREC_L_BLOCK(P) == NULL)
    printfd("Prec: L==NULL\n");
  if(PREC_U_BLOCK(P) == NULL)
    printfd("Prec: U==NULL\n");
/*   if(P->D == NULL) */
/*     printfd("Prec: D==NULL\n"); */
  if(PREC_E(P) == NULL)
    printfd("Prec: E==NULL\n");
  if(PREC_F(P) == NULL)
    printfd("Prec: F==NULL\n");
  if(PREC_EDB(P) == NULL)
    printfd("Prec: E==NULL\n");
  if(PREC_FDB(P) == NULL)
    printfd("Prec: F==NULL\n");

  if(PREC_SL_BLOCK(P) == NULL)
    printfd("Prec: SL==NULL\n");

  if(PREC_SU_BLOCK(P) == NULL)
    printfd("Prec: SU==NULL\n");

  if(P->B == NULL)
    printfd("Prec: B==NULL\n");

  if(P->prevprec == NULL)
    printfd("Prec: prevprec==NULL\n");

  if(P->nextprec == NULL) {
    printfd("Prec: nextprec==NULL\n");
  }
  else {
    printfd("Prec: nextprec==: (!=NULL)\n");
    DBPrec_Print(P->nextprec);
  }
}



void DBPrec_Clean(DBPrec *P)
{

#ifdef DEBUG_M
  if(P->forwardlev > 0) {
    assert(P->nextprec != NULL || (P->phidalPrec != NULL));
  } else
    assert(P->nextprec == NULL);
#endif

  if(P->forwardlev  > 0)
    {
      if(P->nextprec != NULL)
        {
          if(PREC_SL_BLOCK(P) == PREC_L_BLOCK(P->nextprec)) PREC_SL_BLOCK(P)=NULL; /* FIX, ne pas dÃ©sallouer 2 fois */
          if(PREC_SU_BLOCK(P) == PREC_U_BLOCK(P->nextprec)) PREC_SU_BLOCK(P)=NULL; /* FIX, ne pas dÃ©sallouer 2 fois */
          DBPrec_Clean(P->nextprec);
          free(P->nextprec);
        }
      
      if (P->EF != NULL) {
	if(PREC_E(P) != NULL)
	  {
	    PhidalMatrix_Clean(PREC_E(P));
	    free(PREC_E(P));
	  }
	
	if(PREC_F(P) != NULL)
	  {
	    PhidalMatrix_Clean(PREC_F(P));
	    free(PREC_F(P));
	  }
	
	free(P->EF);
      }
  
      if (P->EF_DB != NULL)  {
	if(PREC_EDB(P) != NULL)
	  {
	    DBMatrix_Clean(PREC_EDB(P));
	    free(PREC_EDB(P));
	  }
	
	if(PREC_FDB(P) != NULL)
	  {
	    DBMatrix_Clean(PREC_FDB(P));
	    free(PREC_FDB(P));
	  }
	
	free(P->EF_DB);
      }
  
      if (P->S != NULL) {
	if(PREC_SL_BLOCK(P) != NULL)
	  {
	    if(P->schur_method == 1) { /* sinon NULL, dc test redondant */
	      DBMatrix_Clean(PREC_SL_BLOCK(P));
	      free(PREC_SL_BLOCK(P));
	    }
	  }
	
	if((PREC_SU_BLOCK(P) != NULL) && (P->symmetric == 0))
	  {
	    if(P->schur_method == 1) {/* sinon NULL, dc test redondant */
	      DBMatrix_Clean(PREC_SU_BLOCK(P));
	      free(PREC_SU_BLOCK(P));
	    }
	  }
	
	free(P->S);
      }

      if (P->B != NULL) {
	if(PREC_B(P) != NULL)
	  {
	    PhidalMatrix_Clean(PREC_B(P));
	    free(P->B);
	  }
	
	free(P->B);
      }

    }
  
  if (P->LU != NULL) {
    if(PREC_L_BLOCK(P) != NULL)
      {
	DBMatrix_Clean(PREC_L_BLOCK(P));
	free(PREC_L_BLOCK(P));
      }
    
    if((PREC_U_BLOCK(P) != NULL) && (P->symmetric == 0))
      {
	DBMatrix_Clean(PREC_U_BLOCK(P));
	free(PREC_U_BLOCK(P));
      }
    
    free(P->LU);
  }

/*   if(P->pivoting == 1) */
/*     free(P->permtab); */

  P->symmetric = 0; /* pourquoi ne pas appeller INIT ? */
  P->dim = 0;
  P->levelnum = 0;
  P->forwardlev = 0;
  P->schur_method = 0;

  if(P->phidalS != NULL)
    {
      /* P->phidalS->cia = NULL; */
/*       P->phidalS->cja = NULL; */
/*       P->phidalS->ria = NULL; */
/*       P->phidalS->rja = NULL; */
      
      PhidalMatrix_Clean(P->phidalS);
      free(P->phidalS);
    }

  if(P->phidalPrec != NULL)
    {
      PhidalPrec_Clean(P->phidalPrec);
      free(P->phidalPrec);
    }

/*   P->pivoting = 0; */

/*   P->tol_schur = 1; */
}



/* @authors J. GAIDAMOUR */

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h> /* strcmp */

#include "block.h"

/* TODO : coefnbr = -1*/

void DBMatrix_Setup_SYMBOL(DBMatrix* m, SymbolMatrix* symbmtx, 
			   PhidalHID *BL, int_t levelnum, int_t levelnum_l);

/* subfunctions called by DBMatrix_Setup_symbol_pattern() */
void DBMatrix_Setup_cblktosolvmtx(DBMatrix* m);
/**/

void DBMatrix_Setup(dim_t tli, dim_t tlj, dim_t bri, dim_t brj, char *UPLO, char *DIAG, 
		    int_t locally_nbr, DBMatrix* m, 
		    SymbolMatrix* symbmtx, PhidalHID *BL, 
		    int_t levelnum, int_t levelnum_l, alloc_t alloc) {

  DBMatrix_Setup_HID(tli, tlj, bri, brj, UPLO, DIAG, locally_nbr, m, BL);

  m->alloc = alloc;

  m->cblknbr = symbmtx->cblknbr;
  m->nodenbr = symbmtx->nodenbr;

  if ((m->symmetric == 0) && 
      ((strcmp(UPLO, "U") == 0)/* U */ || (tli<tlj) /* F */)) 
    {
      DBMatrix_Transpose(m);
      /*fix*/
      m->alloc = alloc;
    }

  DBMatrix_Setup_SYMBOL(m, symbmtx, BL, levelnum, levelnum_l);
 
  m->coefmax = DBMatrix_calcCoefmax(m, m);


  /* a deplacer */
  {
    int i0,i,j;

    int tl, br;
    INTL *ia; dim_t *ja;
    tl = m->tlj;
    br = m->brj;
    ia = m->cia;
    ja = m->cja;
    
    for(i0=0,i=tl;i<=br;i0++,i++) {
      for(j=ia[i];j<ia[i+1];j++) {
	m->ca[j]->solvmtx->coefmax = m->coefmax;
      }
    }
  }
  /* */


  if ((m->symmetric == 0) && 
      ((strcmp(UPLO, "U") == 0)/* U */ || (tli<tlj) /* F */)) 
    {
      DBMatrix_Transpose(m);
    }
  
  /* m->ccblktab allouÃ© ou non : pas d'importance pour ONE (sauf desalloc ) */
  if (m->alloc == RBLK) {
    m->ccblktab = symbmtx->ccblktab; 
    /*     symbmtx->ccblktab = NULL; TODO! */
  } else {
    m->ccblktab = NULL;
  }

  /* tmp */
  if (m->alloc == CBLK || m->alloc == BLK) {
    m->ccblktab = symbmtx->ccblktab; 
  }
}


void DBMatrix_Setup_SYMBOL(DBMatrix* m, SymbolMatrix* symbmtx, PhidalHID *BL, int_t levelnum, int_t levelnum_l) {
  /*****************************************/
  /** Fill-in of SymbolMatrix structures **/
  /*****************************************/
  int i, i0, j;

#ifdef DEBUG_M
  int DEBUG_bloknbr=0;
  int DEBUG_bloknbr1=0;
  int DEBUG_bloknbr2=0;
#endif

  int tl, br;
  INTL *ia; dim_t *ja;
  VSolverMatrix **a;
  SolverMatrix *solv=NULL;
  SymbolMatrix *symb=NULL, *SM;
  int *videb, *vilast, *vjdeb, *vjlast;
  int jdeb, jlast;

  /* m->hdim pas d'importance pour ONE (sauf desalloc) */
  if ((m->alloc == RBLK) || (m->alloc == BLK)) {
    m->hdim = (int*)malloc(sizeof(int)*symbmtx->cblknbr);
    memset(m->hdim, 0, sizeof(int)*symbmtx->cblknbr);
  } else {
    m->hdim = NULL;
  }

  /* tmp */
  if (m->alloc == CBLK) {
    m->hdim = (int*)malloc(sizeof(int)*symbmtx->cblknbr);
    memset(m->hdim, 0, sizeof(int)*symbmtx->cblknbr);
  }

  if ((m->alloc == ONE) || (m->alloc == CBLK) || (m->alloc == BLK)) {
    tl = m->tlj;
    br = m->brj;
    ia = m->cia;
    ja = m->cja;
    a  = m->ca;     /* TODO : change variable name 'a' bcse m->a exist */

    videb  = &jdeb;
    vilast = &jlast;
    vjdeb  = vjlast = &i;

  } else {
    assert(m->alloc == RBLK);

    tl = m->tli;
    br = m->bri;
    ia = m->ria;
    ja = m->rja;
    a  = m->ra;

    videb  = vilast = &i;
    vjdeb  = &jdeb;
    vjlast = &jlast;
  }

  if (m->alloc == ONE) {
    m->a = (SolverMatrix*)malloc(sizeof(SolverMatrix)/*(ilast == 1)*/);
    memcpy(&m->a[0].symbmtx, symbmtx, sizeof(SymbolMatrix));
    solv = &m->a[0];
    symb = &m->a[0].symbmtx;
    /*     SymbolMatrix_Init(symbmtx); TODO ! */

    assert(symb->ccblktab != NULL);

    assert(symb->virtual == 0);
    assert(symb->hdim    == NULL);
    assert(symb->stride  == NULL);
    symb->hdim = symb->stride = (int*)malloc(sizeof(int)*symb->cblknbr); 
    memset(symb->hdim, 0, sizeof(int)*symb->cblknbr);
    
    m->a[0].symbmtx.tli = m->a[0].symbmtx.ccblktab[0].fcolnum; /*FIX*/
  } else if (m->alloc == BLK) {
    m->a = NULL; /* (SolverMatrix*)malloc(sizeof(SolverMatrix)*m->bloknbr);  */
    
    symb = symbmtx; /* todo : cela fait faire trop de parcours avec Cut/Cut2*/
  } else {
    /* a ne pas faire dans le cas filling=0, 
       pour symÃ©trie avec free() et gÃ©nÃ©rer les symbol ds le GEMM aussi ? */
    m->a = (SolverMatrix*)malloc(sizeof(SolverMatrix)*(br-tl+1)); 
  }

  for(i0=0,i=tl; i<=br; i0++, i++) {
#ifdef DEBUG_M
    DEBUG_bloknbr2 = 0;
#endif
    jdeb  = BL->block_levelindex[levelnum];  /* a sortir de la boucle */
    jlast = BL->block_levelindex[levelnum_l]-1;

    /* levelnum et _l pas vraiment necessaire mais plus pratique */
    /* A voir pour E    assert(BL->block_index[jdeb] == symbmtx->cblktab[0].fcolnum);  /\* can be != 0 (for C/S) *\/ */
    /*     assert(BL->block_index[jlast+1]-1 == symbmtx->cblktab[symbmtx->cblknbr-1].lcolnum); /\* can be != symbmtx->nodenbr-1 *\/ */

    if ((m->alloc != ONE) && (m->alloc != BLK)) {
      solv = &m->a[i0];
      symb = &m->a[i0].symbmtx;   /* remplacer solv/symb par 1 seule variable vsolv*/
      
      SymbolMatrix_Cut2(symbmtx, symb,
			BL->block_index[*videb], BL->block_index[*vjdeb],
			BL->block_index[*vilast+1]-1, BL->block_index[*vjlast+1]-1, m->alloc);
#ifdef DEBUG_M
      DEBUG_bloknbr1 += symb->bloknbr;
#endif
  
      DBMatrix_Setup_solvermatrix(&m->a[i0], &m->a[i0].symbmtx);
      
      if(m->alloc == CBLK) {
	symb->hdim = symb->stride = (int*)malloc(sizeof(int)*symb->cblknbr); 
	memset(symb->hdim, 0, sizeof(int)*symb->cblknbr);
      } else { /* RBLK utilisÃ© ds caclCoefMax */
	symb->stride = m->hdim;
      }
      
    }
   
    for(j=ia[i];j<ia[i+1];j++) {
      jdeb = jlast = ja[j];
   
      SM = &a[j]->symbmtx;

      if (m->alloc != BLK) {
	/* hdim set in this function */      
	SymbolMatrix_Cut(symb, &a[j]->symbmtx,
			 BL->block_index[*videb], BL->block_index[*vjdeb],
			 BL->block_index[*vilast+1]-1, BL->block_index[*vjlast+1]-1);

	a[j]->solvmtx = solv /* &m->a[i0] */; /* renseignement correspondance solv<->symb pour tout remplissage CBLK RBLK ...*/

	/* set stride : SIMPLIFIER ONE==CBLK*/
	if (m->alloc == ONE)      
	  SM->stride = symb->hdim/*==stride*/ + SM->facedecal - /*f*/m->a[0].symbmtx.facedecal;
	else if (m->alloc == CBLK) {
	  SM->stride = symb->stride; assert(symb->hdim == symb->stride);
	} else { /* RBLK */
	  SM->stride = SM->hdim; /* symb->stride/\*m->=hdim*\/ + a[j]->facedecal- /\*f*\/m->a[0].symbmtx.facedecal; */
	}
	/**/

/* 	printfv(5, "cblktlj %d %d\n", symb->cblktlj, a[j]->cblktlj ); */
/* 	printfv(5, "facedecal %d %d\n", symb->facedecal,  a[j]->facedecal); */
/* 	assert(a[j]->facedecal == symb->facedecal); */

      } else /* BLK */ {

	SymbolMatrix_Cut2(symb, &a[j]->symbmtx,
			  BL->block_index[*videb], 
			  BL->block_index[*vjdeb],
			  BL->block_index[*vilast+1]-1, 
			  BL->block_index[*vjlast+1]-1, m->alloc);
	SM->hdim = SM->stride = (int*)malloc(sizeof(int)*SM->cblknbr);

	/* set hdim */
	{
	  SymbolMatrix* symbol = &a[j]->symbmtx;
	  dim_t k,p;
	  memset(symbol->hdim, 0, sizeof(int)*symbol->cblknbr);
	  for(k=0; k<symbol->cblknbr;k++) {
	    for(p=symbol->bcblktab[k].fbloknum;p<=symbol->bcblktab[k].lbloknum;p++) {
	      SM->hdim[k] += symbol->bloktab[p].lrownum - symbol->bloktab[p].frownum +1;
	    }
	  }
	}
	
	a[j]->solvmtx = (SolverMatrix*)malloc(sizeof(SolverMatrix));
	memset(&a[j]->solvmtx->symbmtx, 0, sizeof(SymbolMatrix)); /* pour desalloc virtual=0 et * == NULL */
	a[j]->solvmtx->symbmtx.bloknbr = a[j]->symbmtx.bloknbr; /* pour desalloc */

	DBMatrix_Setup_solvermatrix(a[j]->solvmtx, &a[j]->symbmtx);

      }

#ifdef DEBUG_M
      DEBUG_bloknbr += a[j]->symbmtx.bloknbr;
      if ((m->alloc != ONE) && (m->alloc != BLK))
	DEBUG_bloknbr2 += a[j]->symbmtx.bloknbr;
#endif

      /* TODO : en RBLK : faire ds le bon sens. TODO : faire par k ou par block phidal */
      /* mieux de calculer a part ou de faire l'aggregation dans Cut ? */
      {
	int *hdim;
	dim_t i;

	/*ONE et CBLK peuvent Ãªtre regroupÃ© : symb[0]->stride */
	/* SIMPLIFIER ? */
	if (m->alloc == ONE) {
	  hdim = symb->hdim + SM->facedecal- /*f*/m->a[0].symbmtx.facedecal;
	} else if (m->alloc == CBLK) {
	  hdim = symb->hdim; /*== symb->stride*/
	} else if (m->alloc == BLK) {
	  hdim = m->hdim    + SM->facedecal- /*f*/m->ca[0]->symbmtx.facedecal;
	  assert((m->hdim == symb->stride) || (m->alloc == BLK));
	  
	} else { /* RBLK */
	  hdim = m->hdim    + SM->facedecal- /*f*/m->a[0].symbmtx.facedecal;
	  assert((m->hdim == symb->stride) || (m->alloc == BLK));
	}

	for(i=0; i<SM->cblknbr; i++) {
	  hdim[i] += SM->hdim[i];
	}

	/* tmp */
	if (m->alloc == CBLK) {
	  hdim = m->hdim    + SM->facedecal- /*f*/m->a[0].symbmtx.facedecal;
	  for(i=0; i<SM->cblknbr; i++) {
	    hdim[i] += SM->hdim[i];
	  }
	}

      }
    } /* j loop */

    if (m->alloc == RBLK) {
      m->a[i0].symbmtx.ccblktab = NULL; /* ou virtuel sur db */
      free(m->a[i0].symbmtx.bcblktab); m->a[i0].symbmtx.bcblktab = NULL;
      /* plus maintenant      assert(m->a[i0].symbmtx.stride == NULL); */
      assert(m->a[i0].symbmtx.hdim == NULL);
      /* free(bloktab); rÃ©utilisÃ© sur les Cut1 */
    }
   
#ifdef DEBUG_M
/*     if ((m->alloc != ONE) && (m->alloc != BLK)) */
/*       assert(DEBUG_bloknbr2 == symb->bloknbr); */
#endif

  } /* i loop */

  DBMatrix_Setup_cblktosolvmtx(m);
  
  if (m->alloc == ONE) {
    DBMatrix_Setup_solvermatrix(&m->a[0], &m->a[0].symbmtx);
  }

#ifdef DEBUG_M
/*   if ((m->alloc != ONE) && (m->alloc != BLK)) */
/*     assert(DEBUG_bloknbr1 == symbmtx->bloknbr); */
/*   assert(DEBUG_bloknbr == symbmtx->bloknbr); */
#endif
}

void DBMatrix_Setup_cblktosolvmtx(DBMatrix* m) {
  int k0, k, i0, i;

  m->cblktosolvmtx = NULL;
  if (m->alloc == CBLK) {
    m->cblktosolvmtx = (SolverMatrix**)malloc(sizeof(SolverMatrix*)*m->cblknbr);

    k=0;
    for(i0=0,i=m->tlj; i<=m->brj; i0++, i++) 
      for(k0=0; k0<m->a[i0].symbmtx.cblknbr; k0++, k++) 
	m->cblktosolvmtx[k] = &m->a[i0];

  }

}

/***************************************************************************/

void DBMatrix_SetupV(DBMatrix *M, DBMatrix *Cpy) {
  assert(M->alloc != BLK);

  int i0, ilast;

  memcpy(Cpy, M, sizeof(DBMatrix));
  Cpy->virtual = COPY;

  if (M->alloc == ONE)
    ilast = 1;
  else if (M->alloc == BLK)
    ilast = M->cblknbr;
  else if (M->alloc == CBLK)
    ilast = M->brj - M->tlj + 1;
  else 
    ilast = M->bri - M->tli + 1;

  Cpy->a = (SolverMatrix*)malloc(sizeof(SolverMatrix)*ilast);
  memcpy(Cpy->a, M->a, sizeof(SolverMatrix)*ilast);

  for(i0=0; i0<ilast; i0++) {
    Cpy->a[i0].virtual = COPY;
  }

  DBMatrix_Setup_cblktosolvmtx(Cpy);

  /* todo : a revoir voir version 169 */
  Cpy->bloktab = (VSolverMatrix*)malloc(sizeof(VSolverMatrix) * M->bloknbr);
  memcpy(Cpy->bloktab, M->bloktab, sizeof(VSolverMatrix) * M->bloknbr);
/*   for(i=0; i<) */


  /**/
  {
    dim_t i;
    Cpy->ra = (VSolverMatrix **)malloc(sizeof(VSolverMatrix*)* (Cpy->bloknbr));
    for(i=0;i<Cpy->bloknbr;i++)
      Cpy->ra[i] = Cpy->bloktab + (M->ra[i] - M->bloktab);
    
    Cpy->ca = (VSolverMatrix **)malloc(sizeof(VSolverMatrix*)* (Cpy->bloknbr));
    for(i=0;i<Cpy->bloknbr;i++)
      Cpy->ca[i] = Cpy->bloktab + (M->ca[i] - M->bloktab);
    /**/
  }

  {
    dim_t i,j;
    int tl, br;
    INTL *ia; dim_t *ja;
    VSolverMatrix **a;
    SolverMatrix *solv;

    if ((Cpy->alloc == ONE) || (Cpy->alloc == CBLK) || (Cpy->alloc == BLK)) {
      tl = Cpy->tlj;
      br = Cpy->brj;
      ia = Cpy->cia;
      ja = Cpy->cja;
      a  = Cpy->ca;     /* TODO : change variable name 'a' bcse Cpy->a exist */
    } else {
      assert(Cpy->alloc == RBLK);
    
      tl = Cpy->tli;
      br = Cpy->bri;
      ia = Cpy->ria;
      ja = Cpy->rja;
      a  = Cpy->ra;
    }

    solv = &Cpy->a[0];

    for(i0=0,i=tl; i<=br; i0++, i++) {
      if (Cpy->alloc != ONE) {
	solv = &Cpy->a[i0];
      }
      for(j=ia[i];j<ia[i+1];j++) {
	a[j]->solvmtx = solv;
      }
    }
 
  }
  
}

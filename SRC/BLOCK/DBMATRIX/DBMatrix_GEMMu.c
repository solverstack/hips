/* @authors J. GAIDAMOUR */

#include <stdio.h>
#include <stdlib.h>
#include <string.h> /* memset */
#include <assert.h>

#include "block.h"

#include "base.h"

#define DEBUG Dprintf

#define MAX(x,y) (((x)<(y))?(y):(x))
#define MIN(x,y) (((x)>(y))?(y):(x))

/* TODO : stride */
/* TODO : simplification cas pattern sym */

/* TODO : faire le calcul sur L et sur U en mÃªme temps pour un k donnÃ©? */

struct DBMatrixPart_ {
  INTL *ia;
  dim_t *ja;
  VSolverMatrix **a;
};
typedef struct DBMatrixPart_  DBMatrixPart;

void DBMatrix_sGEMM2(int iB, COEF alpha,
		     DBMatrix* A, DBMatrixPart* Ap,
		     /* DBMatrix* B, DBMatrixPart* Bp, */
		     int nk, dim_t *jak, VSolverMatrix** rak,
		     DBMatrix* C, DBMatrixPart* Cp,
		     DBMatrix* D,
		     int *tabA, int *tabC,
		     COEF* E, COEF* F, COEF* W, dim_t tli,
		     flag_t Aalloc);

void DBMatrix_sGEMM(int i, COEF alpha, 
		    DBMatrix* A, INTL **Ap, /* TODO : faire un transpose Ã  la place */
		    /* DBMatrix* B, INTL **Bp, */
		    int nk, dim_t *jak, VSolverMatrix** rak,
		    DBMatrix* C, INTL **Cp,
		    DBMatrix* D,	 
		    int *tabA, int *tabC, 
		    COEF* E, COEF* F, COEF* W, dim_t tli, flag_t Aalloc) {
  
  DBMatrix_sGEMM2(i, alpha,
		  A, (DBMatrixPart*)Ap,
		  /* B, (DBMatrixPart*)Bp, */
		  nk, jak, rak,
		  C, (DBMatrixPart*)Cp,
		  D,
		  tabA, tabC, E, F, W, tli, Aalloc);
}

/* TODO : calcul sur la diag */
/* TODO : uniformiser les opÃ©rations TRSM GEMM Facto avec double one et double zero */
/* TODO : Phidal ou (PhidalL + PhidalU) ? */
/* TODO : calcul par block phidal pour par k ? */
/* taba et tabc symmÃ©trique */

void DBMatrix_GEMMu(UDBMatrix* CLU, COEF alpha, DBMatrix* A, DBMatrix* B, DBMatrix* DTMP, 
                    flag_t fill, UPhidalMatrix* PhidalLU, PhidalHID *BL) {
  /************************************************************************************/
  /* This function performs C = C + alpha.A.Bt.D          (C = L/U)                   */
  /************************************************************************************/
  int i0, iB;
 
  DBMatrix* CL = CLU->L;
  DBMatrix* CU = CLU->U;

  PhidalMatrix* PhidalL = PhidalLU->L;
  PhidalMatrix* PhidalU = PhidalLU->U;
  COEF *E, *F, *W;
    
  int *tabA, *tabC;
  
  /* Allocation des vecteurs temporaires */
  E = (COEF *)malloc(sizeof(COEF)*A->coefmax);
  assert(E != NULL);

  F = (COEF *)malloc(sizeof(COEF)*A->coefmax);
  assert(F != NULL);

  W = (COEF *)malloc(sizeof(COEF)*A->coefmax);
  assert(W != NULL);

  tabA = (int*)malloc(sizeof(int)*A->dim1);
  tabC = (int*)malloc(sizeof(int)*CL/*CU?*/->dim1);
  assert(tabA != NULL);
  assert(tabC != NULL);
  assert(A->dim1 = CL/*CU*/->dim1);

  assert(((A->alloc == ONE ) && (B->alloc == ONE )) ||
	 ((A->alloc == RBLK) && (B->alloc == CBLK)) ||
	 ((A->alloc == CBLK) && (B->alloc == RBLK)) );
/*   assert((D->alloc == ONE) || (D->alloc == CBLK)); */
  assert((CL->alloc == ONE) || (CL->alloc == CBLK));
  if (CU != NULL) /* ou == CL ? */
    assert((CU->alloc == ONE) || (CU->alloc == RBLK));
  if (fill == INGEMM) 
    assert((A->alloc == RBLK) && (B->alloc == CBLK) && (CL->alloc == CBLK) /* && CU*/); /* pas necessaire, a retirer */

  if (fill == INGEMM) /* tmp */
    assert(CL->alloc != BLK);

 int nk;
  dim_t *jak;
  VSolverMatrix** rak;
  {
    int ii;
    dim_t k;
    DBMatrix* L = A;

    ii = 0;
    for(k=L->tli;k<=L->bri;k++)
      if(L->ria[k+1] - L->ria[k]/* -1 */ > ii)
	ii = L->ria[k+1] - L->ria[k]/* -1 */;
    if(ii>0)
      {
/* 	struct DBMatrixPart_ { */
/* 	  INTL *ia; */
/* 	  dim_t *ja; */
/* 	  VSolverMatrix **a; */
/* 	}; */

/* 	printfv(5, "ii=%d\n", ii); */
	jak = (dim_t *)malloc(sizeof(dim_t)*ii);
	rak = (VSolverMatrix **)malloc(sizeof(VSolverMatrix*)*ii);
      }
    else
      {
	jak = NULL;
	rak = NULL;
      }
  }
   
  /* parcours des cblk phidal (de B et C) */
  for(i0=0, iB=B->tlj;iB<=B->brj;i0++, iB++) {

    if ((fill == INGEMM) && (CL->alloc == CBLK)) {
      SolverMatrix *solvCL = &CL->a[i0]; /* OU i0-cequ'il faut */
      
      DBMatrix_Symb2SolvMtx(solvCL);
      DBMatrix_AllocSymbmtx(i0, iB, CL, CBLK, CL->cia, CL->cja, CL->ca, PhidalL->cia, PhidalL->cja, PhidalL->ca, solvCL, "T", BL);
      
      if (CU != NULL) /* && (CU->alloc == CBLK) */ {
	SolverMatrix *solvCU = &CU->a[i0]; /* symbC = &solvC->symbmtx; */
	
	DBMatrix_Symb2SolvMtx(solvCU);
	DBMatrix_AllocSymbmtx(i0, iB, CU, RBLK, CU->ria, CU->rja, CU->ra, PhidalU->cia, PhidalU->cja, PhidalU->ca, solvCU, "N", BL);
      }
    }

    {
      int jj;
      int k = iB;
      DBMatrix* L = B;
      
      nk = 0;
      for(jj=L->cia[k];jj<L->cia[k+1]/* -1 */;jj++)
	{
	  jak[nk] = L->cja[jj];
	  rak[nk] = L->ca[jj];
	  nk++;
	}
    }

    DBMatrix_sGEMM(iB, alpha, A, &A->cia, nk, jak, rak, CL, &CL->cia, NULL/* D */,
		   tabA, tabC, E, F, W, A->tli, A->alloc);
    
    if (CU != NULL) {

      {
	int jj;
	int k = iB;
	DBMatrix* L = A;
	
	nk = 0;
	for(jj=L->ria[k];jj<L->ria[k+1]/* -1 */;jj++)
	  {
	    jak[nk] = L->rja[jj];
	    rak[nk] = L->ra[jj];
	    nk++;
	  }
      }
      
      DBMatrix_sGEMM(iB, alpha, B, &B->ria, nk, jak, rak, CU, &CU->ria, NULL/* D */,
      		     tabA, tabC, E, F, W, B->tlj, A->alloc);

    }

    if (fill == INGEMM) { /* todo : simplifier */
      if (A->alloc == RBLK)
	SolverMatrix_Clean(&A->a[iB-B->tlj]);
      
      if (CU != NULL)
	if (B->alloc == CBLK)
	  SolverMatrix_Clean(&B->a[iB-B->tlj]);
    } /* if fill == INGEMM */

  } /* main loop */
  
  if ((fill == INGEMM) && (A->alloc == RBLK)) {
    free(A->a);
    
    if (CU != NULL)
      free(B->a);
    
    A->a = NULL;
    B->a = NULL;
  }
  
  free(E);
  free(F);
  free(W);
  free(tabA);
  free(tabC);
 
}



/* TODO : faire une structure pour cela ! */
void DBMatrix_sGEMM2(int iB, COEF alpha,
		     DBMatrix* A, DBMatrixPart* Ap,
		     /* DBMatrix* B, DBMatrixPart* Bp, */
		     int nk, dim_t *jak, VSolverMatrix** rak,
		     DBMatrix* C, DBMatrixPart* Cp,
		     DBMatrix* D,
		     int *tabA, int *tabC,
		     COEF* E, COEF* F, COEF* W, dim_t tli, int Aalloc) {

  SymbolMatrix *SA;
  SymbolMatrix *SB;
  
  /*   SymbolMatrix *SD; */

  SymbolMatrix *SC;

  SolverMatrix *solvA, *solvB, *solvC;

  dim_t j;
  int iA,jA,jC, m;
  int k /*, kD */;
  int pSA, pSB;
  int qSA;

  int cdim, hdim, rdim, rdim2, mdim;
  int Srdim;
  blas_t strideA, strideA2, strideB/* , strideD */;
  int bloknum;
  int decalcol, decalrow;
  int facestride;

  COEF *ac, *bc, *cc, *wc, *fc/* , *dc */;
  COEF *Ec, *ec;

  /*   COEF alphadiag; */

  COEF zero = 0.0, one = 1.0;
  char *opA = "N"; /** Ne pas transposer A **/
  char *opB = "T"; /** Transpose B **/
  int UN = 1;

  int tabi;
  int ii;
  
  /* parcours des blk phidal (de B) */
  /* parcours des blk phidal (de B) */
  for(j=0; j<nk; j++) { 
    SB    = &rak[j]->symbmtx;
    solvB =  rak[j]->solvmtx;
    iA    =  jak[j];

/*       if (D->alloc != ONE) { */
/* 	solvD = &D->a[iA-D->tli]; */
/* 	kD=0; */
/* 	assert(0 == D->tli); */
/*       } else { kD=SB->cblktlj; } */

/*       SD = &D->ca[D->cia[iA]]->symbmtx; /\* Diagonal Matrix. first phidal block is diagonal block *\/ */

      if(SB->bloknbr ==0) continue;
            
      /***********************/
      /* Recherche des blocs */
      /***********************/
      tabi=0;
      jA=Ap->ia[iA];      
      for(jC=Cp->ia[iB];jC<Cp->ia[iB+1];jC++) {
	/* Recherche du block correspondant dans A (s'il existe) */
	while(jA<Ap->ia[iA+1]) { if (Ap->ja[jA] < Cp->ja[jC]) jA++; else goto test; }
	continue;
      test:
	if (Ap->ja[jA] != Cp->ja[jC]) continue;
	assert(Ap->ja[jA] == Cp->ja[jC]);
	
	tabC[tabi] = jC; 
	tabA[tabi] = jA; tabi++;
      }
          
      /* parcours des cblk (de SB) */
      for(k=0;k<SB->cblknbr;k++) {

	cdim = SB->ccblktab[k].lcolnum - SB->ccblktab[k].fcolnum +1; /* Largeur du bloc colonne */

	strideA = strideA2 = A->hdim[k+SB->facedecal]; /* TODO : a calculer pour V2 */
	if(A->alloc == ONE) {	
	  strideA2 = strideA;
	}

	strideB = SB->stride[k];
/* 	strideD = SD->stride[k]; */

	hdim = SB->hdim[k]; /* the number of columns of the matrix Bt */ /* todo : deplacer vers le bas */
	if (hdim ==0) continue;

	/**************************************************/
	/* ConcatÃ©nation des blocks de A en fonction de C */
	/**************************************************/
	rdim=0;
	Ec = E;
	
	for(ii=0;ii<tabi;ii++) {
	  jA = tabA[ii];
	  SA    = &Ap->a[jA]->symbmtx;
	  solvA = Ap->a[jA]->solvmtx; /* &A->a[Ap->ja[jA]-tli];  */
	  if(Aalloc == RBLK) {
	    strideA2 = SA->stride[k];
	  }
	  if(Aalloc == BLK) {
	    strideA2 = SA->stride[k];
	  }

	  Srdim = SA->hdim[k]; /* the number of rows of the matrix A */
	  if (Srdim == 0) continue;

	  /* copie du cblk de SA dans E */
	  pSA  = SA->bcblktab[k].fbloknum;
	  /* check(A, solvA, SA); */
	  ac = solvA->coeftab + solvA->bloktab[pSA].coefind;
	  ec = Ec;
	  for(m=0;m<cdim;m++) {
	    BLAS_COPY(Srdim, ac, UN, ec, UN);
	    ac += strideA2;
	    ec += strideA;
	  }
	  Ec+=Srdim; rdim+=Srdim;
	}
	/* ** */

	/***********************************************************/
	/* "Diviser" le bloc de SB par le bloc diagonal de D dans F*/
	/***********************************************************/
	pSB=SB->bcblktab[k].fbloknum;
	bc = solvB->coeftab + solvB->bloktab[pSB].coefind;
	/* 	dc = solvD->coeftab + solvD->bloktab[ SD->bcblktab[k].fbloknum ].coefind; */

	fc = F; /** Pointeur vers la m-ieme colonne dans le buffer F **/

/* 	assert(cdim*strideB <= Fsize); */
	for(m=0;m<cdim;m++) {
	  BLAS_COPY(hdim, bc, UN, fc, UN);     /** On copie les bloc extra diagonaux dans F **/ /* hdim = the number of rows of B ! */
	  /* BLAS_SCAL(hdim, alphadiag,  fc, UN); /\** On divise la m-ieme colonne par le terme diagonal **\/ */
	  
	  bc += strideB;
	  fc += strideB;
	}
	/*****************************************************/
	/* -----Affectation dans C                                */
	/*****************************************************/
	
	for(pSB=SB->bcblktab[k].fbloknum; pSB<=SB->bcblktab[k].lbloknum; pSB++) {
	  fc = F + solvB->bloktab[pSB].coefind - solvB->bloktab[ SB->bcblktab[k].fbloknum ].coefind;

	  mdim = SB->bloktab[pSB].lrownum - SB->bloktab[pSB].frownum+1; /*** Largueur de la zone modifiÃÂ©e 
										   == hauteur du bloc p dans le bloc colonne k **/
	  /*****************************************************/
	  /* GEMM                                              */
	  /*****************************************************/
/* 	  printfv(5, "GEMM : rdim %d, mdim %d, cdim %d, 1.0, ec, strideA %d, fc, strideB %d, 0.0, W, strideA %d\n", */
/* 		 rdim, mdim, cdim, strideA, strideB, strideA); */
	  if(strideA ==0) continue;
/* 	  printfv(5, "rdim %d mdim %d cdim %d A %d B %d\n", rdim, mdim, cdim, strideA, strideB); */
	  BLAS_GEMM(opA, opB, rdim, mdim, cdim, one, E, strideA, fc, strideB, zero, W, strideA);

	  SC = &Cp->a[Cp->ia[iB]]->symbmtx;
	  solvC = Cp->a[Cp->ia[iB]]->solvmtx;

	  /* *****************************/
	  /** Nombre de colonne "ÃÂ  gauche" de la zone  modifiÃÂ©e dans le bloc colonne facecblknum **/	  
	  decalcol = SB->bloktab[pSB].frownum - SC->ccblktab[0].fcolnum; 

	  /* *****************************/
	  /* parcours des blocs de la cblk nÂ°k de A */
	  rdim2=0;
	  
	  for(ii=0;ii<tabi;ii++) {
	    jA = tabA[ii];
	    jC = tabC[ii];
	    
	    assert(Ap->ja[jA] == Cp->ja[jC]);
	    
	    SC = &Cp->a[jC]->symbmtx; 
	    solvC = Cp->a[jC]->solvmtx; /* todo verif si utile */
	    SA = &Ap->a[jA]->symbmtx; 
	    solvA = Ap->a[jA]->solvmtx;
	   
	    facestride  = SC->stride[0];   /** Stride du cblk en face du bloc extra-diagonal p **/

	    for(qSA=SA->bcblktab[k].fbloknum;qSA<=SA->bcblktab[k].lbloknum;qSA++) {
	      /* *****************************/
	      
	      bloknum = SC->bcblktab[0].fbloknum;     /** Indice du bloc **/
	      
	      /** Calcul du pointeur de debut du bloc correspondant ÃÂ  A(q,k) dans W **/
	      wc = W + rdim2 + solvA->bloktab[qSA].coefind - solvA->bloktab[ SA->bcblktab[k].fbloknum/*todo : rdim*/ ].coefind; /* +  */

	      /** Calcul du pointeur de debut de la zone modifiÃÂ©e dans le bloc C(bloknum, facebloknum) */
	      /* check(C, solvC, SC); */
	      decalrow = SA->bloktab[qSA].frownum - SC->bloktab[bloknum].frownum; /* TODO SC ? C ? */
	      cc = solvC->coeftab + solvC->bloktab[bloknum].coefind + decalcol*facestride + decalrow;
	      
	      hdim = SA->bloktab[qSA].lrownum - SA->bloktab[qSA].frownum+1; /** Hauteur du bloc colonne qSA **/
	      
	      /*** Update de la contribution ***/
	      for(m=0; m < mdim; m++)
		{
		  BLAS_AXPY(hdim, alpha, wc, UN, cc, UN);
		  wc += strideA;
		  cc += facestride;
		}
	    }
	    
	    rdim2 += SA->hdim[k]; /* the number of rows of the matrix A */ /* TODO : on peut aussi faire avancer wc au fur et a mesure */
	  }

	  /** Mettre ÃÂ  jour la hauteur des bloc extra diagonaux qui restent **/
	  /* rdim -= SB->bloktab[pSB].lrownum - SB->bloktab[pSB].frownum+1; /\*a faire q=p*\/ */
	}
      }

    }

}

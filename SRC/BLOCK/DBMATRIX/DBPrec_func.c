/* @authors J. GAIDAMOUR */

#include <stdio.h>
#include <stdlib.h>
#include <string.h> /* strcmp */

#define DEBUG_M_PRINT

#include "block.h"
#include "prec.h"

/* TODO : en dropping S, afficher quand meme sans dropping pour comparaison */
/* TODO : mem->SL dÃ©pend de la strat 1 / 2 et LOC 0 ALL : si on veut tout afficher, faut 2 variables ? */

/* RQ   : EN LL (old) dropping S : juste le after qui change par rapport au sans dropping. */
/*        On pourrait afficher surcout de conversion de SL DB -> en PH (1 colonne) */
/*        On pourrait prendre en compte que S moins important si conversion pendant le GEMM */
/* mais actuelement, la conversion est mal codÃ© */

/* RQ   : EN RL dropping S : */
/*        On pourrait afficher surcout de conversion de SL DB -> en PH (1 colonne) */
/*        Sinon, ok, le cout d'avoir SL stockÃ© en DB pris en compte dans DBPrec_NNZ_RL en fait */

long DBMatrix_NNZGemmPic(flag_t symmetric, DBMatrix* E, DBMatrix* S);

#ifndef PARALLEL /* compil only 1x */
void DBPrecMem_Init(DBPrecMem* mem) {
  mem->A = -1;
/*   mem->L_NOREC = -1; */
  mem->L = -1;
  mem->E = -1;
  mem->E_DB = -1;
  mem->Emax = -1;
  mem->C = -1;
  mem->SL = -1;
  mem->LS = -1;
  mem->phidalS = -1;
  mem->ll = -1;
  mem->dropE = -1;
  mem->dropEAfter = -1;
}

void DBPrecMem_SetA(PhidalMatrix* m, DBPrecMem *mem) {
  mem->A      = PhidalMatrix_NNZ(m);
}

void DBPrecMem_SetC(PhidalMatrix* m, DBPrecMem *mem) {
  mem->C      = PhidalMatrix_NNZ(m);
}

void DBPrecMem_SetE(PhidalMatrix* m, DBPrecMem *mem) {
  mem->E      = PhidalMatrix_NNZ(m);
}

/* void DBPrecMem_SetL_NOREC(PhidalMatrix* m, DBPrecMem *mem) { */
/*   mem->L_NOREC = PhidalMatrix_NNZ(m); */
/* } */
#endif

/* ************************************** */
/* LEV0 */
void CMD(_DBPrecMem,Set0)(_DBDistrPrec *P, DBPrecMem *mem, flag_t dropE) {

  mem->L      = DBPrecPart_NNZ("L", P->symmetric, S(PREC_L_BLOCK(P)));

  if (P->forwardlev > 0) {
    if (dropE == 0) { 
      mem->E_DB = DBPrecPart_NNZ("N", P->symmetric, S(PREC_EDB(P)));

      mem->SL   = DBPrecPart_NNZ("L", P->symmetric, S(PREC_SL_BLOCK(P)));
      
      /* LL = vers. historique, alloc de S au fur et Ã  mesure */
      if (S(PREC_EDB(P))->alloc == RBLK && S(PREC_SL_BLOCK(P))->alloc == CBLK) { /* TODO : else : calculable quand mÃªme */
        mem->ll = DBMatrix_NNZGemmPic(P->symmetric, S(PREC_EDB(P)),/*S*/S(PREC_SL_BLOCK(P)));
      }
    }
    
    mem->Emax = calc_Emax(S(PREC_EDB(P))); /* meme en DROP_E==1 */
    if (P->symmetric == 0)
      mem->Emax *= 2;
    
  }   
  
}

void CMD(_DBPrecMem,SetPhidalS)(_DBDistrPrec *P, DBPrecMem *mem) {
  mem->phidalS = PhidalMatrix_NNZ(S(P->phidalS));
}

void CMD(_DBPrecMem,SetDropE)(_PhidalDistrPrec *P, DBPrecMem *mem) {
  mem->dropE = PhidalMatrix_NNZ(S(PREC_E(P)));
  if (P->symmetric == 0) {
    mem->dropE += PhidalMatrix_NNZ(S(PREC_F(P)));
  }
}

/* TODO : en dropE, on a avant, apres, pas pendant ? */
void CMD(_DBPrecMem,SetDropEAfter)(_PhidalDistrPrec *P, DBPrecMem *mem) {
  assert(P->schur_method == 2); /* S pas comptÃ© encore */
  
  mem->dropEAfter = PhidalMatrix_NNZ(S(PREC_L_SCAL(P)));
  
  if(P->symmetric == 0)
    mem->dropEAfter += PhidalMatrix_NNZ(S(PREC_U_SCAL(P)));
  else
    mem->dropEAfter += (long)S(PREC_L_SCAL(P))->dim1;
}

/* LEV1 */
void CMD(_DBPrecMem,Set1)(_DBDistrPrec *P, DBPrecMem *mem) {
  if (P->nextprec != NULL) {
    assert(P->phidalPrec == NULL);
    mem->LS = CMD(_DBDistrPrec,NNZ)(P->nextprec);
  } else {
    assert(P->phidalPrec != NULL);
    mem->LS = CMD(_PhidalDistrPrec,NNZ)(P->phidalPrec);
  }
}

#define printNNZ(msg, func) \
{                                                                                  \
 long tmp = (func);                                                                \
 if (tmp != -1)                                                                    \
   PRINTALL_MSG(msg"%ld \tPic: %ld \tRatio: %g\n", tmp, MAX(tmp, after), (REAL)MAX(tmp, after)/(REAL)mem->A)   \
 else										   \
   ;/* printfv(5, msg"%ld \tPic: %ld \tRatio: %g\n", (long)-1, (long)-1, (REAL)-1); */                          \
}

#define printMem(msg, mem) if ((mem) != -1) PRINTALL_MSG(msg, (mem))
       
void CMD(_DBPrecMem,print)(_DBDistrPrec* P, DBPrecMem *mem, _PhidalDistrHID* DBL, int verbose) {
  long before, after;

  if (verbose < 5) return;

  printMem("Mem : A       : %ld \n", mem->A);
  printMem("Mem : L       : %ld \n", mem->L);
/*   printMem("Mem : L_NOREC : %ld \n", mem->L_NOREC); */

  if (P->forwardlev > 0) {
/*     if (S(P->schur_method) != 1)     /\* et pas de dropping *\/ { */
/*       printfv(5, "%ld %ld\n", mem->SL, mem->LS); */
/*       assert(mem->SL == mem->LS); */
/*     } */

    printMem("Matrix : C       : %ld \n", mem->C);
    printMem("Matrix : E       : %ld \n", mem->E);

    printMem("Matrix : E_DB     : %ld \n", mem->E_DB);
    printMem("Matrix : E_max    : %ld \n", mem->Emax);
    printMem("Matrix : E(DropE) : %ld \n", mem->dropE);
    printMem("Matrix : S        : %ld \n", mem->SL);     
    printMem("Matrix : phidalS  : %ld \n", mem->phidalS); /* TODO : jamais utilisÃ© ds les calculs */
    printMem("Matrix : LS       : %ld \n", mem->LS);
    printMem("Matrix : LS(DropE): %ld \n", mem->dropEAfter);

    before = DBPrec_NNZ_Before(mem);
    after  = DBPrec_NNZ_After(P->schur_method, mem);

    if (DBPrec_NNZ_LL(mem) != -1)
      assert(before <= DBPrec_NNZ_LL(mem)); /* Donc pas besoin de le compter */
    assert(before <= DBPrec_NNZ_In(mem));

    printMem("Prec : before GEMM : LL       : %ld\n", before);
    printMem("Prec : after GEMM  : LL/RL    : %ld\n", after); /* dÃ©pend M0,1 ou 2 */

    printNNZ("Prec : in GEMM : basic LL/RL  : ", DBPrec_NNZ_In(mem));
    printNNZ("Prec : in GEMM : optim LL     : ", DBPrec_NNZ_LL(mem));
    printNNZ("Prec : in GEMM : optim RL     : ", DBPrec_NNZ_RL(mem));

    /* TODO : afficher le Pic aprÃ¨s en DROPE */
    printMem("Prec : in GEMM : opt RL dropE : %ld\n", DBPrec_NNZ_RL_DROPE(mem));
    printMem("Prec : aft GEMM: opt RL dropE : %ld\n", DBPrec_NNZ_RL_DROPEAfter(mem));


  }

  /* todo : dropE */
}

#ifdef PARALLEL

#define reinit(arg) if ((arg) <0) (arg)= -1;
void DBDistrPrecMem_reduce(DBDistrPrec* P, DBPrecMem *mem, DBPrecMem *memsum, MPI_Comm comm) {
  
  MPI_Reduce(mem, memsum,sizeof(DBPrecMem)/sizeof(long), MPI_LONG, MPI_SUM, 0, comm);
 
  reinit(memsum->A);
  reinit(memsum->L);
  reinit(memsum->E_DB);
  reinit(memsum->Emax);
  reinit(memsum->SL);
  reinit(memsum->LS);
  reinit(memsum->phidalS);
  reinit(memsum->ll);
  reinit(memsum->dropE);
  reinit(memsum->dropEAfter);
}

void DBDistrPrecMem_reduceMAX(DBDistrPrec* P, DBPrecMem *mem, DBPrecMem *memsum, MPI_Comm comm) {
  
  MPI_Reduce(mem, memsum,sizeof(DBPrecMem)/sizeof(long), MPI_LONG, MPI_MAX, 0, comm);
 
}


#endif


#ifndef PARALLEL /* compil only 1x */

/* TODO : a vÃ©rifier UNSYM */

long DBMatrix_NNZ(char* TYPE, DBMatrix* m);

long DBPrecPart_NNZ(char* TYPE, flag_t symmetric, DBMatrix* m) { /* TODO : arg symmetric inutile, sauf si on le transforme */
  long nnz=0;

  assert(symmetric == m->symmetric);

  nnz = DBMatrix_NNZ(TYPE, m);

  if (symmetric == 0)
      nnz *= 2;
  if (strcmp(TYPE, "L") == 0) /* /!\ must be done after nnz*=2 */
    nnz += (long)m->dim1;

  return nnz;
}

long DBM_NNZ(char* TYPE, DBMatrix* m) {
  return DBPrecPart_NNZ(TYPE, m->symmetric, m);
}


/* void DBPrec_Info(DBPrec *p) */
/* { */
/*   long nnz; */

/*   fprintfv(5, stdout, "-------Level %d NNZ ----------------\n", p->levelnum); */

/*   nnz = DBMatrix_NNZ("L", p->L); */
/*   fprintfv(5, stdout, "L(%d) : %ld \n", p->levelnum, nnz); */
/*   if(p->symmetric == 0) */
/*     fprintfv(5, stdout, "U(%d) : %ld \n", p->levelnum, nnz+(long)p->L->dim1); */
/*   else */
/*     fprintfv(5, stdout, "D(%d) : %ld \n", p->levelnum, (long)p->L->dim1); */
/*   if(p->forwardlev > 0) */
/*     { */
/*       fprintfv(5, stdout, "E(%d) : %ld \n", p->levelnum, PhidalMatrix_NNZ(p->E)); */
/*       if(p->symmetric == 0) */
/* 	fprintfv(5, stdout, "F(%d) : %ld \n", p->levelnum, PhidalMatrix_NNZ(p->F)); */

/*       if(p->schur_method == 1) { */
/* 	if (p->SL != NULL) { /\* equivalent (option->droptol1 == 0) (& -1 hack) *\/ */
/* 	  fprintfv(5, stdout, "S(%d) : %ld \n",  p->levelnum, DBPrecPart_NNZ("L", p->symmetric, p->SL)); */
/* 	} else { */
/* 	  fprintfv(5, stdout, "S(%d) : %ld \n",  p->levelnum, PhidalMatrix_NNZ(p->phidalS)); */
/* 	} */
/*       } */

/*       if(p->schur_method == 2)  */
/* 	fprintfv(5, stdout, "C(%d) : %ld \n",  p->levelnum, PhidalMatrix_NNZ(p->B)); */
      
/*       if (p->nextprec != NULL) { */
/* 	DBPrec_Info(p->nextprec); */
/*       } else { */
/* 	PhidalPrec_Info(p->phidalPrec); */
/*       } */
/*     } */
/*   fprintfv(5, stdout, "------------------------------------\n"); */
/* } */

#endif

/* Preconditionner size (in nnz) after his computation */
long CMD(_DBDistrPrec,NNZ)(_DBDistrPrec *P)
{
  long nnz;

  assert(P != NULL);

  /** L / U **/
  nnz = DBPrecPart_NNZ("L", P->symmetric, S(PREC_L_BLOCK(P)));

  if(P->forwardlev > 0)
    {

      /** L_S / U_S **/
      if (P->nextprec != NULL) {
        nnz += CMD(_DBDistrPrec,NNZ)(P->nextprec);
      } else {
        nnz += CMD(_PhidalDistrPrec,NNZ)(P->phidalPrec);
      }

      /** S (exact Shur complement) for method 1 **/
      if(P->schur_method == 1)
        {
          if (PREC_SL_BLOCK(P) != NULL) {
            nnz += DBPrecPart_NNZ("L", P->symmetric, S(PREC_SL_BLOCK(P)));
          } else {
            nnz += PhidalMatrix_NNZ(S(P->phidalS));
          }
        }
    }

  return nnz;
}

#ifdef PARALLEL
/* Preconditionner size (in nnz) after his computation */
long DBDistrPrec_NNZ_All(DBDistrPrec *p, MPI_Comm mpicom)
{
  long nnz;
  long gnnz;
  
  nnz = DBDistrPrec_NNZ(p);

  MPI_Allreduce(&nnz, &gnnz, 1, MPI_LONG, MPI_SUM, mpicom);
  return gnnz;
}
#endif

#ifndef PARALLEL /* compil only 1x */
/************************************************************************************/
/************************************************************************************/
/************************************************************************************/

/* TODO : a factoriser avec DBMatrix_NNZ */
/* Ne marche pas pour alloc == ONE*/
/* Utiliser coefnbr ? */
long CBLKDBMatrix_NNZ(char* TYPE, flag_t symmetric, DBMatrix* m, int i0) {
  long nnz = m->a[i0].coefnbr;
  long dim=0;

  if (strcmp(TYPE, "L") == 0) {
    int i = i0 + m->tlj; 
    int j = m->cia[i];
    dim_t k, c;
    SymbolMatrix* symb = &m->ca[j]->symbmtx;

    assert(m->cja[j] == i); /* diagonal phidal block */

    for(k=0; k<symb->cblknbr; k++) {
      c = symb->ccblktab[k].lcolnum - symb->ccblktab[k].fcolnum +1;
      nnz -= (c*(c+1))/2 /* ordre important */;
      dim += c;
    }
  }

  /* Au mÃªme niveau que DBPrecPart : */
  if (symmetric == 0)
    nnz *= 2;
  if (strcmp(TYPE, "L") == 0) /* /!\ must be done after nnz*=2*/
    nnz += dim;

  return nnz;
}

long DBMatrix_NNZGemmPic(flag_t symmetric, DBMatrix* E, DBMatrix* S) {
  dim_t i;
  long nnzcurr, nnzmax;

  assert(E->alloc == RBLK); /* TODO : else : calculable quand mÃªme */
  assert(S->alloc == CBLK);

  nnzmax = nnzcurr = DBPrecPart_NNZ("N", symmetric, E);

  /* 'virtual GEMM' */
  for(i=0; i <= E->bri - E->tli; i++) {
    nnzcurr += CBLKDBMatrix_NNZ("L", symmetric, S, i);

    nnzmax = MAX(nnzmax, nnzcurr);

    nnzcurr -= CBLKDBMatrix_NNZ("N", symmetric, E, i);
  }

  assert(nnzcurr == DBPrecPart_NNZ("L", symmetric, S));

  return nnzmax;
}

/************************************************************************************/
/************************************************************************************/
/************************************************************************************/

long DBMatrix_NNZ(char* TYPE, DBMatrix* m) {
  int i, ilast, j, k;
  SymbolMatrix* symb;
  dim_t c;
  int nnz=0;
#ifdef DEBUG_M
  int dim=0;
#endif

  if (m->alloc != BLK) {
    
    if (m->alloc == ONE)
      ilast = 1;
    else if (m->alloc == CBLK)
      ilast = m->brj - m->tlj + 1;
    else 
      ilast = m->bri - m->tli + 1;
    
    for (i=0; i<ilast; i++) {
      nnz += m->a[i].coefnbr;
    }
    
  } else {
    for(i=m->tlj; i<=m->brj; i++) {
      for(j=m->cia[i];j<m->cia[i+1];j++) {
	nnz += m->ca[j]->solvmtx->coefnbr;
      }
    }
  }
  
  if ((strcmp(TYPE, "L") == 0))
    {
      /* Remove upper part of diagonal block (+ diagonal) */
      for(i=m->tlj; i<=m->brj; i++) {
	j=m->cia[i];
	assert(m->cja[j] == i); /* diagonal phidal block */
	symb = &m->ca[j]->symbmtx;
	
	for(k=0; k<symb->cblknbr; k++) {
	  c = symb->ccblktab[k].lcolnum - symb->ccblktab[k].fcolnum +1;
	  nnz -= (c*(c+1))/2 /* ordre important */;
#ifdef DEBUG_M
	  dim += c;
#endif

	}
      }
#ifdef DEBUG_M
  assert(dim == m->dim1);
#endif
    }

  return nnz;
}

/************************************************************************************/
/************************************************************************************/
/************************************************************************************/

/* iscaletab utile ? */
void DBPrec_Unscale(REAL *scaletab, REAL *iscaletab, DBPrec *P, PhidalHID *BL)
{

  /*****************************************************/
  /*  A = Dr^-1.A'.Dr^-1                               */
  /*  A = Dr^-1.L'.D'.L't.Dr^-1                        */
  /*  A = Dr^-1.L'.Dr . Dr^-1.D'.Dr^-1  . Dr.L't.Dr^-1 */
  /*  A =     L       .      D          . Lt           */
  /*****************************************************/

#ifdef DEBUG_M
  assert(P->symmetric == 1);
#endif

  DBMatrix_RowMult(iscaletab, PREC_L_BLOCK(P), BL);  
  DBMatrix_ColMult(scaletab, PREC_L_BLOCK(P), BL);
  
  DBMatrix_DiagMult(iscaletab, PREC_L_BLOCK(P), BL); /* iscaletab for DB, scaletab for Phidal */
  
  if(P->forwardlev > 0)
    {
      
      if (P->nextprec != NULL) {
        DBPrec_Unscale(scaletab + PREC_L_BLOCK(P)->dim1, iscaletab + PREC_L_BLOCK(P)->dim1, P->nextprec, BL);
      } else {
        PhidalPrec_SymmetricUnscale(scaletab + PREC_L_BLOCK(P)->dim1, iscaletab + PREC_L_BLOCK(P)->dim1, P->phidalPrec, BL);
      }

      /* Si plus de deux niveaux*/
      /*       if(PREC_E(P)->virtual == 0) */
      /*        { */
      /*          PhidalMatrix_RowMult(iscaletab + PREC_L_BLOCK(P)->dim1, PREC_E(P), BL); */
      /*          PhidalMatrix_ColMult(iscaletab, PREC_E(P), BL); */
      /*        } */
 
      if(P->schur_method == 1)
        {
          if (PREC_SL_BLOCK(P) != NULL) { /* equivalent (option->droptol1 == 0) (& -1 hack) */
            DBMatrix_RowMult(iscaletab + PREC_L_BLOCK(P)->dim1, PREC_SL_BLOCK(P), BL);  
            DBMatrix_ColMult(iscaletab + PREC_L_BLOCK(P)->dim1, PREC_SL_BLOCK(P), BL);
          } else {
            PhidalMatrix_RowMult2(iscaletab + PREC_L_BLOCK(P)->dim1, P->phidalS, BL);
            PhidalMatrix_ColMult2(iscaletab + PREC_L_BLOCK(P)->dim1, P->phidalS, BL);
          }


	}

      /* Si plus de deux niveaux*/
      /*       if(P->schur_method == 2 && P->levelnum > 0) */
      /* 	{ */
      /* #ifdef DEBUG_M */
      /*          assert(P->B->virtual == 0); */
      /* #endif */
      /*          PhidalMatrix_RowMult(iscaletab + PREC_L_BLOCK(P)->dim1, P->B, BL); */
      /*          PhidalMatrix_ColMult(iscaletab + PREC_L_BLOCK(P)->dim1, P->B, BL); */
      /*        } */
      
    }
 
}




void DBPrec_Unscale_Unsym(REAL *scalerowtab, REAL *iscalerowtab, REAL *scalecoltab, REAL *iscalecoltab, DBPrec *P, PhidalHID *BL)
{

  /*****************************************************/
  /*  A = Dr^-1.A'.Dc^-1                               */
  /*  A = Dr^-1.L'.U'.Dc^-1                            */
  /*  A = Dr^-1.L'.Dr   .   Dr^-1.U'.Dc^-1             */
  /*  A =     L       .      U                         */
  /*****************************************************/

  COEF* D;

  /* ? */
  /*   printfv(5, ">> %e\n", PREC_L_BLOCK(P)->a[0].coeftab[0]); */
  /*   printfv(5, ">> %e\n", PREC_U_BLOCK(P)->a[0].coeftab[0]); */
  /* ? */

  DBMatrix_RowMult(iscalerowtab, PREC_L_BLOCK(P), BL);
  DBMatrix_ColMult(scalerowtab,  PREC_L_BLOCK(P), BL);
  
  DBMatrix_Transpose(PREC_U_BLOCK(P));
  DBMatrix_RowMult(iscalecoltab, PREC_U_BLOCK(P), BL); /* /!\ doit Ãªtre inversÃ© par rapport Ã  Phidal (Transpose) */
  DBMatrix_ColMult(iscalerowtab, PREC_U_BLOCK(P), BL); /* /!\ doit Ãªtre inversÃ© par rapport Ã  Phidal (Transpose) */
  DBMatrix_Transpose(PREC_U_BLOCK(P));

  /* ? */
  D = (COEF *)malloc(sizeof(COEF)*PREC_L_BLOCK(P)->dim1);
  DBMatrix_Transpose(PREC_U_BLOCK(P));
  DBMatrix_GetDiag(PREC_U_BLOCK(P), D);
  DBMatrix_Transpose(PREC_U_BLOCK(P));
  DBMatrix_SetDiag(PREC_L_BLOCK(P), D);
  free(D);
  /*   printfv(5, ">> %e\n", PREC_L_BLOCK(P)->a[0].coeftab[0]); */
  /*   printfv(5, ">> %e\n", PREC_U_BLOCK(P)->a[0].coeftab[0]); */
  /* ? */

  if(P->forwardlev > 0)
    {
      
      if (P->nextprec != NULL) {
        DBPrec_Unscale_Unsym(scalerowtab + PREC_L_BLOCK(P)->dim1, iscalerowtab + PREC_L_BLOCK(P)->dim1, 
                                  scalecoltab+PREC_U_BLOCK(P)->dim1, iscalecoltab+PREC_U_BLOCK(P)->dim1, P->nextprec, BL);
      } else {
        PhidalPrec_UnsymmetricUnscale(scalerowtab + PREC_L_BLOCK(P)->dim1, iscalerowtab + PREC_L_BLOCK(P)->dim1, 
                                      scalecoltab+PREC_U_BLOCK(P)->dim1, iscalecoltab+PREC_U_BLOCK(P)->dim1, P->phidalPrec, BL);
      }
      
      /* if(PREC_E(P)->virtual == 0) */
      /*        { */
      /*          PhidalMatrix_RowMult(iscalerowtab + PREC_L_BLOCK(P)->dim1, PREC_E(P), BL); */
      /*          PhidalMatrix_ColMult(iscalecoltab, PREC_E(P), BL); */
      /*        }  */
      
      /*       if(PREC_F(P)->virtual == 0) */
      /*        { */
      /*          PhidalMatrix_RowMult(iscalerowtab, PREC_F(P), BL); */
      /*          PhidalMatrix_ColMult(iscalecoltab + PREC_L_BLOCK(P)->dim1, PREC_F(P), BL); */
      /*        }  */
 

      if(P->schur_method == 1)
        {
          if (PREC_SL_BLOCK(P) != NULL) { /* equivalent (option->droptol1 == 0) (& -1 hack) */
            DBMatrix_RowMult(iscalerowtab + PREC_L_BLOCK(P)->dim1, PREC_SL_BLOCK(P), BL);
            DBMatrix_ColMult(iscalecoltab + PREC_L_BLOCK(P)->dim1, PREC_SL_BLOCK(P), BL);
            DBMatrix_RowMult(iscalecoltab + PREC_U_BLOCK(P)->dim1, PREC_SU_BLOCK(P), BL);
            DBMatrix_ColMult(iscalerowtab + PREC_U_BLOCK(P)->dim1, PREC_SU_BLOCK(P), BL);
          } else {
            PhidalMatrix_RowMult2(iscalerowtab + PREC_L_BLOCK(P)->dim1, P->phidalS, BL);
            PhidalMatrix_ColMult2(iscalecoltab + PREC_L_BLOCK(P)->dim1, P->phidalS, BL);
          }
        }

      /*  if(P->schur_method == 2 && P->levelnum > 0) */
      /* 	{ */
      /* #ifdef DEBUG_M */
      /*          assert(P->B->virtual == 0); */
      /* #endif */
      /*          PhidalMatrix_RowMult(iscalerowtab + PREC_L_BLOCK(P)->dim1, P->B, BL); */
      /*          PhidalMatrix_ColMult(iscalecoltab + PREC_L_BLOCK(P)->dim1, P->B, BL); */
      /*        } */

    }

}
#endif

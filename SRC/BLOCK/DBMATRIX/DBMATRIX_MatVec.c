/* @authors J. GAIDAMOUR */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>

#include "solver.h"
#include "block.h"

/*Version 1 : ne marche pas avec une alloc par morceaux*/
/* TODO : fonction bmatvec etc a adapter avec ->tli etc. */
/* U et L faux (voir 1er bloc) ? */
/*reporter les changements sur bmatvec2 */

void DBMATRIX_MatVec(DBMatrix *L, DBMatrix *U, PhidalHID *BL, COEF *x, COEF *y)
{
  /*****************************************************************/
  /* This function does y = M.x                                    */
  /*****************************************************************/

  /*---------------------------------------------------------------------
    | This function does the matrix vector product y = A x.
    |----------------------------------------------------------------------
    | on entry:
    | solvmtx = the matrix (in SolverMatrix form)
    | x       = a vector
    |
    | on return
    | y     = the product A * x
    | TODO : expliquer UPLO
    |--------------------------------------------------------------------*/
  int i,k,kglobal,p,p2;
  int ilast;
  dim_t tli = L->a[0].symbmtx.tli;

  COEF* xptr;
  COEF* yptr;
  COEF* solvmtxptr;
  int height, width, stride;

  SolverMatrix *solvL, *solvU;
  SymbolMatrix* symbmtx;

  /* vecteur tmp */
  COEF* tmp = (COEF*)malloc(sizeof(COEF)*L->nodenbr);
  int jtmp, size;

  COEF zero=0.0, one=1.0;
  char *uploL = "L";
  char *transN = "N", *transT = "T";
  int UN = 1;

  assert((L->alloc == ONE) || (L->alloc == CBLK));
  /*   assert((U->alloc == ONE) || (U->alloc == CBLK)); todo : unsym */

  /* TODO dim1 / dim 2 */
  /*   printfv(5, "symbmtx->nodenbr = %d\n",symbmtx->nodenbr); */
  bzero(y, sizeof(COEF)*L->nodenbr);

  if (L->alloc == ONE)
    ilast = 1;
  else
    ilast = L->brj - L->tlj + 1;

  /* parcours par blocs colonnes */
  kglobal = 0;
  for(i=0;i<ilast;i++) {
    solvL = &L->a[i];
    solvU = &U->a[i];
    symbmtx = &solvL->symbmtx;
      
    /*     printfv(5, "i=%d ilast, kmax-1 : %d\n", i, ilast, symbmtx->cblknbr); sur S, cblknbr=1 tj  */
    for(k=0;k<symbmtx->cblknbr;k++, kglobal++) {
      width  = symbmtx->ccblktab[k].lcolnum - symbmtx->ccblktab[k].fcolnum +1;
      height = symbmtx->hdim[k]; /** Hauteur de la surface compactÃ©e des bloc extra-diagonaux **/
      stride = symbmtx->stride[k]; assert(height == stride);
      p    = symbmtx->bcblktab[k].fbloknum; /* triangular block */
      xptr = x + symbmtx->ccblktab[k].fcolnum - tli;
      yptr = y + symbmtx->ccblktab[k].fcolnum - tli;

      /* assert(L->symbmtx.cblktab[kglobal].fcolnum == symbmtx->cblktab[k].fcolnum); */

      /*******************************************************************************/
      if (L == U) {
	/* bloc triangulaire */
	
	/* y := alpha*A*x + beta*y */
	
	solvmtxptr = solvL->coeftab + solvL->bloktab[p].coefind;

	BLAS_SYMV(uploL, width, one /*alpha*/,
	      solvmtxptr, stride, xptr, UN,
	      one /*beta*/, yptr, UN);
	
/* 	if ((i == ilast-1) && (k == symbmtx->cblknbr-1)) */
/* 	  continue;  /\** Pas de bloc extradiagonal le calcul est terminer **\/ */
	if (p == symbmtx->bcblktab[k].lbloknum)
	  continue; /* TODO : a reporter sur desc2.c*/
	
	/* skip diag block ... */
	height -= width; /** Hauteur de la surface compactÃ©e des bloc extra-diagonaux **/
	p++;

      }

      /*******************************************************************************/

      solvmtxptr = solvU->coeftab + solvU->bloktab[p].coefind;

      /* tmp := x */
      for(p2=p, jtmp=0; p2<=symbmtx->bcblktab[k].lbloknum; p2++) {
	size = symbmtx->bloktab[p2].lrownum - symbmtx->bloktab[p2].frownum +1;
	BLAS_COPY(size, x + symbmtx->bloktab[p2].frownum - tli, UN, tmp + jtmp, UN);
	jtmp += size;
      }
      
      /* calcul ligne */
      BLAS_GEMV(transT, height, width, one /*coef alpha*/,
	    solvmtxptr, stride,
	    tmp, UN,       one /*coef beta*/,
	    yptr, UN);
    
      /*******************************************************************************/

      if (L != U) { /* fait un peu doublon ... */
	if (p == symbmtx->bcblktab[k].lbloknum) continue;

	/* skip diag block ... */
	height -= width; /** Hauteur de la surface compactÃ©e des bloc extra-diagonaux **/
	p++;
      }

      solvmtxptr = solvL->coeftab + solvL->bloktab[p].coefind;
      
      /* calcul colonne */
      BLAS_GEMV(transN, height, width, one /*coef alpha*/,
	    solvmtxptr, stride,
	    xptr, UN,      zero /*coef beta*/,
	    tmp, UN);
      
      /* y := tmp  */
      for(p2=p, jtmp=0; p2<=symbmtx->bcblktab[k].lbloknum; p2++) {
	size = symbmtx->bloktab[p2].lrownum - symbmtx->bloktab[p2].frownum +1;
	BLAS_AXPY(size, one, tmp + jtmp, UN, y + symbmtx->bloktab[p2].frownum - tli, UN);
	jtmp += size;
      }

      /*******************************************************************************/
      
    }
  }
  
  free(tmp);
  
}

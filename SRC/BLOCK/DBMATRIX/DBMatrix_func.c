/* @authors J. GAIDAMOUR */

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include <string.h> /* memset */

#include "block.h"

void SymbolMatrix_hdim(SymbolMatrix* symbmtx) {
  dim_t k, p;
  assert(symbmtx->hdim == NULL);

  symbmtx->hdim = (int*)malloc(sizeof(int)*symbmtx->cblknbr);
  assert(symbmtx->hdim != NULL);

  /* cblk */
  for(k=0; k<symbmtx->cblknbr; k++) {
    symbmtx->hdim[k] = 0;
    
    /* block */
    for(p=symbmtx->bcblktab[k].fbloknum; p<=symbmtx->bcblktab[k].lbloknum; p++) {
      symbmtx->hdim[k] += symbmtx->bloktab[p].lrownum - symbmtx->bloktab[p].frownum + 1;
    }
  }
}


/* TODO : flag pour indiquer le virtual, pour le free() */

void DBMatrix_VirtualCpy(DBMatrix *M, DBMatrix *Cpy) {
  memcpy(Cpy, M, sizeof(DBMatrix));
  Cpy->virtual = VIRTUAL;
}

void DBMatrix_Transpose(DBMatrix *M)
{
  /********************************************************************/
  /* This function transpose a dbmatrix                               */
  /********************************************************************/
  dim_t tmp;
  dim_t* tmpptr2;
  INTL *tmpptr;
  VSolverMatrix **tmpcsr;

  /** The principle is really simple: the block rows of the phidal matrix 
      becomes the column of the transpose phidal matrix **/
  tmp = M->dim1;
  M->dim1 = M->dim2;
  M->dim2 = tmp;

  tmp = M->tli;
  M->tli = M->tlj;
  M->tlj = tmp;

  tmp = M->bri;
  M->bri = M->brj;
  M->brj = tmp;
  
  tmpptr = M->ria;
  M->ria = M->cia;
  M->cia = tmpptr;
  
  tmpptr2 = M->rja;
  M->rja = M->cja;
  M->cja = tmpptr2;
  
  tmpcsr = M->ra;
  M->ra = M->ca;
  M->ca = tmpcsr;

  if(M->csc == 0)
    M->csc = 1;
  else
    M->csc = 0;

  if (M->alloc == CBLK) 
    M->alloc = RBLK;
  else if(M->alloc == RBLK)
    M->alloc = CBLK;
}

/* a utiliser dans le cas locally */
void DBMatrix_Cpy(DBMatrix *M, DBMatrix *Cpy) {
  int i0, ilast;

  memcpy(Cpy, M, sizeof(DBMatrix));
  Cpy->virtual = COPY;

  if (M->alloc == ONE)
    ilast = 1;
  else if (M->alloc == CBLK)
    ilast = M->brj - M->tlj + 1;
  else 
    ilast = M->bri - M->tli + 1;

  Cpy->a = (SolverMatrix*)malloc(sizeof(SolverMatrix)*ilast);
  memcpy(Cpy->a, M->a, sizeof(SolverMatrix)*ilast);

  for(i0=0; i0<ilast; i0++) {
    Cpy->a[i0].virtual = COPY;
  
    Cpy->a[i0].coeftab = (COEF*)malloc(sizeof(COEF)*Cpy->a[i0].coefnbr);
    assert(Cpy->a[i0].coeftab != NULL);
    memcpy(Cpy->a[i0].coeftab, M->a[i0].coeftab, sizeof(COEF)*Cpy->a[i0].coefnbr);
  }

  DBMatrix_Setup_cblktosolvmtx(Cpy);
}



void SolverMatrix2SolverMatrix(SolverMatrix* solvM, SymbolMatrix* symbM, 
			       SolverMatrix* solvCopy, SymbolMatrix* symbCopy);
void cpymat(int cdim, int hdim, COEF* a, blas_t strideA, COEF* b, blas_t strideB);


/* TODO : la src doit avoir plus de termes que la dest ? */
/* TODO : nom des variable par rapport Ã  DBMatrixCopy  P = SRC==M*/
/* UtilisÃ© que pour copier les blocs PHIDAL diagonals ? => amÃ©lioration ?*/

void DBMatrix_Copy2(DBMatrix* M, DBMatrix* Copy) {
  int j, j2;
  SolverMatrix *solvM=NULL, *solvCopy=NULL;

  int tl, br;

  INTL *ia; dim_t *ja;
  VSolverMatrix **a;

  INTL *Pia; dim_t *Pja;
  VSolverMatrix **Pa;

  int i0, i;

  assert(M->alloc == Copy->alloc); /* pas utile, mais pour l'instant, ne fonctionne que comme ca */

  /* TODO : utilitÃ© de cela dans ce contexte ? => pour travailler sur la meme solver */
  if ((M->alloc == ONE) || (M->alloc == CBLK) || (M->alloc == BLK)) {
    tl = M->tlj;
    br = M->brj;

    ia = M->cia;
    ja = M->cja;
    a  = M->ca;

    Pia = Copy->cia; /* todo : Pa en a - */
    Pja = Copy->cja;
    Pa  = Copy->ca;
 
  } else {
    assert(M->alloc == RBLK);

    tl = M->tli;
    br = M->bri;

    ia = M->ria;
    ja = M->rja;
    a  = M->ra;

    Pia = Copy->ria;
    Pja = Copy->rja;
    Pa  = Copy->ra;
  }
  
  if (M->alloc == ONE) {
    solvM = &M->a[0];
    solvCopy = &Copy->a[0];
    DBMatrix_Symb2SolvMtx(/* Copy, */solvCopy);    
  }
  
  for(i0=0,i=tl;i<=br;i0++,i++) { /* i = indice for dbmatrix and P */

    if ((M->alloc == CBLK) || (M->alloc == RBLK)) {
      solvM  = &M->a[i0];
      solvCopy = &Copy->a[i0];
      DBMatrix_Symb2SolvMtx(solvCopy);
    } 

    j2=Pia[i]; 
    
    for(j=ia[i];j<ia[i+1];j++) {
      if (j2>=Pia[i+1]) break;/**??*/
      if (ja[j] != Pja[j2]) continue;

      if (M->alloc == BLK) {
	solvM  = &M->a[j];
	solvCopy = &Copy->a[j];
	DBMatrix_Symb2SolvMtx(solvCopy);
      }

      /* TODO : dans le cas ou c'est copiable d'un coup */
      SolverMatrix2SolverMatrix(solvM, &a[j]->symbmtx, solvCopy, &Pa[j2]->symbmtx); 
      
      j2++;
    }
    
  }
    
}

/* TODO : tester en CBLK, klocal/global*/
/* TODO : changer les nom, mÃªme nb de lettre */
/* TODO : en CBLK , pas besoin de faire for(k...) recopie complÃ¨te d'un coup ?*/
/* si matrices identique, recopie d'un coup (localement consistant)*/
void SolverMatrix2SolverMatrix(SolverMatrix* solvM, SymbolMatrix* symbM, 
			       SolverMatrix* solvCopy, SymbolMatrix* symbCopy) {

  dim_t k;
  int cdim, hdim;
  blas_t strideM, strideCopy; 
  int pM, pCopy;
  COEF *mc, *cc;

  assert(symbM->cblknbr == symbCopy->cblknbr);

  for(k=0; k<symbM->cblknbr; k++) {
    cdim   = symbM->ccblktab[k].lcolnum - symbM->ccblktab[k].fcolnum +1; /** Largeur du bloc colonne **/
	
    strideM    = symbM   ->stride[k/*?? n'importe quoi +symbM->cblktlj */];
    strideCopy = symbCopy->stride[k/*                  +symbCopy->cblktlj */];

    pM     = symbM   ->bcblktab[k].fbloknum;
    pCopy  = symbCopy->bcblktab[k].fbloknum;

    mc = solvM   ->coeftab + solvM   ->bloktab[pM]   .coefind;
    cc = solvCopy->coeftab + solvCopy->bloktab[pCopy].coefind;

    hdim = symbM->hdim[k];  /** Hauteur de la surface compactÃÂ©e des blocs **/
    assert(hdim == symbCopy->hdim[k]);

    cpymat(cdim, hdim, mc, strideM, cc, strideCopy);

  }

}
      
/* TODO : recupÃ©rer les conventions des BLAS, mettre ds FACTO, TRSM etc. */
void cpymat(int cdim, int hdim, COEF* a, blas_t strideA, COEF* b, blas_t strideB)
{
  dim_t m;
  int UN = 1;
  COEF *ac=a, *bc=b;    
    
  for(m=0;m<cdim;m++)
    {
      BLAS_COPY(hdim, ac, UN, bc, UN);
      
      ac += strideA;
      bc += strideB;
    }
}

/*  m = P->nextprec->L; */
/*  copy = P->nextprec->L == U */
/*  P->nextprec->L = P->nextprec->U = (DBMatrix*)malloc(sizeof(DBMatrix));  */

void DBMatrix_Copy3(UDBMatrix* LU, UDBMatrix* LUcopy, int_t levelnum, int_t locally_nbr, PhidalHID* BL) {
  DBMatrix* L = LU->L;
  DBMatrix* Lcopy = LUcopy->L;

  SymbolMatrix symbmtxCutS;

  /* printfv(5, "*** call DBMatrix_Copy3\n"); */

  DBMatrix_Init(Lcopy);
  Lcopy->symmetric = L->symmetric;
  Lcopy->alloc = ALLOC_S2;

  DBMatrix_Setup_HID(L->tli, L->tlj, L->bri, L->brj, "L", "N", locally_nbr, Lcopy, BL); /* TODO : get "L" and "N" from m matrix */
  /* can be shared */

  DBMatrix_HID2SymbolMatrix(Lcopy, &symbmtxCutS, BL, 
			    L->a[0].symbmtx.facedecal, L->a[0].symbmtx.tli/*==fcolnum, pas besoin d'arg*/);
	      
  Lcopy->cblknbr = symbmtxCutS.cblknbr;
  Lcopy->nodenbr = symbmtxCutS.nodenbr;
  /* 	Lcopy->ccblktab = NULL; /\* car != RBLK*\/ */

  { /* cf DBMatrix_Setup */
    if (Lcopy->alloc == RBLK) {
      Lcopy->ccblktab = symbmtxCutS.ccblktab; 
      /*     symbmtxCutS.ccblktab = NULL; TODO! */
    } else {
      Lcopy->ccblktab = NULL;
    }
		
    /* tmp */
    if (Lcopy->alloc == CBLK) {
      Lcopy->ccblktab = symbmtxCutS.ccblktab; 
    }
  }
	      
  /* Lcopy->hdim = NULL;     /\* car != RBLK*\/ */ /* DANS DBMATRIX_SETUP_SYMBOL !, alloc pour RBLK BLK */
  DBMatrix_Setup_SYMBOL(Lcopy, &symbmtxCutS, BL, levelnum+1-1, BL->nlevel);
  fix(&symbmtxCutS, ALLOC_S2);
  SymbolMatrix_Clean(&symbmtxCutS);
	      
  /***/
  Lcopy->coefmax = DBMatrix_calcCoefmax(Lcopy, Lcopy);
	      
  /* A optim : quand c'est possible, utiliser DBMatrix_Cpy(m, m); old */
  /* A optim : reprendre les Cut dejÃ  rÃ©alisÃ©s pour S ! Ne refaire que Setup2 si necessaire */
	      
  /* Equivalent du DBMatrix_copy->, sauf que la source est une DBMatrix et non une phidal */
  /* 	printfv(5, "%d %d\n",m->a[0].symbmtx.facedecal, Lcopy->a[0].symbmtx.facedecal); */
	      
  DBMatrix_Copy2(L, Lcopy);

}

void DBMatrix_Copy3_unsym(UDBMatrix* LU, UDBMatrix* LUcopy, int_t levelnum, int_t locally_nbr, PhidalHID* BL) {
  DBMatrix* L = LU->L;
  DBMatrix* U = LU->U;

  DBMatrix* Lcopy = LUcopy->L;
  DBMatrix* Ucopy = LUcopy->U;

  SymbolMatrix symbmtxCutS;

  /* printfv(5, "*** call DBMatrix_Copy3_unsym\n"); */
	    
  /** L **/
  DBMatrix_Init(Lcopy);
  Lcopy->symmetric =  L->symmetric;
  Lcopy->alloc = ALLOC_S2;

  DBMatrix_Setup_HID(L->tli, L->tlj, L->bri, L->brj, "L", "N", locally_nbr, Lcopy, BL); /* TODO : get "L" and "N" from m matrix */
  DBMatrix_HID2SymbolMatrix(Lcopy, &symbmtxCutS, BL, 
			    L->a[0].symbmtx.facedecal, L->a[0].symbmtx.tli/*==fcolnum, pas besoin d'arg*/);
	    
  Lcopy->cblknbr = symbmtxCutS.cblknbr;
  Lcopy->nodenbr = symbmtxCutS.nodenbr;
  Lcopy->ccblktab = NULL; /* car != RBLK*/
  Lcopy->hdim = NULL;     /* car != RBLK*/
	    
  DBMatrix_Setup_SYMBOL(Lcopy, &symbmtxCutS, BL, levelnum+1-1, BL->nlevel);
	    
  fix(&symbmtxCutS, ALLOC_S2);
  SymbolMatrix_Clean(&symbmtxCutS);
  /** **/
	    
  /** U **/
  DBMatrix_SetupV(Lcopy, Ucopy); /*Cpy coeftab*/
  DBMatrix_Transpose(Ucopy);
  /** **/
	    
  Lcopy->coefmax = Ucopy->coefmax = DBMatrix_calcCoefmax(Lcopy, Lcopy);
	    
  DBMatrix_Copy2(L, Lcopy);
  DBMatrix_Copy2(U, Ucopy);
}

/* TODO : algo dans l'autre sens pour BLK (block phidal par block phidal) ? Celui ci copie l'algo adaptÃ© Ã  CBLK */
void DBMatrix_ColMult2(REAL *b, DBMatrix *D, PhidalHID *BL) {
  SolverMatrix* solvmtx=NULL;
  SymbolMatrix* symbmtx;
  int k,i, i0, ii, iiglobal=-1, iiglobalt=-1, j;
  int hdim, stride;
  dim_t p;
  COEF* ptr;
  
  int UN = 1;

  if (D->alloc == ONE) {
    solvmtx = &D->a[0];    
  }

  /* parcours par blocs colonnes */
  iiglobal=0;
  for(i0=0,i=D->tlj; i<=D->brj; i0++, i++) {

    symbmtx = &D->ca[D->cia[i]]->symbmtx;
    
    if (D->alloc == CBLK) {
      solvmtx = &D->a[i0];
    }

    if(D->alloc == BLK) {
      solvmtx = D->ca[D->cia[i]]->solvmtx;
    }

    if (D->alloc == RBLK) {
      solvmtx = &D->a[D->cja[D->cia[i]]-D->tli];
    }
    assert(solvmtx == D->ca[D->cia[i]]->solvmtx);
    solvmtx = D->ca[D->cia[i]]->solvmtx;

    for(k=0; k<symbmtx->cblknbr; k++) {  

      for(j=D->cia[i]; j<D->cia[i+1]; j++) {
	
	symbmtx = &D->ca[j]->symbmtx;
	if (D->alloc == RBLK) {
	  solvmtx = &D->a[D->cja[j]-D->tli];
	}
	
	if(D->alloc == BLK) {
	  solvmtx = D->ca[j]->solvmtx;
	}
	
	assert(solvmtx == D->ca[j]->solvmtx);
	solvmtx = D->ca[j]->solvmtx;

	/*******/
	p      = symbmtx->bcblktab[k].fbloknum;        /* triangular block */
	if (p > symbmtx->bcblktab[k].lbloknum) continue;
	ptr    = solvmtx->coeftab + solvmtx->bloktab[p].coefind;
	hdim   = symbmtx->hdim[k];
	stride = symbmtx->stride[k];
	
	for (ii=symbmtx->ccblktab[k].fcolnum, iiglobalt=iiglobal; ii<=symbmtx->ccblktab[k].lcolnum; ii++, iiglobalt++) { 
	  BLAS_SCAL2(hdim, b[iiglobalt], ptr, UN);
	  
	  ptr += stride;
	}
	/*******/

      }
      iiglobal=iiglobalt;

    }
  }

}


void DBMatrix_ColMult(REAL *b, DBMatrix *D, PhidalHID *BL) {
  
  if (D->alloc == BLK || D->alloc == RBLK) {
    DBMatrix_ColMult2(b, D, BL);
    return;
  }

  SolverMatrix* solvmtx;
  SymbolMatrix* symbmtx;
  int k,i, ilast, ii, iiglobal;
  int hdim, stride;
  dim_t p;
  COEF* ptr;
  
  int UN = 1;

  assert((D->alloc == ONE) || (D->alloc == CBLK));
  if (D->alloc == ONE) ilast = 1;
  else ilast = D->brj - D->tlj + 1;

  /* parcours par blocs colonnes */
  iiglobal=0;
  for(i=0;i<ilast;i++) {
    solvmtx = &D->a[i];
    symbmtx = &solvmtx->symbmtx;
    
    for(k=0; k<symbmtx->cblknbr; k++) {  
      p      = symbmtx->bcblktab[k].fbloknum;        /* triangular block */
      ptr    = solvmtx->coeftab + solvmtx->bloktab[p].coefind;
      hdim   = symbmtx->hdim[k];
      stride = symbmtx->stride[k];

      for (ii=symbmtx->ccblktab[k].fcolnum; ii<=symbmtx->ccblktab[k].lcolnum; ii++, iiglobal++) { 
	BLAS_SCAL2(hdim, b[iiglobal], ptr, UN);

	/* optim ?            */
	/* ptr += stride + 1; */
	/* hdim -= 1;         */
	ptr += stride;
      }
    }
  }

}

void DBMatrix_RowMult2(REAL *b, DBMatrix *D, PhidalHID *BL) {
  SolverMatrix* solvmtx=NULL;
  SymbolMatrix* symbmtx;
  int k,i, i0, /* iiglobal, iiglobalt, */ j;
  int hdim, stride;
  dim_t p;
  COEF* ptr;
  COEF* lc;

  dim_t m;
  int cdim;
  int init;

  if (D->alloc == ONE) {
    solvmtx = &D->a[0];    
  }
  
  symbmtx = &D->ca[D->cia[D->tlj]]->symbmtx;

  init= symbmtx->bloktab[symbmtx->bcblktab[0].fbloknum].frownum; /*todo*/

  /* parcours par blocs colonnes */
  for(i0=0,i=D->tlj; i<=D->brj; i0++, i++) {

    symbmtx = &D->ca[D->cia[i]]->symbmtx;
    
    if (D->alloc == CBLK) {
      solvmtx = &D->a[i0];
    }

    if(D->alloc == BLK) {
      solvmtx = D->ca[D->cia[i]]->solvmtx;
    }

    if (D->alloc == RBLK) {
      solvmtx = &D->a[D->cja[D->cia[i]]-D->tli];
    }
    assert(solvmtx == D->ca[D->cia[i]]->solvmtx);
    solvmtx = D->ca[D->cia[i]]->solvmtx;

    for(k=0; k<symbmtx->cblknbr; k++) {  /* inverser boucle k et j ? */

      cdim   = symbmtx->ccblktab[k].lcolnum - symbmtx->ccblktab[k].fcolnum + 1;

      for(j=D->cia[i]; j<D->cia[i+1]; j++) {

	symbmtx = &D->ca[j]->symbmtx;
	if (D->alloc == RBLK) {
	  solvmtx = &D->a[D->cja[j]-D->tli];
	}
	
	if(D->alloc == BLK) {
	  solvmtx = D->ca[j]->solvmtx;
	}
	
	assert(solvmtx == D->ca[j]->solvmtx);
	solvmtx = D->ca[j]->solvmtx;

	/*******/
	p      = symbmtx->bcblktab[k].fbloknum;        /* triangular block */
	if (p > symbmtx->bcblktab[k].lbloknum) continue;
	ptr    = solvmtx->coeftab + solvmtx->bloktab[p].coefind;
	hdim   = symbmtx->hdim[k];
	stride = symbmtx->stride[k];

	/* assert tj vrai sinon : verifier sym/unsym dans inputs (en RBLK) */
	/* printfv(5, "cdim = %d, %d \n", cdim, symbmtx->ccblktab[k].lcolnum - symbmtx->ccblktab[k].fcolnum + 1); */
	/* check sherman3 LL, 1, 2 : = pb TRANSPOSE */	
	assert(cdim == symbmtx->ccblktab[k].lcolnum - symbmtx->ccblktab[k].fcolnum + 1);
	
	for(;p<=symbmtx->bcblktab[k].lbloknum; p++)
	  {
	    /** Calcul du pointeur dans coeftab vers le debut des coefficients du bloc p **/ 
	    lc = solvmtx->coeftab + solvmtx->bloktab[p].coefind;
	    
	    for(m=symbmtx->bloktab[p].frownum;m<=symbmtx->bloktab[p].lrownum;m++)
	      {
		/* printfv(5, "i=%d, k=%d m=%d: BLAS_SCAL cdim=%d b=%e %d stride=%d\n", i,k,m,cdim, b[m-init], solvmtx->bloktab[p].coefind, /\* lc, *\/ stride); */
		BLAS_SCAL2(cdim, b[m-init], lc, stride);    
		lc++;
	      }
	  }
	/*******/

      }
      /* iiglobal=iiglobalt; */

    }
  }

}

void DBMatrix_RowMult(REAL *b, DBMatrix *D, PhidalHID *BL) {
  
  if (D->alloc == BLK || D->alloc == RBLK) {
    DBMatrix_RowMult2(b, D, BL);
    return;
  }

  SolverMatrix* solvmtx;
  SymbolMatrix* symbmtx;
  int k,i, ilast, init, m;
  int cdim, hdim, stride;
  dim_t p;
  COEF* lc;
  
  assert((D->alloc == ONE) || (D->alloc == CBLK));
  if (D->alloc == ONE) ilast = 1;
  else ilast = D->brj - D->tlj + 1;

  /* parcours par blocs colonnes */
  init=D->a[0].symbmtx.bloktab[D->a[0].symbmtx.bcblktab[0].fbloknum].frownum; /*todo*/

  for(i=0;i<ilast;i++) {
    solvmtx = &D->a[i];
    symbmtx = &solvmtx->symbmtx;
    
    for(k=0; k<symbmtx->cblknbr; k++) {  
      p      = symbmtx->bcblktab[k].fbloknum;        /* triangular block */
      
      cdim   = symbmtx->ccblktab[k].lcolnum - symbmtx->ccblktab[k].fcolnum + 1; /** Largeur du bloc colonne **/
     
      hdim   = symbmtx->hdim[k];
      stride = symbmtx->stride[k];
      assert(hdim == stride);

      for(;p<=symbmtx->bcblktab[k].lbloknum; p++)
	{
	  /** Calcul du pointeur dans coeftab vers le debut des coefficients du bloc p **/ 
	  lc = solvmtx->coeftab + solvmtx->bloktab[p].coefind;

	  for(m=symbmtx->bloktab[p].frownum;m<=symbmtx->bloktab[p].lrownum;m++)
	    {
	      /* printfv(5, "i=%d, k=%d m=%d: BLAS_SCAL cdim=%d b=%e %d stride=%d\n", i,k,m,cdim, b[m-init], solvmtx->bloktab[p].coefind, /\* lc, *\/ stride); */
	      BLAS_SCAL2(cdim, b[m-init], lc, stride);    
	      lc++;
	    }
	  
	}
      
    }
  }
  
}


/* todo : la version 1 ne sert Ã  rien */ 
void DBMatrix_DiagMult2(REAL *b, DBMatrix *D, PhidalHID *BL) {
  SolverMatrix* solvSD;
  SymbolMatrix* SD;
  int k, i, i0, ii, iiglobal;
  blas_t stride;
  dim_t p;
  COEF* ptr;
  
  /* if (D->alloc == ONE) { */
    solvSD = &D->a[0];
  /* } */

  /* parcours par blocs colonnes */
  iiglobal=0;
  for(i0=0,i=D->tlj; i<=D->brj; i0++, i++) {
    SD = &D->ca[D->cia[i]]->symbmtx;

    if (D->alloc == CBLK) {
      solvSD = &D->a[i0];
    }

    if(D->alloc == BLK) {
      solvSD = D->ca[D->cia[i]]->solvmtx;
    }

    if (D->alloc == RBLK) {
      solvSD = &D->a[D->cja[D->cia[i]]-D->tli];
    }

    for(k=0; k<SD->cblknbr; k++) {
      p      = SD->bcblktab[k].fbloknum;        /* triangular block */
      ptr    = solvSD->coeftab + solvSD->bloktab[p].coefind;
      stride = SD->stride[k];

      for (ii=SD->ccblktab[k].fcolnum; ii<=SD->ccblktab[k].lcolnum; ii++, iiglobal++) { /* TODO : nom de variables */
	ptr[0] *= b[iiglobal] * b[iiglobal];
	ptr += stride + 1;
      }
    }
  }

}

void DBMatrix_DiagMult(REAL *b, DBMatrix *D, PhidalHID *BL) {
  if (D->alloc == BLK || D->alloc == RBLK) {
    DBMatrix_DiagMult2(b, D, BL);
    return;
  }

  SolverMatrix* solvmtx;
  SymbolMatrix* symbmtx;
  int k,i, ilast, ii, iiglobal;
  blas_t stride;
  dim_t p;
  COEF* ptr;
  
  assert((D->alloc == ONE) || (D->alloc == CBLK));
  if (D->alloc == ONE) ilast = 1;
  else ilast = D->brj - D->tlj + 1;

  /* parcours par blocs colonnes */
  iiglobal=0;
  for(i=0;i<ilast;i++) {
    solvmtx = &D->a[i];
    symbmtx = &solvmtx->symbmtx;
    
    for(k=0; k<symbmtx->cblknbr; k++) {  
      p      = symbmtx->bcblktab[k].fbloknum;        /* triangular block */
      ptr    = solvmtx->coeftab + solvmtx->bloktab[p].coefind;
      stride = symbmtx->stride[k];

      for (ii=symbmtx->ccblktab[k].fcolnum; ii<=symbmtx->ccblktab[k].lcolnum; ii++, iiglobal++) { 
	ptr[0] *= b[iiglobal] * b[iiglobal];
	ptr += stride + 1;
      }
    }
  }

}



void DBMatrix_PRINT(char* filename, DBMatrix *D) {
  SolverMatrix* solvmtx;
  SymbolMatrix* symbmtx;
  int k,i, i0, ii, iiglobal=-1, iiglobalt=-1, j;
  int rdim, stride;
  dim_t p;
  COEF* ptr;
  dim_t m;

  FILE* fd = fopen(filename, "w");

  fprintfd(fd, "A DEBUG, utiliser + tot fdumpPhidalMatrix\n");

  if (D->alloc == ONE) {
    solvmtx = &D->a[0];    
  }

  /* parcours par blocs colonnes */
  iiglobal=0;
  for(i0=0,i=D->tlj; i<=D->brj; i0++, i++) {

    symbmtx = &D->ca[D->cia[i]]->symbmtx;
    
    for(k=0; k<symbmtx->cblknbr; k++) {  

      for(j=D->cia[i]; j<D->cia[i+1]; j++) {
	
	symbmtx = &D->ca[j]->symbmtx;
	solvmtx = D->ca[j]->solvmtx;

	/*******/
	/* p      = symbmtx->bcblktab[k].fbloknum;        /\* triangular block *\/ */
	/* 	if (p > symbmtx->bcblktab[k].lbloknum) { */
	/* 	  /\* fprintf(fd, "continue\n"); *\/ */
	/* 	  continue; */
	/* 	} */
	/* hdim   = symbmtx->hdim[k]; */
	stride = symbmtx->stride[k];
	
	for (ii=symbmtx->ccblktab[k].fcolnum, iiglobalt=iiglobal; ii<=symbmtx->ccblktab[k].lcolnum; ii++, iiglobalt++) { 
	  /* fprintf(fd, "iiglobalt =%d (%d %d)\n", iiglobalt+1, symbmtx->bcblktab[k].fbloknum, symbmtx->bcblktab[k].lbloknum); */

	  for(p = symbmtx->bcblktab[k].fbloknum;p<=symbmtx->bcblktab[k].lbloknum; p++)
	    {
	      ptr = solvmtx->coeftab + solvmtx->bloktab[p].coefind;	      
	      rdim = symbmtx->bloktab[p].lrownum - symbmtx->bloktab[p].frownum+1;
	      /* fprintf(fd, ". rdim = %d\n", rdim); */

	      for(m=0; m<rdim; m++) {
		if (ptr[m] != 0) { /* don't print 0 */
		  if (m+symbmtx->bloktab[p].frownum >= iiglobalt) {
		    
		    /* +1 pour i et j pour fortran numbering */ 
#ifdef TYPE_COMPLEX
		    fprintf(fd, "%d %d %.13e  %.13e\n", m+symbmtx->bloktab[p].frownum +1, iiglobalt +1, pcoef(ptr[m]));
#else
		    fprintf(fd, "%d %d %.13e\n", m+symbmtx->bloktab[p].frownum +1, iiglobalt +1, pcoef(ptr[m]));
#endif
		  } else {
		    /* fprintf(fd, "non : %d %d\n", m+symbmtx->bloktab[p].frownum +1, iiglobalt +1); */
		  }
		  
		} else { 
/* 		  fprintf(fd, "0 : %d %d\n", m+symbmtx->bloktab[p].frownum +1, iiglobalt +1);  */
/* 		  printfv(5, "0 : %d %d\n", m+symbmtx->bloktab[p].frownum +1, iiglobalt +1);  */
		  if (m+symbmtx->bloktab[p].frownum == iiglobalt) {
#ifdef TYPE_COMPLEX
		    fprintf(fd, "%d %d %.13e  %.13e\n", m+symbmtx->bloktab[p].frownum +1, iiglobalt +1, pcoef(ptr[m]));
#else
		    fprintf(fd, "%d %d %.13e\n", m+symbmtx->bloktab[p].frownum +1, iiglobalt +1, pcoef(ptr[m]));
#endif
		  }
		}
	      } 
	    }
	  
	  ptr += stride;
	}
	/*******/

      }
      iiglobal=iiglobalt;

    }
  }

  fclose(fd);
}

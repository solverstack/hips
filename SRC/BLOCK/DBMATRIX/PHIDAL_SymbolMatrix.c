/* @authors J. GAIDAMOUR */

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include <string.h>

#include "phidal_struct.h" /*utile ?*/

#include "block.h"

#include "symbol.h" /*MatrixStruct*/
#include "solver.h"

#include "phidal_sequential.h"

void HID2MatrixStruct(dim_t *rangtab, MatrixStruct* mtxstruct, PhidalHID* BL, int startlevel, int_t locally_nbr);

/* void PHIDAL_SymbolMatrix(csptr mat, SymbolMatrix* symbmtx, dim_t **rangtab_pt, dim_t *treetab, dim_t cblknbr_l1, PhidalHID* BL, int_t locally_nbr) { */
/*   chrono_t t1,t2,ttotal; /\* benchmark *\/ */

/*   MatrixStruct* mtxstr; */
/*   dim_t cblknbr = cblknbr_l1 + (BL->nblock - BL->block_levelindex[1]);  */

/*   dim_t *rangtab = *rangtab_pt; */

/*   /\************************************************************************************************************\/ */
/*   /\************************************************************************************************************\/ */
/*   /\************************************************************************************************************\/ */
/*   /\* LEVEL 1 *\/ */

/*   /\************************************************************************************************************\/ */
/*   /\* Symbolic Block Factorisation  ****************************************************************************\/ */
/*   /\************************************************************************************************************\/ */
/*   fprintfv(5, stdout, " Symbolic Factorisation\n"); */
/*   ttotal = 0; */

/*   /\* Build a sorted list of intervalls for each column blocks of level 1 *\/ */
/*   t1  = dwalltime();  */
/*   mtxstr = (MatrixStruct*)malloc(sizeof(MatrixStruct)*cblknbr); */
/*   symbolicBlok(mtxstr, mat, cblknbr_l1, rangtab); */
/*   t2  = dwalltime(); ttotal += t2-t1; */
/*   fprintfv(5, stdout, "  Compute block structure in %g seconds\n", t2-t1); */

/*   /\* Symbolic factorisation : compute block fill-in (level 1 only) *\/ */
/*   t1  = dwalltime();  */
/*   symbolicFacto(mtxstr, cblknbr_l1, treetab, rangtab);  */
/*   t2  = dwalltime(); ttotal += t2-t1; */
/*   fprintfv(5, stdout, "  Symbolic Factorisation  in %g seconds\n", t2-t1); */

 

/*   /\* LEVEL 2 *\/ */

/*   *rangtab_pt = (int*)malloc(sizeof(int)*(cblknbr+1)); */
/*   memcpy(*rangtab_pt, rangtab, sizeof(int)*(cblknbr_l1+1)); */
/*   free(rangtab); rangtab = *rangtab_pt; */

/*   /\*   printfv(5, "rangtab[%d] = %d\n", cblknbr_l1, rangtab[cblknbr_l1]); *\/ */

/*   HID2MatrixStruct(rangtab+cblknbr_l1, mtxstr+cblknbr_l1, BL, 1, locally_nbr); */


/*   /\************************************************************************************************************\/ */
/*   /\*** Amalgamation ****************************************************************************************************\/ */
/*   /\************************************************************************************************************\/ */
/*   /\*amalgamate(0.05, );*\/ */






/*   /\************************************************************************************************************\/ */
/*   /\************************************************************************************************************\/ */
/*   /\************************************************************************************************************\/ */
/*   /\* MatrixStruct -> SymbolMatrix *\/ */

/*   /\* Build SymbolMatrix structure from mtxstr *\/  */
/*   t1  = dwalltime();  */
/*   MatrixStruct2SymbolMatrix(symbmtx, mtxstr, cblknbr, rangtab); */
/*   t2  = dwalltime(); */
/*   fprintfv(5, stdout, "  Build SymbolMatrix in %g seconds\n", t2-t1); */

/*   freeMatrixStruct(mtxstr, cblknbr); */
/* } */

/*arg : rangtab decalé... pas tres joli finalement. on pourrait passer en arg cblknbr_l1x*/
void HID2MatrixStruct(dim_t *rangtab, MatrixStruct* mtxstruct, PhidalHID* BL, int startlevel, int_t locally_nbr) {
  dim_t i,j;
  int iBL;

  char* UPLO = "U"; /* == L in CSR. phidal block pattern is symmetric */
  char* DIAG = "N";
  csptr P;

  /*****************************************/
  /*** Compute the sparse block pattern ****/
  /*****************************************/
  P = (csptr)malloc(sizeof(struct SparRow));
  initCS(P, BL->nblock);

  phidal_block_pattern(0, 0, BL->nblock-1, BL->nblock-1, UPLO, DIAG, P, locally_nbr, BL);

  /* la première case du tableau est la séparation entre level1 et level2 :*/
  /*   printfv(5, "%d %d\n",rangtab[0],BL->block_index[iBL]); */

  /*   assert(rangtab[0] == BL->block_index[BL->block_levelindex[startlevel]/\*iBL0*\/]); */
  rangtab[0] = BL->block_index[BL->block_levelindex[startlevel]];

  for (i=0, iBL=BL->block_levelindex[startlevel]; iBL<BL->nblock; iBL++, i++) { 
    rangtab[i+1] = BL->block_index[iBL+1]; /* fin du bloc courant */
    mtxstruct[i].bloknbr = P->nnzrow[iBL]-1; /* -1 because mtxstruct doesn't contain diagonal block ! */

    /* Dprintfv(5, "%d %d (%d)\n",rangtab[i],rangtab[i+1], mtxstruct[i].bloknbr); */

    if (mtxstruct[i].bloknbr >0)
      mtxstruct[i].bloktab = (BlokStruct*)malloc(sizeof(BlokStruct)*mtxstruct[i].bloknbr);
    else { mtxstruct[i].bloktab = NULL; }

    for(j=1;j<P->nnzrow[iBL];j++) { /* Start to j=1 because mtxstruct doesn't contain diagonal block ! */
      /*       Dprintfv(5, "B(%d) : %d - %d\n",i,BL->block_index[P->ja[iBL][j]],BL->block_index[P->ja[iBL][j]+1]-1); */
      mtxstruct[i].bloktab[j-1].frownum = BL->block_index[P->ja[iBL][j]];
      mtxstruct[i].bloktab[j-1].lrownum = BL->block_index[P->ja[iBL][j]+1]-1;
    }
    
  }
  cleanCS(P);
  free(P);
}

/* /\* version sans, non testé, sera + propre, evite un decoupage en 3, generation de S a part *\/ */
/* void HID2MatrixStruct2(dim_t *rangtab, MatrixStruct* mtxstruct, PhidalHID* BL, int startlevel, int_t locally_nbr) { */
/*   int i, iBL; */

/*   /\*\la première case du tableau est la séparation entre level1 et level2 :*\/ */
/*   assert(rangtab[0] == BL->block_index[BL->block_levelindex[startlevel]/\*iBL0*\/]); */

/*   for (i=0, iBL=BL->block_levelindex[startlevel]; iBL<BL->nblock; iBL++, i++) {  */
/*     rangtab[i+1] = BL->block_index[iBL+1]; /\* fin du bloc courant *\/ */
/*     mtxstruct[i].bloknbr = 0; */
/*     mtxstruct[i].bloktab = NULL; */
/*   } */
/* } */


/* A debugger si utilisé (voir fonction d'apres) */
/* void HID2MatrixStruct3(DBMatrix* m, PhidalHID* BL, int startlevel) { */
/*   int i,j0, j; */
/*   int iBL; */


/*   exit(1);   /\* non utilisé une étape de la construction de HID2Symbmtx *\/ */
/*   MatrixStruct* mtxstruct; */

/*   for (i=0, iBL=BL->block_levelindex[startlevel]; iBL<BL->nblock; iBL++, i++) {  */
/*         rangtab[i+1] = BL->block_index[iBL+1]; /\* fin du bloc courant *\/ */

/*     mtxstruct[i].bloknbr = m->cia[i+1] - m->cia[i]    -1; /\* -1 because mtxstruct doesn't contain diagonal block ! *\/ */

/*     if (mtxstruct[i].bloknbr >0) */
/*       mtxstruct[i].bloktab = (BlokStruct*)malloc(sizeof(BlokStruct)*mtxstruct[i].bloknbr); */
/*     else { mtxstruct[i].bloktab = NULL; } */

/*     for(j0=1, j=m->cia[i]+1;j<m->cia[i+1];j0++, j++) { /\* Start to j=1 because mtxstruct doesn't contain diagonal block ! *\/ */
/*       mtxstruct[i].bloktab[j0-1].frownum = BL->block_index[m->cja[j]]; */
/*       mtxstruct[i].bloktab[j0-1].lrownum = BL->block_index[m->cja[j]+1]-1; */
/*     } */
    
/*   } */

/* } */

/*TODO : ccblktab*/
void DBMatrix_HID2SymbolMatrix(DBMatrix* m, SymbolMatrix* symbmtx, PhidalHID* BL, flag_t facedecal, dim_t tli) {
  int i0, iBL; 
  dim_t j;
  dim_t p;

  symbmtx->cblknbr   = (BL->nblock - m->tlj);         /*+ Number of column blocks           +*/
  symbmtx->facedecal = facedecal;
  symbmtx->tli       = tli;
  symbmtx->virtual   = 0;
  symbmtx->nodenbr   = BL->block_index[BL->nblock] - BL->block_index[m->tlj];
    
  symbmtx->ccblktab = (SymbolCCblk*)malloc(sizeof(SymbolCCblk)*symbmtx->cblknbr);    /*+ Array of column blocks [based] +*/
  symbmtx->bcblktab = (SymbolBCblk*)malloc(sizeof(SymbolBCblk)*symbmtx->cblknbr);    /*+ Array of column blocks [based] +*/
  symbmtx->hdim     = NULL;/* (int*)        malloc(sizeof(int)        *symbmtx->cblknbr); */
  symbmtx->stride   = NULL;/* symbmtx->hdim; */

  /**** Compute blocknbr */
  symbmtx->bloknbr = 0;  
  for (iBL=m->tlj; iBL<BL->nblock; iBL++) { 
    symbmtx->bloknbr += m->cia[iBL+1] - m->cia[iBL];
  }

  /**** Filling up data structure */
  symbmtx->bloktab = (SymbolBlok*)malloc(sizeof(SymbolBlok)*symbmtx->bloknbr);

  assert(m->tlj == BL->block_levelindex[1/* Sur S, startlevel */]); /* TODO simplifier ailleurs */

  p=0; /* block index in symbmtx->bloktab*/
  for (i0=0,iBL=m->tlj; iBL<BL->nblock; i0++, iBL++) { 
    symbmtx->ccblktab[i0].fcolnum = BL->block_index[iBL];
    symbmtx->ccblktab[i0].lcolnum = BL->block_index[iBL+1] - 1;
    symbmtx->bcblktab[i0].fbloknum = p;
    /*  symbmtx->hdim[i0] = 0; */

    assert(BL->block_index[m->cja[m->cia[iBL]]] == BL->block_index[iBL]);

    for(j=m->cia[iBL];j<m->cia[iBL+1]; j++, /*!!*/p++) { 
      symbmtx->bloktab[p].frownum = BL->block_index[m->cja[j]];
      symbmtx->bloktab[p].lrownum = BL->block_index[m->cja[j]+1]-1;
      /* symbmtx->hdim[i0] += symbmtx->bloktab[p].lrownum - symbmtx->bloktab[p].frownum +1; */
      symbmtx->bloktab[p].cblknum = m->cja[j] - m->tlj + facedecal;
    }
    
    symbmtx->bcblktab[i0].lbloknum = p-1; /*todo : deplacer les p++*/
  }
  
  
}

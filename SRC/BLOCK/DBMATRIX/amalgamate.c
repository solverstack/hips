/* @authors P. HENON */

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include "queue.h"
/*#include "perf.h"*/
#include "block.h"
#include "phidal_sequential.h"
#include "symbol.h" /*MatrixStruct*/
#include "solver.h"



/*#define BLAS_GAIN*/    /** Amalgamation use the best ratio time/nnzadd
			 to merge cblk **/

#define INFINI 10e6


/** Local function **/
int merge_cost(int a, int b, csptr P, int *colweight);
#ifdef BLAS_GAIN
REAL cblk_time(int n, dim_t *ja, int colnbr);
REAL merge_gain(int a, int b, csptr P, int *colweight, int *tmp);
#endif

void  merge_col(int a, int b, csptr P);
void get_son(int node, int *sonindex, int *sontab, int *colweight, int *ns, int *list);


void amalgamate(REAL rat, int nrow, csptr P, int snodenbr, int *snodetab, dim_t *treetab, int *cblknbr, dim_t **rangtab, dim_t *nodetab)
{
  /********************************************************************/
  /* amagamate takes a supernode graph (P,colwgt) and performs some   */
  /* amalgation of the column until a fill tolerance (rat) is reached */
  /* It returns the partition of columns obtained                     */
  /* On return                                                        */
  /* P  is updated to form the column compressed graph (not the
     quotient graph !!) */
  /* treetab is also updated to fit with the new column compressed    */
  /*   graph
       */
  /* cblknbr, rangtab is the supernode partition of the initial
     matrix P */
  /********************************************************************/
  
  int *nnzadd;
  int *sonindex;

  int *sontab;
  dim_t *treetab2; /** The treetab is updated as amalgamation comes
		     along **/
  int *tmp, *tmp2;
  int *newnum;
  int i,j,k,ind;
  int father;
  int cblknum;
  int *colweight;
  float *gain;
  int toto;
  int n, nn;

  float key;
  Queue heap;
  long fillmax, fill;

  n = P->n; /** Number of supernode **/
  if(snodetab != NULL)
    {
      nn = snodetab[snodenbr]; /** Number of unknowns **/
    }
  else
    nn = n;


#ifdef DEBUG_M
  if(snodetab != NULL)
    {
      assert(n == snodenbr);
      for(i=0;i<n;i++)
	{
	  assert(P->nnzrow[i] >= snodetab[i+1]-snodetab[i]);
	  k = 0;
	  for(j=snodetab[i]; j < snodetab[i+1];j++)
	    assert(P->ja[i][k++] == j);
	}
    }

  /** Check some things about P **/
  for(i=0;i<P->n;i++)
    {
      if(P->nnzrow[i] < (*rangtab)[i+1]-(*rangtab)[i])
	for(j=0;j< P->nnzrow[i];j++)
	  fprintfd(stderr, "ja = %d, rang = %d \n", P->ja[i][j],(*rangtab)[i]+j);

      assert(P->nnzrow[i] >= (*rangtab)[i+1]-(*rangtab)[i]);
      for(j=0;j< (*rangtab)[i+1]-(*rangtab)[i];j++)
	{
	  if(P->ja[i][j] != (*rangtab)[i]+j)
	    fprintfd(stderr, "Cblk %d j %d ja %d rangtab[%d]=%d rangtab[%d] = %d \n", 
		    i, j, P->ja[i][j], i, (*rangtab)[i], i+1, (*rangtab)[i+1]);
	  assert(P->ja[i][j] == (*rangtab)[i]+j);
	}

      /** The matrix should be sorted **/
      for(j=1;j<P->nnzrow[i];j++)
	assert(P->ja[i][j] > P->ja[i][j-1]);
    }


#endif

  /*** Set the weight of each column ***/
  colweight = (int *)malloc(sizeof(int)*n);  
  if(snodetab == NULL)
    {
      for(i=0;i<n;i++)
	colweight[i] = 1;
    }
  else
    {
      for(i=0;i<snodenbr;i++)
	colweight[i] = snodetab[i+1]-snodetab[i];
    }



#ifdef DEBUG_M
  for(i=0;i<n;i++)
    assert(colweight[i] >= 0);
#endif

  /*** Compute the maximum extra fill allowed ***/
  if(snodetab == NULL)
    fillmax = (long)CSnnz(P)*rat;
  else
    {
      fillmax = 0;
      for(i=0;i<P->n;i++)
	{
	  fillmax += (colweight[i]*(colweight[i]+1))/2;
#ifdef DEBUG_M
	  if(P->nnzrow[i] < colweight[i])
	    fprintfd(stderr, "nnzrow[%ld] = %ld colweight = %ld \n", (long)i, (long)P->nnzrow[i], (long)colweight[i]);
	  assert(P->nnzrow[i] >= colweight[i]);
#endif
	  fillmax += (P->nnzrow[i]-colweight[i])*colweight[i];
	}
      fprintfd(stderr, "(NNZL) = %ld \n", (long)fillmax);
      
      fillmax = (long)(fillmax*rat);
    }



  treetab2 = (dim_t *)malloc(sizeof(dim_t)*n);
  nnzadd = (int *)malloc(sizeof(int)*n);
  gain = (float *)malloc(sizeof(float)*n);
  memcpy(treetab2, treetab, sizeof(int)*n);
  tmp = (int *)malloc(sizeof(int)*nrow);
  tmp2 = (int *)malloc(sizeof(int)*nrow);



  /**********************************************/
  /*** Compute the son list of each supernode ***/
  /**********************************************/
  /*** Compute the number of sons of each cblk ***/
  bzero(tmp, sizeof(int)*n);
  for(i=0;i<n-1;i++)
    if(treetab2[i] >= 0) /** IF THIS SNODE IS NOT A ROOT **/
      tmp[treetab2[i]]++;

  ind = 0;
  for(i=0;i<n;i++)
    ind += tmp[i];

  sonindex = (int *)malloc(sizeof(int)*(n+1));
  if(ind!=0)
    sontab = (int *)malloc(sizeof(int)*ind);
  else { printfd("sontab is null\n"); sontab = NULL; /* assert(0); */ }
  ind = 0;
  for(i=0;i<n;i++)
    {
      sonindex[i] = ind;
      ind += tmp[i];
    }
  sonindex[n] = ind;

  bzero(tmp, sizeof(int)*n);

  for(i=0;i<n-1;i++)
    {
      cblknum = treetab2[i];
      if(cblknum >= 0) /** IF THIS SNODE IS NOT A ROOT **/
	{
	  sontab[sonindex[cblknum]+tmp[cblknum]] = i;
	  tmp[cblknum]++;
	}
    }


  /***********************************************************/
  /* Compute the fill to merge a column of P with its father */
  /***********************************************************/
  for(i=0;i<n;i++)
    {
      father = treetab2[i];
      if(father == -1 || father == i)
	{
	  nnzadd[i] = INFINI;
	  gain[i] = INFINI;
	  continue;
	}

      nnzadd[i] = merge_cost(i, father, P, colweight);
#ifdef BLAS_GAIN
      gain[i] = merge_gain(i, father, P, colweight, tmp2)/nnzadd[i];
#else
      gain[i] = nnzadd[i];
#endif

      /*if(nnzadd[i] == 0)
	fprintfd(stderr, "Cblk %ld \n", i);*/
    }

  /*exit(0);*/

  /*****************************************************/
  /** Merge all the columns so it doesn't add fill-in **/
  /*****************************************************/
  toto = 0;
  /*fprintfd(stderr, "LOOP \n");*/
  for(i=0;i<n;i++)
    {
	
#ifdef DEBUG_M
      assert(colweight[i] > 0);
#endif
      if(colweight[i] != 0 && nnzadd[i] == 0 && gain[i] <= 0)
	{
	  father = treetab2[i];
	  
	  toto++;
#ifdef DEBUG_M
	  assert(father > 0 && father != i);
#endif
	  /** We merge the snode i and its father **/
	  merge_col(i, father, P);

	  /*fprintfd(stderr, "MERGE %ld (%ld) and %ld (%ld) \n", (long)i, (long)colweight[i], 
	    (long)father, (long)colweight[father]); */

	  colweight[father] += colweight[i];
	  colweight[i] = 0; /** mark this node as does not exist **/
	  
	  /**  we update nnzadd for the father **/
	  k = treetab2[father];
	  if(k != -1 && k != father)
	    {
	      nnzadd[father] = merge_cost(father, k, P, colweight);
#ifdef BLAS_GAIN
	      gain[father] = merge_gain(father, k, P, colweight, tmp2)/nnzadd[father];
#else
	      gain[father] = nnzadd[father];
#endif
	    }
	      
	  /** We update the sons of i now **/
	  get_son(i, sonindex, sontab, colweight, &ind, tmp);
	  for(j=0;j<ind;j++)
	    {
	      k = tmp[j];
	      treetab2[k] = father;
	      nnzadd[k] = merge_cost(k, father, P, colweight);
#ifdef BLAS_GAIN
	      gain[k] = merge_gain(k, father, P, colweight, tmp2)/nnzadd[k];
#else
	      gain[k] = nnzadd[k];
#endif

#ifdef DEBUG_M
	      assert(nnzadd[k] > 0);
#endif
	    }

	}
    }

  fprintfd(stderr, "APRES AMALGAMATION INIT cblk = %ld \n", (long)(n-toto));
  
  /*** Put in a sort heap the column sorted by their nnzadd ***/
  queueInit(&heap, n);
  for(i=0;i<n;i++)
    if(colweight[i] > 0 && (treetab2[i] > 0 && treetab2[i] != i))
      {
#ifdef DEBUG_M
#ifndef BLAS_GAIN
	if(nnzadd[i] <= 0)
	  fprintfd(stderr, "nnzadd[%ld] = %ld \n", (long)i, (long)nnzadd[i]);
	assert(nnzadd[i]>0);
#endif
#endif

#ifdef BLAS_GAIN
	if(gain[i] <= 0)
	  queueAdd(&heap, i, gain[i]);
#else
	queueAdd(&heap, i, gain[i]);
#endif
      }

  /*******************************************/
  /* Merge supernodes until we reach fillmax */
  /*******************************************/
  /*** Merge supernodes untill we reach the fillmax limit ****/
  fill = 0.0;
  while(queueSize(&heap)>0 && fill < fillmax)
    {
      /*i = queueGet2(&heap, &key);*/
      queueGet2(&heap, &i, &key);
       
       /*if(nnzadd[i] != (int)key || colweight[i] <= 0)*/
       if(gain[i] != key || colweight[i] <= 0)
	 continue;
       
       if(fill + nnzadd[i] > fillmax)
	 break;
       else
	 fill += nnzadd[i];




       toto++;
       father = treetab2[i];
#ifdef DEBUG_M
       assert(father > 0 && father != i);
       assert(colweight[father]>0);
#endif

       /*fprintfd(stderr, "Merge col %ld and %ld gain = %g \n", (long)i, (long)father, gain[i]);*/
       
       /** We merge the snode i and its father and 
	   we update treetab2, nnzadd, colweight **/
       merge_col(i, father, P);
       colweight[father] += colweight[i];
       colweight[i] = 0; /** mark this node as does not exist **/
 
       /**  we update nnzadd for the father **/
       k = treetab2[father];
       if(k != -1 && k != father)
	 {
	   nnzadd[father] = merge_cost(father, k, P, colweight);
#ifdef BLAS_GAIN
	   gain[father] = merge_gain(father, k, P, colweight, tmp2)/nnzadd[father];
#else
	   gain[father] = nnzadd[father];
#endif

	   /*queueAdd(&heap, father, (REAL) nnzadd[father]);*/
#ifdef BLAS_GAIN
	   if(gain[father] <= 0)
	     queueAdd(&heap, father, gain[father]);
#else
	   queueAdd(&heap, father, gain[father]);
#endif
	 }

       /** We update the sons of i now **/
       get_son(i, sonindex, sontab, colweight, &ind, tmp);
       for(j=0;j<ind;j++)
	 {
	   k = tmp[j];
	   treetab2[k] = father;
	   nnzadd[k] = merge_cost(k, father, P, colweight);
#ifdef BLAS_GAIN
	   gain[k] = merge_gain(k, father, P, colweight, tmp2)/nnzadd[k];
	   if(gain[k] <= 0)
	     queueAdd(&heap, k, gain[k]);
#else
	   gain[k] = nnzadd[k];
	   queueAdd(&heap, k, gain[k]);
#endif
	 }
     }

  free(nnzadd);
  free(gain);
  queueExit(&heap);

  /*fprintfd(stderr, "FINAL cblk = %ld \n", (long)(n-toto));*/



  /********************************/
  /* Compute the new partition    */
  /********************************/

  /** Count the number of supernodes **/

  /** tmp will be the newnum of node i in the rangtab **/
  newnum = tmp;
  bzero(newnum, sizeof(int)*n);
  k = 0;
  for(i=0;i<n;i++)
    if(colweight[i] > 0)
      newnum[i] = k++;
  *cblknbr = k;
  fprintfd(stderr, "NUMBER OF CBLK AFTER AMAL = %ld \n", (long)*cblknbr);

  free(*rangtab);
  *rangtab = (dim_t *)malloc(sizeof(dim_t)*(k+1));
  bzero(*rangtab, sizeof(int)*(k+1));

  for(i=0;i<n;i++)
    if(colweight[i] > 0)
      (*rangtab)[newnum[i] + 1]+= colweight[i];

  for(i=1;i<= (*cblknbr);i++)
    (*rangtab)[i] += (*rangtab)[i-1];


  for(i=0;i<n;i++)
    {
      if(colweight[i] > 0)
	{
	  if(snodetab != NULL)
	    for(j=snodetab[i];j<snodetab[i+1];j++)
	      nodetab[(*rangtab)[newnum[i]]++] = j;
	  else
	    nodetab[(*rangtab)[newnum[i]]++] = i;
	}
      else
	{
	  /** find the cblk this node is in **/
	  father = i;
	  while(colweight[father] <= 0)
	    {
	      father = treetab2[father];
#ifdef DEBUG_M
	      assert(father > 0);
#endif
	    }
	  if(snodetab != NULL)
	    for(j=snodetab[i];j<snodetab[i+1];j++)
	      nodetab[(*rangtab)[newnum[father]]++] = j;
	  else
	    nodetab[(*rangtab)[newnum[father]]++] = i;
	}
    }

  /** P can be a rectangular matrix so nrow > nn ;
      we have to complete the permutation vector for 
      the unknowns that do not move **/
  for(i=nn;i<nrow;i++)
    nodetab[i] = i; 

  /** reset rangtab to its real value **/
  for(i= *cblknbr; i>0; i--)
    (*rangtab)[i] = (*rangtab)[i-1];
  (*rangtab)[0] = 0;



  



#ifdef DEBUG_M
  /*for(i=0;i<*cblknbr+1;i++)
    fprintfd(stderr, "rangtab[%ld] = %ld \n", (long)i, (long)(*rangtab)[i]);
    exit(0);*/
  for(i=0;i<n;i++)
    if(colweight[i] > 0)
      assert(colweight[i] == (*rangtab)[newnum[i]+1]-(*rangtab)[newnum[i]]);


  /** check the iperm vector (nodetab) **/
 {
   int *flag;
   fprintfd(stderr, "Cblknbr = %d NN = %ld \n", *cblknbr, (long)nn);
   assert( (*rangtab)[*cblknbr] == nn);

   flag = (int *)malloc(sizeof(int)*nn);


   bzero(flag, sizeof(int)*nn);
   for(i=0;i<nn;i++)
     {
       assert(nodetab[i] >= 0 && nodetab[i] < nn);
       flag[nodetab[i]]++;
     }
   for(i=0;i<nn;i++)
     {
       if(flag[nodetab[i]] != 1)
	 fprintfd(stderr, "(Nodetab[%ld]Â = %ld falg = %ld ) ", (long)i, (long)nodetab[i], (long)flag[nodetab[i]]);
       assert(flag[nodetab[i]] == 1);
     }
   
   free(flag);
 }
#endif

  /** Compact P: each column from 1 to cblknbr will represent a cblk ***/
  ind = 0;
  while(ind < P->n && P->nnzrow[ind] >0 )
    ind++;

  for(i=ind;i<n;i++)
    if(P->nnzrow[i] > 0)
      {
#ifdef DEBUG_M
	assert(colweight[i] > 0);
#endif
	P->nnzrow[ind] = P->nnzrow[i];
	P->ja[ind] = P->ja[i];
	P->nnzrow[i] = 0;
	P->ja[i] = NULL;
	ind++;
      }
  P->n = *cblknbr;

  /*** Apply the new permutation to P ****/
  /** tmp is the perm vector **/
  /*for(i=0;i<nn;i++)*/
  for(i=0;i<nrow;i++)
    tmp[nodetab[i]] = i;

  for(i=0;i<P->n;i++)
    {
      dim_t *ja;
      ja = P->ja[i];
      for(j=0;j<P->nnzrow[i];j++)
	ja[j] = tmp[ja[j]];
    }
      
  
#ifdef DEBUG_M
  /** Check some things about P **/
  for(i=0;i<P->n;i++)
    {
      if(P->nnzrow[i] < (*rangtab)[i+1]-(*rangtab)[i])
	for(j=0;j< P->nnzrow[i];j++)
	  fprintfd(stderr, "ja = %d, rang = %d \n", P->ja[i][j],(*rangtab)[i]+j);

      assert(P->nnzrow[i] >= (*rangtab)[i+1]-(*rangtab)[i]);
      for(j=0;j< (*rangtab)[i+1]-(*rangtab)[i];j++)
	{
	  if(P->ja[i][j] != (*rangtab)[i]+j)
	    fprintfd(stderr, "Cblk %d j %d ja %d rangtab[%d]=%d rangtab[%d] = %d \n", 
		    i, j, P->ja[i][j], i, (*rangtab)[i], i+1, (*rangtab)[i+1]);
	  assert(P->ja[i][j] == (*rangtab)[i]+j);
	}

      /** The matrix should be still sorted **/
      for(j=1;j<P->nnzrow[i];j++)
	assert(P->ja[i][j] > P->ja[i][j-1]);
    }

  /*for(i=0;i<nn;i++)
    fprintfd(stderr, "%ld ", nodetab[i]);
    fprintfd(stderr, "\n");*/

#endif
  


  free(tmp);
  free(tmp2);
  free(sonindex);
  if(sontab != NULL)
    free(sontab);
  free(treetab2);
  free(colweight);
  
}



int merge_cost(int a, int b, csptr P, int *colweight)
{
  int i1, n1, i2, n2;
  dim_t *ja1, *ja2;
  int cost;

  ja1 = P->ja[a];
  ja2 = P->ja[b];


  n1 = P->nnzrow[a];
  n2 = P->nnzrow[b];

  i1 = i2 = 0;
  /** The diagonal elements of row a does not create fill-in **/ 
  while(i1 < n1 && ja1[i1] < ja2[0])
    i1++;


  /*fprintfd(stderr, "MERGECOST %ld (%ld) + %ld (%ld)  i1 = %ld \n", a, n1, b, n2, i1);*/

  cost = 0;
  while(i1 < n1 && i2 < n2)
    {
      if(ja1[i1] < ja2[i2])
	{
	  cost += colweight[b];
	  /*fprintfd(stderr, "TOTO cost1 %ld \n", cost);*/
	  i1++;
	  continue;
	}
      if(ja1[i1] > ja2[i2])
	{
	  cost += colweight[a];
	  /*fprintfd(stderr, "TOTO cost2 %ld \n", cost);*/
	  i2++;
	  continue;
	}

      /** ja1[i1] == ja2[i2] **/
      i1++;
      i2++;
    }

  while(i1 < n1)
    {
      cost += colweight[b];
      /*fprintfd(stderr, "TOTO costR1 %ld \n", cost);*/
      i1++;
      continue;
    }

  while(i2 < n2)
    {
      cost += colweight[a];
      /*fprintfd(stderr, "TOTO costR2 %ld \n", cost);*/
      i2++;
      continue;
    }
#ifdef DEBUG_M
  assert(cost >= 0);
#endif

  return cost;
}


void get_son(int node, int *sonindex, int *sontab, int *colweight, int *ns, int *list)
{
  dim_t i, s;
  int nss;
  int ind;
  ind = 0;
  for(i=sonindex[node];i<sonindex[node+1];i++)
    {
      s = sontab[i];
      if(colweight[s] <= 0)
	{
	  get_son(s, sonindex, sontab, colweight, &nss, list+ind);
	  ind += nss;
	}
      else
	list[ind++] = s;
    }
  *ns = ind;

}


void  merge_col(int a, int b, csptr P)
{
  int i, i1, i2;
  int n1, n2;
  dim_t *ja1, *ja2;
  dim_t *ja;
  

  ja1 = P->ja[a];
  ja2 = P->ja[b];
  n1 = P->nnzrow[a];
  n2 = P->nnzrow[b];


  ja = (dim_t *)malloc(sizeof(dim_t)*(n1+n2));

  i1 = 0;
  i2 = 0;
  i = 0;

  /*fprintfd(stderr, "MERGE %ld and %ld  \n", (long)a, (long)b);*/
  
  while(i1 < n1 && i2 < n2)
    {
      /*fprintfd(stderr, "i1 %ld i2 %ld n1 %ld n2 %ld \n", (long)i1, (long)i2, (long)n1, (long)n2);*/
      if(ja1[i1] < ja2[i2])
	{
	  ja[i] = ja1[i1];
	  i1++;
	  i++;
	  continue;
	}

      if(ja1[i1] > ja2[i2])
	{
	  ja[i] = ja2[i2];
	  i2++;
	  i++;
	  continue;
	}
      
      ja[i] = ja1[i1];
      i++;
      i1++;
      i2++;
    }
  /*fprintfd(stderr, "DONE LOPP \n");
    fprintfd(stderr, "DONE i1 %ld i2 %ld n1 %ld n2 %ld \n", (long)i1, (long)i2, (long)n1, (long)n2);*/

#ifdef DEBUG_M
  assert(i1 == n1 || i2 == n2);
#endif

  for(;i1<n1;i1++)
    ja[i++] = ja1[i1];

  for(;i2<n2;i2++)
    ja[i++] = ja2[i2];

  /*fprintfd(stderr, "E1 \n");*/

#ifdef DEBUG_M
  assert(i >= n1 && i >= n2);
  assert(i <= n1+n2);
#endif


  if(P->ja[a] != NULL)
    {
      P->nnzrow[a] = 0;
      free(P->ja[a]);
    }


  /*fprintfd(stderr, "E2: realloc i = %ld  \n", (long)i);*/

  if(P->ja[b] != NULL)
    free(P->ja[b]);
  P->nnzrow[b] = i;
  /*P->ja[b] = (int *)realloc(ja, sizeof(int)*i);*/
  P->ja[b] = (int *)malloc(sizeof(int)*i);
  memcpy(P->ja[b], ja, sizeof(int)*i);
  free(ja);

  /*fprintfd(stderr, "DONE2 i1 %ld i2 %ld n1 %ld n2 %ld \n", (long)i1, (long)i2, (long)n1, (long)n2);*/

}

#ifdef BLAS_GAIN
REAL merge_gain(int a, int b, csptr P, int *colweight, int *tmp)
{
  REAL costa, costb, costm;
  int nm;

  costa = cblk_time(P->nnzrow[a], P->ja[a], colweight[a]);
  costb = cblk_time(P->nnzrow[b], P->ja[b], colweight[b]);
  
  UnionSet(P->ja[a], P->nnzrow[a], P->ja[b], P->nnzrow[b], tmp, &nm);
  
  costm = cblk_time(nm, tmp, colweight[a] + colweight[b]);

  /*fprintfd(stderr, "Cost(%ld) = %g cost(%ld) = %g costm = %g \n", (long)a, costa, (long)b, costb, costm);*/
  
#ifdef DEBUG_M
  /*if(costm > costa + costb)
    fprintfd(stderr, "BLAS negative column %ld merge column %ld \n", (long)a, (long)b);*/
#endif

  return costm - costa - costb;

}


REAL cblk_time(int n, dim_t *ja, int colnbr) 
{
  /*******************************************/
  /* Compute the time to compute a cblk      */
  /* according to the BLAS modelization      */
  /*******************************************/
  REAL cost;
  dim_t i;
  int L, G, H;

  /** The formula are based on the costfunc.c in blend **/
  /** @@@Â OIMBE: il faudra faire les DOF_CONSTANT ***/

  /** Diagonal factorization and TRSM **/
  L = colnbr;
  G = n-L;
#define CHOLESKY
#ifndef CHOLESKY
  cost =(REAL)(L*PERF_COPY(L)+ PERF_PPF(L) + PERF_TRSM(L, G) + L*PERF_SCAL(G)
		  + L*PERF_COPY(G)); 
#else
  cost = (REAL)(PERF_POF(L) + PERF_TRSM(L, G)) ;
#endif

  /** Contributions **/
  i = colnbr;
  while(i<n)
    {
      H = 1;
      i++;
      while(i<n && ja[i] == ja[i-1]+1)
	{
	  i++;
	  H++;
	}
      
      cost += (REAL)(PERF_GEMM(G, H, L));
      G -= H;

    }
  
  return cost;
}
#endif

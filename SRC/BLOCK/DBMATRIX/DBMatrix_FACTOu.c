/* @authors J. GAIDAMOUR */

#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#include <assert.h>
#include "block.h"

#include "base.h"

#define MAX(x,y) (((x)<(y))?(y):(x))
#define MIN(x,y) (((x)>(y))?(y):(x))

/*TODO optim = access a coeftab multiple, les limiter*/
/* TODO : 2 blocknums */
/* idem pour bloknum, sans doute optimisable*/
/* voir les noms de variables, la maniÃ¨res dont elles sont utilisÃ©es ou rÃ©utilisÃ©es ... + commentaires */

/* TODO : calcul du bloc du mileu en double*/
/*TODO : remettre / / assert ds le cas direct*/

/* Faire une vrai facto right looking en dÃ©coupant les fonctions par blocs phidals */

void DBMatrix_sFACTO(int k, int cdim, 
		     DBMatrix* L, SolverMatrix* solvL, SymbolMatrix* symbmtx, COEF* lc, int p, int rdim, blas_t stride,
                     COEF* uc,
                     COEF* W);

#ifdef WITH_PASTIX
void DBMatrix_FACTOu_pastix(UDBMatrix* mLU) {
  SymbolMatrix *symbmtx;
  SolverMatrix *solvL, *solvU;
  DBMatrix *L, *U;
  int i, ilast;
  
  L=mLU->L;
  U=mLU->U;

  ilast = L->brj - L->tlj + 1;

  L->pastix_str = malloc(ilast*sizeof(pastix_struct));// on n'alloue que dans L, pas dans U

  for(i=0;i<ilast;i++) {

    solvL = L->ca[L->cia[i+L->tlj]]->solvmtx;     
    symbmtx = &L->ca[L->cia[i+L->tlj]]->symbmtx;     
    solvU = U->ca[U->cia[i+U->tlj]]->solvmtx;

    pastix_init(L->pastix_str+i, symbmtx); 
    pastix_factorization(L->pastix_str+i, solvL, solvU, symbmtx, 1);
    
  }
}

void check_equal(int r, double a, double b) {
  assert(fabs(a-b) < 1e-5);
  /*if(fabs(a-b) >= 1e-5)
    fprintf(stderr, "ERROR (r=%d) hips : %f ; pastix : %f\n", r, a, b);
  else
  fprintf(stderr, "OK(r=%d) %f\n", r,a);*/
}

void check_pastix_factorization(UDBMatrix *mLU) {

  SolverMatrix *solvL;
  SolverMatrix *solvU;
  SymbolMatrix *symbmtx;
  PastixSolverMatrix *solvPastix;
  int i, ilast, r;
  COEF *ptrL, *ptrU;
  pastix_float_t *ptrPastixL, *ptrPastixU;
  int first_cblok, k, m, bloknum, numline, numcol, stride, stridePastix;
  DBMatrix* L=mLU->L;
  DBMatrix* U=mLU->U;

  ilast = L->brj - L->tlj + 1;
 
  for(r=0;r<ilast;r++) {
    solvL = L->ca[L->cia[r+L->tlj]]->solvmtx;    
    solvU = U->ca[U->cia[r+U->tlj]]->solvmtx; 
    symbmtx = &L->ca[L->cia[r+L->tlj]]->symbmtx;     
    solvPastix = &((L->pastix_str+r)->pastix_data->solvmatr);
  
    ptrL = solvL->coeftab + solvL->bloktab[symbmtx->bcblktab[0].fbloknum].coefind ;  
    ptrU = solvU->coeftab + solvU->bloktab[symbmtx->bcblktab[0].fbloknum].coefind ;  
    ptrPastixL = solvPastix->coeftab + solvPastix->bloktab[0].coefind;
    ptrPastixU = solvPastix->ucoeftab + solvPastix->bloktab[0].coefind;
  
    first_cblok = symbmtx->facedecal;
  
    for(k = 0; k < symbmtx->cblknbr; k++) 
      {
	stride = symbmtx->stride[k];
	stridePastix = solvPastix->cblktab[k].stride;

	for(m = 0; m < symbmtx->ccblktab[k].lcolnum - symbmtx->ccblktab[k].fcolnum + 1; m++) {
	  bloknum = symbmtx->bcblktab[k].fbloknum;	
	  numline = symbmtx->bloktab[bloknum].frownum;
	  numcol = symbmtx->ccblktab[k].fcolnum + m;
	  
	  for(i=0; i<stride; i++) {	  
	    if(bloknum == symbmtx->bcblktab[k].fbloknum) {
	      if(numline > numcol) 
		check_equal(r,ptrL[i], ptrPastixL[i]);
	      else
		check_equal(r,ptrU[(i-m)*stride+m], ptrPastixU[(i-m)*stridePastix+m]);
	    }
	    else {	 
	      check_equal(r,ptrL[i],ptrPastixL[i]);
	      check_equal(r,ptrU[i],ptrPastixU[i]);
	    }
	      
	    if(numline == symbmtx->bloktab[bloknum].lrownum) {
	      bloknum++;
	      numline = symbmtx->bloktab[bloknum].frownum;
	    }
	    else
	      numline++;
	  }
	  
	  ptrL+=stride;
	  ptrU+=stride;
	  ptrPastixL += stridePastix;
	  ptrPastixU += stridePastix;

	}
      }
  }
}
#endif // WITH_PASTIX

void DBMatrix_FACTOu(UDBMatrix* mLU) 
{
  DBMatrix* L=mLU->L;
  DBMatrix* U=mLU->U;
  
  /*************************************************************/
  /* This function performs in place the L.U factorization of  */
  /* a symmetric sparse matrix structured in dense blocks of   */
  /* coefficients                                              */
  /*************************************************************/
  SymbolMatrix *symbmtx;
  SolverMatrix *solvL;
  SolverMatrix *solvU;

  int i, ilast;
  dim_t p, k;
  COEF *W; /** buffers de travail **/

  COEF one = 1.0;
  char /* *sideL  = "L", */ *sideR  = "R";
  char /* *uploU  = "U", */ *uploL  = "L";
  char /* *transN = "N", */ *transT = "T";
  char *diagN  = "N", *diagU  = "U";

  blas_t stride;
  int cdim, rdim;
  COEF *ccL, *ccU; 
  COEF *bc, *lc, *uc; 

  assert((L->alloc == ONE) || (L->alloc == CBLK));
  assert((U->alloc == ONE) || (U->alloc == RBLK));

  /*** Allocation de buffers de travail ***/
  W = (COEF *)malloc(sizeof(COEF)*U->coefmax);
  assert(W != NULL);

  if (L->alloc == ONE) {
    ilast = 1;
  } else ilast = L->brj - L->tlj + 1;

  for(i=0;i<ilast;i++) {
    solvL = &L->a[i];     
    symbmtx = &solvL->symbmtx;     
    solvU = &U->a[i];    

    for(k=0;k<symbmtx->cblknbr;k++) {
      cdim   = symbmtx->ccblktab[k].lcolnum - symbmtx->ccblktab[k].fcolnum + 1; /** Largeur du bloc colonne **/
      stride = symbmtx->stride[k];

      /**********************************/
      /* Factorisation du bloc diagonal */
      /**********************************/
      /** Calcul du pointeur dans coeftab vers le  dÃÂ©but des coefficients du bloc diagonal **/ 
      p   = symbmtx->bcblktab[k].fbloknum;
      ccL = solvL->coeftab + solvL->bloktab[p].coefind;
      ccU = solvU->coeftab + solvU->bloktab[p].coefind;
      
      LU(cdim, ccL, stride, ccU, stride, EPSILON);
      
      /* TODO : voir desc2.c */      
      if ((i == ilast-1) && (k == symbmtx->cblknbr-1))
	continue;  /** Pas de bloc extradiagonal le calcul est terminer **/

      /************************************************************/
      /* "Diviser" les blocs extra diagonaux par le bloc diagonal */
      /************************************************************/
      {
	/** On effectue l'operation : M = M.(L^-1)t  avec M = l'ensemble compacte des blocs extra-diagonaux du bloc colonne **/

	p = symbmtx->bcblktab[k].fbloknum+1; /** Indice du premier bloc extra-diagonal **/
	if (symbmtx->bcblktab[k].fbloknum == symbmtx->bcblktab[k].lbloknum)
	  continue; /*todo : voir le continue prÃ©cÃ©dent */
	
	/* * * */ /** ==> L21 = A21 . (U11)^-1 **/
	/** Calcul du pointeur dans coeftab vers le  dÃÂ©but des coefficients du premier bloc extra diagonal **/ 

	bc = solvL->coeftab + solvL->bloktab[p].coefind;

	rdim = symbmtx->hdim[k] - cdim;  /** Hauteur de la surface compactÃÂ©e des bloc extra-diagonaux **/
	BLAS_TRSM(sideR,  uploL, transT, diagN, rdim, cdim, one, ccU, stride, bc, stride);
	/* * * */

	/* * * */ /** ==> U12 = (L11)^-1 . A12 **/
	/** Calcul du pointeur dans coeftab vers le dÃÂ©but des coefficients du premier bloc extra diagonal **/ 
	bc = solvU->coeftab + solvU->bloktab[p].coefind;
	BLAS_TRSM(sideR, uploL, transT, diagU, rdim, cdim, one, ccL, stride, bc, stride);
	/* * * */
      } 

      /** Boucle sur les blocs extra-diagonaux **/
      /** On l'initialise ÃÂ  la heuteur totale des bloc extra diagonaux **/

      for(;p<=symbmtx->bcblktab[k].lbloknum; p++)
	{
	  /** Calcul du pointeur dans coeftab vers le dÃÂ©but des coefficients du bloc extra-diagonal p **/ 
	  lc = solvL->coeftab + solvL->bloktab[p].coefind;
	  uc = solvU->coeftab + solvL->bloktab[p].coefind;

	  /* sur L */
	  DBMatrix_sFACTO(k, cdim, 
			  L, solvL, symbmtx, lc, p, rdim, stride,
			  uc,			    
			  W); /* TODO : supprimer des arguments et les "recalculer" dans la fonction ? */
	    
	  /* sur U */
	  DBMatrix_sFACTO(k, cdim,
			  U, solvU, symbmtx, uc, p, rdim, stride,
			  lc, 
			  W);

	  /** Mettre ÃÂ jour la hauteur des bloc extra diagonaux qui restent **/
	  rdim -= symbmtx->bloktab[p].lrownum - symbmtx->bloktab[p].frownum+1;
	} /* p */
	
	
    } /* k */
  } /* i */


  /** LibÃÂ©ration des buffers **/
  free(W);
}

void DBMatrix_sFACTO(int k, int cdim, 
		     DBMatrix* L, SolverMatrix* solvL, SymbolMatrix* symbmtx, COEF* lc, int p, int rdim, blas_t stride,
		     COEF* uc,
		     COEF* W){

  dim_t q, m;

  COEF *bc, *wc; 
  int hdim, mdim;

  SolverMatrix* facesolvmtx;
  SymbolMatrix* facesymbmtx;
  int facestride;
  int bloknum, facecblknum;
  int decalcol;
  int decalrow;

  int UN=1;
  COEF minusone = -1.0, zero = 0.0, one = 1.0;
  char *transN = "N", *transT = "T";

  /** Calcul de la hauteur du bloc extra diagonal p **/
  hdim = symbmtx->bloktab[p].lrownum - symbmtx->bloktab[p].frownum+1;

  /* assert(stride*hdim <= L->coefmax); */
       
  /*** Stocker dans W le produit des blocs {A(q,k), q>=p} avec Ft(p,k) ***/
  /*   printfv(5, "%d rdim=%d hdim=%d cdim=%d stride=%d stride=%d\n", ii, rdim, hdim, cdim, stride, stride); ii++; */
  BLAS_GEMM(transN, transT, rdim, hdim, cdim, one, lc, stride, uc, stride, zero, W, stride);

  /****************************************************************************************/
  /** Pour tout les blocs extra-diagonaux {A(j,k), j>= i} faire A(i,j) = A(i,j) - W(i, ) **/
  /****************************************************************************************/

  if (L->alloc == ONE) {
    facesolvmtx = L->a;
    facesymbmtx = &facesolvmtx->symbmtx;
  } else /* if (L->alloc == CBLK) */ { /** a simplifier */
    facecblknum = symbmtx->bloktab[p].cblknum; /** Indice du bloc colonne en face du bloc extra-diagonal p **/
    facecblknum -= L->a[0].symbmtx.facedecal; /* evitable en conservant le vecteur de depart, avec tlj brj*/ 
    /* TODO L ou U  : L permet de virer un paramÃ¨tre ...*/
    /*pour B assert(L->cblktosolvL[0]->symbmtx.facedecal == 0);*/

    facesolvmtx = L->cblktosolvmtx[facecblknum];
    facesymbmtx = &facesolvmtx->symbmtx;
  }
	    
  facecblknum = symbmtx->bloktab[p].cblknum - facesymbmtx->facedecal; 

  /** Stride du cblk en face du bloc extra-diagonal p **/
  facestride = facesymbmtx->stride[facecblknum];

  /** Nombre de colonne "ÃÂ  gauche" de la zone modifiÃÂ©e dans le bloc colonne facecblknum **/
  decalcol = symbmtx->bloktab[p].frownum - facesymbmtx->ccblktab[facecblknum].fcolnum;

  /*** Largueur de la zone modifiÃÂ©e == hauteur du bloc p dans le bloc colonne k **/
  mdim = symbmtx->bloktab[p].lrownum - symbmtx->bloktab[p].frownum+1;

  bloknum = facesymbmtx->bcblktab[facecblknum].fbloknum; /** Indice du bloc diagonal du bloc colonne facebloknum **/

  for(q=p;q<=symbmtx->bcblktab[k].lbloknum;q++) /** Pour tous les bloc extradiagonaux A(q,k) avec q >= p **/
    {

      if (bloknum >= facesymbmtx->bcblktab[facecblknum].lbloknum+1/*todo le +1*/) goto fin; /* utile ici ? */

      /* skip block that cannot match */
      while(symbmtx->bloktab[q].frownum > facesymbmtx->bloktab[bloknum].lrownum) {
	bloknum++;
	if (bloknum >= facesymbmtx->bcblktab[facecblknum].lbloknum+1/*todo le +1*/) goto fin;
      }
	      
      /* for every block that match */
      while(symbmtx->bloktab[q].lrownum >= facesymbmtx->bloktab[bloknum].frownum) {

	/* assert ds le cas direct*/
	/*		assert(symbmtx->bloktab[bloknum].frownum <= symbmtx->bloktab[q].frownum);*/
	/*		assert(symbmtx->bloktab[bloknum].lrownum >= symbmtx->bloktab[q].lrownum);*/
		
	decalrow = symbmtx->bloktab[q].frownum - facesymbmtx->bloktab[bloknum].frownum;
		
	/** Calcul du pointeur de debut de la zone modifiÃÂ©e dans le bloc A(bloknum, facebloknum) **/
	bc = facesolvmtx->coeftab + facesolvmtx->bloktab[bloknum].coefind + decalcol*facestride + MAX(0,decalrow);
	      
	/** Calcul du pointeur de debut du bloc correspondant ÃÂ  A(q,k) dans W **/
	wc = W + solvL->bloktab[q].coefind - solvL->bloktab[p].coefind + MAX(0,-decalrow);
		
	/*** Update de la contribution ***/
	hdim = MIN(symbmtx->bloktab[q].lrownum, facesymbmtx->bloktab[bloknum].lrownum) - 
	  MAX(symbmtx->bloktab[q].frownum, facesymbmtx->bloktab[bloknum].frownum) +1; /** Hauteur du bloc sur lequel on effectue la diffÃ©rence **/

	/* assert cas direct */
	/* assert(hdim == symbmtx->bloktab[q].lrownum -  symbmtx->bloktab[q].frownum +1); */

	for(m=0; m < mdim; m++)
	  {
	    BLAS_AXPY(hdim, minusone, wc, UN, bc, UN);
	    wc += stride;
	    bc += facestride;
	  }
		
	bloknum++;
	if (bloknum >= facesymbmtx->bcblktab[facecblknum].lbloknum+1/*todo +1*/) break;
      }
	      
    fin:
      bloknum--;
    }

}

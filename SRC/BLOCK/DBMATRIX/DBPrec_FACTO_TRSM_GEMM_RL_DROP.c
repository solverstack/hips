/* @authors J. GAIDAMOUR */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>

#include "phidal_sequential.h"
#include "block.h"
#include "prec.h"

#define INAROW 1 /* optim : 1 */

void CMDu(_DBDistrPrec,FACTO_TRSM_DROP)(_DBDistrPrec* P, _PhidalDistrPrec* phidalPrecL0,
					PhidalOptions* option, REAL *droptab, 
					flag_t fill, _PhidalDistrHID* _DBL);

void CMDu(_DBDistrPrec,FACTO_TRSM_GEMM_RL_DROPE)(_DBDistrPrec* P, _PhidalDistrPrec* phidalPrecL0, flag_t fillE, PhidalOptions* option, _PhidalDistrHID* _DBL, chrono_t* ttotal, DBPrecMem* mem) {
  chrono_t t1, t2;
  int_t levelnum = 0;

  t1  = dwalltime(); 
    
  /* phidalPrecL0 is prec for level 0 here */
  CMD(_PhidalDistrPrec,Init)(phidalPrecL0);
      
  phidalPrecL0->symmetric = P->symmetric; 
  phidalPrecL0->dim = S(PREC_E(P))->dim1;
  phidalPrecL0->levelnum = levelnum;
  phidalPrecL0->forwardlev = option->forwardlev -1;
  phidalPrecL0->schur_method = option->schur_method;
  phidalPrecL0->pivoting = 0;  /** 1 := column pivoting in ILUTP */
  /* phidalPrecL0->tol_schur; /\* non utilisÃÂ© ?*\/ */
  
  M_MALLOC(phidalPrecL0->EF, D(SCAL));
  PREC_E_SCAL(phidalPrecL0) = (_PhidalDistrMatrix*)malloc(sizeof(_PhidalDistrMatrix));
#ifdef UNSYMMETRIC
  PREC_F_SCAL(phidalPrecL0) = (_PhidalDistrMatrix*)malloc(sizeof(_PhidalDistrMatrix));
#endif

  CMDu(_DBDistrPrec,FACTO_TRSM_DROP)(P, phidalPrecL0, option, NULL/* droptab */, fillE, _DBL);

  CMD(_DBPrecMem,SetDropE)(phidalPrecL0, mem); /* OK */

  t2  = dwalltime(); *ttotal += t2-t1;
  
  DUP(HDIM_tmp_apres, S(PREC_EDB(P)), S(PREC_FDB(P)));

#ifdef PARALLEL
  DBDistrMatrix_Clean(PREC_EDB(P));
#else
  DBMatrix_Clean(PREC_EDB(P));
#endif
  free(PREC_EDB(P));
  PREC_EDB(P)=NULL; /* useful ? */
  
#ifdef UNSYMMETRIC

/* #ifdef PARALLEL */
/* /\*   DBMatrix_Clean(PREC_FDB(P)); *\/ */
/*   /\* non DBDistrMatrix_Clean(PREC_FDB(P)); *\/ */
/* #else */
/* /\*   DBMatrix_Clean(PREC_FDB(P)); *\/ */
/* #endif */

  if(S(PREC_FDB(P))->a != NULL)
    free(S(PREC_FDB(P))->a);
  if(S(PREC_FDB(P))->bloktab != NULL)
    free(S(PREC_FDB(P))->bloktab);
  if(S(PREC_FDB(P))->ra != NULL)
    free(S(PREC_FDB(P))->ra);
  if(S(PREC_FDB(P))->ca != NULL)
    free(S(PREC_FDB(P))->ca);

  free(PREC_FDB(P));
  PREC_FDB(P)=NULL;
#endif

}


#ifdef SYMMETRIC
void CMD(_DBDistrPrec,FACTO_TRSM_DROP)(_DBDistrPrec* P, _PhidalDistrPrec* phidalPrecL0,
				       PhidalOptions* option, REAL *droptab, 
                                       flag_t fill, _PhidalDistrHID* _DBL)
{
  DBMatrix* L = S(PREC_L_BLOCK(P));
  DBMatrix* E = S(PREC_EDB(P));
  PhidalMatrix* PhidalEU = S(PREC_E(phidalPrecL0));
  PhidalMatrix* PhidalE = S(PREC_E(P));
  int_t locally_nbr = option->locally_nbr;

  dim_t i,j;
  VSolverMatrix* csL;

  int coefmax;  
  COEF *tmpF, *tmpW;
  
  int Emax;
  COEF* coeftabE;

  SolverMatrix* solvE;

  REAL *dropptr;

  chrono_t t1, t2;  
  REAL tfacto=0, ttrsm=0, tconvert=0;

  /* Allocation des vecteurs temporaires */
  coefmax = MAX(E->coefmax, L->coefmax);

  tmpF = (COEF *)malloc(sizeof(COEF)*coefmax);
  assert(tmpF != NULL);

  tmpW = (COEF *)malloc(sizeof(COEF)*coefmax);
  assert(tmpW != NULL);

  /* TODO : pouvoir utiliser l'autre version aussi (rÃÂ©duction pic ? )*/
  Emax = calc_Emax(E);
  coeftabE = (COEF*)malloc(sizeof(COEF)*Emax);

  PrecInfo_AddNNZ_(P->info, Emax, "Emax");

  t1  = dwalltime(); 
#ifndef PARALLEL
  PhidalMatrix_Init_fromDB(PREC_E_BLOCK(P), PhidalEU, "N", locally_nbr, BL);
#else 
  PhidalDistrMatrix_Init_fromDBDistr(PREC_E_BLOCK(P), PREC_E(phidalPrecL0), "N", locally_nbr, _DBL);
#endif
  t2  = dwalltime(); tconvert += t2 - t1;

#ifdef WITH_PASTIX
  if(option->use_pastix == 1)
    L->pastix_str = malloc((L->brj-L->tlj+1)*sizeof(pastix_struct));
#endif // WITH_PASTIX

  for(i=L->tlj;i<=L->brj;i++)
    {

      /* FACTO */
      csL =  L->ca[ L->cia[i] ];

      t1  = dwalltime();

#ifdef WITH_PASTIX
      if(option->use_pastix == 1)
          VS_ICCT_pastix(L->pastix_str+(i-L->tlj), csL, tmpF, tmpW);
      else
#endif // WITH_PASTIX
          VS_ICCT(csL, tmpF, tmpW);

      t2  = dwalltime(); tfacto += t2 - t1;

      /* TRSM */
      t1  = dwalltime(); 
      /* ici, assert(i == i-L->tli */
      /* if ((E->alloc == CBLK) && (fill == INGEMM)) */
      solvE = &E->a[i];

      DBMatrix_Symb2SolvMtx_(solvE, coeftabE);
      DBMatrix_AllocSymbmtx(i, i, E, CBLK, 
			    E->cia, E->cja, E->ca, 
			    PhidalE->cia, PhidalE->cja, PhidalE->ca, 
			    solvE, "N", LHID(_DBL)); /* TODO : N ? */
      /*     } */
      /*     } */

      t1  = dwalltime(); 
      if (/* (E->alloc == ONE) TODO! || */ (E->alloc == CBLK))
	{

	  E->a[i].coefmax = E->coefmax; /*fix*/
	  VS2_InvLT(1, csL->solvmtx, &csL->symbmtx, &E->a[i], &E->a[i].symbmtx, tmpW);
	  
	} else {

	  for(j=E->cia[i];j<E->cia[i+1];j++) {
	    E->ca[j]->solvmtx->coefmax = E->coefmax; /*fix*/
	    VS_InvLT(1, csL, E->ca[j], tmpW);
	  }

	}
      t2  = dwalltime(); ttrsm += t2 - t1;

      if(droptab != NULL)
	dropptr = droptab + (LHID(_DBL)->block_index[i] - LHID(_DBL)->block_index[PhidalEU->tli]);
      else
	dropptr = NULL;

      t1  = dwalltime(); 
      for(j=E->cia[i];j<E->cia[i+1];j++) {
	
	initCS(PhidalEU->ca[j], E->ca[j]->symbmtx.nodenbr);
	if (solvE->coefnbr > 0)
	  VSolverMatrix2SparRowDrop(solvE, &E->ca[j]->symbmtx, PhidalEU->ca[j], INAROW, 0, 0, option->droptolE); 
	
	/* free */
	SymbolMatrix_Clean(&E->ca[j]->symbmtx); /* symbmtx in L->bloktab */

	/* CS_DropT(PhidalEU->ca[j], option->droptolE, dropptr); 	  */
      }

      /* free */
      t2  = dwalltime(); tconvert += t2 - t1;

      /* if ((E->alloc == CBLK) && (fill == INGEMM)) */
      solvE->coeftab = NULL; /* don't desallocate coeftabE */
      SolverMatrix_Clean(solvE);
      
    }

  /* if ((E->alloc == CBLK) && (fill == INGEMM)) */
  { /* do a function <=> DBMatrix2SolverMatrix */
    DBMatrix* L = E;
    free(L->bloktab); L->bloktab = NULL; /* has been freed */
    free(L->a); L->a = NULL;             /* has been freed */

    /* maintenant non récup ! */
    free(L->cia);
    free(L->cja);
    free(L->ria);
    free(L->rja);
    
    L->cia = NULL;
    L->cja = NULL;
    L->ria = NULL;
    L->rja = NULL;

    /* DBMatrix_Clean(L); done in DBMATRIX_MLICCREAL */ 
  }

  PrecInfo_AddNNZ_(P->info, PhidalMatrix_NNZ(PhidalEU), "PhidalEU");
  
  free(coeftabE);
  PrecInfo_SubNNZ_(P->info, Emax, "Emax");

  free(tmpF);
  free(tmpW);
  
  fprintfv(5, stdout, "  M : Numeric Factorisation in %g seconds\n\n", tfacto);
  fprintfv(5, stdout, "  TRSM in %g seconds\n\n", ttrsm);
  fprintfv(5, stdout, "  Convert DB2Phidal in %g seconds\n\n", tconvert);

#ifdef OLDCOUNT
  PrecInfo_SubNNZ_(P->info, PhidalMatrix_NNZ(PhidalEU), "PhidalEU");
#endif

}

		 
#else

void CMDu(_DBDistrPrec,FACTO_TRSM_DROP)(_DBDistrPrec* P, _PhidalDistrPrec* phidalPrecL0,
                                        PhidalOptions* option, REAL *droptab, 
                                        flag_t fill, _PhidalDistrHID* _DBL) {
  DBMatrix* L = S(PREC_L_BLOCK(P));
  DBMatrix* U = S(PREC_U_BLOCK(P));
  DBMatrix* E = S(PREC_EDB(P));
  DBMatrix* F = S(PREC_FDB(P));
  PhidalMatrix*  PhidalEU = S(PREC_E(phidalPrecL0));
  PhidalMatrix*  PhidalLF = S(PREC_F(phidalPrecL0));
  PhidalMatrix*  PhidalE = S(PREC_E(P));
  PhidalMatrix*  PhidalF = S(PREC_F(P));
  int_t locally_nbr = option->locally_nbr;

  dim_t i,j;
  VSolverMatrix* csL, *csU;

  int coefmax;
  COEF *tmpF, *tmpW;
  
  int Emax;
  COEF* coeftabE, *coeftabF;

  SolverMatrix* solvE, * solvF;

  REAL *dropptr;

  chrono_t t1, t2;
  REAL tfacto=0, ttrsm=0, tconvert=0;

  /* Allocation des vecteurs temporaires */
  coefmax = MAX(E->coefmax, L->coefmax);

  tmpF = (COEF *)malloc(sizeof(COEF)*coefmax);
  assert(tmpF != NULL);

  tmpW = (COEF *)malloc(sizeof(COEF)*coefmax);
  assert(tmpW != NULL);

  /* TODO : pouvoir utiliser l'autre version aussi (rÃÂ©duction pic ? )*/
  Emax = calc_Emax(E);
  coeftabE = (COEF*)malloc(sizeof(COEF)*Emax);
  coeftabF = (COEF*)malloc(sizeof(COEF)*Emax);

  PrecInfo_AddNNZ_(P->info, 2*Emax, "Emax (*2)");

  t1  = dwalltime();
#ifdef PARALLEL
  UDBMatrix ARG; ARG.L=S(PREC_L_BLOCK(P)); ARG.U=S(PREC_U_BLOCK(P));

#ifdef WITH_PASTIX
  if(option->use_pastix == 1) {
    DBMatrix_FACTOu_pastix(&ARG);
    DBMatrix_FACTOu(&ARG);
    //check_pastix_factorization(&ARG);
  }
  else
#endif // WITH_PASTIX
    DBMatrix_FACTOu(&ARG);

#else

#ifdef WITH_PASTIX
  if(option->use_pastix == 1) {
    DBMatrix_FACTOu_pastix(&PREC_LU_BLOCK(P));
    DBMatrix_FACTOu(&PREC_LU_BLOCK(P));
    //check_pastix_factorization(&PREC_LU_BLOCK(P));
  }
  else
#endif // WITH_PASTIX
    DBMatrix_FACTOu(&PREC_LU_BLOCK(P));
#endif
  t2  = dwalltime(); tfacto += t2 - t1;

  t1  = dwalltime();

#ifndef PARALLEL
  PhidalMatrix_Init_fromDB(E, PhidalEU, "N", locally_nbr, BL);
  PhidalMatrix_Init_fromDB(F, PhidalLF, "N", locally_nbr, BL);
#else 
  PhidalDistrMatrix_Init_fromDBDistr(PREC_EDB(P), PREC_E(phidalPrecL0), "N", locally_nbr, _DBL);
  PhidalDistrMatrix_Init_fromDBDistr(PREC_FDB(P), PREC_F(phidalPrecL0), "N", locally_nbr, _DBL);
#endif

  t2  = dwalltime(); tconvert += t2 - t1;

  for(i=L->tlj;i<=L->brj;i++)
    {

      /* FACTO */
      csL =  L->ca[ L->cia[i] ];
      csU =  U->ca[ U->cia[i] ];

      if(droptab != NULL)
	dropptr = droptab + (LHID(_DBL)->block_index[i] - LHID(_DBL)->block_index[PhidalEU->tli]);
      else
	dropptr = NULL;

      assert(L->cia == U->ria); /* TODO : ÃÂ  voir ... */
      assert(L->ria == U->cia);

      /* t1  = dwalltime();  */
      /* VS_ICCT(csL, tmpF, tmpW); */
      /* t2  = dwalltime(); tfacto += t2 - t1; */

      /* TRSM */
      t1  = dwalltime(); 
      /* if ((E->alloc == CBLK) && (fill == INGEMM)) */
      solvE = &E->a[i];

      DBMatrix_Symb2SolvMtx_(solvE, coeftabE);
      DBMatrix_AllocSymbmtx(i, i, E, CBLK,
			    E->cia, E->cja, E->ca,
			    PhidalE->cia, PhidalE->cja, PhidalE->ca,
			    solvE, "T", LHID(_DBL));

      solvF = &F->a[i];

      DBMatrix_Symb2SolvMtx_(solvF, coeftabF);
      DBMatrix_AllocSymbmtx(i, i, F, RBLK, 
			    F->ria, F->rja, F->ra, 
			    PhidalF->ria, PhidalF->rja, PhidalF->ra, 
			    solvF, "N", LHID(_DBL));

      /*     } */
      /*     } */

      if (/* (E->alloc == ONE) TODO! || */ (E->alloc == CBLK))
	{
	  E->a[i].coefmax = E->coefmax; /*fix*/
	  F->a[i].coefmax = E->coefmax; /*fix*/
	  VS2_InvLT(0, csU->solvmtx, &csU->symbmtx, &E->a[i], &E->a[i].symbmtx, tmpW);
	  VS2_InvLT(1, csL->solvmtx, &csL->symbmtx, &F->a[i], &F->a[i].symbmtx, tmpW);
	} else {
	  
	  for(j=E->cia[i];j<E->cia[i+1];j++) {
	    E->ca[j]->solvmtx->coefmax = E->coefmax; /*fix*/
	    VS_InvLT(0, csU, E->ca[j], tmpW);
	  }

	  for(j=F->ria[i];j<F->ria[i+1];j++) {
	    F->ra[j]->solvmtx->coefmax = E->coefmax; /*fix*/
	    VS_InvLT(1, csL, F->ra[j], tmpW);
	  }
	  
	}
      t2  = dwalltime(); ttrsm += t2 - t1;

      t1  = dwalltime(); /* TODO : encore + entrelacer en unsym */
      for(j=E->cia[i];j<E->cia[i+1];j++) {
	
	initCS(PhidalEU->ca[j], E->ca[j]->symbmtx.nodenbr);
	if (solvE->coefnbr > 0) 
	  VSolverMatrix2SparRowDrop(solvE, &E->ca[j]->symbmtx, PhidalEU->ca[j], INAROW, 0, 0, option->droptolE);

	/* CS_DropT(PhidalEU->ca[j], option->droptolE, dropptr); */

	initCS(PhidalLF->ra[j], F->ra[j]->symbmtx.nodenbr);
	if (solvF->coefnbr > 0)
	  VSolverMatrix2SparRowDrop(solvF, &F->ra[j]->symbmtx, PhidalLF->ra[j], INAROW, 0, 0, option->droptolE); 

	/* CS_DropT(PhidalLF->ra[j], option->droptolE, dropptr); */

	/* free */
	SymbolMatrix_Clean(&E->ca[j]->symbmtx); /* symbmtx in L->bloktab */

      }

      t2  = dwalltime(); tconvert += t2 - t1;

      /* if ((E->alloc == CBLK) && (fill == INGEMM)) */
      solvE->coeftab = NULL; /* don't desallocate coeftabE */
      SolverMatrix_Clean(solvE);
      
      solvF->coeftab = NULL; /* don't desallocate coeftabF */

    }

  /* if ((E->alloc == CBLK) && (fill == INGEMM)) */
  { /* do a function <=> DBMatrix2SolverMatrix */
    DBMatrix* L = E;
    free(L->bloktab); L->bloktab = NULL; /* has been freed */
    free(L->a); L->a = NULL;             /* has been freed */

    /* maintenant non récup ! */
    free(L->cia);
    free(L->cja);
    free(L->ria);
    free(L->rja);

    L->cia = NULL;
    L->cja = NULL;
    L->ria = NULL;
    L->rja = NULL;

    /* DBMatrix_Clean(L); done in DBMATRIX_MLICCREAL */ 
  }
  { /* do a function <=> DBMatrix2SolverMatrix */
    DBMatrix* L = F;
    free(L->bloktab); L->bloktab = NULL; /* has been freed */
    free(L->a); L->a = NULL;             /* has been freed */

    /* maintenant non récup ! */
/*     free(L->cia); */
/*     free(L->cja); */
/*     free(L->ria); */
/*     free(L->rja); */
    
    L->cia = NULL;
    L->cja = NULL;
    L->ria = NULL;
    L->rja = NULL;

    /* DBMatrix_Clean(L); done in DBMATRIX_MLICCREAL */ 
  }

  PrecInfo_AddNNZ_(P->info, PhidalMatrix_NNZ(PhidalEU), "PhidalEU");
  PrecInfo_AddNNZ_(P->info, PhidalMatrix_NNZ(PhidalLF), "PhidalLF");

  free(coeftabE);
  free(coeftabF);
  PrecInfo_SubNNZ_(P->info, 2*Emax, "Emax *2");

  free(tmpF);
  free(tmpW);
  
  /* E */
  PhidalMatrix_Transpose(PhidalEU);
  PhidalMatrix_Transpose_SparMat(PhidalEU, /*job*/1, "N", LHID(_DBL));
  PhidalMatrix_Transpose(PhidalEU);


  /* for(i=PhidalEU->tlj;i<=PhidalEU->brj;i++) */
/*     { */
/*       for(j=PhidalEU->cia[i];j<PhidalEU->cia[i+1];j++) { */
	
/* 	CS_DropT(PhidalEU->ca[j], option->droptolE, dropptr); /\* a retourner avant ? *\/ */
/*       } */
/*     } */
  
  fprintfv(5, stdout, "  M : Numeric Factorisation in %g seconds\n\n", tfacto);
  fprintfv(5, stdout, "  TRSM in %g seconds\n\n", ttrsm);
  fprintfv(5, stdout, "  Convert DB2Phidal in %g seconds\n\n", tconvert);

#ifdef OLDCOUNT
  PrecInfo_SubNNZ_(P->info, PhidalMatrix_NNZ(PhidalEU), "PhidalEU");
  PrecInfo_SubNNZ_(P->info, PhidalMatrix_NNZ(PhidalLF), "PhidalLF");
#endif


}

#endif

/* @authors P. HENON */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "phidal_struct.h" /*utile ?*/
#include "block.h"
#include "symbol.h" /*MatrixStruct*/
#include "solver.h"
#include "phidal_sequential.h"



void SF_Direct(csptr A, dim_t cblknbr, dim_t *rangtab, dim_t *treetab, csptr P)
{
  /********************************************************/
  /* This function computes the direct factor nnz pattern */
  /* of a matrix A given the supernode partition          */
  /********************************************************/
  dim_t i,j,k;
  int ind, nnznbr, father;
  int *tmpj, *tmp, *tmp2;
  int *ja;
  int *node2cblk;

  tmpj = (int *)malloc(sizeof(int)*A->n);
  tmp = (int *)malloc(sizeof(int)*A->n);
  node2cblk = (int *)malloc(sizeof(int)*A->n);

#ifdef DEBUG_M
  assert(rangtab[cblknbr] <= A->n);
#endif

  for(k=0;k<cblknbr;k++)
    for(i=rangtab[k];i<rangtab[k+1];i++)
      node2cblk[i] = k;

  /*** if we do a partial symbolic factorization then
       rangtab[cblknbr]<n, we complete the node2cblk vector 
       with high value to mark the nodes that do not face a cblk
       in the part of the matrix we treat ***/
  for(i=rangtab[cblknbr];i<A->n;i++)
    node2cblk[i] = cblknbr+1;
       


  /** Compute the nnz structure of each supernode in A **/
  initCS(P, cblknbr);

  for(k=0;k<cblknbr;k++)
    {
      ind = rangtab[k];

      /** Put the diagonal elements (A does not contains them) **/
      j = 0;
      for(i=rangtab[k];i<rangtab[k+1];i++)
	tmpj[j++] = i; 
      nnznbr = j;

      for(i=rangtab[k];i<rangtab[k+1];i++)
      {
	j = 0;
	while(j<A->nnzrow[i] && A->ja[i][j] <= i)
	  j++;

	/** Realise la fusion de 2 listes triees croissantes **/
	UnionSet(tmpj, nnznbr, A->ja[i]+j, A->nnzrow[i]-j, tmp, &ind);
	
	/** echange tmpj et le resultat de la fusion **/
	nnznbr = ind;
	tmp2 = tmpj;
        tmpj = tmp;
	tmp  = tmp2;
      }
#ifdef DEBUG_M
      ind = 0;
      for(j=rangtab[k]; j < rangtab[k+1];j++)
	assert(tmpj[ind++] == j);
#endif

#ifdef DEBUG_M
      assert(nnznbr > 0);
#endif
      P->nnzrow[k] = nnznbr;
      P->ja[k] = (int *)malloc(sizeof(int)*nnznbr);
      memcpy(P->ja[k], tmpj, sizeof(int)*nnznbr);
      P->ma[k]=NULL;
    }

 
  

  
  P->n = cblknbr;

  /** Compute the symbolic factorization **/
  for(k=0;k<cblknbr;k++)
    {
      father = treetab[k];
      ja = P->ja[k];
      i = 0;
      while(i < P->nnzrow[k] && node2cblk[ja[i]] <= k)
	i++;

      if(father != k && father > 0)
	{
	  UnionSet(P->ja[k]+i, P->nnzrow[k]-i, P->ja[father], P->nnzrow[father], tmpj, &nnznbr);
	  free(P->ja[father]);
#ifdef DEBUG_M
	  assert(nnznbr>0);
#endif
	  P->ja[father] = (int *)malloc(sizeof(int)*nnznbr);
	  memcpy(P->ja[father], tmpj, sizeof(int)*nnznbr);
	  P->nnzrow[father] = nnznbr;
	}
    }


#ifdef DEBUG_M
  for(i=0;i<cblknbr;i++)
    {
      dim_t j;
      assert(P->nnzrow[i] >= rangtab[i+1]-rangtab[i]);
      k = 0;
      for(j=rangtab[i]; j < rangtab[i+1];j++)
	assert(P->ja[i][k++] == j);
    }
  
  /** Check that all terms of A are in the pattern **/
  for(k=0;k<cblknbr;k++)
    {
      for(i=rangtab[k];i<rangtab[k+1];i++)
	{
	  j = 0;
	  while(j<A->nnzrow[i] && A->ja[i][j] < i)
	    j++;

	  for(ind = j;ind < A->nnzrow[i];ind++)
	    assert(A->ja[i][ind] >= i);
	  for(ind = j+1;ind < A->nnzrow[i];ind++)
	    assert(A->ja[i][ind] > A->ja[i][ind-1]);  
	  
	  
	  UnionSet(P->ja[k], P->nnzrow[k], A->ja[i]+j, A->nnzrow[i]-j, tmp, &ind);
	  if(ind > P->nnzrow[k])
	    fprintfd(stderr, "k=%ld [%ld %ld]  i=%ld ind %ld nnz %ld \n", 
		     (long)k, (long)rangtab[k], (long)rangtab[k+1], (long)i, (long)ind, (long)P->nnzrow[k]);

	  assert(ind <= P->nnzrow[k]);
	}
    }
#endif

  free(node2cblk);
  free(tmpj);
  free(tmp);
  
}


/* @authors J. GAIDAMOUR */

#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#include <assert.h>
#include "block.h"
#include "prec.h"

#include "base.h"

/* NO REC ou LEV > 0*/
/*  a simplifier */
void CMDu(_DBDistrMatrix,FACTOFACTO)(_UDBDistrMatrix* LU, PhidalOptions* option, _PhidalDistrHID* _DBL) {

#ifndef PARALLEL
#if defined(SYMMETRIC)
  _DBDistrMatrix* L = LU->L;
  if ((L->alloc != BLK) && (L->alloc != RBLK))
    DBMatrix_FACTO(LU);
  else
    DBMatrix_FACTO2(LU);
#elif defined(UNSYMMETRIC)
  DBMatrix_FACTOu(LU);
#endif

#else

#if defined(SYMMETRIC)
  printfv(5, "DBDistrMatrix_ICCT ... \n");
  DBDistrMatrix_ICCT(0, LU->L, DBL, option);
#elif defined(UNSYMMETRIC)
  printfv(5, "DBDistrMatrix_ILUT ... \n");
  exit(1);
#endif
#endif
   
}


#ifndef PARALLEL
#if defined(SYMMETRIC)

void CMD(_DBDistrMatrix,FACTO_L)(_DBDistrMatrix* L) 
{
  dim_t i;
  VSolverMatrix* csL;

  COEF *tmpF, *tmpW;

  /* Allocation des vecteurs temporaires */
  tmpF = (COEF *)malloc(sizeof(COEF)*L->coefmax);
  assert(tmpF != NULL);

  tmpW = (COEF *)malloc(sizeof(COEF)*L->coefmax);
  assert(tmpW != NULL);


  for(i=L->tlj;i<=L->brj;i++)
    {

      csL =  L->ca[ L->cia[i] ];
      VS_ICCT(csL, tmpF, tmpW);

    }

  free(tmpF);
  free(tmpW);

}

/* void DBMatrix_FACTO_Lu(DBMatrix* L, DBMatrix* U)  */
/* { */
/*   dim_t i; */
/*   VSolverMatrix *csL, *csU; */

/*   COEF *tmpW; */

/*   /\* Allocation des vecteurs temporaires *\/ */
/*   tmpW = (COEF *)malloc(sizeof(COEF)*L->coefmax); */
/*   assert(tmpW != NULL); */

/*   for(i=L->tlj;i<=L->brj;i++) */
/*     { */

/*       csL =  L->ca[ L->cia[i] ]; */
/*       csU =  U->ca[ U->ria[i] ]; */
/*       VS_ICCTu(csL, csU, tmpW); */

/*     } */

/*   free(tmpW); */

/* } */


/* todo : alloc fur et a mesure */
void CMD(_DBDistrPrec,FACTO_TRSM)(_DBDistrPrec* P) 
{
  _DBDistrMatrix* L = PREC_L_BLOCK(P);
  _DBDistrMatrix* E = PREC_EDB(P);

  dim_t i,j;
  VSolverMatrix* csL;

  int coefmax;  
  COEF *tmpF, *tmpW;

  /* Allocation des vecteurs temporaires */
  coefmax = MAX(E->coefmax, L->coefmax);

  tmpF = (COEF *)malloc(sizeof(COEF)*L->coefmax);
  assert(tmpF != NULL);

  tmpW = (COEF *)malloc(sizeof(COEF)*coefmax);
  assert(tmpW != NULL);

  for(i=L->tlj;i<=L->brj;i++)
    {

      csL =  L->ca[ L->cia[i] ];
      VS_ICCT(csL, tmpF, tmpW);

      if (/* (E->alloc == ONE) TODO! || */ (E->alloc == CBLK))
	{

	  E->a[i].coefmax = E->coefmax; /*fix*/
	  VS2_InvLT(1, csL->solvmtx, &csL->symbmtx, &E->a[i], &E->a[i].symbmtx, tmpW);

	} else {


	  for(j=E->cia[i];j<E->cia[i+1];j++) {
	    E->ca[j]->solvmtx->coefmax = E->coefmax; /*fix*/
	    VS_InvLT(1, csL, E->ca[j], tmpW);
	  }

	}

    }

  free(tmpF);
  free(tmpW);

}

/*TODO optim = access a coeftab multiple, les limiter*/
/* TODO : 2 blocknums */
/* idem pour bloknum, sans doute optimisable*/
/* voir les noms de variables, la maniÃ¨res dont elles sont utilisÃ©es ou rÃ©utilisÃ©es ... + commentaires */

/* TODO : calcul du bloc du mileu en double*/
/*TODO : remettre/ / assert ds le cas direct*/

/* Faire une vrai facto right looking en dÃ©coupant les fonctions par blocs phidals */

void DBMatrix_sFACTO(int k, int cdim, 
		     DBMatrix* L, SolverMatrix* solvL, SymbolMatrix* symbmtx, COEF* lc, int p, int rdim, blas_t stride,
                     COEF* uc,
                     COEF* W);

void DBMatrix_FACTO(UDBMatrix* LU) 
{
  DBMatrix* L = LU->L;

  /*************************************************************/
  /* This function performs in place the L.Lt factorization of */
  /* a symmetric sparse matrix structured in dense blocks of   */
  /* coefficients                                              */
  /*************************************************************/
  SymbolMatrix *symbmtx;
  SolverMatrix *solvL;

  int i, ilast;
  dim_t p, k;
  dim_t m;
  COEF *W, *F; /** buffers de travail **/
  COEF alphadiag;

  int UN=1;
  COEF one = 1.0;
  char /* *sideL  = "L", */ *sideR  = "R";
  char /* *uploU  = "U", */ *uploL  = "L";
  char /* *transN = "N", */ *transT = "T";
  char /* *diagN  = "N", */ *diagU  = "U";

  blas_t stride;
  int cdim, rdim;
  COEF *ccL; 
  COEF *bc, *lc, *uc; 

  int bloknum;

  assert((L->alloc == ONE) || (L->alloc == CBLK));

  /*** Allocation de buffers de travail ***/
  F = (COEF *)malloc(sizeof(COEF)*L->coefmax);
  assert(F != NULL);
  W = (COEF *)malloc(sizeof(COEF)*L->coefmax);
  assert(W != NULL);

  if (L->alloc == ONE) {
    ilast = 1;
  } else ilast = L->brj - L->tlj + 1;

  for(i=0;i<ilast;i++) {
    solvL = &L->a[i];     
    symbmtx = &solvL->symbmtx;
      
    for(k=0;k<symbmtx->cblknbr;k++) {
      cdim   = symbmtx->ccblktab[k].lcolnum - symbmtx->ccblktab[k].fcolnum + 1; /** Largeur du bloc colonne **/
      stride = symbmtx->stride[k];

      /**********************************/
      /* Factorisation du bloc diagonal */
      /**********************************/
      /** Calcul du pointeur dans coeftab vers le  dÃÂ©but des coefficients du bloc diagonal **/ 
      p   = symbmtx->bcblktab[k].fbloknum;
      ccL = solvL->coeftab + solvL->bloktab[p].coefind;

      LDLt_piv(cdim, stride, ccL, EPSILON);


      /* TODO : voir desc2.c */      
      if ((i == ilast-1) && (k == symbmtx->cblknbr-1))
	continue;  /** Pas de bloc extradiagonal le calcul est terminer **/

      /************************************************************/
      /* "Diviser" les blocs extra diagonaux par le bloc diagonal */
      /************************************************************/
      {
	/** On effectue l'operation : M = M.(L^-1)t  avec M = l'ensemble compacte des blocs extra-diagonaux du bloc colonne **/

	p = bloknum = symbmtx->bcblktab[k].fbloknum+1; /** Indice du premier bloc extra-diagonal **/
	if (symbmtx->bcblktab[k].fbloknum == symbmtx->bcblktab[k].lbloknum)
	  continue; /*todo : voir le continue prÃ©cÃ©dent -- a verif */
	
	/* * * */ /** ==> L21 = A21 . (U11)^-1 **/
	/** Calcul du pointeur dans coeftab vers le  dÃÂ©but des coefficients du premier bloc extra diagonal **/ 

	bc = solvL->coeftab + solvL->bloktab[p].coefind;

	rdim = symbmtx->hdim[k] - cdim;  /** Hauteur de la surface compactÃÂ©e des bloc extra-diagonaux **/
	BLAS_TRSM(sideR, uploL, transT, diagU, rdim, cdim, one, ccL, stride, bc, stride);
	
	/** Calcul de F dans un buffer temporaire: on divise toutes les colonnes par leur terme diagonal  **/
	for(m=0;m<cdim;m++)
	  {
	    /** Pointeur vers la m-ieme colonne dans le bloc colonne **/
	    bc = solvL->coeftab + solvL->bloktab[p].coefind + m*stride;

	    /* uc == fc */
	    /** Pointeur vers la m-ieme colonne dans le buffer F **/
	    uc = F + m*stride;
#ifndef DEBUG_NOALLOCATION
	    alphadiag = 1.0/ ccL[m*stride + m]; /** alpha est le m-ieme terme diagonal du bloc diagonal **/
#endif
	    BLAS_COPY(rdim, bc, UN, uc, UN); /** On copie les bloc extra diagonaux dans F **/
	    BLAS_SCAL(rdim, alphadiag, uc, UN); /** On divise la m-ieme colonne par le terme diagonal **/
	  }
      }
    
      /** Boucle sur les blocs extra-diagonaux **/
      /** On l'initialise ÃÂ  la heuteur totale des bloc extra diagonaux **/

      for(;p<=symbmtx->bcblktab[k].lbloknum; p++)
	{
	  /** Calcul du pointeur dans coeftab vers le dÃÂ©but des coefficients du bloc extra-diagonal p **/ 
	  lc = solvL->coeftab + solvL->bloktab[p].coefind;

	  /** Calcul du pointeur dans F vers le debut des coefficients du bloc extra-diagonal p correspondant **/
	  uc = /* fc = */ F + solvL->bloktab[p].coefind -  solvL->bloktab[ bloknum ].coefind;

	  /* sur L */
	  DBMatrix_sFACTO(k, cdim, 
			  L, solvL, symbmtx, lc, p, rdim, stride,
			  uc,			    
			  W); /* TODO : supprimer des arguments et les "recalculer" dans la fonction ? */

	  /** Mettre ÃÂ jour la hauteur des bloc extra diagonaux qui restent **/
	  rdim -= symbmtx->bloktab[p].lrownum - symbmtx->bloktab[p].frownum+1;
	} /* p */
      
      /** On recopie F dans les bloc extradiagonaux **/
      rdim = symbmtx->hdim[k] - cdim; /* hauteur des blocs extradiagonaux **/
      for(m=0;m<cdim;m++)
	{
	  /** Pointeur vers la m-ieme colonne dans le bloc colonne **/
	  bc = solvL->coeftab + solvL->bloktab[bloknum].coefind + m*stride;
	  
	  /** Pointeur vers la m-ieme colonne dans le buffer F **/
	  uc = F + m*stride;
	  BLAS_COPY(rdim, uc, UN, bc, UN); /** On copie la colonne de F **/
	}

    } /* k */
  } /* i */


  /** LibÃÂ©ration des buffers **/
  free(W);
  free(F);

}

#endif
#endif

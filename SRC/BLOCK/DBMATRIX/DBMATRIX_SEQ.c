/* @authors J. GAIDAMOUR */

#include <stdio.h>
#include <stdlib.h>

#include "hips_define.h"
#include "block.h"

void DBMATRIX_Precond(PhidalMatrix *A, DBPrec *P, SymbolMatrix* symbmtx, PhidalHID *BL, PhidalOptions *option)
{
#ifdef DEBUG_M
  assert(A->symmetric == option->symmetric);
#endif
  
  /** Correct the options **/
  PhidalOptions_Fix(option, BL);

  /** Print the options **/
  if(option->verbose > 0)
    PhidalOptions_Print(stdout, option);

  if(A->symmetric == 0)
    DBMATRIX_MLILUPrec(A, P, symbmtx, BL, option);
  else
    DBMATRIX_MLICCPrec(A, P, symbmtx, BL, option);
}

/* TODO : nom de la fonction : c'est aussi le nom d'un fichier */
INTS DBMATRIX_Solve(PhidalMatrix *A, DBPrec *P, PhidalHID *BL, PhidalOptions *option, COEF *rhs, COEF *x, dim_t *itertab, REAL *resnormtab) {
  chrono_t t1, t2;
  t1  = dwalltime();

  /*option->krylov_method = 0;*/ /* !! */

  /** The first level is always fgmres **/
  if(option->krylov_method != 1 || (option->forwardlev > 0 && option->schur_method != 0))
    {
      CHECK_RETURN(HIPS_Fgmresd_PH_DB(option->verbose, option->tol, option->itmax, A, P, BL, option, rhs, x, option->fd, itertab, resnormtab));
    }
  else
    {
      CHECK_RETURN(HIPS_PCG_PH_DB(option->verbose, option->tol, option->itmax, A, P, BL, option, rhs, x, option->fd, itertab, resnormtab));
    }

  t2  = dwalltime();

  /* if(option->verbose > 0) */
  fprintfv(1, stdout, "Number of outer iterations %d \n", itertab[0]);
  
  fprintfv(1, stdout, "\n SOLVED IN %g \n", t2-t1);
  return HIPS_SUCCESS;
}


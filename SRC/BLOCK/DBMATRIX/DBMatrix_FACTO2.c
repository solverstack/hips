/* @authors J. GAIDAMOUR */

#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#include <assert.h>
#include "block.h"

#include "base.h"

#define MAX(x,y) (((x)<(y))?(y):(x))
#define MIN(x,y) (((x)>(y))?(y):(x))

/*TODO optim = access a coeftab multiple, les limiter*/
/* TODO : 2 blocknums */

/*idem pour bloknum, sans doute optimisable*/



#define check(L, solvL, symbLs)

void DBMatrix_FACTO2(UDBMatrix* LU) 
{
  DBMatrix* L = LU->L;
  /*************************************************************/
  /* This function performs in place the L.Lt factorization of */
  /* a symmetric sparse matrix structured in dense blocks of   */
  /* coefficients                                              */
  /*************************************************************/
  SolverMatrix *solvL=NULL, *solvSL=NULL;
  SymbolMatrix *SL;
  SymbolMatrix *symbA;
  int i, i0;
  dim_t p, q, k, m;
  COEF *W, *F; /** buffers de travail **/
  int UN=1;
  COEF alpha, beta;
  blas_t stride, strideCC, strideBC;
  int cdim, rdim, hdim, mdim;
  /*   int Srdim; */
  /*   int pSA; */
  /*   int hdim2; */
  /*   COEF *ac; */
  COEF *cc; 
  COEF *bc; 
  COEF *fc, *fc_; 
  COEF *wc; 
  int facestride;
  int bloknum, facecblknum;
  int decalcol;
  int decalrow;
  int decalF;
  int decalW;
  SymbolMatrix* symbLs;

  SolverMatrix* facesolvmtx=NULL;
  SymbolMatrix* facesymbmtx=NULL;
  
  int j, j2, jC, bloknumLs;

  if ((L->alloc == ONE) || (L->alloc == CBLK))
     printfd("***  Warning : DBMatrix_FACTO2 instead of de DBMatrix_FACTO\n");

  /*   assert((L->alloc == ONE) || (L->alloc == CBLK) || (L->alloc == RBLK) || (L->alloc == BLK)); */

  /*** Allocation de buffers de travail ***/
  F = (COEF *)malloc(sizeof(COEF)*L->coefmax);
  assert(F != NULL);
  W = (COEF *)malloc(sizeof(COEF)*L->coefmax);
  assert(W != NULL);

  if (L->alloc == ONE) {
    solvL = &L->a[0];
    solvSL = &L->a[0];
    facesolvmtx = L->a;
    facesymbmtx = &facesolvmtx->symbmtx;
  }

  for(i0=0,i=L->tlj; i<=L->brj; i0++, i++) {/*  todo : simplier, sans i0 */

    if (L->alloc == CBLK) {
      solvSL = &L->a[i0];
      solvL = &L->a[i0];
    }

    SL = &L->ca[L->cia[i]]->symbmtx;

    if(L->alloc == BLK) {
      /*       solvSL = &L->a[L->cia[i]]; */
      solvSL = L->ca[L->cia[i]]->solvmtx;
    }

    for(k=0;k<SL->cblknbr; k++) {
      cdim   = SL->ccblktab[k].lcolnum - SL->ccblktab[k].fcolnum + 1; /** Largeur du bloc colonne **/
      
      if (L->alloc != BLK)
	stride = L->hdim[k+SL->facedecal - L->a->symbmtx.facedecal]; /*calculable avec la premiÃ¨re boucle*/
      else 
	stride = L->hdim[k+SL->facedecal - L->ca[0]->symbmtx.facedecal]; /*calculable avec la premiÃ¨re boucle*/

      strideCC = SL->stride[k];

      if ((L->alloc == ONE) || (L->alloc == CBLK))
	strideBC = strideCC;

      /**********************************/
      /* Factorisation du bloc diagonal */
      /**********************************/
      /** Calcul du pointeur dans coeftab vers le dÃÂ©but des coefficients du bloc diagonal **/ 
      p  = SL->bcblktab[k].fbloknum;
      if (L->alloc == RBLK)
	solvSL = &L->a[L->cja[L->cia[i]]-L->tli];

      cc = solvSL->coeftab + solvSL->bloktab[p].coefind;
      LDLt_piv(cdim, strideCC, cc, EPSILON);
	
      /* TODO : voir desc2.c */      
      if ((i == L->brj) && (k == SL->cblknbr-1))
	continue;  /** Pas de bloc extradiagonal le calcul est terminer **/

      /**********************************************************************/
      /* "Diviser" les blocs de M par le bloc diagonal de L                 */
      /**********************************************************************/
      {
	/** On effectue l'operation : M = M.(L^-1)t  avec M = l'ensemble compacte
	    des blocs extra-diagonaux du bloc colonne de M **/

	char *side = "R"; /* a sortir */
	char *uplo = "L";
	char *trans = "T";
	flag_t unitdiag = 1;
	char *diag; if(unitdiag==1) diag = "U"; else diag = "N";
		
	fc_ = F;

	/* parcours des blk phidal */
	blas_t stridecount = 0;
	for(j=L->cia[i]/*!!!*/;j<L->cia[i+1];j++){
	  symbLs = &L->ca[j]->symbmtx;

	  if(L->alloc == BLK) {
	    solvL = L->ca[j]->solvmtx;
	    strideBC = symbLs->stride[k];
	  }

	  if(L->alloc == RBLK) {
	    solvL = &L->a[L->cja[j]-L->tli];
	    strideBC = symbLs->stride[k];
	  }
	  
	  stridecount += symbLs->hdim[k];

	  /* Srdim au lieu de rdim */
	  rdim = symbLs->hdim[k]; /** Hauteur de la surface compactÃÂ©e des blocs **/
	  
	  /** Calcul du pointeur dans coeftab vers le dÃÂ©but des coefficients de M */
	  bloknumLs = symbLs->bcblktab[k].fbloknum;   /** Indice du premier bloc (triangulaire) dans M **/ /*!pas M,*/

	  /* FIX*/
	  if (j == L->cia[i]) {
	    bloknumLs++;
	    rdim -= cdim;
	  }

	  if (rdim == 0) { continue; }
	  
	  bc = solvL->coeftab + solvL->bloktab[bloknumLs].coefind;
	  alpha = 1.0;
	  BLAS_TRSM(side, uplo, trans, diag, rdim, cdim, alpha, cc, strideCC, bc, strideBC);

	  /** Calcul de F dans un buffer temporaire: on divise toutes les colonnes par leur terme diagonal  **/
	  fc = fc_;
	  for(m=0;m<cdim;m++)
	    {
#ifndef DEBUG_NOALLOCATION
	      alpha = 1.0/ cc[m*strideCC + m]; /** alpha est le m-ieme terme diagonal du bloc diagonal **/ 
	      /* TODO en changeant stride change */
#endif
	      BLAS_COPY(rdim, bc, UN, fc, UN); /** On copie les bloc extra diagonaux dans F **/
	      BLAS_SCAL(rdim, alpha, fc, UN); /** On divise la m-ieme colonne par le terme diagonal **/ /*peut etre sorti*/
	      bc += strideBC;
	      fc += stride;
	    }
	  fc_ += rdim;
	}

	assert(stridecount == stride);
/* 	stride = stridecount; */
      }
  
      rdim = stride/*hdim global*/ - cdim;  /** Hauteur de la surface compactÃÂ©e des bloc extra-diagonaux **/

      decalF = 0;
      for(j=L->cia[i]/*!!!*/;j<L->cia[i+1];j++){
	symbLs = &L->ca[j]->symbmtx;

	if(L->alloc == BLK) {
	  solvL = L->ca[j]->solvmtx;
	  strideBC = symbLs->stride[k];
	}

	if(L->alloc == RBLK) {
	  solvL = &L->a[L->cja[j]-L->tli];
	  strideBC = symbLs->stride[k];
	}
	
	bloknumLs = symbLs->bcblktab[k].fbloknum; 
	if (j == L->cia[i]) {
	  bloknumLs++;
	}
	
	for(p=bloknumLs;p<=symbLs->bcblktab[k].lbloknum;p++) {
	  
	  /*** Stocker dans W le produit des blocs {A(q,k), q>=p} avec Ft(p,k) ***/
	  char *opA = "N"; /** Ne pas transposer A(**) **/
	  char *opF = "T"; /** Transpose F **/

	  /** Calcul du pointeur dans coeftab vers le dÃÂ©but des coefficients du bloc extra-diagonal p **/ 
	  check(L, solvL, symbLs);
	  bc = solvL->coeftab + solvL->bloktab[p].coefind;

	  /** Calcul du pointeur dans F vers le debut des coefficients du bloc extra-diagonal p correspondant **/
	  bloknum = SL->bcblktab[k].fbloknum + 1; /** Indice du premier bloc extra-diagonal**/  /*a sortir*/

	  /* if (SL->bcblktab[k].fbloknum == SL->bcblktab[k].lbloknum) continue; non */

	  /* TODO : ne dÃ©pend que de k TODO : juste pour la ligne d'apres et pour la ligne du for(p*/
	  check(L, solvSL, SL);
	  fc = F + decalF;/* solvL->bloktab[p].coefind - solvSL->bloktab[ bloknum ].coefind; / / pas en RBLK ! */
/* 	  assert(solvL->bloktab[p].coefind - solvSL->bloktab[ bloknum ].coefind == decalF); */
	  
	  /** Calcul de la hauteur du bloc extra diagonal p **/
	  hdim = symbLs->bloktab[p].lrownum - symbLs->bloktab[p].frownum+1;
	  decalF += hdim;

	  alpha = 1.0;
	  beta = 0.0;

	  assert(stride*hdim <= L->coefmax);
/* 	  BLAS_GEMM(opA, opF, rdim, hdim, cdim, alpha, bc, strideBC, fc, stride, beta, W, stride); */
	  BLAS_GEMM(opA, opF, rdim, hdim, cdim, alpha, fc, stride, bc, strideBC, beta, W, stride);

  	  decalW=0;
	  /****************************************************************************************/
	  /** Pour tout les blocs extra-diagonaux {A(j,k), j>= i} faire A(i,j) = A(i,j) - W(i, ) **/
	  /****************************************************************************************/
	  jC = L->cia[L->cja[j]];
	  facesymbmtx = &L->ca[jC]->symbmtx;
	  if (L->alloc == CBLK)
	    facesolvmtx = &L->a[L->cja[j]-L->tlj/*tli?*/];

	  facecblknum = symbLs->bloktab[p].cblknum - facesymbmtx->facedecal; 

	  /** Nombre de colonne "ÃÂ  gauche" de la zone modifiÃÂ©e dans le bloc colonne facecblknum **/
	  decalcol = symbLs->bloktab[p].frownum - facesymbmtx->ccblktab[facecblknum].fcolnum;

	  /*** Largueur de la zone modifiÃÂ©e == hauteur du bloc p dans le bloc colonne k **/
	  mdim = symbLs->bloktab[p].lrownum - symbLs->bloktab[p].frownum+1;

	  /* 	  printfd("facedecal = %d facedecal = %d\n", facesymbmtx->facedecal, symbA->facedecal); */
	  /* 	  assert(facesymbmtx->facedecal == symbA->facedecal); */
	  /* 	  assert(facestride == symbA->stride[facecblknum]); */

	  /**/
	  q=p;
	  for(j2=j;j2<L->cia[i+1];j2++){
	    symbA = &L->ca[j2]->symbmtx;
	    
	    if (j2 != j) q = symbA->bcblktab[k].fbloknum; /* avant, pour le goto fin? */
	    	     
	    /* update facesolvmtx/symbmtx */ /*simplifier*/


	    while ((jC < L->cia[L->cja[j]+1]) && (L->cja[j2] > L->cja[jC])) jC++;
	    if ((jC >= L->cia[L->cja[j]+1]) || (L->cja[j2] != L->cja[jC])) {
	      decalW += symbA->hdim[k];
	      continue;
	    }
	    assert(jC < L->cia[L->cja[j]+1]);

	    facesymbmtx = &L->ca[jC]->symbmtx;
	    if (L->alloc == BLK)
	      facesolvmtx = L->ca[jC]->solvmtx;
	    if (L->alloc == RBLK)
	      facesolvmtx = &L->a[L->cja[jC]-L->tli/*tlj?*/];

	    check(L, facesolvmtx, facesymbmtx);
	    
	    bloknum = facesymbmtx->bcblktab[facecblknum].fbloknum; /** Indice du bloc diagonal 
								       du bloc colonne facebloknum **/
	    

	    /** Stride du cblk en face du bloc extra-diagonal p **/
	    facestride = facesymbmtx->stride[facecblknum];
	    
	    for(;q<=symbA->bcblktab[k].lbloknum;q++) /** Pour tous les bloc extradiagonaux A(q,k) avec q >= p **/ {
	      /**/

	      /* skip block that cannot match */
	      if (bloknum >= facesymbmtx->bcblktab[facecblknum].lbloknum+1/*todo le +1*/) {
		goto fin; /* utile ici ? */
	      }

	      while(symbA->bloktab[q].frownum > facesymbmtx->bloktab[bloknum].lrownum) {
		bloknum++;
		if (bloknum >= facesymbmtx->bcblktab[facecblknum].lbloknum+1/*todo le +1*/) {
		  goto fin;
		}
	      }
	      
	      /* for every block that match */
	      while(symbA->bloktab[q].lrownum >= facesymbmtx->bloktab[bloknum].frownum) {
		/* assert ds le cas direct*/
		/*		assert(symbmtx->bloktab[bloknum].frownum <= symbmtx->bloktab[q].frownum);*/
		/*		assert(symbmtx->bloktab[bloknum].lrownum >= symbmtx->bloktab[q].lrownum);*/
		
		decalrow = symbA->bloktab[q].frownum - facesymbmtx->bloktab[bloknum].frownum;
		
		/** Calcul du pointeur de debut de la zone modifiÃÂ©e dans le bloc A(bloknum, facebloknum) **/
		bc = facesolvmtx->coeftab + facesolvmtx->bloktab[bloknum].coefind + decalcol*facestride + MAX(0,decalrow);
	      
		/** Calcul du pointeur de debut du bloc correspondant ÃÂ  A(q,k) dans W **/

#ifdef DEBUG_M
		if(L->alloc == ONE) {
		  assert(decalW == solvL->bloktab[q].coefind - solvL->bloktab[p].coefind); /* en ONE ou CBLK */
		}
#endif
		wc = W + decalW + MAX(0,-decalrow);

		/*** Update de la contribution ***/
		hdim = MIN(symbA->bloktab[q].lrownum, facesymbmtx->bloktab[bloknum].lrownum) - 
		  MAX(symbA->bloktab[q].frownum, facesymbmtx->bloktab[bloknum].frownum) +1; /** Hauteur du bloc sur lequel on effectue la diffÃ©rence **/

		/*assert cas direct*/
		/*		assert(hdim == symbL->bloktab[q].lrownum -  symbL->bloktab[q].frownum +1);*/

		alpha = -1.0;
		for(m=0; m < mdim; m++)
		  {
		    BLAS_AXPY(hdim, alpha, wc, UN, bc, UN);
		    wc += stride;
		    bc += facestride;
		  }
		
		bloknum++;
		if (bloknum >= facesymbmtx->bcblktab[facecblknum].lbloknum+1/*todo +1*/) break;
	      }
	      
	    fin:
	      bloknum--;
	      decalW += symbA->bloktab[q].lrownum - symbA->bloktab[q].frownum+1;
	    }

	  } /* for j2 */
	  
	  /** Mettre ÃÂ  jour la hauteur des bloc extra diagonaux qui restent **/
	  rdim -= symbLs->bloktab[p].lrownum - symbLs->bloktab[p].frownum+1; /*mdim?*/
	} /* for p */

      } /* for j */

      
      /* parcours des blk phidal */
      fc_ = F;
      for(j=L->cia[i]/*!!!*/;j<L->cia[i+1];j++){
	symbLs = &L->ca[j]->symbmtx;
	
	if(L->alloc == BLK) {
	  solvL = L->ca[j]->solvmtx;
	  strideBC = symbLs->stride[k];
	}

	if(L->alloc == RBLK) {
	  solvL = &L->a[L->cja[j]-L->tli];
	  strideBC = symbLs->stride[k];
	}
	
	/* Srdim au lieu de rdim */
	rdim = symbLs->hdim[k]; /** Hauteur de la surface compactÃÂ©e des blocs **/
	
	/** Calcul du pointeur dans coeftab vers le dÃÂ©but des coefficients de M */
	bloknumLs = symbLs->bcblktab[k].fbloknum;   /** Indice du premier bloc (triangulaire) dans M **/ /*!pas M,*/
	
	/* FIX*/
	if (j == L->cia[i]) {
	  bloknumLs++;
	  rdim -= cdim;
	}
	
	if (rdim == 0) { continue; }
	
	bc = solvL->coeftab + solvL->bloktab[bloknumLs].coefind;
	
	/** Calcul de F dans un buffer temporaire: on divise toutes les colonnes par leur terme diagonal  **/
	fc = fc_;
	for(m=0;m<cdim;m++)
	  {
	    BLAS_COPY(rdim, fc, UN, bc, UN); /** On copie la colonne de F **/
	    bc += strideBC;
	    fc += stride;
	  }
	fc_ += rdim;
      }
      
    }
  }


  /** LibÃÂ©ration des buffers **/
  free(W);
  free(F);

}


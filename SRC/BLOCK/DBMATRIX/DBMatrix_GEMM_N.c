/* @authors J. GAIDAMOUR */

#include <stdio.h>
#include <stdlib.h>
#include <string.h> /* memset */
#include <assert.h>

#include "block.h"

#include "base.h"

void DBMatrix_sGEMM_sym(int i, COEF alpha, 
			DBMatrix* A, INTL **Ap, /* TODO : faire un transpose Ã  la place */
/* 			DBMatrix* B, INTL **Bp, */
			int nk, dim_t *jak, VSolverMatrix** rak,
			DBMatrix* C, INTL **Cp,
			DBMatrix* D,	 
			int *tabA, int *tabC, 
			COEF* E, COEF* F, COEF* W, dim_t tli, int Aalloc);

/* void DBMatrix_sGEMM_sym(int i, COEF alpha,  */
/* 			DBMatrix* A, INTL **Ap, */
/* 			int nk, dim_t *jak, VSolverMatrix** rak, */
/* 			DBMatrix* C, INTL **Cp, */
/* 			DBMatrix* D,	  */
/* 			int *tabA, int *tabC,  */
/* 			COEF* E, COEF* F, COEF* W, dim_t tli, int Aalloc); */

void DBMatrix_GEMM_N(DBMatrix* C, COEF alpha, DBMatrix* A, DBMatrix* B_OLD, DBMatrix* D, 
		   flag_t fill, PhidalMatrix* Phidal, PhidalHID *BL) {
  /************************************************************************************/
  /* This function performs C = C + alpha.A.D^-1.B                                    */
  /************************************************************************************/
  /* int i0, iB, j, i; */
  dim_t ii;
  dim_t jj; /* int64 */
  INTL iB;

  COEF *E, *F, *W;
    
  int *tabA, *tabC;
  
  /* Allocation des vecteurs temporaires */
  E = (COEF *)malloc(sizeof(COEF)*A->coefmax);
  assert(E != NULL);

  F = (COEF *)malloc(sizeof(COEF)*A->coefmax);
  assert(F != NULL);

  W = (COEF *)malloc(sizeof(COEF)*A->coefmax);
  assert(W != NULL);

  tabA = (int*)malloc(sizeof(int)*A->dim1);
  tabC = (int*)malloc(sizeof(int)*C->dim1);
  assert(tabA != NULL);
  assert(tabC != NULL);

  for (jj=A->tlj; jj<=A->brj; jj++) {
    
    for (ii=A->cia[jj];ii<A->cia[jj+1];ii++) {

      /* printfv(5, "iB = %d, j= %d (%d)\n", A->cja[ii], jj, A->ca[ii] - A->bloktab); */

      iB = A->cja[ii];
      
      DBMatrix_sGEMM_sym(iB, alpha, A, &A->cia, /* A, &A->ria, */ 1, &jj, A->ca+ii, C, &C->cia, D,
			 tabA, tabC, E, F, W, A->tli, A->alloc);


    }
    /* printfv(5, "--\n"); */
  }

/*   for(i0=0, iB=A->tli;iB<=A->bri;i0++, iB++) { */
    
/*     for(j=A->ria[iB];j<A->ria[iB+1];j++) { */
            
/*       printfv(5, "iB = %d, j = %d (%d)\n", iB, *(A->rja+j), /\* j, *\/ A->ra[j] - A->bloktab); */

/*       DBMatrix_sGEMM_sym(iB, alpha, A, &A->cia, 1, A->rja+j, A->ra+j, C, &C->cia, D, */
/* 			 tabA, tabC, E, F, W, A->tli, A->alloc); */
    
/*     } */
/*     printfv(5, "--\n"); */
/*   } */

  free(E);
  free(F);
  free(W);
  free(tabA);
  free(tabC);

  
}




void DBMatrix_GEMMpart(dim_t jj, DBMatrix* C, COEF alpha, DBMatrix* A, /* DBMatrix* B_OLD, */ DBMatrix* D, 
		       /* flag_t fill, */ /* PhidalMatrix* Phidal, PhidalHID *BL,  */
		       COEF* E, COEF* F, COEF* W, int *tabA, int *tabC) {
  dim_t ii;
  INTL iB;
  
  for (ii=A->cia[jj];ii<A->cia[jj+1];ii++) {
    
      iB = A->cja[ii];
      
      DBMatrix_sGEMM_sym(iB, alpha, A, &A->cia, /* A, &A->ria, */ 1, &jj, A->ca+ii, C, &C->cia, D,
			 tabA, tabC, E, F, W, A->tli, A->alloc);


    }
 
}


void DBMatrix_sGEMM(int i, COEF alpha, 
		    DBMatrix* A, INTL **Ap, /* TODO : faire un transpose à la place */
		    /* DBMatrix* B, INTL **Bp, */
		    int nk, dim_t *jak, VSolverMatrix** rak,
		    DBMatrix* C, INTL **Cp,
		    DBMatrix* D,	 
		    int *tabA, int *tabC, 
		    COEF* E, COEF* F, COEF* W, dim_t tli, flag_t Aalloc);

void DBMatrix_GEMMpartu(dim_t jj, DBMatrix* CL, DBMatrix* CU, COEF alpha, DBMatrix* A, DBMatrix* B,
			COEF* E, COEF* F, COEF* W, int *tabA, int *tabC) {
  dim_t ii;
  INTL iB;
  
  for (ii=A->cia[jj];ii<A->cia[jj+1];ii++) {
    
    iB = A->cja[ii];
    
    DBMatrix_sGEMM    (iB, alpha, A, &A->cia, 1, &jj, B->ra+ii, CL, &CL->cia, NULL,
		       tabA, tabC, E, F, W, A->tli, A->alloc);
    
    DBMatrix_sGEMM    (iB, alpha, B, &B->ria, 1, &jj, A->ca+ii, CU, &CU->ria, NULL,
		       tabA, tabC, E, F, W, B->tlj, A->alloc);

  }
  
}








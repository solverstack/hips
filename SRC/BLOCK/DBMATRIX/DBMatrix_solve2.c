/* @authors J. GAIDAMOUR */

#include <stdio.h>
#include <stdlib.h>

#include "block.h"

/* void DBLsolv2(flag_t unitdiag, DBMatrix *L, COEF *b, COEF *x); */
/* void DBUsolv2(flag_t unitdiag, DBMatrix *U, COEF *b, COEF *x); */

/* void DBMatrix_Lsolv2(flag_t unitdiag, DBMatrix *L, COEF *x, COEF *b, PhidalHID *BL) { */
/*   /\* Lsolv(unitdiag, &(L->solvmtx), b, x); /\\*\TODO : les args sont inversé entre Lsolv et PhidalSequential*\\/ *\/ */
/*   DBLsolv2(unitdiag, L, b, x); /\*TODO : les args sont inversé entre Lsolv et PhidalSequential*\/ */
/* } */

void DBLsolv2(flag_t unitdiag, DBMatrix *L, COEF *b, COEF *x) {
  int i, i0, j, k, p;
  int jxtmp;

  SolverMatrix* solvSL=NULL;
  SymbolMatrix* SL;
  SolverMatrix* solvLs=NULL;
  SymbolMatrix* symbLs;
  dim_t tli;
  if (L->alloc != BLK)
    tli = L->a[0].symbmtx.tli;
  else /* (L->alloc == BLK) */
    tli = L->ca[0]->symbmtx.tli;

  COEF* xptr;
  int height, width, stride;

  COEF* xtmp;
  int size;

  COEF minusone= -1.0, zero=0.0, one=1.0;
  char *uploL = "L";
  char *transN = "N";
  char* diag = unitdiag?"U":"N";
  int UN = 1;
  assert((unitdiag == 0) || (unitdiag == 1));

  if ((L->alloc == ONE) || (L->alloc == CBLK))
     printfd("***  Warning : DBMatrix_solve2 instead of de DBMatrix_solve\n");

  if (x!=b)
    BLAS_COPY(L->nodenbr, b, UN, x, UN);

  /* vecteur tmp */
  xtmp = (COEF*)malloc(sizeof(COEF)*L->nodenbr);

 /*  for(i=0; i<L->nodenbr; i++) */
  /*     xtmp[i] = i; */

  assert((L->alloc == ONE) || (L->alloc == CBLK) || (L->alloc == RBLK) || (L->alloc == BLK));

  if (L->alloc == ONE) {
    solvSL = &L->a[0];    
    solvLs = &L->a[0];
  }

  /* parcours par blocs colonnes */
  for(i0=0,i=L->tlj; i<=L->brj; i0++, i++) {
    SL = &L->ca[L->cia[i]]->symbmtx;

    if (L->alloc == CBLK) {
      solvSL = &L->a[i0];
      solvLs = &L->a[i0];
    }

    if(L->alloc == BLK) {
      solvSL = L->ca[L->cia[i]]->solvmtx;
    }

    if (L->alloc == RBLK) {
      solvSL = &L->a[L->cja[L->cia[i]]-L->tli];
    }

    for(k=0;k<SL->cblknbr; k++) {
      width  = SL->ccblktab[k].lcolnum - SL->ccblktab[k].fcolnum +1;
      xptr   = x + SL->ccblktab[k].fcolnum - tli;

      {
	stride = SL->stride[k];
	p = SL->bcblktab[k].fbloknum; /* triangular block */

	/* résolution triangulaire */
	BLAS_TRSV(uploL, transN, diag, width /*=h*/,
		  solvSL->coeftab + solvSL->bloktab[p].coefind, stride, xptr, UN);

      }

      for(j=L->cia[i], jxtmp=0; j<L->cia[i+1]; j++){
	symbLs = &L->ca[j]->symbmtx;
	if (L->alloc == RBLK) {
	  solvLs = &L->a[L->cja[j]-L->tli];
	}
	
	if(L->alloc == BLK) {
	  solvLs = L->ca[j]->solvmtx;
	}

	p = symbLs->bcblktab[k].fbloknum;
	height = symbLs->hdim[k]; /** Hauteur de la surface compactÃ©e des bloc extra-diagonaux **/
	stride = symbLs->stride[k];
	if (j==L->cia[i]) {
	  if (p == symbLs->bcblktab[k].lbloknum)
	    continue; /*simplifier*/
	  p++;
	  height = symbLs->hdim[k] - width; /** Hauteur de la surface compactÃ©e des bloc extra-diagonaux **/
	}

	if (height == 0) continue;

	/* calcul des contributions (aggrégation des colonnes) */
	BLAS_GEMV(transN, height, width, minusone /*coef alpha*/,
		  solvLs->coeftab + solvLs->bloktab[p].coefind, stride,
		  xptr, UN,       zero /*coef beta*/,
		  xtmp + jxtmp, UN);
	jxtmp += height;
      }
       
      /* x := xtmp (out) */
      for(j=L->cia[i], jxtmp=0; j<L->cia[i+1]; j++){
	symbLs = &L->ca[j]->symbmtx;

	p = symbLs->bcblktab[k].fbloknum;
	if (j==L->cia[i]) p++;
	if (p> symbLs->bcblktab[k].lbloknum) continue;

	for(; p<=symbLs->bcblktab[k].lbloknum; p++) {
	  size = symbLs->bloktab[p].lrownum - symbLs->bloktab[p].frownum +1;
	  BLAS_AXPY(size, one, xtmp + jxtmp, UN, x + symbLs->bloktab[p].frownum - tli, UN);
	  jxtmp += size;
	}
      }

    }
  }

  free(xtmp);  
}

/* void DBMatrix_Usolv2(flag_t unitdiag, DBMatrix *U, COEF *x, COEF *b, PhidalHID *BL) { */
/*  /\*    if (U->alloc == ONE) *\/ */
/* /\*       Usolv(unitdiag, &(U->solvmtx), b, x); *\/ */
/* /\*     else *\/ */
/*       DBUsolv2(unitdiag, U, b, x); */
/* } */

/* TODO : fusionner SU Us*/
void DBUsolv2(flag_t unitdiag, DBMatrix *U, COEF *b, COEF *x) {
  int i, i0, j, k, p;
  int jxtmp;

  SolverMatrix* solvSU=NULL;
  SymbolMatrix* SU;
  SolverMatrix* solvUs=NULL;
  SymbolMatrix* symbUs=NULL;
  dim_t tli;
  if (U->alloc != BLK)
    tli = U->a[0].symbmtx.tli;
  else /* (U->alloc == BLK) */
    tli = U->ca[0]->symbmtx.tli;

  COEF* xptr;
  int height, width, stride;

  COEF* xtmp;
  int size;

  COEF minusone= -1.0, one=1.0;
  char *uploL = "L";
  char *transT = "T";
  char* diag = unitdiag?"U":"N";
  int UN = 1;
  assert((unitdiag == 0) || (unitdiag == 1));

  if ((U->alloc == ONE) || (U->alloc == CBLK))
     printfd("***  Warning : DBMatrix_solve2 instead of de DBMatrix_solve\n");

  if (x!=b)
    BLAS_COPY(U->nodenbr, b, UN, x, UN);

  /* vecteur tmp */
  xtmp = (COEF*)malloc(sizeof(COEF)*U->nodenbr);

  /*   assert((U->alloc == ONE) || (U->alloc == RBLK)); TODO Symm/Unsym */
  /* assert((U->alloc == ONE) || (U->alloc == CBLK) || (U->alloc == RBLK) || (U->alloc == BLK)); */

  if (unitdiag == 0) /*unsym*/
    {
      assert(0);
      DBMatrix_Transpose(U);
    }

  if (U->alloc == ONE) {
    solvSU = &U->a[0];
    solvUs = &U->a[0];
  }

  /* parcours par blocs colonnes */
  for(i0=U->brj-U->tlj,i=U->brj; i0>=0; i0--, i--) {
    SU = &U->ca[U->cia[i]]->symbmtx;

    if (U->alloc == CBLK) {
      solvSU = &U->a[i0];
      solvUs = &U->a[i0];
    }

    if(U->alloc == BLK) {
      solvSU = U->ca[U->cia[i]]->solvmtx;
    }

    if (U->alloc == RBLK) {
      solvSU = &U->a[U->cja[U->cia[i]]-U->tli];
    }
    
    for(k=SU->cblknbr-1; k>=0; k--) {
      width  = SU->ccblktab[k].lcolnum - SU->ccblktab[k].fcolnum +1;
      xptr = x + SU->ccblktab[k].fcolnum - tli;
      
      /* if (p != symbmtx->bcblktab[k].lbloknum) { */
	
      /* xtmp := x (in) */
      for(j=U->cia[i], jxtmp=0; j<U->cia[i+1]; j++){
	symbUs = &U->ca[j]->symbmtx;

	p = symbUs->bcblktab[k].fbloknum;
	if (j==U->cia[i]) p++;
	if (p> symbUs->bcblktab[k].lbloknum) continue;
	
	for(; p<=symbUs->bcblktab[k].lbloknum; p++) {
	  size = symbUs->bloktab[p].lrownum - symbUs->bloktab[p].frownum +1;
	  BLAS_COPY(size, x + symbUs->bloktab[p].frownum - tli, UN, xtmp + jxtmp, UN);
	  jxtmp += size;
	}
	/** A SUPPR --> */ /* suppr de jxtmp, attention au continue !! */
      }

      for(j=U->cia[i], jxtmp=0; j<U->cia[i+1]; j++){
	/** <-- A SUPPR */

	symbUs = &U->ca[j]->symbmtx;
	if (U->alloc == RBLK) {
	  solvUs = &U->a[U->cja[j]-U->tli];
	}
	
	if(U->alloc == BLK) {
	  solvUs = U->ca[j]->solvmtx;
	}
	
	p = symbUs->bcblktab[k].fbloknum;
	height = symbUs->hdim[k]; /** Hauteur de la surface compactÃ©e des bloc extra-diagonaux **/
	stride = symbUs->stride[k];
	if (j==U->cia[i]) {
	  if (p == symbUs->bcblktab[k].lbloknum)
	    continue; /*simplifier*/
	  p++;
	  height = symbUs->hdim[k] - width; /** Hauteur de la surface compactÃ©e des bloc extra-diagonaux **/
	}
	
	if (height == 0) continue;
	
	/* calcul des contributions (aggrégation des colonnes) */
	BLAS_GEMV(transT, height, width, minusone /*coef alpha*/,
		  solvUs->coeftab + solvUs->bloktab[p].coefind, stride,
		  xtmp + jxtmp, UN,       one /*coef beta*/,
		  xptr, UN);
	
	jxtmp += height;
      }
      
      /* résolution triangulaire */ /* peut etre mis hors boucle  */
      {
	stride = SU->stride[k];      
	p = SU->bcblktab[k].fbloknum; /* triangular block */
	BLAS_TRSV(uploL, transT, diag, width /*=h*/,
		  solvSU->coeftab + solvSU->bloktab[p].coefind, stride, xptr, UN);
      }
 
    }
    
  }

  if (unitdiag == 0) /*unsym*/
    {
      DBMatrix_Transpose(U);
    }

  free(xtmp);
}

/* todo : la version 1 ne sert à rien */ 
void DBMatrix_Dsolv2(DBMatrix *D, COEF* b) {
#ifdef DEBUG_NOALLOCATION
  return;
#endif

/*   DBMatrix_Dsolv(D, b); */
/*   return; */

  /*   Dsolv(&(D->solvmtx), b); */

  SolverMatrix* solvSD=NULL;
  SymbolMatrix* SD;
  int k, i, i0, ii, iiglobal;
  blas_t stride;
  dim_t p;
  COEF* ptr;
  
  /*   assert((D->alloc == ONE) || (D->alloc == CBLK) || D->allo); */
  
  if (D->alloc == ONE) {
    solvSD = &D->a[0];
  }

  /* parcours par blocs colonnes */
  iiglobal=0;
  for(i0=0,i=D->tlj; i<=D->brj; i0++, i++) {
    SD = &D->ca[D->cia[i]]->symbmtx;

    if (D->alloc == CBLK) {
      solvSD = &D->a[i0];
    }

    if(D->alloc == BLK) {
      solvSD = D->ca[D->cia[i]]->solvmtx;
    }

    if (D->alloc == RBLK) {
      solvSD = &D->a[D->cja[D->cia[i]]-D->tli];
    }

    for(k=0; k<SD->cblknbr; k++) {
      p      = SD->bcblktab[k].fbloknum;        /* triangular block */
      ptr    = solvSD->coeftab + solvSD->bloktab[p].coefind;
      stride = SD->stride[k];

      for (ii=SD->ccblktab[k].fcolnum; ii<=SD->ccblktab[k].lcolnum; ii++, iiglobal++) { /* TODO : nom de variables */
	b[iiglobal] /= ptr[0];
	ptr += stride + 1;
      }
    }
  }

}

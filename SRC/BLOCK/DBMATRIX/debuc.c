/* @release_exclude */
/* @authors J. GAIDAMOUR */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>

#include "phidal_parallel.h"
#include "db_parallel.h"
#include "block.h"

void printDiag_db(COEF* D, int n, int proc_id, char* info) {
    char filename[100];
    sprintf(filename, "db/diag-%s-proc-%d.txt", info, proc_id);
    
    FILE* stream = fopen(filename, "w");

    {   
      int i;
      for(i=0; i<n; i++) {
	fprintf(stream, "%d %.13e\n", i, D[i]);
      }
      
    }

    fclose(stream);
}

void printDiag_ph(COEF* D, int n, int proc_id, char* info) {
    char filename[100];
    sprintf(filename, "ph/diag-%s-proc-%d.txt", info, proc_id);
    
    FILE* stream = fopen(filename, "w");

    {   
      int i;
      for(i=0; i<n; i++) {
	fprintf(stream, "%d %.13e\n", i, 1/D[i]);
      }
      
    }

    fclose(stream);
}


void printPhidalMatrix(DBMatrix* L, int proc_id, char* info) {
    char filename[100];
    sprintf(filename, "ph/mtx-%s-proc-%d.txt", info, proc_id);
    
    FILE* stream = fopen(filename, "w");
    dumpPhidalMatrix(stream, L);
    fclose(stream);
}


/* print bloc par bloc, dans S uniquement, en BLK uniquement (debug) */
void printDBMatrix(DBMatrix* L, int proc_id, char* info) {
  int i, j, m, ii;
  int cdim;
  SymbolMatrix* symbLs;
  SolverMatrix* solvL;
  char filename[100];

  sprintf(filename, "db/mtx-%s-proc-%d.txt", info, proc_id);
  FILE* stream = fopen(filename, "w");
  assert(stream != NULL);

  assert(L->alloc == BLK); /* et uniquement pour S */

  for(i=L->tlj; i<=L->brj; i++) {
    fprintf( stream,"Colonne k = %d/%d\n", i, L->brj);
    for(j=L->cia[i];j<L->cia[i+1];j++){ 
      fprintf(stream, "bloc n°%d, ligne cja %d (id : %d)\n", j, L->cja[j], L->ca[j] - L->ca[0]);
      
      // equivalent de dumpCS2(1, stream, L->ca[j]);
      { /**********/
	
	symbLs   = &L->ca[j]->symbmtx;
	solvL    =  L->ca[j]->solvmtx;
	
	
	cdim = symbLs->ccblktab[0].lcolnum - symbLs->ccblktab[0].fcolnum + 1;
	for(m=0;m<cdim;m++)
	  {
	    
	    for(ii=0; ii<symbLs->hdim[0]; ii++) {
	      COEF val=*(solvL->coeftab + ii + (symbLs->hdim[0]*m));
	      if (((j == L->cia[i]) && (ii>=m)) || (j != L->cia[i]))
		if (val != 0)
		  fprintf(stream, "%d %d %.13e \n", ii+1, m+1, val);

	    }


	  }

	
      } /**********/
      
      
    }
  }



  close(stream);

}




void printDBMatrix_withoutdiag(DBMatrix* L, int proc_id, char* info) {
  int i, j, m, ii;
  int cdim;
  SymbolMatrix* symbLs;
  SolverMatrix* solvL;
  char filename[100];

  sprintf(filename, "db/mtx-%s-proc-%d.txt", info, proc_id);
  FILE* stream = fopen(filename, "w");
  assert(stream != NULL);

  assert(L->alloc == BLK); /* et uniquement pour S */

  for(i=L->tlj; i<=L->brj; i++) {
    fprintf( stream,"Colonne k = %d/%d\n", i, L->brj);
    for(j=L->cia[i];j<L->cia[i+1];j++){ 
      fprintf(stream, "bloc n°%d, ligne cja %d (id : %d)\n", j, L->cja[j], L->ca[j] - L->ca[0]);
      
      // equivalent de dumpCS2(1, stream, L->ca[j]);
      { /**********/
	
	symbLs   = &L->ca[j]->symbmtx;
	solvL    =  L->ca[j]->solvmtx;
	
	
	cdim = symbLs->ccblktab[0].lcolnum - symbLs->ccblktab[0].fcolnum + 1;
	for(m=0;m<cdim;m++)
	  {
	    
	    for(ii=0; ii<symbLs->hdim[0]; ii++) {
	      COEF val=*(solvL->coeftab + ii + (symbLs->hdim[0]*m));
	      if (((j == L->cia[i]) && (ii>m)) || (j != L->cia[i]))
		if (val != 0)
		  fprintf(stream, "%d %d %.13e \n", ii+1, m+1, val);

	    }


	  }

	
      } /**********/
      
      
    }
  }



  close(stream);

}












#ifdef DEBUG_PP

  {
    int k, i;
    for(k=LL->tlj;k<=LL->brj;k++) {
      
      Dprintf("colonne k = %d/%d\n", k,LL->brj);
      
      for(i=LL->cia[k];i<=LL->cia[k+1]-1;i++) {
	Dprintf("bloc n°%d, ligne cja %d (id : %d)\n", i, LL->cja[i], LL->ca[i] - LL->ca[0]);
      }
    }
  }
  
  {
    int k, i;
    for(k=LL->tlj;k<=LL->brj;k++) {
      
      Dprintf("ligne k = %d/%d\n", k,LL->brj);
      
      for(i=LL->ria[k];i<=LL->ria[k+1]-1;i++) {
	Dprintf("bloc n°%d, colonne cja %d (id : %d)\n", i, LL->rja[i], LL->ra[i] - LL->ra[0]);
      }
    }
  }

	
  exit(1);
#endif


#ifdef DEBUG_DE_GEMM
/* PARCOURS DES BLOCS MEMOIRE IMPLIQUES DANS LE GEMM : */ 
	{ /* BLAS_GEMM check */
	  int h, l;
	  for(l=0; l<cdim; l++) {
	    for(h=0; h<rdim; h++) {
	      printf("E[i] = %lf\n", E[l*strideA+h]);
	    }
	  }
	}

	{ /* BLAS_GEMM check */
	  int h, l;
	  for(l=0; l<cdim; l++) {
	    for(h=0; h<mdim; h++) {
	      printf("bc[i] = %lf\n", bc[l*strideB+h]);
	    }
	  }
	}

	{ /* BLAS_GEMM check */
	  int h, l;
	  for(l=0; l<cdim; l++) {
	    for(h=0; h<rdim; h++) {
	      printf("W[i] = %lf\n", W[l*strideA+h]);
	    }
	  }
	}
#endif

/* @authors J. GAIDAMOUR */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>

#include "block.h"
#include "prec.h"

void DBPrec_SchurProd(DBPrec *P, PhidalHID *BL, COEF *x, COEF *y)
{
  /**************************************************************/
  /* This function compute the implicit Schur Product           */
  /* y = S.x    where S is not stored                           */
  /* eg y = B.x - E.U^-1.L^1.F.x (for unsymmetric)              */
  /**************************************************************/

  /*** @@OIMBE PROBLEM SI PERMUTATION ????? B n'EST PAS PERMUTE ****/

  COEF *t;
  
  DBMatrix *L, *U;
  PhidalMatrix *E, *F, *B;

#ifdef DEBUG_M
  assert(P->schur_method == 2);
#endif

  E = PREC_E(P);
  F = PREC_F(P);
  L = PREC_L_BLOCK(P);
  U = PREC_U_BLOCK(P);
  B = PREC_B(P);

  t = (COEF *)malloc(sizeof(COEF)*L->dim1);

  PHIDAL_MatVec(B, BL, x, y);
  PHIDAL_MatVec(F, BL, x, t);

  DBMatrix_Lsolv(1, L, t, t, BL);
  if(P->symmetric == 1)
    DBMatrix_Dsolv(L, t);
  DBMatrix_Usolv(P->symmetric, U, t, t, BL);

  PHIDAL_MatVecSub(E, BL, t, y);

  free(t);
}

/* @authors J. GAIDAMOUR */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>

#include "phidal_parallel.h"
#include "db_parallel.h"
#include "block.h"

#define Dprintf(arg...)

void DBMatrix_ICCT_Restrict(int START, DBMatrix *L, PhidalHID *BL, PhidalOptions *option);

void DBMatrix_ICCT(DBMatrix *L, PhidalHID *BL, PhidalOptions *option)
{
  /******************************************************************************************/
  /* This function computes the incomplete factorization A = L.D.Lt of a symmetric matrix   */
  /* On entry:                                                                              */
  /*   L is the lower triangular part of the matrix to factorize in CSC format              */
  /*   job == 0 : the shared block are freed on the non-leader                              */
  /*   processors otherwise they stay in memory (need this when to use                      */
  /* L^-1.M in multilevel recursion)                                                        */
  /* on return:                                                                             */
  /* L the strictly lower triangular factor (unitary on the diagonal not stored) in CSC     */
  /* D is the INVERSE of the diagonal factor                                                */
  /* NOTE: L is sorted by row index on return                                               */
  /******************************************************************************************/
#ifdef DEBUG_M
  assert(S(L)->symmetric == 1);
#endif
  DBMatrix_ICCT_Restrict(L->tli, L, BL, option);
}



void DBMatrix_ICCT_Restrict(int START, DBMatrix *L, PhidalHID *BL, PhidalOptions *option)
{

  dim_t i,j, k;
  int nk;
  dim_t *jak; int *rindk;
  VSolverMatrix **rak;
  int ii, jj, kk;
  VSolverMatrix *csL;
  VSolverMatrix **csrtab1, **list1;
  VSolverMatrix **csrtab2, **list2;
  int *listindex;
  int nnb;
  CellDB *firstcol;
  DBMatrix *LL=L;

  COEF *E, *W;
  int *tabA, *tabC;
 
  printf("DBMatrix_ICCT\n");

  E = (COEF *)malloc(sizeof(COEF)*LL->coefmax);
  assert(E != NULL);
 
  W = (COEF *)malloc(sizeof(COEF)*LL->coefmax); /* TODO */
  assert(W != NULL);

  tabA = (int*)malloc(sizeof(int)*LL->dim1);
  tabC = (int*)malloc(sizeof(int)*LL->dim1);












  /* repère A */




  for(k=START;k<=LL->brj;k++)
    {
      Dprintf("k=%d\n", k);
      /*** Factorize the column block k ***/
	
      csL =  LL->ca[ LL->cia[k] ];

      if (k>L->tlj) {
	Dprintf(" DBICCprod\n", k, LL->brj);
	
	DB_ICCprod(k, -1,
		   LL,
		   nk, jak, rak,
		   tabA, tabC,
		   E, NULL, W);
	
      }
      /* Repère B */









     
      /*** Factorize the diagonal block matrix of the column k ***/
      /* kk = BL->block_index[k]-BL->block_index[LL->tlj]; unused */

      Dprintf("Facto du id=%d\n", LL->ca[LL->cia[k]] - LL->ca[0]);
      VS_ICCT(csL, /* F */E, W); /* todo : simplifier, ce n'est qu'un bloc dense dans S */

    
      /*** Divide the column block matrices by the lower triangular
	   block diagonal factor ***/
      for(ii=LL->cia[k]+1;ii<LL->cia[k+1];ii++) {
	i = LL->cja[ii];
	
	Dprintf("Div de id=%d par id=%d\n", LL->ca[ii] - LL->ca[0], LL->ca[LL->cia[k]] - LL->ca[0]);

	
	VS_InvLT(1, csL, LL->ca[ii], W); /* todo : simplifier, ce n'est qu'un bloc dense dans S */
      }





      
    } /* (k=START;k<=LL->brj;k++) */
  
  free(E);
  free(W);
  free(tabA);
  free(tabC);

  /* /\* if (k==START+1) *\/ { printDBMatrix(L); exit(1); } */
}


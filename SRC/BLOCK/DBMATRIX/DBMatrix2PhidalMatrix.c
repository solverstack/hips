/* @authors J. GAIDAMOUR */

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h> /*memcpy*/

#include <assert.h>
#include "phidal_struct.h"
#include "block.h"

#include "base.h"

/**/
#ifndef COMPILE_WITH_DROP

#define MACRO_VSolverMatrix2SparRow(solvmtx, symbmtx, cs, inarow, diag, unitdiag, droptol) \
              VSolverMatrix2SparRow(solvmtx, symbmtx, cs, inarow, diag, unitdiag)

/* simplifier*/
#define MACRO_TEST (*ind != 0               && (!diag || (j>i) || (j==i && !unitdiag)))

#else

#define MACRO_VSolverMatrix2SparRow VSolverMatrix2SparRowDrop
#define MACRO_TEST (coefabs(*ind) > droptol && (!diag || (j>i) || (j==i && !unitdiag)))

#endif
/**/

#define check_(a,b,c)
#define check(a,b,c)

#ifndef COMPILE_WITH_DROP

void printAlloc(int alloc) {
  if(alloc == CBLK) {
    printfd("CBLK");
  } else {
    if(alloc == RBLK) {
      printfd("RBLK");
    }
    else {
      if(alloc == ONE) {
	printfd("ONE");
      }
      else {
	if(alloc == BLK)
	  printfd("BLK");
      }
    }
  }
}

/* - version non sym */

void testme(PhidalMatrix* P) {
  int i, i0, j;
  dim_t k;
  for(i0=0,i=P->tlj;i<=P->brj;i0++,i++) {
    /* printfd("%d/%d \n",i, P->brj); */
    for(j=P->cia[i];j<P->cia[i+1];j++) {
/*       printfd("   %d\n",P->cja[j]); */
      if (i0 != P->cja[j]) continue;

      for (k=0; k<P->ca[j]->n; k++)
	assert(P->ca[j]->nnzrow[k] != 0);
      
    }
  }
}


void PhidalMatrix_Init_fromDB(DBMatrix* M, PhidalMatrix* Copy, char* UPLO, int_t locally_nbr,  PhidalHID *BL) {
#ifdef M2
  dim_t i;

  Copy->symmetric = M->symmetric;
  Copy->csc = M->csc;     /* ÃÂÃÂ  vÃÂÃÂ©rif */
  Copy->dim1 = M->dim1; 
  Copy->dim2 = M->dim2;

  Copy->tli = M->tli;
  Copy->tlj = M->tlj;
  Copy->bri = M->bri; 
  Copy->brj = M->brj; 

  Copy->bloknbr = M->bloknbr;
  Copy->bloktab = (struct SparRow *)malloc(sizeof(struct SparRow) * Copy->bloknbr);
  Copy->virtual = 0;

  /* memcpy ? */
  Copy->cia = M->cia;
  Copy->cja = M->cja;
  Copy->ca = (struct SparRow **)malloc(sizeof(struct SparRow *) * Copy->bloknbr);

  Copy->ria = M->ria;
  Copy->rja = M->rja;
  Copy->ra = (struct SparRow **)malloc(sizeof(struct SparRow *) * Copy->bloknbr);

  for(i=0;i<Copy->bloknbr;i++) /* ou memcpy et - M->bloktab + Copy->bloktab */
    Copy->ra[i] = Copy->bloktab + (M->ra[i] - M->bloktab);
  for(i=0;i<Copy->bloknbr;i++) 
    Copy->ca[i] = Copy->bloktab + (M->ca[i] - M->bloktab);
#else
  
  PhidalMatrix_Setup2(M->tli, M->tlj, M->bri,  M->brj, UPLO, "N", locally_nbr, Copy, BL);
  
  assert(Copy->dim1 == M->dim1);
  assert(Copy->dim1 == M->dim1);
  assert(Copy->virtual == 0);
 
  Copy->symmetric = M->symmetric;
  Copy->csc = M->symmetric;
#endif /* M2 */

}


/* Get Diag AND 1/d*/
/* TODO : rÃÂÃÂ©flÃÂÃÂ©chir ÃÂÃÂ  iiglobal ? a ne pas mettre en / / et sinon ? */
void DBMatrix_GetDiag(DBMatrix* D, COEF* b) {
  SolverMatrix* solvSD=NULL;
  SymbolMatrix* SD;
  int k, i, i0, ii/* , iiglobal */;
  blas_t stride;
  dim_t p;
  COEF* ptr;
    
  if (D->alloc == ONE) {
    solvSD = &D->a[0];
  }

  /* parcours par blocs colonnes */
  /* iiglobal=0; */
  for(i0=0,i=D->tlj; i<=D->brj; i0++, i++) {
    SD = &D->ca[D->cia[i]]->symbmtx;

    if (D->alloc == CBLK) {
      solvSD = &D->a[i0];
    }

    if(D->alloc == BLK) {
      solvSD = D->ca[D->cia[i]]->solvmtx;
    }

    if (D->alloc == RBLK) {
      solvSD = &D->a[D->cja[D->cia[i]]-D->tli];
    }

    for(k=0; k<SD->cblknbr; k++) {
      p      = SD->bcblktab[k].fbloknum;        /* triangular block */
      ptr    = solvSD->coeftab + solvSD->bloktab[p].coefind;
      stride = SD->stride[k];

      for (ii=SD->ccblktab[k].fcolnum; ii<=SD->ccblktab[k].lcolnum; ii++/* , iiglobal++ */) { /* TODO : nom de variables */
	b[ii/* global */] = 1/ptr[0];
	ptr += stride + 1;
      }
    }
  }
  
}


/* Get Diag AND 1/d*/
/* TODO : rÃÂÃÂ©flÃÂÃÂ©chir ÃÂÃÂ  iiglobal ? a ne pas mettre en / / et sinon ? */
void DBMatrix_SetDiag(DBMatrix* D, COEF* b) {
  SolverMatrix* solvSD=NULL;
  SymbolMatrix* SD;
  int k, i, i0, ii/* , iiglobal */;
  blas_t stride;
  dim_t p;
  COEF* ptr;
    
  if (D->alloc == ONE) {
    solvSD = &D->a[0];
  }

  /* parcours par blocs colonnes */
  /* iiglobal=0; */
  for(i0=0,i=D->tlj; i<=D->brj; i0++, i++) {
    SD = &D->ca[D->cia[i]]->symbmtx;

    if (D->alloc == CBLK) {
      solvSD = &D->a[i0];
    }

    if(D->alloc == BLK) {
      solvSD = D->ca[D->cia[i]]->solvmtx;
    }

    if (D->alloc == RBLK) {
      solvSD = &D->a[D->cja[D->cia[i]]-D->tli];
    }

    for(k=0; k<SD->cblknbr; k++) {
      p      = SD->bcblktab[k].fbloknum;        /* triangular block */
      ptr    = solvSD->coeftab + solvSD->bloktab[p].coefind;
      stride = SD->stride[k];

      for (ii=SD->ccblktab[k].fcolnum; ii<=SD->ccblktab[k].lcolnum; ii++/* , iiglobal++ */) { /* TODO : nom de variables */
	ptr[0] = 1/b[ii/* global */];
	ptr += stride + 1;
      }
    }
  }

}
#endif /* COMPILE_WITH_DROP */

/* TODO : clean en fonction de db->alloc */

#ifndef COMPILE_WITH_DROP
void DBMatrix2PhidalMatrix(UDBMatrix* LU, PhidalMatrix* Copy, int_t locally_nbr,  PhidalHID *BL, flag_t inarow, flag_t unitdiag) 
#else
void DBMatrix2PhidalMatrixDrop(UDBMatrix* LU, PhidalMatrix* Copy, int_t locally_nbr,  PhidalHID *BL, flag_t inarow, flag_t unitdiag, REAL droptol) 
#endif
{
  int i0, i;  
  dim_t j;

  int tl, br;

  DBMatrix* L = LU->L;

  SolverMatrix *solvL=NULL;
  INTL *ia; dim_t *ja;
  VSolverMatrix **a;

  INTL *Pia; dim_t *Pja;
  struct SparRow **Pa;

  /* PhidalMatrix_Init_fromDB(L, Copy, "L", locally_nbr, BL);  fait à l'exterieur */

  assert(Copy->symmetric == 1);
  assert(Copy->csc == 1);
    
  /* if / else for desalloc order */
  if ((L->alloc == ONE) || (L->alloc == CBLK) || (L->alloc == BLK)) {
    tl = L->tlj;
    br = L->brj;

    ia = L->cia;
    ja = L->cja;
    a  = L->ca;

    Pia = Copy->cia;
    Pja = Copy->cja;
    Pa  = Copy->ca;
 
  } else {
    assert(L->alloc == RBLK);

    tl = L->tli;
    br = L->bri;

    ia = L->ria;
    ja = L->rja;
    a  = L->ra;

    Pia = Copy->ria;
    Pja = Copy->rja;
    Pa  = Copy->ra;
  }
  
  if (L->alloc == ONE) {
    solvL = &L->a[0];
  }
  
  for(i0=0,i=tl;i<=br;i0++,i++) {

    if ((L->alloc == CBLK) || (L->alloc == RBLK)) {
      solvL  = &L->a[i0];
    } 

    for(j=ia[i];j<ia[i+1];j++) {
      if (L->alloc == BLK) {
	solvL = a[j]->solvmtx;
      }

      initCS(Pa[j], a[j]->symbmtx.nodenbr);
      if (solvL->coefnbr > 0)
	MACRO_VSolverMatrix2SparRow(solvL, &a[j]->symbmtx, Pa[j], inarow, i == ja[j], unitdiag, droptol); 

      /* free */
      SymbolMatrix_Clean(&a[j]->symbmtx); /* symbmtx in L->bloktab */
      if (L->alloc == BLK) {
	SolverMatrix_Clean(solvL); /* solvmtx in L->a, clean also solvL->symbmtx */
	free(solvL);
      }
    }
    
    /* free */
    if ((L->alloc == CBLK) || (L->alloc == RBLK)) {
      SolverMatrix_Clean(solvL); /* solvmtx in L->a, clean also solvL->symbmtx */
    }
  }
    

  /* free */
  free(L->bloktab); L->bloktab = NULL; /* has been freed */
  if (L->alloc != BLK) {
    free(L->a); L->a = NULL;             /* has been freed */
  }

#ifdef M2 /* Ce n'est plus rÃ©cupÃ©rÃ© ... */
  L->cia = NULL;
  L->cja = NULL;
  L->ria = NULL;
  L->rja = NULL;
#endif

  DBMatrix_Clean(L);

#ifdef DUMP
  dumpPhidalMatrix(Copy);
  exit(1);
#endif
}

#ifndef COMPILE_WITH_DROP
void VSolverMatrix2SparRow(SolverMatrix* solvmtx, SymbolMatrix* symbmtx, csptr cs, flag_t inarow, flag_t diag, flag_t unitdiag) 
#else
void VSolverMatrix2SparRowDrop(SolverMatrix* solvmtx, SymbolMatrix* symbmtx, csptr cs, flag_t inarow, flag_t diag, flag_t unitdiag, REAL droptol)
#endif
  {
  int k, i, i2, p, j;
  COEF* ind;
  int *nnzrow;
 
  assert(cs->n == symbmtx->nodenbr);
  nnzrow = (int*)malloc(sizeof(int)*cs->n);
  memset(nnzrow, 0, sizeof(int)*cs->n); /* useful for "continue;" */

  /* compute cs->nnzrow[i] for each i */
  i=0;
  for(k=0; k<symbmtx->cblknbr; k++) {
    p = symbmtx->bcblktab[k].fbloknum;
    if (p > symbmtx->bcblktab[k].lbloknum) {
      i+= symbmtx->ccblktab[k].lcolnum - symbmtx->ccblktab[k].fcolnum+1;
      continue;
    }

    ind = solvmtx->coeftab + solvmtx->bloktab[p].coefind;
    
    for(i2 = symbmtx->ccblktab[k].fcolnum; i2<=symbmtx->ccblktab[k].lcolnum; i++,i2++) { /* todo : i2 */      
      assert(i<cs->n);
      nnzrow[i] = symbmtx->hdim[k];

      /* triangular block */
      p=symbmtx->bcblktab[k].fbloknum;
      /** todo  + rapide ? */
      /* nnzrow[i] -= i2 - symbmtx->bloktab[p].frownum; */
/*       ind += i2 - symbmtx->bloktab[p].frownum; */
/*       for(j=i2; j<=symbmtx->bloktab[p].lrownum/\* - symbmtx->tli *\/; j++) { */
/* 	if (*ind == 0) { */
/* 	  nnzrow[i]--; */
/* 	} */
/* 	ind++; */
/*       } */

      /* take into account zeros */
      for(; p<=symbmtx->bcblktab[k].lbloknum; p++) {
/* 	printfd("k= %d i2 = %d p = %d/%d\n",k,i2,p, symbmtx->bcblktab[k].lbloknum); */
/* 	printfd("%d %d\n",symbmtx->bloktab[p].frownum - symbmtx->tli,symbmtx->bloktab[p].lrownum - symbmtx->tli); */

	for(j=symbmtx->bloktab[p].frownum - symbmtx->tli; j<=symbmtx->bloktab[p].lrownum - symbmtx->tli; j++) {
	  if (!MACRO_TEST) 
	    { 
	      nnzrow[i]--;
	    }
	  ind++;
	}
      }

      ind += symbmtx->stride[k] - symbmtx->hdim[k];
    }
  }

  /* allocation of cs->ma, cs->ja */
  cs->inarow = inarow; /* todo : si dÃÂÃÂ©jÃÂÃÂ  allouÃÂÃÂ©, vÃÂÃÂ©rifier que inarow == cs->inarow*/
  if (inarow == 1) { /* store the matrix in two contigue block of memory */
    COEF* matab;
    dim_t *jatab;
    assert(1); /* non tester, normalement ok */
    int size;
    
    size = 0;
    for(i=0; i<cs->n; i++) {
      size += cs->nnzrow[i];
      size += nnzrow[i];
    }

    if (size > 0) {
      matab = (COEF*)malloc(sizeof(COEF)*size);
      jatab = (dim_t*)malloc(sizeof(dim_t)*size);
    } else {
      matab = NULL;
      jatab = NULL;
    }

    if ((cs->matab != NULL) && (cs->jatab != NULL)) {
      size = 0;
      for(i=0; i<cs->n; i++) {
	  memcpy(matab + size, cs->ma[i], sizeof(COEF)*cs->nnzrow[i]);	
	  memcpy(jatab + size, cs->ja[i], sizeof(dim_t)*cs->nnzrow[i]);	
	  size += cs->nnzrow[i] + nnzrow[i];
      }
      
      free(cs->matab);
      free(cs->jatab);
    }
	
    cs->matab = matab;
    cs->jatab = jatab;

    size = 0;
    for(i=0; i<cs->n; i++) {
      if ((nnzrow[i] != 0) || (cs->nnzrow[i] != 0)) {
	cs->ma[i] = cs->matab + size;
	cs->ja[i] = cs->jatab + size;
	size += cs->nnzrow[i] + nnzrow[i];
      } else {
	cs->ma[i] = NULL; /* c'est ca ? */
	cs->ja[i] = NULL;
      }
    }
    
  } else {
/*     int ii; */

    for(i=0; i<cs->n; i++) {
      if ((nnzrow[i] != 0) || (cs->nnzrow[i] != 0)) {
/* 	printfd("realloc %d %p %d %d\n", i, cs->ma[i], cs->nnzrow[i],nnzrow[i]); */
/* 	for(ii=0; ii<cs->nnzrow[i]; ii++) { */
/* 	  assert(cs->ma[i][ii] != 0); */
/* 	  printfd("%0.9f\n",cs->ma[i][ii]); */

/* 	} */
	/* free(cs->ma[i]); cs->ma[i] = NULL; */

	assert(cs->nnzrow[i]+nnzrow[i]>=0);

	cs->ma[i] = (COEF*)realloc(cs->ma[i], sizeof(COEF)*(cs->nnzrow[i]+nnzrow[i]));
/* 	printfd("ja : %p %d %d\n", cs->ja[i], cs->nnzrow[i], nnzrow[i]); */
	assert(cs->nnzrow[i]+nnzrow[i]>=0);
	cs->ja[i] = (int*) realloc(cs->ja[i], sizeof(int) *(cs->nnzrow[i]+nnzrow[i]));
      } else {
	cs->ma[i] = NULL;
	cs->ja[i] = NULL;
      }
    }
    
  }

  /* now, fill phidalmatrix */
  i=0;
  for(k=0; k<symbmtx->cblknbr; k++) {
    p = symbmtx->bcblktab[k].fbloknum;
    if (p > symbmtx->bcblktab[k].lbloknum) {
      i+= symbmtx->ccblktab[k].lcolnum - symbmtx->ccblktab[k].fcolnum+1;
      continue;
    }

    ind = solvmtx->coeftab + solvmtx->bloktab[p].coefind;	
    
    for(i2 = symbmtx->ccblktab[k].fcolnum; i2<=symbmtx->ccblktab[k].lcolnum; i++,i2++) { /* todo : i2 */      

      /* coef */
      for(p=symbmtx->bcblktab[k].fbloknum; p<=symbmtx->bcblktab[k].lbloknum; p++) {
	
	for(j=symbmtx->bloktab[p].frownum - symbmtx->tli; j<=symbmtx->bloktab[p].lrownum - symbmtx->tli; j++) {
	  if MACRO_TEST { 
	    cs->ma[i][cs->nnzrow[i]] = *ind; /* or memcpy */
	    cs->ja[i][cs->nnzrow[i]] = j;
	    cs->nnzrow[i]++;
	  }
	  ind++;
	}
      }
      ind += symbmtx->stride[k] - symbmtx->hdim[k];
    }
  }

  free(nnzrow);

  /* non zero rows */
  CS_SetNonZeroRow(cs); /* mÃÂÃÂªme pour L block diag en unsym pour funct. Transpose */

#ifdef DEBUG_M
  /* zero terms have been removed */
  for(i=0; i<cs->n; i++) {
    for(j=0; j<cs->nnzrow[i]; j++) {
      assert(cs->ma[i][j] != 0);
    }
  }

  /* use SparRow2SolverMatrix to test this function */
#endif

}

#ifndef COMPILE_WITH_DROP
void DBMatrix2PhidalMatrix_Unsym(UDBMatrix* LU, PhidalMatrix* Copy, int_t locally_nbr,  PhidalHID *BL, flag_t inarow)
#else
void DBMatrix2PhidalMatrix_UnsymDrop(UDBMatrix* LU, PhidalMatrix* Copy, int_t locally_nbr,  PhidalHID *BL, flag_t inarow, REAL droptol)
#endif
{
  int i0, i;  
  int j,j2;

  int tl, br;
  DBMatrix* L = LU->L;
  DBMatrix* U = LU->U;

  assert(L != U);

  SolverMatrix *solvL=NULL, *solvU=NULL;
  INTL *Lia; dim_t *Lja;
  INTL *Uia; dim_t *Uja;
  VSolverMatrix **La;
  VSolverMatrix **Ua;

  INTL *PLia; dim_t *PLja;
  struct SparRow **PLa;
  INTL *PUia; dim_t *PUja;
  struct SparRow **PUa;

  /* PhidalMatrix_Init_fromDB(L, Copy, "N", locally_nbr, BL); */

  /*   DBMatrix_Transpose(U); /\* faire dans le bon ordre pour U *\/ */

  /* if / else for desalloc order */
  if ((L->alloc == ONE) || (L->alloc == CBLK) || (L->alloc == BLK)) {
    tl = L->tlj;
    br = L->brj;

    Lia = L->cia;
    Lja = L->cja;
    La  = L->ca;

    Uia = U->ria;
    Uja = U->rja;
    Ua  = U->ra;

/*     assert(L->cia == U->ria); */
/*     assert(L->cja == U->rja); */

/*     printAlloc(L->alloc); */
/*     printfd("\n"); */

    /* TODO assert(L->ca == U->ra); */

/*     Uia = U->cia; */
/*     Uja = U->cja; */
/*     Ua  = U->ca; */

    PLia = Copy->cia;
    PLja = Copy->cja;
    PLa  = Copy->ca;

    PUia = Copy->ria;
    PUja = Copy->rja;
    PUa  = Copy->ra;

/*     PUia = Copy->cia; */
/*     PUja = Copy->cja; */
/*     PUa  = Copy->ca; */
 
  } else {
    exit(1);
  }
  
  if (L->alloc == ONE) {
    solvL = &L->a[0];
    solvU = &U->a[0];
  }
  
  {
/*     for(i0=0,i=tl;i<=br;i0++,i++) { */
/*       for (j=Lia[i]; j<Lia[i+1]; j++) */
/* 	printfd("DB L : [%d %d]\n", i, Lja[j]); */

/*       for (j=Uia[i]; j<Uia[i+1]; j++) */
/* 	printfd("DB U : [%d %d]\n", i, Uja[j]); */

/*       for (j=PLia[i]; j<PLia[i+1]; j++) */
/* 	printfd("PH PL : [%d %d]\n", i, PLja[j]); */

/*       for (j=PUia[i]; j<PUia[i+1]; j++) */
/* 	printfd("PH PU : [%d %d]\n", i, PUja[j]); */
      
/*       printfd("--\n"); */
/*     } */

#define DEBUGL_
#ifdef DEBUGL_
    /* ==== L ==== */
    for(i0=0,i=tl;i<=br;i0++,i++) {
      if ((/* L */L->alloc == CBLK) || (L->alloc == RBLK)) {
	solvL = &L->a[i0];
      } 
        
      j  = Lia[i];
      j2 = PLia[i]; 
    
      /* amÃÂÃÂ©liorer, on peut savoir lequel correspond ... */
      while(j<Lia[i+1] && j2<PLia[i+1]) {
        if (Lja[j] < PLja[j2]) { j++;  continue; }
        if (Lja[j] > PLja[j2]) { j2++; continue; }
      
	assert(Lja[j] == PLja[j2]);
    
	if (L->alloc == BLK) {
	  solvL = La[j]->solvmtx;
	}

/* 	printfd("PhidalMatrix : bloc : colonne : %d ligne : %d\n", i, PLja[j2]); */
	check(L, solvL, &La[j]->symbmtx);
	
/* 	printfd("L : PhidalMatrix : bloc : colonne : %d ligne : %d\n", i, PUja[j2]); */
	initCS(PLa[j2], La[j]->symbmtx.nodenbr);
	MACRO_VSolverMatrix2SparRow(solvL, &La[j]->symbmtx, PLa[j2], inarow, i == Lja[j], 0, droptol);

/* 	/\* free *\/ */
/* 	SymbolMatrix_Clean(&La[j]->symbmtx); /\* symbmtx in L->bloktab *\/ */
/* 	if (L->alloc == BLK) { */
/* 	  SolverMatrix_Clean(solvL); /\* solvmtx in L->a, clean also solvL->symbmtx *\/ */
/* 	} */

	j++;
	j2++;
      }

      if ((/* L */L->alloc == CBLK) || (L->alloc == RBLK)) {
	solvL->coefnbr = 0;
	free(solvL->coeftab);
	solvL->coeftab = NULL;
      } 
    
/*       /\* free *\/ */
/*       if ((L->alloc == CBLK) || (L->alloc == RBLK)) { */
/* 	SolverMatrix_Clean(solvL); /\* solvmtx in L->a, clean also solvL->symbmtx *\/ */
/*       } */
    }

    PhidalMatrix_Transpose(Copy); /* pour que BL correspond */
    PhidalMatrix_Transpose_SparMat(Copy, /*job*/3, "U", BL);
    PhidalMatrix_Transpose(Copy);
#endif

#define DEBUGU_
#ifdef DEBUGU_
    /* ==== U ==== */ 
    for(i0=0,i=tl;i<=br;i0++,i++) {
      if ((/* L */U->alloc == CBLK) || (U->alloc == RBLK)) {
/* 	printfd("solvU=%d\n", i0); */
	solvU = &U->a[i0];
      } 
        
      j  = Uia[i];
      j2 = PUia[i]; 
    
      /* amÃÂÃÂ©liorer, on peut savoir lequel correspond ... */
      while(j<Uia[i+1] && j2<PUia[i+1]) {
        if (Uja[j] < PUja[j2]) { j++;  continue; }
        if (Uja[j] > PUja[j2]) { j2++; continue; }
      
	assert(Uja[j] == PUja[j2]);
    
	if (U->alloc == BLK) {
	  solvU = Ua[j]->solvmtx;
	}

/* 	printfd("PhidalMatrix : bloc : colonne : %d ligne : %d\n", i, PUja[j2]); */

/* 	if ((/\* L *\/U->alloc == CBLK) || (U->alloc == RBLK)) { */
/* 	  printfd("solvU=%d\n", Uja[j]); */
/* 	  solvU = &U->a[Uja[j]]; */
/* 	}  */
	
	check_(U, solvU, &Ua[j]->symbmtx);
	check(U, solvU, &Ua[j]->symbmtx);

	if (Uja[j] != i) 
	  initCS(PUa[j2], Ua[j]->symbmtx.nodenbr);
	else assert(PUa[j2]->inarow == 0);
	/* else assert(PUa[j2]->n == Ua[j]->symbmtx.nodenbr); */
	MACRO_VSolverMatrix2SparRow(solvU, &Ua[j]->symbmtx, PUa[j2], inarow, i == Uja[j], 1, droptol);
	assert(PUa[j2]->inarow == 0);
	assert(PUa[j2]->n != 0);

	/* DEBUG */
/* 	csptr amat = PUa[j2]; */
/* 	int k, len; */
/* 	printfd("nnzr = %d\n", amat->nnzr); */
/* 	for (k=0; k<amat->nnzr; k++)  */
/* 	  { */
/* 	    j = amat->nzrtab[k]; */
/* 	    len = amat->nnzrow[j]; */
/* 	    printfd("k=%d len = %d %p\n", k, len, amat->ja[j]); */
/* 	  } */
/* 	printfd("--fin\n"); */

/* 	ascend_column_reorder(PUa[j2]); */

	/* free */
/* 	SymbolMatrix_Clean(&Ua[j]->symbmtx); /\* symbmtx in U->bloktab *\/ */
/* 	if (U->alloc == BLK) { */
/* 	  SolverMatrix_Clean(solvU); /\* solvmtx in U->a, clean also solvU->symbmtx *\/ */
/* 	} */

	j++;
	j2++;
      }
    
/*       /\* free *\/ */
/*       if ((U->alloc == CBLK) || (U->alloc == RBLK)) { */
/* 	SolverMatrix_Clean(solvU); /\* solvmtx in U->a, clean also solvU->symbmtx *\/ */
/*       } */
    }

#endif
  
  }


  /* free */
  free(L->bloktab); L->bloktab = NULL; /* has been freed */
  free(L->a); L->a = NULL;             /* has been freed */
#ifdef M2 /* Ce n'est plus rÃ©cupÃ©rÃ© ... */
  L->cia = NULL;
  L->cja = NULL;
  L->ria = NULL;
  L->rja = NULL;

  U->cia = NULL;
  U->cja = NULL;
  U->ria = NULL;
  U->rja = NULL;
#endif

  DBMatrix_Clean(L); 

  free(U->a);
  free(U->bloktab);
  free(U->ra);
  free(U->ca);

  U->bloktab = NULL;
  U->a = NULL;
  DBMatrix_Clean(U);

/*   { */
/*     PhidalMatrix* A = Copy; */
/*     PhidalMatrix L; */
/*     PhidalMatrix U; */

/*     PhidalMatrix_Setup(A->tli, A->tlj, A->bri, A->brj, "U", "N", locally_nbr, &U, BL); */
/*     PhidalMatrix_Copy(A, &U, BL); */
    
/*     PhidalMatrix_Setup(A->tli, A->tlj, A->bri, A->brj, "L", "N", locally_nbr, &L, BL); */
/*     PhidalMatrix_Copy(A, &L, BL); */

/*     printfd("TEST A : \n"); */
/*     testme(A); */
/*     printfd("TEST L : \n"); */
/*     testme(&L); */
/*     printfd("TEST U : \n"); */
/*     testme(&U); */
/*   } */
   
/*   PhidalMatrix* a = Copy; */
/*   csptr b; */
/*   int ii; */
/*   for(i0=0,i=a->tli; i<=a->bri; i0++, i++) { */
/*     for(j=a->cia[i];j<a->cia[i+1];j++) { */
/*       printfd("CS_SetNonZeroRow on : %d %d (%d)\n", i, a->cja[j], j); */
/*       b = Copy->ca[j]; */
/*       ascend_column_reorder(b);       */
/*       bzero (b->nzrtab, sizeof(int)*b->n); */
/*       CS_SetNonZeroRow(b); */
/*       for(ii=0;ii<b->n; ii++) { */
/* 	printfd("nzrtab de %d : %d\n",ii, b->nzrtab[ii]); */
/*       } */

/*     } */
/*   } */

    assert(Copy->symmetric == 0);
#ifdef DUMP
  dumpPhidalMatrix(Copy);
  exit(1);
#endif

}

/* @authors J. GAIDAMOUR */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>

#include "phidal_parallel.h"
#include "db_parallel.h"
#include "block.h"

#define CSnormFrob(arg) 1.0

void DB_ICCprod2(int iB, COEF alpha, 
		 DBDistrMatrix* A, COEF* diag,
		 int nk, dim_t *jak, VSolverMatrix** rak,
		 int *tabA, int *tabC, 
		 COEF* E, COEF* F, COEF* W, PhidalHID* BL);

void VSolverMatrix_GetDiag(VSolverMatrix* D, COEF* b);

void DBDistrMatrix_ICCT_Restrict(int START, flag_t job, DBDistrMatrix *L, PhidalDistrHID *DBL, PhidalOptions *option);

void DBDistrMatrix_ICCT(flag_t job, DBDistrMatrix *L, PhidalDistrHID *DBL, PhidalOptions *option)
{
  /******************************************************************************************/
  /* This function computes the incomplete factorization A = L.D.Lt of a symmetric matrix   */
  /* On entry:                                                                              */
  /*   L is the lower triangular part of the matrix to factorize in CSC format              */
  /*   job == 0 : the shared block are freed on the non-leader                              */
  /*   processors otherwise they stay in memory (need this when to use                      */
  /* L^-1.M in multilevel recursion)                                                        */
  /* on return:                                                                             */
  /* L the strictly lower triangular factor (unitary on the diagonal not stored) in CSC     */
  /* D is the INVERSE of the diagonal factor                                                */
  /* NOTE: L is sorted by row index on return                                               */
  /******************************************************************************************/
#ifdef DEBUG_M
  assert(L->M.symmetric == 1);
#endif
  DBDistrMatrix_ICCT_Restrict(L->M.tli, job, L, DBL, option);
}

void DBDistrMatrix_ICCT_Restrict(int START, flag_t job, DBDistrMatrix *L, PhidalDistrHID *DBL, PhidalOptions *option)
{
  /************************************************/
  /* THE BLOCK ARE FREED IN THE COUPLING MATRIX   */
  /* ie in ALL ROW BEFORE START                   */
  /************************************************/
  COEF* D; /* tmp, pour que ca compil */
  dim_t i, j, k;
  int nk, *jak, *rindk;
  VSolverMatrix* *rak;
  int ii, jj, kk;
  int *wki1, *wki2;
  COEF *wkd;
  VSolverMatrix* csL;
  int nnb;
  DBMatrixFactComm FC;
  PhidalHID *BL;
  DBMatrix *LL;
  /*   MPI_Request *diag_rqtab; */

  COEF *E, *F, *W;
  int *tabA, *tabC;

  LL = &(L->M);
  BL = &(DBL->LHID);

  
/*   printDBMatrix(LL, L->proc_id, "init"); */


#ifdef DEBUG_M
  assert(COMM_AHEAD >= 1);
  assert(START >= LL->tli);
  assert(START <= LL->bri);
#endif

  /*** Allocate the communicator of the matrix ***/  
  DBMatrixFactComm_Setup(&FC, L, DBL);

/* #define TRACE_COM */
#ifdef TRACE_COM

  for(i=LL->tli;i<=LL->bri;i++)
    for(j=LL->cia[i];j<LL->cia[i+1];j++)
      {
	if(L->cind[j] < 0)
	  {
	    fprintfd(stderr, "Block L (%d %d) lead %d \n",  DBL->loc2glob_blocknum[LL->cja[j]], DBL->loc2glob_blocknum[i], L->clead[j]);
	  }
	else
	  {
	    fprintfd(stderr, "Block L (%d %d) lead %d CIND %d nbr %d (loc = %d %d)\n",  DBL->loc2glob_blocknum[LL->cja[j]], DBL->loc2glob_blocknum[i], L->clead[j], L->cind[j], L->pset_index[L->cind[j]+1]-L->pset_index[L->cind[j]], LL->cja[j], i);
	  }
      }
#endif

/* #ifdef COMM_ONE */
  /*** Poste receive for contribution blocks **/ 
  DBMatrixFactComm_PosteCtrbReceive(&FC, L, START, LL->brj);
/* #endif */

  /*** Create request for receive of diagonal term from leader
       processors ***/
  /*   diag_rqtab = (MPI_Request *)malloc(sizeof(MPI_Request)*(LL->bri-START+1)); */
  /*   DBposte_DiagRcv(START, LL->bri, D, diag_rqtab, L, DBL); */

  E = (COEF *)malloc(sizeof(COEF)*LL->coefmax);
  assert(E != NULL);
  
/*   F = (COEF *)malloc(sizeof(COEF)*LL->coefmax); /\* TODO *\/ */
/*   assert(F != NULL); */
  
  W = (COEF *)malloc(sizeof(COEF)*LL->coefmax); /* TODO */
  assert(W != NULL);

  tabA = (int*)malloc(sizeof(int)*LL->dim1);
  tabC = (int*)malloc(sizeof(int)*LL->dim1);

  D = (COEF *)malloc(sizeof(COEF)*LL->dim1);
#ifdef DEBUG_M
  memset(D, 0, sizeof(COEF)*LL->dim1);
#endif

/* #ifndef OLD_COMM_AHEAD */
/*   DBMatrixFactComm_PosteCtrbReceive(&FC, L, START, MIN(START+COMM_AHEAD-1, LL->brj)); */
/* #endif */
  
  for(k=START;k<=LL->brj;k++)
    {
      /*** Poste receive for contribution blocks in this column **/ 
      /*fprintfv(5, stderr, "Poste ctrb for column block %d \n", k);*/
#warning TODO
/* #ifdef OLD_COMM_AHEAD */
/*       if ((k-START) % COMM_AHEAD == 0) */
/* 	DBMatrixFactComm_PosteCtrbReceive(&FC, L, k, MIN(k+COMM_AHEAD-1, LL->bri)); */
/* #else */
/*       printf("~~~ %d %d %d\n", k, COMM_AHEAD, LL->bri); */
/*       if(k+COMM_AHEAD <= LL->bri) { */
/*       assert(0); */
/* 	printf("DBMatrixFactComm_PosteCtrbReceive(&FC, L, k+COMM_AHEAD, MIN(k+COMM_AHEAD, LL->bri)); COMM_AHEAD=%d k=%d LL->bri=%d\n", COMM_AHEAD, k, LL->bri); */
/* 	DBMatrixFactComm_PosteCtrbReceive(&FC, L, k+COMM_AHEAD, MIN(k+COMM_AHEAD, LL->bri)); */
/*       } */
/* #endif */
      /*fprintfv(5, stderr, "Poste diag for column block %d \n", k);
	DBposte_DiagRcv(k, k, D, diag_rqtab, L, DBL);*/

      //if (k==START+1) printDBMatrix(LL, L->proc_id, "k2-deb");

      /*** Factorize the column block k ***/
      csL =  LL->ca[ LL->cia[k] ];

      /*** OIMBE: il faut enlever ca pour que ca marche 
	   en multiniveau; sinon ca deadlock
	   if(k>LL->tlj) ***/
      {
	/**************************************************/
	/* L(k,k) = L(k,k) - L(k, 0:k-1).D-1.L(k,0:k-1)t **/
	/**************************************************/
	      
	/** Compute the product  
	    L(k,k) = L(k,k) - L(k,0:k-1).D-1.L(k, 0:k-1)t  ONLY
	    for LOCAL block **/

	//CSCrowICCprod(0.0, NULL, -1.0, nk, rak, csL, D2, wki1, wki2, wkd, celltab, cellptrtab);  /*** NO DROPPING HERE ***/
	DB_ICCprod2(k, -1,
		    L, D,
		    nk, jak, rak,
		    tabA, tabC,
		    E, NULL, W, BL);


#ifdef NON
	/*** The CSCrowICCprod function also divides the block by the
	     diagonal; so we have to do it for non local block that
	     have not been processed by ICCprod;
	     CSCrowICCprod also sorts the matrix ***/
	for(jj=LL->ria[k];jj<LL->ria[k+1]-1;jj++) 
	  if(L->rlead[jj] != L->proc_id /* && LL->ra[jj]->nnzr > 0 */)
	    {
	      j = LL->rja[jj];
	      kk = BL->block_index[j] - BL->block_index[LL->tli];
	      //ascend_column_reorder(LL->ra[jj]);
	      /** CSC matrix so CS_RowMult means that the column are
		  divided **/
	      //CS_RowMult(D+kk, LL->ra[jj]);
	    }
#endif

	/** Send the diagonal block contribution to the leader  **/
	if(L->clead[LL->cia[k]] != L->proc_id)
	  {
	    DBsend_matrix(csL, L->cind[ LL->cia[k]], CTRB_TAG, &FC);
	    reinitVSolverMatrix(csL);  /* reinitCS(csL); */
	    
	    /** Poste the receive for the factorized matrix that
		the leader processor will send **/
	    DBposte_block_receive(L->cind[LL->cia[k]], LEAD_TAG,  &FC);
	  }
	
	/************************************************/
	/** L(i,k) = L(i,k) - L(i, 0:k-1).L(k, 0:k-1)t **/
	/************************************************/
	/** This time, we need to put in jak all the non null blocks
	    in row k (local and non local) **/
	
	
	/** Treat the contribution to non local block in priority **/
	for(ii=0;ii<LL->cia[k+1]-LL->cia[k]-1;ii++)
	  {
	    jj = ii+LL->cia[k]+1;
	    if(L->clead[jj] != L->proc_id)
	      {
		i = LL->cja[jj];
		/*** Compute L(i,k) = L(i,k) - L(i, 0:k-1).L(k, 0:k-1)t **/	      
		/*** NO DROPPING HERE ***/
		
		/* 		    CSCrowMultCSRcol(0.0, NULL, -1.0, nnb, -1.0, csrtab1, csrtab2,  */
		/* 				     LL->ca[jj], BL->block_index[i+1]-BL->block_index[i],  */
		/* 				     wki1, wki2, wkd, celltab, cellptrtab);  */
		
		
		/** Send the contrib to the leader processor of this
		    matrix **/
		DBsend_matrix(LL->ca[jj], L->cind[jj], CTRB_TAG, &FC); 
		reinitVSolverMatrix(LL->ca[jj]); /* reinitCS(LL->ca[jj]); */
		
		
		/** Poste the receive for the factorized matrix that
		    the leader processor will send **/
		DBposte_block_receive(L->cind[jj], LEAD_TAG, &FC);
	      }
	  }

#ifdef NON
	/** Now deals with the local blocks **/
	for(ii=0;ii<LL->cia[k+1]-LL->cia[k]-1;ii++)
	  {
	    jj = ii+LL->cia[k]+1;
	    if(L->clead[jj] == L->proc_id)
	      {
		i = LL->cja[jj];
		/*** Compute L(i,k) = L(i,k) - L(i, 0:k-1).L(k, 0:k-1)t **/	      
		/*** NO DROPPING HERE ***/
		
		/* 		    CSCrowMultCSRcol(0.0, NULL, -1.0, nnb, -1.0, csrtab1, csrtab2,  */
		/* 				     LL->ca[jj], BL->block_index[i+1]-BL->block_index[i],  */
		/* 				     wki1, wki2, wkd, celltab, cellptrtab); */

		  
	      }
	  }
#endif
      } 
      
      /** Deallocate blocks which column indices are < START **/
      for(ii=LL->ria[k];ii<LL->ria[k+1];ii++)
      	{
      	  if(LL->rja[ii] >= START)
      	    break;
	  
      	  if(L->rlead[ii] == DBL->proc_id)
      	    {
	      reinitVSolverMatrix(LL->ra[ii]); /* reinitCS(LL->ra[ii]); */
      	    }
      	}
      
      //if (k==START+1) printDBMatrix(LL, L->proc_id, "k2-deb2");

      //if (k==START) printDBMatrix(LL, L->proc_id, "k1-facto1");

      /*** Factorize the diagonal block matrix of the column k ***/
      kk = BL->block_index[k]-BL->block_index[LL->tlj];

      if(L->clead[ LL->cia[k]] == L->proc_id)
	{


	  /*** Receive contribution for the diagonal block ***/
	  if(L->cind[LL->cia[k]] >= 0)
	    DBreceive_contrib(SYNCHRONOUS, csL, L->cind[LL->cia[k]], &FC);

	  /* CS_ICCT(csL, D+kk, droptol, droptab+kk, fillrat, wki1, wki2, wkd, celltab, cellptrtab, option->shiftdiag); */
	  VS_ICCT(csL, /* F */E, W);

	  //if (k==START) printDBMatrix(LL, L->proc_id, "k1-facto2");

	  /** Send L to the other (non leader) processors **/
	  if(L->cind[LL->cia[k]] >= 0)
	    {
	      /** Send the diagonal D to the other processor **/
#ifdef TRACE_COM
/* 	      fprintfv(5, stderr, "Proc %d send diagonal %d \n", L->proc_id, DBL->loc2glob_blocknum[k]); */
#endif

	      /* send_diagonal(D+kk,  BL->block_index[k+1]-BL->block_index[k], L->cind[LL->cia[k]], &FC); */

#ifdef TRACE_COM
	      fprintfv(5, stderr, "PROC %d SEND CSL[%d]\n", DBL->proc_id,  DBL->loc2glob_blocknum[k]);
#endif

	      DBsend_matrix(csL, L->cind[LL->cia[k]], LEAD_TAG, &FC);
	    }

	}
      else
	{

#ifdef TRACE_COM
/* 	  fprintfv(5, stderr, "Proc %d receive diag %d \n", L->proc_id, DBL->loc2glob_blocknum[k]); */
#endif
	  /* /\** Receive the diagonal **\/ */
	  /* 	  if(MPI_Wait(diag_rqtab+k-START, FC.status)) */
	  /* 	    { */
	  /* 	      fprintfv(5, stderr, "Proc %d Error in DBDistrMatrix_ICCT for vector %d \n" ,DBL->proc_id, k); */
	  /* 	      MPI_Abort(DBL->mpicom, -1); */
	  /* 	    } */

#ifdef TRACE_COM
	  fprintfv(5, stderr, "PROC %d receive CSL[%d] = %g \n", DBL->proc_id, DBL->loc2glob_blocknum[k], CSnormFrob(csL));
#endif	  

	  /** Receive the factorized diagonal block **/ /*ICI*/
	  DBreceive_matrix(SYNCHRONOUS, csL, L->cind[LL->cia[k]], &FC); 
	  
	}

      //if (k==START) printDBMatrix(LL, L->proc_id, "k1-div1");

      /*** Divide the column block matrices by the lower triangular
	   block diagonal factor ***/
      /*** Each processor only treat the block where it is the leader **/
      for(ii=LL->cia[k]+1;ii<LL->cia[k+1];ii++)
	if(L->clead[ii] == L->proc_id)
	  {
	    i = LL->cja[ii];

	    /*** Receive the contributions for this block ****/
	    if(L->cind[ii] >= 0)
	      DBreceive_contrib(SYNCHRONOUS, LL->ca[ii], L->cind[ii], &FC);

	    /* 	    CSC_CSR_InvLT(csL, LL->ca[ii],  BL->block_index[i+1]-BL->block_index[i], droptol, droptabtmp+kk,  */
	    /* 			  fillrat, wki1, wki2, wkd, celltab, cellptrtab); */

	    VS_InvLT(1, csL, LL->ca[ii], W); 

	    /** Send the factorized matrix to the other (non leader) processors  **/
	    if(L->cind[ii] >= 0)
	      DBsend_matrix(LL->ca[ii], L->cind[ii], LEAD_TAG, &FC);
	  }
      
      
            /**** Deallocate the non local block in L(k,:) *****/
      if(job == 0)
      	for(ii=LL->ria[k];ii<LL->ria[k+1];ii++)
      	  if(L->rlead[ii] != DBL->proc_id)
      	    {
      	      /* reinitCS(LL->ra[ii]); */
	      /* TODO */
	      if (LL->rja[ii] != k) {
		reinitVSolverMatrix(LL->ra[ii]);
	      } else {
		int kk = BL->block_index[k]-BL->block_index[LL->tlj];
		/* printf("BKP Diag ! kk=%d (k=%d tlj=%d)\n", kk, k, LL->tlj); */
		VSolverMatrix_GetDiag(LL->ra[ii], D+kk) ;
		reinitVSolverMatrix(LL->ra[ii]);
	      }
      	    }
      
   



      /** Receive the non-local block in column L(k+1:n , k) **/
      /** @@ OIMBE on peut faire qqchose de + compliquer pour retarder
	  la reception de ces blocs ***/
      for(ii=LL->cia[k]+1;ii<LL->cia[k+1];ii++)
	if(L->clead[ii] != L->proc_id)
	  {
	    i = LL->cja[ii];

	    /*** Receive the matrix from the leader processor ****/
	    DBreceive_matrix(SYNCHRONOUS, LL->ca[ii], L->cind[ii], &FC);

	  }

      //if (k==START) printDBMatrix(LL, L->proc_id, "k1-div2");

    }
  
  /** Deallocate the communicators **/
  DBMatrixFactComm_Clean(&FC);
  /* if(diag_rqtab != NULL) */
  /* 	free(diag_rqtab); */
  
  /* externalisation complète de D (pour le solve) de manière non intrusive */
  for(k=START;k<=LL->brj;k++)
    {
      csL =  LL->ca[ LL->cia[k] ];

      if(L->clead[LL->cia[k]] == L->proc_id) { /* else : D déjà externalisée */
	int kk = BL->block_index[k]-BL->block_index[LL->tlj];
	VSolverMatrix_GetDiag(csL, D+kk) ;
      }
    }
  
  L->extDiag = D; /* free(D); */

  free(W);  
  free(E);  
  free(tabA);
  free(tabC);

/*   printDBMatrix(LL, L->proc_id, "fin"); */

}


/* TODO : cf DBMatrix_GetDiag pour alloc != BLK */
void VSolverMatrix_GetDiag(VSolverMatrix* D, COEF* b) {
  /* assert(alloc == BLK); */
  int m;
  int cdim;
  SymbolMatrix* symbLs;
  SolverMatrix* solvL;

  symbLs   = &D->symbmtx;
  solvL    =  D->solvmtx;
  
  cdim = symbLs->ccblktab[0].lcolnum - symbLs->ccblktab[0].fcolnum + 1;
  for(m=0;m<cdim;m++)
    {
      b[m] = solvL->coeftab[m*symbLs->hdim[0] + m];
    }
}


/* TODO : reinit avec free */

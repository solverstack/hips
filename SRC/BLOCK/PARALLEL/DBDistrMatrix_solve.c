/* @authors J. GAIDAMOUR */

#include <stdio.h>
#include <stdlib.h>

#include <string.h> /* memcpy */

#include "block.h"
#include "db_parallel.h"

/* void DBDistrMatrix_Lsolv(flag_t unitdiag, DBDistrMatrix *L, COEF *x, COEF *b, PhidalHID *BL) { */
/*   DBMatrix_Lsolv(unitdiag, &L->M, x, b, BL); */
/* } */

/* void DBDistrMatrix_Usolv(flag_t unitdiag, DBDistrMatrix *U, COEF *x, COEF *b, PhidalDistrHID *DBL) { */
/*   DBMatrix_Usolv(unitdiag, &U->M, x, b, &DBL->LHID); */
/* } */

/* void DBDistrMatrix_Dsolv(DBDistrMatrix *D, COEF* b) { */
/*   DBMatrix_Dsolv(&D->M, b); */
/* } */

/* TODO : sortir xtmp */

void DBDistrMatrix_Lsolv(flag_t unitdiag, DBDistrMatrix *DL, COEF *x, COEF *b, PhidalDistrHID *DBL) {

  assert(DL->M.alloc == BLK);

  /*****************************************************************************************/
  /* This function compute x = L^-1.b                                                      */
  /* If unitdiag == 1 then the diagonal is considered to be unitary (stored or not stored) */
  /*   THIS CONCERNS THE DIAGONAL NOT THE BLOCK DIAGONAL                                   */
  /* Work also in place (i.e. x == b)                                                      */
  /*****************************************************************************************/
  dim_t i, j, k;
  int *bind;
  COEF *xptr, *xiptr;
  int offset;
  DBMatrix *L;
  PhidalHID *BL;
  DBMatrixCommVec *commvec;
  mpi_t proc_id;

  BL = &DBL->LHID;
  proc_id = DL->proc_id;
  L = &(DL->M);
  commvec = &(DL->commvec);
  bind = BL->block_index;
  offset = bind[L->tli];

#ifdef DEBUG_M
  assert(L->dim1 == L->dim2);
  assert(L->tli == L->tlj);
  assert(L->bri == L->brj);
  assert(commvec->init == 1);
#endif

  if(x != b)
    memcpy(x, b, sizeof(COEF)*L->dim1);


  /*bzero(commvec->t, sizeof(COEF)*L->dim1);*/ /*NO NEED */
  /** IMPORTANT: we store the contribution for x[i] part in x 
      so it must be initialized to zero where the processor is not
      leader **/
  for(i=L->tli;i<=L->bri;i++)
    if(DBL->row_leader[i] != proc_id)
      bzero(x + bind[i]-offset, sizeof(COEF)*(bind[i+1]-bind[i]));

  memcpy(commvec->loc_cnt, commvec->loc_ctrb, sizeof(int)*(L->bri-L->tli+1));

  /** Start the persistent receive requests **/
  for(i=L->tli;i<=L->bri;i++)
    {
      if(commvec->out_ctrb[i-L->tli] > 0)
	{
#ifdef TRACE_COM
/* 	  fprintfd(stderr, "Proc %d poste ctrb receive %d \n", DL->proc_id, DBL->loc2glob_blocknum[i]); */
#endif
	  if(MPI_Startall(commvec->out_ctrb[i-L->tli], commvec->recv_rqtab[i-L->tli]) != MPI_SUCCESS)
	    {
/* 	      fprintfd(stderr, "Error in start recv contrib for vec %d \n", i); */
	      assert(0);
	    }
	}
      if(commvec->out_ctrb[i-L->tli] == -1)
	{
#ifdef TRACE_COM
	  /* fprintfd(stderr, "Proc %d poste vector receive %d \n", DL->proc_id, DBL->loc2glob_blocknum[i]); */
#endif 
	  if(MPI_Start(commvec->recv_rqtab[i-L->tli]) != MPI_SUCCESS)
	    {
	      /* fprintfd(stderr, "Error in start recv vector for vec %d \n", i); */
	      assert(0);
	    }
	}
    }

  /** We use a column algorithm 
      that allows to overlap communication and computation ***/
  for(j=L->tlj;j<=L->brj;j++)
    {
      xptr = x+(bind[j]-offset);
#ifdef DEBUG_M
      assert(L->cja[L->cia[j]] == j);
#endif
      if(DBL->row_leader[j] == proc_id)
	{
	  if(commvec->out_ctrb[j-L->tlj] > 0)
	    {
	      /** Receive the contribution from non leader processors **/
	      DBMatrixCommVec_ReceiveVecAdd(j-L->tlj, xptr, commvec); 
	      /*fprintfd(stderr, "Proc %d receive ctrb %d = %g \n", DL->proc_id, DBL->loc2glob_blocknum[j], 
		norm2(commvec->recv_buffer[j-L->tlj], commvec->veclen[j-L->tlj]));*/
	    }

	  /** Solve **/
	  VS_Lsol(unitdiag, L->ca[L->cia[j]], xptr, xptr);
	  
	  if(DBL->row_leader[j] == proc_id && DBL->block_psetindex[j+1]-DBL->block_psetindex[j]>1)
	    {
	      /** Send the final vector to non-leader processor **/
	      for(k=DBL->block_psetindex[j];k<DBL->block_psetindex[j+1];k++)
		if(DBL->block_pset[k] != proc_id)
		  MPI_Send(xptr, CC(bind[j+1]-bind[j]), MPI_COEF_TYPE, DBL->block_pset[k], DBL->loc2glob_blocknum[j], MPI_COMM_WORLD); 
	      /*memcpy(commvec->t + bind[j]-offset, xptr, sizeof(COEF) * (commvec->veclen[j-L->tlj]));
		fprintfd(stderr, "Proc %d send final x %d \n", DL->proc_id, DBL->loc2glob_blocknum[j]);
	      MPI_Startall(commvec->out_ctrb[j-L->tlj], commvec->send_rqtab[j-L->tlj]);
	      MPI_Waitall(commvec->out_ctrb[j-L->tlj], commvec->send_rqtab[j-L->tlj], commvec->status);*/
	    }
	}
      else
	{
#ifdef DEBUG_M
	  assert(commvec->out_ctrb[j-L->tlj] == -1);
#endif
	  /** Receive the vector from the row_leader **/
	  if(MPI_Wait(commvec->recv_rqtab[j-L->tlj], commvec->status) != MPI_SUCCESS)
	    {
	      fprintfd(stderr, "Error in PhidalDistrMatrix_LSolve for vector %d \n" ,j);
	      MPI_Abort(MPI_COMM_WORLD, - 1);
	    }

	  memcpy(xptr, commvec->t+(bind[j]-offset), sizeof(COEF) * (commvec->veclen[j-L->tlj]));
	}
	
      /*** Multiply x[j]  by the off-diagonal blocks **/
      for(k=L->cia[j]+1;k<L->cia[j+1];k++)
#ifndef POUR_DEBUG
	if(DL->clead[k] == proc_id/*  && L->ca[k]->nnzr > 0 */) 
#else
	if(DL->clead[k] == proc_id) 
#endif
	  {
	    i = L->cja[k];
	    xiptr = x+(bind[i]-offset);
	    
	    /*fprintfd(stderr, "Proc %d: norm %d before ctrb = %g \n", DBL->proc_id,DBL->loc2glob_blocknum[i],  
	      norm2(xiptr, bind[i+1]-bind[i]));*/

	    VS_LMATVEC(L->ca[k], xptr, xiptr);
	    /* 	      matvecz(L->ca[k], xptr, xiptr, xiptr);  */

/* 	    if(L->csc == 0) */
/* 	      matvecz(L->ca[k], xptr, xiptr, xiptr);  */
/* 	    else */
/* 	      CSC_matvecz(L->ca[k], xptr, xiptr, xiptr);  */

	    /*fprintfd(stderr, "Proc %d: norm %d after ctrb = %g \n", DBL->proc_id, DBL->loc2glob_blocknum[i], 
	      norm2(xiptr, bind[i+1]-bind[i]));*/

	    if(DBL->row_leader[i] != proc_id && --(commvec->loc_cnt[i-L->tli]) == 0 )
	      {
		/** Send the contribution to row_leader processor
		 **/
		/*fprintfd(stderr, "Proc %d send ctrb %d Norm = %g \n", DL->proc_id, DBL->loc2glob_blocknum[i], 
		  norm2(xiptr, bind[i+1]-bind[i]));*/
		
		MPI_Send(xiptr, CC(bind[i+1]-bind[i]), MPI_COEF_TYPE, DBL->row_leader[i], 
			 DBL->loc2glob_blocknum[i], MPI_COMM_WORLD); 
	      }
	  }
    }
  
  /*for(i=L->tli;i<=L->bri;i++)
    if(DBL->row_leader[i] == proc_id)
      fprintfd(stderr, "Proc %d norm2b[%d] = %g \n", DL->proc_id, DBL->loc2glob_blocknum[i],
      norm2(x+(BL->block_index[i]-offset), BL->block_index[i+1]-BL->block_index[i]));*/
}

/* TODO : boucle comme pascal inversé */

void TR(flag_t unitdiag, SolverMatrix* solvUs, SymbolMatrix* symbUs, COEF *x, COEF *b, COEF* xtmp);
void DBMATVEC2(SolverMatrix* solvUs, SymbolMatrix* symbUs, COEF *x, COEF *b, COEF* xtmp);

/* TODO : inverser nom variable i et j */
void DBDistrMatrix_Usolv2(flag_t unitdiag, DBDistrMatrix *DU, COEF *x, COEF *b, PhidalDistrHID *DBL)
{
  
  DBMatrix* U = &DU->M;
  int i, i0, j;
  int UN = 1;
  COEF* xtmp;

  if ((U->alloc == ONE) || (U->alloc == CBLK))
     printfd("***  Warning : DBMatrix_solve2 instead of de DBMatrix_solve\n");

  if (x!=b)
    BLAS_COPY(U->nodenbr, b, UN, x, UN);

  /* vecteur tmp */
  xtmp = (COEF*)malloc(sizeof(COEF)*U->nodenbr);
  
  /* assert((U->alloc == ONE) || (U->alloc == RBLK)); TODO Symm/Unsym */
  /* assert((U->alloc == ONE) || (U->alloc == CBLK) || (U->alloc == RBLK) || (U->alloc == BLK)); */

  if (unitdiag == 0) /*unsym*/
    {
      assert(0);
      DBMatrix_Transpose(U);
    }

  

  /* parcours par blocs colonnes */
  for(i0=U->bri-U->tli,i=U->bri; i0>=0; i0--, i--) {

    assert(U->rja[U->ria[i+1]-1] == i); /* diag */
    
    j=U->ria[i+1]-1;
    TR(unitdiag, U->ra[j]->solvmtx, &U->ra[j]->symbmtx, x, b, xtmp);
    
    for(j=U->ria[i+1]-2; j>=U->ria[i]; j--) {
      DBMATVEC2(U->ra[j]->solvmtx, &U->ra[j]->symbmtx, x, b, xtmp);
    }
    
  }

  if (unitdiag == 0) /*unsym*/
    {
      DBMatrix_Transpose(U);
    }

  free(xtmp);

  
}



void DBDistrMatrix_Usolv1(flag_t unitdiag, DBDistrMatrix *DU, COEF *x, COEF *b, PhidalDistrHID *DBL)
{
  
  DBMatrix* U = &DU->M;
  int i, i0, j;
  int UN = 1;
  COEF* xtmp;

  if ((U->alloc == ONE) || (U->alloc == CBLK))
     printfd("***  Warning : DBMatrix_solve2 instead of de DBMatrix_solve\n");

  if (x!=b)
    BLAS_COPY(U->nodenbr, b, UN, x, UN);

  /* vecteur tmp */
  xtmp = (COEF*)malloc(sizeof(COEF)*U->nodenbr);
  
  /* assert((U->alloc == ONE) || (U->alloc == RBLK)); TODO Symm/Unsym */
  /* assert((U->alloc == ONE) || (U->alloc == CBLK) || (U->alloc == RBLK) || (U->alloc == BLK)); */

  DBMatrix_Transpose(U);
  
  /* parcours par blocs colonnes */
  for(i0=U->brj-U->tlj,i=U->brj; i0>=0; i0--, i--) {

    assert(U->cja[U->cia[i+1]-1] == i); /* diag */
    
    j=U->cia[i+1]-1;
    TR(unitdiag, U->ca[j]->solvmtx, &U->ca[j]->symbmtx, x, b, xtmp);
    
    for(j=U->cia[i+1]-2; j>=U->cia[i]; j--) {
      DBMATVEC2(U->ca[j]->solvmtx, &U->ca[j]->symbmtx, x, b, xtmp);
    }
    
  }

  DBMatrix_Transpose(U);
  
  free(xtmp);
  
}





void DBDistrMatrix_Usolv(flag_t unitdiag, DBDistrMatrix *DU, COEF *x, COEF *b, PhidalDistrHID *DBL)
{
  /*****************************************************************************************/
  /* This function computes x = U^-1.b                                                     */
  /* If unitdiag == 1 then the diagonal is considered to be unitary (stored or not stored) */
  /*   THIS CONCERNS THE DIAGONAL NOT THE BLOCK DIAGONAL                                   */
  /* Work also in place (i.e. x == b)                                                      */
  /*****************************************************************************************/
  dim_t i, j, k;
  int *bind;
  COEF *xptr, *xiptr;
  int offset;
  DBMatrix *U;
  PhidalHID *BL;
  DBMatrixCommVec *commvec;
  mpi_t proc_id;

  BL = &DBL->LHID;
  proc_id = DU->proc_id;
  U = &(DU->M);
  commvec = &(DU->commvec);
  bind = BL->block_index;
  offset = bind[U->tli];


#ifdef DEBUG_M
  assert(U->dim1 == U->dim2);
  assert(U->tli == U->tlj);
  assert(U->bri == U->brj);
  assert(commvec->init == 1);
#endif
  if(x != b)
    memcpy(x, b, sizeof(COEF)*U->dim1);

/*   if (x!=b) */
/*     BLAS_COPY(U->nodenbr, b, UN, x, UN); */

  DBDistrMatrix_Transpose(DU);


  /* vecteur tmp */
  COEF* xtmp = (COEF*)malloc(sizeof(COEF)*U->nodenbr);

  /*bzero(commvec->t, sizeof(COEF)*U->dim1);*/ /*NO NEED */
  /** IMPORTANT: we store the contribution for x[i] part in x 
      so it must be initialized to zero where the processor is not
      leader **/
  for(i=U->tli;i<=U->bri;i++)
    if(DBL->row_leader[i] != proc_id)
      bzero(x + bind[i]-offset, sizeof(COEF)*(bind[i+1]-bind[i]));

  memcpy(commvec->loc_cnt, commvec->loc_ctrb, sizeof(int)*(U->bri-U->tli+1));

  /** Start the persistent receive requests **/
  for(i=U->bri;i>=U->tli;i--)
    {
      if(commvec->out_ctrb[i-U->tli] > 0)
	{
	  /*fprintfd(stderr, "Proc %d poste ctrb receive %d \n", DU->proc_id, i);*/
	  if(MPI_Startall(commvec->out_ctrb[i-U->tli], commvec->recv_rqtab[i-U->tli]) != MPI_SUCCESS)
	    {
	      fprintfd(stderr, "Error in start recv contrib for vec %d \n", i);
	      assert(0);
	    }
	}
      if(commvec->out_ctrb[i-U->tli] == -1)
	if(MPI_Start(commvec->recv_rqtab[i-U->tli]) != MPI_SUCCESS)
	  {
	    fprintfd(stderr, "Error in start recv vector for vec %d \n", i);
	    assert(0);
	  }
    }

 /*  for(j=U->brj;j>=U->tlj;j--) */
/*     { */
/*       printfd("%d : j=%d\n",proc_id, j); */
/*       for(k=U->cia[j+1]-2;k>=U->cia[j];k--) { */
/* 	printfd("%d : j=%d k=%d\n", proc_id, j, k); */
/*       } */
/*     } */

/*   exit(1); */

/*   printfd("La\n"); */
  /* if (proc_id == 1) return; */

  /** We use a column algorithm 
      that allows to overlap communication and computation ***/
  for(j=U->brj;j>=U->tlj;j--)
    {
     /*  printfd("%d : j=%d\n",proc_id, j); */

      xptr = x+(bind[j]-offset);
#ifdef DEBUG_M
      assert(U->cja[U->cia[j+1]-1] == j);
#endif
      if(DBL->row_leader[j] == proc_id)
	{
	  if(commvec->out_ctrb[j-U->tlj] > 0)
	    {
/* 	      fprintfd(stderr, "Proc %d receive ctrb .... \n", proc_id); */

/* 	      printfd("proc=%d j=%d : Leader, contrib ...\n", proc_id, j); */
	      /** Receive the contribution from non leader processors **/
	      /* printfd("proc=%d j=%d : Wait Contrib\n", proc_id, j); */
	      DBMatrixCommVec_ReceiveVecAdd(j-U->tlj, xptr, commvec); 
/* 	      fprintfd(stderr, "Proc %d receive ctrb %d = %g \n", DU->proc_id, DBL->loc2glob_blocknum[j],  */
/* 		      norm2(commvec->recv_buffer[j-U->tlj], commvec->veclen[j-U->tlj])); */

	      /* printfd("proc=%d j=%d : Leader, contrib reçu\n", proc_id, j); */

	    } /* else { printfd("proc=%d j=%d : Leader, Pas de contrib\n", proc_id, j); } */

	  /** Solve **/
	  assert(U->cja[U->cia[j+1]-1] == j);

	  /* printfd("%d : La2 j=%d\n",proc_id, j); */

/* 	  if(U->csc == 0) */
/* 	    CSR_Usol(unitdiag, U->ca[U->cia[j+1]-1], xptr, xptr); */
/* 	  else */
/* 	    CSC_Usol(unitdiag, U->ca[U->cia[j+1]-1], xptr, xptr); */

/* #warning x ou xptr */
	  /* printfd("%d : TR1\n", proc_id); */
	  /* printfd("proc=%d j=%d : Leader, TR\n", proc_id, j); */
	  TR1(unitdiag, U->ca[U->cia[j+1]-1], xptr, xptr, xtmp); /* TODO : uniformiser x ou xptr !!!! */
	  /* printfd("%d : TR1 --\n", proc_id); */

	  if(DBL->row_leader[j] == proc_id && DBL->block_psetindex[j+1]-DBL->block_psetindex[j]>1)
	    {
	      /* 	      printfd("SEND ... \n"); */
	      /** Send the final vector to non-leader processor **/
	      /* printfd("proc=%d j=%d : Leader, Send the final vector to non-leader processorS ...\n", proc_id, j); */
	      for(k=DBL->block_psetindex[j];k<DBL->block_psetindex[j+1];k++)
		if(DBL->block_pset[k] != proc_id)
		  MPI_Send(xptr, CC(bind[j+1]-bind[j]), MPI_COEF_TYPE, DBL->block_pset[k], DBL->loc2glob_blocknum[j], MPI_COMM_WORLD); 
	      /*memcpy(commvec->t + bind[j]-offset, xptr, sizeof(COEF) * (commvec->veclen[j-U->tlj]));
		fprintfd(stderr, "Proc %d send final x %d \n", DU->proc_id, DBL->loc2glob_blocknum[j]);
	      MPI_Startall(commvec->out_ctrb[j-U->tlj], commvec->send_rqtab[j-U->tlj]);
	      MPI_Waitall(commvec->out_ctrb[j-U->tlj], commvec->send_rqtab[j-U->tlj], commvec->status);*/
	    }
	}
      else
	{

	  /* printfd("proc=%d j=%d : Pas Leader\n", proc_id, j); */

	  /* printfd("LALALAL\n"); */
#ifdef DEBUG_M
	  assert(commvec->out_ctrb[j-U->tlj] == -1);
#endif

	  /* printfd("proc=%d j=%d : Pas Leader, recv from row_leader\n", proc_id, j); */
	  /** Receive the vector from the row_leader **/
	  if(MPI_Wait(commvec->recv_rqtab[j-U->tlj], commvec->status) != MPI_SUCCESS)
	    {
	      fprintfd(stderr, "Error in DBDistrMatrix_LSolve for vector %d \n" ,j);
	      MPI_Abort(MPI_COMM_WORLD, - 1);
	    }

	  memcpy(xptr, commvec->t+(bind[j]-offset), sizeof(COEF) * (commvec->veclen[j-U->tlj]));
	}
	
          
      /*** Multiply x[j]  by the off-diagonal blocks **/
      for(k=U->cia[j+1]-2;k>=U->cia[j];k--)
	/*for(k=U->cia[j];k<U->cia[j+1]-1;k++)*/
#ifndef POUR_DEBUG
	if(DU->clead[k] == proc_id /* && U->ca[k]->nnzr > 0 */)
#else
	if(DU->clead[k] == proc_id)
#endif
	  {

	    /* printfd("proc=%d j=%d k=%d : off diag, DBMATVEC\n", proc_id, j, k); */

	    i = U->cja[k];
	    xiptr = x+(bind[i]-offset);
	    
	    /*fprintfd(stderr, "Proc %d: norm %d before ctrb = %g \n", DBL->proc_id,DBL->loc2glob_blocknum[i],  
	      norm2(xiptr, bind[i+1]-bind[i]));*/
/* 	    if(U->csc == 0) */
/* 	      matvecz(U->ca[k], xptr, xiptr, xiptr);  */
/* 	    else */
/* 	      CSC_matvecz(U->ca[k], xptr, xiptr, xiptr);  */

/* 	    VS_LMATVEC(U->ca[k], xptr, xiptr); */
/* 	    DBMATVEC21(U->ca[k], x, x, xtmp); */
	    DBMATVEC21(U->ca[k], xptr, xptr, xtmp);

	    /*fprintfd(stderr, "Proc %d: norm %d after ctrb = %g \n", DBL->proc_id, DBL->loc2glob_blocknum[i], 
	      norm2(xiptr, bind[i+1]-bind[i]));*/

	    if(DBL->row_leader[i] != proc_id && --(commvec->loc_cnt[i-U->tli]) == 0 )
	      {
		/** Send the contribution to row_leader processor
		 **/
		/*fprintfd(stderr, "Proc %d send ctrb %d Norm = %g \n", DU->proc_id, DBL->loc2glob_blocknum[i], 
		  norm2(xiptr, bind[i+1]-bind[i]));*/
		
		/* printfd("proc=%d j=%d k=%d: off diag, send\n", proc_id, j, k); */

		MPI_Send(xiptr, CC(bind[i+1]-bind[i]), MPI_COEF_TYPE, DBL->row_leader[i], 
			 DBL->loc2glob_blocknum[i], MPI_COMM_WORLD); 
	      }
	  }
    }

  /*for(i=U->tli;i<=U->bri;i++)
    if(DBL->row_leader[i] == proc_id)
      fprintfd(stderr, "Proc %d norm2b[%d] = %g \n", DU->proc_id, DBL->loc2glob_blocknum[i],
      norm2(x+(BL->block_index[i]-offset), BL->block_index[i+1]-BL->block_index[i]));*/
  free(xtmp);

  DBDistrMatrix_Transpose(DU);

/*   printfd("%d : FIN\n", proc_id); */
}




void DBDistrMatrix_Dsolv(DBDistrMatrix *DD, COEF* b) {
#ifdef DEBUG_NOALLOCATION
  return;
#endif

  DBMatrix *D;
  D = &(DD->M);

  SolverMatrix* solvSD=NULL;
  SymbolMatrix* SD;
  int k, i, i0, ii, iiglobal;
  blas_t stride;
  dim_t p;
  COEF* ptr;
  
  /*   assert((D->alloc == ONE) || (D->alloc == CBLK) || D->allo); */
  
  if (D->alloc == ONE) {
    solvSD = &D->a[0];
  }

  /* parcours par blocs colonnes */
  iiglobal=0;
  for(i0=0,i=D->tlj; i<=D->brj; i0++, i++) {
    SD = &D->ca[D->cia[i]]->symbmtx;

    if (D->alloc == CBLK) {
      solvSD = &D->a[i0];
    }

    if(D->alloc == BLK) {
      solvSD = D->ca[D->cia[i]]->solvmtx;
    }

    if (D->alloc == RBLK) {
      solvSD = &D->a[D->cja[D->cia[i]]-D->tli];
    }

    for(k=0; k<SD->cblknbr; k++) {
      p      = SD->bcblktab[k].fbloknum;        /* triangular block */
      ptr    = solvSD->coeftab + solvSD->bloktab[p].coefind;
      stride = SD->stride[k];

      for (ii=SD->ccblktab[k].fcolnum; ii<=SD->ccblktab[k].lcolnum; ii++, iiglobal++) { /* TODO : nom de variables */
	b[iiglobal] /= ptr[0];
	ptr += stride + 1;
      }
    }
  }

}

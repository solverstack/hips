/* @authors J. GAIDAMOUR */

#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#include <assert.h>
#include "block.h"

#include "base.h"

void VS_RowDiv(VSolverMatrix* D, VSolverMatrix* M) {
  SolverMatrix* solvD = D->solvmtx;
  SolverMatrix* solvM = M->solvmtx;

  SymbolMatrix* symbD = &D->symbmtx;
  SymbolMatrix* symbM = &M->symbmtx;

  dim_t k, m;
  COEF alphadiag;
  blas_t strideD, strideM;
  int cdim, hdim;
  COEF *dc; 
  COEF *mc; 

  int UN = 1;

  /* ./testDBDistrMatrix.ex 7 bcsstk01.rsa */
  if (symbM->bloknbr == 0) return;

  for(k=0;k<symbM->cblknbr;k++)
    {
      cdim    = symbM->ccblktab[k].lcolnum - symbM->ccblktab[k].fcolnum + 1;
      strideD = symbD->stride[k];
      strideM = symbM->stride[k];
      hdim    = symbM->hdim[k];

      dc = solvD->coeftab + solvD->bloktab[ symbD->bcblktab[k].fbloknum ].coefind;
      mc = solvM->coeftab + solvM->bloktab[ symbM->bcblktab[k].fbloknum ].coefind;

      for(m=0;m<cdim;m++) {
#ifndef DEBUG_NOALLOCATION      
	alphadiag = 1.0 / dc[m*strideD + m];
#endif
	BLAS_SCAL(hdim, alphadiag, mc, UN);
	mc += strideM;
      }
      
    }
  
}


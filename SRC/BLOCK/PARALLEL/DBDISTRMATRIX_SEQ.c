/* @authors J. GAIDAMOUR */

#include <stdio.h>
#include <stdlib.h> /*exit()*/

#define PARALLEL

#include "block.h"
#include "db_parallel.h"

void DBDISTRMATRIX_Precond(PhidalDistrMatrix *A, DBDistrPrec *P, SymbolMatrix* symbmtx, PhidalDistrHID *DBL, PhidalOptions *option)
{
#ifdef DEBUG_M
  assert(A->M.symmetric == option->symmetric);
#endif
  
  PhidalHID* BL = &DBL->LHID;

  /** Correct the options **/
  PhidalOptions_Fix(option, BL);

  /** Print the options **/
  if(option->verbose > 0 && DBL->proc_id == 0)
    PhidalOptions_Print(stdout, option);

  if(A->M.symmetric == 0)
    DBDISTRMATRIX_MLILUPrec(A, P, symbmtx, DBL, option);
  else
    DBDISTRMATRIX_MLICCPrec(A, P, symbmtx, DBL, option);

/*   MPI_Finalize(); */
/*   exit(1); */
}

void DBDISTRMATRIX_Solve(PhidalMatrix *A, DBDistrPrec *P, PhidalHID *BL, PhidalOptions *option, COEF *rhs, COEF *x) {
  chrono_t t1, t2;
  int iter=-1;
  t1  = dwalltime();

  /* iter = DBDISTRMATRIX_Fgmresd(option->verbose, option->tol, option->itmax, A, P, BL, option, rhs, x, option->fd); */
  /* if(option->verbose > 0) */
  fprintfv(1, stdout, "Number of outer iterations %d \n", iter);
  
  t2  = dwalltime();
  
  fprintfv(1, stdout, "\n SOLVED IN %g \n", t2-t1);
}


/* @authors J. GAIDAMOUR */

#include <stdio.h>
#include <stdlib.h>

#include <string.h> /* memcpy */

#include "block.h"
#include "db_parallel.h"

void TR(flag_t unitdiag, SolverMatrix* solvUs, SymbolMatrix* symbUs, COEF *x, COEF *b, COEF* xtmp);

void TR1(flag_t unitdiag, VSolverMatrix* vsolvmtx, COEF* b, COEF* x, COEF* xtmp) {
  TR(unitdiag, vsolvmtx->solvmtx, &vsolvmtx->symbmtx, b, x, xtmp);
}

void TR(flag_t unitdiag, SolverMatrix* solvUs, SymbolMatrix* symbUs, COEF *x, COEF *b, COEF* xtmp) {
  dim_t k, p;
  int jxtmp;

  dim_t tli = symbUs->tli; /* cf LSolv */

  COEF* xptr;
  int height, width, stride;

  int size;

  COEF minusone= -1.0, one=1.0;
  char *uploL = "L";
  char *transT = "T";
  char* diag = unitdiag?"U":"N";
  int UN = 1;
  assert((unitdiag == 0) || (unitdiag == 1));
  
  for(k=symbUs->cblknbr-1; k>=0; k--) {
    width  = symbUs->ccblktab[k].lcolnum - symbUs->ccblktab[k].fcolnum +1;
    xptr = x + symbUs->ccblktab[k].fcolnum - tli;
      	
    /* xtmp := x (in) */
     
    jxtmp=0;
    
    p = symbUs->bcblktab[k].fbloknum+1;
    if (p> symbUs->bcblktab[k].lbloknum) goto next;
	
    for(; p<=symbUs->bcblktab[k].lbloknum; p++) {
      size = symbUs->bloktab[p].lrownum - symbUs->bloktab[p].frownum +1;

      BLAS_COPY(size, x + symbUs->bloktab[p].frownum - tli, UN, xtmp + jxtmp, UN);
      jxtmp += size;
    }

   
    p = symbUs->bcblktab[k].fbloknum+1;
    height = symbUs->hdim[k] - width; /** Hauteur de la surface compactÃ©e des blocs extra-diagonaux **/    
    stride = symbUs->stride[k];
    if (height == 0) goto next;   /* inutile ? */
        	
    /* calcul des contributions (aggrégation des colonnes) */
    BLAS_GEMV(transT, height, width, minusone /*coef alpha*/,
	      solvUs->coeftab + solvUs->bloktab[p].coefind, stride,
	      xtmp, UN,       one /*coef beta*/,
	      xptr, UN);
	
  next:
    stride = symbUs->stride[k];
    p = symbUs->bcblktab[k].fbloknum; /* triangular block */
    BLAS_TRSV(uploL, transT, diag, width /*=h*/,
	      solvUs->coeftab + solvUs->bloktab[p].coefind, stride, xptr, UN);
  }
}

/* void TR(flag_t unitdiag, SolverMatrix* solvUs, SymbolMatrix* symbUs, COEF *x, COEF *b, COEF* xtmp) { */
/*   dim_t k, p; */
/*   int jxtmp; */

/*   dim_t tli; */
/*   printfv(5, "TODO : tli !\n"); */
/* /\*   if (U->alloc != BLK) *\/ */
/* /\*     tli = U->a[0].symbmtx.tli; *\/ */
/* /\*   else /\\* (U->alloc == BLK) *\\/ *\/ */
/* /\*     tli = U->ca[0]->symbmtx.tli; *\/ */

/*   COEF* xptr; */
/*   int height, width, stride; */

/*   int size; */

/*   COEF minusone= -1.0, one=1.0; */
/*   char *uploL = "L"; */
/*   char *transT = "T"; */
/*   char* diag = unitdiag?"U":"N"; */
/*   int UN = 1; */
/*   assert((unitdiag == 0) || (unitdiag == 1)); */
 	
/*   for(k=symbUs->cblknbr-1; k>=0; k--) { */
/*     width  = symbUs->ccblktab[k].lcolnum - symbUs->ccblktab[k].fcolnum +1; */
/*     xptr = x + symbUs->ccblktab[k].fcolnum - tli; */
      	
/*     /\* xtmp := x (in) *\/ */
     
/*     jxtmp=0; */
    
/*     p = symbUs->bcblktab[k].fbloknum+1; */
/*     if (p> symbUs->bcblktab[k].lbloknum) goto next; */
	
/*     for(; p<=symbUs->bcblktab[k].lbloknum; p++) { */
/*       size = symbUs->bloktab[p].lrownum - symbUs->bloktab[p].frownum +1; */
/*       BLAS_COPY(size, x + symbUs->bloktab[p].frownum - tli, UN, xtmp + jxtmp, UN); */
/*       jxtmp += size; */
/*     } */

   
/*     p = symbUs->bcblktab[k].fbloknum+1; */
/*     height = symbUs->hdim[k] - width; /\** Hauteur de la surface compactÃ©e des blocs extra-diagonaux **\/     */
/*     stride = symbUs->stride[k]; */
/*     if (height == 0) goto next;   /\* inutile ? *\/ */
        	
/*     /\* calcul des contributions (aggrégation des colonnes) *\/ */
/*     BLAS_GEMV(transT, height, width, minusone /\*coef alpha*\/, */
/* 	      solvUs->coeftab + solvUs->bloktab[p].coefind, stride, */
/* 	      xtmp, UN,       one /\*coef beta*\/, */
/* 	      xptr, UN); */
	
/*   next: */
/*     stride = symbUs->stride[k]; */
/*     p = symbUs->bcblktab[k].fbloknum; /\* triangular block *\/ */
/*     BLAS_TRSV(uploL, transT, diag, width /\*=h*\/, */
/* 	      solvUs->coeftab + solvUs->bloktab[p].coefind, stride, xptr, UN); */
/*   } */
/* } */

void DBMATVEC2(SolverMatrix* solvUs, SymbolMatrix* symbUs, COEF *x, COEF *b, COEF* xtmp);

void DBMATVEC21(VSolverMatrix* vsolvmtx, COEF* b, COEF* x, COEF* xtmp) {
  DBMATVEC2(vsolvmtx->solvmtx, &vsolvmtx->symbmtx, b, x, xtmp);
}



void DBMATVEC2(SolverMatrix* solvUs, SymbolMatrix* symbUs, COEF *x, COEF *b, COEF* xtmp) {
  dim_t k, p;
  int jxtmp;
  
  dim_t tli = symbUs->tli; /* cf LSolv */

  COEF* xptr;
  int height, width, stride;

  int size;

  COEF minusone= -1.0, one=1.0;
  char *transT = "T";
  int UN = 1;

  for(k=symbUs->cblknbr-1; k>=0; k--) {
    width  = symbUs->ccblktab[k].lcolnum - symbUs->ccblktab[k].fcolnum +1;
    xptr = x + symbUs->ccblktab[k].fcolnum - tli;
      	
    /* xtmp := x (in) */
     
    jxtmp=0;
    
    p = symbUs->bcblktab[k].fbloknum;
    if (p> symbUs->bcblktab[k].lbloknum) continue;
	
    for(; p<=symbUs->bcblktab[k].lbloknum; p++) {
      size = symbUs->bloktab[p].lrownum - symbUs->bloktab[p].frownum +1;

      BLAS_COPY(size, x + symbUs->bloktab[p].frownum - tli, UN, xtmp + jxtmp, UN);
      jxtmp += size;
    }

   
    p = symbUs->bcblktab[k].fbloknum;
    height = symbUs->hdim[k]; /** Hauteur de la surface compactÃ©e des bloc extra-diagonaux **/
    stride = symbUs->stride[k];
    if (height == 0) continue;
   	
    /* calcul des contributions (aggrégation des colonnes) */
    BLAS_GEMV(transT, height, width, minusone /*coef alpha*/,
	      solvUs->coeftab + solvUs->bloktab[p].coefind, stride,
	      xtmp, UN,       one /*coef beta*/,
	      xptr, UN);
  }
}


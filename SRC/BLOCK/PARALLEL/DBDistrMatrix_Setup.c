/* @authors J. GAIDAMOUR */

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h> /* strcmp */

#include "block.h"

#include "db_parallel.h"
/* TODO : coefnbr = -1*/


/* /\* subfunctions called by DBDistrMatrix_Setup_symbol_pattern() *\/ */
/* void DBDistrMatrix_Setup_cblktosolvmtx(DBDistrMatrix* m); */



/**/

void DBDistrMatrix_Setup(dim_t tli, dim_t tlj, dim_t bri, dim_t brj, char *UPLO, char *DIAG, 
			 int_t locally_nbr, DBDistrMatrix* DM, 
			 SymbolMatrix* symbmtx, PhidalDistrHID *BL, 
			 int_t levelnum, int_t levelnum_l, alloc_t alloc) {
  DBMatrix* m = &DM->M;

  DBDistrMatrix_Setup_HID(tli, tlj, bri, brj, UPLO, DIAG, locally_nbr, DM, BL);

  DM->extDiag = NULL; /* TODO faire un Init() qq part */

  m->alloc = alloc;

  m->cblknbr = symbmtx->cblknbr;
  m->nodenbr = symbmtx->nodenbr;

  if ((m->symmetric == 0) && 
      ((strcmp(UPLO, "U") == 0)/* U */ || (tli<tlj) /* F */)) 
    {
      DBDistrMatrix_Transpose(DM);
      /*fix*/
      m->alloc = alloc;
    }

  DBDistrMatrix_Setup_SYMBOL(DM, symbmtx, BL, levelnum, levelnum_l);
/*   m->coefmax = DBDistrMatrix_calcCoefmax(m, m); */
  m->coefmax = DBMatrix_calcCoefmax(m, m);

  /* a deplacer */
  {
    int i0,i,j;

    int tl, br;
    INTL *ia; dim_t *ja;
    tl = m->tlj;
    br = m->brj;
    ia = m->cia;
    ja = m->cja;
    
    for(i0=0,i=tl;i<=br;i0++,i++) {
      for(j=ia[i];j<ia[i+1];j++) {
	m->ca[j]->solvmtx->coefmax = m->coefmax;
      }
    }
  }
  /* */

  if ((m->symmetric == 0) && 
      ((strcmp(UPLO, "U") == 0)/* U */ || (tli<tlj) /* F */)) 
    {
      DBDistrMatrix_Transpose(DM);
    }
  
  /* m->ccblktab alloué ou non : pas d'importance pour ONE (sauf desalloc ) */
  if (m->alloc == RBLK) {
    m->ccblktab = symbmtx->ccblktab; 
    /*     symbmtx->ccblktab = NULL; TODO! */
  } else {
    m->ccblktab = NULL;
  }

  /* tmp */
  if (m->alloc == CBLK) {
    m->ccblktab = symbmtx->ccblktab; 
  }
}

void DBDistrMatrix_Setup_SYMBOL(DBDistrMatrix* DM, SymbolMatrix* symbmtx, PhidalDistrHID *DBL, 
				int_t levelnum, int_t levelnum_l) {

  DBMatrix_Setup_SYMBOL(&DM->M, symbmtx, &DBL->LHID, levelnum, levelnum_l);

}

/* void DBDistrMatrix_Setup_cblktosolvmtx(DBDistrMatrix* m) { */
/* /\*   int k0, k, i0, i; *\/ */

/*   m->M.cblktosolvmtx = NULL; */
/* /\*   if (m->alloc == CBLK) { *\/ */
/* /\*     m->cblktosolvmtx = (SolverMatrix**)malloc(sizeof(SolverMatrix*)*m->cblknbr); *\/ */

/* /\*     k=0; *\/ */
/* /\*     for(i0=0,i=m->tlj; i<=m->brj; i0++, i++)  *\/ */
/* /\*       for(k0=0; k0<m->a[i0].symbmtx.cblknbr; k0++, k++)  *\/ */
/* /\* 	m->cblktosolvmtx[k] = &m->a[i0]; *\/ */

/* /\*   } *\/ */

/* } */

/***************************************************************************/

/* void DBDistrMatrix_SetupV(DBDistrMatrix *DM, DBDistrMatrix *DCpy) { */
/*   int i0, ilast; */

/*   DBMatrix *M = &DM->M; */
/*   DBMatrix *Cpy = &DCpy->M; */

/*   assert(M->alloc != BLK); */

/*   memcpy(DCpy, DM, sizeof(DBDistrMatrix)); */
/*   Cpy->virtual = COPY; */

/*   if (M->alloc == ONE) */
/*     ilast = 1; */
/*   else if (M->alloc == BLK) */
/*     ilast = M->cblknbr; */
/*   else if (M->alloc == CBLK) */
/*     ilast = M->brj - M->tlj + 1; */
/*   else  */
/*     ilast = M->bri - M->tli + 1; */

/*   Cpy->a = (SolverMatrix*)malloc(sizeof(SolverMatrix)*ilast); */
/*   memcpy(Cpy->a, M->a, sizeof(SolverMatrix)*ilast); */

/*   for(i0=0; i0<ilast; i0++) { */
/*     Cpy->a[i0].virtual = COPY; */
/*   } */

/*   DBDistrMatrix_Setup_cblktosolvmtx(DCpy); */

/* } */

void DBDistrMatrix_SetupV(DBDistrMatrix *DM, DBDistrMatrix *DCpy) {
  DBMatrix* M = &DM->M;
  DBMatrix* Cpy = &DCpy->M;

  assert(M->alloc != BLK);

  int i0, ilast;

  memcpy(Cpy, M, sizeof(DBMatrix));
  Cpy->virtual = COPY;

  if (M->alloc == ONE)
    ilast = 1;
  else if (M->alloc == BLK)
    ilast = M->cblknbr;
  else if (M->alloc == CBLK)
    ilast = M->brj - M->tlj + 1;
  else 
    ilast = M->bri - M->tli + 1;

  Cpy->a = (SolverMatrix*)malloc(sizeof(SolverMatrix)*ilast);
  memcpy(Cpy->a, M->a, sizeof(SolverMatrix)*ilast);

  for(i0=0; i0<ilast; i0++) {
    Cpy->a[i0].virtual = COPY;
  }

  DBMatrix_Setup_cblktosolvmtx(Cpy);

  /* todo : a revoir voir version 169 */
  Cpy->bloktab = (VSolverMatrix*)malloc(sizeof(VSolverMatrix) * M->bloknbr);
  memcpy(Cpy->bloktab, M->bloktab, sizeof(VSolverMatrix) * M->bloknbr);
/*   for(i=0; i<) */


  /**/
  {
    dim_t i;
    Cpy->ra = (VSolverMatrix **)malloc(sizeof(VSolverMatrix*)* (Cpy->bloknbr));
    for(i=0;i<Cpy->bloknbr;i++)
      Cpy->ra[i] = Cpy->bloktab + (M->ra[i] - M->bloktab);
    
    Cpy->ca = (VSolverMatrix **)malloc(sizeof(VSolverMatrix*)* (Cpy->bloknbr));
    for(i=0;i<Cpy->bloknbr;i++)
      Cpy->ca[i] = Cpy->bloktab + (M->ca[i] - M->bloktab);
    /**/
  }

  {
    dim_t i,j;
    int tl, br;
    INTL *ia; dim_t *ja;
    VSolverMatrix **a;
    SolverMatrix *solv;

    if ((Cpy->alloc == ONE) || (Cpy->alloc == CBLK) || (Cpy->alloc == BLK)) {
      tl = Cpy->tlj;
      br = Cpy->brj;
      ia = Cpy->cia;
      ja = Cpy->cja;
      a  = Cpy->ca;     /* TODO : change variable name 'a' bcse Cpy->a exist */
    } else {
      assert(Cpy->alloc == RBLK);
    
      tl = Cpy->tli;
      br = Cpy->bri;
      ia = Cpy->ria;
      ja = Cpy->rja;
      a  = Cpy->ra;
    }

    solv = &Cpy->a[0];

    for(i0=0,i=tl; i<=br; i0++, i++) {
      if (Cpy->alloc != ONE) {
	solv = &Cpy->a[i0];
      }
      for(j=ia[i];j<ia[i+1];j++) {
	a[j]->solvmtx = solv;
      }
    }
 
  }
  
}

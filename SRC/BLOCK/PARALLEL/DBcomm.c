/* @authors J. GAIDAMOUR */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>

#include "db_parallel.h"

#define COMM_MEMRATIO 1.0

/* #define TRACE_COM */
/* #define PRINT(arg...) printf(arg) */
#define PRINT(arg...) 

void DBMatrix_MatricesAdd(VSolverMatrix* a, int matnbr, COEF **coeff_buff);
void DBMatrix_MatricesCopy(VSolverMatrix* a, COEF *coeff_buff);

long VSolverMatrix_nnz(VSolverMatrix* mat) {
  dim_t k;
  long nnz=0;

  SymbolMatrix* symbmtx = &mat->symbmtx;

  for (k=0; k<symbmtx->cblknbr; k++) {
    assert(symbmtx->cblknbr == 1); /* juste pour voir, inutile */
    nnz += symbmtx->hdim[k]*(symbmtx->ccblktab[k].lcolnum-symbmtx->ccblktab[k].fcolnum+1);
  }

  return nnz;
}

int DBbuffer_size(int nnzB);
void DBposte_block_receive(int indnum, mpi_t tag, DBMatrixFactComm *FC);
void DBadd_contrib(VSolverMatrix* mat, DBBlockComm *bc);
void DBgather_contrib(VSolverMatrix*, DBBlockComm *bc, VSolverMatrix* cstab);
int DBpack_matrix(VSolverMatrix* mat, COEF *coeff_buff, int *coeff_size_packed, int limitsize);

void DBMatrixFactComm_Setup(DBMatrixFactComm *FC, DBDistrMatrix *DM, PhidalDistrHID *DBL)
{
  dim_t i, j, k, m;
  int ind;
  DBBlockComm *bc;
  PhidalHID *BL;
  DBMatrix *M;

  PRINT("comm.c : DBMatrixFactComm_Setup\n");

  DBMatrixFactComm_Init(FC);
  
  BL = &(DBL->LHID);
  M = &(DM->M);
  
  FC->comm_memratio = COMM_MEMRATIO;

  FC->proc_id = DM->proc_id;

  FC->bcomnbr = DM->sharenbr;

  FC->mpicom = DBL->mpicom;

  /* printf("-->%d\n", DM->sharenbr); */

  if (DM->sharenbr > 0) 
    FC->bcomtab = (DBBlockComm *)malloc(sizeof(DBBlockComm) * DM->sharenbr);
  else 
    FC->bcomtab = NULL;  

  FC->maxproc = 1; /** Need at least one buffer in cstab **/ 

  for(i=M->tlj; i<= M->brj; i++)
    for(k=M->cia[i];k<M->cia[i+1];k++)
      {
	ind = DM->cind[k];

	if(ind >= 0)
	  {
	    bc = FC->bcomtab+ind;
	    if(DM->clead[k] == DM->proc_id)
	      {
#ifdef DEBUG_M
		assert(DM->pset_index[ind+1]-DM->pset_index[ind] > 1);
#endif
		DBBlockComm_Init(bc, DM->pset_index[ind+1]-DM->pset_index[ind] - 1);
		m = 0;
		for(j=DM->pset_index[ind];j<DM->pset_index[ind+1];j++)
		  if(DM->pset[j] != DM->proc_id)
		    bc->proctab[m++] = DM->pset[j];
		if(m  > FC->maxproc)
		  FC->maxproc = m;
	      }
	    else
	      {
		DBBlockComm_Init(bc, 1);
		bc->proctab[0] = DM->clead[k];
	      }

	    bc->buffsize = DBbuffer_size((int)VSolverMatrix_nnz(M->ca[k])/* ,nbcoli, nbcolj, FC->comm_memratio */);

	  }
      }

  if(FC->maxproc >0)
    {

      FC->status = (MPI_Status *)malloc(sizeof(MPI_Status)*FC->maxproc);
    }

  
}


void DBMatrixFactComm_PosteCtrbReceive(DBMatrixFactComm *FC, DBDistrMatrix *DM, dim_t tlj, dim_t brj) {
  int /* i, */ j, k, ind;
  DBMatrix *M;
  M = &(DM->M);
  
  PRINT("comm.c : DBMatrixFactComm_PosteCtrbReceive\n");

#ifdef DEBUG_M
  /* assert(M->tli <= tli); */
  /* assert(M->bri >= bri); */
  assert(M->tlj <= tlj);
  assert(M->brj >= brj);
#endif
  
  /* if(bri - tli < brj - tlj) { */
/*     assert(0); */
/*     for(i=tli;i<=bri;i++) */
/*       { */
/* 	for(k=M->ria[i];k<M->ria[i+1];k++) */
/* 	  { */
/* 	    ind = DM->rind[k]; */
/* 	    if(ind >= 0 && DM->rlead[k] == DM->proc_id) */
/* 	      DBposte_block_receive(ind, CTRB_TAG, FC); */
/* 	  } */
/*       } */
/*   } else */
/*     { */
      for(j=tlj;j<=brj;j++)
	{
	  for(k=M->cia[j];k<M->cia[j+1];k++)
	    {
	      ind = DM->cind[k];
	      if(ind >= 0 && DM->clead[k] == DM->proc_id)
		{
		  /* fprintf(stderr, "Proc %d poste ctrb receive for block %d %d \n", DM->proc_id, M->cja[k], j); */
		  DBposte_block_receive(ind, CTRB_TAG, FC);
		}
	    }
	}
  /*   } */
}


void DBposte_block_receive(int indnum, mpi_t tag, DBMatrixFactComm *FC)
{
  dim_t k;
  DBBlockComm *bc;
#ifdef DEBUG_M
  assert(indnum >= 0);
#endif
  
  PRINT("comm.c : DBposte_block_receive\n");

  bc = FC->bcomtab+indnum;
  /*fprintfv(5, stderr, "indnum %d rqnbr %d \n", indnum, bc->rqnbr);*/

  if(bc->posted == 1) { /** The messages are already posted **/
#ifdef DEBUG_M
    fprintfd(stderr, "Proc %d : poste_block_receive : receive already posted\n", FC->proc_id);
#endif
    return;
  }
 
  
  for(k=0;k<bc->rqnbr;k++)
    {
#ifdef DEBUG_M
      assert(bc->buffsize>0);
#endif
     
      bc->cbufftab[k] = (COEF *)malloc(sizeof(COEF)*(bc->buffsize));
      
      assert( bc->cbufftab[k] != NULL);
      
      /** Poste the Irecv for indice and coeff **/
      /*fprintfv(5, stderr, "Proc %d : poste_block_receive COEF from proc %d\n", FC->proc_id, bc->proctab[k]);*/
      if(MPI_Irecv( bc->cbufftab[k], CC(bc->buffsize), MPI_COEF_TYPE, bc->proctab[k], 
		    tag, MPI_COMM_WORLD, bc->crqtab+k) != MPI_SUCCESS)
	{
	  fprintfd(stderr, "Proc %d : Error in poste_block_receive \n", FC->proc_id);
	  MPI_Abort(MPI_COMM_WORLD, -1);
	}
      
    }

  bc->posted = 1;
  
}


flag_t DBreceive_contrib(flag_t mode, VSolverMatrix* mat, int indnum, DBMatrixFactComm *FC)
{
  /*---------------------------------------/
    / Receive contributions for submatrix mat/
    / identified by indnum                   /
    / and add them to these blocks           /
    /---------------------------------------*/
  dim_t i;
  DBBlockComm *bc;

  PRINT("comm.c : DBreceive_contrib\n");

  bc = FC->bcomtab+indnum;

  if(mode == ASYNCHRONOUS)
    {
      int flag;
      MPI_Testall(bc->rqnbr, bc->irqtab, &flag, FC->status);
      if(flag != TRUE)
	return NOT_READY;
      MPI_Testall(bc->rqnbr, bc->crqtab, &flag, FC->status);
      if(flag != TRUE)
	return NOT_READY;
    }

  /** RECEIVE COEFF **/
  if(MPI_Waitall(bc->rqnbr, bc->crqtab, FC->status) 
     != MPI_SUCCESS)
    {
      fprintfd(stderr, "Proc %d, Error in receive_ctrb for block indnum = %d \n", FC->proc_id, indnum);
      MPI_Abort(MPI_COMM_WORLD, -1);
    }

  /** ADD CONTRIBUTIONS **/
  //printf("ICI : add contrib\n");
  DBadd_contrib(mat, bc);

  for(i=0;i<bc->rqnbr;i++)
    if(bc->cbufftab[i] != NULL)
      {
	free(bc->cbufftab[i]);
	bc->cbufftab[i] = NULL;
      }


  bc->posted = 0;
  return READY;
}

int DBreceive_gather(flag_t mode, VSolverMatrix* mat, int indnum, DBMatrixFactComm *FC)
{
  /*---------------------------------------/
  / Receive contributions for submatrix mat/
  / identified by indnum                   /
  / and add them to these blocks           /
  /---------------------------------------*/
  dim_t i;
  DBBlockComm *bc;

  assert(0);

  bc = FC->bcomtab+indnum;

  if(mode == ASYNCHRONOUS)
    {
      int flag;
/*       MPI_Testall(bc->rqnbr, bc->irqtab, &flag, FC->status); */
/*       if(flag != TRUE) */
/* 	return NOT_READY; */
      MPI_Testall(bc->rqnbr, bc->crqtab, &flag, FC->status);
      if(flag != TRUE)
	return NOT_READY;
    }

/*   /\** RECEIVE INDICE **\/ */
/*   /\*fprintfd(stderr, "Proc %d : wait receive IND from %d procs\n", FC->proc_id, bc->rqnbr);*\/ */
/*   if(MPI_Waitall(bc->rqnbr, bc->irqtab, FC->status) */
/*      != MPI_SUCCESS) */
/*     { */
/*       fprintfd(stderr, "Proc %d, Error in receive_ctrb for block indnum = %d \n", FC->proc_id, indnum); */
/*       MPI_Abort(FC->mpicom, -1); */
/*     } */
 
  /** RECEIVE COEFF **/
  /*fprintfd(stderr, "Proc %d : wait receive COEFF from %d procs\n", FC->proc_id, bc->rqnbr);*/
  if(MPI_Waitall(bc->rqnbr, bc->crqtab, FC->status) 
     != MPI_SUCCESS)
    {
      fprintfd(stderr, "Proc %d, Error in receive_ctrb for block indnum = %d \n", FC->proc_id, indnum);
      MPI_Abort(FC->mpicom, -1);
    }

  /** ADD CONTRIBUTIONS **/
  DBgather_contrib(mat, bc, FC->cstab/* , FC->tmpj, FC->tmpa, FC->jrev */);

  /** Free the receive buffers **/
/*   for(i=0;i<bc->rqnbr;i++) */
/*     if(bc->ibufftab[i] != NULL) */
/*       { */
/* 	free(bc->ibufftab[i]); */
/* 	bc->ibufftab[i] = NULL; */
/* 	FC->bufmem -= sizeof(dim_t)*bc->buffsize; */
/* 	FC->posted--; */
/*       } */
  for(i=0;i<bc->rqnbr;i++)
    if(bc->cbufftab[i] != NULL)
      {
	free(bc->cbufftab[i]);
	bc->cbufftab[i] = NULL;
	FC->bufmem -= sizeof(COEF)*bc->buffsize;
	FC->posted--;
      }
  
  bc->posted = 0;
  return READY;
}


flag_t DBreceive_matrix(flag_t mode, VSolverMatrix* mat, int indnum, DBMatrixFactComm *FC)
{
  /***** ALWAYS USE SYNCHRONOUS MODE BECAUSE THERE IS NO TAG ON
	 THE MESSAGE ******/

/*   PRINT("comm.c : DBreceive_matrix\n"); */

  /*-------------------------------------------/
    / Receive a SparRow matrix from the leader   /
    / of this matrix                             /
    /-------------------------------------------*/
  DBBlockComm *bc;


  bc = FC->bcomtab+indnum;

#ifdef DEBUG_M
/*   printf("%d\n", bc->rqnbr); */
  assert(bc->rqnbr == 1);
#endif

  if(mode == ASYNCHRONOUS)
    {
      int flag;
      MPI_Test(bc->irqtab, &flag, FC->status);
      if(flag != TRUE)
	return NOT_READY;
      MPI_Test(bc->crqtab, &flag, FC->status);
      if(flag != TRUE)
	return NOT_READY;
    }
#ifdef TRACE_COM
  fprintfd(stderr, "comm.c : Proc %d receive matrix \n", FC->proc_id);
#endif

  /** RECEIVE COEFF **/
/*   printf("%p %p\n", bc->crqtab, FC->status); */
  if(MPI_Wait(bc->crqtab, FC->status) 
     != MPI_SUCCESS)
    {
      fprintfd(stderr, "comm.c : Proc %d, Error in receive_matrix for block indnum = %d \n", FC->proc_id, indnum);
      MPI_Abort(MPI_COMM_WORLD, -1);
    }
#ifdef TRACE_COM
  fprintfd(stderr, "comm.c --- DONE Proc %d receive matrix \n", FC->proc_id);
#endif
  /** UNPACK THE MATRIX IN mat **/

  DBMatrix_MatricesCopy(mat, bc->cbufftab[0]);

  /*   memset(mat->solvmtx->coeftab, 0, sizeof(mat->solvmtx->coefnbr)); */
  /*   DBMatrix_MatricesAdd(mat, 1, &bc->cbufftab[0]); */

  if(bc->cbufftab[0] != NULL)
    {
      free(bc->cbufftab[0]);
      bc->cbufftab[0] = NULL;
    }


  return READY;
}

void DBsend_matrix(VSolverMatrix* mat, int indnum, mpi_t tag, DBMatrixFactComm *FC)
{
  dim_t k;
  DBBlockComm *bc;
  COEF *coeff_buffptr;
  int coeff_size_packed;
  
  PRINT("comm.c : DBsend_matrix\n");

  bc = FC->bcomtab+indnum;
 
  coeff_buffptr = (COEF *)malloc(sizeof(COEF)*bc->buffsize);
  assert(coeff_buffptr != NULL);

  /** Pack contrib of block (i,j) into buffers **/
  coeff_size_packed = 0;

  if(DBpack_matrix(mat, coeff_buffptr, &coeff_size_packed, bc->buffsize)!=0)
    {
      fprintfd(stderr, "Error in pack_matrix: unsufficient memory allocated for buffer \n");
      /*       fprintfv(5, stderr, "Number of nnz in matrix to pack = %ld Dim = %ld \n", (long)CSnnz(mat), (long)mat->n); */
      fprintfd(stderr, "buffer size in NNZ = %ld, try a higher value for comm_memratio\n", (long int)bc->buffsize);
      MPI_Abort(MPI_COMM_WORLD, -1);
    } 
  
  /** The dest. of the message is the same set of processor 
      than the one from which the local processor waits for message **/
  for(k=0;k<bc->rqnbr;k++)
    {
#ifdef TRACE_COM
      fprintfd(stderr, "comm.c : Proc %d send matrix to proc %d\n", FC->proc_id, bc->proctab[k]);
#endif
      MPI_Send(coeff_buffptr, CC(coeff_size_packed), MPI_COEF_TYPE, bc->proctab[k], tag, MPI_COMM_WORLD);
#ifdef TRACE_COM
      fprintfd(stderr, "comm.c : --- DONE Proc %d send matrix to proc %d\n", FC->proc_id, bc->proctab[k]);
#endif
    }
 
  free(coeff_buffptr);

}

int reinitVSolverMatrix(VSolverMatrix* mat)
{
  SymbolMatrix* symbmtx = &mat->symbmtx;
  SolverMatrix* solvmtx = mat->solvmtx;

  /* hack , pb valeur solvmtx->coeftab dans la VSolverMatrix */
  solvmtx->coefnbr = symbmtx->hdim[0]*(symbmtx->ccblktab[0].lcolnum-symbmtx->ccblktab[0].fcolnum+1);
 
  memset(solvmtx->coeftab, 0, solvmtx->coefnbr*sizeof(COEF));
}

int DBpack_matrix(VSolverMatrix* mat, COEF *coeff_buff, int *coeff_size_packed, int limitsize)
{
  /*** PACK THE COEFFICIENTS  ***/

  SymbolMatrix* symbmtx = &mat->symbmtx;
  SolverMatrix* solvmtx = mat->solvmtx;

  PRINT("comm.c : DBpack_matrix\n");

#define OLD
#ifndef OLD
  /* en alloc=BLK, c super simple ! */

  assert(symbmtx->cblknbr == 1);
  assert(solvmtx->coefnbr == symbmtx->hdim[0]*(symbmtx->ccblktab[0].lcolnum-symbmtx->ccblktab[0].fcolnum+1));
 
  memcpy(coeff_buff, solvmtx->coeftab, solvmtx->coefnbr*sizeof(COEF));
  (*coeff_size_packed) = solvmtx->coeftab;

#else

  dim_t k,i;
  int ind = 0;
  COEF* bc;
  blas_t stride, hdim;

  for (k=0; k<symbmtx->cblknbr; k++) {
    hdim = symbmtx->hdim[k];
    if (hdim == 0) continue;

    bc = solvmtx->coeftab + solvmtx->bloktab[symbmtx->bcblktab[k].fbloknum].coefind;
    stride = symbmtx->stride[k];

    if (ind + hdim*(symbmtx->ccblktab[k].lcolnum-symbmtx->ccblktab[k].fcolnum+1) > limitsize) 
      return -1;
   
    for(i=symbmtx->ccblktab[k].fcolnum; i<=symbmtx->ccblktab[k].lcolnum; i++) {
      memcpy(coeff_buff+ind, bc, hdim*sizeof(COEF));
      bc  += stride;
      ind += hdim;
    }
  }

  (*coeff_size_packed) = ind;

#endif

  return 0;
}

int DBbuffer_size(int nnzB/* , int nbcoli, int nbcolj, REAL commratio */)
{
  PRINT("comm.c : DBbuffer_size\n");

  return nnzB;
}

void DBMatrix_MatricesCopy(VSolverMatrix* a, COEF *coeff_buff) {
/*   memset(a->solvmtx->coeftab, 0, sizeof(a->solvmtx->coefnbr));  */
/*   DBMatrix_MatricesAdd(a, 1, &coeff_buff); */

  dim_t k,i;
  blas_t stride, hdim;
  int ind=0;
  COEF* bc;
  SymbolMatrix* symbmtx = &a->symbmtx;
  SolverMatrix* solvmtx = a->solvmtx;
  
  PRINT("comm.c : DBMatrix_MatricesCopy\n");

#define OLD_2
#ifndef OLD_2
  /* en alloc=BLK, c super simple ! */

  assert(0);

  /* hack , pb valeur solvmtx->coeftab dans la VSolverMatrix */
  assert(symbmtx->cblknbr == 1);
  solvmtx->coefnbr = symbmtx->hdim[0]*(symbmtx->ccblktab[0].lcolnum-symbmtx->ccblktab[0].fcolnum+1);
 
  memcpy(coeff_buff, solvmtx->coeftab, solvmtx->coefnbr*sizeof(COEF));
  (*coeff_size_packed) = solvmtx->coeftab;

#else

  for (k=0; k<symbmtx->cblknbr; k++) {
    {
      hdim = symbmtx->hdim[k];
      if (hdim == 0) continue;
      
      bc = solvmtx->coeftab + solvmtx->bloktab[symbmtx->bcblktab[k].fbloknum].coefind;
      stride = symbmtx->stride[k];

      /**  **/
      for(i=symbmtx->ccblktab[k].fcolnum; i<=symbmtx->ccblktab[k].lcolnum; i++) {
	memcpy(bc, coeff_buff+ind, hdim*sizeof(COEF));
	bc  += stride;
	ind += hdim;
      }
      
    }
  }

#endif
}
  

/* void VSolverMatrix_p(VSolverMatrix* a) { */
/* /\*   COEF un=1.0; *\/ */
/* /\*   int UN=1; *\/ */

/*   int /\* im,  *\/i, /\* j, *\/ k; */
/*   blas_t stride, hdim; */
/*   int ind=0; */
/*   COEF* bc; */
/*   SymbolMatrix* symbmtx = &a->symbmtx; */
/*   SolverMatrix* solvmtx = a->solvmtx; */

/*   printfd("ADD %p %d\n", a, symbmtx->cblknbr); */
  
/*   for (k=0; k<symbmtx->cblknbr; k++) { */
/*     { */
/*       hdim = symbmtx->hdim[k]; */
/*       if (hdim == 0) continue; */
      
/*       bc = solvmtx->coeftab + solvmtx->bloktab[symbmtx->bcblktab[k].fbloknum].coefind; */
/*       stride = symbmtx->stride[k]; */
      
/*       printfd("ICI\n"); */

/*       /\** Add row i of each contribution matrices **\/ */
/*       for(i=symbmtx->ccblktab[k].fcolnum; i<=symbmtx->ccblktab[k].lcolnum; i++) { */
/* /\* 	for(im=0;im<matnbr;im++) *\/ */
/* /\* 	  { *\/ */
/* 	    /\* BLAS_AXPY(hdim, un, coeff_buff[im]+ind, UN, bc, UN); *\/ */

/* 	    printfd("ADD\n"); */
/* /\* 	  } *\/ */
/* 	bc  += stride; */
/* 	ind += hdim; */
/*       } */
/*     } */
/*   } */
/*   exit(1); */
/* } */

void DBMatrix_MatricesAdd(VSolverMatrix* a, int matnbr, COEF **coeff_buff) {

  COEF un=1.0;
  int UN=1;

  int im, i, /* j, */ k;
  blas_t stride, hdim;
  int ind=0;
  COEF* bc;
  SymbolMatrix* symbmtx = &a->symbmtx;
  SolverMatrix* solvmtx = a->solvmtx;
  
  PRINT("comm.c : DBMatrix_MatricesAdd %d\n", matnbr);

#define OLD_3
#ifndef OLD_3
  /* en alloc=BLK, c super simple ! */

  assert(0);

  /* hack , pb valeur solvmtx->coeftab dans la VSolverMatrix */
  assert(symbmtx->cblknbr == 1);
  solvmtx->coefnbr = symbmtx->hdim[0]*(symbmtx->ccblktab[0].lcolnum-symbmtx->ccblktab[0].fcolnum+1);
 
  memcpy(coeff_buff, solvmtx->coeftab, solvmtx->coefnbr*sizeof(COEF));
  (*coeff_size_packed) = solvmtx->coeftab;

#else

  for (k=0; k<symbmtx->cblknbr; k++) {
    {
      hdim = symbmtx->hdim[k];
      if (hdim == 0) continue;
      
      bc = solvmtx->coeftab + solvmtx->bloktab[symbmtx->bcblktab[k].fbloknum].coefind;
      stride = symbmtx->stride[k];
      
      /** Add row i of each contribution matrices **/
      for(i=symbmtx->ccblktab[k].fcolnum; i<=symbmtx->ccblktab[k].lcolnum; i++) {
	for(im=0;im<matnbr;im++)
	  {
	    /* printf("ICI %lf <- += %lf\n", bc[0], (coeff_buff[im]+ind)[0]); */
	    BLAS_AXPY(hdim, un, coeff_buff[im]+ind, UN, bc, UN);
	  }
	bc  += stride;
	ind += hdim;
      }
    }
  }
#endif
}

void DBMatrix_MatricesGather(VSolverMatrix* a, int matnbr, COEF **coeff_buff) {

  COEF un=1.0;
  int UN=1;

  int im, i, /* j, */ k;
  blas_t stride, hdim;
  int ind=0;
  COEF* bc;
  SymbolMatrix* symbmtx = &a->symbmtx;
  SolverMatrix* solvmtx = a->solvmtx;
  
  PRINT("comm.c : DBMatrix_MatricesGather %d\n", matnbr);

#define OLD_4
#ifndef OLD_4
  /* en alloc=BLK, c super simple ! */

  assert(0);

  /* hack , pb valeur solvmtx->coeftab dans la VSolverMatrix */
  assert(symbmtx->cblknbr == 1);
  solvmtx->coefnbr = symbmtx->hdim[0]*(symbmtx->ccblktab[0].lcolnum-symbmtx->ccblktab[0].fcolnum+1);
 
  memcpy(coeff_buff, solvmtx->coeftab, solvmtx->coefnbr*sizeof(COEF));
  (*coeff_size_packed) = solvmtx->coeftab;

#else

  for (k=0; k<symbmtx->cblknbr; k++) {
    {
      hdim = symbmtx->hdim[k];
      if (hdim == 0) continue;
      
      bc = solvmtx->coeftab + solvmtx->bloktab[symbmtx->bcblktab[k].fbloknum].coefind;
      stride = symbmtx->stride[k];
      
      /** Add row i of each contribution matrices **/
      for(i=symbmtx->ccblktab[k].fcolnum; i<=symbmtx->ccblktab[k].lcolnum; i++) {
	for(im=0;im<matnbr;im++)
	  {
	    
	    /* if several entryies */
	    /* a(i,j) in the contribution have not the same value then the   */
	    /* one with the maximum modulus is retained 	*/
	    {
	      int l;
	      for(l=0; l<hdim; l++) {
		COEF val = (coeff_buff[im]+ind)[l];
		if (coefabs(bc[l]) <  coefabs(val))
		  bc[l] = val;
	      }
   	    }

	  }
	bc  += stride;
	ind += hdim;
      }
    }
  }
#endif
}
  
void DBadd_contrib(VSolverMatrix* mat, DBBlockComm *bc)
{

  PRINT("comm.c : DBadd_contrib\n");
      
#ifdef DEBUG_M
  assert(bc->rqnbr>0);
#endif

  /*   DBMatrix_MatricesCopy(mat, bc->cbufftab[0]); */
  /*   VSolverMatrix_print(stdout, mat, DIAG); */
  
  /** Add the contributions in matrix mat **/
  /*   DBMatrix_MatricesAdd(mat, bc->rqnbr, cstab/\* , tmpj, tmpa, jrev *\/); */
  DBMatrix_MatricesAdd(mat, bc->rqnbr, bc->cbufftab);
     
}

void DBgather_contrib(VSolverMatrix* mat, DBBlockComm *bc, VSolverMatrix* cstab /*, int *tmpj, COEF *tmpa, int *jrev */)
{
  dim_t k;
/*   csptr t; */
/*   if(mat->inarow != 0) */
/*     CSunrealloc(mat); */

  assert(0);
  
#ifdef DEBUG_M
/*   assert(mat->inarow == 0); */
  assert(bc->rqnbr>0);
#endif

/*   for(k=0;k<bc->rqnbr;k++) */
/*     unpack_matrix(cstab+k, bc->ibufftab[k], bc->cbufftab[k]); */
  
  /** Add the contributions in matrix mat **/
  DBMatrix_MatricesGather(mat, bc->rqnbr, bc->cbufftab/* cstab, tmpj, tmpa, jrev */);

}

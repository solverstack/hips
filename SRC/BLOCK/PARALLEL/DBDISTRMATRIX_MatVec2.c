/* @authors J. GAIDAMOUR */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>

#include "solver.h"
#include "db_parallel.h"

/* calcul ordonnancé comme ds MatVec.c mais possible de faire autrement ? (une boucle ?) */ 

void DBDISTRMATRIX_MatVec2(DBDistrMatrix *L, DBDistrMatrix *U, PhidalDistrHID *DBL, COEF *x, COEF *y)
{

  assert(0);
#ifdef TIOTI
 /*  DBMATRIX_MatVec(L, U, BL, x, y); */
/*   return; */

  /*****************************************************************/
  /* This function does y = M.x                                    */
  /*****************************************************************/

  /*---------------------------------------------------------------------
    | This function does the matrix vector product y = A x.
    |----------------------------------------------------------------------
    | on entry:
    | solvmtx = the matrix (in SolverMatrix form)
    | x       = a vector
    |
    | on return
    | y     = the product A * x
    | TODO : expliquer UPLO
    |--------------------------------------------------------------------*/
  int i,i0,j,k,p,p2;
  dim_t tli = L->a[0].symbmtx.tli;

  COEF* xptr;
  COEF* yptr;
  COEF* solvmtxptr;
  int height, width, stride;

  SymbolMatrix *SL, *symbLUs;
  SolverMatrix *solvSL;
  SolverMatrix *solvLs, *solvUs;


  /* vecteur tmp */
  COEF* tmp = (COEF*)malloc(sizeof(COEF)*L->nodenbr);
  int jtmp, size;

  COEF zero=0.0, one=1.0;
  char *uploL = "L";
  char *transN = "N", *transT = "T";
  int UN = 1;

  /*   assert((L->alloc == ONE) || (L->alloc == CBLK) || (L->alloc == RBLK) || (L->alloc == BLK)); */
  /*   assert((U->alloc == ONE) || (U->alloc == CBLK)); todo : unsym */

  bzero(y, sizeof(COEF)*L->nodenbr);

  if (L->alloc == ONE) {
    solvSL = &L->a[0];    
    solvLs = &L->a[0];
    solvUs = &U->a[0];
  }

  /* parcours par blocs colonnes */
  for(i0=0,i=L->tlj; i<=L->brj; i0++, i++) {
    SL = &L->ca[L->cia[i]]->symbmtx;
/*     assert(SL == U->ra[U->ria[i]]); unsym */
    
    if (L->alloc == CBLK) {
      solvSL = &L->a[i0];
      solvLs = &L->a[i0];
      solvUs = &U->a[i0];
    }
    
    if (L->alloc == BLK) {
      solvSL = &L->a[L->cia[i]];
    }
    
    if (L->alloc == RBLK) {
      solvSL = &L->a[L->cja[L->cia[i]]-L->tli];
    }

    for(k=0;k<SL->cblknbr;k++) {
      width  = SL->ccblktab[k].lcolnum - SL->ccblktab[k].fcolnum +1;
      stride = SL->stride[k]; 
      p    = SL->bcblktab[k].fbloknum; /* triangular block */
      xptr = x + SL->ccblktab[k].fcolnum - tli;
      yptr = y + SL->ccblktab[k].fcolnum - tli;
      	
      /*******************************************************************************/
      if (L == U) {
	/* bloc triangulaire */
	
	/* y := alpha*A*x + beta*y */
	
	solvmtxptr = solvSL->coeftab + solvSL->bloktab[p].coefind;

	BLAS_SYMV(uploL, width, one /*alpha*/,
	      solvmtxptr, stride, xptr, UN,
	      one /*beta*/, yptr, UN);
      }
      /*******************************************************************************/

      for(j=L->cia[i], jtmp=0; j<L->cia[i+1]; j++) {
	symbLUs = &L->ca[j]->symbmtx; /*L ! */
	
	p = symbLUs->bcblktab[k].fbloknum;
	if ((L == U) && (j == U->cia[i])) {
	  if (p == symbLUs->bcblktab[k].lbloknum)
	    continue; /*simplifier*/
	  p++;
	}

	/* tmp := x */
	for(p2=p; p2<=symbLUs->bcblktab[k].lbloknum; p2++) {
	  size = symbLUs->bloktab[p2].lrownum - symbLUs->bloktab[p2].frownum +1;
	  BLAS_COPY(size, x + symbLUs->bloktab[p2].frownum - tli, UN, tmp + jtmp, UN);
	  jtmp += size;
	}

      }

      /* calcul ligne */
      for(j=L->cia[i], jtmp=0; j<L->cia[i+1]; j++) {
	symbLUs = &L->ca[j]->symbmtx;
	
	if (L->alloc == RBLK) {
	  solvUs = &U->a[U->cja[j]-U->tli];
	}
	
	if(L->alloc == BLK) {
	  solvUs = &U->a[j];
	}

	p = symbLUs->bcblktab[k].fbloknum;
	height = symbLUs->hdim[k];           /** Hauteur de la surface compactÃ©e des bloc extra-diagonaux **/
	stride = symbLUs->stride[k];
	if ((L == U) && (j == U->cia[i])) {
	  if (p == symbLUs->bcblktab[k].lbloknum)
	    continue; /*simplifier*/
	  p++;
	  height = symbLUs->hdim[k] - width; /** Hauteur de la surface compactÃ©e des bloc extra-diagonaux **/
	}

	if (height == 0) continue;

	solvmtxptr = solvUs->coeftab + solvUs->bloktab[p].coefind;
	/* calcul ligne */
	BLAS_GEMV(transT, height, width, one /*coef alpha*/,
		  solvmtxptr, stride,
		  tmp + jtmp, UN,       one /*coef beta*/,
		  yptr, UN);
	jtmp += height;
      }

      /*******************************************************************************/

      for(j=L->cia[i], jtmp=0; j<L->cia[i+1]; j++) {
	symbLUs = &L->ca[j]->symbmtx;

	if (L->alloc == RBLK) {
	  solvLs = &L->a[L->cja[j]-L->tli];
	}
	
	if(L->alloc == BLK) {
	  solvLs = &L->a[j];
	}

	p = symbLUs->bcblktab[k].fbloknum;
	height = symbLUs->hdim[k];           /** Hauteur de la surface compactÃ©e des bloc extra-diagonaux **/
	stride = symbLUs->stride[k];
	if (j == L->cia[i]) {
	  if (p == symbLUs->bcblktab[k].lbloknum)
	    continue; /*simplifier*/
	  p++;
	  height = symbLUs->hdim[k] - width; /** Hauteur de la surface compactÃ©e des bloc extra-diagonaux **/
	}

	if (height == 0) continue;
	
	solvmtxptr = solvLs->coeftab + solvLs->bloktab[p].coefind;
	
	/* calcul colonne */
	BLAS_GEMV(transN, height, width, one /*coef alpha*/,
		  solvmtxptr, stride,
		  xptr, UN,      zero /*coef beta*/,
		  tmp + jtmp, UN);
	
	jtmp += height;
      }

      for(j=L->cia[i], jtmp=0; j<L->cia[i+1]; j++) {
     	symbLUs = &L->ca[j]->symbmtx;

	p = symbLUs->bcblktab[k].fbloknum;
	if (j == L->cia[i]) {
	  if (p == symbLUs->bcblktab[k].lbloknum)
	    continue; /*simplifier*/
	  p++;
	}
	/* y := tmp  */
	for(p2=p; p2<=symbLUs->bcblktab[k].lbloknum; p2++) {
	  size = symbLUs->bloktab[p2].lrownum - symbLUs->bloktab[p2].frownum +1;
	  BLAS_AXPY(size, one, tmp + jtmp, UN, y + symbLUs->bloktab[p2].frownum - tli, UN);
	  jtmp += size;
	}
      }

      /*******************************************************************************/
      
    }
  }
  
  free(tmp);
#endif  
}

/* @authors J. GAIDAMOUR */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>

#define PARALLEL

#include "block.h"
#include "db_parallel.h"
#include "phidal_parallel.h"

void DBDistrPrec_Unscale(REAL *scaletab, REAL *iscaletab, DBDistrPrec *P, PhidalDistrHID *BL)
{

  /*****************************************************/
  /*  A = Dr^-1.A'.Dr^-1                               */
  /*  A = Dr^-1.L'.D'.L't.Dr^-1                        */
  /*  A = Dr^-1.L'.Dr . Dr^-1.D'.Dr^-1  . Dr.L't.Dr^-1 */
  /*  A =     L       .      D          . Lt           */
  /*****************************************************/

#ifdef DEBUG_M
  assert(P->symmetric == 1);
#endif

  DBDistrMatrix_RowMult(iscaletab, PREC_L_BLOCK(P), BL);  
  DBDistrMatrix_ColMult(scaletab, PREC_L_BLOCK(P), BL);
  
  DBDistrMatrix_DiagMult(iscaletab, PREC_L_BLOCK(P), BL); /* iscaletab for DB, scaletab for Phidal */
  
  if(P->forwardlev > 0)
    {
      
      if (P->nextprec != NULL) {
        DBDistrPrec_Unscale(scaletab + PREC_L_BLOCK(P)->M.dim1, iscaletab + PREC_L_BLOCK(P)->M.dim1, P->nextprec, BL);
      } else {
        PhidalDistrPrec_SymmetricUnscale(scaletab + PREC_L_BLOCK(P)->M.dim1, iscaletab + PREC_L_BLOCK(P)->M.dim1, P->phidalPrec, BL);
      }

      /* Si plus de deux niveaux*/
      /*       if(P->E->virtual == 0) */
      /*        { */
      /*          PhidalDistrMatrix_RowMult(iscaletab + PREC_L_BLOCK(P)->dim1, P->E, BL); */
      /*          PhidalDistrMatrix_ColMult(iscaletab, P->E, BL); */
      /*        } */
 
      if(P->schur_method == 1)
        {
          if (PREC_SL_BLOCK(P) != NULL) { /* equivalent (option->droptol1 == 0) (& -1 hack) */
            DBDistrMatrix_RowMult(iscaletab + PREC_L_BLOCK(P)->M.dim1, PREC_SL_BLOCK(P), BL);  
            DBDistrMatrix_ColMult(iscaletab + PREC_L_BLOCK(P)->M.dim1, PREC_SL_BLOCK(P), BL);
          } else {
            PhidalDistrMatrix_RowMult2(iscaletab + PREC_L_BLOCK(P)->M.dim1, P->phidalS, BL);
            PhidalDistrMatrix_ColMult2(iscaletab + PREC_L_BLOCK(P)->M.dim1, P->phidalS, BL);
          }


	}

      
    }
 
}

void DBDistrPrec_Unscale_Unsym(REAL *scalerowtab, REAL *iscalerowtab, REAL *scalecoltab, REAL *iscalecoltab, DBDistrPrec *P, PhidalDistrHID *BL)
{

  COEF* D;

  DBDistrMatrix_RowMult(iscalerowtab, PREC_L_BLOCK(P), BL);
  DBDistrMatrix_ColMult(scalerowtab,  PREC_L_BLOCK(P), BL);
  
  DBDistrMatrix_Transpose(PREC_U_BLOCK(P));
  DBDistrMatrix_RowMult(iscalecoltab, PREC_U_BLOCK(P), BL); /* /!\ doit Ãªtre inversÃ© par rapport Ã  Phidal (Transpose) */
  DBDistrMatrix_ColMult(iscalerowtab, PREC_U_BLOCK(P), BL); /* /!\ doit Ãªtre inversÃ© par rapport Ã  Phidal (Transpose) */
  DBDistrMatrix_Transpose(PREC_U_BLOCK(P));

  /* ? */
  D = (COEF *)malloc(sizeof(COEF)*PREC_L_BLOCK(P)->M.dim1);
  DBDistrMatrix_Transpose(PREC_U_BLOCK(P));
  DBMatrix_GetDiag(&PREC_U_BLOCK(P)->M, D);
  DBDistrMatrix_Transpose(PREC_U_BLOCK(P));
  DBMatrix_SetDiag(&PREC_L_BLOCK(P)->M, D);
  free(D);

  if(P->forwardlev > 0)
    {
      
      if (P->nextprec != NULL) {
        DBDistrPrec_Unscale_Unsym(scalerowtab + PREC_L_BLOCK(P)->M.dim1, iscalerowtab + PREC_L_BLOCK(P)->M.dim1, 
                                  scalecoltab+PREC_U_BLOCK(P)->M.dim1, iscalecoltab+PREC_U_BLOCK(P)->M.dim1, P->nextprec, BL);
      } else {
        PhidalDistrPrec_UnsymmetricUnscale(scalerowtab + PREC_L_BLOCK(P)->M.dim1, iscalerowtab + PREC_L_BLOCK(P)->M.dim1, 
                                           scalecoltab+PREC_U_BLOCK(P)->M.dim1, iscalecoltab+PREC_U_BLOCK(P)->M.dim1, P->phidalPrec, BL);
      }
      
      if(P->schur_method == 1)
        {
          if (PREC_SL_BLOCK(P) != NULL) { /* equivalent (option->droptol1 == 0) (& -1 hack) */
            DBDistrMatrix_RowMult(iscalerowtab + PREC_L_BLOCK(P)->M.dim1, PREC_SL_BLOCK(P), BL);
            DBDistrMatrix_ColMult(iscalecoltab + PREC_L_BLOCK(P)->M.dim1, PREC_SL_BLOCK(P), BL);
            DBDistrMatrix_RowMult(iscalecoltab + PREC_U_BLOCK(P)->M.dim1, PREC_SU_BLOCK(P), BL);
            DBDistrMatrix_ColMult(iscalerowtab + PREC_U_BLOCK(P)->M.dim1, PREC_SU_BLOCK(P), BL);
          } else {
            PhidalDistrMatrix_RowMult2(iscalerowtab + PREC_L_BLOCK(P)->M.dim1, P->phidalS, BL);
            PhidalDistrMatrix_ColMult2(iscalecoltab + PREC_L_BLOCK(P)->M.dim1, P->phidalS, BL);
          }
        }


    }
}

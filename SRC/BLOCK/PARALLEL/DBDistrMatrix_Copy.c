/* @authors J. GAIDAMOUR */

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include "block.h"

#include "db_parallel.h"
#include <string.h> /* memset */

void DBDistrMatrix_Copy(PhidalMatrix* mat, PhidalDistrHID* DBL, 
		   PhidalMatrix* Phidal, DBDistrMatrix* DBM,
		   char* UPLO, int_t levelnum, int_t levelnum_l, flag_t fill) {

  DBMatrix_Copy(mat, &DBL->LHID, 
		Phidal, &DBM->M,
		UPLO,  levelnum,  levelnum_l,  fill);
    

}

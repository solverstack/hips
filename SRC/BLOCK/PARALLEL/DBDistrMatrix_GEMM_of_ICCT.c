/* @authors J. GAIDAMOUR */

#include <stdio.h>
#include <stdlib.h>
#include <string.h> /* memset */
#include <assert.h>

#define PARALLEL

#include "block.h"
#include "db_parallel.h"

#include "base.h"

/* #define DEBUG Dprintf */
/* #define Dprintf(arg...) printf(arg) */
#define Dprintf(arg...)
#define check(a,b,c)

#define MAX(x,y) (((x)<(y))?(y):(x))
#define MIN(x,y) (((x)>(y))?(y):(x))

struct DBDistrMatrixPart_ {
  INTL  *ia;
  dim_t *ja;
  VSolverMatrix **a;
};
typedef struct DBDistrMatrixPart_  DBDistrMatrixPart;


void DBDistrMatrix_sssGEMM2_sym(int iB, COEF alpha,
				DBDistrMatrix* A, DBDistrMatrixPart* Ap,
				DBDistrMatrix* B, DBDistrMatrixPart* Bp,
				int nk, dim_t *jak, VSolverMatrix** rak,
				DBDistrMatrix* C, DBDistrMatrixPart* Cp,
				DBDistrMatrix* D, COEF* diag,
				int *tabA, int *tabC,
				COEF* E, COEF* F, COEF* W, dim_t tli,
				int Aalloc, PhidalHID* BL);

void DBDistrMatrix_sssGEMM_sym(int i, COEF alpha, 
			DBDistrMatrix* A, INTL **Ap, /* TODO : faire un transpose Ã  la place */
			DBDistrMatrix* B, INTL **Bp,
			int nk, dim_t *jak, VSolverMatrix** rak,
			DBDistrMatrix* C, INTL **Cp,
			DBDistrMatrix* D, COEF* diag,	 
			int *tabA, int *tabC, 
			COEF* E, COEF* F, COEF* W, dim_t tli, int Aalloc, PhidalHID* BL) {
  
  DBDistrMatrix_sssGEMM2_sym(i, alpha,
			     A, (DBDistrMatrixPart*)Ap,
			     B, (DBDistrMatrixPart*)Bp,
			     nk, jak, rak,
			     C, (DBDistrMatrixPart*)Cp,
			     D, diag,
			     tabA, tabC, E, F, W, tli, Aalloc, BL);
}




void DBDistrMatrix_sssGEMM2_sym(int iB, COEF alpha,
				DBDistrMatrix* AA, DBDistrMatrixPart* Ap,
				DBDistrMatrix* BB, DBDistrMatrixPart* Bp,
				int nk, dim_t *jak, VSolverMatrix** rak,
				DBDistrMatrix* CC, DBDistrMatrixPart* Cp,
				DBDistrMatrix* DD, COEF* diag,
				int *tabA, int *tabC,
				COEF* E, COEF* F, COEF* W, dim_t tli, int Aalloc, PhidalHID* BL) {
  
  SymbolMatrix *SA;
  SymbolMatrix *SB;
  
  SymbolMatrix *SD;

  SymbolMatrix *SC;

  SolverMatrix *solvA, *solvB, *solvD, *solvC;
  
  DBMatrix *A=S(AA);
  DBMatrix *B=S(BB);
  DBMatrix *C=S(CC);
  DBMatrix *D=S(DD);

  dim_t j;
  int iA,/* jB, */jA,jC, m;
  dim_t k;
  int pSA, pSB;
  int qSA;

  int cdim, hdim, rdim, rdim2, mdim;
  int Srdim;
  blas_t strideA, strideA2, strideB, strideD;
  int bloknum;
  int decalcol, decalrow;
  int /* facecblknum, */ facestride;

  COEF *ac, *bc, *cc, *wc, *dc;
  COEF *Ec, *ec;

  COEF alphadiag;

  COEF zero = 0.0, one = 1.0;
  char *opA = "N"; /** Ne pas transposer A **/
  char *opB = "T"; /** Transpose B **/
  int UN = 1;

  int tabi;
  int ii;
  
  /* parcours des blk phidal (de B) */
  /* XXXXXXXXXXXXX vrai boucle */
  int jB;
  
  int proc_id = CC->proc_id;
  int leader;

  assert(F == NULL); /* unused */
  assert(A->alloc == BLK);
  assert(B->alloc == BLK);
  assert(C->alloc == BLK);
  assert(Aalloc == BLK);

  for(j=0, jB=B->ria[iB];jB<B->ria[iB+1]-1/*/!\*/;j++, jB++) {

    Dprintf("--- bloc B : id=%d\n", B->ra[jB] - B->ra[0]);

    SB = &B->ra[jB]->symbmtx;
    solvB = B->ra[jB]->solvmtx;
    iA = B->rja[jB];

    mpi_t *ps1, *ps2;
    int k1, k2, l1, l2;
    k1  = BB->rind[jB];
    ps1 = BB->pset + BB->pset_index[k1];
    l1  = BB->pset_index[k1+1] - BB->pset_index[k1];

    /* if (k1 != -1) { */
/*       printf("k1 = %d - l1 = %d - pset = ", k1, l1); */
/*       int g; */
/*       for(g=0; g<l1; g++) { */
/* 	printf("%d ", ps1[g]); */
/*       } */
/*       printf("\n"); */
/*     } */

    assert(iA != iB); /* on s'arrête là */
	
/*   for(j=0; j<nk; j++) { /\* Utilisé dans DBMatrix_GEMMpart *\/ */
/*     SB    = &rak[j]->symbmtx; */
/*     solvB =  rak[j]->solvmtx; */
/*     iA    =  jak[j]; */

    SD = &D->ca[D->cia[iA]]->symbmtx;   /* Diagonal Matrix. first phidal block is diagonal block */
    solvD = D->ca[D->cia[iA]]->solvmtx; /* Diagonal Matrix. first phidal block is diagonal block */

    int ext_diag;
    if(DD->clead[D->cia[iA]] != proc_id) {
      /* printf("use extern diag\n"); */
      ext_diag = 1;
    } else { ext_diag = 0; }

    if(SB->bloknbr == 0) continue;
            
    /***********************/
    /* Recherche des blocs */
    /***********************/
    int stmp=0;
    tabi=0;
    jA=Ap->ia[iA];
    for(jC=Cp->ia[iB];jC<Cp->ia[iB+1];jC++) {
      /* Recherche du block correspondant dans A (s'il existe) */
      while(jA<Ap->ia[iA+1]) { if (Ap->ja[jA] < Cp->ja[jC]) jA++; else goto test; }
      continue;
    test:
      if (Ap->ja[jA] != Cp->ja[jC]) continue;
      assert(Ap->ja[jA] == Cp->ja[jC]);

      /* Compute the product ONLY for local block */
      
      k2  = AA->cind[jC];
      ps2 = AA->pset + AA->pset_index[k2];
      l2  = AA->pset_index[k2+1] - AA->pset_index[k2];
      
/*       if (k2 != -1) { */
/* 	/\* printf("k2 = %d - l1 = %d - pset = ", k1, l1); *\/ */
/* 	int g; */
/* 	for(g=0; g<l1; g++) { */
/* 	  printf("%d ", ps1[g]); */
/* 	} */
/* 	printf("\n"); */
/*       } */

      Dprintf("--- bloc A : id=%d\n", Ap->a[jA] - Ap->a[0]);
      Dprintf("--- bloc C : id=%d\n", Cp->a[jC] - Cp->a[0]);
      
      if (k1 == -1 || k2 == -1){
	leader = proc_id;
	/* printf("leader du calcul = %d (local)\n", leader); */
      } else {
	leader = choose_leader(l1, ps1, l2, ps2);
	/* printf("leader du calcul = %d (balance)\n", leader); */
      }

     
      if (leader == proc_id) {

	tabC[tabi] = jC;
	tabA[tabi] = jA; tabi++;

	stmp += Ap->a[jA]->symbmtx.hdim[0];
	assert(Ap->a[jA]->symbmtx.hdim[0] == Ap->a[jA]->symbmtx.stride[0]);

      }
    }

    /* non, il faut faire la division (equivalent de CS_RowMult(D+kk, LL->ra[jj]);) */
/*     if (tabi == 0) { */
/*       printf("nothing to do\n"); */
/*       return; /\* nothing to do *\/ */
/*     } */

    /* parcours des cblk (de SB) */
    for(k=0;k<SB->cblknbr;k++) { /* pas de boucled si dans S */ assert(k==0);

      Dprintf("---- k de B = %d/%d\n", k, SB->cblknbr-1);

      cdim = SB->ccblktab[k].lcolnum - SB->ccblktab[k].fcolnum +1;

/* #ifdef DEBUG_M2 */
      if ((Aalloc == ONE || Aalloc == RBLK)) {
	Dprintf("%d %d\n", SB->cblktlj, SB->facedecal);
/* 	assert(SB->cblktlj == SB->facedecal); */
      }
/* #endif */

      Dprintf("SB->cblktlj = %d  + SB->facedecal = %d\n", SB->cblktlj, SB->facedecal);
      
/*       if (A->alloc != ONE) { /\* test if temporaire *\/ */
/* 	strideA = strideA2 = A->hdim[k]; /\* TODO : a calculer pour V2 *\/ */
/*       } else { */
	strideA = stmp; /* stride minimal ici == sum des hdim */
/*       } */

      strideB = SB->stride[k];
      strideD = SD->stride[k];

      hdim = SB->hdim[k]; /* the number of columns of the matrix Bt */ /* todo : deplacer vers le bas */
      if (hdim == 0) continue;

      /**************************************************/
      /* ConcatÃ©nation des blocks de A en fonction de C */
      /**************************************************/
      rdim=0;
      Ec = E;
	
      for(ii=0;ii<tabi;ii++) {
	jA = tabA[ii];
	SA    = &Ap->a[jA]->symbmtx;
	solvA = Ap->a[jA]->solvmtx;
	if ( Aalloc == RBLK || Aalloc == BLK) {
	  strideA2 = SA->stride[k];
	}

	Srdim = SA->hdim[k]; /* the number of rows of the matrix A */
	if (Srdim == 0) continue;

	/* copie du cblk de SA dans E */
	pSA  = SA->bcblktab[k].fbloknum;
	check(A, solvA, SA);
	ac = solvA->coeftab + solvA->bloktab[pSA].coefind;
	ec = Ec;
	for(m=0;m<cdim;m++) {
	  BLAS_COPY(Srdim, ac, UN, ec, UN);
	  ac += strideA2;
	  ec += strideA;
	}
	Ec+=Srdim; rdim+=Srdim;
      }
      /* ** */

      /***********************************************************/
      /* "Diviser" le bloc de SB par le bloc diagonal de D dans F*/
      /***********************************************************/
      check(B, solvB, SB);
      check(D, solvD, SD);
      pSB=SB->bcblktab[k].fbloknum; /* ICI */
      assert(solvB->symbmtx.bloknbr > pSB);
      bc = solvB->coeftab + solvB->bloktab[pSB].coefind;
      dc = solvD->coeftab + solvD->bloktab[ SD->bcblktab[k].fbloknum ].coefind;

      /* 	assert(cdim*strideB <= Fsize); */
      int kk;
      if(ext_diag == 1) {
        kk = BL->block_index[iA] - BL->block_index[D->tlj];
      }
      
      for(m=0;m<cdim;m++) {

	if(ext_diag == 1) {
	  /* printf("iA=%d D->tli=%d %d - %d\n", iA, D->tlj); */

	  alphadiag = 1.0 / diag[kk+m];
	  // printf("kk=%d dc[m*strideD + m]=%lf diag[kk+m]=%lf\n", kk, dc[m*strideD + m], diag[kk+m]);
	  // debug, vrai si on fait de clean : assert(dc[m*strideD + m] == diag[kk+m]);
	} else {
#ifndef DEBUG_NOALLOCATION
	  alphadiag = 1.0 / dc[m*strideD + m]; /** alpha est l'inverse du m-ieme terme diagonal du bloc diagonal de D **/
#endif
	}
	
	// printf("%lf %lf\n", dc[m*strideD + m], alphadiag);

	BLAS_SCAL(hdim, alphadiag,  bc, UN); /** On divise la m-ieme colonne par le terme diagonal **/
	bc += strideB;
      }
      /*****************************************************/
      /* -----Affectation dans C                                */
      /*****************************************************/
	
      for(pSB=SB->bcblktab[k].fbloknum; pSB<=SB->bcblktab[k].lbloknum; pSB++) {
	bc = solvB->coeftab + solvB->bloktab[pSB].coefind - solvB->bloktab[ SB->bcblktab[k].fbloknum ].coefind;

	mdim = SB->bloktab[pSB].lrownum - SB->bloktab[pSB].frownum+1; /*** Largueur de la zone modifiÃÂ©e
									   == hauteur du bloc p dans le bloc colonne k **/
	/*****************************************************/
	/* GEMM                                              */
	/*****************************************************/
	/* 	  printfv(5, "GEMM : rdim %d, mdim %d, cdim %d, 1.0, ec, strideA %d, bc, strideB %d, 0.0, W, strideA %d\n", */
	/* 		 rdim, mdim, cdim, strideA, strideB, strideA); */
	if(strideA == 0) continue;
	Dprintf("BLAS_GEMM %d %d %d - %d %d\n", rdim, mdim, cdim, strideA, strideB);
/* 	assert(rdim<strideA); */
	
	BLAS_GEMM(opA, opB, rdim, mdim, cdim, one, E, strideA, bc, strideB, zero, W, strideA);

	/* *****************************/
	SC = &Cp->a[Cp->ia[iB]]->symbmtx;
	solvC = Cp->a[Cp->ia[iB]]->solvmtx; /* todo : inutile ? */

	/** Nombre de colonne "ÃÂ  gauche" de la zone  modifiÃÂ©e dans le bloc colonne facecblknum **/
	decalcol = SB->bloktab[pSB].frownum - SC->ccblktab[0].fcolnum;
	  
	/* *****************************/
	/* parcours des blocs de la cblk nÂ°k de A */
	rdim2=0;
	  
	for(ii=0;ii<tabi;ii++) {
	  jA = tabA[ii];
	  jC = tabC[ii];
	    
	  assert(Ap->ja[jA] == Cp->ja[jC]);
	    
	  SC = &Cp->a[jC]->symbmtx;
	  solvC = Cp->a[jC]->solvmtx;
	  SA = &Ap->a[jA]->symbmtx;
	  solvA = Ap->a[jA]->solvmtx;

	  check(C, solvC, SC);
	  facestride  = SC->stride[0];   /** Stride du cblk en face du bloc extra-diagonal p **/

	  Dprintf("--- bloc A : id=%d\n", Ap->a[jA] - Ap->a[0]);
	  Dprintf("--- bloc C : id=%d\n", Cp->a[jC] - Cp->a[0]);

	  for(qSA=SA->bcblktab[k].fbloknum;qSA<=SA->bcblktab[k].lbloknum;qSA++) {
	    /* *****************************/
	      
	    bloknum = SC->bcblktab[0].fbloknum;     /** Indice du bloc **/
	      
	    /** Calcul du pointeur de debut du bloc correspondant ÃÂ  A(q,k) dans W **/
	    wc = W + rdim2 + solvA->bloktab[qSA].coefind - solvA->bloktab[ SA->bcblktab[k].fbloknum/*todo : rdim*/ ].coefind; /* +  */

	    /** Calcul du pointeur de debut de la zone modifiÃÂ©e dans le bloc C(bloknum, facebloknum) */
	    check(C, solvC, SC);
	    decalrow = SA->bloktab[qSA].frownum - SC->bloktab[bloknum].frownum; /* TODO SC ? C ? */
	    cc = solvC->coeftab + solvC->bloktab[bloknum].coefind + decalcol*facestride + decalrow;
	      
	    hdim = SA->bloktab[qSA].lrownum - SA->bloktab[qSA].frownum+1; /** Hauteur du bloc colonne qSA **/
	      
	    /*** Update de la contribution ***/
	    for(m=0; m < mdim; m++)
	      {
		BLAS_AXPY(hdim, alpha, wc, UN, cc, UN);
		wc += strideA;
		cc += facestride;
	      }
	  }
	    
	  rdim2 += SA->hdim[k]; /* the number of rows of the matrix A */ /* TODO : on peut aussi faire avancer wc au fur et a mesure */
	}

	/** Mettre ÃÂ  jour la hauteur des bloc extra diagonaux qui restent **/
	/* rdim -= SB->bloktab[pSB].lrownum - SB->bloktab[pSB].frownum+1; /\*a faire q=p*\/ */
      }
    }

  }
    
}

void DB_ICCprod2(int iB, COEF alpha, 
		 DBDistrMatrix* A, COEF* diag,
		int nk, dim_t *jak, VSolverMatrix** rak,
		int *tabA, int *tabC, 
		COEF* E, COEF* F, COEF* W, PhidalHID* BL) {
  assert(F == NULL);

  DBDistrMatrix_sssGEMM_sym(iB, alpha, A, &S(A)->cia, A, &S(A)->ria, nk, jak, rak, A, &S(A)->cia, A,
			    diag, tabA, tabC, E, F, W, S(A)->tli, S(A)->alloc, BL);

}



/* TODO : refaire les tests entre concaténé et non concaténé (effet blas) */

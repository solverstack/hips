/* @authors J. GAIDAMOUR */

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h> /*memcpy*/

#include <assert.h>
#include "phidal_parallel.h"
#include "db_parallel.h"

#include "block.h"

#include "base.h"

void PhidalDistrMatrix_Init_fromDBDistr(DBDistrMatrix* Orig, PhidalDistrMatrix* DM, 
					char* UPLO, int_t locally_nbr, PhidalDistrHID *DBL) {

#ifdef NON
  /* PhidalDistrMatrix_Init(Copy); */

  /*   Copy->proc_id = M->proc_id; */
  /*   Copy->nproc   = M->nproc; */
  
  /*   Copy->clead = M->clead; */
  /*   Copy->cind = M->cind; */
  
  /*   Copy->rlead = M->rlead; */
  /*   Copy->rind = M->rind; */
  
  /*   Copy->pset_index = M->pset_index; */
  /*   Copy->pset = M->pset; */
  
  /*   Copy->sharenbr = M->sharenbr;   */
  /*   Copy->maxproc = M->maxproc; */
  
  /*   /\**\/ */
  
  /*   printfv(5, "*** Warning : delete DBDistrMatrix M=%p\n", M); */
  /*   M->clead = NULL; */
  /*   M->cind = NULL; */
  
  /*   M->rlead = NULL; */
  /*   M->rind = NULL; */
  
  /*   M->pset_index = NULL; */
  /*   M->pset = NULL; */
  
  /*   /\* TODO DBMatrixCommVec_Clean(&M->commvec); *\/ */
  
  /**/

  csptr P;
  dim_t i, j, k;
  int ind;
  int nnz;
  PhidalHID *BL;
  int *key, *tmp;
  int kl, tkl;
  int *block_psetindex, *block_pset;
  PhidalMatrix *M;

  BL = &(DBL->LHID);
  M = &(DM->M);

  /**/

  M->symmetric = Orig->M.symmetric;
  M->csc = Orig->M.csc;     /* ÃÂ  vÃÂ©rif */
  M->dim1 = Orig->M.dim1; 
  M->dim2 = Orig->M.dim2;

  M->tli = Orig->M.tli;
  M->tlj = Orig->M.tlj;
  M->bri = Orig->M.bri; 
  M->brj = Orig->M.brj; 

  dim_t tli = M->tli, tlj = M->tlj, bri = M->bri, brj = M->brj;
  char *DIAG = "N";
  /**/

  block_psetindex = DBL->block_psetindex;
  block_pset = DBL->block_pset;

  PhidalDistrMatrix_Init(DM);
  DM->proc_id = DBL->proc_id;
  DM->nproc = DBL->nproc;
  
  if(bri < 0)
    bri = BL->nblock-1;
  if(brj < 0)
    brj = BL->nblock-1;
  
  if(tli<0)
    tli = 0;
  if(tlj<0)
    tlj = 0;
#ifdef DEBUG_M
  assert(bri < BL->nblock);
  assert(brj < BL->nblock);
  assert(tli <= bri);
  assert(tlj <= brj);
#endif

  M->tli = tli;
  M->tlj = tlj;
  M->bri = bri;
  M->brj = brj;
  
  M->dim1 = BL->block_index[bri+1]-BL->block_index[tli]; 
  M->dim2 = BL->block_index[brj+1]-BL->block_index[tlj]; 
  
  if(locally_nbr > BL->nlevel-1 || locally_nbr < 0)
    locally_nbr = BL->nlevel-1;

 

  
  /*****************************************/
  /*** Compute the sparse block pattern ****/
  /*****************************************/
  P = (struct SparRow *)malloc(sizeof(struct SparRow));
  initCS(P, BL->nblock);
  phidal_block_pattern(tli, tlj, bri, brj, UPLO, DIAG, P, locally_nbr, BL);
  nnz = CSnnz(P);


  M->virtual = 0;
  M->bloknbr = nnz;
  if(M->bloknbr > 0)
    {
      M->bloktab = (struct SparRow *)malloc(sizeof(struct SparRow) * M->bloknbr);
      for(i=0;i<M->bloknbr;i++)
	initCS(M->bloktab+i, 0);
    }
  else 
    M->bloktab = NULL;		       

  
  key = (int *)malloc(sizeof(int)*DBL->nproc);
  M->ria  = (int *)malloc(sizeof(int)* (BL->nblock+1));
  M->rja  = (int *)malloc(sizeof(int)* (M->bloknbr));
  DM->rind = (int *)malloc(sizeof(int)* (M->bloknbr));

  cs2csr(P, M->ria, M->rja, NULL);
  /*fprintfv(5, stderr, "\n P \n");
    dumpCS(0, stdout, P);
    dumpcsr(stderr, NULL, M->rja, M->ria, P->n); 
    fprintfv(5, stderr, "\n ");*/

  M->ra = (csptr *)malloc(sizeof(csptr)* (M->bloknbr));
  ind = 0;
  for(i=0;i<P->n;i++)
    for(j=0;j<P->nnzrow[i];j++)
      M->ra[ind++] = M->bloktab + (int)P->ma[i][j];

  /*** Compute rind ***/
  tmp  = (int *)malloc(sizeof(int)* (M->bloknbr));
  DM->sharenbr = 0;
  ind = 0;
  tkl = 0;
  for(i=0;i<P->n;i++)
    if(block_psetindex[i+1]-block_psetindex[i] > 1)
      for(k=0;k<P->nnzrow[i];k++)
	{
	  j = P->ja[i][k];
	  /** Compute the key of block(i,j) **/
	  intersect_key(block_psetindex[i+1]-block_psetindex[i], block_psetindex[j+1]-block_psetindex[j], 
			block_pset+block_psetindex[i], block_pset+block_psetindex[j], key, &kl);
	  if(kl>1) /** This block is shared by several processors **/
	    {

	      /*{
		dim_t w;
 		fprintfv(5, stderr, "BLOCK %d %d \n", DBL->loc2glob_blocknum[i], DBL->loc2glob_blocknum[j]);
		fprintfv(5, stderr, "key i = ");
		for(w = block_psetindex[i]; w < block_psetindex[i+1];w++)
		fprintfv(5, stderr, "%d ", block_pset[w]);
		fprintfv(5, stderr, "\n");
		fprintfv(5, stderr, "key j = ");
		for(w = block_psetindex[j]; w < block_psetindex[j+1];w++)
		fprintfv(5, stderr, "%d ", block_pset[w]);
		fprintfv(5, stderr, "\n");
		}*/


	      tmp[(int)P->ma[i][k]] = DM->sharenbr; /** Need this tmp
						       vector to compute cind **/
	      tkl += kl;
	      DM->rind[ind] = DM->sharenbr++;
	      
	    }
	  else
	    {
#ifdef DEBUG_M
	      assert(kl == 1);
	      assert(key[0] == DBL->proc_id);
#endif
	      tmp[(int)P->ma[i][k]] = -1;
	      DM->rind[ind] = -1;
	    }
	  
	  ind++;
	}
    else
      for(k=0;k<P->nnzrow[i];k++)
	{
#ifdef DEBUG_M
	  assert(block_psetindex[i+1]-block_psetindex[i] == 1);
	  if(block_pset[block_psetindex[i]] != DBL->proc_id)
	     fprintfv(5, stderr, "proc_id = %d != %d \n", DBL->proc_id, block_pset[block_psetindex[i]]);
  	  assert(block_pset[block_psetindex[i]] == DBL->proc_id);
#endif
	  tmp[(int)P->ma[i][k]] = -1;
	  DM->rind[ind] = -1;
	  /*fprintfv(5, stderr, "Block %d %d is not shared ind = %d \n", i,P->ja[i][k] ,DM->rind[ind]);*/
	  ind++;
	}

  CS_Transpose(1, P, P->n);
  M->cia = (int *)malloc(sizeof(int)* (BL->nblock+1));
  M->cja = (int *)malloc(sizeof(int)* (M->bloknbr));
  DM->cind = (int *)malloc(sizeof(int)* (M->bloknbr));
  cs2csr(P, M->cia, M->cja, NULL);
  M->ca = (csptr *)malloc(sizeof(csptr)* (M->bloknbr));
  ind = 0;
  for(i=0;i<P->n;i++)
    for(j=0;j<P->nnzrow[i];j++)
      M->ca[ind++] = M->bloktab + (int)P->ma[i][j];

  /*** Compute cind ***/
  ind = 0;
  for(i=M->tlj;i<=M->brj;i++)
    for(k=0;k<P->nnzrow[i];k++)
      DM->cind[ind++] = tmp[(int)P->ma[i][k]]; 

  free(tmp);
  cleanCS(P);
  free(P);

  /*** Compute the processor set of each block ***/
  DM->pset_index = (int *)malloc(sizeof(int)* (DM->sharenbr+1));
  if(tkl != 0)
    DM->pset       = (int *)malloc(sizeof(int)* tkl);
  else DM->pset = NULL;
  
  ind = 0;
  tkl = 0;
  /*dumpcsr(stdout, NULL, M->rja, M->ria, M->bri-M->tli+1); */
  for(i=tli;i<=bri;i++)
    {
      if(block_psetindex[i+1]-block_psetindex[i] == 1)
	continue;
      
      for(k=M->ria[i];k<M->ria[i+1];k++)
	{
	  j = M->rja[k];

	  intersect_key(block_psetindex[i+1]-block_psetindex[i], block_psetindex[j+1]-block_psetindex[j], 
			block_pset+block_psetindex[i],
			block_pset+block_psetindex[j], key, &kl);

	  if(kl>1)
	    {
#ifdef DEBUG_M
	      assert(DM->rind[k] == ind);
#endif
	      DM->pset_index[ind] = tkl;
	      memcpy(DM->pset+tkl, key, sizeof(int)*kl);
	      tkl+=kl;
	      ind++;
	    }
	}
    }
  DM->pset_index[ind] = tkl;
  free(key);

  /****************************************************************/
  /* Set the leader (TEMPORAIRE POUR TEST==>LOAD BALANCE A FAIRE) */  
  /****************************************************************/
  DM->rlead = (int *)malloc(sizeof(int)*M->bloknbr);
  for(i=tli;i<=bri;i++)
    for(k=M->ria[i];k<M->ria[i+1];k++)
      {
	if(DM->rind[k] == -1)
	  DM->rlead[k] = DBL->proc_id;
	else
	  DM->rlead[k] = DM->pset[ DM->pset_index[DM->rind[k]]];
      }

  /*fprintfv(5, stderr, "\n");
    for(i=tli;i<=bri;i++)
    for(k=M->ria[i];k<M->ria[i+1];k++)
    fprintfv(5, stderr, "Proc %d B(%d %d) rlead = %d rind = %d \n", DM->proc_id, i, M->rja[k], DM->rlead[k], DM->rind[k]); 
    fprintfv(5, stderr, "\n");*/

  DM->clead = (int *)malloc(sizeof(int)*M->bloknbr);
#ifdef OLD_M
  for(j=tlj;j<=brj;j++)  /** Compute the maximum number of processors that share a bloc **/
    DM->maxproc = 0;
  for(i=M->tli;i<=M->bri;i++)
    for(k=M->ria[i];k<M->ria[i+1];k++)
      {
	ind = DM->rind[k];
	if(ind >= 0 && DM->pset_index[ind+1]-DM->pset_index[ind] > DM->maxproc)
	  DM->maxproc = DM->pset_index[ind+1]-DM->pset_index[ind];
      }
#endif
  /*fprintfv(5, stderr, "Proc %d Sharenbr = %d \n", DM->proc_id, DM->sharenbr);*/


  /**/
#else

  DBMatrix* MO = &(Orig->M);
  PhidalMatrix* Copy = &(DM->M);

  PhidalDistrMatrix_Setup(MO->tli, MO->tlj, MO->bri,  MO->brj, UPLO, "N", locally_nbr, DM, DBL);
  
  assert(Copy->dim1 == MO->dim1);
  assert(Copy->dim1 == MO->dim1);
  assert(Copy->virtual == 0);
  
  Copy->symmetric = MO->symmetric;
  Copy->csc = MO->symmetric;
#endif
}

void DBDistrMatrix2PhidalDistrMatrix(UDBDistrMatrix* LU, PhidalDistrMatrix* Copy, int_t locally_nbr, PhidalDistrHID *DBL, flag_t inarow, flag_t unitdiag) {

  DBDistrMatrix* L = LU->L;
  /* PhidalDistrMatrix_Init_fromDBDistr(L, Copy, "L", locally_nbr, DBL); */

  UDBMatrix ARG; ARG.L = &L->M;
  DBMatrix2PhidalMatrix(&ARG, &Copy->M, locally_nbr, &DBL->LHID, inarow, unitdiag);

  /* void DBDistrMatrix_Clean(DBDistrMatrix* a) */
  {
    DBDistrMatrix* a = L;
    if(a->clead != NULL)
      free(a->clead);
    if(a->cind != NULL)
      free(a->cind);
  
    if(a->rlead != NULL)
      free(a->rlead);
    if(a->rind != NULL)
      free(a->rind);
  
    if(a->pset_index != NULL)
      free(a->pset_index);
    if(a->pset != NULL)
      free(a->pset);
  
    a->proc_id=0;
    a->nproc=0;
  
    a->clead=NULL;
    a->cind=NULL; 
  
    a->rlead=NULL;
    a->rind=NULL; 
  
    a->pset_index=NULL;
    a->pset=NULL;
  
    a->sharenbr=0;
  }
  /* void DBDistrMatrix_Clean(DBDistrMatrix* a) */
}

void DBDistrMatrix2PhidalDistrMatrix_Unsym(UDBDistrMatrix* LU, PhidalDistrMatrix* Copy, int_t locally_nbr, PhidalDistrHID *DBL, flag_t inarow) {

  DBDistrMatrix* L = LU->L;
  DBDistrMatrix* U = LU->U;

  /* PhidalDistrMatrix_Init_fromDBDistr(L, Copy, "N", locally_nbr, DBL); */

  UDBMatrix ARG; ARG.L = &L->M;  ARG.U = &U->M;
 
  DBMatrix2PhidalMatrix_Unsym(&ARG, &Copy->M, locally_nbr, &DBL->LHID, inarow);

  /* void DBDistrMatrix_Clean(DBDistrMatrix* a) */
  {
    DBDistrMatrix* a = L;
    if(a->clead != NULL)
      free(a->clead);
    if(a->cind != NULL)
      free(a->cind);
  
    if(a->rlead != NULL)
      free(a->rlead);
    if(a->rind != NULL)
      free(a->rind);
  
    if(a->pset_index != NULL)
      free(a->pset_index);
    if(a->pset != NULL)
      free(a->pset);
  
    a->proc_id=0;
    a->nproc=0;
  
    a->clead=NULL;
    a->cind=NULL; 
  
    a->rlead=NULL;
    a->rind=NULL; 
  
    a->pset_index=NULL;
    a->pset=NULL;
  
    a->sharenbr=0;
  }
  /* void DBDistrMatrix_Clean(DBDistrMatrix* a) */

  /* void DBDistrMatrix_Clean(DBDistrMatrix* a) */
  {
    DBDistrMatrix* a = U;
/*     if(a->clead != NULL) */
/*       free(a->clead); */
/*     if(a->cind != NULL) */
/*       free(a->cind); */
  
/*     if(a->rlead != NULL) */
/*       free(a->rlead); */
/*     if(a->rind != NULL) */
/*       free(a->rind); */
  
/*     if(a->pset_index != NULL) */
/*       free(a->pset_index); */
/*     if(a->pset != NULL) */
/*       free(a->pset); */
  
    a->proc_id=0;
    a->nproc=0;
  
    a->clead=NULL;
    a->cind=NULL; 
  
    a->rlead=NULL;
    a->rind=NULL; 
  
    a->pset_index=NULL;
    a->pset=NULL;
  
    a->sharenbr=0;
  }
  /* void DBDistrMatrix_Clean(DBDistrMatrix* a) */

}


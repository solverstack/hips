/* @authors J. GAIDAMOUR, P. HENON */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <assert.h>
#include <math.h>

#include "phidal_parallel.h"
#include "db_parallel.h"


/** Local function **/




void CellDBDistr_ListUpdate(CellDBDistr *celltab, int num,  int n,  dim_t *ja)
{
  dim_t i;
  CellDBDistr *c;

  for(i=0;i<n;i++)
    {
      c = celltab+ja[i];

      while(c->nnz > 0)
	if( *(c->ja) < num /* || c->ma[0]->nnzr == 0 */ ) 
	  {
	    c->nnz--;
	    c->ja++;
	    c->pind++;
	    c->ma++;
	  }
	else
	  break;
    }
}


void CellDBDistr_ListCopy(CellDBDistr *src, CellDBDistr *dest,  int n,  dim_t *ja)
{
  dim_t i;
  for(i=0;i<n;i++)
    dest[ja[i]] = src[ja[i]];
}



void CellDBDistr_GetCSList(int num, CellDBDistr *celltab, int n,  dim_t *ja, VSolverMatrix **ma, int *nnb, VSolverMatrix* *csrtab1, VSolverMatrix* *csrtab2)
{
  int i, ind;
  CellDBDistr *c;

  ind = 0;
   for(i=0;i<n;i++)
    {
      c = celltab+ja[i];
      
      while(c->nnz > 0)
	if( *(c->ja) < num /*  || c->ma[0]->nnzr == 0 */)
	  {
	    c->nnz--;
	    c->ja++;
	    c->pind++;
	    c->ma++;
	  }
	else
	  break;
      
      if( *(c->ja) == num)
	{
	  csrtab1[ind] = c->ma[0];
	  csrtab2[ind] = ma[i];
	  ind++;
	}
    }
   *nnb = ind;
}

void CellDBDistr_IntersectList(mpi_t proc_id, CellDBDistr *celltab, INTL *pset_index, mpi_t *pset,
			       int ncol,  int *colja, int *colpind, VSolverMatrix* *colma, 
			       int nrow, int *rowja,  
			       int *nnztab, VSolverMatrix* *listA, VSolverMatrix* *listB)
{

  int jj, ii, i, j;
  mpi_t *ps1, *ps2;
  int k1, k2, l1, l2;
  int leader;

  VSolverMatrix* cmat;
  int nnz;
  dim_t *ja;
  INTL *pind;
  VSolverMatrix* *ma;
  VSolverMatrix* **listAtab, * **listBtab;

  if(nrow == 0 || ncol == 0)
    return;

  /***** Find the size of all the rowlist *****/
  listAtab = (VSolverMatrix* **)malloc(sizeof(VSolverMatrix* *)*nrow);
  listBtab = (VSolverMatrix* **)malloc(sizeof(VSolverMatrix* *)*nrow);

  for(i=0;i<nrow;i++)
    {
      listAtab[i] = listA+i*ncol;
      listBtab[i] = listB+i*ncol;
    }

  bzero(nnztab, sizeof(int)*nrow);

  for(jj=0;jj<ncol;jj++)
    {
      cmat = colma[jj];
      j = colja[jj];
      nnz = celltab[j].nnz;
      ja = celltab[j].ja;
      pind = celltab[j].pind;
      ma = celltab[j].ma;
      k1 = colpind[jj];
      
      ii = 0;
      i = 0;
      while(ii<nnz && i <nrow)
	{
	  if(ja[ii] < rowja[i])
	    {
	      ii++;
	      continue;
	    }

	  if(ja[ii] > rowja[i])
	    {
	      i++;
	      continue;
	    }

	  

	  /** ja[ii] == ja[i] **/
	  if(/* ma[ii]->nnzr > 0 */1) 
	    {
	      k2 = pind[ii];
	      
	      if(k1 != -1 && k2 != -1)
		{
		  ps1 = pset + pset_index[k1];
		  ps2 = pset + pset_index[k2];
		  l1 = pset_index[k1+1] - pset_index[k1];
		  l2 = pset_index[k2+1] - pset_index[k2];

		  leader = choose_leader(l1, ps1, l2, ps2);
		}
	      else
		leader = proc_id;

	      if(proc_id == leader)/** @@ OIMBE load balance temporaire **/ 
		{
		  listAtab[i][nnztab[i]] = ma[ii];
		  listBtab[i][nnztab[i]] = cmat;
		  nnztab[i]++;
		}
	    }
	  ii++;
	  i++;
	}
    }

  free(listAtab);
  free(listBtab);
}


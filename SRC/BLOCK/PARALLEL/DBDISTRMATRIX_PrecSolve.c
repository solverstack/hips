/* @authors J. GAIDAMOUR */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>

#define PARALLEL

#include "block.h"
#include "phidal_parallel.h"
#include "db_parallel.h"

INTS DBMATRIX_DistrSolve(PhidalDistrMatrix *A, DBDistrPrec *P, PhidalDistrHID *DBL, PhidalOptions *option, COEF *rhs, COEF * x, dim_t *itertab, REAL *resnormtab)
{
  int iter=-1;
  /** There is no flexible PCG so the first level is always
      fgmres **/
  if(option->krylov_method != 1 || (option->forwardlev > 0 && option->schur_method != 0))
    {
      CHECK_RETURN(HIPS_DistrFgmresd_PH_DB(option->verbose, option->tol, option->itmax, A, P, DBL, option, rhs, x, option->fd, itertab, resnormtab));
    }
  else
    {
      /** assert(0) **/
      iter = HIPS_DistrPCG_PH_DB(option->verbose, option->tol, option->itmax, A, P, DBL, option, rhs, x, option->fd, itertab, resnormtab); 
    }

  if(option->verbose > 0 && DBL->proc_id == 0)
    fprintfv(5, stdout, "Number of outer iterations %d \n", iter);

  return HIPS_SUCCESS;
}

INTS DBDistrPrec_SolveForward(REAL tol, int_t itmax, DBDistrPrec *P, COEF *x, COEF *b, PhidalDistrHID *DBL, PhidalOptions *option, dim_t *itertab, REAL *resnormtab);

INTS DBDISTRMATRIX_PrecSolve(REAL tol, DBDistrPrec *P, COEF *x, COEF *b, PhidalDistrHID *DBL, PhidalOptions *option, dim_t *itertab, REAL *resnormtab)
{
  /* PhidalHID *BL = &DBL->LHID; */

  DBDistrMatrix *L, *U;
  int_t itmax;
  dim_t i;

  L = PREC_L_BLOCK(P);
  U = PREC_U_BLOCK(P);

  if(P->forwardlev == 0)
    {

      /*TODO : tmp !*/
      DBMatrixCommVec_Setup(1, L, DBL);
      /****/

      /* if (L->M.alloc == ONE) /\* todo .. first lev. *\/ */
      //	DBMatrix_Lsolv(1, &L->M, x, b, &DBL->LHID);
      /*       else */
      assert(S(L)->alloc == BLK);
      DBDistrMatrix_Lsolv(1, L, x, b, DBL);
      
      /* if(P->symmetric == 1) */
      /*  DBDistrMatrix_Dsolv(L, x); */
      if(P->symmetric == 1) {
	assert(L->extDiag != NULL);
	for(i=0;i<S(L)->dim1;i++)
	  x[i] /= L->extDiag[i];
      }
      /* DEBUG */
      /* {  */
      /* 	  COEF* D = (COEF*)malloc(sizeof(COEF)*50); */
      /* 	  DBMatrix_GetDiag(S(L), D); */
      /* 	  print_vec("PrecSolve Diag =", D, 4); */
      /* 	  free(D); */
      /* 	} */
      
      /* TODO : tmp ! */
      DBDistrMatrix_Transpose(U);
      DBMatrixCommVec_Setup(2, U, DBL);
      DBDistrMatrix_Transpose(U);
      /* **** */
      
      /* if (L->M.alloc == ONE) /\* todo .. first lev. *\/ */
      /*  DBMatrix_Usolv(P->symmetric, &U->M, x, x, &DBL->LHID); */
      /* else */
      assert(S(U)->alloc == BLK);
	DBDistrMatrix_Usolv(P->symmetric, U, x, x, DBL);
	
    } else {

    /*assert(0);*/ /* todo */
    itmax = option->itmaxforw;
    for(i=P->forwardlev; i<option->forwardlev;i++)
      itmax = (int)(itmax*option->itforwrat);
    
    CHECK_RETURN(DBDistrPrec_SolveForward(tol, itmax, P, x, b, DBL, option, itertab, resnormtab));
  }
  return HIPS_SUCCESS;
}
/* TODO : pivoting, /\** permute back x **\/ */
INTS DBDistrPrec_SolveForward(REAL tol, int_t itmax, DBDistrPrec *P, COEF *x, COEF *b, PhidalDistrHID *DBL, PhidalOptions *option, dim_t *itertab, REAL *resnormtab)
{
  /* int UN=1; */
  int dim;

  DBDistrMatrix *L, *U;
  DBDistrMatrix *SL, *SU;
  PhidalDistrMatrix *E, *F;
  DBMatrix *LL, *UU;
  COEF *y;
  PhidalHID *BL;
  REAL bn, rn;
  REAL tolrat;

  dim_t g;

  L = PREC_L_BLOCK(P);
  E = PREC_E(P);
  U = PREC_U_BLOCK(P);
  F = PREC_F(P); 
/*   SL = PREC_SL_BLOCK(P); */
/*   SU = PREC_SU_BLOCK(P); */

  LL = &L->M;
  UU = &U->M;

  BL = &DBL->LHID;

  y = (COEF *)malloc(sizeof(COEF)*P->dim);
  memcpy(y, b, sizeof(COEF)*P->dim);
  
  /*** Restriction operation ***/

#ifdef WITH_PASTIX
  if(option->use_pastix == 1)
      Solve_with_pastix(&L->M, b, x);
  else
#endif // WITH_PASTIX
    {
      DBMatrix_Lsolv(1, &L->M, x, b, BL);

      if(P->symmetric == 1) 
          DBMatrix_Dsolv(&L->M, x);

      DBMatrix_Usolv(P->symmetric, &U->M, x, x, BL);
    }
  
  PhidalDistrMatrix_MatVecSub(0, E, DBL, x, y+L->M.dim1);

#ifdef DEBUG_M
  /*if ((SL != 0) && (P->schur_method == 1))
    assert(SL->M.dim1 == BL->n - BL->block_index[BL->block_levelindex[P->levelnum]]-L->M.dim1);*/
#endif
    
  dim = P->dim - L->M.dim1;
/*   tolrat = sqrt(dist_ddot(DBL->proc_id, b, b, L->M.tli, L->M.bri, DBL))/sqrt(dist_ddot(DBL->proc_id, y+L->M.dim1, y+L->M.dim1, L->M.bri+1, BL->nblock-1, DBL)); */

  bn = dist_ddot(DBL->proc_id, b, b, LL->tli, LL->bri, DBL);
  rn = dist_ddot(DBL->proc_id, y+LL->dim1, y+LL->dim1, LL->bri+1, BL->nblock-1, DBL);
  tolrat = sqrt(bn)/sqrt(rn);

  if(option->schur_method == 0 || itmax <= 1)
    {
      if (P->nextprec != NULL)
	{
	  CHECK_RETURN(DBDISTRMATRIX_PrecSolve(tol*tolrat, P->nextprec, x+L->M.dim1, y+L->M.dim1, DBL, option, itertab, resnormtab));
	}
      else
	{
	  CHECK_RETURN(PHIDAL_DistrPrecSolve(tol*tolrat, P->phidalPrec, x+L->M.dim1, y+L->M.dim1, DBL, option, itertab, resnormtab));
	}
    }
  else
    {
      flag_t verbose = (option->verbose >= 4)?2:0;
      option->krylov_method = 0; /** POUR DEBUG **/
      if (option->krylov_method != 1)
	{
	  if (P->nextprec != NULL)
	    {
	      if(P->schur_method == 2)
		{
		  CHECK_RETURN(HIPS_DistrFgmresd_DB(verbose, tol*tolrat, itmax, P->nextprec, DBL,
						    option, y+L->M.dim1, x+L->M.dim1, stdout, itertab, resnormtab));
		}
	      else /* (P->schur_method == 1) */
		{
		  printf("ASSERT1\n");
		  assert(0);
/* 		  CHECK_RETURN(HIPS_DistrFgmresd_DB_DB(verbose, tol*tolrat, itmax, SL, SU, P->nextprec, DBL, */
/* 						       option, y+L->M.dim1, x+L->M.dim1, stdout, itertab, resnormtab)); */
		}
	    } else {
	      if(P->schur_method == 2) {
		/*assert(0);*/ /* a tester */
		CHECK_RETURN(HIPS_DistrFgmresd_DB_PH(verbose, tol*tolrat, itmax, P, P->phidalPrec, DBL,
						     option, y+L->M.dim1, x+L->M.dim1, stdout, itertab, resnormtab));
		
	      } else /* (P->schur_method == 1) */ {
		
		CHECK_RETURN(HIPS_DistrFgmresd_PH_PH(verbose, tol*tolrat, itmax, P->phidalS, P->phidalPrec, DBL, 
						     option, y+L->M.dim1, x+L->M.dim1, stdout, itertab, resnormtab));
		
	      }
	    }
	}
      else
	{
	  if (P->nextprec != NULL)
	    {
	      assert(0);
/* 	       if(P->schur_method == 2) */
/* 	      	    iter = HIPS_DistrPCG_DB(verbose, tol*tolrat, itmax, P->nextprec, BL, */
/* 	      				   option, y+L->dim1, x+L->dim1, stdout, itertab); */
/* 	      	  else /\* (P->schur_method == 1) *\/ */
/* 	      	    iter = HIPS_DistrPCG_DB_DB(verbose, tol*tolrat, itmax, SL, SU, P->nextprec, BL, */
/* 	      				      option, y+L->dim1, x+L->dim1, stdout, itertab); */
	    } else {
	      
	      if(P->schur_method == 2) {
		/*assert(0);*/ /* a tester */
		CHECK_RETURN(HIPS_DistrPCG_DB_PH(verbose, tol*tolrat, itmax, P, P->phidalPrec, DBL,
						 option, y+L->M.dim1, x+L->M.dim1, stdout, itertab, resnormtab));
		
	      } else /* (P->schur_method == 1) */ {
		
		CHECK_RETURN(HIPS_DistrPCG_PH_PH(verbose, tol*tolrat, itmax, P->phidalS, P->phidalPrec, DBL, 
						 option, y+L->M.dim1, x+L->M.dim1, stdout, itertab, resnormtab));
		
	      }
	    }
	}
      
      if (/* (option->verbose >= 3) && */ (DBL->proc_id == 0))
	{
	  for(g=0;g<P->levelnum+1;g++)
	    fprintfv(1, stdout, "    ");
	  if (itertab != NULL)
	    fprintfv(1, stdout, "Level %ld : inner iterations = %ld \n", (long)(P->levelnum+1),(long)itertab[0]);
	}

    }

  /*** Prolongation operation ***/
  PhidalDistrMatrix_MatVecSub(0, F, DBL, x+L->M.dim1, y); 

#ifdef WITH_PASTIX
  if(option->use_pastix == 1)
    Solve_with_pastix(&L->M, y, x);
  else
#endif // WITH_PASTIX
    {
      DBMatrix_Lsolv(1, &L->M, x, y, BL);
      
      if(P->symmetric == 1) 
	DBMatrix_Dsolv(&L->M, x);
      
      DBMatrix_Usolv(P->symmetric, &U->M, x, x, BL);
    }
  
  free(y);
  return HIPS_SUCCESS;
}

/* @authors J. GAIDAMOUR */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>

#include "block.h"
#include "phidal_parallel.h"
#include "db_parallel.h"

void DBDistrMatrix_VirtualCpy(DBDistrMatrix *M, DBDistrMatrix *Cpy) {
  memcpy(Cpy, M, sizeof(DBDistrMatrix));
  Cpy->M.virtual = VIRTUAL;
}

void DBDistrMatrix_Transpose(DBDistrMatrix *DM)
{
   /********************************************************************/
  /* This function transpose a phidal matrix                          */
  /* exemple: if M is an upper phidal matrix in CSR block matrices    */
  /*  then it becomes a lower phidal matrix in CSC block matrices     */
  /********************************************************************/
  mpi_t *tmp1;
  INTL   *tmp2;
 
  tmp1 = DM->clead;
  DM->clead = DM->rlead;
  DM->rlead = tmp1;

  tmp2 = DM->cind;
  DM->cind = DM->rind;
  DM->rind = tmp2;

  /** The principle is really simple: the block rows of the phidal matrix 
      becomes the column of the transpose phidal matrix **/
  DBMatrix_Transpose(&DM->M);
  
}



/* void DBDistrMatrix_CleanNonLocalBLock(DBDistrMatrix *DM) */
/* { */
/*   /\*************************************************\/ */
/*   /\* This function deallocates all the non local   *\/ */
/*   /\*     submatrices that are not local to the     *\/ */
/*   /\* processor owner of the DBDistrMatrix      *\/ */
/*   /\*************************************************\/ */
/*   dim_t i, j; */
/*   DBMatrix *M; */
/*   M = &(DM->M); */

/*   for(i=M->tli;i<M->bri;i++) */
/*     for(j=M->ria[i];j<M->ria[i+1];j++) */
/*       if(DM->rlead[j] != DM->proc_id) */
/* 	reinitCS(M->ra[j]); */
/* } */



void DBDistrMatrix_Copy2(DBDistrMatrix* M, DBDistrMatrix* Copy) {

  DBMatrix_Copy2(&M->M, &Copy->M);
    
}


void DBDistrMatrix_ColMult(REAL *b, DBDistrMatrix *DA, PhidalDistrHID *DBL) {

  DBMatrix* D = &DA->M;
/*   PhidalHID* BL = &DBL->LHID; */

  SolverMatrix* solvmtx=NULL;
  SymbolMatrix* symbmtx;
  int k,i, i0, ii, iiglobal=-1, iiglobalt=-1, j;
  int hdim, stride;
  dim_t p;
  COEF* ptr;
  
  int UN = 1;

  if (D->alloc == ONE) {
    solvmtx = &D->a[0];    
  }

  /* parcours par blocs colonnes */
  iiglobal=0;
  for(i0=0,i=D->tlj; i<=D->brj; i0++, i++) {

    symbmtx = &D->ca[D->cia[i]]->symbmtx;
    
    if (D->alloc == CBLK) {
      solvmtx = &D->a[i0];
    }

    if(D->alloc == BLK) {
      solvmtx = D->ca[D->cia[i]]->solvmtx;
    }

    if (D->alloc == RBLK) {
      solvmtx = &D->a[D->cja[D->cia[i]]-D->tli];
    }
    assert(solvmtx == D->ca[D->cia[i]]->solvmtx);
    solvmtx = D->ca[D->cia[i]]->solvmtx;

    for(k=0; k<symbmtx->cblknbr; k++) {  

      for(j=D->cia[i]; j<D->cia[i+1]; j++) {
	
/* 	if ((DA->clead[D->cia[i]] == DA->proc_id)  */
/* 	  ||(D->cja[j] == i)) { */

	symbmtx = &D->ca[j]->symbmtx;
	  if (symbmtx->bcblktab != NULL) {
	    assert(symbmtx->bcblktab != NULL);

	  if (D->alloc == RBLK) {
	    solvmtx = &D->a[D->cja[j]-D->tli];
	  }
	  
	  if(D->alloc == BLK) {
	    solvmtx = D->ca[j]->solvmtx;
	  }
	  
	  assert(solvmtx == D->ca[j]->solvmtx);
	  solvmtx = D->ca[j]->solvmtx;
	  
	  /*******/
	  p      = symbmtx->bcblktab[k].fbloknum;        /* triangular block */
	  if (p > symbmtx->bcblktab[k].lbloknum) continue;
	  ptr    = solvmtx->coeftab + solvmtx->bloktab[p].coefind;
	  hdim   = symbmtx->hdim[k];
	  stride = symbmtx->stride[k];
	  
	  for (ii=symbmtx->ccblktab[k].fcolnum, iiglobalt=iiglobal; ii<=symbmtx->ccblktab[k].lcolnum; ii++, iiglobalt++) { 
	    BLAS_SCAL2(hdim, b[iiglobalt], ptr, UN);
	    
	    ptr += stride;
	  }
	  /*******/
	  
	}
      }
      iiglobal=iiglobalt;
      
    }
  }
  
}

void DBDistrMatrix_RowMult(REAL *b, DBDistrMatrix *DA, PhidalDistrHID *DBL) {

  DBMatrix* D = &DA->M;
/*   PhidalHID* BL = &DBL->LHID; */

  SolverMatrix* solvmtx=NULL;
  SymbolMatrix* symbmtx=NULL;
  int k,i, i0, /* iiglobal, iiglobalt, */ j;
  int hdim, stride;
  dim_t p;
  COEF* ptr;
  COEF* lc;

  dim_t m;
  int cdim;
  int init;

  if (D->alloc == ONE) {
    solvmtx = &D->a[0];    
  }
  
  symbmtx = &D->ca[D->cia[D->tlj]]->symbmtx;

  init= symbmtx->bloktab[symbmtx->bcblktab[0].fbloknum].frownum; /*todo*/

  /* parcours par blocs colonnes */
  for(i0=0,i=D->tlj; i<=D->brj; i0++, i++) {

    symbmtx = &D->ca[D->cia[i]]->symbmtx;
    
    if (D->alloc == CBLK) {
      solvmtx = &D->a[i0];
    }

    if(D->alloc == BLK) {
      solvmtx = D->ca[D->cia[i]]->solvmtx;
    }

    if (D->alloc == RBLK) {
      solvmtx = &D->a[D->cja[D->cia[i]]-D->tli];
    }
    assert(solvmtx == D->ca[D->cia[i]]->solvmtx);
    solvmtx = D->ca[D->cia[i]]->solvmtx;

    for(k=0; k<symbmtx->cblknbr; k++) {  /* inverser boucle k et j ? */

      cdim   = symbmtx->ccblktab[k].lcolnum - symbmtx->ccblktab[k].fcolnum + 1;

      for(j=D->cia[i]; j<D->cia[i+1]; j++) {

/* 	if ((DA->clead[D->cia[i]] == DA->proc_id)  */
/* 	  ||(D->cja[j] == i)) { */
	  
	  symbmtx = &D->ca[j]->symbmtx;
	  if (symbmtx->bcblktab != NULL) {
	  assert(symbmtx->bcblktab != NULL);
	  
	  if (D->alloc == RBLK) {
	    solvmtx = &D->a[D->cja[j]-D->tli];
	  }
	  
	  if(D->alloc == BLK) {
	    solvmtx = D->ca[j]->solvmtx;
	  }
	  
	  assert(solvmtx == D->ca[j]->solvmtx);
	  solvmtx = D->ca[j]->solvmtx;
	  
	  /*******/
	  p      = symbmtx->bcblktab[k].fbloknum;        /* triangular block */
	  if (p > symbmtx->bcblktab[k].lbloknum) continue;
	  ptr    = solvmtx->coeftab + solvmtx->bloktab[p].coefind;
	  hdim   = symbmtx->hdim[k];
	  stride = symbmtx->stride[k];
	  
	  /* assert tj vrai sinon : verifier sym/unsym dans inputs (en RBLK) */
	  /* printfv(5, "cdim = %d, %d \n", cdim, symbmtx->ccblktab[k].lcolnum - symbmtx->ccblktab[k].fcolnum + 1); */
	  /* check sherman3 LL, 1, 2 : = pb TRANSPOSE */	
	  assert(cdim == symbmtx->ccblktab[k].lcolnum - symbmtx->ccblktab[k].fcolnum + 1);
	  
	  for(;p<=symbmtx->bcblktab[k].lbloknum; p++)
	    {
	      /** Calcul du pointeur dans coeftab vers le debut des coefficients du bloc p **/ 
	      lc = solvmtx->coeftab + solvmtx->bloktab[p].coefind;
	      
	      for(m=symbmtx->bloktab[p].frownum;m<=symbmtx->bloktab[p].lrownum;m++)
		{
		  /* printfv(5, "i=%d, k=%d m=%d: BLAS_SCAL cdim=%d b=%e %d stride=%d\n", i,k,m,cdim, b[m-init], solvmtx->bloktab[p].coefind, /\* lc, *\/ stride); */
		  BLAS_SCAL2(cdim, b[m-init], lc, stride);
		  lc++;
		}
	    }
	  /*******/
	  
	} /* else { symbmtx = &D->ca[j]->symbmtx; printfv(5, "symbmtx = %p\n", symbmtx->bcblktab); assert(symbmtx->bcblktab == NULL); } */
		
      }
    }
  }

}


void DBDistrMatrix_DiagMult(REAL *vec, DBDistrMatrix *A, PhidalDistrHID *DBL) {
  DBMatrix_DiagMult(vec, &A->M, &DBL->LHID);
}



void PhidalDistrMatrix_InitScale(PhidalDistrMatrix* DA, REAL* scaletab, REAL* iscaletab, PhidalDistrHID* DBL) {
  dim_t i;
  PhidalMatrix* A = &DA->M;

  /*** Symmetric scale ***/
  /*PhidalDistrMatrix_ColNorm2(DA, DBL, iscaletab);*/
  /** Temporaire **/
  PhidalDistrMatrix_RowNorm2(DA, DBL, iscaletab);
  
  /** Take the square root of the norm such that a term 
      aij = aij / (sqrt(rowi)*sqrt(colj)) **/
  for(i=0;i<A->dim1;i++)
	iscaletab[i] = sqrt(iscaletab[i]); 
  for(i=0;i<A->dim1;i++)
    scaletab[i] = 1.0/iscaletab[i];
  
  
}

void PhidalDistrMatrix_Scale(PhidalDistrMatrix* DA, REAL* scaletab, PhidalDistrHID* DBL) {
  PhidalDistrMatrix_RowMult2(scaletab, DA, DBL);
  PhidalDistrMatrix_ColMult2(scaletab, DA, DBL);
}

void PhidalDistrMatrix_InitScale_Unsym(PhidalDistrMatrix* DA, 
			      REAL* scalerow, REAL* iscalerow, 
			      REAL* scalecol, REAL* iscalecol, PhidalOptions* option, PhidalDistrHID* DBL) {
  dim_t i;
  PhidalMatrix* A = &DA->M;
  fprintfv(5, stdout, "Scaling\n");
  
  
  /*if(option->scale == 1)*/
  if(0)
    {
      /*** Unsymetric scale ***/
      PhidalDistrMatrix_UnsymScale(option->scalenbr, DA, DBL, scalerow, scalecol, iscalerow, iscalecol);
    }
  else 
    {
      
      /*** Symmetric scale ***/
      /*PhidalDistrMatrix_ColNorm2(DA, DBL, iscaletab);*/
      /** Temporaire **/
      PhidalDistrMatrix_ColNorm2(DA, DBL, iscalecol);
      PhidalDistrMatrix_RowNorm2(DA, DBL, iscalerow);
      
      /** Take the square root of the norm such that a term 
	  aij = aij / (sqrt(rowi)*sqrt(colj)) **/
      for(i=0;i<A->dim1;i++)
	iscalecol[i] = sqrt(iscalecol[i]);
      for(i=0;i<A->dim1;i++)
	iscalerow[i] = sqrt(iscalerow[i]); 
      for(i=0;i<A->dim1;i++)
	scalecol[i] = 1.0/iscalecol[i];
      for(i=0;i<A->dim1;i++)
	scalerow[i] = 1.0/iscalerow[i];
      
      
      
      PhidalDistrMatrix_ColMult2(scalecol, DA, DBL);
      PhidalDistrMatrix_RowMult2(scalerow, DA, DBL);
      
      /**** Compute the droptab ******/
      /**** MUST BE THERE : AFTER THE SCALING ****/
      /**** BE CAREFUL USE A DROPTAB ONLY IF A SCALING IS DONE !! ***/
      /*if(droptab != NULL)
	PhidalDistrMatrix_ColNorm2(DA, DBL, droptab);*/
    }
  /** Temporaire **/
/*   if(droptab != NULL) */
/*     PhidalDistrMatrix_RowNorm2(DA, DBL, droptab); */
  
}


void PhidalDistrMatrix_Scale_Unsym(PhidalDistrMatrix* DA, REAL* scalerow, REAL* scalecol, PhidalDistrHID* DBL) {
  PhidalDistrMatrix_ColMult2(scalecol, DA, DBL);
  PhidalDistrMatrix_RowMult2(scalerow, DA, DBL);	
}

/* @authors J. GAIDAMOUR */

#include <stdio.h>
#include <stdlib.h>
#include <string.h> /* todo : bzero is deprecated */

#define PARALLEL
#include "block.h"
#include "db_parallel.h"

void DBDistrMatrix_Init(DBDistrMatrix *a)
{
#ifdef DEBUG_M
  assert(a!=NULL);
#endif

  a->proc_id=0;
  a->nproc=0;
  
  a->clead=NULL;
  a->cind=NULL; 
  
  a->rlead=NULL;
  a->rind=NULL; 
  
  a->pset_index=NULL;
  a->pset=NULL;
  
  a->sharenbr=0;
  a->maxproc = 0;
  DBMatrix_Init(&a->M);

  a->extDiag=NULL;

  PhidalCommVec_Init(&a->commvec);
}

void DBDistrMatrix_Clean(DBDistrMatrix* a)
{
#ifdef DEBUG_M
  assert(a!=NULL);
#endif
 
  if(a->clead != NULL)
    free(a->clead);
  if(a->cind != NULL)
    free(a->cind);
  
  if(a->rlead != NULL)
    free(a->rlead);
  if(a->rind != NULL)
    free(a->rind);
  
  if(a->pset_index != NULL)
    free(a->pset_index);
  if(a->pset != NULL)
    free(a->pset);
  
  if(a->extDiag != NULL) free(a->extDiag);

  a->proc_id=0;
  a->nproc=0;
  
  a->clead=NULL;
  a->cind=NULL; 
  
  a->rlead=NULL;
  a->rind=NULL; 
  
  a->pset_index=NULL;
  a->pset=NULL;
  
  a->sharenbr=0;

  a->extDiag = NULL;

  DBMatrix_Clean(&a->M);
  /* init commvec */

}


void DBDistrPrec_Init(DBDistrPrec *P)
{
  P->symmetric = 0;
  P->dim = 0;
  P->levelnum = 0;
  P->forwardlev = 0;
  P->LU = NULL;
  P->EF = NULL;
  P->EF_DB = NULL;
  P->S = NULL;
  P->B = NULL;

  P->prevprec = NULL;
  P->nextprec = NULL;

  P->schur_method = 0;
  /*   P->pivoting = 0; */
  /*   P->permtab = NULL; */
  
  /*   P->tol_schur = 1; */

  P->phidalS = NULL;
  P->phidalPrec = NULL;

/*   p->info = NULL; */
}


void DBDistrPrec_Print(DBDistrPrec *P)
{

  printfd("Prec : symmetric : %d dim : %d levelnum : %d forwardlev : %d schur_method : %d\n",   
         P->symmetric,
         P->dim,
         P->levelnum,
         P->forwardlev,
         P->schur_method);

  if(PREC_L_BLOCK(P) == NULL)
    printfd("Prec: L==NULL\n");
  if(PREC_U_BLOCK(P) == NULL)
    printfd("Prec: U==NULL\n");
/*   if(P->D == NULL) */
/*     printfd("Prec: D==NULL\n"); */
  if(PREC_E(P) == NULL)
    printfd("Prec: E==NULL\n");
  if(PREC_F(P) == NULL)
    printfd("Prec: F==NULL\n");
  if(PREC_EDB(P) == NULL)
    printfd("Prec: E==NULL\n");
  if(PREC_FDB(P) == NULL)
    printfd("Prec: F==NULL\n");

  if(PREC_SL_BLOCK(P) == NULL)
    printfd("Prec: SL==NULL\n");

  if(PREC_SU_BLOCK(P) == NULL)
    printfd("Prec: SU==NULL\n");

  if(P->B == NULL)
    printfd("Prec: B==NULL\n");

  if(P->prevprec == NULL)
    printfd("Prec: prevprec==NULL\n");

  if(P->nextprec == NULL) {
    printfd("Prec: nextprec==NULL\n");
  } else {
    printfd("Prec: nextprec==: (!=NULL)\n");
    DBDistrPrec_Print(P->nextprec);
  }
}



void DBDistrPrec_Clean(DBDistrPrec *P)
{
#ifdef DEBUG_M
  if(P->forwardlev > 0) {
    assert(P->nextprec != NULL || (P->phidalPrec != NULL));
  } else
    assert(P->nextprec == NULL);
#endif

  if(P->forwardlev  > 0)
    {
      if(P->nextprec != NULL)
        {
          if(PREC_SL_BLOCK(P) == PREC_L_BLOCK(P->nextprec)) PREC_SL_BLOCK(P)=NULL; /* FIX, ne pas desallouer 2 fois */
          if(PREC_SU_BLOCK(P) == PREC_U_BLOCK(P->nextprec)) PREC_SU_BLOCK(P)=NULL; /* FIX, ne pas desallouer 2 fois */
          DBDistrPrec_Clean(P->nextprec);
          free(P->nextprec);
        }
      
      if (P->EF != NULL) {
	if(PREC_E(P) != NULL)
	  {
	    PhidalCommVec_Clean(&PREC_E(P)->commvec);
	    PhidalDistrMatrix_Clean(PREC_E(P));
	    free(PREC_E(P));
	  }
      
	if(PREC_F(P) != NULL)
	  {
	    PhidalCommVec_Clean(&PREC_F(P)->commvec);
	    PhidalDistrMatrix_Clean(PREC_F(P));
	    free(PREC_F(P));
	  }

	free(P->EF);

      }

      if (P->EF_DB != NULL) {
	if(PREC_EDB(P) != NULL)
	  {
	    DBDistrMatrix_Clean(PREC_EDB(P));
	    free(PREC_EDB(P));
	  }
      
	if(PREC_FDB(P) != NULL)
	  {
	    DBDistrMatrix_Clean(PREC_FDB(P));
	    free(PREC_FDB(P));
	  }

	free(P->EF_DB);
      }
  
      if (P->S != NULL) {
	if(PREC_SL_BLOCK(P) != NULL)
	  {
	    if(P->schur_method == 1) {/* sinon NULL, dc test redondant */
	      DBDistrMatrix_Clean(PREC_SL_BLOCK(P));
	      free(PREC_SL_BLOCK(P));
	    }
	  }
	
	if((PREC_SU_BLOCK(P) != NULL) && (P->symmetric == 0))
	  {
	    
	    if(P->schur_method == 1) { /* sinon NULL, dc test redondant */
	      DBMatrix_Clean(&PREC_SU_BLOCK(P)->M); /*TODO : FIXME*/  /* DBDistrMatrix_Clean(PREC_SU_BLOCK(P)); */
	    free(PREC_SU_BLOCK(P));
	    }
	  }
	free(P->S);
      }

      if (P->B != NULL)  {
	if(PREC_B(P) != NULL)
	  {
	    PhidalDistrMatrix_Clean(PREC_B(P));
	    free(PREC_B(P));
	  }
	
	free(P->B);
      }

    }
  
  if (P->LU != NULL) {
    if(PREC_L_BLOCK(P) != NULL)
      {
	DBDistrMatrix_Clean(PREC_L_BLOCK(P));
	free(PREC_L_BLOCK(P));
      }
    
    if((PREC_U_BLOCK(P) != NULL) && (P->symmetric == 0))
      {
	
	/* FIX
	   a->clead=NULL;
	   a->cind=NULL; 
	   
	   a->rlead=NULL;
	   a->rind=NULL; 
	   
	   a->pset_index=NULL;
	   a->pset=NULL;
	*/
	
	DBMatrix_Clean(&PREC_U_BLOCK(P)->M);
	/* DBDistrMatrix_Clean(PREC_U_BLOCK(P));/\*TODO : FIXME*\/ */
	
	free(PREC_U_BLOCK(P));
      }
    
    free(P->LU);
  }

/*   if(P->pivoting == 1) */
/*     free(P->permtab); */

  P->symmetric = 0; /* pourquoi ne pas appeller INIT ? */
  P->dim = 0;
  P->levelnum = 0;
  P->forwardlev = 0;
  P->schur_method = 0;

  if(P->phidalS != NULL)
    {
      /* P->phidalS->cia = NULL; */
/*       P->phidalS->cja = NULL; */
/*       P->phidalS->ria = NULL; */
/*       P->phidalS->rja = NULL; */
      
      PhidalDistrMatrix_Clean(P->phidalS);
      free(P->phidalS);
    }

  if(P->phidalPrec != NULL)
    {
      PhidalDistrPrec_Clean(P->phidalPrec);
      free(P->phidalPrec);
    }


/*   P->pivoting = 0; */

/*   P->tol_schur = 1; */
}


#define DBMatrixCommVec_Init PhidalCommVec_Init
#define DBMatrixCommVec_Clean PhidalCommVec_Clean


void DBBlockComm_Init(DBBlockComm *b, int srcnbr)
{
#ifdef DEBUG_M
  assert(srcnbr > 0);
#endif
  b->posted = 0;
  b->rqnbr = srcnbr;
  b->proctab = (mpi_t *)malloc(sizeof(mpi_t)*srcnbr);
  b->irqtab = (MPI_Request *)malloc(sizeof(MPI_Request)*srcnbr);
  b->crqtab = (MPI_Request *)malloc(sizeof(MPI_Request)*srcnbr);
/*   b->ibufftab = (int **)malloc(sizeof(int *)*srcnbr); */
  b->cbufftab = (COEF **)malloc(sizeof(COEF *)*srcnbr);
}


void DBBlockComm_Clean(DBBlockComm *b)
{
  free(b->proctab);
  free(b->irqtab);
  free(b->crqtab);
/*   free(b->ibufftab); */
  free(b->cbufftab);
}

void DBMatrixFactComm_Init(DBMatrixFactComm *FC)
{
  FC->bcomtab = NULL;
  FC->proc_id = -1;
  FC->bcomnbr = 0;
  FC->maxproc = 0;
  FC->cstab = NULL;
/*   FC->tmpj = NULL; */
/*   FC->tmpa = NULL; */
/*   FC->jrev = NULL; */
  FC->comm_memratio = 1.0;
}

void DBMatrixFactComm_Clean(DBMatrixFactComm *FC)
{
  dim_t k;
/*   VSolverMatrix* t; */

  for(k=0;k<FC->bcomnbr;k++)
    DBBlockComm_Clean(FC->bcomtab+k);
  if(FC->bcomnbr > 0)
    free(FC->bcomtab);

  /** Set the matrices used to unpack to null **/      
  for(k=0;k<FC->maxproc;k++)
    {
/* #warning TODO */
/*       t = FC->cstab+k; */
/*       bzero(t->nnzrow, sizeof(int)*t->n); */
/*       bzero(t->ja, sizeof(int *)*t->n); */
/*       bzero(t->ma, sizeof(COEF *)*t->n); */
/*       CS_SetNonZeroRow(FC->cstab+k); */
/*       cleanCS(FC->cstab+k); */
    }
  if(FC->cstab) free(FC->cstab);
/*   if(FC->jrev)  free(FC->jrev); */
/*   if(FC->tmpj)  free(FC->tmpj); */
/*   if(FC->tmpa)  free(FC->tmpa); */

  FC->bcomnbr = 0;
}



/* @authors J. GAIDAMOUR */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>

#define PARALLEL

#include "db_parallel.h"
#include "phidal_parallel.h"

/* TODO : factoriser cela */


/******************************************/
/******************************************/
/***  communication routines for vector ***/
/***      (matrix-vector operations)    ***/
/******************************************/
/******************************************/



void DBMatrixCommVec_Setup(flag_t job, DBDistrMatrix *DM, PhidalDistrHID *DBL)
{
  /***************************************************/
  /* This function sets up the commicator for matrix */
  /* vector operations                               */
  /* Depending on job the communicator enables :     */
  /* job = 0: matrix-vector multiplication           */
  /* job = 1: forward triangular solve               */
  /* job = 2: backward triangular solve              */
  /***************************************************/

  /**** OIMBE @@@@ : faire un schema ou on compte exactement les 
	contribution non nul ******/

  int i, j, k, veclen, offseti;
  DBMatrixCommVec *commvec;
  DBMatrix *M;
  PhidalHID *BL;
  BL = &DBL->LHID;
  commvec = &(DM->commvec);
  M = &(DM->M);

#ifdef DEBUG_M
  /*   assert(commvec->init == 0); */
#endif

  /*PhidalCommVec_Init(commvec);*/

  commvec->init = 1;
  commvec->recv_rqtab  = (MPI_Request **)malloc(sizeof(MPI_Request *) * (M->bri-M->tli+1));
  commvec->recv_buffer = (COEF **)     malloc(sizeof(COEF *) * (M->bri-M->tli+1) );

  commvec->out_ctrb = (int *)malloc(sizeof(int) * (M->bri-M->tli+1));
  commvec->out_proctab = (int **)malloc(sizeof(int *) * (M->bri-M->tli+1));

  commvec->veclen = (int *)malloc(sizeof(int) * (M->bri-M->tli+1));
  commvec->out_cnt = (int *)malloc(sizeof(int) * (M->bri-M->tli+1));
  commvec->loc_ctrb =   (int *)malloc(sizeof(int) * (M->bri-M->tli+1));
  commvec->loc_cnt =   (int *)malloc(sizeof(int) * (M->bri-M->tli+1));
  commvec->t = (COEF *)malloc(sizeof(COEF) * M->dim1);

  if(DBL->adjpnbr>0)
    commvec->status = (MPI_Status *)malloc(sizeof(MPI_Status)*(DBL->adjpnbr));
  else
    commvec->status = NULL;

  commvec->rqnbr = M->bri-M->tli+1;

  bzero(commvec->veclen, sizeof(int)*(M->bri-M->tli+1));

  switch(job){
  case 0:
    /***** Set the counters ****/
    if(M->symmetric == 0)
      {
	/** Count  the number of contributions in each row **/
	for(i=M->tli;i<= M->bri;i++)
	  {
	    commvec->loc_ctrb[i-M->tli] = 0;
	    for(j=M->ria[i];j<M->ria[i+1];j++)
#ifndef POUR_DEBUG
	      if(/* M->ra[j]->nnzr > 0 &&  */DM->rlead[j] == DM->proc_id)
#else
	      if(DM->rlead[j] == DM->proc_id)
#endif
		commvec->loc_ctrb[i-M->tli]++;
	  }
      }
    else
      {
	/** Count  the number of contributions in each row **/
	for(i=M->tli;i<= M->bri;i++)
	  {
	    commvec->loc_ctrb[i-M->tli] = 0;
	    for(j=M->ria[i];j<M->ria[i+1];j++)
#ifndef POUR_DEBUG
	      if(/* M->ra[j]->nnzr > 0 &&  */DM->rlead[j] == DM->proc_id)
#else
	      if(DM->rlead[j] == DM->proc_id)
#endif
		commvec->loc_ctrb[i-M->tli]++;
	    
	    /** Symmetric part **/
	    for(j=M->cia[i];j<M->cia[i+1];j++)
#ifndef POUR_DEBUG
	      if(/* M->ca[j]->nnzr > 0 && */ DM->clead[j] == DM->proc_id)
#else
	      if(DM->clead[j] == DM->proc_id)
#endif
		commvec->loc_ctrb[i-M->tli]++;
	  }
      }

    /***********************************************************************/
    /* out_ctrb est modifie plus bas pour faire une compte plus precis des */
    /* contributions reels en ne tenant pas compte des blocs nulles        */
    /* ce bout de code ne sert donc pas si le reste est active             */
    /***********************************************************************/
    for(i=M->tli;i<= M->bri;i++)
      {
	if(DBL->row_leader[i] == DM->proc_id)
	  commvec->out_ctrb[i-M->tli] = DBL->block_psetindex[i+1] - DBL->block_psetindex[i]-1;
	else
	  commvec->out_ctrb[i-M->tli] = -1;
      }

    break;
  case 1:
  case 2:

    /** Count the number of contributions in each row **/
    for(i=M->tli;i<= M->bri;i++)
      {
	commvec->loc_ctrb[i-M->tli] = 0;
	for(j=M->ria[i];j<M->ria[i+1];j++)
#ifndef POUR_DEBUG
	  if(/* M->ra[j]->nnzr > 0 &&  */DM->rlead[j] == DM->proc_id)
#else
	  if(DM->rlead[j] == DM->proc_id) 
#endif
	    commvec->loc_ctrb[i-M->tli]++;
      }
    
    break;
  default:
    fprintfd(stderr, "Invalid Argument 'job' in  PhidalCommVec_Setup \n");
    exit(-1);

  }

  /**********************************************************/
  /** Set the loc_ctrb=0 to -1  in order to differentiate  **/
  /** them from the complete ctrb                          **/
  /**********************************************************/
  /** NO NEED OF THAT IN THEORY : BUT "on est jamais trop prudent...
      pour eviter des bugs" ***/
  for(i=M->tli;i<= M->bri;i++)
    if(commvec->loc_ctrb[i-M->tli]==0)
      commvec->loc_ctrb[i-M->tli] = -1;
  

  
  /***************************************************************/
  /** Compute the list of row where the local proc contributes  **/
  /** for each adjacent processor                               **/
  /** We also compute the exact number of contribution from     **/
  /** adjacent processors (we eliminate the false contributions **/
  /** induced by null blocks                                    **/
  /***************************************************************/
  {
    int *proc2adjp, *adjp_blocknbrLOC=NULL, *adjp_blocknbrEXT=NULL;
    int **blistLOC=NULL, **blistEXT=NULL;
    MPI_Request *rqtab=NULL;
      
    proc2adjp = (int *)malloc(sizeof(int)*DBL->nproc);
    bzero(proc2adjp, sizeof(int)*DBL->nproc);
    for(i=0;i<DBL->adjpnbr;i++)
      proc2adjp[ DBL->adjptab[i] ] = i;
      
    /*** Compute the list of connector owned by each adjacent processor ***/
    if(DBL->adjpnbr>0)
      adjp_blocknbrLOC = (int *)malloc(sizeof(int)*DBL->adjpnbr);
    bzero(adjp_blocknbrLOC, sizeof(int)*DBL->adjpnbr);
    for(i=M->tli;i<= M->bri;i++)
      if(commvec->loc_ctrb[i-M->tli] > 0 && DBL->row_leader[i] != DBL->proc_id)
	adjp_blocknbrLOC[ proc2adjp[DBL->row_leader[i]]]++;
      
    if(DBL->adjpnbr>0)
      blistLOC = (int **)malloc(sizeof(int *)*DBL->adjpnbr);
    for(i=0;i<DBL->adjpnbr;i++)
      if(adjp_blocknbrLOC[i] > 0)
	blistLOC[i] = (int *)malloc(sizeof(int)*adjp_blocknbrLOC[i]);
      else
	blistLOC[i] = NULL;

    if(DBL->adjpnbr>0)
      bzero(adjp_blocknbrLOC, sizeof(int)*DBL->adjpnbr);
    for(i=M->tli;i<= M->bri;i++)
      if(commvec->loc_ctrb[i-M->tli] > 0 && DBL->row_leader[i] != DBL->proc_id)
	{
	  k = proc2adjp[DBL->row_leader[i]];
	  blistLOC[k][adjp_blocknbrLOC[k]++] = DBL->loc2glob_blocknum[i];
	}
      
    /*fprintfd(stderr, "Proc %d Local block nbr: \n", DBL->proc_id);
    for(i=0;i<DBL->adjpnbr;i++)
    fprintfd(stderr, "to proc %d = %d \n", DBL->adjptab[i], adjp_blocknbrLOC[i]);*/

    if(DBL->adjpnbr>0)
      {
	adjp_blocknbrEXT = (int *)malloc(sizeof(int)*DBL->adjpnbr);
	memcpy(adjp_blocknbrEXT, adjp_blocknbrLOC, sizeof(int)*DBL->adjpnbr);
      }

   
    for(i=0;i<DBL->adjpnbr;i++)
      {
#ifdef TRACE_COM
	fprintfd(stderr, "MPI_Sendrecv i= %d proc %d with proc %d \n", i, DBL->proc_id, DBL->adjptab[i]);
#endif
	assert(DBL->proc_id !=  DBL->adjptab[i]);
	assert(commvec->status != NULL);
	MPI_Sendrecv_replace(adjp_blocknbrEXT+i, 1, COMM_INT, DBL->adjptab[i], CTRB_TAG, DBL->adjptab[i], CTRB_TAG, 
			     MPI_COMM_WORLD, commvec->status);
      }


    /*fprintfd(stderr, "Proc %d EXT block nbr: \n", DBL->proc_id);
    for(i=0;i<DBL->adjpnbr;i++)
    fprintfd(stderr, "from proc %d = %d \n", DBL->adjptab[i], adjp_blocknbrEXT[i]);*/
    
    if(DBL->adjpnbr>0)
      {
	blistEXT = (int **)malloc(sizeof(int *)*DBL->adjpnbr);
	rqtab = (MPI_Request *)malloc(sizeof(MPI_Request)*DBL->adjpnbr);
      }
    for(i=0;i<DBL->adjpnbr;i++)
      if(adjp_blocknbrEXT[i] > 0)
	{
	  blistEXT[i] = (int *)malloc(sizeof(int)*adjp_blocknbrEXT[i]);
	  assert(DBL->proc_id !=  DBL->adjptab[i]);
	  MPI_Irecv(blistEXT[i], adjp_blocknbrEXT[i], COMM_INT, DBL->adjptab[i], LEAD_TAG, MPI_COMM_WORLD, rqtab+i);
	}
      
    for(i=0;i<DBL->adjpnbr;i++)
      if(adjp_blocknbrLOC[i] > 0)
	{
	  assert(DBL->proc_id !=  DBL->adjptab[i]);
	  MPI_Send(blistLOC[i], adjp_blocknbrLOC[i], COMM_INT, DBL->adjptab[i], LEAD_TAG, MPI_COMM_WORLD);
	}
    /** Receive the list of contribution from adjacent processors **/
    /** and convert them in local numbering **/
    for(i=0;i<DBL->adjpnbr;i++)
      if(adjp_blocknbrEXT[i] > 0)
	{
	  assert(commvec->status!=NULL);
	  MPI_Wait(rqtab+i, commvec->status);
	  for(k=0;k<adjp_blocknbrEXT[i];k++)
	    blistEXT[i][k] = DBL->glob2loc_blocknum[blistEXT[i][k]];
	}


    /*** Compute the processor tab  ***/
    bzero(commvec->out_ctrb, sizeof(int)*(M->bri-M->tli+1));
    for(i=0;i<DBL->adjpnbr;i++)
      for(k=0;k<adjp_blocknbrEXT[i];k++)
	commvec->out_ctrb[blistEXT[i][k]-M->tli]++;
      
      
   
    for(i=0;i<M->bri-M->tli+1;i++)
      if(commvec->out_ctrb[i]>0)
	commvec->out_proctab[i] = (int *)malloc(sizeof(int)*commvec->out_ctrb[i]);

    bzero(commvec->out_ctrb, sizeof(int)*(M->bri-M->tli+1));
    for(i=0;i<DBL->adjpnbr;i++)
      for(k=0;k<adjp_blocknbrEXT[i];k++)
	{
	  j = blistEXT[i][k] - M->tli;
	  commvec->out_proctab[j][commvec->out_ctrb[j]++] = DBL->adjptab[i];
	}

    for(i=M->tli;i<= M->bri;i++)
      if(DBL->row_leader[i] != DM->proc_id)
	commvec->out_ctrb[i-M->tli] = -1;
      
    free(proc2adjp);
    for(i=0;i<DBL->adjpnbr;i++)
      if(adjp_blocknbrLOC[i]>0)
	free(blistLOC[i]);
    for(i=0;i<DBL->adjpnbr;i++)
      if(adjp_blocknbrEXT[i]>0)
	free(blistEXT[i]);
    if(DBL->adjpnbr>0)
      {
	free(blistLOC);
	free(blistEXT);
	free(adjp_blocknbrLOC);
	free(adjp_blocknbrEXT);
	free(rqtab);
      }
    

#ifdef POUR_DEBUG
    fprintfd(stderr, "DEBUG OUT_CTRB \n");
    for(i=M->tli;i<= M->bri;i++)
      {
	if(DBL->row_leader[i] == DM->proc_id)
	  commvec->out_ctrb[i-M->tli] = DBL->block_psetindex[i+1] - DBL->block_psetindex[i]-1;
	else
	  commvec->out_ctrb[i-M->tli] = -1;
      }
    
    for(i=M->tli;i<= M->bri;i++)
      {
	if(commvec->out_ctrb[i-M->tli] > 0)
	  {
	    commvec->out_proctab[i-M->tli] = (int *)malloc(sizeof(int)*commvec->out_ctrb[i-M->tli]);
	    k =0;
	    for(j=DBL->block_psetindex[i];j<DBL->block_psetindex[i+1];j++)
	      if(DBL->block_pset[j] != DBL->proc_id)
		commvec->out_proctab[i-M->tli][k++] = DBL->block_pset[j];
	  }
      }

#endif

    /*-----------------------------*/
    /* Set persistent MPI requests */
    /*-----------------------------*/
    offseti = BL->block_index[M->tli];
    for(i=M->tli;i<= M->bri;i++)
      {
	veclen = DBL->LHID.block_index[i+1]-DBL->LHID.block_index[i];
	commvec->veclen[i-M->tli] = veclen;

	if(commvec->out_ctrb[i-M->tli] > 0)
	  {
	    /** Set the receive request for contribution vectors from
		non-leader processors **/
	    
	    commvec->recv_buffer[i-M->tli] = (COEF *)malloc(sizeof(COEF)*veclen*commvec->out_ctrb[i-M->tli]);
	    commvec->recv_rqtab[i-M->tli] = (MPI_Request *)malloc(sizeof(MPI_Request)*commvec->out_ctrb[i-M->tli]);
	    
	    for(j=0;j<commvec->out_ctrb[i-M->tli];j++)
	      {
		assert(commvec->out_proctab[i-M->tli][j] != DBL->proc_id);
		MPI_Recv_init(commvec->recv_buffer[i-M->tli]+j*veclen, CC(veclen), MPI_COEF_TYPE, commvec->out_proctab[i-M->tli][j], 
			      DBL->loc2glob_blocknum[i], MPI_COMM_WORLD, commvec->recv_rqtab[i-M->tli]+j);
	      }
	  }
	else
	  {

	    
	    if(commvec->out_ctrb[i-M->tli] == -1)
	      {
#ifdef DEBUG_M
		assert(DBL->row_leader[i] != DM->proc_id);
#endif
		commvec->recv_rqtab[i-M->tli] = (MPI_Request *)malloc(sizeof(MPI_Request));

		/** Init the receive for the final vector value from
		    the leader **/
		MPI_Recv_init(commvec->t+BL->block_index[i]-offseti, CC(veclen), MPI_COEF_TYPE, DBL->row_leader[i], 
			      DBL->loc2glob_blocknum[i], MPI_COMM_WORLD, commvec->recv_rqtab[i-M->tli]);

	      }
	    else
	      commvec->recv_rqtab[i-M->tli] = NULL;
	  }
      }

  }
  
}

void DBMatrixCommVec_PrecSetup(DBDistrPrec *P, PhidalDistrHID *DBL)
{

  assert(0);

#ifdef DEBUG_M
  if(P->forwardlev > 0)
    assert(P->nextprec != NULL);
  else
    assert(P->nextprec == NULL);
#endif

  if(P->forwardlev  > 0)
    {
#ifdef DEBUG_M
      assert(PREC_E(P) != NULL);
      assert(PREC_F(P) != NULL);
#endif
      /*if(P->levelnum>0)*/
	{
	  PhidalCommVec_Setup(0, PREC_E(P), &PREC_E(P)->commvec, DBL);
          PhidalCommVec_Setup(0, PREC_F(P), &PREC_F(P)->commvec, DBL);
        }
      if(P->schur_method == 1)
        DBMatrixCommVec_Setup(0, PREC_SL_BLOCK(P), DBL);

      if (P->symmetric == 0) assert(0);
      
      if(P->schur_method == 2)
	PhidalCommVec_Setup(0, PREC_B(P), &PREC_B(P)->commvec, DBL);
      
      if(P->nextprec != NULL)
        DBMatrixCommVec_PrecSetup(P->nextprec, DBL);
    }
#ifdef DEBUG_M
  assert(PREC_L_BLOCK(P) != NULL);
  assert(PREC_U_BLOCK(P) != NULL);
#endif 
  /*if(P->levelnum>0)*/
      {
        DBMatrixCommVec_Setup(1, PREC_L_BLOCK(P), DBL);
        DBMatrixCommVec_Setup(2, PREC_U_BLOCK(P), DBL); 
      }
}

void DBMatrixCommVec_ReceiveVecAdd(int num, COEF *y, DBMatrixCommVec *commvec)
{
  dim_t i, j;
  int veclen;
  COEF *ptr;
#ifdef DEBUG_M
  assert(commvec->init == 1);
  assert(commvec->out_ctrb[num]>0);
#endif

  if(MPI_Waitall(commvec->out_ctrb[num], commvec->recv_rqtab[num], commvec->status) != MPI_SUCCESS)
    {
      fprintfd(stderr, "Error in DBMatrixCommVec_ReceiveVecAdd for request %d \n" ,num);
      assert(0);
    }
  
  /** add the contribution in y **/
  ptr = commvec->recv_buffer[num];
  veclen = commvec->veclen[num];
#ifdef DEBUG_M
  assert(veclen > 0);
#endif
  for(i=0;i<commvec->out_ctrb[num];i++)
    {
      for(j=0;j<veclen;j++)
	y[j] += ptr[j];
      ptr += veclen;
    }

}

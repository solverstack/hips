/* @authors J. GAIDAMOUR */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>

#define PARALLEL

#include "block.h"
#include "db_parallel.h"
#include "phidal_parallel.h"

/* todo : uniquement sÃ©quentiel, juste pour le prototype */

void DBDistrPrec_SchurProd(DBDistrPrec *P, PhidalDistrHID *DBL, COEF *x, COEF *y)
{
  /**************************************************************/
  /* This function compute the implicit Schur Product           */
  /* y = S.x    where S is not stored                           */
  /* eg y = B.x - E.U^-1.L^1.F.x (for unsymmetric)              */
  /**************************************************************/

  /*** @@OIMBE PROBLEM SI PERMUTATION ????? B n'EST PAS PERMUTE ****/

  PhidalHID* BL = &DBL->LHID;

  COEF *t;
  
  DBMatrix *L, *U;
  PhidalDistrMatrix *E, *F;
  PhidalDistrMatrix *B;

#ifdef DEBUG_M
  assert(P->schur_method == 2);
#endif

  E = PREC_E(P);
  F = PREC_F(P);
  L = S(PREC_L_BLOCK(P));
  U = S(PREC_U_BLOCK(P));
  B = PREC_B(P);

  t = (COEF *)malloc(sizeof(COEF)*L->dim1);

  PhidalDistrMatrix_MatVec(0, B, DBL, x, y);
  PhidalDistrMatrix_MatVec(0, F, DBL, x, t);

  DBMatrix_Lsolv(1, L, t, t, BL);
  if(P->symmetric == 1)
    DBMatrix_Dsolv(L, t);
  DBMatrix_Usolv(P->symmetric, U, t, t, BL);

  PhidalDistrMatrix_MatVecSub(0, E, DBL, t, y);

  free(t);
}

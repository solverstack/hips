/* @noheader */

/*----------------------------------------------------------------------
 * preconditioning functions :
 * ilu0 : construct local ilu0 preconditioner. the original local
 *        matrix is stored in CSR format, the preconditioner is
 *        stored in ILUfac format
 * ilut : construct local ilut preconditioner. the original local
 *        matrix is stored in CSR format, the preconditioner is
 *        stored in ILUfac format
 * iluk : construct local iluk preconditioner. the original local
 *        matrix is stored in CSR format, the preconditioner is
 *        stored in ILUfac format
 *--------------------------------------------------------------------*/

#include "../../INCLUDE/psparslib.h"
#define min(a, b) (a) > (b) ? (b) : (a)
int ilu0(DistMatrix dm, PreCon precon, PrePar prepar)
{
  /*--------------------------------------------------------------------
   * construct local ilu0 preconditioner
   *
   * Zhongze Li, June. 2001
   *--------------------------------------------------------------------
   * ON ENTRY
   *===========
   *     dm   =  distributed local matrix handler
   * prepar   =  parameters for constructing preconditioner
   *
   * ON RETURN
   *===========
   * precon   =  preconditioner which is stored in MSR format
   *===========
   * IMPORTANT
   *===========
   * it is assumed that the the elements in the input matrix are stored 
   * in such a way that in each row the lower part comes first and
   * then the upper part. To get the correct ILU factorization, it is
   * also necessary to have the elements of L sorted by increasing
   * column number. It may therefore be necessary to sort the 
   * elements of a, ja, ia prior to calling ilu0. This can be
   * achieved by transposing the matrix twice using csrcsc.
   *------------------------------------------------------------------*/
  DistMatrixCsr dmc;
  int n, *ja, *ia, *rowj, *rowjj, *iw, i, ii, jj, j, ierr=0;
  int jcol, jrow, jw, lenl, lenu, nnz; 
  REAL *a, *rowm, *rowmm;
  chrono_t t1;
  ILUfac ilufac;

  dmc = (DistMatrixCsr)dm;
 
  /* retrieve the size n from DistMatrix structure */
  n = GetValOfDim(dm);

  /* retrieve CSR structure from DistMatrix structure */
  a = dmc->A->smsf->ma;
  ja = dmc->A->smsf->ja;
  ia = dmc->A->smsf->ia;

  /* extract the pointer to precontioner structure */
  ilufac = (ILUfac)precon->precd;
  /* initialize work vector to -1 */
  PARMS_malloc(iw, n, int);
  for(i = 0; i < n; i++) {
    iw[i] = -1;
  }
  /* main loop */
  ierr = 0;
  for(ii = 0; ii < n; ii++) {
    /* calculating the number of entries in L and U */
    lenl = 0;
    lenu = 0;
    for(j = ia[ii]; j < ia[ii+1]; j++) {
      jcol = ja[j];
      if(jcol < ii) {
	++lenl;
      }
      else {
	++lenu;
      }
    }
    ilufac->L->nnzrow[ii] = lenl;
    if(lenl > 0) {
      PARMS_malloc(ilufac->L->ja[ii], lenl, int);
      PARMS_malloc(ilufac->L->ma[ii], lenl, REAL);
    }
    ilufac->U->nnzrow[ii] = lenu;
    PARMS_malloc(ilufac->U->ja[ii], lenu, int);
    PARMS_malloc(ilufac->U->ma[ii], lenu, REAL);

    lenl = 0;
    lenu = 0;
    /* copy row ii of a, ja, ia into L and U part */
    for(j = ia[ii]; j < ia[ii+1]; j++) {
      jcol = ja[j];
      if(jcol < ii) {
	ilufac->L->ja[ii][lenl] = jcol;
	ilufac->L->ma[ii][lenl] = a[j];
	iw[jcol] = lenl;
	++lenl;
      }
      else {
	ilufac->U->ja[ii][lenu] = jcol;
	ilufac->U->ma[ii][lenu] = a[j];
	iw[jcol] = ii + lenu;
	++lenu;
      }
    }

    /* exit if diagonal element is reached. */
    rowj = ilufac->L->ja[ii];
    rowm = ilufac->L->ma[ii];
    for(j = 0; j < lenl; j++) {
      jrow = rowj[j];
      rowjj = ilufac->U->ja[jrow];
      rowmm = ilufac->U->ma[jrow];
      nnz = ilufac->U->nnzrow[jrow];
      rowm[j] *= rowmm[0];
      t1 = rowm[j];
      /* perform linear combination */
      for(jj = 1; jj < nnz; jj++) {
	jw = iw[rowjj[jj]];
	if(jw != -1) {
	  if(jw < ii) {
	    rowm[jw] -= t1*rowmm[jj];
	  }
	  else {
	    ilufac->U->ma[ii][jw-ii] -= t1*rowmm[jj];
	  }
	}
      }
    }
    /* invert and store diagonal element. */
    t1 = ilufac->U->ma[ii][0];
    if(t1 == 0.0) {
      ierr = ii+1;
      break;
    }
    ilufac->U->ma[ii][0] = 1.0/t1;
    /* reset pointer iw to -1 */
    rowj = ilufac->L->ja[ii];
    for(i = 0; i < lenl; i++) {
      iw[rowj[i]] = -1;
    }
    rowj = ilufac->U->ja[ii];
    for(i = 0; i < lenu; i++) {
      iw[rowj[i]] = -1;
    }
  }
  
  free(iw);
  return ierr;
}

int iluk(DistMatrix dm, PreCon precon, PrePar prepar)
{
  /*--------------------------------------------------------------------
   * construct local iluk preconditioner
   * 
   * Zhongze Li, June. 2001
   *--------------------------------------------------------------------
   * ON ENTRY
   *===========
   *     dm   =  distributed local matrix handler
   * prepar   =  parameters for constructing preconditioner
   *
   * ON RETURN
   *===========
   * precon   =  preconditioner which is stored in MSR format
   *  ierr    =  integer. Error message with the following meaning.
   *             ierr  = 0    --> successful return.
   *             ierr  > 0   --> zero pivot encountered at step
   *                              number ierr.
   *             ierr  = -1   --> Error. input matrix may be wrong.
   *                              (The elimination process has
   *                              generated a row in L or U whose
   *                              length is greater than n)
   *             ierr  = -2   --> Illegal value for lfil.
   *             ierr  = -3   --> zero row encountered in A or U
   *------------------------------------------------------------------*/
  DistMatrixCsr dmc;
  ILUfac ilufac;
  int n,n2,nnz,lenl,lenu,jcol,lfil,*ja,*ia,**levs,*lev,*jw,*rowj;
  int jlev,i,j,ii,jj,jpos,jrow,k,ierr=0; 
  REAL *w,*a,*rowm,t,s,fact;

  /* extract parameters from dm and prepar */
  dmc = (DistMatrixCsr)dm;
  ilufac = (ILUfac)precon->precd;
  n = dmc->comm->nloc;
  nnz = GetValOfNnz(dm);
  lfil = prepar->lfil[0];

  if(lfil < 0) {
    ierr = -2;
    return ierr;
  }

  /* retrieve CSR structure from DistMatrix structure */
  a = dmc->A->smsf->ma;
  ja = dmc->A->smsf->ja;
  ia = dmc->A->smsf->ia;

  /* malloc memory for working arrays levs, w and jw */
  PARMS_malloc(levs, n, int *);
  PARMS_malloc(w, n, REAL);
  PARMS_malloc(jw, 3*n, int);

  /* main loop */
  n2 = n << 1;
  for(i = n; i < n2; i++) {
    jw[i] = -1;
    jw[n+i] = 0;
  }
  for(ii = 0; ii < n; ii++) {
    lenl = 0;
    lenu = 1;
    jw[ii] = ii;
    w[ii] = 0.0;
    jw[n+ii] = ii;
    for(j = ia[ii]; j < ia[ii+1]; j++) {
      jcol = ja[j];
      t = a[j];
      if(t == 0.0) continue;
      if(jcol < ii) {
	jw[lenl] = jcol;
	w[lenl] = t;
	jw[n2+lenl] = 0;
	jw[n+jcol] = lenl;
	++lenl;
      }
      else if(jcol == ii) {
	w[ii] = t;
	jw[n2+ii] = 0;
      }
      else {
	jpos = ii + lenu;
	jw[jpos] = jcol;
	w[jpos] = t;
	jw[n2+jpos] = 0;
	jw[n+jcol] = jpos;
	++lenu;
      }
    }
    /* eliminate previous rows */
    for(jj = 0; jj < lenl; jj++) {
      /* in order to do the elimination in the correct order we must
	 select the smallest column index among jw[k], k=jj,...lenl */
      jrow = jw[jj];
      k = jj;
      /* determine smallest column index */
      for(j = jj+1; j < lenl; j++) {
	if(jw[j] < jrow) {
	  jrow = jw[j];
	  k = j;
	}
      }
      if(k != jj) {
	/* exchange in jw */
	j = jw[jj];
	jw[jj] = jw[k];
	jw[k] = j;
	/* exchange in jw(n+ (pointers/ nonzero indicator). */
	jw[n+jrow] = jj;
	jw[n+j] = k;
	/* exchange in jw(n2 + (levels) */
	j = jw[n2+jj];
	jw[n2+jj] = jw[n2+k];
	jw[n2+k] = j;
	/* exchange in w */
	s = w[jj];
	w[jj] = w[k];
	w[k] = s;
      }
      /* zero out element in row by resetting jw[n+jrow] to zero */
      jw[n+jrow] = -1;
      /* get the multiplier for row to be eliminated (jrow) + its
	 level */
      rowj = ilufac->U->ja[jrow];
      rowm = ilufac->U->ma[jrow];
      nnz = ilufac->U->nnzrow[jrow];
      lev = levs[jrow];
      fact = w[jj]*rowm[0];
      jlev = jw[n2+jj];
      if(jlev > lfil) continue;
      /* combine current row and row jrow */
      for(k = 1; k < nnz; k++) {
	s = fact*rowm[k];
	jcol = rowj[k];
	jpos = jw[n+jcol];
	if(jcol >= ii) {
	  /* dealing with upper part */
	  if(jpos == -1) {
	    /* this is a fill-in element */
	    i = ii + lenu;
	    jw[i] = jcol;
	    jw[n+jcol] = i;
	    w[i] = -s;
	    jw[n2+i] = jlev+lev[k]+1;
	    ++lenu;
	    if(lenu > n) {
	      ierr = -1;
	      break;
	    }
	  }
	  else {
	    /* this is not a fill-in element */
	    w[jpos] -= s;
	    jw[n2+jpos] = min(jw[n2+jpos], jlev+lev[k]+1);
	  }
	}
	else {
	  /* dealing with lower part */
	  if(jpos == -1) {
	    /* this is a fill-in element */
	    jw[lenl] = jcol;
	    jw[n+jcol] = lenl;
	    w[lenl] = -s;
	    jw[n2+lenl] = jlev+lev[k]+1;
	    ++lenl;
	    if(lenl > n) {
	      ierr = -1;
	      break;
	    }
	  }
	  else {
	    /* this is not a fill-in element */
	    w[jpos] -= s;
	    jw[n2+jpos] = min(jw[n2+jpos], jlev+lev[k]+1);
	  }
	}
      }
      w[jj] = fact;
      jw[jj] = jrow;
    }
    /* reset REAL-pointer to zero (U-part) */
    for(k = 0; k < lenu; k++) {
      jw[n+jw[ii+k]] = -1;
    }

    /* update l-matrix */
    j = 0;
    for(k = 0; k < lenl; k++) {
      if(jw[n2+k] <= lfil) {
	++j;
      }
    }
    ilufac->L->nnzrow[ii] = j;
    if(j > 0) {
      PARMS_malloc(ilufac->L->ja[ii], j, int);
      PARMS_malloc(ilufac->L->ma[ii], j, REAL);
    }
    rowj = ilufac->L->ja[ii];
    rowm = ilufac->L->ma[ii];
    j = 0;
    for(k = 0; k < lenl; k++) {
      if(jw[n2+k] <= lfil) {
	rowj[j] = jw[k];
	rowm[j] = w[k];
	++j;
      }
    }
    /* update u-matrix */
    if(w[ii] == 0.0) {
      ierr = -3;
      break;
    }
    j = 1;
    for(k = ii+1; k < ii+lenu; k++) {
      if(jw[n2+k] <= lfil) {
	++j;
      }
    }
    ilufac->U->nnzrow[ii] = j;
    PARMS_malloc(ilufac->U->ja[ii], j, int);
    rowj = ilufac->U->ja[ii];
    PARMS_malloc(ilufac->U->ma[ii], j, REAL);
    rowm = ilufac->U->ma[ii];
    PARMS_malloc(levs[ii], j, int);
    lev = levs[ii];
    rowm[0] = 1.0 / w[ii];
    rowj[0] = ii;
    j = 1;
    for(k = ii+1; k < ii+lenu; k++) {
      if(jw[n2+k] <= lfil) {
	rowj[j] = jw[k];
	rowm[j] = w[k];
	lev[j] = jw[n2+k];
	++j;
      }
    }
  }
  for(i = 0; i < ii; i++) {
    free(levs[i]);
  }
  free(levs);
  free(w);
  free(jw);
  return ierr;
}

int ilut(DistMatrix dm, PreCon precon, PrePar prepar)
{
  /*--------------------------------------------------------------------
   * construct local ilut preconditioner
   *
   * Zhongze Li, June. 2001
   *--------------------------------------------------------------------
   * ON ENTRY
   *===========
   *     dm   =  distributed local matrix handler
   * prepar   =  parameters for constructing preconditioner
   *
   * ON RETURN
   *===========
   * precon   =  preconditioner which is stored in MSR format
   *   ierr   =  integer. Error message with the following meaning.
   *             ierr = 0   --> successful return
   *             ierr > 0   --> zero pivot encountered at step number
   *                            ierr.
   *             ierr = -1  --> Error input matrix may be wrong.
   *                            (The elimination process has generated
   *                             a row in L or U whose length is
   *                             greater n)
   *             ierr = -2  --> Illegal value for lfil.
   *             ierr = -3  --> zero row encountered
   *------------------------------------------------------------------*/
  DistMatrixCsr dmc;
  ILUfac ilufac;
  int n,lfil,*ja,*ia,*rowj,*jw,ii,i,jj,j,lenl,lenu,j1,j2,jpos;
  int k,nnz,len,jrow, jcol, ierr;  
  REAL *w,*a,*rowm,dtol,tnorm, fact, s, t;

  dmc = (DistMatrixCsr)dm;
  ilufac = (ILUfac)precon->precd;
  n = dmc->comm->nloc;
  lfil = prepar->lfil[0];
  dtol = prepar->droptol[0];
  /* retrieve CSR structure from DistMatrix structure */
  a = dmc->A->smsf->ma;
  ja = dmc->A->smsf->ja;
  ia = dmc->A->smsf->ia;

  /* malloc memory for working arrays w and jw */
  PARMS_malloc(w, 2*n, REAL);
  PARMS_malloc(jw, 2*n, int);

  /* initialize nonzero indicator array */
  for(j = 0; j < n; j++) {
    jw[n+j] = -1;
  }
  /* beginning of main loop */
  ierr = 0;
  for(ii = 0; ii < n; ii++) {
    tnorm = 0.0;
    j1 = ia[ii]; 
    j2 = ia[ii+1];
    for(k = j1; k < j2; k++) {
      tnorm += fabs(a[k]);
    }
    if(tnorm == 0.0) {
      ierr = -3;
      break;
    }
    tnorm /= (REAL)(j2-j1);
    /* unpack L-part and U-part of row of A in arrays w */
    lenu = 1;
    lenl = 0;
    jw[ii] = ii;
    w[ii] = 0.0;
    jw[n+ii] = ii;
    for(j = j1; j < j2; j++) {
      jcol = ja[j];
      t = a[j];
      if(jcol < ii) {
	jw[lenl] = jcol;
	w[lenl] = t;
	jw[n+jcol] = lenl;
	++lenl;
      }
      else if(jcol == ii){
	w[ii] = t;
      }
      else {
	jpos = ii + lenu;
	jw[jpos] = jcol;
	w[jpos] = t;
	jw[n+jcol] = jpos;
	++lenu;
      }
    }
    len = 0;
    /* eliminate previous rows */
    for(jj = 0; jj < lenl; jj++) {
      /* in order to do the elimination in the correct order we must
	 select the smallest column index among jw[k],
	 k=jj+1,...,lenl.*/
      jrow = jw[jj];
      k = jj;
      for(j = jj+1; j < lenl; j++) {
	if(jw[j] < jrow) {
	  jrow = jw[j];
	  k = j;
	}
      }
      if(k != jj) {
	/* exchange in jw */
	j = jw[jj];
	jw[jj] = jw[k];
	jw[k] = j;
	/* exchange in jr */
	jw[n+jrow] = jj;
	jw[n+j] = k;
	/* exchange in w */
	s = w[jj];
	w[jj] = w[k];
	w[k] = s;
      }
      jw[n+jrow] = -1;
      /* get the multiplier for row to be eliminated (jrow) */
      rowm = ilufac->U->ma[jrow];
      fact = w[jj]*rowm[0];
      if(fabs(fact) <= dtol) {
	continue;
      }
      rowj = ilufac->U->ja[jrow];
      nnz = ilufac->U->nnzrow[jrow];
      /* combine current row and row jrow */
      for(k = 1; k < nnz; k++) {
	s = fact*rowm[k];
	jcol = rowj[k];
	jpos = jw[n+jcol];
	/* if fill-in element is small then disregard */
	if(jcol >= ii) {
	  /* dealing with upper part */
	  if(jpos == -1) {
	    /* this is a fill-in element */
	    i = ii+lenu;
	    jw[i] = jcol;
	    jw[n+jcol] = i;
	    w[i] = -s;
	    ++lenu;
	    if(lenu > n) {
	      ierr = -1;
	      goto done;
	    }
	  }
	  else {
	    /* this is not a fill-in element */
	    w[jpos] -= s;
	  }
	}
	else {
	  /* dealing with lower part */
	  if(jpos == -1) {
	    /* this is a fill-in element */
	    jw[lenl] = jcol;
	    w[lenl] = -s;
	    jw[n+jcol] = lenl;
	    ++lenl;
	    if(lenl > n) {
	      ierr = -1;
	      goto done;
	    }
	  }
	  else {
	    w[jpos] -= s;
	  }
	}
      }
      /*  store this pivot element -- (from left to right -- no danger
	  of overlap with the working elements in L (pivots). */ 
      w[len] = fact;
      jw[len] = jrow;
      ++len;
    }
    /* reset REAL-pointer to -1 (U-part) */
    for(k = 0; k < lenu; k++) {
      jw[n+jw[ii+k]] = -1;
    }

    /* update l-matrix */
    lenl = len > lfil ? lfil : len;
    ilufac->L->nnzrow[ii] = lenl;
    /* weigh the elements before sorting */
    for(k = 0; k < len; k++) {
      w[k] *= (dtol+w[n+jw[k]]);
    }
    /* quick sort */
    if(len > lenl)
      qsplitC(w,jw,len,lenl);

    if(lenl > 0) {
      PARMS_malloc(ilufac->L->ja[ii], lenl, int);
      rowj = ilufac->L->ja[ii];
      PARMS_malloc(ilufac->L->ma[ii], lenl, REAL);
      rowm = ilufac->L->ma[ii];
      memcpy(rowj, jw, lenl*sizeof(int));
    }
   
    for(k = 0; k < lenl; k++) {
      rowm[k] = w[k] / (dtol + w[n+jw[k]]);
    }
    /* update u-matrix */
    len = 0;
    for(k = 1; k < lenu; k++) {
      if(fabs(w[ii+k]) > dtol*fabs(w[ii])) {
	++len;
	w[ii+len] = w[ii+k];
	jw[ii+len] = jw[ii+k];
      }
    }
    lenu = len + 1 > lfil ? lfil: len + 1;
    jpos = lenu - 1;
    if(len > jpos)
      qsplitC(&w[ii+1], &jw[ii+1], len, jpos);
    ilufac->U->nnzrow[ii] = lenu;

    PARMS_malloc(ilufac->U->ma[ii], lenu, REAL);
    rowm = ilufac->U->ma[ii];

    PARMS_malloc(ilufac->U->ja[ii], lenu, int);
    rowj = ilufac->U->ja[ii];

    /* copy the rest of U */
    memcpy(&rowj[1], &jw[ii+1], jpos*sizeof(int));
    memcpy(&rowm[1], &w[ii+1], jpos*sizeof(REAL));
    t = fabs(w[ii]);
    for(k = 1; k < lenu; k++) {
      t += fabs(w[ii+k]);
    }
    w[n+ii] = t / (REAL)(lenu+1);
    /* store inverse of diagonal element of u */
    if(w[ii] == 0.0) {
      w[ii] = (0.0001 + dtol)*tnorm;
    }
    rowm[0] = 1.0 / w[ii];
    rowj[0] = ii;
  }
 done:
  free(w);
  free(jw);
  return ierr;
}

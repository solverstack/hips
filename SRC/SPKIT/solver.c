/* @noheader */

/*----------------------------------------------------------------------
 *   pgmres  : standard preconditioned GMRES as a local accelarator
 *--------------------------------------------------------------------*/
#include "../../INCLUDE/psparslib.h"
int pgmres(DistMatrix dm, PreCon precon, IterPar ipar, Vec rhs, Vec x)
{
  /*---------------------------------------------------------------*
   * preconditioned GMRES as preconditioning operation for FGMRES  *
   *
   * Zhongze Li, June. 2001
   *---------------------------------------------------------------*
   *                                                               *
   * ON ENTRY                                                      *
   *==========                                                     *
   *     dm   =  distributed matrix output from setup              *
   *  precon  =  a pointer to precon strucutre                     *
   *    ipar  =  a pointer to the structure contains               *
   *             parameters(maxits,eps,etc.) related to iteration  *
   *     rhs  =  right hand side vector                            *
   *                                                               *
   * ON RETURN                                                     *
   *===========                                                    *
   *      x  =  local solution vector handler                      *
   *---------------------------------------------------------------*/
  int n,im,i,ii,i1,k,k1,j,jj,maxits,its,incx,out_flag,in_flag,ierr;
  static int iters = 0;
  REAL **vv, **hh, **w, *c, *s, *rs;
  REAL *b,*sol,eps,eps1,ro,t,alpha,gam;
  Vec wk1, wk2;


 /*----------------------------------------------------------------*
  |        PART1: Initialization                                   |
  *----------------------------------------------------------------*/
  n = dm->comm->nloc;
  sol = x->vec;
  for(i = 0;i < n; i++) {
    sol[i] = 0.0;
  }
  b = rhs->vec;
  im = ipar->ipar[3];
  maxits = ipar->ipar[4];
  eps = ipar->pgfpar[0];

  /* Create working vector wk1 and wk2 */
  CreateVec(&wk1);
  CopyComm(dm, wk1);
  CreateVec(&wk2);
  CopyComm(dm, wk2);

  /* allocate memory for working local arrays */
  PARMS_malloc(vv, im+1, REAL *);
  for(i = 0; i < im+1; i++) {
    PARMS_malloc(vv[i], n, REAL);
  }

  PARMS_malloc(hh, im, REAL *);
  for(i = 0; i < im; i++) {
    PARMS_malloc(hh[i], im+1, REAL);
  }
  PARMS_malloc(c, im, REAL);
  PARMS_malloc(s, im, REAL);
  PARMS_malloc(rs, im+1, REAL);
  PARMS_malloc(w, im, REAL *);
  for(i = 0; i < im; i++) {
    PARMS_malloc(w[i], n, REAL);
  }

 /*----------------------------------------------------------------*
  |      PART2: Iterative GMRES                                    |
  *----------------------------------------------------------------*/
  incx = 1;
  out_flag = TRUE;
  its = 0;

  /* local matrix-vector product for the interior unknowns */
  amux(dm, sol, vv[0]);
  
  for(j = 0; j < n; j++) {
    vv[0][j] = b[j] - vv[0][j];
  }
  /* outer loop starts here */
  while(out_flag) {
    ro = DDOT(n, vv[0], incx, vv[0], incx);
    ro = sqrt(ro);
    
    if(fabs(ro-ZERO) <= EPSILON) {
      ierr = -1;
      out_flag = FALSE;
      break;
    }
    t = 1.0 / ro;
    DSCAL(n, t, vv[0], incx);
    if(its == 0)
      eps1 = eps*ro;
    /* initialize 1-st term of rhs of hessenberg system */
    rs[0] = ro;
    i = -1;
    in_flag = TRUE;
    while(in_flag) {
      i++;
      its++;
      i1 = i + 1;
      wk1->vec = vv[i];
      sol0(dm, precon, ipar, wk1, wk2);
      /* matvec */
      amux(dm, wk2->vec, vv[i1]);
      /* modified gram - schmidt */
      for(j = 0; j <= i; j++) {
	t = DDOT(n, vv[j], incx, vv[i1], incx);
	hh[i][j] = t;
	alpha = -t;
	DAXPY(n, alpha, vv[j], incx, vv[i1], incx);
      }
      t = DDOT(n, vv[i1], incx, vv[i1], incx);
      t = sqrt(t);
      hh[i][i1] = t;
      if(fabs(t-ZERO) > EPSILON) {
	t = 1.0 / t;
	DSCAL(n, t, vv[i1], incx);
      }
      /* done with modified gram schimd and arnoldi step. now update
	 factorization of hh */
      if(i != 0) {
	for(k = 1; k <= i; k++) {
	  k1 = k - 1;
	  t = hh[i][k1];
	  hh[i][k1] = c[k1]*t + s[k1]*hh[i][k];
	  hh[i][k] = -s[k1]*t + c[k1]*hh[i][k];
	}
      }
      gam = sqrt(hh[i][i]*hh[i][i] + hh[i][i1]*hh[i][i1]);
      /* if gamma is zero then any small value will do ...
	 will affect only residual estimate */
      if(fabs(gam-ZERO) <= EPSILON)
	gam = EPSMAC;
      /* get next plane rotation */
      c[i] = hh[i][i]/gam;
      s[i] = hh[i][i1]/gam;
      rs[i1] = -s[i]*rs[i];
      rs[i] = c[i]*rs[i];
      /* determine res. norm and test for convergence */
      hh[i][i] = c[i]*hh[i][i] + s[i]*hh[i][i1];
      ro = fabs(rs[i1]);
      if((i+1 >= im) || (ro <= eps1) || (its >= maxits)) {
	in_flag = FALSE;
      }
    }
    /* now compute solution first solve upper triangular system */
    rs[i] = rs[i]/hh[i][i];
    for(ii = 2; ii <= i+1; ii++) {
      k = i-ii+1;
      k1 = k+1;
      t = rs[k];
      for(j = k1; j <= i; j++) {
	t = t - hh[j][k]*rs[j];
      }
      rs[k] = t/hh[k][k];
    }
    /* form linear combination of V(*,i)'s to get solution */
    t = rs[0];
    for(k = 0; k < n; k++) {
      wk2->vec[k] = vv[0][k]*t;
    }

    for(j = 1; j <= i; j++) {
      t = rs[j];
      DAXPY(n, t, vv[j], incx, wk2->vec, incx);
    }
    /* call preconditioner */
    sol0(dm, precon, ipar, wk2, wk2);
    alpha = 1.0;
    DAXPY(n, alpha, wk2->vec, incx, sol, incx);
      
    /* test for return */
    if((ro <= eps1) || (its >=  maxits)) {
      out_flag = FALSE;
      if(ro <= eps1) {
	ierr = 0;
      }
      else if(its >= maxits) {
	ierr = 1;
      }
    }
    else {
      for(j = 1; j <= i+1; j++) {
	jj= i1-j+1;
	rs[jj-1] = -s[jj-1]*rs[jj];
	rs[jj] = c[jj-1]*rs[jj];
      }
      for(j = 0; j <= i1; j++) {
	t = rs[j];
	if(j == 0) t = t-1.0;
	DAXPY(n, t, vv[j], incx, vv[0], incx);
      }      
    }
  }

 /*-----------------------------------------------------------------*
  |     PART3:   Record the convergence iters and Release buffer    |
  *-----------------------------------------------------------------*/
  iters += its;
  ipar->in_iters = iters;
  for(i = 0; i < im+1; i++){
    free(vv[i]);
  }
  free(vv);
  for(i = 0; i < im; i++) {
    free(hh[i]);
  }
  free(hh);
  for(i = 0; i < im; i++) {
    free(w[i]);
  }
  free(w);
  free(c); free(s); free(rs);
  DeleteVec(&wk1);
  DeleteVec(&wk2);
  return ierr;
}
/* End of pgmres() */

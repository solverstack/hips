      subroutine wreadmtpar(job,fname,length,a,ja,ia,lia,lian,rhs,
     * nrhs,guesol,nrow,ncol,nnz,lnnz,title,key,type,nyrange,
     * npro,mypro,ierr)
c-----------------------------------------------------------------------
c     this  subroutine reads  a boeing/harwell matrix in parallel, given the
c     corresponding file. handles right hand sides in full format
c     only (no sparse right hand sides). Also the matrix must  be
c     in assembled forms.
c     The arguments are 
c     job - integer to indicate what is to be read. (note: job is an
c          input and output parameter, it can be modified on return)
c          job = 0    read the values of ncol, nrow, nnz,lnnz, title, key,
c                     type and return. matrix is not read and arrays
c                     a, ja, ia, rhs are not touched.
c          job = 1&2    read matrix i.e., the arrays a ja and ia.
c          job = 3    read matrix and right hand sides: a,ja,ia,rhs.
c                     rhs may contain initial guesses and exact
c                     solutions appended to the actual right hand sides.
c                     this will be indicated by the output parameter
c                     guesol [see below].
c     fname (input)- file name which has to be read
c     length(input)- filename lenght
c     a (output)- elements in part of the matrix to be read from file
c     ja (output)- row indices to be read by one processor
c     ia (output)- all column pointers of the matrix of size n+1
c     lia (output)- local column pointers of the matrix
c     lian (output)- number of elements in lia
c     rhs (output)- right hand side of the matrix
c     nrhs (output & input)- integer. nrhs is an input as well as ouput 
c          parameter at input nrhs contains the total length of the array rhs.
c          See also ierr and nrhs in output parameters.
c guesol(output) = a 2-character string indicating whether an initial guess
c          (1-st character) and / or the exact solution (2-nd
c          character) is provided with the right hand side.
c          if the first character of guesol is 'G' it means that an
c          an intial guess is provided for each right-hand side.
c          These are appended to the right hand-sides in the array rhs.
c          if the second character of guesol is 'X' it means that an
c          exact solution is provided for each right-hand side.
c          These are  appended to the right hand-sides
c          and the initial guesses (if any) in the array rhs.
c     nrow (output)- number of rows of matrix
c     ncol (output)- number of column of matrix
c     nnz (output)- total number of non zero elements of matrix
c     lnnz (output)- array of size number of processors containg the 
c           local numbers
c            of non zero elements
c     title (output) - character title of the matrix character * 72
c     key  (output)- character of length 8 byte (key to matrix)
c     type  (output)- type of the matrix character*3
c     nyrange (output)- int array of size npro (number of processors 
c               reading the matrix and points the range of rows to be 
c               read by each processors (e.g. second processor will be 
c               reading the rows starting from nyrange(2) till nyrange(3)-1)
c     npro (integer input) - total number of processors
c     mypro (integer input) - processor id of local process
c    ierr  (integer output)- return in case of error. 
c
c   NOTE : This routine reads the part of Matrix a,lia and ja from row starting
c          nyrange(mypro) till nyrange(mypro)-1. It reads and stores ia and rhs
c          in full. The output of the matrix has CSC format just like HB uses
c          to store the matrix. 
c          also the maxar is the parameter which could be modified here. The 
c          arrays of this size are work arrays to be used in order to read 
c          the matrix without assigning the big memory to the matrix arrays
c          a, and ja.
c
c
c-----------------------------------------------------------------------
      character fname*100, title*72, key*8, type*3, guesol*2, fname1*100
      integer nrow, ncol, nnz, nrhs, nmax, nzmax, job, length, ierr
      integer ia (*), ja (*), lnnz(*),lia(*), lian
      real*8 a(*), rhs(*)
      integer rank, nyrange(*)

      fname1 = " "
      fname1(1:length) = fname(1:length)
      iounit=10
      open(iounit,file=fname1)
      call readmtpar(job,iounit,a,ja,ia,lia,lian,
     * rhs,nrhs,guesol,nrow,ncol,nnz,lnnz,title,key,type,nyrange,
     * npro,mypro,ierr)
       close(iounit)
      return
      end

c-----------------------------------------------------------------------
      subroutine readmtpar(job,iounit,a,ja,ia,lia,lian,
     *     rhs,nrhs,guesol,nrow,ncol,nnz,lnzmax,title,key,type,
     * nyrange, nop,nrank,ierr)
c-----------------------------------------------------------------------
      PARAMETER(maxar=1000)
      character title*72, key*8, type*3, ptrfmt*16, indfmt*16,
     1     valfmt*20, rhsfmt*20, rhstyp*3, guesol*2
      integer totcrd, ptrcrd, indcrd, valcrd, rhscrd, nrow, ncol,
     1     nnz, neltvl, nrhs, nmax, nzmax, nrwindx, nop, nrank
      integer max(20000),itemp(maxar),jtemp(maxar), nax(20000)
      real*8 atemp(maxar),temp(maxar)
      real*8 a(*), rhs(*)
      integer ia (*), ja (*), nyrange(*),lia(*),lnzmax(*), lian
      integer k,job,iounit
c-----------------------------------------------------------------------
      read (iounit,10) title, key, totcrd, ptrcrd, indcrd, valcrd, 
     1     rhscrd, type, nrow, ncol, nnz, neltvl, ptrfmt, indfmt, 
     2     valfmt, rhsfmt
      n=ncol
              lenrhs = nrhs
      ierr = 0
                       idis=nrow-(nrow/nop)*nop
                       nyrange(1)=1
                       do i=2, nop
                       if(i.le.idis)then
                       nyrange(i)=nyrange(i-1)+(1+nrow/nop)
                       else
                       nyrange(i)=(nrow/nop)+nyrange(i-1)
                       endif
                       enddo
                       nyrange(nop+1)=nrow+1

          iter=nnz/maxar+1
          if(iter.eq.1)then
          max(1)=nnz
          else
          do k=1,iter-1
          max(k)=maxar
          enddo
          max(iter)=mod(nnz,maxar)
          endif
c         In order to skip the column values 
                  jter=n/maxar+1
                  if(jter.eq.1) then
                  nax(1)=n+1
                  else
                  do k=1,jter-1
                  nax(k)=maxar
                  enddo
                  endif
                  nax(jter)=mod(n+1,maxar)
 10   format (a72, a8 / 5i14 / a3, 11x, 4i14 / 2a16, 2a20)
      if (job .le. 0) then
          do kk=1,nop
          ki=0
          do k=1,iter
          rewind(iounit)
c     
      read (iounit,10) title, key, totcrd, ptrcrd, indcrd, valcrd, 
     1     rhscrd, type, nrow, ncol, nnz, neltvl, ptrfmt, indfmt, 
     2     valfmt, rhsfmt
c     
          ierr=0
          if (rhscrd .gt. 0)then
              read (iounit,11) rhstyp, nrhs, nrwindx
           endif
 11   format (a3,11x,i14,i14)
      read (iounit,ptrfmt) ((jtemp (i), i = 1, nax(ii)), ii=1,jter)
      read (iounit,indfmt) ((itemp (i), i = 1, max(ii)),ii=1,k)
      do i=1,max(k)
      if(itemp(i).ge.nyrange(kk).and.itemp(i).lt
     1.nyrange(kk+1))then
           ki=ki+1
        endif
      enddo
      enddo
      lnzmax(kk)=ki
      enddo
      return
c
      else
c
      ki=0
      innz=0
      lnnz=0
      idone=0
      do k=1,iter
      rewind(iounit)
c     
      read (iounit,10) title, key, totcrd, ptrcrd, indcrd, valcrd, 
     1     rhscrd, type, nrow, ncol, nnz, neltvl, ptrfmt, indfmt, 
     2     valfmt, rhsfmt
c     
      ierr=0
          if (rhscrd .gt. 0)then
              read (iounit,11) rhstyp, nrhs, nrwindx
           endif
      read (iounit,ptrfmt) (ia (i), i = 1, n+1)
       
      if(k.ge.iter)then
      read (iounit,indfmt) ((itemp (i), i = 1, max(ii)),ii=1,k)
      else
      read (iounit,indfmt) ((itemp (i), i = 1, max(ii)),ii=1,k)
     *,((jtemp(i),i=1,max(ii)),ii=k+1,iter)
      endif
c       read atemp now
      read (iounit,valfmt) ((atemp(i), i = 1, max(ii)),ii=1,k)
         do i=1,max(k)
       innz=innz+1
       if(itemp(i).ge.nyrange(nrank+1) .and. itemp(i).lt.
     *    nyrange(nrank+2))then
          ki=ki+1
         do i1=1,n
                if(innz.ge.ia(i1).and.innz.lt.ia(i1+1)) then
                if(idone.ne.i1)then
                idone=i1
                lnnz=lnnz+1
                lia(lnnz)=ki
                endif
                endif
         enddo
          ja(ki)=itemp(i)
          a(ki)=atemp(i)
       endif
         enddo
      enddo
      lnzmax(nrank+1)=ki
       lia(lnnz+1)=lnzmax(nrank+1)+1
       lian=lnnz+1
       endif
c     --- reading rhs if required ---------------- 
      if (job .le. 2)  return
c     --- and if available ----------------------- 
      if ( rhscrd .le. 0) then
	 job = 2
	 return
      endif
c     
c     --- read right-hand-side.-------------------- 
c     
      if (rhstyp(1:1) .eq. 'M') then 
         ierr = 4
         return
      endif
c     
      guesol = rhstyp(2:3) 
c     
      nvec = 1 
      if (guesol(1:1) .eq. 'G' .or. guesol(1:1) .eq. 'g') nvec=nvec+1
      if (guesol(2:2) .eq. 'X' .or. guesol(2:2) .eq. 'x') nvec=nvec+1
c     
c      print*,nrhs,nrow,nvec,"nrhs,nrow,nvec"
      len = nrhs*nrow 
c     
      if (len*nvec .gt. lenrhs) then
c       print*, "from here 2"
         ierr = 5
         return
      endif
c     
c     read right-hand-sides
c     
      next = 1
      iend = len
      read(iounit,rhsfmt) (rhs(i), i = next, iend)
c     
c     read initial guesses if available
c     
      if (guesol(1:1) .eq. 'G' .or. guesol(1:1) .eq. 'g') then
         next = next+len
         iend = iend+ len
         read(iounit,valfmt) (rhs(i), i = next, iend)
      endif
c     
c     read exact solutions if available
c     
      if (guesol(2:2) .eq. 'X' .or. guesol(2:2) .eq. 'x') then
         next = next+len
         iend = iend+ len
         read(iounit,valfmt) (rhs(i), i = next, iend)
      endif
c     
      return
      end

      subroutine wreadmtpar2(job,fname,length,a,ja,ia,lia,
     *lian,rhs,nrhs,guesol,nrow,ncol,nnz,title,key,type,
     * riord,riordn,ierr)
c-----------------------------------------------------------------------
c     this  subroutine reads  a boeing/harwell matrix,  given the
c     corresponding file. handles right hand sides in full format
c     only (no sparse right hand sides). Also the matrix must  be
c     in assembled forms.
c     this  subroutine reads  a boeing/harwell matrix in parallel, given the
c     corresponding file. handles right hand sides in full format
c     only (no sparse right hand sides). Also the matrix must  be
c     in assembled forms.
c     The arguments are 
c     job - integer to indicate what is to be read. (note: job is an
c          input and output parameter, it can be modified on return)
c          job = 0    read the values of ncol, nrow, nnz,lnnz, title, key,
c                     type and return. matrix is not read and arrays
c                     a, ja, ia, rhs are not touched.
c          job = 1&2    read matrix i.e., the arrays a ja and ia.
c          job = 3    read matrix and right hand sides: a,ja,ia,rhs.
c                     rhs may contain initial guesses and exact
c                     solutions appended to the actual right hand sides.
c                     this will be indicated by the output parameter
c                     guesol [see below].
c     fname (input)- file name which has to be read
c     length(input)- filename lenght
c     a (output)- elements in part of the matrix to be read from file
c     ja (output)- row indices to be read by one processor
c     ia (output)- all column pointers of the matrix of size n+1
c     lia (output)- local column pointers of the matrix
c     lian (output)- number of elements in lia
c     rhs (output)- right hand side of the matrix
c     nrhs (output & input)- integer. nrhs is an input as well as ouput 
c          parameter at input nrhs contains the total length of the array rhs.
c          See also ierr and nrhs in output parameters.
c     guesol(output) = a 2-character string indicating whether an initial guess
c          (1-st character) and / or the exact solution (2-nd
c          character) is provided with the right hand side.
c          if the first character of guesol is 'G' it means that an
c          an intial guess is provided for each right-hand side.
c          These are appended to the right hand-sides in the array rhs.
c          if the second character of guesol is 'X' it means that an
c          exact solution is provided for each right-hand side.
c          These are  appended to the right hand-sides
c          and the initial guesses (if any) in the array rhs.
c     nrow (output)- number of rows of matrix
c     ncol (output)- number of column of matrix
c     nnz (output)- total number of non zero elements of matrix
c     lnnz (output)- array of size number of processors containg the 
c           local numbers
c            of non zero elements
c     title (output) - character title of the matrix character * 72
c     key  (output)- character of length 8 byte (key to matrix)
c     type  (output)- type of the matrix character*3
c     riord (input) - the permutation integer array indicating the rows which
c           are to be picked up by the local processor.
c     riordn (input) - the length of array riord.
c    ierr  (integer output)- return in case of error. 
c
c   NOTE : This routine reads the part of Matrix a,lia and ja i.e. row numbers
c          contained in riord array. It reads and stores ia and rhs
c          in full. The output of the matrix has CSC format just like HB uses
c          to store the matrix. 
c          also the maxar is the parameter which could be modified here. The 
c          arrays of this size are work arrays to be used in order to read 
c          the matrix without assigning the big memory to the matrix arrays
c          a, and ja.
c
c-----------------------------------------------------------------------
      character fname*100, title*72, key*8, type*3, guesol*2, fname1*100
      integer nrow, ncol, nnz, nrhs, nmax, nzmax, job, length, ierr
      integer ia (*), ja (*), lia(*), lian,riord(*)
      real*8 a(*), rhs(*)
      integer rank, riordn

      fname1 = " "
      fname1(1:length) = fname(1:length)
      iounit=10
      open(iounit,file=fname1)
      call readmtpar2(job,iounit,a,ja,ia,lia,lian,
     * rhs,nrhs,guesol,nrow,ncol,nnz,title,key,type,
     * riord,riordn,ierr)
       close(iounit)
      return
      end
      subroutine readmtpar2(job,iounit,a,ja,ia,lia,lian,
     *     rhs,nrhs,guesol,nrow,ncol,nnz,title,key,type,
     * riord,riordn,ierr)
c-----------------------------------------------------------------------
      PARAMETER(maxar=1000)
      character title*72, key*8, type*3, ptrfmt*16, indfmt*16,
     1     valfmt*20, rhsfmt*20, rhstyp*3, guesol*2
      integer totcrd, ptrcrd, indcrd, valcrd, rhscrd, nrow, ncol,
     1     nnz, neltvl, nrhs, nmax, nzmax, nrwindx, nop, nrank
      integer max(20000),itemp(maxar),jtemp(maxar)
      real*8 atemp(maxar),temp(maxar)
      real*8 a(*), rhs(*)
      integer ia (*), ja (*), lia(*),riord(*)
      integer k,job,iounit, lian,riordn
c-----------------------------------------------------------------------
      read (iounit,10) title, key, totcrd, ptrcrd, indcrd, valcrd, 
     1     rhscrd, type, nrow, ncol, nnz, neltvl, ptrfmt, indfmt, 
     2     valfmt, rhsfmt
      n=ncol
              lenrhs = nrhs
      ierr = 0

          iter=nnz/maxar+1
          if(iter.eq.1)then
          max(1)=nnz
          else
          do k=1,iter-1
          max(k)=maxar
          enddo
          max(iter)=mod(nnz,maxar)
          endif
c         In order to skip the column values 
 10   format (a72, a8 / 5i14 / a3, 11x, 4i14 / 2a16, 2a20)
      if (job .le. 0) then
          ki=0
      innz=0
          do k=1,iter
          rewind(iounit)
c     
      read (iounit,10) title, key, totcrd, ptrcrd, indcrd, valcrd, 
     1     rhscrd, type, nrow, ncol, nnz, neltvl, ptrfmt, indfmt, 
     2     valfmt, rhsfmt
c     
          ierr=0
          if (rhscrd .gt. 0)then
              read (iounit,11) rhstyp, nrhs, nrwindx
           endif
 11   format (a3,11x,i14,i14)
      read (iounit,ptrfmt) (ia (i), i = 1, n+1)
      read (iounit,indfmt) ((itemp (i), i = 1, max(ii)),ii=1,k)
        do i=1,max(k)
        innz=innz+1
        do j=1,riordn
          if(itemp(i).eq.riord(j))then
                 ki=ki+1
          endif
        enddo
        enddo
      enddo
      nnz=ki
      return
c
      else
c
      ki=0
      idone=0
      innz=0
      lnnz=0
        ij=0
      do k=1,iter
      rewind(iounit)
c     
      read (iounit,10) title, key, totcrd, ptrcrd, indcrd, valcrd, 
     1     rhscrd, type, nrow, ncol, nnz, neltvl, ptrfmt, indfmt, 
     2     valfmt, rhsfmt
c     
      ierr=0
          if (rhscrd .gt. 0)then
              read (iounit,11) rhstyp, nrhs, nrwindx
           endif
      read (iounit,ptrfmt) (ia (i), i = 1, n+1)
       
      if(k.ge.iter)then
      read (iounit,indfmt) ((itemp (i), i = 1, max(ii)),ii=1,k)
      else
      read (iounit,indfmt) ((itemp (i), i = 1, max(ii)),ii=1,k)
     *,((jtemp(i),i=1,max(ii)),ii=k+1,iter)
      endif
c       read atemp now
      read (iounit,valfmt) ((atemp(i), i = 1, max(ii)),ii=1,k)
        do i=1,max(k)
        innz=innz+1
        do j=1,riordn
          if(itemp(i).eq.riord(j))then
                 ki=ki+1
                 ja(ki)=itemp(i)
                 a(ki)=atemp(i)
           do kj=1,n
           if(innz.ge.ia(kj).and.innz.lt.ia(kj+1))then
            if(idone.ne.kj)then
            idone=kj
           ij=ij+1
           lia(ij)=ki
           endif
           endif
           enddo
          endif
        enddo
        enddo
       enddo
       lian=ij+1
       lia(ij+1)=ki+1
      nnz=ki
      endif
c     --- reading rhs if required ---------------- 
      if (job .le. 2)  return
c     --- and if available ----------------------- 
      if ( rhscrd .le. 0) then
	 job = 2
	 return
      endif
c     
c     --- read right-hand-side.-------------------- 
c     
      if (rhstyp(1:1) .eq. 'M') then 
         ierr = 4
         return
      endif
c     
      guesol = rhstyp(2:3) 
c     
      nvec = 1 
      if (guesol(1:1) .eq. 'G' .or. guesol(1:1) .eq. 'g') nvec=nvec+1
      if (guesol(2:2) .eq. 'X' .or. guesol(2:2) .eq. 'x') nvec=nvec+1
c     
c      print*,lenrhs,nrhs,nrow,nvec,"lenrhs,nrhs,nrow,nvec"
      len = nrhs*nrow 
c     
      if (len*nvec .gt. lenrhs) then
       print*, "from here 2"
         ierr = 5
         return
      endif
c     
c     read right-hand-sides
c     
      next = 1
      iend = len
      read(iounit,rhsfmt) (rhs(i), i = next, iend)
c     
c     read initial guesses if available
c     
      if (guesol(1:1) .eq. 'G' .or. guesol(1:1) .eq. 'g') then
         next = next+len
         iend = iend+ len
         read(iounit,valfmt) (rhs(i), i = next, iend)
      endif
c     
c     read exact solutions if available
c     
      if (guesol(2:2) .eq. 'X' .or. guesol(2:2) .eq. 'x') then
         next = next+len
         iend = iend+ len
         read(iounit,valfmt) (rhs(i), i = next, iend)
      endif
c     
      return
c---------end-of-readmt-------------------------------------------------
c-----------------------------------------------------------------------
      end

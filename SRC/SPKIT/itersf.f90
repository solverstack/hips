c----------------------------------------------------------------------c
c                          S P A R S K I T                             c
c----------------------------------------------------------------------c

c
c     real*8 function pddot(n,x,ix,y,iy)
c     integer n, ix, iy
c     real*8 x(1+(n-1)*ix), y(1+(n-1)*iy)
c
c     This interface of pddot is exactly the same as that of
c     DDOT (or SDOT if real == real*8) from BLAS-1. It should have
c     same functionality as DDOT on a single processor machine. On a
c     parallel/distributed environment, each processor can perform
c     DDOT on the data it has, then perform a summation on all the
c     partial results.
c

      real*8 function pddot(n,x,ix,y,iy)
c      include 'mpif.h'
      integer n, ix, iy
      real*8 lsd, x(*), y(*), ddot
      external ddot

      lsd = ddot(n,x,ix,y,iy)
      call MPI_allreduce(lsd,pddot,1,
     *     MPI_double_precision,
     *     MPI_sum,MPI_COMM_WORLD, ierr)
      return
      end
c-----end-of-pddot



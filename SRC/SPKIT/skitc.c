/* @noheader */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "../../INCLUDE/armsheads.h"
int qsplitC(REAL *a, int *ind, int n, int ncut)
{
/*----------------------------------------------------------------------
|     does a quick-sort split of a real array.
|     on input a[0 : (n-1)] is a real array
|     on output is permuted such that its elements satisfy:
|
|     abs(a[i]) >= abs(a[ncut-1]) for i < ncut-1 and
|     abs(a[i]) <= abs(a[ncut-1]) for i > ncut-1
|
|     ind[0 : (n-1)] is an integer array permuted in the same way as a.
|---------------------------------------------------------------------*/
   REAL tmp, abskey;
   int j, itmp, first, mid, last;
   first = 0;
   last = n-1;
   if (ncut<first || ncut>last) return 0;
/* outer loop -- while mid != ncut */
label1:
   mid = first;
   abskey = fabs(a[mid]);
  for (j=first+1; j<=last; j++) {
     if (fabs(a[j]) > abskey) {
	 tmp = a[++mid];
	 itmp = ind[mid];
	 a[mid] = a[j];
	 ind[mid] = ind[j];
	 a[j]  = tmp;
	 ind[j] = itmp;
      }
   }
/* interchange */
   tmp = a[mid];
   a[mid] = a[first];
   a[first]  = tmp;
   itmp = ind[mid];
   ind[mid] = ind[first];
   ind[first] = itmp;
/* test for while loop */
   if (mid == ncut) return 0;
   if (mid > ncut) 
      last = mid-1;
   else
      first = mid+1;
   goto label1;
}
/*--------------- end of qsplitC ----------------------------------------
|---------------------------------------------------------------------*/
int SparTran(csptr amat, csptr bmat, int job, int flag)
{
/*----------------------------------------------------------------------
| Finds the transpose of a matrix stored in SparRow format.
|
|-----------------------------------------------------------------------
| on entry:
|----------
| (amat) = a matrix stored in SparRow format.
|
| job    = integer to indicate whether to fill the values (job.eq.1)
|          of the matrix (bmat) or only the pattern.
|
| flag   = integer to indicate whether the matrix has been filled
|          0 - no filled
|          1 - filled
|
| on return:
| ----------
| (bmat) = the transpose of (mata) stored in SparRow format.
|
| integer value returned:
|             0   --> successful return.
|             1   --> memory allocation error.
|---------------------------------------------------------------------*/
   int i, j, *ind, pos, size=amat->n, *aja;
   REAL *ama;
   ind = (int *) malloc(size*sizeof(int));
   if (ind == NULL) return 1;

   for (i=0; i<size; i++)
      ind[i] = 0;
   if(!flag) {
/*  compute lengths  */
   for (i=0; i<size; i++) {
      aja = amat->ja[i];
      for (j=0; j<amat->nnzrow[i]; j++)
	 ind[aja[j]]++;
   }
/*  allocate space  */
   for (i=0; i<size; i++) {
      bmat->ja[i] = (int *) malloc(ind[i]*sizeof(int));
      if (bmat->ja[i] == NULL) return 1;
      bmat->nnzrow[i] = ind[i];
      if (job == 1) {
         bmat->ma[i] = (REAL *) malloc(ind[i]*sizeof(REAL));
         if (bmat->ma[i] == NULL) return 1;
      }
      ind[i] = 0;
   }
   }
/*  now do the actual copying  */
   for (i=0; i<size; i++) {
      aja = amat->ja[i];
      if (job == 1)
	 ama = amat->ma[i];
      for (j=0; j<amat->nnzrow[i]; j++) {
	 pos = aja[j];
         bmat->ja[pos][ind[pos]] = i;
         if (job == 1)
            bmat->ma[pos][ind[pos]] = ama[j];
         ind[pos]++;
      }
   }
   free(ind);
  
   return 0;
}

int rpermC(csptr mat, int *perm)
{
/*----------------------------------------------------------------------
|
| This subroutine permutes the rows of a matrix in SparRow format. 
| rperm  computes B = P A  where P is a permutation matrix.  
| The permutation P is defined through the array perm: for each j, 
| perm[j] represents the destination row number of row number j. 
|
|-----------------------------------------------------------------------
| on entry:
|----------
| (amat) = a matrix stored in SparRow format.
|
|
| on return:
| ----------
| (amat) = P A stored in SparRow format.
|
| integer value returned:
|             0   --> successful return.
|             1   --> memory allocation error.
|---------------------------------------------------------------------*/
   int **addj, *nnz, i, size=mat->n;
   REAL **addm;
   addj = (int **) malloc(size*sizeof(int *));
   addm = (REAL **) malloc(size*sizeof(REAL *));
   nnz = (int *) malloc(size*sizeof(int));
   if ( (addj == NULL) || (addm == NULL) || (nnz == NULL) ) return 1;
   for (i=0; i<size; i++) {
      addj[perm[i]] = mat->ja[i];
      addm[perm[i]] = mat->ma[i];
      nnz[perm[i]] = mat->nnzrow[i];
   }
   for (i=0; i<size; i++) {
      mat->ja[i] = addj[i];
      mat->ma[i] = addm[i];
      mat->nnzrow[i] = nnz[i];
   }
   free(addj);
   free(addm);
   free(nnz);
   return 0;
}
/*------- end of rperm ------------------------------------------------- 
|---------------------------------------------------------------------*/
int cpermC(csptr mat, int *perm) 
{
/*----------------------------------------------------------------------
|
| This subroutine permutes the columns of a matrix in SparRow format.
| cperm computes B = A P, where P is a permutation matrix.
| that maps column j into column perm(j), i.e., on return 
| The permutation P is defined through the array perm: for each j, 
| perm[j] represents the destination column number of column number j. 
|
|-----------------------------------------------------------------------
| on entry:
|----------
| (mat) = a matrix stored in SparRow format.
|
|
| on return:
| ----------
| (mat) = A P stored in SparRow format.
|
| integer value returned:
|             0   --> successful return.
|             1   --> memory allocation error.
|---------------------------------------------------------------------*/
   int i, j, *newj, size=mat->n, *aja;
   newj = (int *) malloc(size*sizeof(int));
   if (newj == NULL) return 1;
   for (i=0; i<size; i++) {
      aja = mat->ja[i];
      for (j=0; j<mat->nnzrow[i]; j++)
	 newj[j] = perm[aja[j]];
  
      for (j=0; j<mat->nnzrow[i]; j++)
	 aja[j] = newj[j];
      mat->ja[i] = aja;
   }
   free(newj);
   return 0;
}
/*------- end of cperm ------------------------------------------------- 
|---------------------------------------------------------------------*/
int dpermC(csptr mat, int *perm) 
{
/*----------------------------------------------------------------------
|
| This subroutine permutes the rows and columns of a matrix in 
| SparRow format.  dperm computes B = P^T A P, where P is a permutation 
| matrix.
|
|-----------------------------------------------------------------------
| on entry:
|----------
| (amat) = a matrix stored in SparRow format.
|
|
| on return:
| ----------
| (amat) = P^T A P stored in SparRow format.
|
| integer value returned:
|             0   --> successful return.
|             1   --> memory allocation error.
|---------------------------------------------------------------------*/
   if (rpermC(mat, perm)) return 1;
   if (cpermC(mat, perm)) return 1;
   return 0;
}
/*------- end of cperm ------------------------------------------------- 
|---------------------------------------------------------------------*/

int roscalC(csptr mata, REAL *diag, int nrm)
{
/*---------------------------------------------------------------------
|
| This routine scales each row of mata so that the norm is 1.
|
|----------------------------------------------------------------------
| on entry:
| mata  = the matrix (in SparRow form)
| nrm   = type of norm
|          0 (\infty),  1 or 2
|
| on return
| diag  = diag[j] = 1/norm(row[j])
|
|     0 --> normal return
|     j --> row j is a zero row
|--------------------------------------------------------------------*/
/*   local variables    */
   int i, k;
   REAL *kr, scal;
   for (i=0; i<mata->n; i++) {
      scal = 0.0;
      kr = mata->ma[i];
      if (nrm == 0) {
	 for (k=0; k<mata->nnzrow[i]; k++)
	    if (fabs(kr[k]) > scal) scal = fabs(kr[k]);
      }
      else if (nrm == 1) {
         for (k=0; k<mata->nnzrow[i]; k++)
            scal += fabs(kr[k]);
      }
      else {  /* nrm = 2 */
         for (k=0; k<mata->nnzrow[i]; k++)
            scal += kr[k]*kr[k];
      }
      if (nrm == 2) scal = sqrt(scal);
      if (scal == 0.0) {
	 return i+1;
      }
      else 
	 scal = 1.0 / scal;
      diag[i] = scal;
      for (k=0; k<mata->nnzrow[i]; k++)
	 kr[k] = kr[k] * scal;
     
   }
   return 0;
}
/*---------------end of roscalC-----------------------------------------
----------------------------------------------------------------------*/
int coscalC(csptr mata, REAL *diag, int nrm)
{
/*---------------------------------------------------------------------
|
| This routine scales each column of mata so that the norm is 1.
|
|----------------------------------------------------------------------
| on entry:
| mata  = the matrix (in SparRow form)
| nrm   = type of norm
|          0 (\infty),  1 or 2
|
| on return
| diag  = diag[j] = 1/norm(row[j])
|
|     0 --> normal return
|     j --> column j is a zero column
|--------------------------------------------------------------------*/
/*   local variables    */
   int i, j, k;
   REAL *kr;
   int *ki;
   for (i=0; i<mata->n; i++)
      diag[i] = 0.0;
/*---------------------------------------
|   compute the norm of each column
|--------------------------------------*/
   for (i=0; i<mata->n; i++) {
      kr = mata->ma[i];
      ki = mata->ja[i];
      if (nrm == 0) {
	 for (k=0; k<mata->nnzrow[i]; k++) {
	    j = ki[k];
	    if (fabs(kr[k]) > diag[j]) diag[j] = fabs(kr[k]);
	 }
      }
      else if (nrm == 1) {
         for (k=0; k<mata->nnzrow[i]; k++)
            diag[ki[k]] += fabs(kr[k]);
      }
      else {
         for (k=0; k<mata->nnzrow[i]; k++)
            diag[ki[k]] += fabs(kr[k])*fabs(kr[k]);
      }
   }
   if (nrm == 2) {
      for (i=0; i<mata->n; i++)
	 diag[i] = sqrt(diag[i]);
   }
/*---------------------------------------
|   invert
|--------------------------------------*/
   for (i=0; i<mata->n; i++) {
      if (diag[i] == 0.0)
	 return i+1;
      else 
	 diag[i] = 1.0 / diag[i];
   }
/*---------------------------------------
|   C = A * D
|--------------------------------------*/
   for (i=0; i<mata->n; i++) {
      kr = mata->ma[i];
      ki = mata->ja[i];
      for (k=0; k<mata->nnzrow[i]; k++)
	 kr[k] = kr[k] * diag[ki[k]];
   }
   return 0;
}
/*---------------end of coscalC-----------------------------------------
----------------------------------------------------------------------*/
void dscale(int n, REAL *dd, REAL *x, REAL * y)
{ 
/* Computes  y == DD * x                               */
/* scales the vector x by the diagonal dd - output in y */
  int k;
  for (k=0; k<n; k++) 
    y[k] = dd[k]*x[k];
}

void printmat(FILE *ft, csptr A, int i0, int i1)
{
/*-------------------------------------------------------------+
| to dump rows i0 to i1 of matrix for debugging purposes       |
|--------------------------------------------------------------*/
  int i, k, nzi;
  int *row;
  REAL *rowm;
  for (i=i0; i<i1; i++) {
    nzi = A->nnzrow[i];
    row = A->ja[i];
    rowm = A->ma[i];
    for (k=0; k< nzi; k++){
      fprintfv(5, ft," row %d  a  %e ja %d \n", i+1, rowm[k], row[k]+1);
    }
  }
}

